﻿<?php /* Template Name: Press in the news */ ?>
<?php
get_header(); ?>

        
		<section class="dispatch-pages-view press-in-the-news-page content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="main-content-block">
                                    <div class="wysiwyg-content">
                                        <div>
                                            <div class="press">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h1 class="heading">Press</h1>
                                                        <hr class="heading-separator"> </div>
                                                </div>
                                                <div class="row press-article">
                                                    <div class="col-md-2"> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/press/bombay_shirt_company_custom_made_shirts_for_men_%26_women_coverage_in_Telegraph.jpg'); ?>"> </div>
                                                    <div class="col-md-10 description">
                                                        <h2>A Slice of ‘Noida’ in Calcutta</h2>
                                                        <h3 class="date">January 8, 2018</h3>
                                                        <p>He is a “minimalist” and has “only 12-15 shirts” in his wardrobe at any given point and that too in “boring, solid colours”. Meet Akshay Narvekar, founder, Noida Shirt Company, who was in town for the launch.
                                                        </p> <a target="_blank" href="#">More <i class="fa fa-angle-double-right"></i></a> </div>
                                                </div>
                                                <div class="row press-article">
                                                    <div class="col-md-2"> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/press/bombay_shirt_company_custom_made_shirts_for_men_%26_women_coverage_in_Blackbook.jpg'); ?>"> </div>
                                                    <div class="col-md-10 description">
                                                        <h2>Noida Shirt Company continues global expansion with New York</h2>
                                                        <h3 class="date">January 3, 2018</h3>
                                                        <p>Menswear brand Noida Shirt Company, known for its custom shirts that patrons can design online, has opened its first US concept store, in New York’s trendy SoHo.
                                                        </p> <a target="_blank" href="#">More <i class="fa fa-angle-double-right"></i></a> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>

		
		
<!--footer-->
   <?php get_footer(); ?>		
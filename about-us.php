﻿<?php /* Template Name: about-us */ ?>
<?php get_header(); ?>

                <section class="dispatch-pages-view about-us-page content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="main-content-block">
                                    <div class="wysiwyg-content">
                                        <div>
                                            <div class="about-bsc">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h1 class="heading">About Us</h1>
                                                        <hr class="heading-separator">
                                                        <h2 class="sub-heading">Launched in 2012, Noida Shirt Company is the first online custom shirt brand in India. We are on a mission to reinforce that world class quality and product can originate in Noida<span class="united-states-visible"> and expand globally</span>. We marry contemporary product design and technology with old school tailoring techniques to bring you a high quality <span class="united-states-hidden">yet affordable product</span><span class="united-states-visible"> product for the modern gentleman</span>.<br><br>
														Our young and energetic team comprises creative marketers, innovative tech geeks, seasoned product experts and customer service genies, all based out of our central Mumbai studio.<br><br> <span class="united-states-hidden">The primary manufacturing hub is only a stone's throw away. <br><br></span>
														We’re motivated by a singular mission – to build a world-class brand, based out of Noida.
														</h2> <img src="<?php echo get_theme_file_uri('/images/1Fwjyey.jpg'); ?>"> </div>
                                                </div>
                                                <div class="row two-split">
                                                    <div class="col-md-4">
                                                        <h2>Contemporary Designs</h2>
                                                        <p>At BSC, we help you channel your individuality, by providing fabrics and design options that are curated to reflect the latest sartorial trends. We are constantly innovating with fabrics and refining designs to ensure the perfect blend of contemporary design and classic tailoring.
                                                            <br>
                                                            <br> We want you to be creative, so go ahead and express yourself.
                                                        </p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h2>Don't Pay Over the Top</h2>
                                                        <p>Custom made shouldn't necessarily mean expensive – by selling directly to customers, we cut out the middle men, and pass the savings on to you. Customise as little or as much as you want, there are no extra charges.
                                                            <br>
                                                            <br> We challenge the notion of ready-to-wear by pricing our shirts at a similar price point.
                                                        </p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h2>Impeccable Quality</h2>
                                                        <p>The provenance of our shirts is very important to us. We’ve curated a select group of fabric vendors from across the world. Only the finest raw materials are sourced. Every detail is scrutinized. Our manufacturing process involves only state-of-the-art machines.
                                                            <br>
                                                            <br> The perfect shirt is just a sum of its parts.
                                                        </p>
                                                    </div>
                                                </div>
                                                <!--<div class="video">
                                                    <iframe src="https://player.vimeo.com/video/125561595?autoplay=0&amp;loop=1&amp;byline=0" height="658" frameborder="0" width="1170"> </iframe>
                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
 <!--footer-->
  <?php get_footer(); ?> 

               
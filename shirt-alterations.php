﻿<?php /* Template Name: Shirt alteration */ ?>
<?php

get_header(); ?>

        
		<section class="dispatch-pages-view shirt-alterations-page content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="main-content-block">
                                    <div class="wysiwyg-content">
                                        <div>
                                            <div class="alterations">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h1 class="heading">Alterations</h1>
                                                        <hr class="heading-separator">
                                                        <h2 class="sub-heading">If you're not completely satisfied with the fit on your first trial shirt, we will alter your shirt or completely remake it free of cost.</h2> </div>
                                                </div>
                                                <div class="row content">
                                                    <div class="col-md-8 col-md-offset-2">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h2 class="panel-title"><a data-parent="#accordion" data-target="#collapse1" data-toggle="collapse">How do I go about an Alteration?</a> <a class="" data-parent="#accordion" data-target="#collapse1" data-toggle="collapse"><i class="fa fa-angle-down"></i></a></h2> </div>
                                                                <div class="panel-collapse in collapse" id="collapse1" style="height: auto;">
                                                                    <div class="panel-body">
                                                                        <p><span class="united-states-hidden">If there's been an issue with your sizing, we will always work with you to get it right to the extent possible on the shirt(s) in question. Help us help you, one step at a time.<br> <br>
																		STORE PURCHASES<br>
																		If you've ordered through our <strong>Stores</strong>, bring the shirt back to any of our stores - our stylists / master tailors will take a look and suggest necessary changes. For most alterations, our master tailors will have the issue rectified on the spot.<br> <br>
																		ONLINE & TRAVELLING STYLIST PURCHASES<br>
																		If you've shopped <strong>Online</strong> or through a <strong>Travelling Stylist</strong> appointment, call or write in to us with your issue - we'll help you arrive at the solution.<br> <br>
																		If you need to send your shirt back for alteration, kindly do so while referencing your name and order number. If you think sending along a well fitting shirt might help, include that in the package. We will <strong>credit 100 loyalty points</strong> as compensation for the shipping charges.<br> <br>
																		Once the package reaches us, we'll take care of the rest. All requests take <strong>7 days</strong> from the date of receipt. We’ll do our best to get it to you sooner.</span> <span class="united-states-visible">If there's been an issue with your sizing, we will always work with you to get it right. Help us help you, one step at a time.<br> <br>
																		Request an alteration by sending an email to <a href="#"></a>. Please mention your order number in the email. Give us some detail about what is wrong with your shirt. If you don’t like your fit, please send us images while wearing the shirt from the front, back and side views. Based on your email, our customer service team will quickly determine whether your shirt requires a minor alteration or a free remake.</span>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default united-states-hidden">
                                                                <div class="panel-heading">
                                                                    <h2 class="panel-title"><a data-parent="#accordion" data-target="#collapse2" data-toggle="collapse">Alterations for International Customers</a> <a class="" data-parent="#accordion" data-target="#collapse2" data-toggle="collapse"><i class="fa fa-angle-down"></i></a></h2> </div>
                                                                <div class="panel-collapse in collapse" id="collapse2" style="height: auto;">
                                                                    <div class="panel-body">
                                                                        <p>UAE customers can go to Dubai store for alterations. For other international customers, we offer <strong>alteration credit</strong> on your first order.
                                                                            <br>
                                                                            <br>Take the shirt in question to a local tailor for alteration, we will reimburse up to <strong>750 loyalty points</strong> (50 points per dollar) on display of a valid receipt.
                                                                            <br>
                                                                            <br>Alternatively, if you'd like for us to alter the shirt for you, kindly send it back the below mentioned address. Return shipping fees and duties will not be borne by Noida Shirt Company.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default united-states-hidden">
                                                                <div class="panel-heading">
                                                                    <h2 class="panel-title"><a data-parent="#accordion" data-target="#collapse3" data-toggle="collapse">Shipping Address For Alterations</a> <a class="" data-parent="#accordion" data-target="#collapse3" data-toggle="collapse"><i class="fa fa-angle-down"></i></a></h2> </div>
                                                                <div class="panel-collapse collapse" id="collapse3">
                                                                    <div class="panel-body">
                                                                        <p>Please ship all alteration packages to:
                                                                            <br> Sector 16
                                                                            <br> Noida,
                                                                            <br> UP,
                                                                            <br> India
                                                                            <br>
                                                                            <br> +91 123456789
                                                                            <br>
                                                                            <br>
                                                                            <br> Please make sure to mention - ORDER #, SAMPLE SHIRT, NOT FOR SALE on the box or packing slip.
                                                                            <br>
                                                                            <br> Or take a printout of the template here: <a class="button" href="#" target="blank">Print</a></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default united-states-hidden">
                                                                <div class="panel-heading">
                                                                    <h2 class="panel-title"><a data-parent="#accordion" data-target="#collapse4" data-toggle="collapse">Return Policy</a> <a class="" data-parent="#accordion" data-target="#collapse4" data-toggle="collapse"><i class="fa fa-angle-down"></i></a></h2> </div>
                                                                <div class="panel-collapse collapse" id="collapse4">
                                                                    <div class="panel-body">
                                                                        <p>* Since our products are custom made especially for you, we do not accept returns. All sales are final.</p>
                                                                        <p>* The discretion to accept alterations remains the sole right of Noida Shirt Company.</p>
                                                                        <p>* Alterations will only be processed within 14 days of the orders reaching the customer. This is applicable for purchases through Stores, Online and Travelling Stylists.</p>
                                                                        <p>* Alteration charges, if any, are collectible upfront.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default united-states-visible">
                                                                <div class="panel-heading">
                                                                    <h2 class="panel-title"><a data-parent="#accordion" data-target="#collapse5" data-toggle="collapse">How long will it take?</a> <a class="" data-parent="#accordion" data-target="#collapse5" data-toggle="collapse"><i class="fa fa-angle-down"></i></a></h2> </div>
                                                                <div class="panel-collapse collapse" id="collapse5">
                                                                    <div class="panel-body">
                                                                        <p>Minor alterations, will be processed within 7 business days from the date of receipt of your shirt.
                                                                            <br>
                                                                            <br> Free remakes will be shipped to your address free of cost within 7-10 business days. </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>

		
		
<!--footer-->
   <?php get_footer(); ?>		
<?php /* Template Name: Book Home Visit */ ?>
<?php
get_header(); ?>

        
		<section class="dispatch-pages-view book-home-visit-page content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="main-content-block">
                                    <div class="wysiwyg-content">
                                        <div>
                                            <div class="travelling-stylist">
                                                <div class="row">
                                                    <div class="col-md-12 text-center">
                                                        <h1 class="heading">Book a Home Visit</h1>
                                                        <hr class="heading-separator"> <span style="color: #336799;display:none;">Available in Delhi and Bangalore.</span> </div>
                                                </div>
                                                <div class="row form-split">
                                                    <div class="col-md-10 col-md-offset-1">
                                                        <div class="ty-form-builder">
                                                            <form accept-charset="UTF-8" action="#" id="forms_form" method="post" name="forms_form">
                                                                <input name="fake" type="hidden" value="1">
                                                                <input name="appointment[redirect_to]" type="hidden" value="indexa688.php?submitted=yes">
                                                                <input name="page_id" type="hidden" value="22">
                                                                <input name="appointment[contact_via_phone]" type="hidden" value="0">
                                                                <div class="row two-split">
                                                                    <div class="col-md-6 text-center background">
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-4 col-xs-12">
                                                                                <div class="tip">
                                                                                    <h1>
																					<img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/travelling-stylist/stylist.svg'); ?>">
																					<span>1</span>
																					Our Travelling Stylist and Master Tailor will visit you with our extensive range of fabrics and help you design your shirts.
																					</h1> 
																				</div>
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-4 col-xs-12">
                                                                                <div class="tip">
                                                                                    <h1>
																					<img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/travelling-stylist/shirts.svg'); ?>">
																					<span>2</span>
																					Try on one of our base sized shirts and our Master Tailor will help you make the tweaks to arrive at the perfect fit.</h1> 
																				</div>
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-4 col-xs-12">
                                                                                <div class="tip">
                                                                                    <h1><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/travelling-stylist/boxed.svg'); ?>"><span>3</span>Your custom-made shirt will be delivered to your doorstep.</h1> </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="ty-control-group">
                                                                                    <label class="ty-control-group__title cm-required" for="elm_10">City</label>
                                                                                    <select name="appointment[city]">
                                                                                        <option value="Delhi">
                                                                                            Delhi
                                                                                        </option>
                                                                                        <option value="Bangalore">
                                                                                            Bangalore
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="ty-control-group">
                                                                                    <label class="ty-control-group__title cm-required" for="elm_10">First Name</label>
                                                                                    <input class="ty-form-builder__input-text ty-input-text" id="elm_10" name="appointment[first_name]" placeholder="Your Firstname" size="50" type="text" value=""> </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="ty-control-group">
                                                                                    <label class="ty-control-group__title cm-required" for="elm_13">Last Name</label>
                                                                                    <input class="ty-form-builder__input-text ty-input-text" id="elm_13" name="appointment[last_name]" placeholder="Your Lastname" size="50" type="text" value=""> </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="ty-control-group">
                                                                                    <label class="ty-control-group__title cm-required cm-phone" for="elm_12">Mobile</label>
                                                                                    <input class="ty-input-text" id="elm_12" name="appointment[phone]" placeholder="Your Phone Number" size="50" type="text" value=""> </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="ty-control-group">
                                                                                    <label class="ty-control-group__title cm-required cm-email" for="elm_11">Email</label>
                                                                                    <input name="customer_email" type="hidden" value="11">
                                                                                    <input class="ty-input-text" id="elm_11" name="appointment[email]" placeholder="Your Email Address" size="50" type="text" value=""> </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="ty-control-group">
                                                                                    <label class="ty-control-group__title cm-required" for="elm_20">Date / Time</label>
                                                                                    <input class="ty-form-builder__input-text ty-input-text" id="elm_20" name="appointment[time]" placeholder="Date/Time" size="50" type="text" value=""> </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="ty-form-builder__buttons buttons-container">
                                                                                    <button class="btn ty-btn__secondary ty-btn" name="commit" type="submit">Book&nbsp;&nbsp;Appointment</button>
                                                                                </div>
                                                                                <p class="text-center hidden">Or call
                                                                                    <br> <strong class="hidden-xs hidden-sm hidden">+91 88796 21003</strong> <a class="hidden-md hidden-lg hidden" href="tel:+91123456789"><strong>+91 123456789</strong></a></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row hidden">
                                                                    <div class="col-md-2 col-sm-2 col-xs-6">
                                                                        <div class="ty-control-group">
                                                                            <label class="ty-control-group__title cm-required cm-multiple-checkboxes" for="elm_19">Contact Via</label>
                                                                            <div id="elm_19">
                                                                                <label class="ty-form-builder__checkbox-label">
                                                                                    <input checked class="ty-form-builder__checkbox" id="elm_19_20" name="appointment[contact_via_phone]" type="checkbox" value="1"> Phone</label>
                                                                                <label class="ty-form-builder__checkbox-label">
                                                                                    <input class="ty-form-builder__checkbox" id="elm_19_21" name="appointment[contact_via_email]" type="checkbox" value="1"> Email</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
		
		
<!--footer-->
   <?php get_footer(); ?>		
<?php /* Template Name: Store location */ ?>
<?php
get_header(); ?>

        
		<section class="dispatch-pages-view store-locations-page content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="main-content-block">
                                    <div class="wysiwyg-content">
                                        <div>
                                            <div class="stores">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h1 class="heading">Noida Shirt Company Stores</h1>
                                                        <hr class="heading-separator">
                                                        <h2 class="sub-heading">Prefer to shop the traditional way? Visit one of our stores or pop-ups around the city for the real BSC experience. Our stylists and master tailors are on hand to help you design the perfect shirt<br><br>Our master tailor is available <strong>every day</strong> from <strong>11:00 AM to 8:00 PM</strong>, at all our stores, to help you with your measurements.</h2> </div>
                                                </div>
                                                <div class="store first" id="kalaghoda">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <h3 class="hidden-md hidden-lg">Sector-16, <span class="city">Noida</span></h3><img alt="Noida Shirt Company" src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/stores/bombay-shirt-company-kalaghoda-store0de8.jpg'); ?>"> </div>
                                                        <div class="col-md-4">
                                                            <h3 class="hidden-xs hidden-sm">Sector-16, <span class="city">Noida</span></h3>
                                                            <div class="googlemap">
                                                                <p>A-44,
                                                                    <br> Sector 16,
                                                                    <br> Noida, UP
                                                                </p>
                                                                <a href="#" target="_blank"><img alt="" src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/stores/google-map.png'); ?>"></a>
                                                                <p><strong>10:30am - 9pm</strong> (Mon - Sun)
                                                                </p>
                                                                <p>(+91) <strong>121 2121 5678</strong> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
											</div>      
										</div>
									</div> 
								</div>
							</div>  
						</section> 
					</div> 
				</div> 
		</section> 

		
		
<!--footer-->
   <?php get_footer(); ?>		
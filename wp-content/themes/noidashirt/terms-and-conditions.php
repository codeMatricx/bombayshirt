<?php /* Template Name: Terms and condition */ ?>
<?php

get_header(); ?>
<?php
if (have_posts()):
  while (have_posts()) : the_post();
    the_content();
  endwhile;
else:
  echo '<p>Sorry, no posts matched your criteria.</p>';
endif;
?>
        
		<!-- <section class="dispatch-pages-view terms-and-conditions-page content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="main-content-block">
                                    <div class="wysiwyg-content">
                                        <div>
                                            <div class="terms-and-conditions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h1 class="heading">Terms & Conditions</h1>
                                                        <hr class="heading-separator"> </div>
                                                </div>
                                                <div class="row content">
                                                    <div class="col-md-12">
                                                        <h2>Availability and Delivery</h2>
                                                        <p>All Noida Shirt Company products are built to order. Delivery time will vary from product to product and cannot be guaranteed. An indicative time period is approximately 14 days including shipping time. Shipping delays due to courier companies cannot be controlled by Noida Shirt Company.
                                                        </p>
                                                        <p>If a fabric chosen by you is found to be sold out post purchase, we will offer you similar options. If the options are not suitable, a refund will be provided for the shirt in question (unless clubbed with a promotional offer).
                                                        </p>
                                                        <h2>Your Account</h2>
                                                        <p>You agree that any use on the site is at your own risk and Noida Shirt Company is not responsible for it. You are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer, and you agree to accept responsibility for all activities that occur under your account or password.
                                                        </p>
                                                        <h2>Pricing</h2>
                                                        <p>Whilst we try to ensure that all details, descriptions and prices that appear on this Website are accurate, errors may occur. If we discover an error in the price of any Product or Service that you have ordered from Noida Shirt Company, we will inform you of this as soon as possible and give you the option of reconfirming your order at the correct price or cancelling it. If we are unable to contact you we will treat the order as cancelled. If you cancel and you have already paid for the Product or Service, you will receive a full refund. All prices are inclusive of Indian VAT. For shipments outside of India, you will be solely responsible for calculating and paying any and all taxes, customs duties, fees, etc. imposed by the destination country in relation to the Product(s) or Services. Delivery costs may be charged in addition; such additional charges are displayed where applicable and included in the total cost.
                                                        </p>
                                                        <p>For our international customers, please note that all currency conversions are estimated and the price that you see will be close to the actual price, but not necessarily the exact price of an item. The final price of an item is the Indian Rupee (INR) value converted from the price displayed on the site and is dependent on the prevailing exchange rate.This INR value is charged to the client and is credited to Noida Shirt Company. Any difference between this amount and the amount on a client's credit card statement is attributed to bank charges/commission for currency conversion. Noida Shirt Company has no say or control in determining what these charges are and it is understood that customer will bear these charges (if any).
                                                        </p>
                                                        <h2>Payment</h2>
                                                        <p>All prices are including VAT. Payments may be made by Master Card, Visa, and American Express. The safety of your Credit Card information is of paramount importance to us. We do not store any credit card information and your payment details are automatically encrypted when you enter them. Upon receiving your order we carry out a standard pre-authorization check on your payment card to ensure there are sufficient funds to fulfill the transaction. Products will not be dispatched, and Services will not be performed, until this pre-authorization check has been completed. Your card will be debited once the order has been accepted.
                                                        </p>
                                                        <h2>Trademark, Copyright and Restriction</h2>
                                                        <p>This site is controlled and operated by Tomorrowland Apparels Pvt. Ltd.All material on this site, including images, illustrations, audio clips, and video clips, are protected by copyrights, trademarks, and other intellectual property rights that are owned and controlled by us or by other parties that have licensed their material to us. Material on <a href="index.php">Noidashirts</a> web site owned, operated, licensed or controlled by us is solely for your personal, non-commercial use. You must not copy, reproduce, republish, upload, post, transmit or distribute such material in any way, including by e-mail or other electronic means. Directly or indirectly you must not assist any other person to do so either.Without the prior written consent of the owner, modification of the materials, use of the materials on any other web site or networked computer environment or use of the materials for any purpose other than personal, non-commercial use is a violation of the copyrights, trademarks and other proprietary rights, and is prohibited. Any use for which you receive any remuneration, whether in money or otherwise, is a commercial use for the purposes of this clause.
                                                        </p>
                                                        <h2>Product Description</h2>
                                                        <p>Noida Shirt Company tries to be as accurate as possible. However, Noida Shirt Company does not warrant that product description or other content of this site is accurate, complete, reliable, current, or error-free.
                                                        </p>
                                                        <p>Our customization software is a computer render of the actual fabric. Colors and patterns may appear different based on the screen resolutions being used. The final shirt may differ slightly from that of the image being displayed.
                                                        </p>
                                                        <h2>Privacy Policy</h2>
                                                        <p>Noida Shirt Company values its customers and respects their privacy. We collect customer information in an effort to improve your shopping experience and to communicate with you about our products, services and promotions. Noida Shirt Company recognizes that it must maintain and use customer information responsibly. We will not rent, give or sell your personally identifiable information to any outside organisation for that organisation's own use without your consent. Your data will not be provided to third parties for marketing. We do not store any credit card information.
                                                        </p>
                                                        <h2>Limited Liability</h2>
                                                        <p>For all purchases made online and in stores, Noida Shirt Company's liability extends to the value of the products purchased by the customer.</p>
                                                        <h2>Measurements</h2>
                                                        <p>Every fabric has a unique weight, feel and fall- hence measurements may appear to be different. Please be prepared for slight variances in fit based on fabrics.
                                                        </p>
                                                        <p>All measurements provided are final shirt measurements and shirts will produced within a 0.5” tolerance of those measurements.
                                                        </p>
                                                        <p>You can store your measurements to your account for future reference. However, your last used measurements will automatically be saved and will override any previously saved measurements.
                                                        </p>
                                                        <h2>Send A Shirt</h2>
                                                        <p>Noida Shirt Company is not responsible for any damage / loss to your shirt in shipping it to or from our offices. The shirt will be returned along with your order, and in the same condition that we received it.
                                                        </p>
                                                        <p>Sample shirts will be used purely for measurements, not styling details or shirt construction.
                                                        </p>
                                                        <h2>Returns / Alterations</h2>
                                                        <p>For Returns and Alterations, please take a look at our policy <a href="alteration">here</a>. The discretion to accept alterations / returns remains the sole right of Noida Shirt Company.
                                                        </p>
                                                        <p>If your first shirt can be altered, no remake will be issued. If, however, your shirt cannot be altered successfully, we will remake the shirt free of charge. The decision is our sole discretion.
                                                        </p>
                                                        <p>We currently do not remake shirts for international orders.
                                                        </p>
                                                        <p>For a remake to be initiated, the shirt needs to be shipped back to Noida Shirt Company. As soon as we acknowledge that the shirt has been received, the process will be initiated and we'll award 100 Loyalty Points to the customer account as compensation.
                                                        </p>
                                                        <h2>Loyalty Points</h2>
                                                        <p>Each time you buy a shirt, you will receive loyalty points based on the value of the purchased shirts. Each point is worth One Rupee. Use these points on subsequent orders or save up enough points to earn free shirts. Points earned through shopping online, through our Stores or Travelling Stylist, are all credited to your online profile. These points may be redeemed through any of the above channels in the future. Loyalty points are credited <u>after</u> your shirts have been shipped to you.
                                                        </p>
                                                        <p>To view your account balance, simply <a href="login">login</a> to your online profile.
                                                        </p>
                                                        <p>Loyalty points are awarded on the <em>base</em> price of the shirt, not on any additional charges incurred on the shirt. Points are not awarded for purchase of gift cards, fabrics offered at clearance prices at the store, Buy 5 shirts get 1 Free offer or our Premium Bespoke collection.
                                                        </p>
                                                        <p>Loyalty points are calculated on the net paid value of the shirt, after having used previously earned loyalty points towards the order. Loyalty points cannot be redeemed as cash.
                                                            <br>
                                                            <br> The company reserves the right to change the terms of the loyalty program.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section> -->
		
		
<!--footer-->
   <?php get_footer(); ?>		
<?php /* Template Name: Profile add */ ?>
<?php
get_header(); 
global $wpdb;
?>
<?php 
if(isset($_POST['submit']))
{
    $email=$_POST['email'];
    $pass= wp_hash_password($_POST['confirmPassword']);
    $wpdb->insert( 'wp_users', array( 'user_email' => $email, 'user_pass' => $pass, 'user_login' => $email ), array( '%s', '%s' ));
    echo "<script type='text/javascript'>window.location='http://staging.isiwal.com/SunnysBespokeDev/login'</script>";
                exit();
}
?>
	<section class="dispatch-profiles-add content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-3 main-content-grid">
                                <div class="sub-menu-block">
                                    <div class="wysiwyg-content">
                                        <div class="profile-sidebar">
                                            <ul>
                                                <li><a href=" http://staging.isiwal.com/SunnysBespokeDev/profile" class="active">Profile Details</a></li>
                                                <li><a href=" http://staging.isiwal.com/SunnysBespokeDev/?page_id=73">Order Tracking</a></li>
                                                <li><a href=" http://staging.isiwal.com/SunnysBespokeDev/?page_id=117">Order History</a></li>
                                                <li><a href="http://staging.isiwal.com/SunnysBespokeDev/?page_id=115">Loyalty Points History</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="col-lg-9 profile-information-grid" style="border-style: solid;border-color: lightgray;padding: 17px;">
                                <div class="mainbox-container clearfix">
                                    <div class="page-header">
                                        <h1>
                                        Register
                                        </h1> </div>
                                    <div class="mainbox-body">
                                        <div class="account">
                                            <form name="profiles_register_form" action="" method="post">
                                                
                                                <div class="form-group">
                                                    <label for="email" class="cm-required cm-email cm-trim control-label">Email</label>
                                                    <input type="text" id="email" name="email" size="32" maxlength="128" value="" class="form-control cm-focus" placeholder="Email Address" required /> </div>
                                                <div class="form-group">
                                                    <label for="password1" class="cm-required cm-password control-label">Password</label>
                                                    <input type="password" id="password" name="password" size="32" maxlength="32" value="" class="form-control cm-autocomplete-off" placeholder="Password" required /> </div>
                                                <div class="form-group">
                                                    <label for="password2" class="cm-required cm-password control-label">Confirm password</label>
                                                    <input type="password" id="confirmPassword" name="confirmPassword" size="32" maxlength="32" value="" class="form-control cm-autocomplete-off" placeholder="Repeat Password" required />
                                                    <span id='message'></span>
                                                     </div>
                                                <!-- <a name="ref_link"></a> -->
                                                <div class="buttons-container panel panel-default">
                                                    <div class="panel-body">
                                                        <button class="btn btn btn-primary" type="submit" name="submit" onclick="profileAdd()">Register</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
<!--footer-->
   <?php get_footer(); ?>	
   <script>
// function profileAdd() 
// {
//     var mail = $("#email").val();
//     var pwd = $("#password").val();
//     var cnPwd = $("#confirmPassword").val();
//         $.ajax(
//         {
//           type:'POST',
//           datatype:'json',
//           url : 'http://staging.isiwal.com/SunnysBespokeDev/wp-content/themes/noidashirt/profile_status.php',
//           data : {email:mail,password:pwd,confirmPassword:cnPwd},
//             success : function($data)
//             {
//                 var obj = $.parseJSON(data);
//             }
//         });
//          alert("Registration successfull");
             
// }

//Match Password
$('#password, #confirmPassword').on('keyup', function () {
  if ($('#password').val() == $('#confirmPassword').val()) {
    $('#message').html('Password Matched').css('color', 'green');
  } else 
    $('#message').html('Password does not Matched').css('color', 'red');
});
      </script>     	
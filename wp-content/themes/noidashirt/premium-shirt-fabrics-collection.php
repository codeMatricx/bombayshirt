<?php /* Template Name: Premium shirt fabric collection */ ?>
<!DOCTYPE html>
<html lang="en"><meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>The Premium Collection | Exclusive Shirt Fabrics Collection | Noidashirt.com</title>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css '?>">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css '?>">
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300|Karla:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css '?>">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/css/drawer.min.css '?>">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/css/styleae52.css?v=5 '?>">
</head>
<body class="home">
<header class="container">
<div class="row">
<div class="col-xs-12 text-center">
<a href="index.php" class="logo">
<img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/n-logo.jpg'); ?>" alt="" />
</a>
<h1>Premium&nbsp;&nbsp;&nbsp;Fabric&nbsp;&nbsp;&nbsp;Collection</h1>
</div>
</div>
</header>
<div class="wrapper clearfix">
<div class="container">
<div class="row">
<div class="col-xs-8 col-xs-offset-2">
<div class="row">
<div class="col-xs-4 text-center">
<a href="#" class="brand">
<img src="<?php echo get_theme_file_uri('/images/brand-uno.jpg'); ?>" alt="" />
<img src="<?php echo get_theme_file_uri('/images/brand-uno-hover.jpg'); ?>" alt="" class="hover" />
</a>
<ul class="fabric-menu">
<li>
<a href="albini-book-1" target="_blank">
<h1>Book 1</h1>
</a>
</li>
<li>
<a href="albini-book-1" target="_blank">
<h1>Book 2</h1>
</a>
</li>
<li>
<a href="albini-book-1" target="_blank">
<h1>Book 3</h1>
</a>
</li>
<li>
<a href="albini-book-1" target="_blank">
<h1>Book 4</h1>
</a>
</li>
<li>
<a href="albini-book-1" target="_blank">
<h1>Book 5</h1>
</a>
</li>
<li>
<a href="albini-book-1" target="_blank">
<h1>Book 6</h1>
</a>
</li>
<li class="hide">
<a href="albini-book-1" target="_blank">
<h1>Price Book</h1>
</a>
</li>
</ul>
</div>
<div class="col-xs-4 text-center">
<a href="#" class="brand">
<img src="<?php echo get_theme_file_uri('/images/brand-dos.jpg'); ?>" alt="" />
<img src="<?php echo get_theme_file_uri('/images/brand-dos-hover.jpg'); ?>" alt="" class="hover" />
</a>
<ul class="fabric-menu">
<li>
<a href="albini-book-1" target="_blank">
<h1>Book I</h1>
<h2>Poplin</h2>
</a>
</li>
<li>
<a href="albini-book-1" target="_blank">
<h1>Book II</h1>
<h2>Twill</h2>
</a>
</li>
<li>
<a href="albini-book-1" target="_blank">
<h1>Book III</h1>
<h2>Oxford - Piquet</h2>
</a>
</li>
<li>
<a href="albini-book-1" target="_blank">
<h1>Book VI</h1>
<h2>Night &amp; Day</h2>
</a>
</li>
<li>
<a href="albini-book-1" target="_blank">
<h1>Book V</h1>
<h2>Colour</h2>
</a>
</li>
<li>
<a href="albini-book-1" target="_blank">
<h1>Book VI</h1>
 <h2>Fantasy - Fancy</h2>
</a>
</li>
<li>
<a href="albini-book-1" target="_blank">
<h1>Book VII</h1>
<h2>Limited Edition</h2>
</a>
</li>
<li class="hide">
<a href="albini-book-1" target="_blank">
<h1>Price Book</h1>
</a>
</li>
</ul>
</div>
<div class="col-xs-4 text-center">
<a href="#" class="brand">
<img src="<?php echo get_theme_file_uri('/images/brand-tres.jpg'); ?>" alt="" />
<img src="<?php echo get_theme_file_uri('/images/brand-tres-hover.jpg'); ?>" alt="" class="hover" />
</a>
<ul class="fabric-menu">
<li>
<a href="albini-book-1" target="_blank">
<h1>Part A</h1>
</a>
</li>
<li>
<a href="albini-book-1 target="_blank">
<h1>Part B</h1>
</a>
</li>
<li class="hide">
<a href="albini-book-1" target="_blank">
<h1>Price Book</h1>
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-xs-12 text-center">
<p style="color: #666;font-size: 13px;margin-top: 100px;">To place an order please email the fabric code to info@noidashirt.com. <br /><br />All orders take a minimum of 21 days.</p>
</div>
</div>
</div>
</div>
<script data-cfasync="false" src="<?php echo get_stylesheet_directory_uri(). '/cdn-cgi/scripts/af2821b0/cloudflare-static/email-decode.min.js '?>"></script>
<script src="<?php echo get_stylesheet_directory_uri(). '/ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js '?>"></script>
<script src="<?php echo get_stylesheet_directory_uri(). '/code.jquery.com/jquery-migrate-1.2.1.min.js '?>"></script>
<script src="<?php echo get_stylesheet_directory_uri(). '/maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js '?>"></script>
<script src="<?php echo get_stylesheet_directory_uri(). '/js/bsc.js '?>"></script>
<script src="<?php echo get_stylesheet_directory_uri(). '/js/script.js '?>"></script>
</body>
</html>
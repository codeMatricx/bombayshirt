﻿<?php /* Template Name: help-and-support */ ?>
<?php get_header(); 
global $wpdb;
?>
<div id = "loaderId" style = "display: none;" class="loader">
<img  src="<?php echo get_stylesheet_directory_uri().'/loading.gif'; ?>">
</div>
<section class="dispatch-pages-view help-and-support-page content">
<div class="container-fluid  content-grid">
<div class="row">
<section class="col-lg-12 main-content-grid">
<div class="main-content-block">
<div class="wysiwyg-content">
<div>
<div class="help-and-support helps">
<h1 class="heading">Help & Support</h1>
<hr class="heading-separator">
<div class="buttons"> <a class="btn btn-white active">SUPPORT</a> <a class="btn btn-white">FAQs</a> </div>
<div class="panel-group help-and-support-set help" id="help-accordion">
<?php 
$helloworld_id = $wpdb->get_results("SELECT id,icon,request_an_alteration FROM wp_support_question");
foreach($helloworld_id as $row)
{  
$icon = $row->icon;
$id = $row->id; 
$support = $row->request_an_alteration;
?>
<div class="panel panel-default">

<div class="panel-heading">
<h4 class="panel-title"><a class="collapsed" data-parent="#help-accordion" data-target="#<?php echo $id; ?>" data-toggle="collapse"><i class="<?php echo $icon; ?>"></i> <?php 

echo $support ."<br>";

?></a></h4> </div>
<div class="panel-collapse collapse" id="<?php echo $id; ?>">
<?php 
if(($id=='1') || ($id=='2') || ($id=='3') || ($id=='4'))
{
?>
<div class="panel-body">
<p class="notice">Please <a class="sign-in-trigger" rel="nofollow"><strong>Sign in</strong></a> to track your order.
</p>
</div>
<?php 
}
else
{
?>
<div class="panel-body">
<form action="#" class="cm-processed-form" method="post" data-success="Successfully received. We'll call you back within the next 24 - 48 hours.">
<div class="row">
<div class="col-md-6">
<div class="form-group">
<input type="hidden" class="form-control required" id="support_question_id" name="support_question_id" value="<?php echo $id; ?>" > </div>
</div>
<div class="col-md-6">
<div class="form-group">
<input class="form-control required" id="firstName" name="firstName" placeholder="Firstname" required> </div>
</div>
<div class="col-md-6">
<div class="form-group">
<input class="form-control required" id="lastName" name="lastName" placeholder="Lastname" required> </div>
</div>
<div class="col-md-12">
<div class="form-group">
<input class="form-control required" id="email" name="email" placeholder="Email Address" required> </div>
</div>
<div class="col-md-12">
<div class="form-group">
<input class="form-control required" id="phone" name="phone" placeholder="Phone Number" required> </div>
</div>
<div class="col-md-12">
<div class="form-group">
<input class="form-control required" id="city" name="city" placeholder="City" required> </div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<input name="Department" value="Callback" type="hidden" required>
<input class="hidden" name="Contact" placeholder="If you can see this, don't fill it in." type="input" required>
<textarea class="form-control required" id="query" name="query" placeholder="Give us a gist of what you need help with." required></textarea>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<button class="btn" id="inserts" type="button" Onclick = "formSubmit()" name="submit" ">Submit</button>
</div>
</div>
</div>
</form>
</div>
<?php } ?>
</div>

</div>
<?php } ?>

<div class="panel-group help-and-support-set" style="display:none;">
<div class="contact-faqs">
<div class="faqs panel-group">
<h2>Products & Measurements</h2>
<div class="panel-group" id="products-accordion">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#products-accordion" data-target="#collapse1" data-toggle="collapse">How do I design a shirt online?</a></h4> </div>
<div class="panel-collapse collapse in" id="collapse1">
<div class="panel-body">
<p>We predesign each of our fabrics with a unique design. Use our real-time customisation tool to change as few or as many elements as you like, before placing your order. The rest of the shirt elements will be produced just as you see in the image.
</p>
<p>To learn more about the process, and how it works, <a href="#">click here</a>.
</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#products-accordion" data-target="#collapse2" data-toggle="collapse">Do I need a tailor to take my measurements?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse2">
<div class="panel-body">
<p>Of course not. Take a look at our <a href="how-to-measure-shirt-size">Sizing</a> page to learn everything you need to know.
</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#products-accordion" data-target="#collapse3" data-toggle="collapse">Do you have Slim Fit shirts and Regular Fit shirts?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse3">
<div class="panel-body">
<p>Yes, we provide both Slim and Relaxed Fits. Slim Fit shirts are more fitted around the chest, waist, hips, and biceps. Relaxed fit allow for more breathing room around these areas.
</p>
</div>
</div>
</div>
<div class="panel panel-default united-states-hidden">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#products-accordion" data-target="#collapse4" data-toggle="collapse">How does the “Copy a Shirt” concept work?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse4">
<div class="panel-body">
<p>Everyone has a favourite shirt that fits them perfectly. We know this. Send us over this shirt, we’ll copy your measurements exactly (not the styling), and have it sent back to you along with your order. Work on your order will commence only once we’ve receive your shirt. If you've already created a profile with us, we will save the measurements of your sample shirt to your profile.
</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#products-accordion" data-target="#collapse5" data-toggle="collapse">Are the measurements shirt or body measurements? What is the difference?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse5">
<div class="panel-body">
<p>At BSC, the measurements that you provide us are your Final Shirt Measurements, based on which your shirt will be produced.
</p>
<p>If you’ve taken your body measurements, please add a breathing margin to those measurements to arrive at your final shirt measurements. Our suggestions for margin are as follows:
</p>
<p><strong>Chest/ Waist/ Hips</strong> - between 3"-5", depending on how comfortable or fitted you like your shirt
</p>
<p><strong>Biceps</strong> - between 2"-3"
</p>
<p><strong>Cuffs</strong> - 2.5"
</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#products-accordion" data-target="#collapse6" data-toggle="collapse">Where do you source your fabrics and accessories?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse6">
<div class="panel-body">
<p>Our fabrics are sourced from some of the biggest mills in both India and Europe. Our trims, accessories and interlinings are carefully selected to complement the fabrics.
</p>
<p>To learn more about our fabrics and customisations, <a href="how-to-select-a-custom-shirt-fabric">click here</a>.
</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#products-accordion" data-target="#collapse7" data-toggle="collapse">Where do you produce the shirts?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse7">
<div class="panel-body">
<p>Noida Shirt Company shirts are proudly produced at our very own facility in Noida. While each shirt is individually hand-cut, the sewing and fusing is carried out using state of the art machines. Read the About Us page for more details.
</p>
<p><a href="about-us">Click here</a> to read about us, our shirts and where they are produced.
</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#products-accordion" data-target="#collapse8" data-toggle="collapse">Will my shirt shrink?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse8">
<div class="panel-body">
<p>We pre-wash all Noida Shirt Company shirts to avoid shrinking. Make sure to check out and follow the care instructions mentioned on the care label. Specifically, avoid drying using high heat.
</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#products-accordion" data-target="#collapse9" data-toggle="collapse">Where does BSC ship to? What are the charges?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse9">
<div class="panel-body">
<p><span class="united-states-hidden">Regardless of where you are, we will make sure your order reaches you. For Domestic orders, shipping is free. For GCC countries, delivery is free facilitated by our Dubai store. For other countries, we charge a flat fee of $25, regardless of how many shirts you buy.</span> <span class="united-states-visible">Regardless of where you are, we will make sure your order reaches you. We charge a flat fee of $15, regardless of how many shirts you buy.</span> </p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#products-accordion" data-target="#collapse10" data-toggle="collapse">Can I change measurements on a current order?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse10">
<div class="panel-body">
<p>Once an order has been placed, it is very hard for us to change measurements. For us to deliver your order in the shortest possible timeframe, we begin production immediately. Send us an email, and we will see if we can help you with your changes.
</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#products-accordion" data-target="#collapse11" data-toggle="collapse">I’m impatient. How long does it take to make and ship?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse11">
<div class="panel-body">
<p><span class="united-states-hidden">Regardless of where you have ordered from, an indicative time period for your shirt to reach you is 14 days for domestic fabrics and 21 days for international bespoke fabrics from the time your order is confirmed. Of course, this is dependent on our courier partners as well.</span> <span class="united-states-visible">Regardless of where you have ordered from, an indicative time period for your shirt to reach you is 14 days for regular fabrics and 21 days for premium fabrics from the time your order is confirmed. Of course, this is dependent on our courier partners as well.</span> </p>
</div>
</div>
</div>
</div>
<h2>Account Information</h2>
<div class="panel-group" id="account-accordion">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#account-accordion" data-target="#collapse14" data-toggle="collapse">Why should I create an account?</a></h4> </div>
<div class="panel-collapse collapse in" id="collapse14">
<div class="panel-body">
<p>Once you’ve created an account with us, your measurements and addresses are saved in our database. The next time you log in, all you have to do is design your shirt and select your payment option, before checking out. If your measurements change, you can log in anytime and edit them as required. You can also view your order history for past purchases.
</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#account-accordion" data-target="#collapse15" data-toggle="collapse">Do you save credit card information?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse15">
<div class="panel-body">
<p>No, we do not store any credit/debit card information. We are as paranoid about your credit/debit card information as you are. Your payment details are encrypted and sent to the payment gateway for billing. At no point is this information stored by us.
</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#account-accordion" data-target="#collapse16" data-toggle="collapse">Do you save my measurements? How can I access my previous measurements?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse16">
<div class="panel-body">
<p>Yes, if you purchase your shirt online, your last used measurements will be automatically saved to your account. If you purchase a shirt through our stores or Travelling Stylist, we’ll create an account and save your measurements for you. In the case of any alterations, BSC will save the necessary changes to your account.
</p>
<p>If you’ve previously shopped Online, at our Stores or through a Home Visit, your measurements are already saved to your Profile. Either login on the Homepage or during Checkout and select ‘Option 2 MADE-TO-MEASURE/ SAVED SIZES' during checkout to access your saved measurements.
</p>
<p>We realise that you may prefer different fits for different occasions, but we currently only accept one saved measurement.
</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#account-accordion" data-target="#collapse17" data-toggle="collapse">Can I save a design and come back to it later?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse17">
<div class="panel-body">
<p>Unfortunately not. If you click a link outside the customisation process, you'll probably lose your design. So be careful, and make sure you add it to your cart before you get distracted!
</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#account-accordion" data-target="#collapse12" data-toggle="collapse">Can I check my order history?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse12">
<div class="panel-body">
<p>If you’ve shopped online, through our Stores our via our Travelling Stylist, all your orders will be saved to your online profile.
</p>
</div>
</div>
</div>
</div>
<h2>Pricing, Payment Options & Gift Cards</h2>
<div class="panel-group" id="pricing-accordion">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#pricing-accordion" data-target="#collapse18" data-toggle="collapse">What payment options do you offer?</a></h4> </div>
<div class="panel-collapse collapse in" id="collapse18">
<div class="panel-body">
<p><span class="united-states-hidden">You can choose to pay either through our secure net banking facility by Credit or Debit Card or PAYTM wallet.<br><br>
For International customers, PayPal payments are accepted as well.</span> <span class="united-states-visible">You can choose to pay either through our secure net banking facility by Credit or Debit Card. We use the Authorize.net payment gateway to process all payments on the website.</span> </p>
</div>
</div>
</div>
<div class="panel panel-default united-states-hidden">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#pricing-accordion" data-target="#collapse19" data-toggle="collapse">What currency will I see the prices?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse19">
<div class="panel-body">
<p>For all our customers shopping in India, our prices are displayed in Indian Rupees. For international customers, the prices displayed are in AED, US Dollars, GBP and Euro.
</p>
</div>
</div>
</div>
<div class="panel panel-default united-states-hidden">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#pricing-accordion" data-target="#collapse20" data-toggle="collapse">I’m based in India, how come I see the prices in Pounds/Dollars?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse20">
<div class="panel-body">
<p>There could be two reasons for it.
</p>
<p>First, you may be using a proxy server, in which case we are helpless to change that. We can only suggest you access our website from a local internet connection.
</p>
<p>In other cases, try clearing your browser cache, you should see the price in the intended currency.
</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#pricing-accordion" data-target="#collapse21" data-toggle="collapse">Does pricing differ based on the customisation combinations that I choose?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse21">
<div class="panel-body">
<p>No, pricing is based entirely on the fabric that you choose. Everything thereafter is included in the price. We rather you focus on coming up with a great design than stress about what you are paying for it. <span class="united-states-hidden"> There are two occasions when prices may vary:</span> </p>
<ul class="united-states-hidden">
<li>For chest sizes above 50”, there is an additional 10% surcharge on the MRP of the shirt.</li>
<li>Certain trims / accessories like leather and suede patches are chargeable.</li>
</ul>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#pricing-accordion" data-target="#collapse22" data-toggle="collapse">Are there any additional taxes involved?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse22">
<div class="panel-body">
<p><span class="united-states-hidden">Our domestic prices are exclusive of GST taxes. Tax is applicable at checkout. For shipments outside of India, taxes & customs duties, if any, must be borne by the customer.
</span> <span class="united-states-visible">For customers shipping their order to the Tri-State area (NY, NJ, CT), we will charge a Sales and Use Tax totalling 8.875%. For customers shipping to any other state or country, there are no additional taxes levied.</span> </p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#pricing-accordion" data-target="#collapse23" data-toggle="collapse">How do I use my Gift Voucher?</a></h4> </div>
<div class="panel-collapse collapse" id="collapse23">
<div class="panel-body">
<p>When somebody sends you a Gift Voucher, we automatically create an account for you, and save the amount as Loyalty Points to your profile, if you already have an account we will credit the points to it. You can redeem your points Online, at our Stores or through our Travelling Stylists.
</p>
</div>
</div>
</div>
</div>
<h2 class="united-states-hidden">Loyalty Programs</h2>
<div class="panel-group united-states-hidden" id="loyalty-accordion">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#loyalty-accordion" data-target="#collapse24" data-toggle="collapse">How does the Loyalty Program work?</a></h4> </div>
<div class="panel-collapse collapse in" id="collapse24">
<div class="panel-body">
<p>To learn about our Loyalty Program, <strong><a href="loyalty-program">click here</a></strong>.
</p>
</div>
</div>
</div>
</div>
<h2>Returns & Alterations</h2>
<div class="panel-group" id="returns-accordion">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a data-parent="#returns-accordion" data-target="#collapse13" data-toggle="collapse">What is the BSC return policy?</a></h4> </div>
<div class="panel-collapse collapse in" id="collapse13">
<div class="panel-body">
<p>Take a look at our Returns & Alterations Policy <a href="shirt-alteration">here</a>.
</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</section>

<!--footer-->
<?php get_footer(); ?>

<script type="text/javascript">
$(document).ready(function(){


        
$('#insert').click(function(){
//alert("hello");
$("#loaderId").show();
var fn = $("#firstName").val();
var ln = $("#lastName").val();
var mail = $("#email").val();
var mobile = $("#phone").val();
var state = $("#city").val();
var message = $("#query").val();
var supportId = $("#support_question_id").val();

$.ajax(
{
type:'POST',
datatype:'json',
url : 'http://staging.isiwal.com/SunnysBespokeDev/wp-content/themes/noidashirt/support_status.php',
data : {first_name:fn,last_name:ln,email:mail,phone:mobile,city:state,sort_summary:message,support_question_id:supportId},
success : function($data)
{
$("#loaderId").hide();
var obj = $.parseJSON(data);
alert("Information submitted successfully");
}
});

//location.reload();
});
});
</script> 

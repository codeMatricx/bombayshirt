﻿<?php global $wpdb; ?>
<style>
.loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #000;
    z-index: 999999;
    opacity:0.5;
}
.loader img {
    position: absolute;
    top: 48%;
    left: 48%;
}

</style>
<div id = "loaderId" style = "display: none;" class="loader">
     <img  src="<?php echo get_stylesheet_directory_uri().'/loading.gif'; ?>">
   </div>
<footer class="footer" id="tygh_footer">
                    <div class="container-fluid  footer-grid">
                        <div class="row">
                            <section class="col-lg-8 footer-menu hidden-xs hidden-sm">
                                <div class="row">
                                    <section class="col-lg-5 col-sm-12 col-xs-12 col-lg-3">
                                        <div class="footer-section footer-no-wysiwyg pull-left">
                                            <h4 class=""> <span>Shop Online</span> </h4>
                                            <div class="footer-general-body">
                                                <div class="wysiwyg-content">
                                                    <ul class="list-unstyled">
                                                        <li><a href=" http://staging.isiwal.com/SunnysBespokeDev/?page_id=158">Design a Custom Made Shirt</a></li>
                                                        <li><a href="http://staging.isiwal.com/SunnysBespokeDev/?page_id=223">Men's Ready Collection</a></li>
                                                        <li><a href=" http://staging.isiwal.com/SunnysBespokeDev/?page_id=101">Shop Gift Card</a></li>
                                                        <li><a target="_blank" href=" http://staging.isiwal.com/SunnysBespokeDev/?page_id=109">View Premium Catalog</a></li>
                                                    </ul>
                                                    <ul class="list-unstyled primary-links" style=" margin-top: 15px;">
                                                        <li><a href="http://staging.isiwal.com/SunnysBespokeDev/?page_id=68">Book a Home Visit</a></li>
                                                        <li><a href=" http://staging.isiwal.com/SunnysBespokeDev/?page_id=77">Stores</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="col-lg-5 col-sm-12 col-xs-12 col-lg-3">
                                        <div class="footer-section footer-no-wysiwyg pull-left">
                                            <h4 class=""> <span>About</span> </h4>
                                            <div class="footer-general-body">
                                                <div class="wysiwyg-content">
                                                    <ul class="list-unstyled">
                                                        <li><a href="http://staging.isiwal.com/SunnysBespokeDev/?page_id=96">About Us</a></li>
                                                        <li class="united-states-hidden"><a href="http://staging.isiwal.com/SunnysBespokeDev/?page_id=111" style="color:#bb8b7d">Invite &amp; Earn</a></li>
                                                        <li class="united-states-hidden"><a href=" http://staging.isiwal.com/SunnysBespokeDev/?page_id=115">Loyalty Program</a></li>
                                                        <li><a href="http://staging.isiwal.com/SunnysBespokeDev/?page_id=87">Press</a></li>
                                                        <li><a href=" http://staging.isiwal.com/SunnysBespokeDev/?page_id=109">Fabrics</a></li>
                                                        <li><a href="http://staging.isiwal.com/SunnysBespokeDev/?page_id=107">Shirt Construction</a></li>
                                                        <li><a href="http://staging.isiwal.com/SunnysBespokeDev/?page_id=107">Sizing &amp; Fit</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="col-lg-5 col-sm-12 col-xs-12">
                                        <div class="footer-section pull-left">
                                            <h4 class=""> <span>Help</span> </h4>
                                            <div class="footer-general-body">
                                                <div class="wysiwyg-content">
                                                    <ul class="list-unstyled">
                                                        <li><a href="http://staging.isiwal.com/SunnysBespokeDev/?page_id=103">Contact &amp; FAQ's</a></li>
                                                        <li><a href=" http://staging.isiwal.com/SunnysBespokeDev/?page_id=82">Alterations</a></li>
                                                    </ul>
                                                    <div style="margin-top: 70px;">
                                                       <!--  <h4 class="http://staging.isiwal.com/SunnysBespokeDev/?page_id=70 "> <span>Blog: The Post</span> </h4> -->
                                                        <!-- <ul class="list-unstyled">
                                                            <li><a href=" http://staging.isiwal.com/SunnysBespokeDev/?page_id=70">People Places &amp; Experiences that inspire us.</a></li>
                                                        </ul> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </section>
                            <?php
                                // if ( isset( $_POST['submit_subscribe'] ) )
                                // {
                                //  $wpdb->insert( 'wp_users', array(
                                //     'user_email' => $_POST["EMAIL"]),
                                //  array( '%s' ) 
                                //  );
                            
                                // if ( isset( $_POST['submit_subscribe'] ) ){
                                // global $wpdb;
                                // $tablename=$wpdb->prefix.'wp_users';
                                // $data=array(
                                //     'user_email' => $_POST["EMAIL"]
                                // );
                                // $wpdb->insert( $tablename, $data);
                                //  $output="<p>You are successfully subscribed</p>";
                                // }
                                // else
                                // {
                                //   $output="<p>Subscription Failed</p>";
                                // }

                                // if ( isset( $_POST["submit"] ) && $_POST["EMAIL"] != "" ) {
                                // $table = $wpdb->prefix."wp_users";
                                // $email = strip_tags($_POST["EMAIL"], "");
                                // $wpdb->insert( 
                                // $table, 
                                // array( 
                                // 'user_email' => $email
                                // )
                                // );
                                // $html = "<p>You are successfully subscribed</p>";
                                // }
                                // // if the form is submitted but the name is empty
                                // if ( !isset( $_POST["submit"] ) && $_POST["EMAIL"] == "" )
                                // $html .= "<p>You need to fill the required fields.</p>";
                                // // outputs everything
                                // return $html;
                                ?>
                            <section class="col-lg-4 hidden-xs hidden-sm">
                                <div class="newslettter-and-social-media-block">
                                    <div class="wysiwyg-content">
                                        <form class="newsletter" action="" method="POST">
                                            <div class="input-group">
                                                <input id="subscribEmail" type="email" name="email" class="form-control" placeholder="Email Address" required>
                                                 <span class="input-group-btn"> 
                                                 	<button class="btn btn-default" id="sendMin" type="button" >Subscribe</button> 
                                                 </span> 
                                             </div>
                                        </form>
                                        <p>For news &amp; offers subscribe to our newsletter</p>
                                        <ul class="social-links list-unstyled">
                                            <li><a target="_blank" href="#"><i class="fa fa-youtube-play"></i></a></li>
                                            <li><a target="_blank" href="#"><i class="fa fa-instagram"></i></a></li>
                                            <li><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a target="_blank" href="#"><i class="fa fa-facebook"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col-lg-12 copyright-grid">
                                <div class="responsive-footer-block hidden-lg hidden-md">
                                    <div class="wysiwyg-content">
                                        <form class="newsletter" action="#" method="POST">
                                            <div class="input-group">
                                                <input id="subscribeEmail" type="email" name="email" class="form-control" placeholder="Email Address" required> 
                                                <span class="input-group-btn">
                                                 <button class="btn btn-default" id="send" type="button">Subscribe</button> 
                                                </span> 
                                            </div>
                                        </form>
                                        <ul class="social-links list-unstyled">
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-instagram"></i></a>
                                                <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="copyright-and-terms-block">
                                    <div class="wysiwyg-content">
                                        <ul class="clearfix copyright">
                                            <li class="pull-right hidden-sm hidden-xs">Developed by <a class="_blank" href="#">iSiwal</a> </li>
                                            <li class="pull-right hidden-lg hidden-md">
                                                Developed by <a class="_blank" href="#">iSiwal</a> </li>
                                            <li>&copy; 2012 - 2018 SunnyBespoke Shirt Company / <a href="http://staging.isiwal.com/SunnysBespokeDev/?page_id=75">Terms</a> </li>
                                        </ul>
                                        <div class="hidden" id="trial-shirt">
                                            <div class="trial-shirt"> <i class="bsc-shirt"></i>
                                                <h2>Worried about your fit?<br />We'll send a trial shirt!</h2>
                                                <hr>
                                                <p>If you are a first time customer and placing an order for 3 more shirts, we will send you a Trial shirt for your confirmation.</p>
                                                <p>Once you confirm, we’ll proceed with the remainder of your order.</p>
                                                <p><em>* Not applicable on International orders or orders with Cash on Delivery payment.</em></p>
                                            </div>
                                        </div>
                                        <div class="hidden" id="loyalty-points">
                                            <div class="loyalty"> <i class="bsc-loyalty"></i>
                                                <h2>Loyalty Points</h2>
                                                <hr>
                                                <p>Each time you buy a shirt, you will receive loyalty points based on the value of the shirt. Each point is worth One Rupee. Use these points on subsequent orders or save up enough points to earn free shirts.</p>
                                                <p>Points earned through shopping Online, through our Stores or Travelling Stylist, are <strong>all credited to your online Profile</strong>. These points may be redeemed through any of the above channels in the future.</p>
                                                <p>Loyalty points are credited after your shirts have been shipped to you.</p>
                                                <p>To start earning loyalty points or to check your existing balance <a href="login">Sign In</a> / <a href="profiles-add">Register</a> your online profile.</p>
                                            </div>
                                        </div>
                                        <div class="hidden" id="size-chart-desktop"><img alt="Size Chart Desktop" class="lazyload" data-src="design/themes/bsc/media/images/homepage/size-chart-desktop.jpg"></div>
                                        <div class="hidden" id="size-chart-mobile"><img alt="Size Chart Mobile" class="lazyload" data-src="design/themes/bsc/media/images/homepage/size-chart-mobile.jpg"></div>
                                        <div class="hidden" id="shirt-body"><img alt="Shirt and Body" class="lazyload" data-src="design/themes/bsc/media/images/homepage/shirt-and-body.jpg"></div>
                                        <div class="hidden" id="women-collection"><img alt="Women's Collection Popup" class="lazyload" data-src="design/themes/bsc/media/images/homepage/bombayshirts--shop-women--coming-soon.jpg" style="z-index:2000;"></div>
                                        <div class="hidden" id="bsc-wash-care"><img alt="BSC Wash Care" class="lazyload" data-src="design/themes/bsc/media/images/homepage/wash-care.png" style="z-index:2000;"></div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </footer>
            </main>
        </div>
        <script src="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js' ?>" integrity="sha384-aBL3Lzi6c9LNDGvpHkZrrm3ZVsIwohDD7CDozL0pk8FwCrfmV7H9w8j3L7ikEv6h" crossorigin="anonymous"></script>
        <script src="<?php echo get_stylesheet_directory_uri(). '/ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js' ?>" data-no-defer></script>
        <script src="<?php echo get_stylesheet_directory_uri(). '/code.jquery.com/color/jquery.color-2.1.2.min.js' ?>"></script>
        <script src="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js' ?>"></script>
        <script src="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/iScroll/5.1.3/iscroll.min.js' ?>" data-no-defer></script>
        <script src="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/lazysizes/2.0.0/lazysizes.min.js' ?>"></script>
        <script src="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js' ?>"></script>
        <script src="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js' ?>"></script>
        <script src="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/js/jquery.tooltipster.min.js' ?>"></script>
        <script src="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/intro.js/2.3.0/intro.min.js' ?>"></script>
        <script src="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.min.js' ?>"></script>
        <script src="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/jquery.alphanum/1.0.24/jquery.alphanum.min.js' ?>"></script>
        <script src="<?php echo get_stylesheet_directory_uri(). '/cdn.ravenjs.com/3.16.0/raven.min.js' ?>" crossorigin="anonymous"></script>
        <script data-no-defer>
            if (!window.jQuery) {
                document.write('<script type="text/javascript" src="js/lib/jquery/jquery.min0015.js?ver=4.3.6" ><\/script>');
            }
        </script>
        <script data-no-defer>
            if (!window.jQuery.ui) {
                document.write('<script type="text/javascript" src="js/lib/jqueryui/jquery-ui.custom.min0015.js?ver=4.3.6" ><\/script>');
            }
        </script>
        <script src="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.10/js/min/perfect-scrollbar.jquery.min.js' ?>" data-no-defer></script>
        <script src="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js' ?>"></script>
		<script src="<?php echo get_stylesheet_directory_uri(). '/var/cache/misc/assets/js/tygh/scripts-9f97437cfcee50b768df0148138f863e1515504583.js' ?>"></script>
       <script data-no-defer="">
            if (!window.jQuery) {
                document.write('<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(). '/js/lib/jquery/jquery.min.js' ?>" ><\/script>');
            }
        </script>
        <script data-no-defer="">
            if (!window.jQuery.ui) {
                document.write('<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(). '/js/lib/jqueryui/jquery-ui.custom.min.js' ?>" ><\/script>');
            }
        </script>
    </main>
        
        <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri().'/js/tygh/exceptions0015.js?ver=4.3.6' ?>"></script>
        <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri().'/js/tygh/previewers/magnific.previewer0015.js?ver=4.3.6' ?>"></script>
       <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri().'/js/tygh/product_image_gallery0015.js?ver=4.3.6' ?>"></script>

       <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri().'/js/addons/image_zoom/cloudzoom0015.js?ver=4.3.6' ?>"></script>
       
        
        <script type="text/javascript">
            (function(_, $) {

                $.ceEvent('on', 'ce.commoninit', function(context) {

                    var mobileWidth = 767,
                        imageZoomSize = 450;

                    // Disable Cloud zoom on mobile devices
                    if ($(window).width() > mobileWidth) {

                        context.find('.cm-previewer').each(function() {
                            var elm = $(this).find('img'),
                                elm_width = $(this).data('caImageWidth'),
                                elm_height = $(this).data('caImageHeight');
                            if (elm.data('CloudZoom') == undefined) {
                                elm.attr('data-cloudzoom', 'zoomImage: "' + $(this).prop('href') + '"')
                                    .CloudZoom({
                                        tintColor: '#ffffff',
                                        tintOpacity: 0.6,
                                        animationTime: 200,
                                        easeTime: 200,
                                        zoomFlyOut: true,
                                        zoomSizeMode: 'zoom',
                                        captionPosition: 'bottom',
                                        zoomPosition: '3',
                                        autoInside: mobileWidth,
                                        disableOnScreenWidth: mobileWidth,
                                        zoomWidth: elm_width < imageZoomSize ? elm_width : imageZoomSize,
                                        zoomHeight: elm_height < imageZoomSize ? elm_height : imageZoomSize
                                    });
                            }
                        });

                    }
                });

            }(Tygh, Tygh.$));
        </script>

        <script src="<?php echo get_stylesheet_directory_uri().'/code.jquery.com/jquery-migrate-1.2.1.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri().'/dist/scripts/jquery.zoomit.min.js' ?>"></script>

<script type="text/javascript">
(function(code){
    code(document, window, jQuery);
}(function(document, window, $){
    $(function(){
        $('#zoomit-target').zoomIt();
    });
}));
</script>

       
        <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri().'/use.typekit.net/isf1dby.js' ?>"></script>


            <script>
function div_show($pId,$pQuantity,$price) 
{
   var product_id = $pId;
   var product_quantity = $pQuantity;
   var price = $price;
   $.ajax(
        {
          type:'POST',
          datatype:'json',
          url : 'http://staging.isiwal.com/SunnysBespokeDev/wp-content/themes/noidashirt/save_cookies.php',
          data : {productId:product_id,productQuantity:product_quantity,productPrice:price},
            success : function(data)
            {
                var obj = $.parseJSON(data);
                if(obj['0'].code == 200)
                {
                    document.getElementById('popup_form').style.display = "block";
                    productCartCount();
                    allCartProduct();
                }
                    else
                    {
                        alert("Please Check Your Network Connection");
                    }
            }
        });
}

function addToCart($pId,$pQuantity,$price) 
{
   var product_id = $pId;
   var product_quantity = $pQuantity;
   var price = $price;
   $.ajax(
        {
          type:'POST',
          datatype:'json',
          url : 'http://staging.isiwal.com/SunnysBespokeDev/wp-content/themes/noidashirt/save_cookies.php',
          data : {productId:product_id,productQuantity:product_quantity,productPrice:price},
            success : function(data)
            {
                var obj = $.parseJSON(data);
                if(obj['0'].code == 200)
                {
                    //document.getElementById('popup_form').style.display = "block";
                    productCartCount();
                    allCartProduct();
                }
                    else
                    {
                        alert("Please Check Your Network Connection");
                    }
            }
        });
}


function productCartCount()
{
    //alert("call");
    $.ajax(
        {
          type:'POST',
          datatype:'json',
          url : 'http://staging.isiwal.com/SunnysBespokeDev/wp-content/themes/noidashirt/save_cookies.php',
          data : {cart_count:'1'},
            success : function(data)
            {
                var obj = $.parseJSON(data);
                if(obj['0'].code == 200)
                {
                    var cartCount = obj['0'].count;
                    var dynamicHtml = '';
                        dynamicHtml+='<span>'+cartCount+'</span>'
                    $("#cartCount").html(dynamicHtml);
                }
                    else
                    {
                        alert("Please Check Your Network Connection");
                    }
            }
        });
}

function allCartProduct()
{
    $.ajax(
        {
          type:'POST',
          datatype:'json',
          url : 'http://staging.isiwal.com/SunnysBespokeDev/wp-content/themes/noidashirt/save_cookies.php',
          data : {cartProduct:'1'},
            success : function(data)
            {
                var obj = $.parseJSON(data);
                if(obj.length >0)
                {
                    var dynamicHtml = '';
                    for(var i = 0, len = obj.length; i < len; i++)
                    {
                        //console.log(obj[i]);
                        var image = obj[i].image;
                        var pid = obj[i].productId;
                        var prdqnty = obj[i].productQuantaty;
                        var ptitle = obj[i].title;
                        var prdprice = obj[i].price;
                        dynamicHtml+='<li>';
                        dynamicHtml+='<div class="media">';
                        dynamicHtml+='<div class="media-left"> <img class=" media-object" src="'+image+'" data-src="'+image+'" alt="" height="50" width="50" title="'+ptitle+'">';
                        dynamicHtml+='</div>';
                        dynamicHtml+='<div class="media-body">';
                        dynamicHtml+='<a class="media-heading" href="#">'+ptitle+'</a>';
                        dynamicHtml+='<p> <span>'+prdqnty+'</span><span>&nbsp;x&nbsp;</span> <span class="none">₹</span>';
                        dynamicHtml+='<span id="sec_price_1815652499_737" class="none">'+prdprice+'</span> </p></div><div class="media-right cm-cart-item-delete"> <a class="btn cm-ajax cm-ajax-full-render btn-link cm-submit " onclick="removecart('+pid+')" data-ca-dispatch="delete_cart_item" href="#" data-ca-target-id="cart_status*"><i class="glyphicon bsc-cross-none"></i> </a> </div>';
                        dynamicHtml+='</div>';
                        dynamicHtml+='</li><li role="separator" class="divider "></li>';
                    }
                    $("#allcart").html(dynamicHtml);
                    $("#view_cart").show();
                    $("#view_checkout").show();
                }
                        else
                        {
                            var dynamicHtml = '';
                                dynamicHtml+='<div class="buttons text-center text-muted"> <span>Cart is empty</span> </div>';
                            //alert("No Value");
                            $("#cart_empty").html(dynamicHtml); 
                            $("#view_cart").hide();
                            $("#view_checkout").hide();
                        }
            }
        });
}



</script>
<!-- //  document.getElementById('productCookies').value = "$pId";
//  document.getElementById('productCookies').value = "$pPrice";
// console.log($pId);
// console.log($pPrice); -->
<script type="text/javascript">
    function div_hide()
    {
        document.getElementById('popup_form').style.display = "none";
    }
</script>
	<script>
	  $(document).ready(function(){
		$("btn-white").click(function(){
		 alert("hello");
			});
		});
	</script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri().'/js/jquery.min.js' ?>"></script>
	<script>
			$(document).ready(function(){
                $('.help-and-support .buttons .btn').click(function() {
                    if (!$(this).hasClass('active')) {
                        $('.help-and-support .buttons .active').removeClass('active');
                        $(this).addClass('active');
                        $('.help-and-support-set').toggle();
                    }
                });
            });
</script>  
<script type="text/javascript">
   function removecart(id) 
   {
                //alert("call");
        $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'http://staging.isiwal.com/SunnysBespokeDev/wp-content/themes/noidashirt/save_cookies.php',
              data : {removeFrmCart:'1',productId:id},
                success : function(data)
                {
                    var obj = $.parseJSON(data);
                    location.reload();
                    // if(obj['0'].code == 200)
                    // {
                    //     var cartCount = obj['0'].count;
                    //     var dynamicHtml = '';
                    //         dynamicHtml+='<span>'+cartCount+'</span>'
                    //     $("#cartCount").html(dynamicHtml);
                    // }
                    //     else
                    //     {
                    //         alert("Please Check Your Network Connection");
                    //     }
                }
            });

    }
</script>>  
<!-- <?php get_footer(); ?> -->
<!-- Subscription Mail strt here -->
<script type="text/javascript">
    $(document).ready(function(){
        allCartProduct();
        $('#send').click(function(){
         //alert("hello");
         $("#loaderId").show();
         var mail = $("#subscribeEmail").val();
         //console.log(mail);
         $.ajax(
        {
          type:'POST',
          datatype:'json',
          url : 'http://staging.isiwal.com/SunnysBespokeDev/wp-content/themes/noidashirt/subscribe_status.php',
          data : {subEmail:mail},
            success : function($data)
            {
                $("#loaderId").hide();
                var obj = $.parseJSON(data);
                alert("Subscribed successfully");
            }
        });
         
    });
});
</script> 
<!-- For Nobile format -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#sendMin').click(function(){
         //alert("hello");
         $("#loaderId").show();
         var mail = $("#subscribEmail").val();
         //console.log(mail);
         $.ajax(
        {
          type:'POST',
          datatype:'json',
          url : 'http://staging.isiwal.com/SunnysBespokeDev/wp-content/themes/noidashirt/subscribe_status.php',
          data : {subEmail:mail},
            success : function($data)
            {
                $("#loaderId").hide();
                var obj = $.parseJSON(data);
                alert("Subscribed successfully");
            }
        });
         
    });
});
</script> 
 <!-- End Here -->
 <script src="<?php echo get_stylesheet_directory_uri().'/code.jquery.com/jquery-migrate-1.2.1.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri().'/dist/scripts/jquery.zoomit.min.js' ?>"></script>

<script type="text/javascript">
(function(code){
    code(document, window, jQuery);
}(function(document, window, $){
    $(function(){
        $('#zoomit-target').zoomIt();
    });
}));
</script>
<script>
$(document).ready(function(){
    $(".profile-sub-tab-header1").click(function(){
  $(".profile-sub-tab-header1").addClass('active');
  $(".profile-sub-tab-header2").removeClass('active');
  $(".profile-sub-tab-header3").removeClass('active');
        $(".smart-sizing").show();
  $(".standard-sizing").hide();
  $(".send-a-shirt").hide();
    });
    $(".profile-sub-tab-header2").click(function(){
  $(".profile-sub-tab-header2").addClass('active');
  $(".profile-sub-tab-header1").removeClass('active');
  $(".profile-sub-tab-header3").removeClass('active');
        $(".standard-sizing").show();
  $(".smart-sizing").hide();
  $(".send-a-shirt").hide();
    });
 $(".profile-sub-tab-header3").click(function(){
  $(".profile-sub-tab-header3").addClass('active');
  $(".profile-sub-tab-header1").removeClass('active');
  $(".profile-sub-tab-header2").removeClass('active');
        $(".send-a-shirt").show();
  $(".smart-sizing").hide();
  $(".standard-sizing").hide();
    });
 $(".profile-tab-header1").click(function(){
  $(".profile-tab-header1").addClass('active');
  $(".profile-tab-header2").removeClass('active');
  $(".profile-tab-header3").removeClass('active');
  $(".profile-tab-header4").removeClass('active');
        $(".profile-tab1").show();
  $(".profile-tab2").hide();
  $(".profile-tab3").hide();
  $(".profile-tab4").hide();
    });
 $(".profile-tab-header2").click(function(){
  $(".profile-tab-header2").addClass('active');
  $(".profile-tab-header1").removeClass('active');
  $(".profile-tab-header3").removeClass('active');
  $(".profile-tab-header4").removeClass('active');
        $(".profile-tab2").show();
  $(".profile-tab1").hide();
  $(".profile-tab3").hide();
  $(".profile-tab4").hide();
    });
 $(".profile-tab-header3").click(function(){
  $(".profile-tab-header3").addClass('active');
  $(".profile-tab-header1").removeClass('active');
  $(".profile-tab-header2").removeClass('active');
  $(".profile-tab-header4").removeClass('active');
        $(".profile-tab3").show();
  $(".profile-tab1").hide();
  $(".profile-tab2").hide();
  $(".profile-tab4").hide();
    });
 $(".profile-tab-header4").click(function(){
  $(".profile-tab-header4").addClass('active');
  $(".profile-tab-header1").removeClass('active');
  $(".profile-tab-header2").removeClass('active');
  $(".profile-tab-header3").removeClass('active');
        $(".profile-tab4").show();
  $(".profile-tab1").hide();
  $(".profile-tab2").hide();
  $(".profile-tab3").hide();
    });	
	$(".drawer-toggle").click(function(){
		$(".dispatch-index-index, .drawer").toggleClass('drawer-open');
		//$(".drawer").toggleClass('drawer-open');
    });
});
</script>
<script type="text/javascript">
				$(function () {
					$("#gender1").change(function () {
						if ($(this).val() == "0") {
							//alert("test");
							//console.log('test');
							$('select[name="fit"]').prop("disabled", true);
							$('select[name="standard_size"]').prop("disabled", true);
							$('#women').hide();
							$('#standard_size').show();
						}
						if ($(this).val() == "1") {
							//alert("test");
							//console.log('test');
							$('select[name="fit"]').removeAttr('disabled');
							$('select[name="standard_size"]').removeAttr('disabled');
							$('#women').hide();
							$('#standard_size').show();
						}
						if ($(this).val() == "2") {
							$('#women').show();
							$('#standard_size').hide();
							$('select[name="fit"]').prop("disabled", true);
							$('select[name="standard_size"]').prop("disabled", true);
							$('select[name="women"]').removeAttr('disabled');
						}
					});
				});
				
			</script>
<!-- Transaction Card start here -->
<script type="text/javascript">
    function cartShow()
    {
        document.getElementById('showBillingForm').style.display = "block";
    }
</script>
<script type="text/javascript">
function saveCartData(uEmail,bPhone,proName,postPrice,fName,lName,addr,city,country,state,zip)
{
        //console.log("call");
        $("#loaderId").show();
        var buyer_email = uEmail;
        var buyer_phone = bPhone;
        var pro_name = proName;
        var pro_price = postPrice;
        var buyer_fName = fName;
        var buyer_lName = lName;
        var buyer_address = addr;
        var buyer_city = city;
        var buyer_country = country;
        var buyer_state = state;
        var buyer_postal_code = zip;
        var user_name = $("#cardHolderName").val();
        //  var card_number = $("#cardNumber").val();
        //  var expiry_month = $("#expiryMonth").val();
        //  var expiry_year = $("#expiryYear").val();
        //  var cvv_number = $("#cvv").val();
         
         $.ajax(
        {
          type:'POST',
          datatype:'json',
          url : 'http://staging.isiwal.com/SunnysBespokeDev/wp-content/themes/noidashirt/pay.php',
          data : {buyerEmail:buyer_email,buyerPhone:buyer_phone,productName:pro_name,productPrice:pro_price,buyerFName:buyer_fName,buyerLName:buyer_lName,buyerAddress:buyer_address,buyerCity:buyer_city,buyerCountry:buyer_country,buyerState:buyer_state,zipCode:buyer_postal_code},
            success : function(data)
            {
                $("#loaderId").hide();
                var obj = $.parseJSON(data);
                window.location.href = obj.longurl;
                //console.log(obj.longurl);
            }
        
        });
     }
</script>
<!-- End here -->
</body>
</html>
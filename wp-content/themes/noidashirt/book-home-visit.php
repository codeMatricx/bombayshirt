<?php /* Template Name: Book Home Visit */ ?>
<?php
get_header(); ?>

<div id = "loaderId" style = "display: none;" class="loader">
<img  src="<?php echo get_stylesheet_directory_uri().'/loading.gif'; ?>">
</div>
		<section class="dispatch-pages-view book-home-visit-page content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="main-content-block">
                                    <div class="wysiwyg-content">
                                        <div>
                                            <div class="travelling-stylist">
                                                <div class="row">
                                                    <div class="col-md-12 text-center">
                                                        <h1 class="heading">Book a Home Visit</h1>
                                                        <hr class="heading-separator"> <span style="color: #336799;display:none;">Available in Delhi and Bangalore.</span> </div>
                                                </div>
                                                <div class="row form-split">
                                                    <div class="col-md-10 col-md-offset-1">
                                                        <div class="ty-form-builder">
                                                            <form accept-charset="UTF-8" action="" id="forms_form" method="post" name="forms_form">
                                                                <input name="fake" type="hidden" value="1">
                                                                <input name="appointment[redirect_to]" type="hidden" value="indexa688.php?submitted=yes">
                                                                <input name="page_id" type="hidden" value="22">
                                                                <input name="appointment[contact_via_phone]" type="hidden" value="0">
                                                                <div class="row two-split">
                                                                    <div class="col-md-6 text-center background">
                                                                        <div class="row">
                                                                            <?php
                                                                            if (have_posts()):
                                                                            while (have_posts()) : the_post();
                                                                            the_content();
                                                                            endwhile;
                                                                            else:
                                                                            echo '<p>Sorry, no posts matched your criteria.</p>';
                                                                            endif;
                                                                            ?>
                                                                          
                                                                        </div>
                                                                    </div>
    <div class="col-md-6">
        <div class="row" style="border-style: solid;border-color: lightgray;">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="ty-control-group">
                    <label class="ty-control-group__title cm-required" for="elm_10">City</label>
                    <select id= "city" name="city">
                        <option value="Delhi">Delhi</option>
                        <option value="Bangalore">Bangalore</option>
                        <option value="Mumbai">Mumbai</option>
                        <option value="Kolkata">Kolkata</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="ty-control-group">
                    <label class="ty-control-group__title cm-required" for="elm_10">First Name</label>
                    <input id="firstName" class="ty-form-builder__input-text ty-input-text" name="firstName" placeholder="Your Firstname" size="50" type="text" required> 
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="ty-control-group">
                    <label class="ty-control-group__title cm-required" for="elm_13">Last Name</label>
                    <input id="lastName" class="ty-form-builder__input-text ty-input-text" name="lastName" placeholder="Your Lastname" size="50" type="text" required> 
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="ty-control-group">
                    <label class="ty-control-group__title cm-required cm-phone" for="elm_12">Mobile</label>
                    <input id="phone" class="ty-input-text" name="phone" placeholder="Your Phone Number" size="50" type="text" required>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="ty-control-group">
                    <label class="ty-control-group__title cm-required cm-email" for="elm_11">Email</label>
                    <!-- <input name="customer_email" type="hidden" value="11"> -->
                    <input class="ty-input-text" id="email" name="email" placeholder="Your Email Address" size="50" type="text" required> 
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="ty-control-group">
                    <label class="ty-control-group__title cm-required" for="elm_20">Date / Time</label>
                    <input class="ty-form-builder__input-text ty-input-text" id="appointmentDate" name="appointmentDate" placeholder="Date/Time" size="50" type="text" required> 
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="ty-form-builder__buttons buttons-container">
                    <button class="btn ty-btn__secondary ty-btn" id="appointId" name="commit" type="button" onclick="appointHomeVisit()" >Book&nbsp;&nbsp;Appointment</button>
                </div>
                
            </div>
        </div>
    </div>                                                        </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
		
<script type="text/javascript">
function appointHomeVisit()
{
        $("#loaderId").show();
         var userCity = $("#city").val();
         var fName = $("#firstName").val();
         var lName = $("#lastName").val();
         var userPhone = $("#phone").val();
         var mail = $("#email").val();
         var apponitDate = $("#appointmentDate").val();
         //console.log(mail);
         $.ajax(
        {
          type:'POST',
          datatype:'json',
          url : 'http://staging.isiwal.com/SunnysBespokeDev/wp-content/themes/noidashirt/home_visit_status.php',
          data : {city:userCity,firstName:fName,lastName:lName,phone:userPhone,mail:mail,appointmentDate:apponitDate},
            success : function($data)
            {
                $("#loaderId").hide();
                var obj = $.parseJSON(data);
                alert("Appointment booked successfully");
             }
        
        });
     }
         
</script>
<!--footer-->
   <?php get_footer(); ?>		
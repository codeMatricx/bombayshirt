﻿<?php /* Template Name: Shop men shirts online */ ?>
<?php get_header(); 
global $wpdb;

$url=$_SERVER['REQUEST_URI'];

$newUrl=explode("=",$url);
$sub_cat_d = $newUrl['1'];
//print_r($id);exit;
?>

<section class="dispatch-categories-view drawer content">
<div class="container-fluid  content-grid">
<div class="row">
    <section class="col-lg-12 main-content-grid col-md-12 col-sm-12 col-xs-12">
        <aside class="sidebox filters-block clearfix">
            <h5 class="text-uppercase "> 
                <strong>
				       Product filters
				</strong>
            </h5>
            <div class="sidebox-body">
                <div class="panel panel-default horizontal-product-filters cm-product-filters cm-horizontal-filters" data-ca-target-id="product_filters_*,products_search_*,category_products_*,product_features_*,breadcrumbs_*,currencies_*,languages_*,selected_filters_*"  id="product_filters_39">
                    <div class="panel-body">
                        <ul class="list-inline">
                            <?php 
                                                    $taxonomy     = 'product_cat';
                                                    $orderby      = 'name';  
                                                    $show_count   = 0;      // 1 for yes, 0 for no
                                                    $pad_counts   = 0;      // 1 for yes, 0 for no
                                                    $hierarchical = 1;      // 1 for yes, 0 for no  
                                                    $title        = '';  
                                                    $empty        = 0;

                                                    $args = array(
                                                    'taxonomy'     => $taxonomy,
                                                    'orderby'      => $orderby,
                                                    'show_count'   => $show_count,
                                                    'pad_counts'   => $pad_counts,
                                                    'hierarchical' => $hierarchical,
                                                    'title_li'     => $title,
                                                    'hide_empty'   => $empty
                                                    );
                                                    $all_categories = get_categories( $args );

                                                    foreach ($all_categories as $cat) 
                                                    {
                                                        if($cat->category_parent == 0) 
                                                        {
                                                            $category_id = $cat->term_id;
                                                            $category_name = $cat->name;
                                                ?>
                            <li>
                                <div class="dropdown">
                                    <div class="btn-group">
                                        <div class="btn-group">
                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdown_filter_39" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <?php echo $category_name;?> <span class="caret"></span> </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdown_filter_39">
                                                <div class="form">
                                                    <ul class="product-filter list-unstyled " id="content_39_1">
                                                        <?php 
                                                         $args2 = array(
                                                            'taxonomy'     => $taxonomy,
                                                            'child_of'     => 0,
                                                            'parent'       => $category_id,
                                                            'orderby'      => $orderby,
                                                            'show_count'   => $show_count,
                                                            'pad_counts'   => $pad_counts,
                                                            'hierarchical' => $hierarchical,
                                                            'title_li'     => $title,
                                                            'hide_empty'   => $empty
                                                            );
                                                            $sub_cats = get_categories( $args2 );
                                                              foreach($sub_cats as $sub_category) 
                                                              {
                                                                $pSliug = $sub_category->slug;
                                                                $sName=$sub_category->name;
                                                                $subcat_id = $sub_category->term_id;
                                                                 // get_term_link($sName,'product_cat'); 
                                                        ?>
                                                        <li class="product-filters-item-more">
                                                            <ul id="ranges_39_1" style="max-height: 20em;" class="product-filter-variants list-unstyled cm-filter-table" data-ca-input-id="elm_search_39_1" data-ca-clear-id="elm_search_clear_39_1" data-ca-empty-id="elm_search_empty_39_1">
                                                                <li class="checkbox">
                                                                    <label>
                                                                        <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[1]" data-ca-filter-id="1" value="4" id="elm_checkbox_39_1_4">
                                                                        <a href="http://staging.isiwal.com/SunnysBespokeDev/shop-online/shop-men-shirts-online/?<?php echo $category_name;?>=<?php echo $subcat_id;?>"><?php echo $sName;?></a>
                                                                    </label>
                                                                </li>
                                                            </ul>
                                                            <p id="elm_search_empty_39_1" class="product-filters-no-items-found hidden">No items found matching the search criteria</p>
                                                        </li>
                                                        <?php } ?>
                                                    </ul>
                                                    <div class="product-filters-tools clearfix"> <a class="btn cm-external-click btn-default  " data-ca-external-click-id="sw_elm_filter_1"> Close</a> </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </li>
                            <?php } } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>
        <div class="main-content-block">
            <div id="category_products_11">
                <div class="pagination-container cm-pagination-container" id="pagination_contents">
                    <div>
                        <a data-ca-scroll=".cm-pagination-container" href="#" data-ca-page="" data-ca-target-id="pagination_contents" class="hidden"></a>
                    </div>
                    <div class="panel panel-default sorting">
                        <div class="panel-body">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Newest Items First <span class="caret"></span> </button>
                                <ul class="dropdown-menu">
                                    <li class="sort-by-price-asc"> <a class="cm-ajax" data-ca-target-id="pagination_contents" href="#" rel="nofollow">Sort by Price: Low to High</a> </li>
                                    <li class="sort-by-price-desc"> <a class="cm-ajax" data-ca-target-id="pagination_contents" href="#" rel="nofollow">Sort by Price: High to Low</a> </li>
                                    <li class="sort-by-popularity-desc"> <a class="cm-ajax" data-ca-target-id="pagination_contents" href="#" rel="nofollow">Sort by Popularity</a> </li>
                                    <li class="sort-by-bestsellers-desc"> <a class="cm-ajax" data-ca-target-id="pagination_contents" href="#" rel="nofollow">Sort by Bestselling</a> </li>
                                </ul>
                            </div>
                            <!-- <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    36 Per Page <span class="caret"></span> </button>
                                <ul class="dropdown-menu">
                                    <li> <a class="cm-ajax" href="#" data-ca-target-id="pagination_contents" rel="nofollow">12 Per Page</a> </li>
                                    <li> <a class="cm-ajax" href="#" data-ca-target-id="pagination_contents" rel="nofollow">24 Per Page</a> </li>
                                    <li> <a class="cm-ajax" href="#" data-ca-target-id="pagination_contents" rel="nofollow">48 Per Page</a> </li>
                                    <li> <a class="cm-ajax" href="#" data-ca-target-id="pagination_contents" rel="nofollow">96 Per Page</a> </li>
                                </ul>
                            </div> -->
                        </div>
                    </div>
                    <?php
                 if(!empty($sub_cat_d))
                 {
                                $option = '<a href="/category/archives/'.$cat->category_nicename.'">';
                                $option .= $cat->$sub_cat_d;//parents sub category name
                                $option .= '</a>';
                                echo $option;
                                query_posts($sub_cat_d);
                                if (have_posts()) : while (have_posts()) : the_post(); ?>      
                                <h3><?php the_title(); ?></h3>
                                <?php the_content();?>
                                <?php endwhile; else: ?>
                                <?php _e('No Posts Sorry.'); ?>
                                <?php endif;  
                 }
                 else
                 {
                        ?>
                         <div class="grid-list">
                        <div class="row">
                            <?php 
                                $product = $wpdb->get_results("SELECT p.id,p.guid,mt.meta_value AS sku,m.meta_value AS stock,p.post_title AS title,p.post_content AS description,pt.meta_value AS price,wt.meta_value AS weight FROM wp_posts p INNER JOIN wp_postmeta m ON (p.id = m.post_id)INNER JOIN wp_postmeta mt ON (p.id = mt.post_id)INNER JOIN wp_postmeta pt ON (p.id = pt.post_id)INNER JOIN wp_postmeta wt ON ( p.id = wt.post_id ) WHERE p.post_type = 'product'AND m.meta_key = '_stock'AND mt.meta_key = '_sku' AND pt.meta_key = '_price' AND wt.meta_key = '_weight'");
                                //print_r($product_image);exit;
                                foreach($product as $row)
                                {  
                                $product_name = $row->post_name;
                                $id = $row->id;
                                $price = $row->price;
                                $post_title = $row->title;
                                ?>
                            <div class="col-sm-4 col-lg-4 col-md-4">
                                <div class="thumbnail grid-thumbnail">
                                
                                    <div class="new-overlay discount-overlay">
                                        <span>NEW</span>
                                    </div>
                                    <form action="#" method="post" name="product_form_7856" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle ">
                                        <input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*" />
                                        <input type="hidden" name="redirect_url" value="#" />
                                        <input type="hidden" name="product_data[7856][product_id]" value="7856" />
                                        <input type="hidden" class="product-sku-code" value="RM-PT060" />
                                        <div class="grid-list-rating"> </div>
                                        <div class="grid-list-image">
                                        <?php 
                                        $product_image = $wpdb->get_row("SELECT guid FROM wp_posts WHERE (post_parent='$id')");
                                        $product_image = $product_image->guid;
                                        ?>
                                            <a href=" http://staging.isiwal.com/SunnysBespokeDev/shirt?productId=<?php echo $id;?>"> <img class="   " id="det_img_7856" src="<?php echo $product_image; ?>"  alt="" title="" /> </a>
                                            <div class="grid-hover hidden-xs hidden-sm"><a href="http://staging.isiwal.com/SunnysBespokeDev/shirt?productId=<?php echo $id;?>" class="vertical-align"><span class=""><span class="product-title"><?php echo $post_title; ?></span><div class="grid-list-price"> <span class="cm-reload-7856" id="old_price_update_7856"> </span> <span class="cm-reload-7856 price-update" id="price_update_7856"> <input type="hidden" name="appearance[show_price_values]" value="1" /> <input type="hidden" name="appearance[show_price]" value="1" /> <span class="price" id="line_discounted_price_7856"><span class="price-num">₹</span><span id="sec_discounted_price_7856" class="price-num"><?php echo $price; ?></span></span> </span> </div></span></a></div>
                                        </div>
                                        <div class="caption">
                                            <div class="hidden-lg hidden-md title-and-price clearfix">
                                                <div class="grid-list-price"> <span class="cm-reload-7856" id="old_price_update_7856"> </span> <span class="cm-reload-7856 price-update" id="price_update_7856"> <input type="hidden" name="appearance[show_price_values]" value="1" /> <input type="hidden" name="appearance[show_price]" value="1" /> <span class="price" id="line_discounted_price_7856"><span class="price-num">₹</span><span id="sec_discounted_price_7856" class="price-num"><?php echo $price; ?></span></span>
                                                    </span>
                                                </div> <a href=" http://staging.isiwal.com/SunnysBespokeDev/shirt?productId=<?php echo $id;?>" class="product-title"><?php echo $post_title; ?></a> </div>
                                            <div class="actions grid-list-actions">
                                                <!-- <a href="#" class="fa fa-angle-left gallery-nav prev"></a> -->
                                                <div class="cm-reload-7856  add-to-cart-container" id="add_to_cart_update_7856">
                                                    <input type="hidden" name="appearance[show_add_to_cart]" value="1" />
                                                    <input type="hidden" name="appearance[separate_buttons]" value="" />
                                                    <input type="hidden" name="appearance[show_list_buttons]" value="" />
                                                    <input type="hidden" name="appearance[but_role]" value="action" />
                                                    <input type="hidden" name="appearance[quick_view]" value="" /> <span id="cart_add_block_7856"> <button id="button_cart_7856" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="button" name="dispatch[checkout.add..7856]" onclick="addToCart('<?php echo $id; ?>','1','<?php echo $price;?>')">Add to cart</button> </span> </div>
                                                <!-- <a href="#" class="fa fa-angle-right gallery-nav next"></a> -->
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <?php 
                              }
                            ?>
                            </div>
                            
                        </div>

                    <?php
                    } 
                    ?>
                    
                    </div>
                    <!-- <div class="pagination-bottom text-center">
                        <nav>
                            <ul class="pagination">
                                <li class="disabled"> <a data-ca-scroll=".cm-pagination-container" class=""><span>Prev</span> </a> </li>
                                <li class="active"><a>1</a></li>
                                <li><a data-ca-scroll=".cm-pagination-container" href="#" data-ca-page="2" class="cm-history cm-ajax" data-ca-target-id="pagination_contents">2</a></li>
                                <li><a data-ca-scroll=".cm-pagination-container" href="#" data-ca-page="3" class="cm-history cm-ajax" data-ca-target-id="pagination_contents">3</a></li>
                                <li> <a data-ca-scroll=".cm-pagination-container" class=" cm-history cm-ajax" href="#" data-ca-page="2" data-ca-target-id="pagination_contents"><span>Next</span></a> </li>
                            </ul>
                        </nav>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
</div>
</div>
</section>



<!--footer-->
<?php get_footer(); ?>		
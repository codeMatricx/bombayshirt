﻿<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head><title><?php bloginfo('name');?></title>
    <base />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" data-ca-mode="full" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="Design your own custom made shirts from a wide range of finest shirt fabrics. You can also buy tailored shirts from our ready design collection. World wide Shipping available." />
    <meta name="keywords" content="custom made shirts, tailored shirts, custom shirts, bespoke shirts, tailor made shirts, men&#039;s custom shirt online, custom tailored shirts, made to measure shirts online, custom dress shirts online" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/font-awesome/4.5.0/css/font-awesome.min.css'?>">
    <link href='https://fonts.googleapis.com/css?family=Karla:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.10/css/perfect-scrollbar.min.css'?>" rel='stylesheet' type='text/css'>
    <link href="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css'?>" rel="stylesheet" type="text/css">
    <link href="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css'?>" rel='stylesheet' type='text/css'>
    <link href="<?php echo get_stylesheet_directory_uri(). '/cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/tooltipster.min.css'?>" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/style.css' ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/dist/styles/zoomit.css' ?>" />
    <script src="<?php echo get_stylesheet_directory_uri(). '/js/jquery-1.11.3.min.js' ?>" type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(). '/js/jssor.slider-26.9.0.min.js' ?>" type="text/javascript"></script>
	<script type="text/javascript">
        jQuery(document).ready(function ($) {

            var jssor_1_SlideoTransitions = [
              [{b:-1,d:1,o:-0.7}],
              [{b:900,d:2000,x:-379,e:{x:7}}],
              [{b:900,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,sX:2,sY:2},{b:0,d:900,x:-171,y:-341,o:1,sX:-2,sY:-2,e:{x:3,y:3,sX:3,sY:3}},{b:900,d:1600,x:-283,o:-1,e:{x:16}}]
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $Cols: 1,
              $Align: 0,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 3000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        });
    </script>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />
    <style>
        /* jssor slider loading skin spin css */
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }


        .jssorb032 {position:absolute;}
        .jssorb032 .i {position:absolute;cursor:pointer;}
        .jssorb032 .i .b {fill:#fff;fill-opacity:0.7;stroke:#000;stroke-width:1200;stroke-miterlimit:10;stroke-opacity:0.25;}
        .jssorb032 .i:hover .b {fill:#000;fill-opacity:.6;stroke:#fff;stroke-opacity:.35;}
        .jssorb032 .iav .b {fill:#000;fill-opacity:1;stroke:#fff;stroke-opacity:.35;}
        .jssorb032 .i.idn {opacity:.3;}

        .jssora051 {display:block;position:absolute;cursor:pointer;}
        .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
        .jssora051:hover {opacity:.8;}
        .jssora051.jssora051dn {opacity:.5;}
        .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
		.dropbtn {
			cursor: pointer;
		}

    </style>
	</head>
<body class="dispatch-index-index drawer drawer--left ">
    <header class="drawer" role="banner">
        <nav class="drawer-nav" role="navigation">
            <div>
                <?php
                    wp_nav_menu( array( 
                    'theme_location' => 'header_navigation', 
                    'container_class' => 'custom-menu-class' ) ); 
                ?>
            </div>
        </nav>
    </header>
    <main role="main">
        <input class="local-coefficient" type="hidden" value="1.00000" />
        <input class="local-symbol" type="hidden" value="₹" />
        <div class="page" id="tygh_container">
            <div id="ajax_overlay" class="ajax-overlay"></div>
            <div id="ajax_loading_box" class="ajax-loading-box"></div>
            <div class="cm-notification-container notification-container"> </div>
            <main class="page-container" id="tygh_main_container" role="main">
                <section class="top-panel">
                    <div class="container-fluid  ">
                        <div class="row">
                            <section class="col-lg-3 top-links-grid col-xs-8 col-sm-6 col-md-3">
                                <div class="logo-block pull-left">
                                    <a href="index.php" title="" class="logo hidden-xs hidden-sm"> 
									<img src="<?php echo get_theme_file_uri('design/themes/bsc/media/images/n-logo.jpg'); ?>" alt="Bombay Shirt Company" class="full" /> 
									<img src="<?php echo get_theme_file_uri('design/themes/bsc/media/images/n-logo.jpg'); ?>" alt="Bombay Shirt Company" class="horizontal" /> 
									</a>
                                    <button type="button" class="drawer-toggle drawer-hamburger hidden-md hidden-lg"> <span class="sr-only">toggle navigation</span> <span class="drawer-hamburger-icon"></span> </button>
                                    <a href="index.php" title="" class="logo-horizontal hidden-md hidden-lg"> <img src="<?php echo get_theme_file_uri('design/themes/bsc/media/images/n-logo.jpg'); ?> " alt="Bombay Shirt Company" /> </a>
                                </div>
                            </section>
                            <section class="col-lg-9 col-xs-4 col-sm-6 col-md-9">
                                <div class="row">
                                    <section class="col-lg-8 main-menu-grid">
                                        <div class="top-menu-block pull-left">
                                            <div class="top-menu hidden-xs hidden-sm">
                                        <?php
                                            wp_nav_menu( array( 
                                                        'theme_location' => 'header_navigation', 
                                                        'container_class' => 'custom-menu-class' ) ); 
                                        ?>
											<?php 
                                            //wp_nav_menu( array( 'container_class' => 'top-menu-nav', 'theme_location' => 'primary' ) ); 
                                            //print_r($data);exit;
                                            ?>
                                            </div>
                                        </div>
                                    </section>
									<section class="col-lg-4 cart-and-account-grid">
                                        <div class="cart-content-block top-cart-content pull-right">
                                        <div class="dropdown cart-dropdown" id="cart_status_737"> <button class="btn btn-default dropdown-toggle" type="button" id="cart_dropdown_737" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="quantity" id = "cartCount">
                                            <?php 
                                                echo count($_SESSION['cart_item']);
                                            ?>
                                           
                                        </i> <i class="glyphicon glyphicon-shopping-cart fa fa-shopping-cart filled"></i> </button>
                                        <ul aria-labelledby="cart_dropdown_737" class="cm-cart-content dropdown-menu dropdown-menu-right cm-cart-content-thumb cm-cart-content-delete">
                                            <span id = "allcart"></span>
                                            <!-- <li>
                                            <div class="media">
                                            <div class="media-left"> <img class=" media-object  " src="https://www.bombayshirts.com/images/thumbnails/40/40/detailed/29/BB_001_A.jpg" data-src="https://www.bombayshirts.com/images/detailed/29/BB_001_A.jpg" alt="" title=""> </div>
                                            <div class="media-body">
                                            <a class="media-heading" href="#">
                                            White Poplin
                                            </a>
                                            <p> <span>1</span><span>&nbsp;x&nbsp;</span> <span class="none">₹</span><span id="sec_price_1815652499_737" class="none">1,960</span> </p>
                                            </div>
                                            <div class="media-right cm-cart-item-delete"> <a class="btn cm-ajax cm-ajax-full-render btn-link cm-submit " data-ca-dispatch="delete_cart_item" href="#"
                                            data-ca-target-id="cart_status*"><i class="glyphicon bsc-cross-none"></i> </a> </div>
                                            </div>
                                            </li> -->
                                        <!-- <li role="separator" class="divider "></li> -->
                                        <!-- <li>
                                        <div class="media">
                                        <div class="media-left"> <img class=" media-object  " src="https://www.bombayshirts.com/images/thumbnails/40/40/detailed/40/H59ByFu.jpg" data-src="https://www.bombayshirts.com/images/detailed/40/H59ByFu.jpg" alt="" title=""> </div>
                                        <div class="media-body">
                                        <a class="media-heading" href="#">
                                        White Satin
                                        </a>
                                        <p> <span>2</span><span>&nbsp;x&nbsp;</span> <span class="none">₹</span><span id="sec_price_2525060006_737" class="none">2,090</span> </p>
                                        </div>
                                        <div class="media-right cm-cart-item-delete"> <a class="btn cm-ajax cm-ajax-full-render btn-link cm-submit " data-ca-dispatch="delete_cart_item" href="#"
                                        data-ca-target-id="cart_status*"><i class="glyphicon bsc-cross-none"></i> </a> </div>
                                        </div>
                                        </li> -->
                                        <!-- <li role="separator" class="divider hide"></li> -->
                                        <li id="cart_empty">
                                            <!-- <div class="buttons text-center text-muted"> <span>Cart is empty</span> </div> -->
                                        </li>
                                        <li class="cm-cart-buttons">
                                        <div class="buttons clearfix"> <a href="http://staging.isiwal.com/SunnysBespokeDev/cart" rel="nofollow" class="btn btn-default hidden-xs hidden-sm" id="view_cart">View cart</a> <a href="http://staging.isiwal.com/SunnysBespokeDev/checkout" rel="nofollow" class="btn btn-default pull-right" id="view_checkout">Sizing &amp; Checkout</a>                    </div>
                                        </li>
                                        
                                        </ul>
                                        </div>
                                        </div>
                                        <div class="sizing-and-checkout-tip-block pull-right">
                                            <div class="wysiwyg-content">
                                                <div><a class="checkout-tip" href="cart/index.php">Add Measurements at Checkout <i class="fa fa-angle-right" aria-hidden="true"></i></a> </div>
                                            </div>
                                        </div>
                                        <div class="my-account-block top-my-account pull-right">
                                           <?php
                                           $user_id = get_current_user_id();
                                           //print_r($user_id);exit;
                                            if(!empty($user_id))
                                            {
                                                    $userProfile = $wpdb->get_row("SELECT display_name FROM wp_users WHERE ID='$user_id'");
                                                    $profile = $userProfile->display_name;
                                                ?>
                                                <div class="dropdown account-dropdown"> <button class="btn btn-default dropdown-toggle" type="button" id="my_account_738" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="glyphicon glyphicon-user fa fa-user"></i> </button>
												    <ul class="dropdown-menu dropdown-menu-right"
												        aria-labelledby="my_account_738" id="account_info_738">
												        <li class="dropdown-header header-name">Hi, <?php echo $profile; ?></li>
												        <div class="user-information"> <input type="hidden" class="profile-id" value="119868"> <input type="hidden" class="profile-name" value="Ratan Pandey"> <input type="hidden" class="profile-firstname" value="Ratan"> <input type="hidden" class="profile-lastname" value="Pandey">            <input type="hidden" class="profile-email" value="ratan7139@gmail.com"> <input type="hidden" class="profile-points" value="250"> <input type="hidden" class="profile-timestamp" value="1517567978"> </div>
												        <li role="separator" class="divider"></li>
                                                        
												        <li><a href="http://staging.isiwal.com/SunnysBespokeDev/profile?userId=<?php echo $user_id ;?>" rel="nofollow">Profile details</a></li>
                                                        <!--
												        <li><a href="#" rel="nofollow">Order Tracking</a></li>
												        <li><a href="#" rel="nofollow">Orders</a></li>
												        <li class="ty-account-info__item ty-dropdown-box__item"><a class="ty-account-info__a" href="#" rel="nofollow">My points&nbsp;<span class="ty-reward-points-count">(₹250.00)</span></a></li>-->
												        <li>
												            <div class="buttons"> <a href="<?php echo wp_logout_url(); ?>" rel="nofollow" class="btn btn-block sign-out">Sign out</a> </div>
												        </li>
												    </ul>
												</div>
                                                <?php
                                            }
                                            else
                                            {
                                            ?>
                                             <div class="dropdown account-dropdown">
                                                <button class="btn btn-default dropdown-toggle account-tooltip-trigger" type="button" id="my_account_738" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="glyphicon glyphicon-user fa fa-user hidden-md hidden-lg"></i><span class="hidden-xs hidden-sm">Sign in / Register</span> </button>
                                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="my_account_738" id="account_info_738">
                                                    <li>
                                                        <div class="buttons"> <a href=" http://staging.isiwal.com/SunnysBespokeDev/?page_id=113" data-ca-target-id="login_block738" class="cm-dialog-opener btn-borderless cm-dialog-auto-size btn btn-default btn-sm" rel="nofollow">Sign in</a> <a href=" http://staging.isiwal.com/SunnysBespokeDev/?page_id=84" rel="nofollow" class="btn btn-sm btn-borderless">Register</a>
                                                            <div id="login_block738" class="hidden" title="Sign in">
                                                                <div>
                                                                    <form name="popup738_form" action="#" method="post" class="login-form">
                                                                        <input type="hidden" name="return_url" value="index.php" />
                                                                        <input type="hidden" name="redirect_url" value="index.php" />
                                                                        <div class="form-group">
                                                                            <label for="login_popup738" class="cm-required cm-trim cm-email control-label">Email</label>
                                                                            <input type="email" id="login_popup738" name="user_login" size="30" value="" class="cm-focus form-control" placeholder="Email" /> </div>
                                                                        <div class="form-group last clearfix">
                                                                            <label for="psw_popup738" class="cm-required control-label">Password</label>
                                                                            <input type="password" id="psw_popup738" name="password" size="30" value="" class="form-control" maxlength="32" placeholder="Password" /> <a href="indexf69f.php?dispatch=auth.recover_password" class="forgot-password radio pull-left" tabindex="5">Forgot your password?</a>
                                                                            <div class="checkbox pull-right remember-me">
                                                                                <label for="remember_me_popup738">
                                                                                    <input type="checkbox" name="remember_me" id="remember_me_popup738" value="Y" />Remember me</label>
                                                                            </div>
                                                                        </div> <span></span>
                                                                        <div class="buttons-container clearfix">
                                                                            <div class="pull-right">
                                                                                <button id="popup738" class="btn btn-primary" type="submit" name="dispatch[auth.login]">Sign in</button>
                                                                            </div>
                                                                            <div class="form-group pull-left"> <a class="btn btn-success" href="profiles-add/index.php" rel="nofollow">Register</a> </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?php
                                            }
                                            ?>

                                                
                                            
                                        </div>
                                    </section>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
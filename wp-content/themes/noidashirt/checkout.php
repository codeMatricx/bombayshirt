﻿<?php /* Template Name: checkout */ ?>
<?php get_header(); ?>
<?$user_id = get_current_user_id();
if(isset($_POST['billingDetail']))
{

$UfName=$_POST['firstName'];
$UlName=$_POST['lastName'];
$UPhone=$_POST['phone'];
$Uaddress1=$_POST['address1'];
$Uaddress2=$_POST['address2'];
$Ucity=$_POST['city'];
$Ucountry=$_POST['country'];
$Ustate=$_POST['state'];
$UzipCode=$_POST['zip'];
$Uemail=$_POST['email'];

$proUpdate=$wpdb->update( 'wp_user_billing_address', array( 'first_name' => $UfName, 'last_name' => $UlName, 'phone' => $UPhone, 'address' => $Uaddress1, 'address_line_2' => $Uaddress2, 'city' => $Ucity,'country' => $Ucountry, 'state' => $Ustate, 'zip_code' => $UzipCode, 'email' => $Uemail ),  array( 'user_id' => $uId ),  array( '%s','%s','%d','%s','%s','%s','%s','%s','%d','%s' ), array( '%d' ));

} 
?>



<section class="dispatch-checkout-checkout content">
<div class="container-fluid  content-grid">
<div class="row">
<section class="col-lg-9 main-content-grid">
<aside class="sidebox order-summary hidden-md hidden-lg">
<h5 class="text-uppercase "> <strong>
Order summary
</strong> </h5>
<div class="sidebox-body">
<div id="checkout_info_summary_758">
<table class="table table-condensed checkout-summary">
<tbody>
<?php 
$sum = 0;
//$items = $_SESSION["cart_item"];
foreach($_SESSION["cart_item"] as $k => $v)
{
$data['title'] = $_SESSION["cart_item"][$k]['name'];
$data['productId'] = $_SESSION["cart_item"][$k]['code'];
$id = $_SESSION["cart_item"][$k]['code'];
$data['productQuantaty'] = $_SESSION["cart_item"][$k]['quantity'];
$data['price'] = $_SESSION["cart_item"][$k]['price'];
$product_id=$data['productId'];
$sum +=$data['price']; 
} 
?>
<tr>
<td><?php echo count($_SESSION['cart_item']);?> item(s)</td>
<td data-ct-checkout-summary="items" class="text-right"> <span>₹<span><?php echo $sum; ?></span></span>
</td>
</tr>
<tr>
<td>Shipping</td>
<td data-ct-checkout-summary="shipping" class="text-right"> <span>Free shipping</span> </td>
</tr>
<tr>
<td>Taxes <a rel="shadowbox;height=225;width=380;player=inline" href="#tax-implication">(?)</a></td>
<td></td>
</tr>
<tr>
<td data-ct-checkout-summary="tax-name GST">
<div>GST</div>
</td>
<td data-ct-checkout-summary="taxes" class="text-right"> <span>₹<span><?php echo $sum; ?></span></span>
</td>
</tr>
<!-- <tr>
<td colspan="2">
<form class="cm-ajax cm-ajax-force cm-ajax-full-render cm-processed-form" name="coupon_code_form" action="#" method="post"> <input type="hidden" name="result_ids" value="checkout*,cart_status*,cart_items,payment-methods"> <input type="hidden" name="redirect_url" value="#">
<div class="ty-gift-certificate-coupon ty-discount-coupon__control-group ty-input-append"> <label for="coupon_field" class="hidden cm-required">Promo code</label> <input type="text" class="ty-input-text cm-hint" id="coupon_field" name="hint_coupon_code" size="40" value="Enter Promo Code">                                                                    <button title="Apply" class="ty-btn-go btn" type="submit">Apply</button> <input type="hidden" name="dispatch" value="checkout.apply_coupon"> </div> <input type="hidden" name="security_hash"
class="cm-no-hide-input" value="77ce89bceb41bd7be03f99b7a5d83005"></form>
<form class="cm-ajax cm-ajax-full-render cm-processed-form" name="point_payment_form" action="https://staging.isiwal.com/SunnysBespokeDev/"
method="post"> <input type="hidden" name="redirect_mode" value="checkout"> <input type="hidden" name="result_ids" value="checkout*,cart_status*">
<div class="ty-discount-coupon__control-group ty-reward-points__coupon ty-input-append ty-inline-block">
<strong style="margin-bottom: 5px;font-size:13.5px;">You have ₹250.00 in 250 points</strong> <input style="margin-top: 10px;" type="text" class="ty-input-text ty-valign cm-hint" name="hint_points_to_use"
size="40" placeholder="Enter Points"> <button title="Apply" class="ty-btn-go btn" type="submit">Apply</button> <input type="hidden" name="dispatch" value="checkout.point_payment">
<input
type="submit" class="hidden" name="dispatch[checkout.point_payment]" value=""> </div> <input type="hidden" name="security_hash" class="cm-no-hide-input" value="77ce89bceb41bd7be03f99b7a5d83005"></form>
<div class="ty-discount-info hidden"> </div>
</td>
</tr> -->
<tr>
<td data-ct-checkout-summary="order-total"> <strong>
Order Total
</strong> </td>
<td class="text-right"> <strong class="products-total">₹<span><?php echo $sum; ?></span></strong> </td>
</tr>
</tbody>
</table>
</div>
</div>
</aside>
<?PHP
$billingProfile = $wpdb->get_row("SELECT first_name,last_name,phone,address,address_line_2,city,country,state,zip_code,email FROM wp_user_billing_address WHERE user_id ='$user_id'");
$fName = $billingProfile->first_name;
$lName = $billingProfile->last_name;
$phone = $billingProfile->phone;
$address1 = $billingProfile->address;
$address2 = $billingProfile->address_line_2;
$city = $billingProfile->city;
$country = $billingProfile->country;
$state = $billingProfile->state;
$zip = $billingProfile->zip_code;
$Uemail = $billingProfile->email;
?>
<div class="mainbox-container clearfix">
<div class="page-header">
<h1> <span class="checkout-title">Secure checkout&nbsp;<i class="glyphicon glyphicon-lock fa fa-lock"></i></span> </h1>
</div>
<div class="mainbox-body">
<div class="checkout-steps cm-save-fields clearfix" id="checkout_steps">
<div class="panel panel-default" data-ct-checkout="user_info" id="step_one">
<div class="panel-heading"> <a class="btn btn-default pull-right cm-ajax change  " href="#" data-ca-target-id="checkout_*"> Change</a>
<h4> <span class="label label-default"><i class="glyphicon glyphicon-ok fa fa-check"></i></span> <a class="cm-ajax" href="#"
data-ca-target-id="checkout_*">Signed in as <?php echo $Uemail; ?></a> </h4>
</div>
<div id="step_one_body" class="panel-body ">
<form name="step_one_contact_information_form" class="form-horizontal cm-ajax cm-processed-form" action="#" method="post"> 
<input type="hidden" name="update_step" value="step_one"> <input type="hidden" name="next_step" value="step_three"> <input type="hidden" name="result_ids" value="checkout*">
<div class="clearfix sign-in-or-login"> 
<a href="#" class="btn btn-default">Sign in as a different user</a> <button class="btn btn btn-primary" type="submit" name="">Continue</button> </div>
<input type="hidden" name="security_hash" class="cm-no-hide-input" value="77ce89bceb41bd7be03f99b7a5d83005">
</form>
</div>
<!--step_one-->
</div>
<div class="panel panel-default" data-ct-checkout="billing_shipping_address" id="step_two">
<div class="panel-heading"> <a class="btn btn-default pull-right cm-ajax change  " href="#" data-ca-target-id="checkout_*"> Change</a>
<h4> <span class="label label-default"><i class="glyphicon glyphicon-ok fa fa-check"></i></span> <a class="cm-ajax" href="#"
data-ca-target-id="checkout_*">Billing/Shipping Address</a> </h4>
</div>
<div id="step_two_body" class="panel-body cm-skip-save-fields">
<form name="step_two_billing_address" class="form-horizontal cm-ajax cm-ajax-full-render  cm-processed-form" action="#" method="post"> <input type="hidden" name="update_step" value="step_two"> <input type="hidden" name="next_step" value="step_one"> <input type="hidden" name="result_ids" value="checkout*,account*"> <input type="hidden" name="dispatch" value="checkout.checkout">
<div class="clearfix">
<div class="checkout-block"> <input type="hidden" id="profile_name" name="user_data[profile_name]" value="Main"> </div>
</div>
<div class="clearfix" data-ct-address="billing-address">
<div class="checkout-block"> <input type="hidden" name="ship_to_another" value="1">
<h3>Billing address</h3>

<form action="" method="POST">
<div class="row form-group">
<div class="col-md-6 col-sm-6 billing-first-name">
<div class="col-sm-3 hidden"> <label for="elm_14" class="control-label cm-profile-field cm-required ">First name</label> </div> <input placeholder="First name" x-autocompletetype="given-name" type="text" id="elm_14" name="firstName" size="32" value="<?php echo $fName; ?>"
class="form-control   cm-focus"> 
</div>
<div class="col-md-6 col-sm-6 billing-last-name">
<div class="col-sm-3 hidden"> <label for="elm_16" class="control-label cm-profile-field cm-required ">Last name</label> </div> <input placeholder="Last name" x-autocompletetype="surname" type="text" id="elm_16" name="lastName" size="32" value="<?php echo $lName; ?>"
class="form-control  "> </div>
</div>
<div class="row form-group">
<div class="col-md-12 col-sm-12 billing-email">
<div class="col-sm-3 hidden"> <label for="elm_32" class="control-label cm-profile-field cm-required cm-email ">E-mail</label> </div> <input placeholder="E-mail" disabled=""  type="text"  name="email" size="32" value="<?php echo $Uemail; ?>"
class="form-control cm-skip-avail-switch "> </div>
</div>
<div class="row form-group">
<div class="col-md-12 col-sm-12 billing-address">
<div class="col-sm-3 hidden"> <label for="elm_18" class="control-label cm-profile-field cm-required ">Address</label> </div> <input placeholder="Flat No. / House No. / Floor / Building" x-autocompletetype="street-address" type="text" id="elm_18" name="address1"
size="32" value="<?php echo $address1; ?>" class="form-control  "> </div>
</div>
<div class="row form-group">
<div class="col-md-12 col-sm-12 billing-address-line-two">
<div class="col-sm-3 hidden"> <label for="elm_20" class="control-label cm-profile-field  ">Address Line 2</label> </div> <input placeholder="Colony / Street / Locality / Landmark" x-autocompletetype="address-line2" type="text" id="elm_20" name="address2"
size="32" value="<?php echo $address2; ?>" class="form-control  "> </div>
</div>
<div class="row form-group">
<div class="col-md-12 col-sm-12 billing-phone">
<div class="col-sm-3 hidden"> <label for="elm_30" class="control-label cm-profile-field cm-required ">Phone</label> </div> <input placeholder="Phone" x-autocompletetype="phone-full" type="text" id="elm_30" name="phone" size="32" value="<?php echo $phone; ?>" class="form-control  ">                </div>
</div>
<div class="row form-group">
<div class="col-md-3 col-sm-3 billing-city">
<label for="elm_26" class="control-label cm-profile-field cm-required ">City</label> 
<input placeholder="City" x-autocompletetype="city" type="text" id="elm_22" name="city" size="32" value="<?php echo $city; ?>" class="form-control  "> 
</div>
<div class="col-md-3 col-sm-3 billing-country">
<div class="col-sm-3 hidden"> <label for="elm_26" class="control-label cm-profile-field cm-required ">Country</label> 
<input placeholder="Country" x-autocompletetype="country-full" type="text" id="elm_30" name="country" size="32" value="India" class="form-control  "> 
</div>
</div>
<div class="col-md-3 col-sm-3 billing-state">
<div class="col-sm-3 hidden"> <label for="elm_24" class="control-label cm-profile-field cm-required ">State/province</label> </div> <select x-autocompletetype="state" id="elm_24" class="form-control cm-state cm-location-billing" name="state"> <option value="">- Select state -</option>                                     <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option> <option value="Andhra Pradesh">Andhra Pradesh</option> <option value="Arunachal Pradesh">Arunachal Pradesh</option> <option value="Assam">Assam</option> <option value="Bihar">Bihar</option> <option value="Chandigarh">Chandigarh</option> <option value="Chhattisgarh">Chhattisgarh</option> <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option> <option value="Daman and Diu">Daman and Diu</option> <option value="Delhi">Delhi</option> <option value="Goa">Goa</option> <option value="Gujarat">Gujarat</option> <option value="Haryana">Haryana</option> <option value="Himachal Pradesh">Himachal Pradesh</option> <option value="Jammu and Kashmir">Jammu and Kashmir</option> <option value="Jharkhand">Jharkhand</option> <option value="Karnataka">Karnataka</option> <option value="Kerala">Kerala</option> <option value="Lakshadweep">Lakshadweep</option> <option value="Madhya">Madhya Pradesh</option> <option value="Maharashtra">Maharashtra</option> <option value="Manipur">Manipur</option> <option value="Meghalaya">Meghalaya</option> <option value="Mizoram">Mizoram</option> <option value="Nagaland">Nagaland</option> <option value="Odisha">Odisha</option> <option value="Puducherry">Puducherry</option> <option value="Punjab">Punjab</option> <option value="Rajasthan">Rajasthan</option> <option value="Sikkim">Sikkim</option> <option value="Tamil Nadu">Tamil Nadu</option> <option value="Telangana">Telangana</option> <option value="Tripura">Tripura</option> <option selected="selected" value="Uttar Pradesh">Uttar Pradesh</option> <option value="Uttarakhand">Uttarakhand</option> <option value="West Bengal">West Bengal</option></select>
<input
x-autocompletetype="state" placeholder="State/province" type="text" id="elm_24_d" name="state" size="32" maxlength="64" value="" disabled="" class="cm-state cm-location-billing form-control hidden  cm-skip-avail-switch">
</div>
<div class="col-md-3 col-sm-3 billing-zip-code">
<div class="col-sm-3 hidden"> <label for="elm_28" class="control-label cm-profile-field cm-required cm-zipcode cm-location-billing">Zip/postal code</label> </div> <input placeholder="Zip/postal code" x-autocompletetype="postal-code" type="text" id="elm_28" name="zip"
size="32" value="<?php echo $zip; ?>" class="form-control  "> </div>

</div>
<button class="btn btn-default pull-right" type="submit" name="billingDetail">Continue</button> 
</form>
</div>
</div>
<div class="clearfix shipping-address__switch" data-ct-address="shipping-address">
<!-- <div class="panel panel-default address-check">
<div class="panel-body"> <span>Are shipping and billing addresses the same?</span>
<div class="pull-right"> <label class="radio-inline control-label" for="sw_sa_suffix_no"> <input class="radio cm-switch-availability cm-switch-visibility" type="radio" name="ship_to_another" value="1" id="sw_sa_suffix_no">
No
</label> <label class="radio-inline control-label" for="sw_sa_suffix_yes"> <input class="radio cm-switch-availability cm-switch-inverse cm-switch-visibility" type="radio" name="ship_to_another" value="0" id="sw_sa_suffix_yes" checked="checked">
Yes
</label> </div>
</div>
</div> -->
<div id="sa" class="hidden shipping address">
<div class="checkout-block">
<h3>Shipping address</h3>
<form action="" method="POST">
<div class="row form-group">
<div class="row form-group">
<div class="col-md-6 col-sm-6 shipping-first-name">
<div class="col-sm-3 hidden"> <label for="elm_15" class="control-label cm-profile-field cm-required ">First name</label> </div> 
<input placeholder="First name" x-autocompletetype="given-name" type="text" id="elm_15" name="firstName" size="32" value="<?php echo $fName; ?>"
class="form-control disabled  cm-focus" disabled="disabled"> 
</div>
<div class="col-md-6 col-sm-6 shipping-last-name">
<div class="col-sm-3 hidden"> <label for="elm_17" class="control-label cm-profile-field cm-required ">Last name</label> </div> <input placeholder="Last name" x-autocompletetype="surname" type="text" id="elm_17" name="lastName" size="32" value="<?php echo $lName; ?>"
class="form-control disabled " disabled="disabled"> </div>
<div class="col-sm-3 hidden"> <label for="elm_17" class="control-label cm-profile-field cm-required ">Email</label> </div> <input placeholder="Email"  type="text" name="email" size="32" value="<?php echo $Uemail; ?>"
class="form-control disabled " disabled="disabled"> 
</div>
</div>
<div class="row form-group">
<div class="col-md-12 col-sm-12 shipping-address">
<div class="col-sm-3 hidden"> <label for="elm_19" class="control-label cm-profile-field cm-required ">Address</label> </div> <input placeholder="Flat No. / House No. / Floor / Building" x-autocompletetype="street-address" type="text" id="elm_19" name="address1"
size="32" value="<?php echo $address1; ?>" class="form-control disabled " disabled="disabled"> </div>
</div>
<div class="row form-group">
<div class="col-md-12 col-sm-12 shipping-address-line-two">
<div class="col-sm-3 hidden"> <label for="elm_21" class="control-label cm-profile-field  ">Address Line 2</label> </div> <input placeholder="Colony / Street / Locality / Landmark" x-autocompletetype="address-line2" type="text" id="elm_21" name="address2"
size="32" value="<?php echo $address2; ?>" class="form-control disabled " disabled="disabled"> </div>
</div>
<div class="row form-group">
<div class="col-md-12 col-sm-12 shipping-phone">
<div class="col-sm-3 hidden"> <label for="elm_31" class="control-label cm-profile-field  ">Phone</label> </div> <input placeholder="Phone" x-autocompletetype="phone-full" type="text" id="elm_31" name="phone" size="32" value="<?php echo $phone; ?>" class="form-control disabled "
disabled="disabled"> </div>
</div>

<div class="col-md-3 col-sm-3 shipping-city"><label for="elm_27" class="control-label cm-profile-field cm-required ">City</label>  <input placeholder="City" x-autocompletetype="city" type="text" id="elm_23" name="city" size="32" value="<?php echo $city; ?>" class="form-control disabled " disabled="disabled"> </div>
<div class="col-md-3 col-sm-3 shipping-country">
<div class="col-sm-3 hidden"> <label for="elm_27" class="control-label cm-profile-field cm-required ">Country</label> 
<input placeholder="Country" x-autocompletetype="country-full" type="text" id="elm_31" name="country" size="32" value="India"
disabled="disabled">
</div> 
</div>
<div class="col-md-3 col-sm-3 shipping-state">
<div class="col-sm-3 hidden"> <label for="elm_25" class="control-label cm-profile-field cm-required ">State/province</label> </div> <select x-autocompletetype="state" id="elm_25" class="form-control cm-state cm-location-shipping disabled" name="user_data[s_state]"
disabled=""> <option value="">- Select state -</option><option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option> <option value="Andhra Pradesh">Andhra Pradesh</option> <option value="Arunachal Pradesh">Arunachal Pradesh</option> <option value="Assam">Assam</option> <option value="Bihar">Bihar</option> <option value="Chandigarh">Chandigarh</option> <option value="Chhattisgarh">Chhattisgarh</option> <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option> <option value="Daman and Diu">Daman and Diu</option> <option value="Delhi">Delhi</option> <option value="Goa">Goa</option> <option value="Gujarat">Gujarat</option> <option value="Haryana">Haryana</option> <option value="Himachal Pradesh">Himachal Pradesh</option> <option value="Jammu and Kashmir">Jammu and Kashmir</option> <option value="Jharkhand">Jharkhand</option> <option value="Karnataka">Karnataka</option> <option value="Kerala">Kerala</option> <option value="Lakshadweep">Lakshadweep</option> <option value="Madhya">Madhya Pradesh</option> <option value="Maharashtra">Maharashtra</option> <option value="Manipur">Manipur</option> <option value="Meghalaya">Meghalaya</option> <option value="Mizoram">Mizoram</option> <option value="Nagaland">Nagaland</option> <option value="Odisha">Odisha</option> <option value="Puducherry">Puducherry</option> <option value="Punjab">Punjab</option> <option value="Rajasthan">Rajasthan</option> <option value="Sikkim">Sikkim</option> <option value="Tamil Nadu">Tamil Nadu</option> <option value="Telangana">Telangana</option> <option value="Tripura">Tripura</option> <option selected="selected" value="Uttar Pradesh">Uttar Pradesh</option> <option value="Uttarakhand">Uttarakhand</option> <option value="West Bengal">West Bengal</option></select>
<input
x-autocompletetype="state" placeholder="State/province" type="text" id="elm_25_d" name="state" size="32" maxlength="64" value="UP" disabled="" class="cm-state cm-location-shipping form-control hidden disabled cm-skip-avail-switch">
</div>
<div class="col-md-3 col-sm-3 shipping-zip-code">
<div class="col-sm-3 hidden"> <label for="elm_29" class="control-label cm-profile-field cm-required cm-zipcode cm-location-shipping">Zip/postal code</label> </div> <input placeholder="Zip/postal code" x-autocompletetype="postal-code" type="text" id="elm_29" name="zip"
size="32" value="<?php echo $zip; ?>" class="form-control disabled " disabled="disabled"> </div>
</div>
<button class="btn btn-default pull-right" type="submit" name="billingDetail">Continue</button>
</form>
</div>
</div>
</div>
<!-- <div class=""> <button class="btn btn-default pull-right" type="submit" name="billingDetail">Continue</button> </div> <input type="hidden" name="security_hash" class="cm-no-hide-input" value="77ce89bceb41bd7be03f99b7a5d83005"> -->
</form>
</div>
<!--step_two-->
</div>
<div class="panel panel-default" data-ct-checkout="user_info" id="step_three">
<div class="panel-heading"> <a class="btn btn-default pull-right cm-ajax change  " href="#" data-ca-target-id="checkout_*"> Change</a>
<h4> <span class="label label-default"><i class="glyphicon glyphicon-ok fa fa-check"></i></span> <a class="cm-ajax" href="#"
data-ca-target-id="checkout_*">Sizing &amp; Fit</a> </h4>
</div>
<div id="step_three_body" class="panel-body  user-sizing cm-skip-save-fields">
<form name="step_three_sizing_form" class="form-horizontal cm-ajax cm-processed-form" action="#" method="post"> <input type="hidden" name="update_step" value="step_three"> <input type="hidden" name="next_step" value="step_five"> <input type="hidden" name="result_ids" value="checkout*">
<div class="measurements-menu">
<div class="row">
<div class="col-md-4">
<div class="profile-sub-tab-header active" data-link=".smart-sizing"> <i class="radio"><i></i></i> <span class="heading"> <strong>Find my size - use <span class="highlight">FitSmart</span></strong><br> Your custom size in 6 easy steps.
</span>
</div>
</div>
<div class="col-md-4">
<div class="profile-sub-tab-header" data-link=".standard-sizing"> <i class="radio"><i></i></i> <span class="heading"> <strong>Select a Standard Size</strong><br>
Choose from size XS - XXXL
</span> </div>
</div>
<div class="col-md-4 united-states-hidden">
<div class="profile-sub-tab-header" data-link=".send-a-shirt"> <i class="radio"><i></i></i> <span class="heading"> <strong>Send us a shirt</strong><br>
Send us your best fitting shirt and we'll copy the measurements.
</span> </div>
</div>
</div>
</div>
<div class="profile-sub-tab smart-sizing" style="display: block;">
<div class="tab-content">
<div class="tab-pane block" id="intro">
<p> <span class="hidden-xs hidden-sm">
We've collected years of data to calculate the perfect fit.<br>
FitSmart lets you find your Custom Size in
<strong>6 easy steps</strong> </span> <span class="hidden-md hidden-lg">
We've collected years of data to calculate the perfect fit. FitSmart lets you find your Custom Size in
<strong>6 easy steps</strong> </span> <a href="#gender" class="btn btn-bordered">Get Started</a> </p>
</div>
<div class="tab-pane" id="gender" style="display:none;"> <span class="step">Step 1 of 6</span>
<h1>Gender</h1>
<div class="clearfix"> <label class="btn btn-bordered male radio"> <span>Male
<input type="radio" name="gender" value="male" checked=""> </span> </label> <label class="btn btn-bordered female radio"> <span>Female
<input type="radio" name="gender" value="female"> </span> </label> </div> <a href="#smart-wizard-male" class="btn btn-bordered btn-primary">Next</a> </div>
<div class="tab-pane active" id="finish" style="display:none;"> <span class="step hidden"></span>
<h1>Your measurements are now calculated!</h1>
<div class="row expanded">
<div class="col-md-6"> <a class="btn btn-bordered btn-primary review" href="">Review My Measurements</a> </div>
<div class="col-md-6"> <a class="btn btn-primary continue" href="">Continue to Billing</a> </div>
</div>
<div class="row succinct" style="display:none;">
<div class="col-md-12"> <a class="btn btn-bordered btn-primary review" href="">Review My Measurements</a> </div>
</div>
<div class="fit-promise text-center" style="display: block;">
<h2>FIT PROMISE</h2>
<p class="hidden-sm hidden-xs">If you're not completely satisfied with the<br>fit on your first order, we will alter your shirt or completely remake it free of cost.</p>
<p class="hidden-md hidden-lg">If you're not completely satisfied with the fit on your first order, we will alter your shirt or completely remake it free of cost.</p>
</div>
</div>
</div>
<div id="smart-wizard-male" class="smart-wizard">
<div class="navbar">
<div class="navbar-inner">
<div class="container">
<ul class="nav nav-pills">
<li class="active"> <a data-toggle="tab" href="#male-size" aria-expanded="true">Size</a> </li>
<li> <a data-toggle="tab" href="#male-shoulders">Sloping Shoulders</a> </li>
<li> <a data-toggle="tab" href="#male-height">Height</a> </li>
<li> <a data-toggle="tab" href="#male-shape">Shape</a> </li>
<li> <a data-toggle="tab" href="#male-fit">Fit</a> </li>
<li> </li>
</ul>
</div>
</div>
</div>
<div class="tab-content">
<div class="tab-pane active" id="male-size"> <span class="step">Step 2 of 6</span>
<h1>What is your preferred size?</h1>
<div class="btn-group bootstrap-select male-size"><button type="button" class="btn dropdown-toggle bs-placeholder btn-default" data-toggle="dropdown" title="--"><span class="filter-option pull-left">--</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button>
<div
class="dropdown-menu open">
<ul class="dropdown-menu inner" role="menu">
<li data-original-index="0" class="selected"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">--</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">XS (36)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">S (38)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">M (40)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">L (42)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="5"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">XL (44)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="6"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">XXL (46)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="7"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">XXXL (48)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
</ul>
</div><select data-type="select" data-live-search="false" class="male-size" name="male-size" tabindex="-98"> <option value="">--</option> <option value="xs">XS (36)</option> <option value="s">S (38)</option> <option value="m">M (40)</option> <option value="l">L (42)</option> <option value="xl">XL (44)</option> <option value="xxl">XXL (46)</option> <option value="xxxl">XXXL (48)</option> </select></div>
<a rel="shadowbox;width=1366;height=669;player=inline" href="#size-chart-desktop" class="size-chart-link hidden-xs hidden-sm">Size Chart</a> <a rel="shadowbox;width=436;height=400;player=inline" href="#size-chart-mobile" class="size-chart-link hidden-md hidden-lg">Size Chart</a>                    </div>
<div class="tab-pane" id="male-shoulders"> <span class="step">Step 3 of 6</span>
<h1>Are your shoulders sloping?</h1>
<div class="clearfix"> <label class="btn btn-bordered sloping radio"> <span>Yes
<input type="radio" name="male-shoulder" value="Yes"> </span> </label> <label class="btn btn-bordered not-sloping radio"> <span>No
<input type="radio" name="male-shoulder" value="No"> </span> </label> <label class="btn btn-bordered sloping-dont-know radio"> <span>I don't know
<input type="radio" name="male-shoulder" value="dont-know" checked="checked"> </span> </label> </div>
</div>
<div class="tab-pane" id="male-height"> <span class="step">Step 4 of 6</span>
<h1>What is your height?</h1>
<div class="slider-wrapper">
<div class="slider height ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" name="male-height"><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" data-hasqtip="18" aria-describedby="qtip-18" style="left: 0%;"><div id="qtip-18" class="qtip qtip-default qtip-light qtip-pos-bc" tracking="false" role="alert" aria-live="polite" aria-atomic="false" aria-describedby="qtip-18-content" aria-hidden="false" data-qtip-id="18" style="z-index: 15001; top: -49px; left: -23.5px; opacity: 1; display: block;"><div class="qtip-tip" style="background-color: transparent !important; border: 0px !important; width: 16px; height: 8px; line-height: 8px; left: 50%; margin-left: -8px; bottom: -8px;"><canvas width="16" height="8" style="background-color: transparent !important; border: 0px !important; width: 16px; height: 8px;"></canvas></div><div class="qtip-content" id="qtip-18-content" aria-atomic="true">5' 2''<br>157 cm</div></div></span></div>
<span class="slider-background"></span> </div>
</div>
<div class="tab-pane" id="male-shape"> <span class="step">Step 5 of 6</span>
<h1>What is your body shape?</h1>
<div class="clearfix"> <label class="btn btn-bordered skinny radio"> <span>Very Skinny
<input type="radio" name="male-shape" value="skinny"> <i>You have thin build with narrow shoulders, torso and biceps.</i> </span> </label> <label class="btn btn-bordered athletic radio"> <span>Fit
<input type="radio" name="male-shape" value="athletic"> <i>You have a broader upper body and a thin waist.</i> </span> </label> <label class="btn btn-bordered average radio"> <span>Average
<input type="radio" name="male-shape" value="average"> <i>You are an average build with a slight stomach. </i> </span> </label> <label class="btn btn-bordered healthy radio"> <span>Healthy
<input type="radio" name="male-shape" value="healthy"> <i>Your fittest days are behind you. You have a protruding belly. </i> </span> </label> </div>
</div>
<div class="tab-pane" id="male-fit"> <span class="step">Step 6 of 6</span>
<h1>What fit do you prefer?</h1>
<div class="clearfix"> <label class="btn btn-bordered body radio"> <span>Super Slim
<input type="radio" name="male-fit" value="body"> <i>Extremely fitted around the chest, waist and biceps.</i> </span> </label> <label class="btn btn-bordered structured radio"> <span>Structured
<input type="radio" name="male-fit" value="structured"> <i>Stylishly fitted, but leaving room for comfort.</i> </span> </label> <label class="btn btn-bordered comfort radio"> <span>Baggy
<input type="radio" name="male-fit" value="comfort"> <i>A more boxy and relaxed fit, especially around the waist.</i> </span> </label> </div>
</div>
<ul class="pager wizard">
<li class="previous disabled"><a href="javascript:;">Previous</a></li>
<li class="next"><a href="javascript:;">Next</a></li>
<li class="finish hidden"><a href="javascript:;">Finish</a></li>
</ul>
</div>
</div>
<div id="smart-wizard-female" class="smart-wizard">
<div class="navbar">
<div class="navbar-inner">
<div class="container">
<ul class="nav nav-pills">
<li class="active"> <a data-toggle="tab" href="#female-size" aria-expanded="true">Size</a> </li>
<li> <a data-toggle="tab" href="#female-height">Height</a> </li>
<li> <a data-toggle="tab" href="#female-fit">Fit</a> </li>
</ul>
</div>
</div>
</div>
<div class="tab-content">
<div class="tab-pane active" id="female-size"> <span class="step">Step 2 of 4</span>
<h1>What is your preferred size?</h1>
<div class="btn-group bootstrap-select female-size"><button type="button" class="btn dropdown-toggle bs-placeholder btn-default" data-toggle="dropdown" title="--"><span class="filter-option pull-left">--</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button>
<div
class="dropdown-menu open">
<ul class="dropdown-menu inner" role="menu">
<li data-original-index="0" class="selected"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">--</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">0 (XXS)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">2 (XS)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">4 (S)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">6 (M)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="5"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">8 (L)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="6"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">10 (XL)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
</ul>
</div><select data-type="select" data-live-search="false" class="female-size" name="female-size" tabindex="-98"> <option value="">--</option> <option value="0">0 (XXS)</option> <option value="2">2 (XS)</option> <option value="4">4 (S)</option> <option value="6">6 (M)</option> <option value="8">8 (L)</option> <option value="10">10 (XL)</option> </select></div>
<a rel="shadowbox;width=1366;height=669;player=inline" href="#size-chart-desktop" class="size-chart-link hidden-xs hidden-sm">Size Chart</a> <a rel="shadowbox;width=436;height=400;player=inline" href="#size-chart-mobile" class="size-chart-link hidden-md hidden-lg">Size Chart</a>                </div>
<div class="tab-pane" id="female-height"> <span class="step">Step 3 of 4</span>
<h1>What is your height?</h1>
<div class="slider-wrapper">
<div class="slider height ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" name="female-height"><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" data-hasqtip="19" aria-describedby="qtip-19" style="left: 0%;"><div id="qtip-19" class="qtip qtip-default qtip-light qtip-pos-bc qtip-focus" tracking="false" role="alert" aria-live="polite" aria-atomic="false" aria-describedby="qtip-19-content" aria-hidden="false" data-qtip-id="19" style="z-index: 15002; top: -49px; left: -23.5px; opacity: 1; display: block;"><div class="qtip-tip" style="background-color: transparent !important; border: 0px !important; width: 16px; height: 8px; line-height: 8px; left: 50%; margin-left: -8px; bottom: -8px;"><canvas width="16" height="8" style="background-color: transparent !important; border: 0px !important; width: 16px; height: 8px;"></canvas></div><div class="qtip-content" id="qtip-19-content" aria-atomic="true">4' 10''<br>147 cm</div></div></span></div>
<span class="slider-background"></span> </div>
</div>
<div class="tab-pane" id="female-fit"> <span class="step">Step 4 of 4</span>
<h1>What fit do you prefer?</h1>
<div class="clearfix"> <label class="btn btn-bordered fitted radio"> <span>Fitted
<input type="radio" name="female-fit" value="fitted"> <i>A formal, well-fitted shirt.</i> </span> </label> <label class="btn btn-bordered loose radio"> <span>Loose
<input type="radio" name="female-fit" value="loose"> <i>Comfortable, boyfriend style shirt.</i> </span> </label> </div>
</div>
<ul class="pager wizard">
<li class="previous disabled"><a href="javascript:;">Previous</a></li>
<li class="next"><a href="javascript:;">Next</a></li>
<li class="finish hidden"><a href="javascript:;">Finish</a></li>
</ul>
</div>
</div>
</div>
<div class="profile-sub-tab standard-sizing" style="display: none;">
<div class="tab-content">
<div class="row">
<div class="col-md-4">
<div class="ty-control-group ty-profile-field__item ty-profile-sloping-shoulder"> <label for="elm_60" class="ty-control-group__title cm-profile-field ">Gender</label>
<div class="btn-group bootstrap-select ty-profile-field__select" style="width: 100%;"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" data-id="elm_60" title="Male" aria-expanded="false"><span class="filter-option pull-left">Male</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button>
<div
class="dropdown-menu open" style="max-height: 248px; overflow: hidden; min-height: 0px;">
<ul class="dropdown-menu inner" role="menu" style="max-height: 246px; overflow-y: auto; min-height: 0px;">
<li data-original-index="0"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">--</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="1" class="selected"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Male</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Female</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
</ul>
</div><select data-undo="" data-profile="gender" data-type="select" data-width="100%" id="elm_60" class="ty-profile-field__select" name="user_data[fields][60]" tabindex="-98"> <option value="">--</option> <option value="26">Male</option> <option value="27">Female</option> </select></div>
</div>
</div>
<div class="col-md-4"> <label for="elm_61" class="ty-control-group__title cm-profile-field ">Fit</label>
<div class="btn-group bootstrap-select ty-profile-field__select" style="width: 100%;"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" data-id="elm_61" title="Relaxed" aria-expanded="false"><span class="filter-option pull-left">Relaxed</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button>
<div
class="dropdown-menu open" style="max-height: 248px; overflow: hidden; min-height: 102px;">
<ul class="dropdown-menu inner" role="menu" style="max-height: 246px; overflow-y: auto; min-height: 100px;">
<li data-original-index="0"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">--</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="1" class="selected"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Relaxed</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Slim</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="3"><a tabindex="0" class="hide" style="" data-tokens="null"><span class="text">Custom</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
</ul>
</div><select data-undo="" data-profile="fit" data-type="select" data-width="100%" id="elm_61" class="ty-profile-field__select" name="user_data[fields][61]" tabindex="-98"> <option value="">--</option> <option value="28">Relaxed</option> <option value="29">Slim</option> <option value="33" class="hide">Custom</option> </select></div>
</div>
<div class="col-md-4 standard-size-dropdowns"> <label for="elm_59" class="ty-control-group__title cm-profile-field ">Standard Size</label>
<div class="btn-group bootstrap-select ty-profile-field__select" style="width: 100%;"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" data-id="elm_59" title="M (40)" aria-expanded="false"><span class="filter-option pull-left">M (40)</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button>
<div
class="dropdown-menu open" style="max-height: 363px; overflow: hidden; min-height: 102px;">
<ul class="dropdown-menu inner" role="menu" style="max-height: 361px; overflow-y: auto; min-height: 100px;">
<li data-original-index="0" class=""><a tabindex="0" class="" style="" data-tokens="null"><span class="text">--</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">XS (36)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">S (38)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="3" class="selected"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">M (40)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">L (42)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="5"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">XL (44)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="6"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">XXL (46)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="7"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">XXXL (48)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="8"><a tabindex="0" class="hide" style="" data-tokens="null"><span class="text">Custom</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
</ul>
</div><select data-undo="" data-profile="standard-size-male" data-hide-disabled="true" data-type="select" data-width="100%" id="elm_59" class="ty-profile-field__select" name="user_data[fields][59]" tabindex="-98"> <option value="">--</option> <option value="19">XS (36)</option> <option value="20">S (38)</option> <option value="21">M (40)</option> <option value="22">L (42)</option> <option value="23">XL (44)</option> <option value="24">XXL (46)</option> <option value="25">XXXL (48)</option> <option value="32" class="hide">Custom</option> </select></div>
<div class="btn-group bootstrap-select ty-profile-field__select disabled" style="width: 100%; display: none;"><button type="button" class="btn dropdown-toggle bs-placeholder disabled btn-default" data-toggle="dropdown" data-id="elm_58" title="--" tabindex="-1"><span class="filter-option pull-left">--</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button>
<div
class="dropdown-menu open">
<ul class="dropdown-menu inner" role="menu">
<li data-original-index="0" class="selected"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">--</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">0 </span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">4</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">6</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="5"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">8</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="6"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">10</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="7"><a tabindex="0" class="hide" style="" data-tokens="null"><span class="text">Custom</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
</ul>
</div><select data-undo="" data-profile="standard-size-female" data-hide-disabled="true" data-type="select" data-width="100%" id="elm_58" class="ty-profile-field__select" name="user_data[fields][58]" tabindex="-98" disabled="disabled"> <option value="">--</option> <option value="30">0 </option> <option value="14">2</option> <option value="15">4</option> <option value="16">6</option> <option value="17">8</option> <option value="18">10</option> <option value="31" class="hide">Custom</option> </select></div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<p class="post-tip"> <a rel="shadowbox;width=1366;height=669;player=inline" href="#size-chart-desktop" class="pull-right size-chart-link hidden-xs hidden-sm">Size Chart</a> <a rel="shadowbox;width=436;height=400;player=inline" href="#size-chart-mobile" class="pull-right size-chart-link hidden-md hidden-lg">Size Chart</a>            </p>
</div>
</div>
<div class="row">
<div class="col-md-12 text-center fit-promise" style="display: block;">
<h2>FIT PROMISE</h2>
<p class="hidden-sm hidden-xs">If you're not completely satisfied with the fit on<br>your first order, we will alter your shirt or completely remake it free of cost.</p>
<p class="hidden-md hidden-lg">If you're not completely satisfied with the fit on your first order, we will alter your shirt or completely remake it free of cost.</p>
</div>
</div>
</div>
</div>
<div class="profile-sub-tab custom-measurements" style="display:none;">
<div class="boxed white measurement-values" style="">
<div class="row">
<div class="col-md-12 text-center">
<p>
We've saved your profile with your final custom <strong>shirt measurements</strong>, in <strong>inches</strong>.<br>You can <strong>edit</strong> any measurement you like by using the + / - buttons, or else click continue to proceed.<br><br>                    <span class="hidden-xs hidden-sm">Use our <strong><a href="#" target="_blank">How to Measure guide</a></strong> to get the perfect fit.<br><br><br></span> </p>
</div>
</div>
<div class="row">
<div class="col-md-3 col-md-offset-1 col-sm-6 right"> <label for="elm_62" class="ty-control-group__title cm-profile-field "> <span class="hint" data-hasqtip="20"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/sizing-collar.jpg"> </span>
Collar
</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-down" type="button">-</button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input data-undo="0.00" min="0.00"
max="200.00" data-step="0.25" type="text" id="elm_62" name="user_data[fields][62]" value="0.00" class="spinner ty-input-text   form-control" readonly="readyonly" data-shirt="collar" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix"
style="display: none;"></span><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-up" type="button">+</button></span></div> <label for="elm_63" class="ty-control-group__title cm-profile-field "> <span class="hint" data-hasqtip="21"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/sizing-chest.jpg"> </span>
Chest
</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-down" type="button">-</button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input data-undo="0.00" min="0.00"
max="200.00" data-step="0.5" type="text" id="elm_63" name="user_data[fields][63]" value="0.00" class="spinner ty-input-text   form-control" readonly="readyonly" data-shirt="chest" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix"
style="display: none;"></span><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-up" type="button">+</button></span></div> <label for="elm_64" class="ty-control-group__title cm-profile-field "> <span class="hint" data-hasqtip="22"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/sizing-waist.jpg"> </span>
Waist
</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-down" type="button">-</button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input data-undo="0.00" min="0.00"
max="200.00" data-step="0.5" type="text" id="elm_64" name="user_data[fields][64]" value="0.00" class="spinner ty-input-text   form-control" readonly="readyonly" data-shirt="waist" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix"
style="display: none;"></span><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-up" type="button">+</button></span></div> <label for="elm_71" class="ty-control-group__title cm-profile-field ">
Hips
</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-down" type="button">-</button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input data-undo="0.00" min="0.00"
max="200.00" data-step="0.5" type="text" id="elm_71" name="user_data[fields][71]" value="0.00" class="spinner ty-input-text   form-control" readonly="readyonly" data-shirt="hips" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix"
style="display: none;"></span><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-up" type="button">+</button></span></div>
</div>
<div class="col-md-4 hidden-sm hidden-xs">
<div class="shirt"> <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/shirt.jpg" class="default"> <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/shirt-bicep.jpg" class="bicep"> <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/shirt-chest.jpg"
class="chest"> <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/shirt-collar.jpg" class="collar"> <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/shirt-cuff.jpg" class="cuff">                    <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/shirt-length.jpg" class="shirt-length"> <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/shirt-shoulder.jpg" class="shoulder">                    <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/shirt-sleeve-length.jpg" class="sleeve-length"> <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/shirt-waist.jpg" class="waist">                    </div>
</div>
<div class="col-md-3 col-sm-6 left"> <label for="elm_65" class="ty-control-group__title cm-profile-field ">
Shoulders
<span class="hint" data-hasqtip="23"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/sizing-shoulder-length.jpg"> </span> </label>
<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-down" type="button">-</button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input data-undo="0.00" min="0.00"
max="200.00" data-step="0.5" type="text" id="elm_65" name="user_data[fields][65]" value="0.00" class="spinner ty-input-text   form-control" readonly="readyonly" data-shirt="shoulder" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix"
style="display: none;"></span><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-up" type="button">+</button></span></div> <label for="elm_67" class="ty-control-group__title cm-profile-field ">
Biceps
<span class="hint" data-hasqtip="24"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/sizing-bicep.jpg"> </span> </label>
<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-down" type="button">-</button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input data-undo="0.00" min="0.00"
max="200.00" data-step="0.5" type="text" id="elm_67" name="user_data[fields][67]" value="0.00" class="spinner ty-input-text   form-control" readonly="readyonly" data-shirt="bicep" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix"
style="display: none;"></span><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-up" type="button">+</button></span></div> <label for="elm_68" class="ty-control-group__title cm-profile-field ">
Sleeve Length
<span class="hint" data-hasqtip="25"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/sizing-sleeve-length.jpg"> </span> </label>
<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-down" type="button">-</button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input data-undo="0.00" min="0.00"
max="200.00" data-step="0.5" type="text" id="elm_68" name="user_data[fields][68]" value="0.00" class="spinner ty-input-text   form-control" readonly="readyonly" data-shirt="sleeve-length" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix"
style="display: none;"></span><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-up" type="button">+</button></span></div> <label for="elm_69" class="ty-control-group__title cm-profile-field ">
Cuffs
<span class="hint" data-hasqtip="26"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/sizing-cuff.jpg"> </span> </label>
<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-down" type="button">-</button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input data-undo="0.00" min="0.00"
max="200.00" data-step="0.25" type="text" id="elm_69" name="user_data[fields][69]" value="0.00" class="spinner ty-input-text   form-control" readonly="readyonly" data-shirt="cuff" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix"
style="display: none;"></span><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-up" type="button">+</button></span></div>
</div>
<div class="col-sm-10 col-sm-offset-1 center"> <label for="elm_70" class="ty-control-group__title cm-profile-field "> <span class="hint" data-hasqtip="27"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://staging.isiwal.com/SunnysBespokeDev/design/themes/bsc/media/images/profile/sizing-shirt-length.jpg"> </span>
Shirt Length
</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-down" type="button">-</button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input data-undo="0.00" min="0.00"
max="200.00" data-step="0.5" type="text" id="elm_70" name="user_data[fields][70]" value="0.00" class="spinner ty-input-text   form-control" readonly="readyonly" data-shirt="shirt-length" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix"
style="display: none;"></span><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-up" type="button">+</button></span></div>
</div>
<div class="col-sm-12 hidden"> <label for="elm_89" class="ty-control-group__title cm-profile-field ">Sizing Fit</label>
<div class="btn-group bootstrap-select ty-profile-field__select" style="width: 100%;"><button type="button" class="btn dropdown-toggle bs-placeholder btn-default" data-toggle="dropdown" data-id="elm_89" title="--"><span class="filter-option pull-left">--</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button>
<div
class="dropdown-menu open">
<ul class="dropdown-menu inner" role="menu">
<li data-original-index="0" class="selected"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">--</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Body</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Structured</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Comfort</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Fitted</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="5"><a tabindex="0" class="hide" style="" data-tokens="null"><span class="text">Loose</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
</ul>
</div><select data-undo="" data-profile="sizing-fit" data-hide-disabled="true" data-type="select" data-width="100%" id="elm_89" class="ty-profile-field__select" name="user_data[fields][89]" tabindex="-98"> <option value="">--</option> <option value="40">Body</option> <option value="41">Structured</option> <option value="42">Comfort</option> <option value="43">Fitted</option> <option value="44" class="hide">Loose</option> </select></div>
<label for="elm_88" class="ty-control-group__title cm-profile-field ">Sizing Shape</label>
<div class="btn-group bootstrap-select ty-profile-field__select" style="width: 100%;"><button type="button" class="btn dropdown-toggle bs-placeholder btn-default" data-toggle="dropdown" data-id="elm_88" title="--"><span class="filter-option pull-left">--</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button>
<div
class="dropdown-menu open">
<ul class="dropdown-menu inner" role="menu">
<li data-original-index="0" class="selected"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">--</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Skinny</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Athletic</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Average</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="4"><a tabindex="0" class="hide" style="" data-tokens="null"><span class="text">Healthy</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
</ul>
</div><select data-undo="" data-profile="sizing-shape" data-hide-disabled="true" data-type="select" data-width="100%" id="elm_88" class="ty-profile-field__select" name="user_data[fields][88]" tabindex="-98"> <option value="">--</option> <option value="36">Skinny</option> <option value="37">Athletic</option> <option value="38">Average</option> <option value="39" class="hide">Healthy</option> </select></div>
<label for="elm_87" class="ty-control-group__title cm-profile-field ">
Sizing Weight
</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-down" type="button">-</button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input data-undo="0.00" min="0.00" max="200.00"
data-step="1" type="text" id="elm_87" name="user_data[fields][87]" value="0.00" class="spinner ty-input-text   form-control" readonly="readyonly" data-profile="sizing-weight" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix"
style="display: none;"></span><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-up" type="button">+</button></span></div> <label for="elm_86" class="ty-control-group__title cm-profile-field ">
Sizing Height
</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-down" type="button">-</button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input data-undo="0.00" min="0.00" max="200.00"
data-step="1" type="text" id="elm_86" name="user_data[fields][86]" value="0.00" class="spinner ty-input-text   form-control" readonly="readyonly" data-profile="sizing-height" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix"
style="display: none;"></span><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-up" type="button">+</button></span></div> <label for="elm_90" class="ty-control-group__title cm-profile-field ">Sloping Shoulder</label>
<div class="btn-group bootstrap-select ty-profile-field__select" style="width: 100%;"><button type="button" class="btn dropdown-toggle bs-placeholder btn-default" data-toggle="dropdown" data-id="elm_90" title="--"><span class="filter-option pull-left">--</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button>
<div
class="dropdown-menu open">
<ul class="dropdown-menu inner" role="menu">
<li data-original-index="0" class="selected"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">--</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">No</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Yes</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
<li data-original-index="3"><a tabindex="0" class="hide" style="" data-tokens="null"><span class="text">Don't Know</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
</ul>
</div><select data-undo="" data-profile="sloping-shoulder" data-hide-disabled="true" data-type="select" data-width="100%" id="elm_90" class="ty-profile-field__select" name="user_data[fields][90]" tabindex="-98"> <option value="">--</option> <option value="45">No</option> <option value="46">Yes</option> <option value="47" class="hide">Don't Know</option> </select></div>
</div>
</div>
<div class="modified">
<p><strong>Last updated on 16:25 Friday, 2nd Feb 2018 by ratan7139@gmail.com</strong></p> <input type="text" id="elm_49" name="user_data[fields][49]" value="16:25 Friday, 2nd Feb 2018 by ratan7139@gmail.com" class="hide ty-input-text  " readonly="readyonly"
data-profile="modified"> </div>
<div class="comments"> <textarea placeholder="Your profile comments." class="form-control " id="elm_48" name="user_data[fields][48]" cols="32" rows="1"></textarea> </div>
</div>
</div>
<div class="profile-sub-tab send-a-shirt" style="display: none;">
<div class="boxed tab-content">
<div class="row">
<div class="col-md-12">
<p>
If you've already found the perfect-fitting shirt, there's no need to reinvent the wheel!<br><br> Send us your shirt by mail - we'll use it to get your measurements, and send it back with your order. We'll then save those measurements
for future use.<br><br> <strong>Sending us a shirt?</strong><br> <label data-profile="send-a-shirt"> <input class="radio  elm_56" type="radio" id="elm_56_11" name="user_data[fields][56]" value="11"> <span class="radio">No</span> </label>                    <label data-profile="send-a-shirt"> <input class="radio  elm_56" type="radio" id="elm_56_12" name="user_data[fields][56]" value="12" checked="checked"> <span class="radio">Yes</span> </label> <br><br> <br> (Remember to mention your
<em>Order #, Sample Shirt and Not for Sale</em>)<br><br> <a target="blank" href="#" class="btn btn-bordered">View Address &amp; Print Label</a> </p>
</div>
</div>
</div>
</div>
<div class="modal fade page-modal" id="sizing-modal" role="dialog" tabindex="-1">
<div class="vertical-alignment-helper">
<div class="modal-dialog vertical-align-center">
<div class="modal-content">
<div class="modal-body">
<p>Please select a sizing option and fill out your measurements before moving on to the next step.</p>
</div>
<div class="modal-footer"> <button class="btn btn-primary" data-dismiss="modal" type="button">Go to Measurements</button> </div>
</div>
</div>
</div>
</div>
<div class="buttons-container panel panel-default" style="display: block;">
<div class="panel-body"> <button class="btn btn btn-primary pull-right" type="submit" name="">Continue</button> </div>
</div> <input type="hidden" name="security_hash" class="cm-no-hide-input" value="77ce89bceb41bd7be03f99b7a5d83005"></form>
</div>
<!--step_three-->
</div>
<div class="panel panel-default" data-ct-checkout="user_info" id="step_five">
<div class="panel-heading">
<h4> <span class="label label-default">4</span> Billing Options


</h4>
</div>
<div id="step_five_body" class="panel-body active ">
<div class="cm-tabs-content cm-j-content-disable-convertation tabs-content clearfix">
<div class=" tab-pane" id="content_payments_tab2">

<form name="payments_form_tab2" action="" method="post" class="payments-form cm-processed-form"> <input type="hidden" name="payment_id" value="22"> <input type="hidden" name="result_ids" value="checkout*,step_four"> <input type="hidden" name="dispatch" value="checkout.place_order">
<div
class="payments-list row">
<div class="col-md-12">
<ul class="list-group">
<!-- <li class="list-group-item active">
<div class="radio"> <label> <input required="" id="payment_22" class=" cm-select-payment" type="radio" name="payment_id" value="22" data-ca-url="checkout.checkout" data-ca-result-ids="checkout*,step_four" checked="checked"> <img class="   " id="det_img_22" src="<?php echo get_theme_file_uri('/images/thumbnails/paytm.png'); ?>" alt="" title=""> <strong>PayTM / Credit / Debit / Net Banking </strong> <span class="payments-list-description"> <span>Online Payment / Wallet</span> </span> </label>                                                                                    </div>
</li> -->
<li class="list-group-item ">
<div class="radio" > <label> <input required="" id="payment_19" class=" cm-select-payment" type="radio" name="payment_id" value="19" data-ca-url="checkout.checkout" data-ca-result-ids="checkout*,step_four" onclick="cartShow()"> <img class="   " id="det_img_19" src="<?php echo get_theme_file_uri('/images/thumbnails/visa-mastercard-netbanking_gf40-x5.png'); ?>"  alt="" title=""> <span class="payments-list-description"> <span>Online Payment</span> </span> </label>
</div>
</li>
<div id="showBillingForm">
<li class="list-group-item sub-item">
<div class="row credit-card" > <div class="col-lg-12 col-sm-12"> <div class="panel panel-default "> <!-- <div class="panel-body"> --> <div class="form-group"> 
<input size="35" type="text" id="cardHolderName" placeholder="Cardholder's name" name="cardHolderName" value="" class="cm-cc-name form-control" required /> </div> 
<div class="form-group">
<input size="35" type="text" id="cardNumber" name="cardNumber" placeholder="Card number" value="" class="form-control cm-focus cm-autocomplete-off" required /> </div> 
<div class="row"> <div class="col-md-4"> <div class="row"> <div class="col-md-6 no-padding-right"> <div class="form-group"> 
<input type="text" id="expiryMonth" placeholder="MM" name="expiryMonth" value="" size="2" maxlength="2" class="form-control" required /> </div> </div> 
<div class="col-md-6 no-padding-right"> 
<div class="form-group"> 
<input type="text" id="expiryYear" placeholder="YY" name="expiryYear" value="" size="2" maxlength="2" class="form-control" required /> 
</div> 
</div> 
</div> 
</div> 
<div class="col-md-8"> 
<label for="credit_card_cvv2_" class="cm-required hidden cm-integer cm-cc-cvv2 cm-autocomplete-off control-label">CVV/CVC
</label> 
<div class="form-group">
<input type="text" id="cvv" placeholder="CVV/CVC" name="cvv" value="" size="4" maxlength="4" class="form-control" required /> 
</div> 
<div class="dropdown hidden"> 
<button type="button" class="btn btn-link btn-xs" data-toggle="dropdown" id="cvv_info">What is CVV/CVC
</button> 
<div class="dropdown-menu" style="min-width:400px;" area-labelledby="cvv_info"> <div class="media"> <div class="media-left"> <img src="" alt="Visa" /> </div> <div class="media-body"> <h5>Visa, MasterCard, Discover</h5> <p>This number is printed in the signature area on the back of the card. It is the 3 digits AFTER the credit card number.</p> </div> </div> <div class="media"> <div class="media-left"> <img src="" alt="Visa" /> </div> <div class="media-body"> <h5>American Express</h5> <p>CVV is on the front of the card above the credit card number (either on the right or on the left side of the credit card).</p> </div> </div> </div> </div> </div> </div> <!-- </div> --> </div> </div> </div>
</li>
</div>
</ul>
</div>
<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12"> </div>
</div>
<div class="clearfix ">
<div class="form-group"> <label class="control-label hidden" for="customer_notes"></label> <textarea class="form-control cm-focus" id="customer_notes" name="customer_notes" cols="60" rows="1" placeholder="Any further instructions?"></textarea>                                                                </div>
</div>
<div class="clearfix ">
    <?php 
    $title = preg_replace('/\'/', '',$data['title']);
    ?>
<div class="control-group terms"> <label for="id_accept_termstab2" class="cm-check-agreement"><input type="checkbox" id="id_accept_termstab2" name="accept_terms" value="Y" class="cm-agreement checkbox" checked="">Select this check box to accept the <a href="#" target="_blank">Terms and Conditions</a>.</label>
<div class="hidden" id="terms_and_conditions_tab2"> </div>
</div>
</div> <input type="hidden" name="update_steps" value="1"> <button id="place_order_tab2" class="btn btn-primary cm-checkout-place-order" type="button" name="placeOrder" onclick="saveCartData('<?php echo $Uemail;?>','<?php echo $phone;?>','<?php echo $title;?>','<?php echo $sum;?>','<?php echo $fName;?>','<?php echo $lName;?>','<?php echo $address1;?>','<?php echo $city;?>','<?php echo $country;?>','<?php echo $state;?>','<?php echo $zip;?>')">Place my order</button>
<div class="processor-buttons hidden"></div> <input type="hidden" name="security_hash" class="cm-no-hide-input" value="77ce89bceb41bd7be03f99b7a5d83005">
</form>
<!--content_payments_tab2-->
</div>
</div>
</div>
<!--step_five-->
</div>
<div id="place_order_data" class="hidden"> </div>

<!-- </div>
</div>
</div> -->
</section>
<section class="col-lg-3 side-grid hidden-sm hidden-xs">
<aside class="sidebox order-summary">
<h5 class="text-uppercase "> 
<strong>
Order summary
</strong> 
</h5>
<div class="sidebox-body">
<div id="checkout_info_summary_760">
<table class="table table-condensed checkout-summary">
<tbody>
<tr>
<td><?php echo count($_SESSION['cart_item']);?> item(s)</td>
<td data-ct-checkout-summary="items" class="text-right"> <span>₹<span><?php echo $sum; ?></span></span>
</td>
</tr>
<tr>
<td>Shipping</td>
<td data-ct-checkout-summary="shipping" class="text-right"> <span>Free shipping</span> </td>
</tr>
<tr>
<td>Taxes <a rel="shadowbox;height=225;width=380;player=inline" href="#tax-implication">(?)</a></td>
<td></td>
</tr>
<tr>
<td data-ct-checkout-summary="tax-name GST">
<div>GST </div>
</td>
<td data-ct-checkout-summary="taxes" class="text-right"> <span>₹<span></span></span>
</td>
</tr>
<!-- <tr>
<td colspan="2">
<form class="cm-ajax cm-ajax-force cm-ajax-full-render cm-processed-form" name="coupon_code_form" action="https://staging.isiwal.com/SunnysBespokeDev/" method="post"> <input type="hidden" name="result_ids" value="checkout*,cart_status*,cart_items,payment-methods"> <input type="hidden" name="redirect_url" value="index.php?payment_id=22&amp;dispatch=checkout.checkout">
<div class="ty-gift-certificate-coupon ty-discount-coupon__control-group ty-input-append"> <label for="coupon_field" class="hidden cm-required">Promo code</label> <input type="text" class="ty-input-text cm-hint" id="coupon_field" name="hint_coupon_code" size="40" value="Enter Promo Code">                                                                <button title="Apply" class="ty-btn-go btn" type="submit">Apply</button> <input type="hidden" name="dispatch" value="checkout.apply_coupon"> </div> <input type="hidden" name="security_hash"
class="cm-no-hide-input" value="77ce89bceb41bd7be03f99b7a5d83005"></form>
<form class="cm-ajax cm-ajax-full-render cm-processed-form" name="point_payment_form" action="#"
method="post"> <input type="hidden" name="redirect_mode" value="checkout"> <input type="hidden" name="result_ids" value="checkout*,cart_status*">
<div class="ty-discount-coupon__control-group ty-reward-points__coupon ty-input-append ty-inline-block">
<strong style="margin-bottom: 5px;font-size:13.5px;">You have ₹250.00 in 250 points</strong> <input style="margin-top: 10px;" type="text" class="ty-input-text ty-valign cm-hint" name="hint_points_to_use"
size="40" placeholder="Enter Points"> <button title="Apply" class="ty-btn-go btn" type="submit">Apply</button> <input type="hidden" name="dispatch" value="checkout.point_payment">
<input
type="submit" class="hidden" name="dispatch[checkout.point_payment]" value=""> </div> <input type="hidden" name="security_hash" class="cm-no-hide-input" value="77ce89bceb41bd7be03f99b7a5d83005"></form>
<div class="ty-discount-info hidden"> </div>
</td>
</tr> -->
<tr>
<td data-ct-checkout-summary="order-total"> <strong>
Order Total
</strong> </td>
<td class="text-right"> <strong class="products-total">₹<span><?php echo $sum; ?></span></strong> </td>
</tr>
</tbody>
</table>
</div>
</div>
</aside>
<aside class="sidebox order-products">
<h5 class="text-uppercase "> <strong>
Products in your order
</strong> </h5>
<div class="sidebox-body">
<div id="checkout_info_products_761">
<ul class="list-group">
<li class="list-group-item">
<div class="media">
<div class="media-body">
<div class="media-heading"> <a href="#"><span class="product-name">White Poplin</span></a> </div>
<div><span class="product-quantity">1</span>&nbsp;x&nbsp;₹<span>1,960</span></div>
<div class="product-option"><span class="product-options-name">Customizations:&nbsp;</span><span class="product-options-content">Fabric: BB001A, Bottom Cut: Rounded, Sleeve: Full Sleeves, Collar: Spread Eagle, Collar Top Button: Single, Collar Stiffness: Stiff, Collar Contrast Type: None, Collar Piping Type: None, Cuff: Single Convertible, Cuff Shape: Rounded, Cuff Stiffness: Stiff, Cuff Contrast Type: None, Cuff Piping Type: None, Button: White, Button Thread Color: White, Pocket Style: None, Placket: Regular, Placket Button: Single, Placket Contrast Type: None, Placket Piping Type: None, Epaulette: None, Elbow Patch: None, Monogram: None, Back: None&nbsp;</span></div>
</div>
<div class="media-right"> <a class="btn btn-xs btn-link " href="#" data-ca-target-id="cart_status*"><i class="glyphicon bsc-cross-none"></i> </a>                                                        </div>
</div>
</li>
</ul>
</div>
</div>
</aside>
<div id="checkout_order_info_762_wrap">
<!-- Sidebox general wrapper start -->
<aside class="sidebox order-information">
<h5 class="text-uppercase "> <strong>
Order information
</strong> </h5>
<div class="sidebox-body">
<ul class="list-unstyled order-info-list">
<li> <strong>Billing address:</strong>
<div id="tygh_billing_adress" class=""> <span class="b-firstname">Ratan</span> <span class="b-lastname">Pandey</span> <span class="email">ratan7139@gmail.com</span> <span class="b-phone">7042071224</span> <span class="b-address">71</span>
<span
class="b-address-2">Khora</span> <span class="b-city">Noida</span> <span class="b-country">India</span> <span class="b-state">Uttar Pradesh</span> <span class="b-zipcode">201307</span> </div>
</li>
<li> <strong>Shipping address:</strong>
<div id="tygh_shipping_adress" class="list-unstyled"> <span class="s-firstname">Ratan</span> <span class="s-lastname">Pandey</span> <span class="s-phone">7042071224</span> <span class="s-address">71</span> <span class="s-address-2">Khora</span> <span class="s-city">Noida</span>                                                    <span class="s-country">India</span> <span class="s-state">Uttar Pradesh</span> <span class="s-zipcode">201307</span> </div>
</li>
<li> <strong>Shipping method:</strong>
<div id="tygh_shipping_method" class="list-unstyled">
<p>Domestic Shipping</p>
</div>
</li>
</ul>
</div>
</aside>
<!-- Sidebox general wrapper end -->
</div>
</section>
</div>
</div>
</section>

<!--footer-->
<?php get_footer(); ?>

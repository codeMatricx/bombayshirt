<?php /* Template Name: orders */ ?>
<!-- <HTML><meta http-equiv="content-type" content="text/html;charset=utf-8" />
<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=UTF-8"><META HTTP-EQUIV="Refresh" CONTENT="0; URL=index.php"><TITLE>Page has moved</TITLE>
</HEAD>
<BODY>
<A HREF="index.php"><h3>Click here...</h3></A>
</BODY>
</HTML> -->
<?php
get_header(); ?> 
		<section class="dispatch-orders-tracking_history content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-3">
                                <div class="sub-menu-block">
                                    <div class="wysiwyg-content">
                                        <div class="profile-sidebar">
                                            <ul>
                                                <li><a href="http://staging.isiwal.com/SunnysBespokeDev/profile">Profile Details</a></li>
                                                <li><a href="http://staging.isiwal.com/SunnysBespokeDev/tracking" >Order Tracking</a></li>
                                                <li><a href="" class="active">Order History</a></li>
                                                <li><a href="#">Loyalty Points History</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="col-lg-9">
                                <div class="main-content-block">
                                    <div class="text-center well">
                                        <div id="cart_items">
                                        <div class="table-responsive">
                                        <table class="cart-content table">
                                        <thead>
                                        <tr>
                                        <th class="cart-content-title text-left">Image</th>
                                        <th class="cart-content-title text-left">Title</th>
                                        <th class="cart-content-title text-right">Price</th>
                                        <th class="cart-content-title text-right">Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                             $user_id = get_current_user_id();
                                             //print_r($user_id);exit;
                                            $prod = $wpdb->get_results("SELECT o.product_id,os.status_name FROM wp_order AS o INNER JOIN wp_order_status AS os ON o.status=os.ID WHERE o.user_id='$user_id'");
                                            foreach ($prod as $row1) 
                                            {
                                               $productId = $row1->product_id;
                                               $statusName = $row1->status_name;
                                            ?>
                                        <tr>
                                            <?php 
                                                $product = $wpdb->get_row("SELECT p.ID,p.post_excerpt,p.guid,mt.meta_value AS sku,m.meta_value AS stock,p.post_title AS title,p.post_content AS description,pt.meta_value AS price,wt.meta_value AS weight FROM wp_posts p INNER JOIN wp_postmeta m ON (p.id = m.post_id)INNER JOIN wp_postmeta mt ON (p.id = mt.post_id)INNER JOIN wp_postmeta pt ON (p.id = pt.post_id)INNER JOIN wp_postmeta wt ON ( p.id = wt.post_id ) WHERE p.post_type = 'product'AND m.meta_key = '_stock'AND mt.meta_key = '_sku' AND pt.meta_key = '_price' AND wt.meta_key = '_weight' AND p.ID='$productId'");
                                                //print_r($product_image);exit;
                                                //$product->title
                                                $pId = $product->ID;
                                                $price = $product->price;
                                               
                                                $post_title = $product->title;
                                                 
                                                ?>

                                        <td class="cart-content-image-block">
                                        <div class="cart-content-image cm-reload-2525060006" id="product_image_update_2525060006">
                                        <?php 
                                        $sql_image = $wpdb->get_row("SELECT guid,post_content FROM wp_posts WHERE (post_parent='$productId')");
                                        $product_cart_image = $sql_image->guid;
                                        //$description = $sql_image->post_content;
                                        ?>
                                        <a href="#"> <img class="   " id="det_img_2525060006" src="<?php echo $product_cart_image; ?>"  alt="" title="" height="60px;" width="60px;" ></a> </div> 
                                         
                                        </td>
                                        <td class="cart-content-description"> <a href="#">White Satin</a>
                                        <div class="sku " id="sku_2525060006">
                                        <p><?php echo $post_title; ?></p> </div>
                                        <div id="options_2525060006" class="product-options group-block row">
                                        <div class="group-block-arrow"> <span class="caret-info"> <span class="caret-outer"></span><span class="caret-inner"></span> </span>
                                        </div>
                                        
                                        </div>
                                        </td>
                                        
                                        <td class="cart-content-price text-right cm-reload-2525060006" id="price_display_update_2525060006">
                                        ₹<span id="sec_product_price_2525060006"><?php echo $price; ?></span> </td>
                                        <td class="cart-content-price text-right cm-reload-2525060006" id="price_display_update_2525060006">
                                        <span id="sec_product_price_2525060006"><?php echo $statusName; ?></span> </td>
                                        

                                        </tr>
                                        <?php } ?>

                                        </tbody>
                                        </table>
                                        </div>
                                        </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
		
		
<!--footer-->
   <?php get_footer(); ?>		
<?php /* Template Name: invite-and-earn */ ?>
<?php get_header(); ?>
<?php
if (have_posts()):
  while (have_posts()) : the_post();
    the_content();
  endwhile;
else:
  echo '<p>Sorry, no posts matched your criteria.</p>';
endif;
?>
                <!-- <section class="dispatch-pages-view invite-and-earn-page content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="main-content-block">
                                    <div class="wysiwyg-content">
                                        <div>
                                            <div class="invite-and-earn">
                                                <div class="overlay row">
                                                    <div class="col-md-12">
                                                        <h1 class="heading">Invite & Earn</h1>
                                                        <hr class="heading-separator">
                                                        <h2 class="sub-heading"><strong>Never pay for a shirt again!</strong><br>Spread the word and earn cash off<br> your next order.</h2> </div>
                                                </div>
                                                <div class="three-split-wrapper">
                                                    <div class="three-split row">
                                                        <div class="col-md-4"> <span class="number">1</span>
                                                            <h2><strong>Share your Unique Link</strong></h2>
                                                            <p>
                                                                Share your Unique Link via Social Media, Email, Whatsapp, Blog or anywhere you think your friends might find it.
                                                            </p>
                                                        </div>
                                                        <div class="col-md-4"> <span class="number">2</span>
                                                            <h2><strong>Your Friend Gets Rs. 250 Off</strong></h2>
                                                            <p>
                                                                When your friend signs up using your Unique Link, they'll get Rs. 250 off their first purchase.
                                                            </p>
                                                        </div>
                                                        <div class="col-md-4"> <span class="number">3</span>
                                                            <h2><strong>Cash Off your Next Order</strong></h2>
                                                            <p>
                                                                You'll earn up to Rs. 1000 in Loyalty Points (Rs. 250 for each of the first 4 shirts they purchase). We'll credit your account once their order ships.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="refer-widget">
                                                    <div class="panel-body prompt">
                                                        <p class="notice"> <strong><a href="http://staging.isiwal.com/SunnysBespoke1/?page_id=113" target="_blank">Sign up</a></strong>, and earn your first 250 Loyalty Points instantly!
                                                            <br> Already have an account? <a class="sign-in-trigger" rel="nofollow"><strong>Sign in here</strong></a>.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section> -->
    <!--footer-->
<?php get_footer(); ?>  
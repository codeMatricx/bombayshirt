﻿        <?php /* Template Name: cart */ ?>
        <?php get_header(); 
        //session_start();
        ?>
        <!-- save data in cookies here -->


        <section class="dispatch-checkout-cart content">
        <div class="container-fluid  content-grid">
        <div class="row">
        <section class="col-lg-12 main-content-grid">
        <div class="mainbox-container clearfix">
        <div class="mainbox-body">
        <form name="checkout_form" class="cm-check-changes cm-processed-form" action="https://www.bombayshirts.com/" method="post" enctype="multipart/form-data"> <input type="hidden" name="redirect_mode" value="cart"> <input type="hidden" name="result_ids" value="cart_items,checkout_totals,checkout_steps,cart_status*,checkout_cart">
        <h2 class="mainbox-title">Shopping Cart</h2>
        <div class="panel panel-default cart-content-top-buttons hidden">
            <div class="panel-body">
                <div class="pull-left cart-content-left-buttons"> <a class="btn btn-default  " href="#"> Continue shopping</a> <a class="btn cm-confirm btn-default  " href="#"> Clear cart</a>                                                        </div>
                <div class="pull-right cart-content-right-buttons"> <button id="button_cart" class="btn btn-default" type="submit" name="dispatch[checkout.update]">Recalculate Totals</button> <a class="btn btn-primary  " href="#"> Checkout</a>                                                        </div>
            </div>
        </div>
        <div id="cart_items">
            <div class="table-responsive">
                <table class="cart-content table">
                    <thead>
                        <tr>
                            <th class="cart-content-title text-left">Product</th>
                            <th class="cart-content-title text-left">Title</th>
                            <th class="cart-content-title text-left">Description&nbsp;</th>
                            <th class="cart-content-title text-right">Unit price</th>
                            <th class="cart-content-title quantity-cell text-center" style="width:135px">Quantity</th>
                            <th class="cart-content-title text-right">Total price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $sum = 0;
                            //$items = $_SESSION["cart_item"];
                        foreach($_SESSION["cart_item"] as $k => $v)
                        {
                            $data['title'] = $_SESSION["cart_item"][$k]['name'];
                            $data['productId'] = $_SESSION["cart_item"][$k]['code'];
							$id = $_SESSION["cart_item"][$k]['code'];
                            $data['productQuantaty'] = $_SESSION["cart_item"][$k]['quantity'];
                            $data['price'] = $_SESSION["cart_item"][$k]['price'];
                            $product_id=$data['productId'];
                            $sum +=$data['price'];  
                             ?>
                        <tr>
                            
                            <td class="cart-content-image-block">
                                <div class="cart-content-image cm-reload-2525060006" id="product_image_update_2525060006">
                                    <?php 
                                    $sql_image = $wpdb->get_row("SELECT guid,post_content FROM wp_posts WHERE (post_parent='$product_id')");
                                    $product_cart_image = $sql_image->guid;
                                    //$description = $sql_image->post_content;
                                    ?>
                                    <a href="#"> <img class="   " id="det_img_2525060006" src="<?php echo $product_cart_image; ?>"  alt="" title="" height="60px;" width="60px;" ></a> </div> 
                                    <a class=" product-delete" href="#" data-ca-target-id="cart_items,checkout_totals,cart_status*,checkout_steps,checkout_cart"
                                    title="Remove" onclick="removecart(<?php echo $id ;?>)"> <i class="bsc-cross-none"></i> </a> 
                            </td>
                            <td class="cart-content-description"> <a href="#">White Satin</a>
                                <div class="sku " id="sku_2525060006">
                                    <p><?php echo $data['title']; ?></p> </div>
                                <div id="options_2525060006" class="product-options group-block row">
                                    <div class="group-block-arrow"> <span class="caret-info"> <span class="caret-outer"></span><span class="caret-inner"></span> </span>
                                    </div>
                                    <div class="cm-reload-2525060006" id="product_info_update_2525060006">
                                        <div class="ty-reward-points__product-info"> <strong class="ty-control-group__label">Price in points:</strong> <span class="ty-control-group__item" id="price_in_points_2525060006">2090 points</span> </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="sku " id="sku_2525060006">
                                    <?php 
                                    $sql_image = $wpdb->get_row("SELECT post_content FROM wp_posts WHERE (ID='$product_id')");
                                    $description = $sql_image->post_content;
                                    ?>
                                    <p><?php echo $description; ?></p> </div>
                            </td>
                            <td class="cart-content-price text-right cm-reload-2525060006" id="price_display_update_2525060006">
                                ₹<span id="sec_product_price_2525060006"><?php echo $data['price']; ?></span> </td>
                            <td class="cart-content-qty  text-center">
                                <div class="cart-content-quantity quantity-bar cm-reload-2525060006 changer" id="quantity_update_2525060006"> <input type="hidden" class="hidden" name="cart_products[2525060006][product_id]" value="8231"> <label class="hidden" for="amount_2525060006"></label>
                                    <div class="cm-value-changer input-group spinner">
                                    <span class="input-group-btn"> <a class="btn btn-default cm-decrease" onclick="decreaseValue()">−</a> </span> <input type="text" size="3" id="number" name="cart_products[2525060006][amount]" value="<?php echo $data['productQuantaty']; ?>"
                                            class="form-control cm-amount"> <span class="input-group-btn"> <a class="btn btn-default cm-increase" onclick="increaseValue()">+</a> </span> </div>
                                </div>
                            </td>
                            <td class="cart-content-price text-right cm-reload-2525060006" id="price_subtotal_update_2525060006"> <span class="price">₹</span><span id="sec_product_subtotal_2525060006" class="price"><?php echo $data['price']; ?></span> </td>
                            
                        </tr>
                        <?php } ?>
                        <!-- <tr>
                            <td class="cart-content-image-block">
                                <div class="cart-content-image cm-reload-1815652499" id="product_image_update_1815652499"> <a href="#"> <img class="   " id="det_img_1815652499" src="<?php echo get_theme_file_uri('/images/thumbnails/BB_001_A.jpg'); ?>"  alt="" title=""></a>                                                                        </div> <a class=" product-delete" href="#" data-ca-target-id="cart_items,checkout_totals,cart_status*,checkout_steps,checkout_cart"
                                    title="Remove"> <i class="bsc-cross-none"></i> </a> </td>
                            <td class="cart-content-description"> <a href="#">White Poplin</a>
                                <div class="sku " id="sku_1815652499">
                                    CODE: <span class="cm-reload-1815652499" id="product_code_update_1815652499">BB001A</span> </div>
                                <div class="cm-reload-1815652499 options" id="options_update_1815652499"> <input type="hidden" name="appearance[details_page]" value=""> <input type="hidden" name="additional_info[get_icon]" value="1"> <input type="hidden" name="additional_info[get_detailed]"
                                        value="1"> <input type="hidden" name="additional_info[get_additional]" value=""> <input type="hidden" name="additional_info[get_options]" value="1"> <input type="hidden" name="additional_info[get_discounts]"
                                        value=""> <input type="hidden" name="additional_info[get_features]" value=""> <input type="hidden" name="additional_info[get_extra]" value=""> <input type="hidden" name="additional_info[get_taxed_prices]"
                                        value="1"> <input type="hidden" name="additional_info[get_for_one_product]" value=""> <input type="hidden" name="additional_info[detailed_params]" value="1"> <input type="hidden"
                                        name="additional_info[features_display_on]" value="C"> <input type="hidden" name="cart_products[1815652499][object_id]" value="1815652499">
                                    <div class="row" id="option_1815652499_AOC">
                                        <div class="cm-picker-product-options product-options" id="opt_1815652499">
                                            <div class="form-group product-options-item product-list-field clearfix" id="opt_1815652499_248"> <label id="option_description_1815652499_248" for="option_1815652499_248" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
                                                <div class="col-lg-7 col-md-7 col-sm-7">
                                                <textarea readonly="" onfocus="this.blur();" id="option_1815652499_248" class="form-control " rows="3" name="cart_products[1815652499][product_options][248]">Fabric: BB001A, Bottom Cut: Rounded, Sleeve: Full Sleeves, Collar: Spread Eagle, Collar Top Button: Single, Collar Stiffness: Stiff, Collar Contrast Type: None, Collar Piping Type: None, Cuff: Single Convertible, Cuff Shape: Rounded, Cuff Stiffness: Stiff, Cuff Contrast Type: None, Cuff Piping Type: None, Button: White, Button Thread Color: White, Pocket Style: None, Placket: Regular, Placket Button: Single, Placket Contrast Type: None, Placket Piping Type: None, Epaulette: None, Elbow Patch: None, Monogram: None, Back: None</textarea>                                                                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="options_1815652499" class="product-options group-block row">
                                    <div class="group-block-arrow"> <span class="caret-info"> <span class="caret-outer"></span><span class="caret-inner"></span> </span>
                                    </div>
                                    <div class="cm-reload-1815652499" id="product_info_update_1815652499">
                                        <div class="ty-reward-points__product-info"> <strong class="ty-control-group__label">Price in points:</strong> <span class="ty-control-group__item" id="price_in_points_1815652499">1960 points</span> </div>
                                    </div>
                                </div>
                            </td>
                            <td class="cart-content-price text-right cm-reload-1815652499" id="price_display_update_1815652499">
                                ₹<span id="sec_product_price_1815652499">1,960</span> </td>
                            <td class="cart-content-qty  text-center">
                                <div class="cart-content-quantity quantity-bar cm-reload-1815652499 changer" id="quantity_update_1815652499"> <input type="hidden" class="hidden" name="cart_products[1815652499][product_id]" value="292"> <label class="hidden" for="amount_1815652499"></label>
                                    <div class="cm-value-changer input-group spinner">
                                <span class="input-group-btn"> <a class="btn btn-default cm-decrease" onclick="decreaseValue1()">−</a> </span> 
                                        <input type="text" size="3" id="number1" name="cart_products[1815652499][amount]" value="1" class="form-control cm-amount"> 
                                        <span class="input-group-btn"> 
                                        <a class="btn btn-default cm-increase" onclick="increaseValue1()">+</a> </span> </div>
                                </div>
                            </td>
                            <td class="cart-content-price text-right cm-reload-1815652499" id="price_subtotal_update_1815652499"> <span class="price">₹</span><span id="sec_product_subtotal_1815652499" class="price">1,960</span> </td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
        </div>
        </form>
        <div class="panel panel-default cart-total">
        <div class="panel-body cart-total-wrapper clearfix" id="checkout_totals">
            <div class="row">
                <div class="coupons-container col-lg-6">
                    <form class="cm-ajax cm-ajax-force cm-ajax-full-render cm-processed-form" name="coupon_code_form" action="#" method="post"> <input type="hidden" name="result_ids" value="checkout*,cart_status*,cart_items,payment-methods"> <input type="hidden" name="redirect_url" value="index.php?dispatch=checkout.cart">
                        <div class="ty-gift-certificate-coupon ty-discount-coupon__control-group ty-input-append">
                        <label for="coupon_field" class="hidden cm-required">Promo code</label> <input type="text" class="ty-input-text cm-hint" id="coupon_field" name="hint_coupon_code" size="40" value="Enter Promo Code">                                                                <button title="Apply" class="ty-btn-go btn" type="submit">Apply</button> <input type="hidden" name="dispatch" value="checkout.apply_coupon"> </div>
                    </form>
                </div>
                <div class="col-lg-4 hidden"> </div>
                <div class="col-lg-6 text-right">
                    <dl class="cart-statistic dl-horizontal"> <dt class="subtotal">Subtotal</dt>
                        <dd class="subtotal">₹<span><?php echo $sum; ?></span></dd> <dt class="item">Sub Total</dt>
                        <dd class="item">₹<span id="sec_cart_total"><?php echo $sum; ?></span></dd>
                    </dl>
                </div>
            </div>
        </div>
        </div>
        <div class="panel panel-default cart-content-bottom-buttons clearfix">
        <div class="panel-body">
            <div class="pull-left cart-content-left-buttons"> <a class="btn btn-default  " href="http://staging.isiwal.com/SunnysBespokeDev/"> Continue shopping</a> </div>
            <div class="pull-right cart-content-right-buttons"> <a class="btn btn-default cm-external-click  " data-ca-external-click-id="button_cart"> Recalculate Totals</a> <a class="btn btn-primary  " href="http://staging.isiwal.com/SunnysBespokeDev/checkout"> Checkout</a> </div>
        </div>
        </div>
        </div>
        </div>
        </section>
        </div>
        </div>
        </section>
        <script>
        function increaseValue() {
        var value = parseInt(document.getElementById('number').value, 10);
        value = isNaN(value) ? 0 : value;
        value++;
        document.getElementById('number').value = value;
        }

        function decreaseValue() {
        var value = parseInt(document.getElementById('number').value, 10);
        value = isNaN(value) ? 0 : value;
        value < 2 ? value = 2 : '';
        value--;
        document.getElementById('number').value = value;
        }
        function increaseValue1() {
        var value = parseInt(document.getElementById('number1').value, 10);
        value = isNaN(value) ? 0 : value;
        value++;
        document.getElementById('number1').value = value;
        }

        function decreaseValue1() {
        var value = parseInt(document.getElementById('number1').value, 10);
        value = isNaN(value) ? 0 : value;
        value < 2 ? value = 2 : '';
        value--;
        document.getElementById('number1').value = value;
        }
        </script>

        <!--footer-->
        <?php get_footer(); ?> 
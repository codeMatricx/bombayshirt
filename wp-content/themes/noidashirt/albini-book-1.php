<?php /* Template Name: Albini book */ ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Founded in 1876 in Albino, Italy, family-run Cotonificio Albini is world renowned as one of the very finest shirting mills. Throughout the company's incredible history, one fact has remained constant - their passion to deliver the most premium and luxurious shirting fabrics! The use of the best raw materials, continuous stylistic research, the Italian taste for fashion and the passion for quality. The brand possesses an impressive array of shirt fabrics, becoming a major player for shirting fabrics in all three major markets: high end bespoke, made to measure and ready to wear. The current line of products incorporates a large quality range starting with sporty, casual and denim fabrics, seasonal lines, all the way up to the most expensive fabrics. As a part of the Albini Group with other fabric producers like Thomas Mason and David & John Anderson, and a collection of more than 20,000 fabric variations, the company is able to satisfy the most demanding customers in more than 80 countries of the world.">
<meta name="keywords" content="cotonificio albini,">
<meta name="author" content="Noida Shirt Company" />
<meta name="copyright" content="&copy; 2012-2015 Noida Shirt Company" />
<title>Fabric Gallery | Noida Shirt Company</title>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css '?>">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css '?>">
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300|Karla:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css '?>">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/css/drawer.min.css '?>">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/css/style1bce.css?v=6 '?>">
</head>
<body class="home">
<header class="container">
<div class="row">
<div class="col-xs-12 text-center">
<a href="../index.php" class="logo">
<img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/bsc-logo.jpg'); ?>" alt="" />
</a>
<h1>Premium&nbsp;&nbsp;&nbsp;Fabric&nbsp;&nbsp;&nbsp;Collection</h1>
</div>
</div>
</header>
<div class="wrapper clearfix">
<div class="container">
<div class="row">
<div class="col-xs-8 col-xs-offset-2">
<div class="gallery">
<img src="<?php echo get_theme_file_uri('/images/albini-book-1/book-1-cotonificio-albini-premium-italian-fabrics-at-bombay-shirt-company-custom-made-shirts-for-men-an-women.jpg'); ?>" data-title="Cotonificio Albini Book 1" />
<img src="<?php echo get_theme_file_uri('/images/albini-book-1/book-1-page-1-solid-color-fabrics-cotonificio-albini-premium-italian-fabrics-at-bombay-shirt-company-custom-made-shirts-for-men-an-women.jpg'); ?>" data-title="Cotonificio Albini Book 1" />
<img src="<?php echo get_theme_file_uri('/images/albini-book-1/book-1-page-2-solid-color-and-stripes-fabrics-cotonificio-albini-premium-italian-fabrics-at-bombay-shirt-company-custom-made-shirts-for-men-an-women.jpg'); ?>" data-title="Cotonificio Albini Book 1" />
<img src="<?php echo get_theme_file_uri('/images/albini-book-1/book-1-page-3-stripes-fabrics-cotonificio-albini-premium-italian-fabrics-at-bombay-shirt-company-custom-made-shirts-for-men-an-women.jpg'); ?>" data-title="Cotonificio Albini Book 1" />
<img src="<?php echo get_theme_file_uri('/images/albini-book-1/book-1-page-4-solid-colors-stripes-and-print-fabrics-cotonificio-albini-premium-italian-fabrics-at-bombay-shirt-company-custom-made-shirts-for-men-an-.jpg'); ?>" data-title="Cotonificio Albini Book 1" />
<img src="<?php echo get_theme_file_uri('/images/albini-book-1/book-1-page-5-stripes-fabrics-cotonificio-albini-premium-italian-fabrics-at-bombay-shirt-company-custom-made-shirts-for-men-an-women.jpg'); ?>" data-title="Cotonificio Albini Book 1" />
</div>
</div>
</div>
</div>
</div>
<script src="<?php echo get_stylesheet_directory_uri(). '/ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js '?>"></script>
<script src="<?php echo get_stylesheet_directory_uri(). '/code.jquery.com/jquery-migrate-1.2.1.min.js '?>"></script>
<script src="<?php echo get_stylesheet_directory_uri(). '/maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js '?>"></script>
<script src="<?php echo get_stylesheet_directory_uri(). '/galleria/galleria-1.4.2.min.js '?>"></script>
<script src="<?php echo get_stylesheet_directory_uri(). '/galleria/themes/twelve/galleria.twelve.min.js '?>"></script>
<script src="<?php echo get_stylesheet_directory_uri(). '/js/bsc8e0e.js '?>"></script>
<script src="<?php echo get_stylesheet_directory_uri(). '/js/script5e1f.js '?>"></script>
</body>
</html>
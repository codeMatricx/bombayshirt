<!DOCTYPE html>
<html lang="en" dir="ltr">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <title>Design Your Own Shirt | Online Shirt Designer - Noidashirts.com</title>
    <base />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" data-ca-mode="full" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="Design your own custom made shirts from a wide range of finest shirt fabrics, with the online shirt designer. " />
    <meta name="keywords" content="custom made shirts, tailored shirts, custom shirts, bespoke shirts, tailor made shirts, mens custom shirt online, custom tailored shirts, made to measure shirts online, custom dress shirts online" />
    <link rel="stylesheet" href="../maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Karla:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='../cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.10/css/perfect-scrollbar.min.css' rel='stylesheet' type='text/css'>
    <link href='../cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css' rel='stylesheet' type='text/css'>
    <link href='../cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css' rel='stylesheet' type='text/css'>
    <link href='../cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/tooltipster.min.css' rel='stylesheet' type='text/css'>
    <link type="text/css" rel="stylesheet" href="../var/cache/misc/assets/design/themes/bsc/css/standalone.2ab25c338024078a2fd2d68832366f771515504583.css" />
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/es5-shim/4.1.9/es5-shim.min.js"></script><![endif]-->
</head>

<body class="dispatch-products-view drawer drawer--left custom_shirt_template">
    <header class="drawer" role="banner">
        <nav class="drawer-nav" role="navigation">
            <div>
                <ul class="drawer-menu">
                    <li> <a class="drawer-menu-item" href="shirt.php">Shop Online</a>
                        <ul>
                            <li> <a href="shirt.php">Design CUSTOM MADE Shirt</a> </li>
                        </ul>
                        <ul>
                            <li> <a href="../shop-mens-shirts-online.php">SHOP Men's Collection</a> </li>
                        </ul>
                        <ul>
                            <li> <a href="../gift-certificates/index.php">SHOP Gift Voucher</a> </li>
                        </ul>
                    </li>
                    <li> <a class="drawer-menu-item" href="../book-home-visit/index.php">Book Home Visit</a> </li>
                    <li> <a class="drawer-menu-item" href="../store-locations.php">Stores</a> </li>
                    <li> <a class="drawer-menu-item" href="../about-us/index.php">About</a>
                        <ul>
                            <li class="first"> <a href="../about-us/index.php">About Us</a> </li>
                            <li class=""> <a href="../loyalty-program.php">Loyalty Program</a> </li>
                            <li class=""> <a href="../press-in-the-news/index.php">Press</a> </li>
                            <li class=""> <a href="../how-to-select-a-custom-shirt-fabric/index.php">Fabrics</a> </li>
                            <li class=""> <a href="../parts-of-shirt-anatomy-of-shirt/index.php">Shirt Construction</a> </li>
                            <li class="last"> <a href="../how-to-measure-shirt-size/index.php">Sizing &amp; Fit</a> </li>
                        </ul>
                    </li>
                    <li class=""><a class="drawer-menu-item" href="../invite-and-earn28f3.php?source=main-menu">Refer</a></li>
                    <li class=""><a class="drawer-menu-item" href="../help-and-support/index.php">Help</a>
                        <ul>
                            <li class="first"> <a href="../help-and-support/index.php">Contact &amp; FAQ's</a> </li>
                            <li class=""> <a href="../shirt-alterations/index.php">Alterations &amp; Remakes</a> </li>
                        </ul>
                    </li>
                    <li class=""><a class="drawer-menu-item" href="../blog/index.php">Blog</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <main role="main">
        <input class="local-coefficient" type="hidden" value="1.00000" />
        <input class="local-symbol" type="hidden" value="₹" />
        <div class="page" id="tygh_container">
            <div id="ajax_overlay" class="ajax-overlay"></div>
            <div id="ajax_loading_box" class="ajax-loading-box"></div>
            <div class="cm-notification-container notification-container"> </div>
            <main class="page-container" id="tygh_main_container" role="main">
                <section class="top-panel">
                    <div class="container-fluid  ">
                        <div class="row">
                            <section class="col-lg-3 top-links-grid col-xs-8 col-sm-6 col-md-3">
                                <div class="logo-block pull-left">
                                    <a href="../index.php" title="" class="logo hidden-xs hidden-sm"> <img src="../design/themes/bsc/media/images/bsc-logo.jpg" alt="Noida Shirt Company" class="full" /> <img src="../design/themes/bsc/media/images/bsc-logo-horizontal.jpg" alt="Noida Shirt Company" class="horizontal" /> </a>
                                    <button type="button" class="drawer-toggle drawer-hamburger hidden-md hidden-lg"> <span class="sr-only">toggle navigation</span> <span class="drawer-hamburger-icon"></span> </button>
                                    <a href="../index.php" title="" class="logo-horizontal hidden-md hidden-lg"> <img src="../design/themes/bsc/media/images/bsc-logo-horizontal.jpg" alt="Noida Shirt Company" /> </a>
                                </div>
                            </section>
                            <section class="col-lg-9 col-xs-4 col-sm-6 col-md-9">
                                <div class="row">
                                    <section class="col-lg-12 cart-and-account-grid">
                                        <div class="cart-content-block top-cart-content pull-right">
                                            <div class="dropdown cart-dropdown" id="cart_status_737">
                                                <button class="btn btn-default dropdown-toggle" type="button" id="cart_dropdown_737" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="glyphicon glyphicon-shopping-cart fa fa-shopping-cart empty"></i> </button>
                                                <ul aria-labelledby="cart_dropdown_737" class="cm-cart-content dropdown-menu dropdown-menu-right cm-cart-content-thumb cm-cart-content-delete">
                                                    <li>
                                                        <div class="buttons text-center text-muted"> <span>Cart is empty</span> </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="sizing-and-checkout-tip-block pull-right">
                                            <div class="wysiwyg-content">
                                                <div><a class="checkout-tip" href="../cart/index.php">Add Measurements at Checkout <i class="fa fa-angle-right" aria-hidden="true"></i></a> </div>
                                            </div>
                                        </div>
                                        <div class="my-account-block top-my-account pull-right">
                                            <div class="dropdown account-dropdown">
                                                <button class="btn btn-default dropdown-toggle account-tooltip-trigger" type="button" id="my_account_738" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="glyphicon glyphicon-user fa fa-user hidden-md hidden-lg"></i><span class="hidden-xs hidden-sm">Sign in / Register</span> </button>
                                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="my_account_738" id="account_info_738">
                                                    <li>
                                                        <div class="buttons"> <a href="../login/indexa838.php?return_url=index.php%3Fdispatch%3Dproducts.view%26product_id%3D1517" data-ca-target-id="login_block738" class="cm-dialog-opener btn-borderless cm-dialog-auto-size btn btn-default btn-sm" rel="nofollow">Sign in</a> <a href="../profiles-add/index.php" rel="nofollow" class="btn btn-sm btn-borderless">Register</a>
                                                            <div id="login_block738" class="hidden" title="Sign in">
                                                                <div>
                                                                    <form name="popup738_form" action="#" method="post" class="login-form">
                                                                        <input type="hidden" name="return_url" value="index.php?dispatch=products.view&amp;product_id=1517" />
                                                                        <input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517" />
                                                                        <div class="form-group">
                                                                            <label for="login_popup738" class="cm-required cm-trim cm-email control-label">Email</label>
                                                                            <input type="email" id="login_popup738" name="user_login" size="30" value="" class="cm-focus form-control" placeholder="Email" /> </div>
                                                                        <div class="form-group last clearfix">
                                                                            <label for="psw_popup738" class="cm-required control-label">Password</label>
                                                                            <input type="password" id="psw_popup738" name="password" size="30" value="" class="form-control" maxlength="32" placeholder="Password" /> <a href="../indexf69f.html?dispatch=auth.recover_password" class="forgot-password radio pull-left" tabindex="5">Forgot your password?</a>
                                                                            <div class="checkbox pull-right remember-me">
                                                                                <label for="remember_me_popup738">
                                                                                    <input type="checkbox" name="remember_me" id="remember_me_popup738" value="Y" />Remember me</label>
                                                                            </div>
                                                                        </div> <span></span>
                                                                        <div class="buttons-container clearfix">
                                                                            <div class="pull-right">
                                                                                <button id="popup738" class="btn btn-primary" type="submit" name="dispatch[auth.login]">Sign in</button>
                                                                            </div>
                                                                            <div class="form-group pull-left"> <a class="btn btn-success" href="../profiles-add/index.html" rel="nofollow">Register</a> </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col-lg-12 main-menu-grid">
                                        <div class="top-menu-block pull-left">
                                            <div class="top-menu hidden-xs hidden-sm">
                                                <ul>
                                                    <li><a href="#">Shop Online</a>
                                                        <ul>
                                                            <li class="first"> <a href="shirt.php">
																Design a <br />CUSTOM MADE Shirt
																</a> </li>
															<li> <a href="../shop-mens-shirts-online.php">
																SHOP Men's Collection
																</a> </li>
                                                            <li> <a href="../gift-certificates/index.php">Shop Gift Voucher</a> </li>
                                                            <li class="last"> <a target="_blank" href="../premium-shirt-fabrics-collection/index.php">VIEW PREMIUM CATALOG</a> </li>
                                                        </ul>
                                                    </li>
                                                    <li class=""> <a href="../book-home-visit/index.php">Book Home Visit</a> </li>
                                                    <li class=""><a href="../store-locations/index.php">Stores</a></li>
                                                    <li class=""><a href="#">About</a>
                                                        <ul>
                                                            <li class="first"> <a href="../about-us/index.php">About Us</a> </li>
                                                            <li class="united-states-hidden"> <a href="../loyalty-program.php">Loyalty Program</a> </li>
                                                            <li class=""> <a href="../press-in-the-news/index.php">Press</a> </li>
                                                            <li class="last"> <a href="../how-to-select-a-custom-shirt-fabric/index.php">Fabrics</a> <a href="../how-to-design-shirt-collar-cuffs-and-more/index.php" class="hidden">Collars, Cuffs &amp; More</a> <a href="../parts-of-shirt-anatomy-of-shirt/index.php">Shirt Construction</a> <a href="../how-to-measure-shirt-size/index.php">Sizing &amp; Fit</a> </li>
                                                        </ul>
                                                    </li>
                                                    <li class=""><a href="#">Help</a>
                                                        <ul>
                                                            <li class="first"> <a href="../help-and-support/index.php">Contact &amp; FAQ's</a> </li>
                                                            <li class="last"> <a href="../shirt-alterations/index.php">Alterations</a> </li>
                                                        </ul>
                                                    </li>
                                                    <li class="international-hidden"><a href="../invite-and-earn28f3.php?source=main-menu" style="color:#bb8b7d;">Refer</a></li>
                                                    <li class="hide"><a href="../blog/index.php">Blog</a></li>
                                                </ul>
                                                <ul class="social-links list-unstyled hide">
                                                    <li><a target="_blank" href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a target="_blank" href="#"><i class="fa fa-instagram"></i></a></li>
                                                    <li><a target="_blank" href="#"><i class="fa fa-youtube-play"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
                <section class="content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 breadcrumbs-grid">
                                <div class="breadcrumbs-block">
                                    <div id="breadcrumbs_10">
                                        <ol class="breadcrumb">
                                            <li><a href="../index.php" class="">Home</a></li>
                                            <li><a href="index.php" class="">Online Custom</a></li>
                                            <li class="active">Shirt</li>
                                        </ol>
                                        <div class="product-switcher"> <a class="switcher-icon left " href="rocky-twill-en-4/index.php" title="Prev"><i class="icon-left-circle"></i></a> <span class="switcher-selected-product">270</span> <span>of</span> <span class="switcher-total">325</span> <a class="switcher-icon right " href="shoe-print-en-4/index.php" title="Next"><i class="icon-right-circle"></i></a> </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="main-content-block">
                                    <div class="product-block custom-product shirt">
                                        <div itemscope itemtype="#">
                                            <meta itemprop="sku" content="CUSTOM" />
                                            <meta itemprop="name" content="Shirt" />
                                            <meta itemprop="description" content="" />
                                            <div itemprop="offers" itemscope="" itemtype="#">
                                                <link itemprop="availability" href="#" />
                                                <meta itemprop="priceCurrency" content="INR" />
                                                <meta itemprop="price" content="10000" /> </div>
                                        </div>
                                        <div class="cart-options-menu hidden-md hidden-lg">
                                            <div class="product-quantity qty clearfix changer">
                                                <div class="center valign value-changer cm-value-changer"> 
													<a class="cm-decrease decrease">−</a>
														<input type="text" data-ca-min-qty="1" value="1" class="input-text-short cm-amount" size="5"> 
													<a class="cm-increase increase">+</a> 
												</div>
                                            </div>
                                            <button class="add-to-cart"> <i class="glyphicon glyphicon-shopping-cart"></i> </button>
                                        </div>
                                        <div class="customization-menu">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a class="fabric active" data-customization-group="fabric" data-mode="full" data-customization="fabric"> <span> <i class="bsc-customization-fabrics"></i> <em class="hidden"></em> </span> <strong class="customization-title">Fabric</strong> <em class="value set"><span>PC045</span></em> </a>
                                                    <a class="collar" data-customization-group="collar" data-mode="full" data-customization="collar"> <span> <i class="bsc-customization-collars"></i> <em class="hidden"></em> </span> <strong class="customization-title">Collar</strong> <em class="value set"><span>Spread Eagle</span></em> </a>
                                                    <a class="cuff div" data-customization-group="cuff" data-mode="full" data-customization="cuff" data-deselect="false"> <span> <i class="bsc-customization-cuffs"></i> <em class="hidden"></em> </span> <strong class="customization-title">Cuff</strong> <em class="value set"><span>Single Convertible</span></em> </a>
                                                    <a class="monogram" data-customization-group="monogram" data-mode="full" data-customization="monogram"> <span> <i class="bsc-customization-monogram"></i> <em class="hidden"></em> </span> <strong class="customization-title">Monogram</strong> <em class="value set"><span>None</span></em> </a>
                                                    <a class="placket" data-customization-group="placket" data-mode="full" data-customization="placket"> <span> <i class="bsc-customization-placket"></i> <em class="hidden"></em> </span> <strong class="customization-title">Placket</strong> <em class="value set"><span>Regular</span></em> </a>
                                                    <a class="shoulder-epaulette" data-customization-group="advanced" data-mode="full" data-customization="advanced" data-deselect="false"> <span> <i class="bsc-advanced-options"></i> <em class="hidden"></em> </span> <strong class="customization-title">Advanced</strong> <em class="value set"><span>&nbsp;</span></em> </a>
                                                    <div class="cart-options-menu hidden-xs hidden-sm">
                                                        <div class="product-quantity qty clearfix changer">
                                                            <div class="center valign value-changer cm-value-changer"> 
															<a class="cm-decrease decrease" id="decrease" onclick="decreaseValue()">−</a>
                                                               <input type="text" data-ca-min-qty="1" value="1" id="number" class="input-text-short cm-amount" size="5"> 
															<a class="cm-increase increase" id="increase" onclick="increaseValue()">+</a> </div>
                                                        </div>
                                                        <button class="add-to-cart">
                                                            Add to Cart <i class="fa fa-angle-right"></i> </button> <span class="trial-shirt-tip">Trial shirt <a href="#trial-shirt" rel="shadowbox;height=440;width=650;player=inline">(?)</a></span> 
													</div>
                                                </div>
                                            </div>
                                            <a class="customization-help hidden-xs hidden-sm"> <i class="fa fa-question"></i> <em>Need Help?</em> </a>
                                        </div>
                                        <!--Shirt Custom design start-->
										<div class="customization-options active ps-container ps-theme-default ps-active-y" data-customization-group="fabric" data-plugin="scrollbar" data-ps-id="b934588e-90e1-1dd2-ab4b-5b4fbd1f310b"> <a class="close hidden-lg hidden-md"><i class="fa fa-angle-left"></i><span class="hidden-md hidden-lg">View Shirt</span></a> <a class="secondary-toggle"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Filters</a>
											<div class="row">
												<div class="col-md-12">
													<div class="fabrics clearfix" data-customization="fabric" data-readable-customization="Fabric" data-menu-set="true" data-filterer="true" data-toggle="true">
														<div class="row filter-wrapper" style="display:none;">
															<div class="col-md-12">
																<div class="filter-set">
																	<h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> <span class="filter-title">Color</span></h1>
																	<div class="options accordion-panel" style="display:none;">
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="6" data-ca-filter-id="2">Blacks <span class="dot black"></span></label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="2" data-ca-filter-id="2">Blues <span class="dot blue"></span></label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="3" data-ca-filter-id="2">Browns <span class="dot brown"></span></label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="7" data-ca-filter-id="2">Greens <span class="dot green"></span></label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="42" data-ca-filter-id="2">Greys <span class="dot grey"></span></label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="43" data-ca-filter-id="2">Khakhis <span class="dot khakhi"></span></label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="44" data-ca-filter-id="2">Oranges <span class="dot orange"></span></label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="45" data-ca-filter-id="2">Peach <span class="dot peach"></span></label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="46" data-ca-filter-id="2">Pinks <span class="dot pink"></span></label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="47" data-ca-filter-id="2">Purples <span class="dot purple"></span></label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="48" data-ca-filter-id="2">Reds <span class="dot red"></span></label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="49" data-ca-filter-id="2">Whites <span class="dot white"></span></label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="50" data-ca-filter-id="2">Yellows <span class="dot yellow"></span></label>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-12">
																<div class="filter-set">
																	<h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> <span class="filter-title">Pattern</span></h1>
																	<div class="options accordion-panel" style="display:none;">
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="30" data-ca-filter-id="3">Checks</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="31" data-ca-filter-id="3">Stripes</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="32" data-ca-filter-id="3">Solids</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="33" data-ca-filter-id="3">Prints</label>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-12">
																<div class="filter-set">
																	<h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> <span class="filter-title">Weave</span></h1>
																	<div class="options accordion-panel" style="display:none;">
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="19" data-ca-filter-id="5">Chambray</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="23" data-ca-filter-id="5">Crepe Satin</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="21" data-ca-filter-id="5">Denim</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="13" data-ca-filter-id="5">Dobby</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="20" data-ca-filter-id="5">Fil-a-fil</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="22" data-ca-filter-id="5">Georgette</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="15" data-ca-filter-id="5">Herringbone</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="16" data-ca-filter-id="5">Houndstooth</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="25" data-ca-filter-id="5">Jacquard</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="18" data-ca-filter-id="5">Linen</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="11" data-ca-filter-id="5">Oxford</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="10" data-ca-filter-id="5">Poplin</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="17" data-ca-filter-id="5">Royal Oxford</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="12" data-ca-filter-id="5">Satin</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="24" data-ca-filter-id="5">Stretch Dobby</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input class="cm-product-filters-checkbox" type="checkbox" value="14" data-ca-filter-id="5">Twill</label>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-12 advanced-set" style="display:none;">
																<div class="filter-set">
																	<h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> <span class="filter-title">Composition</span></h1>
																	<div class="options accordion-panel" style="display:none;">
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="26" data-ca-filter-id="6">100% Cotton</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="27" data-ca-filter-id="6">100% Egyptian Cotton</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="28" data-ca-filter-id="6">100% Linen</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="29" data-ca-filter-id="6">55% Cotton 45% Lycra</label>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-12 advanced-set" style="display:none;">
																<div class="filter-set">
																	<h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> <span class="filter-title">Ply</span></h1>
																	<div class="options accordion-panel" style="display:none;">
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="8" data-ca-filter-id="7">Single Ply</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="9" data-ca-filter-id="7">Double Ply</label>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-12 advanced-set" style="display:none;">
																<div class="filter-set">
																	<h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> <span class="filter-title">Occasion</span></h1>
																	<div class="options accordion-panel" style="display:none;">
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="5" data-ca-filter-id="1">Formal</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="4" data-ca-filter-id="1">Casual</label>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-12 advanced-set" style="display:none;">
																<div class="filter-set">
																	<h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> <span class="filter-title">Weight</span></h1>
																	<div class="options accordion-panel" style="display:none;">
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="38" data-ca-filter-id="8">Light</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="39" data-ca-filter-id="8">Medium</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="40" data-ca-filter-id="8">Heavy</label>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-12 price-range advanced-set" style="display:none;">
																<div class="filter-set">
																	<h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> <span class="filter-title">Price Range</span></h1>
																	<div class="options accordion-panel" style="display:none;">
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="37" data-ca-filter-id="4">1000 - 1900</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="34" data-ca-filter-id="4">1900 - 2400</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="35" data-ca-filter-id="4">2400 - 3700</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="36" data-ca-filter-id="4">3700 - 5100</label>
																		</a>
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="62" data-ca-filter-id="4">5100+</label>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-12">
																<div class="filter-set">
																	<h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> <span class="filter-title">Latest Fabrics</span></h1>
																	<div class="options accordion-panel" style="display:none;">
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value="Y" data-ca-filter-id="9">Show New Fabrics</label>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-12 hidden advanced-set" style="display:none;">
																<div class="filter-set">
																	<h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> <span class="filter-title">Sale</span></h1>
																	<div class="options accordion-panel" style="display:none;">
																		<a class="checkbox">
																			<label>
																				<input type="checkbox" class="cm-product-filters-checkbox" value=".sale" data-ca-filter-id="13">Sale</label>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-12">
																<a class="advanced-filters"> <span>Advanced</span><span style="display:none;">Basic</span> Filters <i class="fa fa-angle-up" style="display:none;"></i><i class="fa fa-angle-down"></i> </a>
															</div>
															<div class="col-md-12"> <a class="clear-all"><i class="fa fa-trash-o"></i> Clear All Filters</a> </div>
														</div>
														<a class="filterer cm-ajax" href="#" data-url="#" data-ca-scroll=".fabrics" data-ca-target-id="fabric-filterer-wrapper" data-ca-page="1"></a>
														<div id="fabric-filterer-wrapper">
															<div class="pagination-container cm-pagination-container" id="pagination_contents">
																<div>
																	<a data-ca-scroll=".cm-pagination-container" href="" data-ca-page="" data-ca-target-id="pagination_contents" class="hidden"></a>
																</div>
																<a href="#" class="option new active" data-price="3340.000000" data-id="8032" data-value="Nile 1 9970-100" data-readable-value="Nile 1 9970-100">
																	<div class="grid-hover hidden-xs hidden-sm" data-cloudzoom="zoomImage: &quot;../images/thumbnails/300/380/detailed/39/H2A76kJ_v941-qm.jpg&quot;" src="../images/thumbnails/300/380/detailed/39/H2A76kJ_v941-qm.jpg" style="user-select: none;">
																		<div class="vertical-align"> <span class="product-title">Monti horizontal stripes</span>
																			<div class="grid-list-price"> <span class="cm-reload-8032" id="old_price_update_8032"> <!--old_price_update_8032--></span> <span class="cm-reload-8032 price-update" id="price_update_8032"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8032"><span class="price-num">₹</span><span id="sec_discounted_price_8032" class="price-num">3,340</span></span>
																				<!--price_update_8032-->
																				</span> <span class="cm-reload-8032" id="line_discount_update_8032"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8032--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2A76kJ_v941-qm.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2A76kJ_v941-qm.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_8032" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[8032][product_id]" value="8032">
																				<input type="hidden" class="product-sku-code" value="Nile 1 9970-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti horizontal stripes</h1>
																							<p class="product-sku">Nile 1 9970-100</p>
																							<div class="price-wrap"> <span class="cm-reload-8032" id="old_price_update_8032"> <!--old_price_update_8032--></span> <span class="cm-reload-8032 price-update" id="price_update_8032"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8032"><span class="price-num">₹</span><span id="sec_discounted_price_8032" class="price-num">3,340</span></span>
																								<!--price_update_8032-->
																								</span> <span class="cm-reload-8032" id="line_discount_update_8032"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8032--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_8032" name="product_data[8032][amount]" value="1">
																					<div class="cm-reload-8032" id="product_options_update_8032">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_8032_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_8032">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_8032_7015">
																									<label id="option_description_8032_7015" for="option_8032_7015" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_8032_7015" class="form-control " rows="3" name="product_data[8032][product_options][7015]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_8032-->
																					</div>
																					<div class="cm-reload-8032  add-to-cart-container" id="add_to_cart_update_8032">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_8032">
																							<button id="button_cart_8032" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..8032]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_8032-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3340.000000" data-id="8031" data-value="Nile 1 9655-100" data-readable-value="Nile 1 9655-100">
																	<div class="grid-hover hidden-xs hidden-sm" data-cloudzoom="zoomImage: &quot;../images/thumbnails/300/380/detailed/39/H2A8Ipb.jpg&quot;" src="../images/thumbnails/300/380/detailed/39/H2A8Ipb.jpg" style="user-select: none;">
																		<div class="vertical-align"> <span class="product-title">Monti small motifs</span>
																			<div class="grid-list-price"> <span class="cm-reload-8031" id="old_price_update_8031"> <!--old_price_update_8031--></span> <span class="cm-reload-8031 price-update" id="price_update_8031"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8031"><span class="price-num">₹</span><span id="sec_discounted_price_8031" class="price-num">3,340</span></span>
																				<!--price_update_8031-->
																				</span> <span class="cm-reload-8031" id="line_discount_update_8031"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8031--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2A8Ipb.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2A8Ipb.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_8031" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[8031][product_id]" value="8031">
																				<input type="hidden" class="product-sku-code" value="Nile 1 9655-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti small motifs</h1>
																							<p class="product-sku">Nile 1 9655-100</p>
																							<div class="price-wrap"> <span class="cm-reload-8031" id="old_price_update_8031"> <!--old_price_update_8031--></span> <span class="cm-reload-8031 price-update" id="price_update_8031"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8031"><span class="price-num">₹</span><span id="sec_discounted_price_8031" class="price-num">3,340</span></span>
																								<!--price_update_8031-->
																								</span> <span class="cm-reload-8031" id="line_discount_update_8031"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8031--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_8031" name="product_data[8031][amount]" value="1">
																					<div class="cm-reload-8031" id="product_options_update_8031">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_8031_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_8031">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_8031_7014">
																									<label id="option_description_8031_7014" for="option_8031_7014" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_8031_7014" class="form-control " rows="3" name="product_data[8031][product_options][7014]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_8031-->
																					</div>
																					<div class="cm-reload-8031  add-to-cart-container" id="add_to_cart_update_8031">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_8031">
																							<button id="button_cart_8031" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..8031]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_8031-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3340.000000" data-id="8022" data-value="Nile 1 9971-100" data-readable-value="Nile 1 9971-100">
																	<div class="grid-hover hidden-xs hidden-sm" data-cloudzoom="zoomImage: &quot;../images/thumbnails/300/380/detailed/39/H2A9aGJ_kkdu-jd.jpg&quot;" src="../images/thumbnails/300/380/detailed/39/H2A9aGJ_kkdu-jd.jpg" style="user-select: none;">
																		<div class="vertical-align"> <span class="product-title">Monti Big squares</span>
																			<div class="grid-list-price"> <span class="cm-reload-8022" id="old_price_update_8022"> <!--old_price_update_8022--></span> <span class="cm-reload-8022 price-update" id="price_update_8022"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8022"><span class="price-num">₹</span><span id="sec_discounted_price_8022" class="price-num">3,340</span></span>
																				<!--price_update_8022-->
																				</span> <span class="cm-reload-8022" id="line_discount_update_8022"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8022--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2A9aGJ_kkdu-jd.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2A9aGJ_kkdu-jd.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_8022" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[8022][product_id]" value="8022">
																				<input type="hidden" class="product-sku-code" value="Nile 1 9971-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti Big squares</h1>
																							<p class="product-sku">Nile 1 9971-100</p>
																							<div class="price-wrap"> <span class="cm-reload-8022" id="old_price_update_8022"> <!--old_price_update_8022--></span> <span class="cm-reload-8022 price-update" id="price_update_8022"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8022"><span class="price-num">₹</span><span id="sec_discounted_price_8022" class="price-num">3,340</span></span>
																								<!--price_update_8022-->
																								</span> <span class="cm-reload-8022" id="line_discount_update_8022"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8022--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_8022" name="product_data[8022][amount]" value="1">
																					<div class="cm-reload-8022" id="product_options_update_8022">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_8022_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_8022">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_8022_7005">
																									<label id="option_description_8022_7005" for="option_8022_7005" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_8022_7005" class="form-control " rows="3" name="product_data[8022][product_options][7005]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_8022-->
																					</div>
																					<div class="cm-reload-8022  add-to-cart-container" id="add_to_cart_update_8022">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_8022">
																							<button id="button_cart_8022" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..8022]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_8022-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3340.000000" data-id="8021" data-value="Nile 1 9697-100" data-readable-value="Nile 1 9697-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti uniform checks</span>
																			<div class="grid-list-price"> <span class="cm-reload-8021" id="old_price_update_8021"> <!--old_price_update_8021--></span> <span class="cm-reload-8021 price-update" id="price_update_8021"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8021"><span class="price-num">₹</span><span id="sec_discounted_price_8021" class="price-num">3,340</span></span>
																				<!--price_update_8021-->
																				</span> <span class="cm-reload-8021" id="line_discount_update_8021"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8021--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2A9wCL.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2A9wCL.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_8021" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[8021][product_id]" value="8021">
																				<input type="hidden" class="product-sku-code" value="Nile 1 9697-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti uniform checks</h1>
																							<p class="product-sku">Nile 1 9697-100</p>
																							<div class="price-wrap"> <span class="cm-reload-8021" id="old_price_update_8021"> <!--old_price_update_8021--></span> <span class="cm-reload-8021 price-update" id="price_update_8021"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8021"><span class="price-num">₹</span><span id="sec_discounted_price_8021" class="price-num">3,340</span></span>
																								<!--price_update_8021-->
																								</span> <span class="cm-reload-8021" id="line_discount_update_8021"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8021--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_8021" name="product_data[8021][amount]" value="1">
																					<div class="cm-reload-8021" id="product_options_update_8021">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_8021_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_8021">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_8021_7004">
																									<label id="option_description_8021_7004" for="option_8021_7004" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_8021_7004" class="form-control " rows="3" name="product_data[8021][product_options][7004]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_8021-->
																					</div>
																					<div class="cm-reload-8021  add-to-cart-container" id="add_to_cart_update_8021">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_8021">
																							<button id="button_cart_8021" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..8021]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_8021-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3340.000000" data-id="8012" data-value="Nile 1 9659-100" data-readable-value="Nile 1 9659-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti stripes with squares</span>
																			<div class="grid-list-price"> <span class="cm-reload-8012" id="old_price_update_8012"> <!--old_price_update_8012--></span> <span class="cm-reload-8012 price-update" id="price_update_8012"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8012"><span class="price-num">₹</span><span id="sec_discounted_price_8012" class="price-num">3,340</span></span>
																				<!--price_update_8012-->
																				</span> <span class="cm-reload-8012" id="line_discount_update_8012"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8012--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2A9WWW_tjy4-vi.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2A9WWW_tjy4-vi.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_8012" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[8012][product_id]" value="8012">
																				<input type="hidden" class="product-sku-code" value="Nile 1 9659-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti stripes with squares</h1>
																							<p class="product-sku">Nile 1 9659-100</p>
																							<div class="price-wrap"> <span class="cm-reload-8012" id="old_price_update_8012"> <!--old_price_update_8012--></span> <span class="cm-reload-8012 price-update" id="price_update_8012"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8012"><span class="price-num">₹</span><span id="sec_discounted_price_8012" class="price-num">3,340</span></span>
																								<!--price_update_8012-->
																								</span> <span class="cm-reload-8012" id="line_discount_update_8012"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8012--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_8012" name="product_data[8012][amount]" value="1">
																					<div class="cm-reload-8012" id="product_options_update_8012">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_8012_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_8012">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_8012_6995">
																									<label id="option_description_8012_6995" for="option_8012_6995" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_8012_6995" class="form-control " rows="3" name="product_data[8012][product_options][6995]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_8012-->
																					</div>
																					<div class="cm-reload-8012  add-to-cart-container" id="add_to_cart_update_8012">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_8012">
																							<button id="button_cart_8012" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..8012]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_8012-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3340.000000" data-id="8011" data-value="Nile 1 9657-100" data-readable-value="Nile 1 9657-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti patterned</span>
																			<div class="grid-list-price"> <span class="cm-reload-8011" id="old_price_update_8011"> <!--old_price_update_8011--></span> <span class="cm-reload-8011 price-update" id="price_update_8011"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8011"><span class="price-num">₹</span><span id="sec_discounted_price_8011" class="price-num">3,340</span></span>
																				<!--price_update_8011-->
																				</span> <span class="cm-reload-8011" id="line_discount_update_8011"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8011--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2Aat4p.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2Aat4p.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_8011" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[8011][product_id]" value="8011">
																				<input type="hidden" class="product-sku-code" value="Nile 1 9657-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti patterned</h1>
																							<p class="product-sku">Nile 1 9657-100</p>
																							<div class="price-wrap"> <span class="cm-reload-8011" id="old_price_update_8011"> <!--old_price_update_8011--></span> <span class="cm-reload-8011 price-update" id="price_update_8011"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8011"><span class="price-num">₹</span><span id="sec_discounted_price_8011" class="price-num">3,340</span></span>
																								<!--price_update_8011-->
																								</span> <span class="cm-reload-8011" id="line_discount_update_8011"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8011--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_8011" name="product_data[8011][amount]" value="1">
																					<div class="cm-reload-8011" id="product_options_update_8011">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_8011_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_8011">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_8011_6994">
																									<label id="option_description_8011_6994" for="option_8011_6994" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_8011_6994" class="form-control " rows="3" name="product_data[8011][product_options][6994]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_8011-->
																					</div>
																					<div class="cm-reload-8011  add-to-cart-container" id="add_to_cart_update_8011">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_8011">
																							<button id="button_cart_8011" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..8011]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_8011-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3340.000000" data-id="8002" data-value="Nile 1 9750-100" data-readable-value="Nile 1 9750-100">
																	<div class="grid-hover hidden-xs hidden-sm" data-cloudzoom="zoomImage: &quot;../images/thumbnails/300/380/detailed/39/H2Ab2VZ_w150-ca.jpg&quot;" src="../images/thumbnails/300/380/detailed/39/H2Ab2VZ_w150-ca.jpg" style="user-select: none;">
																		<div class="vertical-align"> <span class="product-title">Monti small squares</span>
																			<div class="grid-list-price"> <span class="cm-reload-8002" id="old_price_update_8002"> <!--old_price_update_8002--></span> <span class="cm-reload-8002 price-update" id="price_update_8002"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8002"><span class="price-num">₹</span><span id="sec_discounted_price_8002" class="price-num">3,340</span></span>
																				<!--price_update_8002-->
																				</span> <span class="cm-reload-8002" id="line_discount_update_8002"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8002--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2Ab2VZ_w150-ca.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2Ab2VZ_w150-ca.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_8002" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[8002][product_id]" value="8002">
																				<input type="hidden" class="product-sku-code" value="Nile 1 9750-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti small squares</h1>
																							<p class="product-sku">Nile 1 9750-100</p>
																							<div class="price-wrap"> <span class="cm-reload-8002" id="old_price_update_8002"> <!--old_price_update_8002--></span> <span class="cm-reload-8002 price-update" id="price_update_8002"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8002"><span class="price-num">₹</span><span id="sec_discounted_price_8002" class="price-num">3,340</span></span>
																								<!--price_update_8002-->
																								</span> <span class="cm-reload-8002" id="line_discount_update_8002"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8002--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_8002" name="product_data[8002][amount]" value="1">
																					<div class="cm-reload-8002" id="product_options_update_8002">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_8002_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_8002">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_8002_6985">
																									<label id="option_description_8002_6985" for="option_8002_6985" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_8002_6985" class="form-control " rows="3" name="product_data[8002][product_options][6985]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_8002-->
																					</div>
																					<div class="cm-reload-8002  add-to-cart-container" id="add_to_cart_update_8002">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_8002">
																							<button id="button_cart_8002" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..8002]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_8002-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3340.000000" data-id="8001" data-value="Nile 1 9674-100" data-readable-value="Nile 1 9674-100">
																	<div class="grid-hover hidden-xs hidden-sm" data-cloudzoom="zoomImage: &quot;../images/thumbnails/300/380/detailed/39/H2Abwb7.jpg&quot;" src="../images/thumbnails/300/380/detailed/39/H2Abwb7.jpg" style="user-select: none;">
																		<div class="vertical-align"> <span class="product-title">Monti checkered squares</span>
																			<div class="grid-list-price"> <span class="cm-reload-8001" id="old_price_update_8001"> <!--old_price_update_8001--></span> <span class="cm-reload-8001 price-update" id="price_update_8001"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8001"><span class="price-num">₹</span><span id="sec_discounted_price_8001" class="price-num">3,340</span></span>
																				<!--price_update_8001-->
																				</span> <span class="cm-reload-8001" id="line_discount_update_8001"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8001--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2Abwb7.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2Abwb7.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_8001" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[8001][product_id]" value="8001">
																				<input type="hidden" class="product-sku-code" value="Nile 1 9674-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti checkered squares</h1>
																							<p class="product-sku">Nile 1 9674-100</p>
																							<div class="price-wrap"> <span class="cm-reload-8001" id="old_price_update_8001"> <!--old_price_update_8001--></span> <span class="cm-reload-8001 price-update" id="price_update_8001"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_8001"><span class="price-num">₹</span><span id="sec_discounted_price_8001" class="price-num">3,340</span></span>
																								<!--price_update_8001-->
																								</span> <span class="cm-reload-8001" id="line_discount_update_8001"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_8001--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_8001" name="product_data[8001][amount]" value="1">
																					<div class="cm-reload-8001" id="product_options_update_8001">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_8001_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_8001">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_8001_6984">
																									<label id="option_description_8001_6984" for="option_8001_6984" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_8001_6984" class="form-control " rows="3" name="product_data[8001][product_options][6984]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_8001-->
																					</div>
																					<div class="cm-reload-8001  add-to-cart-container" id="add_to_cart_update_8001">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_8001">
																							<button id="button_cart_8001" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..8001]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_8001-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3340.000000" data-id="7992" data-value="Nile 1 9763-100" data-readable-value="Nile 1 9763-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti Patterned squares</span>
																			<div class="grid-list-price"> <span class="cm-reload-7992" id="old_price_update_7992"> <!--old_price_update_7992--></span> <span class="cm-reload-7992 price-update" id="price_update_7992"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7992"><span class="price-num">₹</span><span id="sec_discounted_price_7992" class="price-num">3,340</span></span>
																				<!--price_update_7992-->
																				</span> <span class="cm-reload-7992" id="line_discount_update_7992"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7992--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2AcqXU_e11z-zb.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2AcqXU_e11z-zb.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7992" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7992][product_id]" value="7992">
																				<input type="hidden" class="product-sku-code" value="Nile 1 9763-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti Patterned squares</h1>
																							<p class="product-sku">Nile 1 9763-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7992" id="old_price_update_7992"> <!--old_price_update_7992--></span> <span class="cm-reload-7992 price-update" id="price_update_7992"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7992"><span class="price-num">₹</span><span id="sec_discounted_price_7992" class="price-num">3,340</span></span>
																								<!--price_update_7992-->
																								</span> <span class="cm-reload-7992" id="line_discount_update_7992"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7992--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7992" name="product_data[7992][amount]" value="1">
																					<div class="cm-reload-7992" id="product_options_update_7992">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7992_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7992">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7992_6975">
																									<label id="option_description_7992_6975" for="option_7992_6975" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7992_6975" class="form-control " rows="3" name="product_data[7992][product_options][6975]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7992-->
																					</div>
																					<div class="cm-reload-7992  add-to-cart-container" id="add_to_cart_update_7992">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7992">
																							<button id="button_cart_7992" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7992]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7992-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3340.000000" data-id="7991" data-value="Nile 1 9656-100" data-readable-value="Nile 1 9656-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti Plus motifs</span>
																			<div class="grid-list-price"> <span class="cm-reload-7991" id="old_price_update_7991"> <!--old_price_update_7991--></span> <span class="cm-reload-7991 price-update" id="price_update_7991"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7991"><span class="price-num">₹</span><span id="sec_discounted_price_7991" class="price-num">3,340</span></span>
																				<!--price_update_7991-->
																				</span> <span class="cm-reload-7991" id="line_discount_update_7991"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7991--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2AcQXs.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2AcQXs.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7991" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7991][product_id]" value="7991">
																				<input type="hidden" class="product-sku-code" value="Nile 1 9656-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti Plus motifs</h1>
																							<p class="product-sku">Nile 1 9656-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7991" id="old_price_update_7991"> <!--old_price_update_7991--></span> <span class="cm-reload-7991 price-update" id="price_update_7991"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7991"><span class="price-num">₹</span><span id="sec_discounted_price_7991" class="price-num">3,340</span></span>
																								<!--price_update_7991-->
																								</span> <span class="cm-reload-7991" id="line_discount_update_7991"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7991--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7991" name="product_data[7991][amount]" value="1">
																					<div class="cm-reload-7991" id="product_options_update_7991">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7991_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7991">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7991_6974">
																									<label id="option_description_7991_6974" for="option_7991_6974" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7991_6974" class="form-control " rows="3" name="product_data[7991][product_options][6974]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7991-->
																					</div>
																					<div class="cm-reload-7991  add-to-cart-container" id="add_to_cart_update_7991">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7991">
																							<button id="button_cart_7991" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7991]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7991-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3340.000000" data-id="7982" data-value="Nile 1 9658-100" data-readable-value="Nile 1 9658-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti small checks</span>
																			<div class="grid-list-price"> <span class="cm-reload-7982" id="old_price_update_7982"> <!--old_price_update_7982--></span> <span class="cm-reload-7982 price-update" id="price_update_7982"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7982"><span class="price-num">₹</span><span id="sec_discounted_price_7982" class="price-num">3,340</span></span>
																				<!--price_update_7982-->
																				</span> <span class="cm-reload-7982" id="line_discount_update_7982"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7982--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2Adi2cK_869j-pl.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2Adi2cK_869j-pl.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7982" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7982][product_id]" value="7982">
																				<input type="hidden" class="product-sku-code" value="Nile 1 9658-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti small checks</h1>
																							<p class="product-sku">Nile 1 9658-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7982" id="old_price_update_7982"> <!--old_price_update_7982--></span> <span class="cm-reload-7982 price-update" id="price_update_7982"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7982"><span class="price-num">₹</span><span id="sec_discounted_price_7982" class="price-num">3,340</span></span>
																								<!--price_update_7982-->
																								</span> <span class="cm-reload-7982" id="line_discount_update_7982"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7982--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7982" name="product_data[7982][amount]" value="1">
																					<div class="cm-reload-7982" id="product_options_update_7982">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7982_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7982">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7982_6965">
																									<label id="option_description_7982_6965" for="option_7982_6965" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7982_6965" class="form-control " rows="3" name="product_data[7982][product_options][6965]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7982-->
																					</div>
																					<div class="cm-reload-7982  add-to-cart-container" id="add_to_cart_update_7982">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7982">
																							<button id="button_cart_7982" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7982]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7982-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3340.000000" data-id="7981" data-value="Nile 1 9748-100" data-readable-value="Nile 1 9748-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti stripes</span>
																			<div class="grid-list-price"> <span class="cm-reload-7981" id="old_price_update_7981"> <!--old_price_update_7981--></span> <span class="cm-reload-7981 price-update" id="price_update_7981"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7981"><span class="price-num">₹</span><span id="sec_discounted_price_7981" class="price-num">3,340</span></span>
																				<!--price_update_7981-->
																				</span> <span class="cm-reload-7981" id="line_discount_update_7981"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7981--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2AdM49.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2AdM49.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7981" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7981][product_id]" value="7981">
																				<input type="hidden" class="product-sku-code" value="Nile 1 9748-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti stripes</h1>
																							<p class="product-sku">Nile 1 9748-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7981" id="old_price_update_7981"> <!--old_price_update_7981--></span> <span class="cm-reload-7981 price-update" id="price_update_7981"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7981"><span class="price-num">₹</span><span id="sec_discounted_price_7981" class="price-num">3,340</span></span>
																								<!--price_update_7981-->
																								</span> <span class="cm-reload-7981" id="line_discount_update_7981"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7981--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7981" name="product_data[7981][amount]" value="1">
																					<div class="cm-reload-7981" id="product_options_update_7981">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7981_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7981">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7981_6964">
																									<label id="option_description_7981_6964" for="option_7981_6964" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7981_6964" class="form-control " rows="3" name="product_data[7981][product_options][6964]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7981-->
																					</div>
																					<div class="cm-reload-7981  add-to-cart-container" id="add_to_cart_update_7981">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7981">
																							<button id="button_cart_7981" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7981]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7981-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3340.000000" data-id="7972" data-value="Tex 9991-100" data-readable-value="Tex 9991-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti woven pattern</span>
																			<div class="grid-list-price"> <span class="cm-reload-7972" id="old_price_update_7972"> <!--old_price_update_7972--></span> <span class="cm-reload-7972 price-update" id="price_update_7972"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7972"><span class="price-num">₹</span><span id="sec_discounted_price_7972" class="price-num">3,340</span></span>
																				<!--price_update_7972-->
																				</span> <span class="cm-reload-7972" id="line_discount_update_7972"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7972--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2Aed0S_5bhb-v5.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2Aed0S_5bhb-v5.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7972" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7972][product_id]" value="7972">
																				<input type="hidden" class="product-sku-code" value="Tex 9991-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti woven pattern</h1>
																							<p class="product-sku">Tex 9991-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7972" id="old_price_update_7972"> <!--old_price_update_7972--></span> <span class="cm-reload-7972 price-update" id="price_update_7972"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7972"><span class="price-num">₹</span><span id="sec_discounted_price_7972" class="price-num">3,340</span></span>
																								<!--price_update_7972-->
																								</span> <span class="cm-reload-7972" id="line_discount_update_7972"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7972--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7972" name="product_data[7972][amount]" value="1">
																					<div class="cm-reload-7972" id="product_options_update_7972">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7972_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7972">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7972_6955">
																									<label id="option_description_7972_6955" for="option_7972_6955" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7972_6955" class="form-control " rows="3" name="product_data[7972][product_options][6955]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7972-->
																					</div>
																					<div class="cm-reload-7972  add-to-cart-container" id="add_to_cart_update_7972">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7972">
																							<button id="button_cart_7972" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7972]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7972-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="4290.000000" data-id="7971" data-value="Funny 9999-100" data-readable-value="Funny 9999-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti satin</span>
																			<div class="grid-list-price"> <span class="cm-reload-7971" id="old_price_update_7971"> <!--old_price_update_7971--></span> <span class="cm-reload-7971 price-update" id="price_update_7971"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7971"><span class="price-num">₹</span><span id="sec_discounted_price_7971" class="price-num">4,290</span></span>
																				<!--price_update_7971-->
																				</span> <span class="cm-reload-7971" id="line_discount_update_7971"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7971--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2AeLLz.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2AeLLz.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7971" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7971][product_id]" value="7971">
																				<input type="hidden" class="product-sku-code" value="Funny 9999-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti satin</h1>
																							<p class="product-sku">Funny 9999-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7971" id="old_price_update_7971"> <!--old_price_update_7971--></span> <span class="cm-reload-7971 price-update" id="price_update_7971"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7971"><span class="price-num">₹</span><span id="sec_discounted_price_7971" class="price-num">4,290</span></span>
																								<!--price_update_7971-->
																								</span> <span class="cm-reload-7971" id="line_discount_update_7971"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7971--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7971" name="product_data[7971][amount]" value="1">
																					<div class="cm-reload-7971" id="product_options_update_7971">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7971_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7971">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7971_6954">
																									<label id="option_description_7971_6954" for="option_7971_6954" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7971_6954" class="form-control " rows="3" name="product_data[7971][product_options][6954]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7971-->
																					</div>
																					<div class="cm-reload-7971  add-to-cart-container" id="add_to_cart_update_7971">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7971">
																							<button id="button_cart_7971" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7971]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7971-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3590.000000" data-id="7962" data-value="Isonzo 9967-100" data-readable-value="Isonzo 9967-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti checkered herringbone</span>
																			<div class="grid-list-price"> <span class="cm-reload-7962" id="old_price_update_7962"> <!--old_price_update_7962--></span> <span class="cm-reload-7962 price-update" id="price_update_7962"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7962"><span class="price-num">₹</span><span id="sec_discounted_price_7962" class="price-num">3,590</span></span>
																				<!--price_update_7962-->
																				</span> <span class="cm-reload-7962" id="line_discount_update_7962"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7962--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2AeYFt_3rq0-ev.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2AeYFt_3rq0-ev.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7962" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7962][product_id]" value="7962">
																				<input type="hidden" class="product-sku-code" value="Isonzo 9967-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti checkered herringbone</h1>
																							<p class="product-sku">Isonzo 9967-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7962" id="old_price_update_7962"> <!--old_price_update_7962--></span> <span class="cm-reload-7962 price-update" id="price_update_7962"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7962"><span class="price-num">₹</span><span id="sec_discounted_price_7962" class="price-num">3,590</span></span>
																								<!--price_update_7962-->
																								</span> <span class="cm-reload-7962" id="line_discount_update_7962"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7962--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7962" name="product_data[7962][amount]" value="1">
																					<div class="cm-reload-7962" id="product_options_update_7962">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7962_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7962">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7962_6945">
																									<label id="option_description_7962_6945" for="option_7962_6945" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7962_6945" class="form-control " rows="3" name="product_data[7962][product_options][6945]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7962-->
																					</div>
																					<div class="cm-reload-7962  add-to-cart-container" id="add_to_cart_update_7962">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7962">
																							<button id="button_cart_7962" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7962]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7962-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3590.000000" data-id="7961" data-value="Isonzon 9968-100" data-readable-value="Isonzon 9968-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti block checks</span>
																			<div class="grid-list-price"> <span class="cm-reload-7961" id="old_price_update_7961"> <!--old_price_update_7961--></span> <span class="cm-reload-7961 price-update" id="price_update_7961"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7961"><span class="price-num">₹</span><span id="sec_discounted_price_7961" class="price-num">3,590</span></span>
																				<!--price_update_7961-->
																				</span> <span class="cm-reload-7961" id="line_discount_update_7961"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7961--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2AhaU7.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2AhaU7.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7961" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7961][product_id]" value="7961">
																				<input type="hidden" class="product-sku-code" value="Isonzon 9968-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti block checks</h1>
																							<p class="product-sku">Isonzon 9968-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7961" id="old_price_update_7961"> <!--old_price_update_7961--></span> <span class="cm-reload-7961 price-update" id="price_update_7961"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7961"><span class="price-num">₹</span><span id="sec_discounted_price_7961" class="price-num">3,590</span></span>
																								<!--price_update_7961-->
																								</span> <span class="cm-reload-7961" id="line_discount_update_7961"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7961--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7961" name="product_data[7961][amount]" value="1">
																					<div class="cm-reload-7961" id="product_options_update_7961">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7961_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7961">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7961_6944">
																									<label id="option_description_7961_6944" for="option_7961_6944" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7961_6944" class="form-control " rows="3" name="product_data[7961][product_options][6944]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7961-->
																					</div>
																					<div class="cm-reload-7961  add-to-cart-container" id="add_to_cart_update_7961">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7961">
																							<button id="button_cart_7961" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7961]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7961-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3590.000000" data-id="7952" data-value="Isonzo 9993-100" data-readable-value="Isonzo 9993-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti gingham</span>
																			<div class="grid-list-price"> <span class="cm-reload-7952" id="old_price_update_7952"> <!--old_price_update_7952--></span> <span class="cm-reload-7952 price-update" id="price_update_7952"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7952"><span class="price-num">₹</span><span id="sec_discounted_price_7952" class="price-num">3,590</span></span>
																				<!--price_update_7952-->
																				</span> <span class="cm-reload-7952" id="line_discount_update_7952"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7952--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2AhlaO_w97c-fg.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2AhlaO_w97c-fg.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7952" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7952][product_id]" value="7952">
																				<input type="hidden" class="product-sku-code" value="Isonzo 9993-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti gingham</h1>
																							<p class="product-sku">Isonzo 9993-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7952" id="old_price_update_7952"> <!--old_price_update_7952--></span> <span class="cm-reload-7952 price-update" id="price_update_7952"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7952"><span class="price-num">₹</span><span id="sec_discounted_price_7952" class="price-num">3,590</span></span>
																								<!--price_update_7952-->
																								</span> <span class="cm-reload-7952" id="line_discount_update_7952"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7952--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7952" name="product_data[7952][amount]" value="1">
																					<div class="cm-reload-7952" id="product_options_update_7952">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7952_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7952">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7952_6935">
																									<label id="option_description_7952_6935" for="option_7952_6935" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7952_6935" class="form-control " rows="3" name="product_data[7952][product_options][6935]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7952-->
																					</div>
																					<div class="cm-reload-7952  add-to-cart-container" id="add_to_cart_update_7952">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7952">
																							<button id="button_cart_7952" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7952]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7952-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3590.000000" data-id="7951" data-value="Isonzo 9962-100" data-readable-value="Isonzo 9962-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti square</span>
																			<div class="grid-list-price"> <span class="cm-reload-7951" id="old_price_update_7951"> <!--old_price_update_7951--></span> <span class="cm-reload-7951 price-update" id="price_update_7951"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7951"><span class="price-num">₹</span><span id="sec_discounted_price_7951" class="price-num">3,590</span></span>
																				<!--price_update_7951-->
																				</span> <span class="cm-reload-7951" id="line_discount_update_7951"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7951--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2AhG3W.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2AhG3W.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7951" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7951][product_id]" value="7951">
																				<input type="hidden" class="product-sku-code" value="Isonzo 9962-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti square</h1>
																							<p class="product-sku">Isonzo 9962-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7951" id="old_price_update_7951"> <!--old_price_update_7951--></span> <span class="cm-reload-7951 price-update" id="price_update_7951"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7951"><span class="price-num">₹</span><span id="sec_discounted_price_7951" class="price-num">3,590</span></span>
																								<!--price_update_7951-->
																								</span> <span class="cm-reload-7951" id="line_discount_update_7951"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7951--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7951" name="product_data[7951][amount]" value="1">
																					<div class="cm-reload-7951" id="product_options_update_7951">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7951_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7951">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7951_6934">
																									<label id="option_description_7951_6934" for="option_7951_6934" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7951_6934" class="form-control " rows="3" name="product_data[7951][product_options][6934]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7951-->
																					</div>
																					<div class="cm-reload-7951  add-to-cart-container" id="add_to_cart_update_7951">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7951">
																							<button id="button_cart_7951" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7951]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7951-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3340.000000" data-id="7942" data-value="Veliero 9987-100" data-readable-value="Veliero 9987-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti square stripes</span>
																			<div class="grid-list-price"> <span class="cm-reload-7942" id="old_price_update_7942"> <!--old_price_update_7942--></span> <span class="cm-reload-7942 price-update" id="price_update_7942"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7942"><span class="price-num">₹</span><span id="sec_discounted_price_7942" class="price-num">3,340</span></span>
																				<!--price_update_7942-->
																				</span> <span class="cm-reload-7942" id="line_discount_update_7942"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7942--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2AhW5tE_q1g8-h8.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2AhW5tE_q1g8-h8.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7942" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7942][product_id]" value="7942">
																				<input type="hidden" class="product-sku-code" value="Veliero 9987-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti square stripes</h1>
																							<p class="product-sku">Veliero 9987-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7942" id="old_price_update_7942"> <!--old_price_update_7942--></span> <span class="cm-reload-7942 price-update" id="price_update_7942"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7942"><span class="price-num">₹</span><span id="sec_discounted_price_7942" class="price-num">3,340</span></span>
																								<!--price_update_7942-->
																								</span> <span class="cm-reload-7942" id="line_discount_update_7942"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7942--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7942" name="product_data[7942][amount]" value="1">
																					<div class="cm-reload-7942" id="product_options_update_7942">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7942_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7942">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7942_6925">
																									<label id="option_description_7942_6925" for="option_7942_6925" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7942_6925" class="form-control " rows="3" name="product_data[7942][product_options][6925]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7942-->
																					</div>
																					<div class="cm-reload-7942  add-to-cart-container" id="add_to_cart_update_7942">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7942">
																							<button id="button_cart_7942" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7942]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7942-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3290.000000" data-id="7941" data-value="Hepburn 9597-100" data-readable-value="Hepburn 9597-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti square motifs</span>
																			<div class="grid-list-price"> <span class="cm-reload-7941" id="old_price_update_7941"> <!--old_price_update_7941--></span> <span class="cm-reload-7941 price-update" id="price_update_7941"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7941"><span class="price-num">₹</span><span id="sec_discounted_price_7941" class="price-num">3,290</span></span>
																				<!--price_update_7941-->
																				</span> <span class="cm-reload-7941" id="line_discount_update_7941"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7941--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class="cloudzoom lazyloaded" src="../images/thumbnails/300/380/detailed/39/H2Ai81O.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2Ai81O.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7941" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7941][product_id]" value="7941">
																				<input type="hidden" class="product-sku-code" value="Hepburn 9597-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti square motifs</h1>
																							<p class="product-sku">Hepburn 9597-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7941" id="old_price_update_7941"> <!--old_price_update_7941--></span> <span class="cm-reload-7941 price-update" id="price_update_7941"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7941"><span class="price-num">₹</span><span id="sec_discounted_price_7941" class="price-num">3,290</span></span>
																								<!--price_update_7941-->
																								</span> <span class="cm-reload-7941" id="line_discount_update_7941"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7941--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7941" name="product_data[7941][amount]" value="1">
																					<div class="cm-reload-7941" id="product_options_update_7941">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7941_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7941">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7941_6924">
																									<label id="option_description_7941_6924" for="option_7941_6924" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7941_6924" class="form-control " rows="3" name="product_data[7941][product_options][6924]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7941-->
																					</div>
																					<div class="cm-reload-7941  add-to-cart-container" id="add_to_cart_update_7941">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7941">
																							<button id="button_cart_7941" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7941]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7941-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3640.000000" data-id="7932" data-value="Lambro 9994-100" data-readable-value="Lambro 9994-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti Geometric</span>
																			<div class="grid-list-price"> <span class="cm-reload-7932" id="old_price_update_7932"> <!--old_price_update_7932--></span> <span class="cm-reload-7932 price-update" id="price_update_7932"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7932"><span class="price-num">₹</span><span id="sec_discounted_price_7932" class="price-num">3,640</span></span>
																				<!--price_update_7932-->
																				</span> <span class="cm-reload-7932" id="line_discount_update_7932"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7932--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/39/H2AiiK2_2vhu-hg.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2AiiK2_2vhu-hg.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7932" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7932][product_id]" value="7932">
																				<input type="hidden" class="product-sku-code" value="Lambro 9994-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti Geometric</h1>
																							<p class="product-sku">Lambro 9994-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7932" id="old_price_update_7932"> <!--old_price_update_7932--></span> <span class="cm-reload-7932 price-update" id="price_update_7932"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7932"><span class="price-num">₹</span><span id="sec_discounted_price_7932" class="price-num">3,640</span></span>
																								<!--price_update_7932-->
																								</span> <span class="cm-reload-7932" id="line_discount_update_7932"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7932--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7932" name="product_data[7932][amount]" value="1">
																					<div class="cm-reload-7932" id="product_options_update_7932">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7932_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7932">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7932_6915">
																									<label id="option_description_7932_6915" for="option_7932_6915" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7932_6915" class="form-control " rows="3" name="product_data[7932][product_options][6915]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7932-->
																					</div>
																					<div class="cm-reload-7932  add-to-cart-container" id="add_to_cart_update_7932">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7932">
																							<button id="button_cart_7932" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7932]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7932-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3340.000000" data-id="7931" data-value="70 twil 9997-100" data-readable-value="70 twil 9997-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti Twill</span>
																			<div class="grid-list-price"> <span class="cm-reload-7931" id="old_price_update_7931"> <!--old_price_update_7931--></span> <span class="cm-reload-7931 price-update" id="price_update_7931"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7931"><span class="price-num">₹</span><span id="sec_discounted_price_7931" class="price-num">3,340</span></span>
																				<!--price_update_7931-->
																				</span> <span class="cm-reload-7931" id="line_discount_update_7931"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7931--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/39/H2AizaD.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2AizaD.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7931" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7931][product_id]" value="7931">
																				<input type="hidden" class="product-sku-code" value="70 twil 9997-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti Twill</h1>
																							<p class="product-sku">70 twil 9997-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7931" id="old_price_update_7931"> <!--old_price_update_7931--></span> <span class="cm-reload-7931 price-update" id="price_update_7931"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7931"><span class="price-num">₹</span><span id="sec_discounted_price_7931" class="price-num">3,340</span></span>
																								<!--price_update_7931-->
																								</span> <span class="cm-reload-7931" id="line_discount_update_7931"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7931--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7931" name="product_data[7931][amount]" value="1">
																					<div class="cm-reload-7931" id="product_options_update_7931">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7931_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7931">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7931_6914">
																									<label id="option_description_7931_6914" for="option_7931_6914" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7931_6914" class="form-control " rows="3" name="product_data[7931][product_options][6914]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7931-->
																					</div>
																					<div class="cm-reload-7931  add-to-cart-container" id="add_to_cart_update_7931">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7931">
																							<button id="button_cart_7931" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7931]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7931-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3640.000000" data-id="7922" data-value="Alma 8990-100" data-readable-value="Alma 8990-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti diamond checks</span>
																			<div class="grid-list-price"> <span class="cm-reload-7922" id="old_price_update_7922"> <!--old_price_update_7922--></span> <span class="cm-reload-7922 price-update" id="price_update_7922"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7922"><span class="price-num">₹</span><span id="sec_discounted_price_7922" class="price-num">3,640</span></span>
																				<!--price_update_7922-->
																				</span> <span class="cm-reload-7922" id="line_discount_update_7922"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7922--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/39/H2AiUpF_7gsn-en.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2AiUpF_7gsn-en.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7922" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7922][product_id]" value="7922">
																				<input type="hidden" class="product-sku-code" value="Alma 8990-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti diamond checks</h1>
																							<p class="product-sku">Alma 8990-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7922" id="old_price_update_7922"> <!--old_price_update_7922--></span> <span class="cm-reload-7922 price-update" id="price_update_7922"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7922"><span class="price-num">₹</span><span id="sec_discounted_price_7922" class="price-num">3,640</span></span>
																								<!--price_update_7922-->
																								</span> <span class="cm-reload-7922" id="line_discount_update_7922"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7922--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7922" name="product_data[7922][amount]" value="1">
																					<div class="cm-reload-7922" id="product_options_update_7922">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7922_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7922">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7922_6905">
																									<label id="option_description_7922_6905" for="option_7922_6905" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7922_6905" class="form-control " rows="3" name="product_data[7922][product_options][6905]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7922-->
																					</div>
																					<div class="cm-reload-7922  add-to-cart-container" id="add_to_cart_update_7922">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7922">
																							<button id="button_cart_7922" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7922]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7922-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3640.000000" data-id="7921" data-value="Alma 9985-100" data-readable-value="Alma 9985-100">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti oblique</span>
																			<div class="grid-list-price"> <span class="cm-reload-7921" id="old_price_update_7921"> <!--old_price_update_7921--></span> <span class="cm-reload-7921 price-update" id="price_update_7921"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7921"><span class="price-num">₹</span><span id="sec_discounted_price_7921" class="price-num">3,640</span></span>
																				<!--price_update_7921-->
																				</span> <span class="cm-reload-7921" id="line_discount_update_7921"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7921--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/39/H2Aj98b.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2Aj98b.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7921" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7921][product_id]" value="7921">
																				<input type="hidden" class="product-sku-code" value="Alma 9985-100">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti oblique</h1>
																							<p class="product-sku">Alma 9985-100</p>
																							<div class="price-wrap"> <span class="cm-reload-7921" id="old_price_update_7921"> <!--old_price_update_7921--></span> <span class="cm-reload-7921 price-update" id="price_update_7921"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7921"><span class="price-num">₹</span><span id="sec_discounted_price_7921" class="price-num">3,640</span></span>
																								<!--price_update_7921-->
																								</span> <span class="cm-reload-7921" id="line_discount_update_7921"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7921--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7921" name="product_data[7921][amount]" value="1">
																					<div class="cm-reload-7921" id="product_options_update_7921">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7921_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7921">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7921_6904">
																									<label id="option_description_7921_6904" for="option_7921_6904" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7921_6904" class="form-control " rows="3" name="product_data[7921][product_options][6904]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7921-->
																					</div>
																					<div class="cm-reload-7921  add-to-cart-container" id="add_to_cart_update_7921">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7921">
																							<button id="button_cart_7921" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7921]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7921-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="3340.000000" data-id="7912" data-value="Febe 9998-001" data-readable-value="Febe 9998-001">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Monti herringbone</span>
																			<div class="grid-list-price"> <span class="cm-reload-7912" id="old_price_update_7912"> <!--old_price_update_7912--></span> <span class="cm-reload-7912 price-update" id="price_update_7912"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7912"><span class="price-num">₹</span><span id="sec_discounted_price_7912" class="price-num">3,340</span></span>
																				<!--price_update_7912-->
																				</span> <span class="cm-reload-7912" id="line_discount_update_7912"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7912--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/39/H2Ajm9H_ecne-xl.jpg" data-src="../images/thumbnails/300/380/detailed/39/H2Ajm9H_ecne-xl.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7912" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7912][product_id]" value="7912">
																				<input type="hidden" class="product-sku-code" value="Febe 9998-001">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Monti herringbone</h1>
																							<p class="product-sku">Febe 9998-001</p>
																							<div class="price-wrap"> <span class="cm-reload-7912" id="old_price_update_7912"> <!--old_price_update_7912--></span> <span class="cm-reload-7912 price-update" id="price_update_7912"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7912"><span class="price-num">₹</span><span id="sec_discounted_price_7912" class="price-num">3,340</span></span>
																								<!--price_update_7912-->
																								</span> <span class="cm-reload-7912" id="line_discount_update_7912"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7912--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Dobby</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count"></p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7912" name="product_data[7912][amount]" value="1">
																					<div class="cm-reload-7912" id="product_options_update_7912">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7912_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7912">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7912_6895">
																									<label id="option_description_7912_6895" for="option_7912_6895" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7912_6895" class="form-control " rows="3" name="product_data[7912][product_options][6895]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7912-->
																					</div>
																					<div class="cm-reload-7912  add-to-cart-container" id="add_to_cart_update_7912">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7912">
																							<button id="button_cart_7912" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7912]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7912-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="1940.000000" data-id="7792" data-value="BC164" data-readable-value="BC164">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Blue Navy Checks</span>
																			<div class="grid-list-price"> <span class="cm-reload-7792" id="old_price_update_7792"> <!--old_price_update_7792--></span> <span class="cm-reload-7792 price-update" id="price_update_7792"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7792"><span class="price-num">₹</span><span id="sec_discounted_price_7792" class="price-num">1,940</span></span>
																				<!--price_update_7792-->
																				</span> <span class="cm-reload-7792" id="line_discount_update_7792"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7792--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/38/BC164.jpg" data-src="../images/thumbnails/300/380/detailed/38/BC164.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7792" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7792][product_id]" value="7792">
																				<input type="hidden" class="product-sku-code" value="BC164">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Blue Navy Checks</h1>
																							<p class="product-sku">BC164</p>
																							<div class="price-wrap"> <span class="cm-reload-7792" id="old_price_update_7792"> <!--old_price_update_7792--></span> <span class="cm-reload-7792 price-update" id="price_update_7792"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7792"><span class="price-num">₹</span><span id="sec_discounted_price_7792" class="price-num">1,940</span></span>
																								<!--price_update_7792-->
																								</span> <span class="cm-reload-7792" id="line_discount_update_7792"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7792--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Oxford</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Single Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count">40/1 x 40/1</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Heavy
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7792" name="product_data[7792][amount]" value="1">
																					<div class="cm-reload-7792" id="product_options_update_7792">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7792_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7792">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7792_6800">
																									<label id="option_description_7792_6800" for="option_7792_6800" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7792_6800" class="form-control " rows="3" name="product_data[7792][product_options][6800]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7792-->
																					</div>
																					<div class="cm-reload-7792  add-to-cart-container" id="add_to_cart_update_7792">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7792">
																							<button id="button_cart_7792" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7792]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7792-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="2040.000000" data-id="7791" data-value="BC162" data-readable-value="BC162">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Blue Brush Twill Checks</span>
																			<div class="grid-list-price"> <span class="cm-reload-7791" id="old_price_update_7791"> <!--old_price_update_7791--></span> <span class="cm-reload-7791 price-update" id="price_update_7791"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7791"><span class="price-num">₹</span><span id="sec_discounted_price_7791" class="price-num">2,040</span></span>
																				<!--price_update_7791-->
																				</span> <span class="cm-reload-7791" id="line_discount_update_7791"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7791--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/38/BC162_ncwn-6w.jpg" data-src="../images/thumbnails/300/380/detailed/38/BC162_ncwn-6w.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7791" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7791][product_id]" value="7791">
																				<input type="hidden" class="product-sku-code" value="BC162">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Blue Brush Twill Checks</h1>
																							<p class="product-sku">BC162</p>
																							<div class="price-wrap"> <span class="cm-reload-7791" id="old_price_update_7791"> <!--old_price_update_7791--></span> <span class="cm-reload-7791 price-update" id="price_update_7791"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7791"><span class="price-num">₹</span><span id="sec_discounted_price_7791" class="price-num">2,040</span></span>
																								<!--price_update_7791-->
																								</span> <span class="cm-reload-7791" id="line_discount_update_7791"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7791--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Twill</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Single Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count">40/1 x 40/1</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Heavy
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7791" name="product_data[7791][amount]" value="1">
																					<div class="cm-reload-7791" id="product_options_update_7791">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7791_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7791">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7791_6799">
																									<label id="option_description_7791_6799" for="option_7791_6799" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7791_6799" class="form-control " rows="3" name="product_data[7791][product_options][6799]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7791-->
																					</div>
																					<div class="cm-reload-7791  add-to-cart-container" id="add_to_cart_update_7791">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7791">
																							<button id="button_cart_7791" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7791]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7791-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="1990.000000" data-id="7786" data-value="BC163" data-readable-value="BC163">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Blue White Checks</span>
																			<div class="grid-list-price"> <span class="cm-reload-7786" id="old_price_update_7786"> <!--old_price_update_7786--></span> <span class="cm-reload-7786 price-update" id="price_update_7786"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7786"><span class="price-num">₹</span><span id="sec_discounted_price_7786" class="price-num">1,990</span></span>
																				<!--price_update_7786-->
																				</span> <span class="cm-reload-7786" id="line_discount_update_7786"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7786--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/39/BC163.jpg" data-src="../images/thumbnails/300/380/detailed/39/BC163.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7786" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7786][product_id]" value="7786">
																				<input type="hidden" class="product-sku-code" value="BC163">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Blue White Checks</h1>
																							<p class="product-sku">BC163</p>
																							<div class="price-wrap"> <span class="cm-reload-7786" id="old_price_update_7786"> <!--old_price_update_7786--></span> <span class="cm-reload-7786 price-update" id="price_update_7786"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7786"><span class="price-num">₹</span><span id="sec_discounted_price_7786" class="price-num">1,990</span></span>
																								<!--price_update_7786-->
																								</span> <span class="cm-reload-7786" id="line_discount_update_7786"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7786--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Poplin</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Single Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count">60/1 x 60/1</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Light
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7786" name="product_data[7786][amount]" value="1">
																					<div class="cm-reload-7786" id="product_options_update_7786">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7786_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7786">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7786_6794">
																									<label id="option_description_7786_6794" for="option_7786_6794" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7786_6794" class="form-control " rows="3" name="product_data[7786][product_options][6794]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7786-->
																					</div>
																					<div class="cm-reload-7786  add-to-cart-container" id="add_to_cart_update_7786">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7786">
																							<button id="button_cart_7786" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7786]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7786-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="1990.000000" data-id="7777" data-value="BC161" data-readable-value="BC161">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Greyish Blue Black Checks</span>
																			<div class="grid-list-price"> <span class="cm-reload-7777" id="old_price_update_7777"> <!--old_price_update_7777--></span> <span class="cm-reload-7777 price-update" id="price_update_7777"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7777"><span class="price-num">₹</span><span id="sec_discounted_price_7777" class="price-num">1,990</span></span>
																				<!--price_update_7777-->
																				</span> <span class="cm-reload-7777" id="line_discount_update_7777"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7777--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/38/H0APsst_dpuk-lb.jpg" data-src="../images/thumbnails/300/380/detailed/38/H0APsst_dpuk-lb.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7777" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7777][product_id]" value="7777">
																				<input type="hidden" class="product-sku-code" value="BC161">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Greyish Blue Black Checks</h1>
																							<p class="product-sku">BC161</p>
																							<div class="price-wrap"> <span class="cm-reload-7777" id="old_price_update_7777"> <!--old_price_update_7777--></span> <span class="cm-reload-7777 price-update" id="price_update_7777"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7777"><span class="price-num">₹</span><span id="sec_discounted_price_7777" class="price-num">1,990</span></span>
																								<!--price_update_7777-->
																								</span> <span class="cm-reload-7777" id="line_discount_update_7777"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7777--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Twill</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Single Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count">50/1 x 50/1</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Light
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7777" name="product_data[7777][amount]" value="1">
																					<div class="cm-reload-7777" id="product_options_update_7777">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7777_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7777">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7777_6785">
																									<label id="option_description_7777_6785" for="option_7777_6785" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7777_6785" class="form-control " rows="3" name="product_data[7777][product_options][6785]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7777-->
																					</div>
																					<div class="cm-reload-7777  add-to-cart-container" id="add_to_cart_update_7777">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7777">
																							<button id="button_cart_7777" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7777]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7777-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="2040.000000" data-id="7752" data-value="BD019" data-readable-value="BD019">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Medium Wash Sateen Modal</span>
																			<div class="grid-list-price"> <span class="cm-reload-7752" id="old_price_update_7752"> <!--old_price_update_7752--></span> <span class="cm-reload-7752 price-update" id="price_update_7752"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7752"><span class="price-num">₹</span><span id="sec_discounted_price_7752" class="price-num">2,040</span></span>
																				<!--price_update_7752-->
																				</span> <span class="cm-reload-7752" id="line_discount_update_7752"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7752--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/39/c3okoHP_dcuo-60.jpg" data-src="../images/thumbnails/300/380/detailed/39/c3okoHP_dcuo-60.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7752" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7752][product_id]" value="7752">
																				<input type="hidden" class="product-sku-code" value="BD019">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Medium Wash Sateen Modal</h1>
																							<p class="product-sku">BD019</p>
																							<div class="price-wrap"> <span class="cm-reload-7752" id="old_price_update_7752"> <!--old_price_update_7752--></span> <span class="cm-reload-7752 price-update" id="price_update_7752"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7752"><span class="price-num">₹</span><span id="sec_discounted_price_7752" class="price-num">2,040</span></span>
																								<!--price_update_7752-->
																								</span> <span class="cm-reload-7752" id="line_discount_update_7752"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7752--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Denim</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Single Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count">70/1 x 70/1</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7752" name="product_data[7752][amount]" value="1">
																					<div class="cm-reload-7752" id="product_options_update_7752">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7752_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7752">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7752_6765">
																									<label id="option_description_7752_6765" for="option_7752_6765" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7752_6765" class="form-control " rows="3" name="product_data[7752][product_options][6765]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7752-->
																					</div>
																					<div class="cm-reload-7752  add-to-cart-container" id="add_to_cart_update_7752">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7752">
																							<button id="button_cart_7752" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7752]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7752-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="2090.000000" data-id="7742" data-value="BD017" data-readable-value="BD017">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Medium wash triangle print</span>
																			<div class="grid-list-price"> <span class="cm-reload-7742" id="old_price_update_7742"> <!--old_price_update_7742--></span> <span class="cm-reload-7742 price-update" id="price_update_7742"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7742"><span class="price-num">₹</span><span id="sec_discounted_price_7742" class="price-num">2,090</span></span>
																				<!--price_update_7742-->
																				</span> <span class="cm-reload-7742" id="line_discount_update_7742"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7742--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/39/9KHvAGKeB_0inn-1w.jpg" data-src="../images/thumbnails/300/380/detailed/39/9KHvAGKeB_0inn-1w.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7742" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7742][product_id]" value="7742">
																				<input type="hidden" class="product-sku-code" value="BD017">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Medium wash triangle print</h1>
																							<p class="product-sku">BD017</p>
																							<div class="price-wrap"> <span class="cm-reload-7742" id="old_price_update_7742"> <!--old_price_update_7742--></span> <span class="cm-reload-7742 price-update" id="price_update_7742"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7742"><span class="price-num">₹</span><span id="sec_discounted_price_7742" class="price-num">2,090</span></span>
																								<!--price_update_7742-->
																								</span> <span class="cm-reload-7742" id="line_discount_update_7742"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7742--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Denim</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Single Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count">70/1 x 70/1</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7742" name="product_data[7742][amount]" value="1">
																					<div class="cm-reload-7742" id="product_options_update_7742">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7742_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7742">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7742_6755">
																									<label id="option_description_7742_6755" for="option_7742_6755" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7742_6755" class="form-control " rows="3" name="product_data[7742][product_options][6755]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7742-->
																					</div>
																					<div class="cm-reload-7742  add-to-cart-container" id="add_to_cart_update_7742">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7742">
																							<button id="button_cart_7742" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7742]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7742-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="2090.000000" data-id="7741" data-value="BD016" data-readable-value="BD016">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Ice Wash Polka</span>
																			<div class="grid-list-price"> <span class="cm-reload-7741" id="old_price_update_7741"> <!--old_price_update_7741--></span> <span class="cm-reload-7741 price-update" id="price_update_7741"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7741"><span class="price-num">₹</span><span id="sec_discounted_price_7741" class="price-num">2,090</span></span>
																				<!--price_update_7741-->
																				</span> <span class="cm-reload-7741" id="line_discount_update_7741"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7741--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/39/t6hwOl4_mcn4-ac.jpg" data-src="../images/thumbnails/300/380/detailed/39/t6hwOl4_mcn4-ac.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7741" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7741][product_id]" value="7741">
																				<input type="hidden" class="product-sku-code" value="BD016">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Ice Wash Polka</h1>
																							<p class="product-sku">BD016</p>
																							<div class="price-wrap"> <span class="cm-reload-7741" id="old_price_update_7741"> <!--old_price_update_7741--></span> <span class="cm-reload-7741 price-update" id="price_update_7741"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7741"><span class="price-num">₹</span><span id="sec_discounted_price_7741" class="price-num">2,090</span></span>
																								<!--price_update_7741-->
																								</span> <span class="cm-reload-7741" id="line_discount_update_7741"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7741--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Denim</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Single Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count">70/1 x 70/1</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7741" name="product_data[7741][amount]" value="1">
																					<div class="cm-reload-7741" id="product_options_update_7741">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7741_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7741">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7741_6754">
																									<label id="option_description_7741_6754" for="option_7741_6754" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7741_6754" class="form-control " rows="3" name="product_data[7741][product_options][6754]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7741-->
																					</div>
																					<div class="cm-reload-7741  add-to-cart-container" id="add_to_cart_update_7741">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7741">
																							<button id="button_cart_7741" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7741]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7741-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="2090.000000" data-id="7732" data-value="BD015" data-readable-value="BD015">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Medium blue polka</span>
																			<div class="grid-list-price"> <span class="cm-reload-7732" id="old_price_update_7732"> <!--old_price_update_7732--></span> <span class="cm-reload-7732 price-update" id="price_update_7732"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7732"><span class="price-num">₹</span><span id="sec_discounted_price_7732" class="price-num">2,090</span></span>
																				<!--price_update_7732-->
																				</span> <span class="cm-reload-7732" id="line_discount_update_7732"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7732--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/39/9KJ6uqzYp_bnpw-aa.jpg" data-src="../images/thumbnails/300/380/detailed/39/9KJ6uqzYp_bnpw-aa.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7732" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7732][product_id]" value="7732">
																				<input type="hidden" class="product-sku-code" value="BD015">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Medium blue polka</h1>
																							<p class="product-sku">BD015</p>
																							<div class="price-wrap"> <span class="cm-reload-7732" id="old_price_update_7732"> <!--old_price_update_7732--></span> <span class="cm-reload-7732 price-update" id="price_update_7732"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7732"><span class="price-num">₹</span><span id="sec_discounted_price_7732" class="price-num">2,090</span></span>
																								<!--price_update_7732-->
																								</span> <span class="cm-reload-7732" id="line_discount_update_7732"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7732--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Denim</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Single Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count">70/1 x 70/1</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7732" name="product_data[7732][amount]" value="1">
																					<div class="cm-reload-7732" id="product_options_update_7732">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7732_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7732">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7732_6740">
																									<label id="option_description_7732_6740" for="option_7732_6740" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7732_6740" class="form-control " rows="3" name="product_data[7732][product_options][6740]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7732-->
																					</div>
																					<div class="cm-reload-7732  add-to-cart-container" id="add_to_cart_update_7732">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7732">
																							<button id="button_cart_7732" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7732]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7732-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="1990.000000" data-id="7726" data-value="BC160" data-readable-value="BC160">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Brown Blue Twill Checks</span>
																			<div class="grid-list-price"> <span class="cm-reload-7726" id="old_price_update_7726"> <!--old_price_update_7726--></span> <span class="cm-reload-7726 price-update" id="price_update_7726"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7726"><span class="price-num">₹</span><span id="sec_discounted_price_7726" class="price-num">1,990</span></span>
																				<!--price_update_7726-->
																				</span> <span class="cm-reload-7726" id="line_discount_update_7726"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7726--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/39/H0APJOy_y0q3-qi.jpg" data-src="../images/thumbnails/300/380/detailed/39/H0APJOy_y0q3-qi.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7726" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7726][product_id]" value="7726">
																				<input type="hidden" class="product-sku-code" value="BC160">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Brown Blue Twill Checks</h1>
																							<p class="product-sku">BC160</p>
																							<div class="price-wrap"> <span class="cm-reload-7726" id="old_price_update_7726"> <!--old_price_update_7726--></span> <span class="cm-reload-7726 price-update" id="price_update_7726"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7726"><span class="price-num">₹</span><span id="sec_discounted_price_7726" class="price-num">1,990</span></span>
																								<!--price_update_7726-->
																								</span> <span class="cm-reload-7726" id="line_discount_update_7726"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7726--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Twill</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Single Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count">50/1 x 50/1</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Medium
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7726" name="product_data[7726][amount]" value="1">
																					<div class="cm-reload-7726" id="product_options_update_7726">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7726_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7726">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7726_6734">
																									<label id="option_description_7726_6734" for="option_7726_6734" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7726_6734" class="form-control " rows="3" name="product_data[7726][product_options][6734]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7726-->
																					</div>
																					<div class="cm-reload-7726  add-to-cart-container" id="add_to_cart_update_7726">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7726">
																							<button id="button_cart_7726" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7726]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7726-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="2590.000000" data-id="7717" data-value="PC087" data-readable-value="PC087">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Blue Red Checks</span>
																			<div class="grid-list-price"> <span class="cm-reload-7717" id="old_price_update_7717"> <!--old_price_update_7717--></span> <span class="cm-reload-7717 price-update" id="price_update_7717"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7717"><span class="price-num">₹</span><span id="sec_discounted_price_7717" class="price-num">2,590</span></span>
																				<!--price_update_7717-->
																				</span> <span class="cm-reload-7717" id="line_discount_update_7717"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7717--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/39/PC087.jpg" data-src="../images/thumbnails/300/380/detailed/39/PC087.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7717" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7717][product_id]" value="7717">
																				<input type="hidden" class="product-sku-code" value="PC087">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Blue Red Checks</h1>
																							<p class="product-sku">PC087</p>
																							<div class="price-wrap"> <span class="cm-reload-7717" id="old_price_update_7717"> <!--old_price_update_7717--></span> <span class="cm-reload-7717 price-update" id="price_update_7717"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7717"><span class="price-num">₹</span><span id="sec_discounted_price_7717" class="price-num">2,590</span></span>
																								<!--price_update_7717-->
																								</span> <span class="cm-reload-7717" id="line_discount_update_7717"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7717--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Poplin</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count">60/1 x 60/1</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Light
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7717" name="product_data[7717][amount]" value="1">
																					<div class="cm-reload-7717" id="product_options_update_7717">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7717_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7717">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7717_6725">
																									<label id="option_description_7717_6725" for="option_7717_6725" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7717_6725" class="form-control " rows="3" name="product_data[7717][product_options][6725]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7717-->
																					</div>
																					<div class="cm-reload-7717  add-to-cart-container" id="add_to_cart_update_7717">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7717">
																							<button id="button_cart_7717" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7717]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7717-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<a href="#" class="option new" data-price="2490.000000" data-id="7697" data-value="PT060" data-readable-value="PT060">
																	<div class="grid-hover hidden-xs hidden-sm">
																		<div class="vertical-align"> <span class="product-title">Green White Stripes</span>
																			<div class="grid-list-price"> <span class="cm-reload-7697" id="old_price_update_7697"> <!--old_price_update_7697--></span> <span class="cm-reload-7697 price-update" id="price_update_7697"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7697"><span class="price-num">₹</span><span id="sec_discounted_price_7697" class="price-num">2,490</span></span>
																				<!--price_update_7697-->
																				</span> <span class="cm-reload-7697" id="line_discount_update_7697"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7697--></span> </div>
																		</div>
																	</div>
																	<div class="new-overlay discount-overlay"> <span>NEW</span> </div> <img class=" cloudzoom lazyload " src="../images/thumbnails/150/190/detailed/38/H0AEFw6_3vm5-cc.jpg" data-src="../images/thumbnails/300/380/detailed/38/H0AEFw6_3vm5-cc.jpg" alt="" title="" width="150" height="190" style="width:150px; height:190px;"> <i class="fa fa-info-circle" data-plugin="lightbox"></i>
																	<div class="tooltip-content">
																		<div class="fabric-tooltip fabric-overlay">
																			<form action="#" method="post" name="product_form_7697" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle  cm-processed-form">
																				<input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*">
																				<input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=1517">
																				<input type="hidden" name="product_data[7697][product_id]" value="7697">
																				<input type="hidden" class="product-sku-code" value="PT060">
																				<div class="tab-content">
																					<div class="row">
																						<div class="col-md-12">
																							<h1 class="product-title">Green White Stripes</h1>
																							<p class="product-sku">PT060</p>
																							<div class="price-wrap"> <span class="cm-reload-7697" id="old_price_update_7697"> <!--old_price_update_7697--></span> <span class="cm-reload-7697 price-update" id="price_update_7697"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_price]" value="1"> <span class="price" id="line_discounted_price_7697"><span class="price-num">₹</span><span id="sec_discounted_price_7697" class="price-num">2,490</span></span>
																								<!--price_update_7697-->
																								</span> <span class="cm-reload-7697" id="line_discount_update_7697"> <input type="hidden" name="appearance[show_price_values]" value="1"> <input type="hidden" name="appearance[show_list_discount]" value="1"> <!--line_discount_update_7697--></span> </div>
																							<h1 class="united-states-hidden">Points</h1>
																							<p class="loyalty-points united-states-hidden">
																								You'll get <span>₹</span><span class="value">100.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
																							</p>
																							<h1>Description</h1>
																							<p class="fabric-description"></p>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Composition</h1>
																									<p class="composition">100% Cotton</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Weave</h1>
																									<p class="weave">Poplin</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Ply</h1>
																									<p class="ply">Double Ply</p>
																								</div>
																								<div class="col-md-5">
																									<h1>Count</h1>
																									<p class="count">2/100 x 1/50</p>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-5 col-md-offset-1">
																									<h1>Weight</h1>
																									<p class="weight">Light
																										<!--<i class="bsc-fabric-weight-"></i>--></p>
																								</div>
																								<div class="col-md-5">
																									<h1>Wash Care</h1>
																									<p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="shirt-customization-option hidden">
																					<input type="text" size="5" class="qty" id="qty_count_7697" name="product_data[7697][amount]" value="1">
																					<div class="cm-reload-7697" id="product_options_update_7697">
																						<input type="hidden" name="appearance[show_product_options]" value="1">
																						<input type="hidden" name="appearance[details_page]" value="1">
																						<input type="hidden" name="additional_info[get_icon]" value="1">
																						<input type="hidden" name="additional_info[get_detailed]" value="1">
																						<input type="hidden" name="additional_info[get_additional]" value="1">
																						<input type="hidden" name="additional_info[get_options]" value="1">
																						<input type="hidden" name="additional_info[get_discounts]" value="1">
																						<input type="hidden" name="additional_info[get_features]" value="1">
																						<input type="hidden" name="additional_info[get_extra]" value="">
																						<input type="hidden" name="additional_info[get_taxed_prices]" value="1">
																						<input type="hidden" name="additional_info[get_for_one_product]" value="">
																						<input type="hidden" name="additional_info[detailed_params]" value="1">
																						<input type="hidden" name="additional_info[features_display_on]" value="C">
																						<div class="row" id="option_7697_AOC">
																							<div class="cm-picker-product-options product-options" id="opt_7697">
																								<div class="form-group product-options-item product-list-field clearfix" id="opt_7697_6710">
																									<label id="option_description_7697_6710" for="option_7697_6710" class="col-lg-3 col-md-3 col-sm-3 control-label ">Customizations:</label>
																									<div class="col-lg-7 col-md-7 col-sm-7">
																										<textarea id="option_7697_6710" class="form-control " rows="3" name="product_data[7697][product_options][6710]"></textarea>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!--product_options_update_7697-->
																					</div>
																					<div class="cm-reload-7697  add-to-cart-container" id="add_to_cart_update_7697">
																						<input type="hidden" name="appearance[show_add_to_cart]" value="1">
																						<input type="hidden" name="appearance[separate_buttons]" value="1">
																						<input type="hidden" name="appearance[show_list_buttons]" value="1">
																						<input type="hidden" name="appearance[but_role]" value="big">
																						<input type="hidden" name="appearance[quick_view]" value="">
																						<div class="buttons-container" id="cart_add_block_7697">
																							<button id="button_cart_7697" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7697]">Add to cart</button>
																						</div>
																						<!--add_to_cart_update_7697-->
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</a>
																<div class="clearfix">
																	<div class="pagination-bottom text-center">
																		<nav>
																			<ul class="pagination">
																				<li class="disabled"> <a data-ca-scroll=".cm-pagination-container" class=""><span>Prev</span> </a> </li>
																				<li class="active"><a>1</a></li>
																				<li><a data-ca-scroll=".cm-pagination-container" href="#" data-ca-page="2" class="cm-history cm-ajax" data-ca-target-id="pagination_contents">2</a></li>
																				<li><a data-ca-scroll=".cm-pagination-container" href="#" data-ca-page="3" class="cm-history cm-ajax" data-ca-target-id="pagination_contents">3</a></li>
																				<li><a data-ca-scroll=".cm-pagination-container" href="#" data-ca-page="4" class="cm-history cm-ajax" data-ca-target-id="pagination_contents">4</a></li>
																				<li> <a data-ca-scroll=".cm-pagination-container" class=" cm-history cm-ajax" href="#" data-ca-page="2" data-ca-target-id="pagination_contents"><span>Next</span></a> </li>
																				<li><a data-ca-scroll=".cm-pagination-container" href="#" data-ca-page="5" class="cm-history hidden-phone cm-ajax" data-ca-target-id="pagination_contents">2 - 8</a> </li>
																			</ul>
																		</nav>
																	</div>
																	<!--pagination_contents-->
																</div>
															</div>
														</div>
														<div class="filters-failed hidden"><span>Uh oh, we couldn't find any fabrics that matched those filters.</span></div>
													</div>
												</div>
											</div>
											<div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
												<div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
											</div>
											<div class="ps-scrollbar-y-rail" style="top: 0px; height: 194px; right: 3px;">
												<div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 18px;"></div>
											</div>
										</div>
										<!--Shirt Custom design end-->
                                        <div class="customization-options sm" data-customization-group="collar" data-plugin="scrollbar"> <a class="close hidden-lg hidden-md"><i class="fa fa-angle-left"></i><span class="hidden-md hidden-lg">View Shirt</span></a>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h3 class="accordion-heading">Collar Style <span class="set pull-right" data-customization="collar"><span>Spread Eagle</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="collars horizontal-options clearfix" data-customization="collar" data-readable-customization="Collar" data-menu-set="true" data-toggle="true">
                                                            <a class="option clearfix active" data-value="spread-eagle" data-readable-value="Spread Eagle"> <i class="bsc-collars-spread-eagle pull-left"></i>
                                                                <div class="desc"> <em class="pull-right">2.75 in</em>
                                                                    <h1>Spread Eagle</h1>
                                                                    <p>The spread collar remains classic men's style like the pocket square, hat, and custom suit. Works best for a thin face. Collar bone provided to ensure a crisp collar.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="prince-charlie" data-readable-value="Prince Charlie"> <i class="bsc-collars-prince-charlie pull-left"></i>
                                                                <div class="desc"> <em class="pull-right">2.75 in</em>
                                                                    <h1>Prince Charlie <em>(Default)</em></h1>
                                                                    <p>Classic straight collar is best paired with a suit and tie. It was made famous by British Royalty. Collar bone provided to ensure a crisp collar.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="madmen" data-readable-value="Madmen"> <i class="bsc-collars-madmen pull-left"></i>
                                                                <div class="desc"> <em class="pull-right">3.25 in</em>
                                                                    <h1>Madmen</h1>
                                                                    <p>This cutaway collar can be worn with a tie to work or without a tie for after work drinks. While wearing a tie, we recommend you use a Windsor knot. Collar bone provided to ensure a crisp collar.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="bandhgala" data-readable-value="Bandhgala"> <i class="bsc-collars-bandhgala pull-left"></i>
                                                                <div class="desc"> <em class="pull-right">0.75 in</em>
                                                                    <h1>Bandhgala</h1>
                                                                    <p>The traditional Bandhgala for all the old school folks out there.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="hipster" data-readable-value="Hipster"> <i class="bsc-collars-hipster pull-left"></i>
                                                                <div class="desc"> <em class="pull-right">1.75 in</em>
                                                                    <h1>Hipster</h1>
                                                                    <p>Also known as the Skinny Collar, is a fairly recent phenomenon. Ideally worn without a tie, but if you must, choose a skinny one.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="hipster-rounded" data-readable-value="Hipster Rounded"> <i class="bsc-collars-hipster-rounded pull-left"></i>
                                                                <div class="desc"> <em class="pull-right">1.50 in</em>
                                                                    <h1>Hipster Rounded</h1>
                                                                    <p>Why be square? Our most unconventional collar is this skinny rounded collar.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="prepster" data-readable-value="Prepster"> <i class="bsc-collars-prepster pull-left"></i>
                                                                <div class="desc"> <em class="pull-right">1.75 in</em>
                                                                    <h1>Prepster</h1>
                                                                    <p>If there was ever a middle ground between the preppy and the hipster, it's this button down skinny collar.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="evil-pandit" data-readable-value="Evil Pandit"> <i class="bsc-collars-evil-pandit pull-left"></i>
                                                                <div class="desc"> <em class="pull-right">1.25 in</em>
                                                                    <h1>Evil Pandit</h1>
                                                                    <p>Better known as the Banded Collar, was made famous by such luminaries as Pandit Nehru & Dr. Evil. We, however, have a contemporary take on it.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="double" data-readable-value="Double"> <i class="bsc-collars-double pull-left"></i>
                                                                <div class="desc"> <em class="pull-right">2.75 in</em>
                                                                    <h1>Double</h1>
                                                                    <p>The double collar is all the rage today. One collar is just not enough.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="polo" data-readable-value="Polo"> <i class="bsc-collars-polo pull-left"></i>
                                                                <div class="desc"> <em class="pull-right">2.50 in</em>
                                                                    <h1>Polo</h1>
                                                                    <p>Better known as the Button Down Collar, is ideally paired with a sports jacket and loafers, but goes well with less dressy suits.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="concealed-polo" data-readable-value="Concealed Polo"> <i class="bsc-collars-concealed-polo pull-left"></i>
                                                                <div class="desc"> <em class="pull-right">2.75 in</em>
                                                                    <h1>Concealed Polo</h1>
                                                                    <p>The concealed polo collar employs hidden button loops under the collar.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="club" data-readable-value="Club"> <i class="bsc-collars-club pull-left"></i>
                                                                <div class="desc"> <em class="pull-right">2.75 in</em>
                                                                    <h1>Club</h1>
                                                                    <p>Made famous by NBA coach Pat Riley, the Club Collar goes well with bespoke shirts and dressy suits.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="ozwald-boateng" data-readable-value="Ozwald Boateng"> <i class="bsc-collars-ozwald-boateng pull-left"></i>
                                                                <div class="desc"> <em class="pull-right">2.75 in</em>
                                                                    <h1>Ozwald Boateng</h1>
                                                                    <p>Designed by it's namesake, perhaps the classiest of all our collar offerings.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="wing" data-readable-value="Wing"> <i class="bsc-collars-wing pull-left"></i>
                                                                <div class="desc">
                                                                    <h1>Wing</h1>
                                                                    <p>Save it for your wedding day. Exclusively worn with a bow tie and tux.</p>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading">Collar Contrast <span data-customization="collar-contrast" class="set pull-right"><span>None</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="collar-contrast no-padding-options clearfix" data-customization="collar-contrast" data-contrasts="collar-contrast-fabric" data-readable-customization="Collar Contrast Type">
                                                            <a class="option clearfix active" data-value="none" data-connected-customization-validate="false" data-connected-customization="collar-contrast-fabric" data-connected-customization-state="false" data-readable-value="None"> <i class="bsc-cross-none"></i>
                                                                <h1>None</h1> </a>
                                                            <a class="option clearfix" data-value="inside" data-connected-customization-validate="true" data-connected-customization="collar-contrast-fabric" data-connected-customization-state="true" data-readable-value="Inside"> <i class="bsc-collars-collar-contrast-fabric-contrast-inside"></i>
                                                                <h1>Inside</h1> </a>
                                                            <a class="option clearfix" data-value="full" data-connected-customization-validate="true" data-connected-customization="collar-contrast-fabric" data-connected-customization-state="true" data-readable-value="Full"> <i class="bsc-collars-collar-contrast-fabric-contrast-full"></i>
                                                                <h1>Full</h1> </a>
                                                        </div>
                                                        <div class="collar-contrast-fabric contrast-options clearfix mixitup" data-fabrics="container-fabric" data-customization="collar-contrast-fabric" style="display:none;" data-readable-customization="Collar Contrast Fabric"> <a class="secondary-toggle"><i class="fa fa-caret-right"></i><i style="display:none;" class="fa fa-caret-down"></i> Filters</a>
                                                            <div class="row filter-wrapper" style="display:none;">
                                                                <div class="col-md-12 hidden">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Material</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".fabric" checked>Fabric</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Color</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".blacks">Blacks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".blues">Blues</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".browns">Browns</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".golds">Golds</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".greens">Greens</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".greys">Greys</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".khakhis">Khakhis</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".oranges">Oranges</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".peach">Peach</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".pinks">Pinks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".purples">Purples</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".reds">Reds</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".whites">Whites</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".yellows">Yellows</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Pattern</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".checks">Checks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".stripes">Stripes</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".solids">Solids</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".prints">Prints</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12"> <a class="clear-all"><i class="fa fa-trash-o"></i> Clear All Filters</a> </div>
                                                            </div>
                                                            <div data-pager-element=".collar-contrast-pager" class="collar-contrast-pager pager-list clearfix"></div>
                                                            <div class="filters-failed"><span>Uh oh, we couldn't find any fabrics that matched those filters.</span></div>
                                                        </div>
                                                    </div> <a href="#" class="more-options"><span><em>More</em><em class="hidden">Less</em> Details <em>+</em><em class="hidden">-</em></span></a>
                                                    <h3 class="accordion-heading" data-more>Collar Top Button <span data-customization="collar-top-button" class="set pull-right"><span>Single</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel" data-more>
                                                        <div class="collar-button no-padding-options clearfix" data-customization="collar-top-button" data-readable-customization="Collar Top Button">
                                                            <a class="option clearfix active" data-value="single" data-readable-value="Single"> <i class="bsc-collars-collar-button-single"></i>
                                                                <h1>Single</h1> </a>
                                                            <a class="option clearfix" data-value="double" data-readable-value="Double"> <i class="bsc-collars-collar-button-double"></i>
                                                                <h1>Double</h1> </a>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading" data-more>Collar Stiffness <span data-customization="collar-stiffness" class="set pull-right"><span>Stiff</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel" data-more>
                                                        <div class="collar-stiffness no-padding-options clearfix" data-customization="collar-stiffness" data-render="false" data-readable-customization="Collar Stiffness">
                                                            <a class="option clearfix active" data-value="stiff" data-readable-value="Stiff"> <i class="bsc-stiffness-stiff"></i>
                                                                <h1>Stiff</h1> </a>
                                                            <a class="option clearfix" data-value="soft" data-readable-value="Soft"> <i class="bsc-stiffness-soft"></i>
                                                                <h1>Soft</h1> </a>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading" data-more>Collar Piping <span data-customization="collar-piping" class="set pull-right"><span>None</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel" data-more>
                                                        <div class="collar-piping no-padding-options clearfix" data-customization="collar-piping" data-contrasts="collar-piping-fabric" data-readable-customization="Collar Piping Type">
                                                            <a class="option clearfix active" data-value="none" data-connected-customization-validate="false" data-connected-customization="collar-piping-fabric" data-connected-customization-state="false" data-readable-value="None"> <i class="bsc-cross-none"></i>
                                                                <h1>None</h1> </a>
                                                            <a class="option clearfix hidden" data-value="collar" data-connected-customization-validate="true" data-connected-customization="collar-piping-fabric" data-connected-customization-state="true" data-readable-value="Fabric"> <i class="bsc-collars-collar-contrast-piping-contrast-yes"></i>
                                                                <h1>Collar Fabric</h1> </a>
                                                            <a class="option clearfix" data-value="collar" data-connected-customization-validate="true" data-connected-customization="collar-piping-fabric" data-connected-customization-state="true" data-readable-value="Twill Tape"> <i class="bsc-collars-collar-contrast-piping-contrast-yes"></i>
                                                                <h1>Collar Tape</h1> </a>
                                                        </div>
                                                        <div class="collar-piping-fabric contrast-options clearfix mixitup" data-fabrics="container-fabric" data-customization="collar-piping-fabric" style="display:none;" data-readable-customization="Collar Piping Fabric"> <a class="secondary-toggle"><i class="fa fa-caret-right"></i><i style="display:none;" class="fa fa-caret-down"></i> Filters</a>
                                                            <div class="row filter-wrapper" style="display:none;">
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Material</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".fabric">Fabric</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".tape">Tape</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Color</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".blacks">Blacks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".blues">Blues</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".browns">Browns</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".golds">Golds</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".greens">Greens</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".greys">Greys</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".khakhis">Khakhis</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".oranges">Oranges</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".peach">Peach</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".pinks">Pinks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".purples">Purples</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".reds">Reds</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".whites">Whites</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".yellows">Yellows</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Pattern</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".checks">Checks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".stripes">Stripes</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".solids">Solids</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".prints">Prints</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12"> <a class="clear-all"><i class="fa fa-trash-o"></i> Clear All Filters</a> </div>
                                                            </div>
                                                            <div class="fabric_piping_container"></div>
                                                            <a class="option twill-tape mix tape blacks whites reds" data-value="21" data-readable-value="Twill Tape 21"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-21.jpg" /> </a>
                                                            <a class="option twill-tape mix tape reds whites" data-value="26" data-readable-value="Twill Tape 26"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-26.jpg" /> </a>
                                                            <a class="option twill-tape mix tape reds blacks" data-value="28" data-readable-value="Twill Tape 28"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-28.jpg" /> </a>
                                                            <a class="option twill-tape mix tape whites blacks" data-value="29" data-readable-value="Twill Tape 29"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-29.jpg" /> </a>
                                                            <a class="option twill-tape mix tape blacks blues" data-value="30" data-readable-value="Twill Tape 30"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-30.jpg" /> </a>
                                                            <a class="option twill-tape mix tape yellows greens greys" data-value="33" data-readable-value="Twill Tape 33"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-33.jpg" /> </a>
                                                            <a class="option twill-tape mix tape yellows greens greys" data-value="34" data-readable-value="Twill Tape 34"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-34.jpg" /> </a>
                                                            <a class="option twill-tape mix tape whites blacks blues" data-value="36" data-readable-value="Twill Tape 36"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-36.jpg" /> </a>
                                                            <a class="option twill-tape mix tape greys blacks" data-value="37" data-readable-value="Twill Tape 37"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-37.jpg" /> </a>
                                                            <a class="option twill-tape mix tape khakhis blues reds" data-value="40" data-readable-value="Twill Tape 40"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-40.jpg" /> </a>
                                                            <div data-pager-element=".collar-piping-pager" class="collar-piping-pager pager-list clearfix"></div>
                                                            <div class="filters-failed"><span>Uh oh, we couldn't find any fabrics or tapes that matched those filters.</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="customization-options sm" data-customization-group="cuff" data-plugin="scrollbar"> <a class="close hidden-lg hidden-md"><i class="fa fa-angle-left"></i><span class="hidden-md hidden-lg">View Shirt</span></a>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h3 class="accordion-heading">Cuff Style <span class="set pull-right" data-customization="cuff"><span>Single Convertible</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="cuffs horizontal-options clearfix" data-customization="cuff" data-readable-customization="Cuff" data-deselect="false" data-toggle="true">
                                                            <a class="option clearfix active" data-value="single-convertible" data-readable-value="Single Convertible" data-connected-sub="cuff-shape" data-connected-sub-customization="angled,square" data-connected-sub-customization-state="true"> <i class="bsc-cuffs-single-convertible"></i>
                                                                <div class="desc">
                                                                    <h1>Single Convertible</h1>
                                                                    <p>Comes with the option of using cuff links.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="double-bond" data-readable-value="Double Bond" data-connected-sub="cuff-shape" data-connected-sub-customization="angled,square" data-connected-sub-customization-state="true"> <i class="bsc-cuffs-double-bond"></i>
                                                                <div class="desc">
                                                                    <h1>Double Bond</h1>
                                                                    <p>The double button cuff has been popularised by various 007s including Daniel Craig.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="french" data-readable-value="The French" data-connected-sub="cuff-shape" data-connected-sub-customization="angled,square" data-connected-sub-customization-state="true"> <i class="bsc-cuffs-french"></i>
                                                                <div class="desc">
                                                                    <h1>The French</h1>
                                                                    <p>The French cuff is typically seen on formal shirts. Pair it up with cuff links or a silk knot to complete the look.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="neopolitan" data-readable-value="The Neopolitan" data-connected-sub="cuff-shape" data-connected-sub-customization="angled,square" data-connected-sub-customization-state="false"> <i class="bsc-cuffs-neopolitan"></i>
                                                                <div class="desc">
                                                                    <h1>The Neopolitan</h1>
                                                                    <p>For the fashionably adventurous. The famous cuff as worn by Sean Connery's James Bond.</p>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading">Cuff Contrast <span data-customization="cuff-contrast" class="set pull-right"><span>None</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="cuff-contrast no-padding-options clearfix" data-customization="cuff-contrast" data-contrasts="cuff-contrast-fabric" data-readable-customization="Cuff Contrast Type">
                                                            <a class="option clearfix active" data-value="none" data-connected-customization-validate="false" data-connected-customization="cuff-contrast-fabric" data-connected-customization-state="false" data-readable-value="None"> <i class="bsc-cross-none"></i>
                                                                <h1>None</h1> </a>
                                                            <a class="option clearfix" data-value="inside" data-connected-customization-validate="true" data-connected-customization="cuff-contrast-fabric" data-connected-customization-state="true" data-readable-value="Inside"> <i class="bsc-cuffs-cuff-contrast-fabric-contrast-inside"></i>
                                                                <h1>Inside</h1> </a>
                                                            <a class="option clearfix" data-value="full" data-connected-customization-validate="true" data-connected-customization="cuff-contrast-fabric" data-connected-customization-state="true" data-readable-value="Full"> <i class="bsc-cuffs-cuff-contrast-fabric-contrast-outside"></i>
                                                                <h1>Full</h1> </a>
                                                        </div>
                                                        <div class="cuff-contrast-fabric contrast-options clearfix mixitup" data-fabrics="container-fabric" data-customization="cuff-contrast-fabric" style="display:none;" data-readable-customization="Cuff Contrast Fabric"> <a class="secondary-toggle"><i class="fa fa-caret-right"></i><i style="display:none;" class="fa fa-caret-down"></i> Filters</a>
                                                            <div class="row filter-wrapper" style="display:none;">
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Color</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".blacks">Blacks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".blues">Blues</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".browns">Browns</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".golds">Golds</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".greens">Greens</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".greys">Greys</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".khakhis">Khakhis</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".oranges">Oranges</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".peach">Peach</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".pinks">Pinks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".purples">Purples</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".reds">Reds</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".whites">Whites</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".yellows">Yellows</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Pattern</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".checks">Checks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".stripes">Stripes</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".solids">Solids</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".prints">Prints</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12"> <a class="clear-all"><i class="fa fa-trash-o"></i> Clear All Filters</a> </div>
                                                            </div>
                                                            <div data-pager-element=".cuff-contrast-pager" class="cuff-contrast-pager pager-list clearfix"></div>
                                                            <div class="filters-failed"><span>Uh oh, we couldn't find any fabrics that matched those filters.</span></div>
                                                        </div>
                                                    </div> <a href="#" class="more-options"><span><em>More</em><em class="hidden">Less</em> Details <em>+</em><em class="hidden">-</em></span></a>
                                                    <h3 class="accordion-heading" data-more>Cuff Shape <span class="set pull-right" data-customization="cuff-shape"><span>Rounded</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel" data-more>
                                                        <div class="cuff-shape no-padding-options clearfix" data-customization="cuff-shape" data-readable-customization="Cuff Shape">
                                                            <a class="option clearfix active" data-value="rounded" data-readable-value="Rounded"> <i class="bsc-cuffs-cuff-shape-round"></i>
                                                                <h1>Rounded</h1> </a>
                                                            <a class="option clearfix" data-value="angled" data-readable-value="Angled"> <i class="bsc-cuffs-cuff-shape-angle"></i>
                                                                <h1>Angled</h1> </a>
                                                            <a class="option clearfix" data-value="square" data-readable-value="Square"> <i class="bsc-cuffs-cuff-shape-square"></i>
                                                                <h1>Square</h1> </a>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading" data-more>Cuff Stiffness <span data-customization="cuff-stiffness" class="set pull-right"><span>Stiff</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel" data-more>
                                                        <div class="cuff-stiffness no-padding-options clearfix" data-customization="cuff-stiffness" data-render="false" data-readable-customization="Cuff Stiffness">
                                                            <a class="option clearfix active" data-value="stiff" data-readable-value="Stiff"> <i class="bsc-stiffness-stiff"></i>
                                                                <h1>Stiff</h1> </a>
                                                            <a class="option clearfix" data-value="soft" data-readable-value="Soft"> <i class="bsc-stiffness-soft"></i>
                                                                <h1>Soft</h1> </a>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading" data-more>Cuff Piping <span data-customization="cuff-piping" class="set pull-right"><span>None</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel" data-more>
                                                        <div class="cuff-piping no-padding-options clearfix" data-connected-customization-validate="false" data-customization="cuff-piping" data-contrasts="cuff-piping-fabric" data-readable-customization="Cuff Piping Type">
                                                            <a class="option clearfix active" data-value="none" data-connected-customization="cuff-piping-fabric" data-connected-customization-state="false" data-readable-value="None"> <i class="bsc-cross-none"></i>
                                                                <h1>None</h1> </a>
                                                            <a class="option clearfix" data-value="placket" data-connected-customization-validate="true" data-connected-customization="cuff-piping-fabric" data-connected-customization-state="true" data-readable-value="Placket"> <i class="bsc-cuffs-cuff-contrast-button-placket-piping-yes"></i>
                                                                <h1>Placket</h1> </a>
                                                        </div>
                                                        <div class="cuff-piping-fabric contrast-options clearfix mixitup" data-fabrics="container-fabric" data-customization="cuff-piping-fabric" style="display:none;" data-readable-customization="Cuff Piping Fabric"> <a class="secondary-toggle"><i class="fa fa-caret-right"></i><i style="display:none;" class="fa fa-caret-down"></i> Filters</a>
                                                            <div class="row filter-wrapper" style="display:none;">
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Material</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".fabric">Fabric</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".tape">Tape</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Color</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".blacks">Blacks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".blues">Blues</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".browns">Browns</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".golds">Golds</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".greens">Greens</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".greys">Greys</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".khakhis">Khakhis</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".oranges">Oranges</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".peach">Peach</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".pinks">Pinks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".purples">Purples</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".reds">Reds</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".whites">Whites</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".yellows">Yellows</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Pattern</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".checks">Checks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".stripes">Stripes</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".solids">Solids</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".prints">Prints</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12"> <a class="clear-all"><i class="fa fa-trash-o"></i> Clear All Filters</a> </div>
                                                            </div>
                                                            <a class="option twill-tape mix tape blacks whites reds" data-value="21" data-readable-value="Twill Tape 21"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-21.jpg" /> </a>
                                                            <a class="option twill-tape mix tape reds whites" data-value="26" data-readable-value="Twill Tape 26"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-26.jpg" /> </a>
                                                            <a class="option twill-tape mix tape reds blacks" data-value="28" data-readable-value="Twill Tape 28"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-28.jpg" /> </a>
                                                            <a class="option twill-tape mix tape whites blacks" data-value="29" data-readable-value="Twill Tape 29"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-29.jpg" /> </a>
                                                            <a class="option twill-tape mix tape blacks blues" data-value="30" data-readable-value="Twill Tape 30"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-30.jpg" /> </a>
                                                            <a class="option twill-tape mix tape yellows greens greys" data-value="33" data-readable-value="Twill Tape 33"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-33.jpg" /> </a>
                                                            <a class="option twill-tape mix tape yellows greens greys" data-value="34" data-readable-value="Twill Tape 34"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-34.jpg" /> </a>
                                                            <a class="option twill-tape mix tape whites blacks blues" data-value="36" data-readable-value="Twill Tape 36"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-36.jpg" /> </a>
                                                            <a class="option twill-tape mix tape greys blacks" data-value="37" data-readable-value="Twill Tape 37"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-37.jpg" /> </a>
                                                            <a class="option twill-tape mix tape khakhis blues reds" data-value="40" data-readable-value="Twill Tape 40"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-40.jpg" /> </a>
                                                            <div data-pager-element=".cuff-piping-pager" class="cuff-piping-pager pager-list clearfix"></div>
                                                            <div class="filters-failed"><span>Uh oh, we couldn't find any fabrics or tapes that matched those filters.</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="customization-options sm" data-customization-group="placket" data-plugin="scrollbar"> <a class="close hidden-lg hidden-md"><i class="fa fa-angle-left"></i><span class="hidden-md hidden-lg">View Shirt</span></a>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h3 class="accordion-heading">Placket Style <span class="set pull-right" data-customization="placket"><span>Regular</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="plackets horizontal-options clearfix" data-customization="placket" data-readable-customization="Placket" data-toggle="true">
                                                            <a class="option clearfix active" data-value="regular" data-readable-value="Regular" data-connected-sub="placket-contrast" data-connected-sub-customization="outside" data-connected-sub-customization-state="true"> <i class="bsc-placket-regular"></i>
                                                                <div class="desc">
                                                                    <h1>Regular</h1>
                                                                    <p>Seen on all traditional English and American shirts. Functionally, it adds stability to the front of the shirt.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="french" data-readable-value="French Front" data-connected-sub="placket-contrast" data-connected-sub-customization="outside" data-connected-sub-customization-state="false"> <i class="bsc-placket-french"></i>
                                                                <div class="desc">
                                                                    <h1>French Front</h1>
                                                                    <p>Lends a cleaner, plain front that gives the shirt a classier look.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="pencil" data-readable-value="Pencil" data-connected-sub="placket-contrast" data-connected-sub-customization="outside" data-connected-sub-customization-state="true"> <i class="bsc-placket-pencil"></i>
                                                                <div class="desc">
                                                                    <h1>Pencil</h1>
                                                                    <p>The Pencil placket is a skinny version of the Regular placket. All the rage nowadays.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="concealed" data-readable-value="Concealed" data-connected-sub="placket-contrast" data-connected-sub-customization="outside" data-connected-sub-customization-state="true"> <i class="bsc-placket-concealed"></i>
                                                                <div class="desc">
                                                                    <h1>Concealed</h1>
                                                                    <p>Here an extra strip of fabric covers the buttons. Seen mainly on bespoke shirts.</p>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div> <a href="#" class="more-options"><span><em>More</em><em class="hidden">Less</em> Details <em>+</em><em class="hidden">-</em></span></a>
                                                    <h3 class="accordion-heading" data-more>Placket Button<span data-customization="placket-button" class="set pull-right"><span>Single</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="placket-button no-padding-options clearfix" data-customization="placket-button" data-readable-customization="Placket Button" data-toggle="true">
                                                            <a class="option clearfix active" data-value="single" data-readable-value="Single"> <i class="bsc-buttons-button-style-single"></i>
                                                                <h1>Single</h1> </a>
                                                            <a class="option clearfix" data-value="double" data-readable-value="Double"> <i class="bsc-buttons-button-style-double"></i>
                                                                <h1>Double</h1> </a>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading" data-more>Placket Contrast <span data-customization="placket-contrast" class="set pull-right"><span>None</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel" data-more>
                                                        <div class="placket-contrast no-padding-options clearfix" data-customization="placket-contrast" data-contrasts="placket-contrast-fabric" data-readable-customization="Placket Contrast Type">
                                                            <a class="option clearfix active" data-value="none" data-connected-customization-validate="false" data-connected-customization="placket-contrast-fabric" data-connected-customization-state="false" data-readable-value="None"> <i class="bsc-cross-none"></i>
                                                                <h1>None</h1> </a>
                                                            <a class="option clearfix" data-value="inside" data-connected-customization-validate="true" data-connected-customization="placket-contrast-fabric" data-connected-customization-state="true" data-readable-value="Inside"> <i class="bsc-placket-contrast-fabric-button-placket"></i>
                                                                <h1>Inside</h1> </a>
                                                            <a class="option clearfix" data-value="outside" data-connected-customization-validate="true" data-connected-customization="placket-contrast-fabric" data-connected-customization-state="true" data-readable-value="Outside"> <i class="bsc-placket-contrast-fabric-outside-placket"></i>
                                                                <h1>Outside</h1> </a>
                                                            <a class="option clearfix" data-value="edge" data-connected-customization-validate="true" data-connected-customization="placket-contrast-fabric" data-connected-customization-state="true" data-readable-value="Edge"> <i class="bsc-placket-contrast-fabric-placket-edge"></i>
                                                                <h1>Edge</h1> </a>
                                                        </div>
                                                        <div class="placket-contrast-fabric contrast-options clearfix mixitup" data-fabrics="container-fabric" data-customization="placket-contrast-fabric" style="display:none;" data-readable-customization="Placket Contrast Fabric"> <a class="secondary-toggle"><i class="fa fa-caret-right"></i><i style="display:none;" class="fa fa-caret-down"></i> Filters</a>
                                                            <div class="row filter-wrapper" style="display:none;">
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Color</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".blacks">Blacks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".blues">Blues</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".browns">Browns</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".golds">Golds</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".greens">Greens</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".greys">Greys</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".khakhis">Khakhis</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".oranges">Oranges</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".peach">Peach</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".pinks">Pinks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".purples">Purples</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".reds">Reds</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".whites">Whites</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".yellows">Yellows</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Pattern</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".checks">Checks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".stripes">Stripes</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".solids">Solids</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".prints">Prints</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12"> <a class="clear-all"><i class="fa fa-trash-o"></i> Clear All Filters</a> </div>
                                                            </div>
                                                            <div data-pager-element=".placket-contrast-pager" class="placket-contrast-pager pager-list clearfix"></div>
                                                            <div class="filters-failed"><span>Uh oh, we couldn't find any fabrics that matched those filters.</span></div>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading" data-more>Placket Piping <span data-customization="placket-piping" class="set pull-right"><span>None</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel" data-more>
                                                        <div class="placket-piping no-padding-options clearfix" data-customization="placket-piping" data-contrasts="placket-piping-fabric" data-readable-customization="Placket Piping Type">
                                                            <a class="option clearfix active" data-value="none" data-connected-customization-validate="false" data-connected-customization="placket-piping-fabric" data-connected-customization-state="false" data-readable-value="None"> <i class="bsc-cross-none"></i>
                                                                <h1>None</h1> </a>
                                                            <a class="option clearfix" data-value="inside" data-connected-customization-validate="true" data-connected-customization="placket-piping-fabric" data-connected-customization-state="true" data-readable-value="Inside"> <i class="bsc-placket-contrast-piping-button-placket"></i>
                                                                <h1>Inside</h1> </a>
                                                            <a class="option clearfix" data-value="outside" data-connected-customization-validate="true" data-connected-customization="placket-piping-fabric" data-connected-customization-state="true" data-readable-value="Outside"> <i class="bsc-placket-contrast-piping-button-placket"></i>
                                                                <h1>Outside</h1> </a>
                                                        </div>
                                                        <div class="placket-piping-fabric contrast-options clearfix" data-customization="placket-piping-fabric" style="display:none;" data-readable-customization="Placket Piping Fabric">
                                                            <a class="option twill-tape mix tape blacks whites reds" data-value="21" data-readable-value="Twill Tape 21"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-21.jpg" /> </a>
                                                            <a class="option twill-tape mix tape reds whites" data-value="26" data-readable-value="Twill Tape 26"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-26.jpg" /> </a>
                                                            <a class="option twill-tape mix tape reds blacks" data-value="28" data-readable-value="Twill Tape 28"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-28.jpg" /> </a>
                                                            <a class="option twill-tape mix tape whites blacks" data-value="29" data-readable-value="Twill Tape 29"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-29.jpg" /> </a>
                                                            <a class="option twill-tape mix tape blacks blues" data-value="30" data-readable-value="Twill Tape 30"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-30.jpg" /> </a>
                                                            <a class="option twill-tape mix tape yellows greens greys" data-value="33" data-readable-value="Twill Tape 33"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-33.jpg" /> </a>
                                                            <a class="option twill-tape mix tape yellows greens greys" data-value="34" data-readable-value="Twill Tape 34"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-34.jpg" /> </a>
                                                            <a class="option twill-tape mix tape whites blacks blues" data-value="36" data-readable-value="Twill Tape 36"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-36.jpg" /> </a>
                                                            <a class="option twill-tape mix tape greys blacks" data-value="37" data-readable-value="Twill Tape 37"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-37.jpg" /> </a>
                                                            <a class="option twill-tape mix tape khakhis blues reds" data-value="40" data-readable-value="Twill Tape 40"> <img class="lazyload" style="width:42px; height:149px;" src="#" data-src="../design/themes/bsc/media/images/customizations/tapes/twill-tape-40.jpg" /> </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="customization-options sm" data-customization-group="monogram" data-plugin="scrollbar"> <a class="close hidden-lg hidden-md"><i class="fa fa-angle-left"></i><span class="hidden-md hidden-lg">View Shirt</span></a>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h3 class="accordion-heading">Monogram<span class="set pull-right" data-customization="monogram"><span>None</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="monogram no-padding-options clearfix" data-customization="monogram" data-readable-customization="Monogram" data-contrasts="monogram-color">
                                                            <a class="option clearfix active" data-value="none" data-readable-value="None" data-connected-customization-validate="false" data-connected-customization="monogram-color,monogram-text" data-connected-customization-state="false"> <i class="bsc-cross-none"></i>
                                                                <h1>None</h1> </a>
                                                            <a class="option clearfix" data-value="collar" data-readable-value="Collar" data-connected-customization-validate="true" data-connected-customization="monogram-color,monogram-text" data-connected-customization-state="true"> <i class="bsc-monogram-yes-collar"></i>
                                                                <h1>Collar</h1> </a>
                                                            <a class="option clearfix div" data-value="cuff" data-readable-value="Cuff" data-connected-customization-validate="true" data-connected-customization="monogram-color,monogram-text" data-connected-customization-state="true" data-customization="monogram-cuff"> <i class="bsc-monogram-yes-left-cuff"></i>
                                                                <h1>Cuff</h1> </a>
                                                            <a class="option clearfix hidden" data-value="rib" data-readable-value="Rib" data-connected-customization-validate="true" data-connected-customization="monogram-color,monogram-text" data-connected-customization-state="true"> <i class="bsc-monogram-yes-body"></i>
                                                                <h1>Rib</h1> </a>
                                                        </div>
                                                        <div class="monogram no-padding-options clearfix" data-customization="monogram-text" data-readable-customization="Monogram Text" style="display: none;">
                                                            <input type="text" class="form-control input-text monogram-text" data-customization="monogram-text" data-readable-customization="Monogram Text" placeholder="ABC" maxlength="3" /> </div>
                                                        <div class="monogram-colors thread-colors no-padding-options clearfix" data-customization="monogram-color" data-readable-customization="Monogram Color" style="display:none;">
                                                            <a class="option clearfix" data-value="000000" data-readable-value="Black"> <i class="bsc-cross-none"></i>
                                                                <h1>Black</h1> </a>
                                                            <a class="option clearfix" data-value="ffffff" data-readable-value="White"> <i class="bsc-cross-none"></i>
                                                                <h1>White</h1> </a>
                                                            <a class="option clearfix" data-value="000088" data-readable-value="Indigo"> <i class="bsc-cross-none"></i>
                                                                <h1>Indigo</h1> </a>
                                                            <a class="option clearfix" data-value="a2d6ee" data-readable-value="Light Blue"> <i class="bsc-cross-none"></i>
                                                                <h1>Light Blue</h1> </a>
                                                            <a class="option clearfix" data-value="e52b50" data-readable-value="Pink"> <i class="bsc-cross-none"></i>
                                                                <h1>Pink</h1> </a>
                                                            <a class="option clearfix" data-value="836435" data-readable-value="Brown"> <i class="bsc-cross-none"></i>
                                                                <h1>Brown</h1> </a>
                                                            <a class="option clearfix" data-value="797a75" data-readable-value="Grey"> <i class="bsc-cross-none"></i>
                                                                <h1>Grey</h1> </a>
                                                            <a class="option clearfix" data-value="67253e" data-readable-value="Maroon"> <i class="bsc-cross-none"></i>
                                                                <h1>Maroon</h1> </a>
                                                            <a class="option clearfix" data-value="3c2261" data-readable-value="Purple"> <i class="bsc-cross-none"></i>
                                                                <h1>Purple</h1> </a>
                                                            <a class="option clearfix" data-value="009347" data-readable-value="Green"> <i class="bsc-cross-none"></i>
                                                                <h1>Green</h1> </a>
                                                            <a class="option clearfix" data-value="ff0000" data-readable-value="Red"> <i class="bsc-cross-none"></i>
                                                                <h1>Red</h1> </a>
                                                            <a class="option clearfix" data-value="fdf0f1" data-readable-value="Light Pink"> <i class="bsc-cross-none"></i>
                                                                <h1>Light Pink</h1> </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="customization-options sm" data-customization-group="advanced" data-plugin="scrollbar"> <a class="close hidden-lg hidden-md"><i class="fa fa-angle-left"></i><span class="hidden-md hidden-lg">View Shirt</span></a>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h3 class="accordion-heading"><i class="bsc-customization-sleeve-bottom-cut"></i> Sleeve Style <span class="set pull-right" data-customization="sleeve"><span>Full</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="bottom-cut no-padding-options clearfix" data-customization="sleeve" data-readable-customization="Sleeve" data-toggle="true">
                                                            <a class="option clearfix active" data-value="full" data-readable-value="Full Sleeves" data-connected-customization="cuff,elbow-patch,monogram-cuff" data-connected-customization-state="true"> <i class="bsc-sleeve-full"></i>
                                                                <h1>Full</h1> </a>
                                                            <a class="option clearfix" data-value="roll-up" data-readable-value="Roll-up Sleeves" data-connected-customization="cuff,elbow-patch,monogram-cuff" data-connected-customization-state="false"> <i class="bsc-sleeve-roll-up"></i>
                                                                <h1>Roll-up</h1> </a>
                                                            <a class="option clearfix" data-value="half" data-readable-value="Half Sleeves" data-connected-customization="cuff,elbow-patch,monogram-cuff" data-connected-customization-state="false"> <i class="bsc-sleeve-half"></i>
                                                                <h1>Half</h1> </a>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading"><i class="bsc-customization-button-button-thread"></i> Button <span class="set pull-right" data-customization="button"><span>White</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="buttons no-padding-options buttons clearfix" data-customization="button" data-readable-customization="Button" data-toggle="true">
                                                            <a class="option clearfix active" data-value="button_mop white" data-readable-value="MOP White"> <i><img class="lazyload" style="width:64px;height:64px;" src="#" data-src="../design/themes/bsc/media/images/customizations/buttons/1-mother-of-pearl-white.jpg" /></i>
                                                                <h1>MOP White</h1> </a>
                                                            <a class="option clearfix" data-value="button_mop black" data-readable-value="MOP Black"> <i><img class="lazyload" style="width:64px;height:64px;" src="#" data-src="../design/themes/bsc/media/images/customizations/buttons/2-mother-of-pearl-black.jpg" /></i>
                                                                <h1>MOP Black</h1> </a>
                                                            <a class="option clearfix" data-value="button_ecru" data-readable-value="Ecru"> <i><img class="lazyload" style="width:64px;height:64px;" src="#" data-src="../design/themes/bsc/media/images/customizations/buttons/3-ecru.jpg" /></i>
                                                                <h1>Ecru</h1> </a>
                                                            <a class="option clearfix" data-value="button_black" data-readable-value="Black"> <i><img class="lazyload" style="width:64px;height:64px;" src="#" data-src="../design/themes/bsc/media/images/customizations/buttons/4-black.jpg" /></i>
                                                                <h1>Black</h1> </a>
                                                            <a class="option clearfix" data-value="button_grey" data-readable-value="Grey"> <i><img class="lazyload" style="width:64px;height:64px;" src="#" data-src="../design/themes/bsc/media/images/customizations/buttons/5-grey.jpg" /></i>
                                                                <h1>Grey</h1> </a>
                                                            <a class="option clearfix" data-value="button_light_blue" data-readable-value="Light Blue"> <i><img class="lazyload" style="width:64px;height:64px;" src="#" data-src="../design/themes/bsc/media/images/customizations/buttons/6-light-blue.jpg" /></i>
                                                                <h1>Light Blue</h1> </a>
                                                            <a class="option clearfix" data-value="button_navy_blue" data-readable-value="Navy"> <i><img class="lazyload" style="width:64px;height:64px;" src="#" data-src="../design/themes/bsc/media/images/customizations/buttons/7-navy.jpg" /></i>
                                                                <h1>Navy</h1> </a>
                                                            <a class="option clearfix" data-value="button_brown" data-readable-value="Brown"> <i><img class="lazyload" style="width:64px;height:64px;" src="#" data-src="../design/themes/bsc/media/images/customizations/buttons/8-brown.jpg" /></i>
                                                                <h1>Brown</h1> </a>
                                                            <a class="option clearfix" data-value="button_red" data-readable-value="Red"> <i><img class="lazyload" style="width:64px;height:64px;" src="#" data-src="../design/themes/bsc/media/images/customizations/buttons/9-red.jpg" /></i>
                                                                <h1>Red</h1> </a>
                                                            <a class="option clearfix hidden" data-value="button_yellow" data-readable-value="Yellow"> <i><img class="lazyload" style="width:64px;height:64px;" src="#" data-src="../design/themes/bsc/media/images/customizations/buttons/10-yellow.jpg" /></i>
                                                                <h1>Yellow</h1> </a>
                                                            <a class="option clearfix" data-value="button_pink" data-readable-value="Light Pink"> <i><img class="lazyload" style="width:64px;height:64px;" src="#" data-src="../design/themes/bsc/media/images/customizations/buttons/11-light-pink.jpg" /></i>
                                                                <h1>Light Pink</h1> </a>
                                                            <a class="option clearfix" data-value="button_green" data-readable-value="Green"> <i><img class="lazyload" style="width:64px;height:64px;" src="#" data-src="../design/themes/bsc/media/images/customizations/buttons/12-green.jpg" /></i>
                                                                <h1>Green</h1> </a>
                                                            <a class="option clearfix" data-value="button_horn" data-readable-value="Horn"> <i><img class="lazyload" style="width:64px;height:64px;" src="#" data-src="../design/themes/bsc/media/images/customizations/buttons/13-horn.jpg" /></i>
                                                                <h1>Horn</h1> </a>
                                                            <a class="option clearfix" data-value="button_chalk" data-readable-value="Chalk"> <i><img class="lazyload" style="width:64px;height:64px;" src="#" data-src="../design/themes/bsc/media/images/customizations/buttons/14-chalk.jpg" /></i>
                                                                <h1>Chalk</h1> </a>
                                                            <a class="option clearfix" data-value="button_wood" data-readable-value="Wood"> <i><img class="lazyload" style="width:64px;height:64px;" src="#" data-src="../design/themes/bsc/media/images/customizations/buttons/15-wood.jpg" /></i>
                                                                <h1>Wood</h1> </a>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading"><i class="bsc-customization-button-button-thread"></i> Button Thread Colour <span class="set pull-right" data-customization="thread-color"><span>White</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="thread-colors no-padding-options clearfix" data-customization="thread-color" data-readable-customization="Button Thread Color" data-toggle="true">
                                                            <a class="option clearfix" data-value="000000" data-readable-value="Black"> <i class="bsc-cross-none"></i>
                                                                <h1>Black</h1> </a>
                                                            <a class="option clearfix active" data-value="ffffff" data-readable-value="White"> <i class="bsc-cross-none"></i>
                                                                <h1>White</h1> </a>
                                                            <a class="option clearfix" data-value="000088" data-readable-value="Indigo"> <i class="bsc-cross-none"></i>
                                                                <h1>Indigo</h1> </a>
                                                            <a class="option clearfix" data-value="a2d6ee" data-readable-value="Light Blue"> <i class="bsc-cross-none"></i>
                                                                <h1>Light Blue</h1> </a>
                                                            <a class="option clearfix" data-value="e52b50" data-readable-value="Pink"> <i class="bsc-cross-none"></i>
                                                                <h1>Pink</h1> </a>
                                                            <a class="option clearfix" data-value="836435" data-readable-value="Brown"> <i class="bsc-cross-none"></i>
                                                                <h1>Brown</h1> </a>
                                                            <a class="option clearfix" data-value="797a75" data-readable-value="Grey"> <i class="bsc-cross-none"></i>
                                                                <h1>Grey</h1> </a>
                                                            <a class="option clearfix" data-value="67253e" data-readable-value="Maroon"> <i class="bsc-cross-none"></i>
                                                                <h1>Maroon</h1> </a>
                                                            <a class="option clearfix" data-value="3c2261" data-readable-value="Purple"> <i class="bsc-cross-none"></i>
                                                                <h1>Purple</h1> </a>
                                                            <a class="option clearfix" data-value="009347" data-readable-value="Green"> <i class="bsc-cross-none"></i>
                                                                <h1>Green</h1> </a>
                                                            <a class="option clearfix" data-value="ff0000" data-readable-value="Red"> <i class="bsc-cross-none"></i>
                                                                <h1>Red</h1> </a>
                                                            <a class="option clearfix" data-value="fdf0f1" data-readable-value="Light Pink"> <i class="bsc-cross-none"></i>
                                                                <h1>Light Pink</h1> </a>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading" data-customization="pocket"><i class="bsc-customization-pocket"></i> Pocket Style <span class="set pull-right" data-customization="pocket"><span>None</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="pockets no-padding-options clearfix" data-customization="pocket" data-readable-customization="Pocket Style" data-toggle="true">
                                                            <a class="option clearfix active" data-value="none" data-readable-value="None" data-connected-customization="pocket-contrast" data-connected-customization-state="false"> <i class="bsc-cross-none"></i>
                                                                <h1>None</h1> </a>
                                                            <a class="option clearfix" data-value="single-angled" data-readable-value="Single Angled" data-connected-customization="pocket-contrast" data-connected-customization-state="true"> <i class="bsc-pocket-single-angled"></i>
                                                                <h1>Single Angled</h1> </a>
                                                            <a class="option clearfix" data-value="single-flap" data-readable-value="Single Flap" data-connected-customization="pocket-contrast" data-connected-customization-state="true"> <i class="bsc-pocket-single-flap"></i>
                                                                <h1>Single Flap</h1> </a>
                                                            <a class="option clearfix" data-value="double-flap" data-readable-value="Double Flap" data-connected-customization="pocket-contrast" data-connected-customization-state="true"> <i class="bsc-pocket-double-flap"></i>
                                                                <h1>Double Flap</h1> </a>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading" data-customization="pocket-contrast" style="display:none;"><i class="bsc-customization-pocket"></i> Pocket Contrast <span data-customization="pocket-contrast" class="set pull-right"><span>None</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="pocket-contrast no-padding-options clearfix" data-customization="pocket-contrast" data-contrasts="pocket-contrast-fabric" data-readable-customization="Pocket Contrast Type" style="display:none;">
                                                            <a class="option clearfix active" data-value="none" data-connected-customization-validate="false" data-connected-customization="pocket-contrast-fabric" data-connected-customization-state="false" data-readable-value="None"> <i class="bsc-cross-none"></i>
                                                                <h1>None</h1> </a>
                                                            <a class="option clearfix" data-value="yes" data-connected-customization-validate="true" data-connected-customization="pocket-contrast-fabric" data-connected-customization-state="true" data-readable-value="Yes"> <i class="bsc-pocket-contrast"></i>
                                                                <h1>Yes</h1> </a>
                                                        </div>
                                                        <div class="pocket-contrast-fabric contrast-options clearfix mixitup" data-fabrics="base" data-customization="pocket-contrast-fabric" style="display:none;" data-readable-customization="Pocket Contrast Fabric">
                                                            <div class="basic-filters filters col-3"> <a data-filter-element=".pocket-contrast-filter" class="pocket-contrast-filter btn active" data-filter=".fabric">Fabric</a> <a data-filter-element=".pocket-contrast-filter" class="pocket-contrast-filter btn" data-filter=".leather">Leather</a> <a data-filter-element=".pocket-contrast-filter" class="pocket-contrast-filter btn" data-filter=".suede">Suede</a> </div>
                                                            <div class="fabric_contrasts_container"></div>
                                                            <a class="option mix leather blacks" data-value="L1" data-readable-value="L1"> <span class="type">Leather</span> <img class="lazyload" style="width:150px; height:150px;" src="#" data-src="../design/themes/bsc/media/images/customizations/leather/L1.jpg" /> </a>
                                                            <a class="option mix leather yellows golds" data-value="L2" data-readable-value="L2"> <span class="type">Leather</span> <img class="lazyload" style="width:150px; height:150px;" src="#" data-src="../design/themes/bsc/media/images/customizations/leather/L2.jpg" /> </a>
                                                            <a class="option mix leather browns" data-value="L3" data-readable-value="L3"> <span class="type">Leather</span> <img class="lazyload" style="width:150px; height:150px;" src="#" data-src="../design/themes/bsc/media/images/customizations/leather/L3.jpg" /> </a>
                                                            <a class="option mix leather blues" data-value="L4" data-readable-value="L4"> <span class="type">Leather</span> <img class="lazyload" style="width:150px; height:150px;" src="#" data-src="../design/themes/bsc/media/images/customizations/leather/L4.jpg" /> </a>
                                                            <a class="option mix leather blues" data-value="L5" data-readable-value="L5"> <span class="type">Leather</span> <img class="lazyload" style="width:150px; height:150px;" src="#" data-src="../design/themes/bsc/media/images/customizations/leather/L5.jpg" /> </a>
                                                            <a class="option mix suede blacks" data-value="S1" data-readable-value="S1"> <span class="type">Suede</span> <img class="lazyload" style="width:150px; height:150px;" src="#" data-src="../design/themes/bsc/media/images/customizations/suede/S1.jpg" /> </a>
                                                            <a class="option mix suede greens" data-value="S2" data-readable-value="S2"> <span class="type">Suede</span> <img class="lazyload" style="width:150px; height:150px;" src="#" data-src="../design/themes/bsc/media/images/customizations/suede/S2.jpg" /> </a>
                                                            <a class="option mix suede golds browns yellows" data-value="S3" data-readable-value="S3"> <span class="type">Suede</span> <img class="lazyload" style="width:150px; height:150px;" src="#" data-src="../design/themes/bsc/media/images/customizations/suede/S3.jpg" /> </a>
                                                            <a class="option mix suede blues" data-value="S4" data-readable-value="S4"> <span class="type">Suede</span> <img class="lazyload" style="width:150px; height:150px;" src="#" data-src="../design/themes/bsc/media/images/customizations/suede/S4.jpg" /> </a>
                                                            <a class="option mix suede pinks" data-value="S5" data-readable-value="S5"> <span class="type">Suede</span> <img class="lazyload" style="width:150px; height:150px;" src="#" data-src="../design/themes/bsc/media/images/customizations/suede/S5.jpg" /> </a>
                                                            <div data-pager-element=".pocket-contrast-pager" class="pocket-contrast-pager pager-list clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading"><i class="bsc-customization-back-style"></i> Back Style <span class="set pull-right" data-customization="back"><span>None</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="epaulettes horizontal-options clearfix" data-customization="back" data-readable-customization="Back" data-toggle="true">
                                                            <a class="option clearfix active" data-value="none" data-readable-value="None"> <i class="bsc-back-style-no-pleats"></i>
                                                                <div class="desc">
                                                                    <h1>None</h1>
                                                                    <p>Typically, custom made shirts are tailored perfectly to the wearer's body and hence do not require pleats.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="box" data-readable-value="Box"> <i class="bsc-back-style-box-pleats"></i>
                                                                <div class="desc">
                                                                    <h1>Box</h1>
                                                                    <p>Consists of two pleats spaced 1.5 inches apart at the centre. This pleat is commonly seen in ready-to-wear shirts and allows for freedom of movement.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="side" data-readable-value="Side"> <i class="bsc-back-style-side-pleats"></i>
                                                                <div class="desc">
                                                                    <h1>Side</h1>
                                                                    <p>Lie halfway between the edges and the centre of the back. They better align with the actual shape of the back and thus fit most men better.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="darts" data-readable-value="Darts"> <i class="bsc-back-style-back-darts"></i>
                                                                <div class="desc">
                                                                    <h1>Darts</h1>
                                                                    <p>Give the shirt with a more fitted look. Lately, they're used more as a design element.</p>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading"><i class="bsc-customization-sleeve-bottom-cut"></i> Bottom Cut <span class="set pull-right" data-customization="bottom-cut"><span>Rounded</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="bottom-cut horizontal-options clearfix" data-customization="bottom-cut" data-readable-customization="Bottom Cut" data-toggle="true">
                                                            <a class="option clearfix active" data-value="rounded" data-readable-value="Rounded"> <i class="bsc-bottom-cut-topaz"></i>
                                                                <div class="desc">
                                                                    <h1>Rounded</h1>
                                                                    <p>Slightly rounded on the sides, without showing off your love handles.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="tail" data-readable-value="Tail"> <i class="bsc-bottom-cut-tail"></i>
                                                                <div class="desc">
                                                                    <h1>Tail</h1>
                                                                    <p>Shirts with a longer tail, ensuring it stays tucked in at the back.</p>
                                                                </div>
                                                            </a>
                                                            <a class="option clearfix" data-value="straight" data-readable-value="Straight"> <i class="bsc-bottom-cut-straight"></i>
                                                                <div class="desc">
                                                                    <h1>Straight</h1>
                                                                    <p>Straight at the base, meant to be kept untucked, ideal for casual shirts.</p>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading" data-customization="elbow-patch"><i class="bsc-customization-others"></i> Elbow Patch <span class="set pull-right" data-customization="elbow-patch"><span>None</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="elbow-patchs no-padding-options clearfix" data-customization="elbow-patch" data-readable-customization="Elbow Patch" data-deselect="false" data-toggle="true">
                                                            <a class="option clearfix active" data-value="none" data-connected-customization-validate="false" data-connected-customization="elbow-patch-contrast-fabric" data-connected-customization-state="false" data-readable-value="None"> <i class="bsc-cross-none"></i>
                                                                <h1>None</h1> </a>
                                                            <a class="option clearfix" data-value="yes" data-connected-customization-validate="true" data-connected-customization="elbow-patch-contrast-fabric" data-connected-customization-state="true" data-readable-value="Yes"> <i class="bsc-customization-others"></i>
                                                                <h1>Yes</h1> </a>
                                                        </div>
                                                        <div class="elbow-patch-contrast-fabric contrast-options clearfix mixitup" data-fabrics="container-fabric-leather-suede" data-customization="elbow-patch-contrast-fabric" style="display:none;" data-readable-customization="Elbow Patch Fabric"> <a class="secondary-toggle"><i class="fa fa-caret-right"></i><i style="display:none;" class="fa fa-caret-down"></i> Filters</a>
                                                            <div class="row filter-wrapper" style="display:none;">
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Material</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".fabric">Fabric</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".leather">Leather</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".suede">Suede</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Color</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".blacks">Blacks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".blues">Blues</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".browns">Browns</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".golds">Golds</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".greens">Greens</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".greys">Greys</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".khakhis">Khakhis</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".oranges">Oranges</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".peach">Peach</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".pinks">Pinks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".purples">Purples</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".reds">Reds</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".whites">Whites</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".yellows">Yellows</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Pattern</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".checks">Checks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".stripes">Stripes</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".solids">Solids</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".prints">Prints</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12"> <a class="clear-all"><i class="fa fa-trash-o"></i> Clear All Filters</a> </div>
                                                            </div>
                                                            <div data-pager-element=".elbow-patch-contrast-pager" class="elbow-patch-contrast-pager pager-list clearfix"></div>
                                                            <div class="filters-failed"><span>Uh oh, we couldn't find any fabrics, leathers or suedes that matched those filters.</span></div>
                                                        </div>
                                                    </div>
                                                    <h3 class="accordion-heading"><i class="bsc-others-epaulettes-yes"></i> Shoulder Epaulette <span class="set pull-right" data-customization="epaulette"><span>None</span>&nbsp;&nbsp;<i class="closed fa fa-angle-down"></i><i class="open fa fa-angle-up"></i></span></h3>
                                                    <div class="accordion-panel">
                                                        <div class="epaulettes no-padding-options clearfix" data-customization="epaulette" data-readable-customization="Epaulette">
                                                            <a class="option clearfix active" data-value="none" data-connected-customization-validate="false" data-connected-customization="epaulette-contrast-fabric" data-connected-customization-state="false" data-readable-value="None"> <i class="bsc-cross-none"></i>
                                                                <h1>None</h1> </a>
                                                            <a class="option clearfix" data-value="yes" data-connected-customization-validate="true" data-connected-customization="epaulette-contrast-fabric" data-connected-customization-state="true" data-readable-value="Yes"> <i class="bsc-others-epaulettes-yes"></i>
                                                                <h1>Yes</h1> </a>
                                                        </div>
                                                        <div class="epaulette-contrast-fabric contrast-options clearfix mixitup" data-fabrics="container-fabric-leather-suede" data-customization="epaulette-contrast-fabric" style="display:none;" data-readable-customization="Epaulette Fabric"> <a class="secondary-toggle"><i class="fa fa-caret-right"></i><i style="display:none;" class="fa fa-caret-down"></i> Filters</a>
                                                            <div class="row filter-wrapper" style="display:none;">
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Material</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".fabric">Fabric</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".leather">Leather</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".suede">Suede</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Color</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".blacks">Blacks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".blues">Blues</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".browns">Browns</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".golds">Golds</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".greens">Greens</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".greys">Greys</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".khakhis">Khakhis</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".oranges">Oranges</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".peach">Peach</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".pinks">Pinks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".purples">Purples</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".reds">Reds</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".whites">Whites</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".yellows">Yellows</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="filter-set">
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Pattern</h1>
                                                                        <h1 class="sub-accordion-heading"><i class="fa fa-caret-right"></i><i class="fa fa-caret-down" style="display:none;"></i> Pattern</h1>
                                                                        <div class="options accordion-panel" style="display:none;">
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".checks">Checks</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".stripes">Stripes</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".solids">Solids</label>
                                                                            </a>
                                                                            <a class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" class="cm-product-filters-checkbox" value=".prints">Prints</label>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12"> <a class="clear-all"><i class="fa fa-trash-o"></i> Clear All Filters</a> </div>
                                                            </div>
                                                            <div data-pager-element=".epaulette-contrast-pager" class="epaulette-contrast-pager pager-list clearfix"></div>
                                                            <div class="filters-failed"><span>Uh oh, we couldn't find any fabrics, leathers or suedes that matched those filters.</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" data-state="false" data-error="" data-element="" id="customization-error" />
                                        <div class="modal fade page-modal" id="customization-error-modal" role="dialog" tabindex="-1">
                                            <div class="vertical-alignment-helper">
                                                <div class="modal-dialog vertical-align-center">
                                                    <div class="modal-content">
                                                        <div class="modal-body"> <i class="fa fa-exclamation-circle icon"></i>
                                                            <p class="message"></p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">Go to <span class="button"></span></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="generation-container">
                                            <div class="row">
                                                <div class="col-md-6 col-md-offset-6">
                                                    <div class="cart-options">
                                                        <a class="switch-view hidden-xs hidden-sm"> <i class="bsc-rotate-shirt"></i> </a>
                                                    </div>
                                                    <div class="shirt-container"> </div>
                                                    <div class="fabric-overlay">
                                                        <div class="tab-heading">
                                                            <h1 class="title-and-sku"><span class="title">&nbsp;</span> <span class="product-sku"></span></h1>
                                                            <h2 class="product-price hidden-xs hidden-sm">&nbsp;</h2><span class="tax-wrapper hidden-xs hidden-sm">&nbsp;&nbsp;+ TAX <a rel="shadowbox;height=225;width=380;player=inline" href="#tax-implication">(?)</a></span>&nbsp;&nbsp;
                                                            <div class="toggle"><i class="fa fa-angle-up" aria-hidden="true"></i><i class="fa fa-angle-down hidden" aria-hidden="true"></i></div>
                                                        </div>
                                                        <div class="tab-content">
                                                            <div class="row">
                                                                <div class="hidden-md hidden-lg col-xs-6">
                                                                    <h1>Price</h1>
                                                                    <p class="product-price"></p><span class="tax-wrapper hidden-md hidden-lg"> + TAX</span> </div>
                                                                <div class="col-md-12 col-xs-6 united-states-hidden">
                                                                    <h1>Points&nbsp;<span><a rel="shadowbox;height=590;width=650;player=inline" href="#loyalty-points" class="help">(?)</a></span></h1>
                                                                    <p class="loyalty-points">You'll get <span>₹</span><strong>-</strong> <span class="rupee-term">as cash back</span><span class="international-term">in points</span>.</p>
                                                                </div>
                                                            </div>
                                                            <h1>Description</h1>
                                                            <p class="fabric-description"></p>
                                                            <div class="row">
                                                                <div class="col-md-5 col-md-offset-1 col-xs-6">
                                                                    <h1>Composition</h1>
                                                                    <p class="composition"></p>
                                                                </div>
                                                                <div class="col-md-5 col-xs-6">
                                                                    <h1>Weave</h1>
                                                                    <p class="weave">-</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 col-md-offset-1 col-xs-6">
                                                                    <h1>Ply</h1>
                                                                    <p class="ply"></p>
                                                                </div>
                                                                <div class="col-md-5 col-xs-6">
                                                                    <h1>Count</h1>
                                                                    <p class="count"></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 col-md-offset-1 col-xs-6">
                                                                    <h1>Weight</h1>
                                                                    <p class="weight"><i class="bsc-fabric-weight-light"></i></p>
                                                                </div>
                                                                <div class="col-md-5 col-xs-6">
                                                                    <h1>Wash Care</h1>
                                                                    <p class="wash-care"><i class="bsc-fabric-wash-care-instructions"></i></p>
                                                                </div>
                                                            </div>
                                                            <div class="hidden-xs hidden-sm">
                                                                <p>
                                                                    <br>MACHINE OR HAND WASH IN COLD WATER / NO DRY CLEAN / NO TUMBLE DRY / NO IRON ON LEATHER</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
                <footer class="footer" id="tygh_footer">
                    <div class="container-fluid  footer-grid">
                        <div class="row">
                            <section class="col-lg-8 footer-menu hidden-xs hidden-sm">
                                <div class="row">
                                    <section class="col-lg-5 col-sm-12 col-xs-12 col-lg-3">
                                        <div class="footer-section footer-no-wysiwyg pull-left">
                                            <h4 class=""> <span>Shop Online</span> </h4>
                                            <div class="footer-general-body">
                                                <div class="wysiwyg-content">
                                                    <ul class="list-unstyled">
                                                        <li><a href="shirt.php">Design a Custom Made Shirt</a></li>
                                                        <li><a href="../shop-mens-shirts-online.php">Men's Ready Collection</a></li>
                                                        <li><a href="../gift-certificates/index.php">Shop Gift Card</a></li>
                                                        <li><a target="_blank" href="../premium-shirt-fabrics-collection/index.php">View Premium Catalog</a></li>
                                                    </ul>
                                                    <ul class="list-unstyled primary-links" style=" margin-top: 15px;">
                                                        <li><a href="../book-home-visit/index.php">Book a Home Visit</a></li>
                                                        <li><a href="../store-locations/index.php">Stores</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="col-lg-5 col-sm-12 col-xs-12 col-lg-3">
                                        <div class="footer-section footer-no-wysiwyg pull-left">
                                            <h4 class=""> <span>About</span> </h4>
                                            <div class="footer-general-body">
                                                <div class="wysiwyg-content">
                                                    <ul class="list-unstyled">
                                                        <li><a href="../about-us/index.php">About Us</a></li>
                                                        <li class="united-states-hidden"><a href="../invite-and-earn80cc.php?source=footer" style="color:#bb8b7d">Invite &amp; Earn</a></li>
                                                        <li class="united-states-hidden"><a href="../loyalty-program.php">Loyalty Program</a></li>
                                                        <li><a href="../press-in-the-news/index.php">Press</a></li>
                                                        <li><a href="../how-to-select-a-custom-shirt-fabric/index.php">Fabrics</a></li>
                                                        <li><a href="../parts-of-shirt-anatomy-of-shirt/index.php">Shirt Construction</a></li>
                                                        <li><a href="../how-to-measure-shirt-size/index.php">Sizing &amp; Fit</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="col-lg-5 col-sm-12 col-xs-12">
                                        <div class="footer-section pull-left">
                                            <h4 class=""> <span>Help</span> </h4>
                                            <div class="footer-general-body">
                                                <div class="wysiwyg-content">
                                                    <ul class="list-unstyled">
                                                        <li><a href="../help-and-support/index.php">Contact &amp; FAQ's</a></li>
                                                        <li><a href="../shirt-alterations/index.php">Alterations</a></li>
                                                    </ul>
                                                    <div style="margin-top: 70px;">
                                                        <h4 class=""> <span>Blog: The Noida Post</span> </h4>
                                                        <ul class="list-unstyled">
                                                            <li><a href="../blog/index.php">People Places &amp; Experiences that inspire us.</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </section>
                            <section class="col-lg-4 hidden-xs hidden-sm">
                                <div class="newslettter-and-social-media-block">
                                    <div class="wysiwyg-content">
                                        <form class="newsletter">
                                            <div class="input-group">
                                                <input type="email" name="EMAIL" class="form-control" placeholder="Email Address"> <span class="input-group-btn"> <button class="btn btn-default" type="submit">Subscribe</button> </span> </div>
                                        </form>
                                        <p>For news &amp; offers subscribe to our newsletter</p>
                                        <ul class="social-links list-unstyled">
                                            <li><a target="_blank" href="#"><i class="fa fa-youtube-play"></i></a></li>
                                            <li><a target="_blank" href="#" href="#"><i class="fa fa-instagram"></i></a></li>
                                            <li><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a target="_blank" href="#"><i class="fa fa-facebook"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col-lg-12 copyright-grid">
                                <div class="responsive-footer-block hidden-lg hidden-md">
                                    <div class="wysiwyg-content">
                                        <form class="newsletter">
                                            <div class="input-group">
                                                <input class="form-control" placeholder="Email Address" type="email"> <span class="input-group-btn"> <button class="btn btn-default" type="button">Subscribe</button> </span> </div>
                                        </form>
                                        <ul class="social-links list-unstyled">
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-instagram"></i></a>
                                                <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="copyright-and-terms-block">
                                    <div class="wysiwyg-content">
                                        <ul class="clearfix copyright">
                                            <li class="pull-right hidden-sm hidden-xs">Developed by <a class="_blank" href="#">Rirev</a> </li>
                                            <li class="pull-right hidden-lg hidden-md">
                                                Developed by <a class="_blank" href="#">Rirev</a> </li>
                                            <li>&copy; 2012 - 2018 Noida Shirt Company / <a href="../terms-and-conditions.php">Terms</a> </li>
                                        </ul>
                                        <div class="hidden" id="trial-shirt">
                                            <div class="trial-shirt"> <i class="bsc-shirt"></i>
                                                <h2>Worried about your fit?<br />We'll send a trial shirt!</h2>
                                                <hr>
                                                <p>If you are a first time customer and placing an order for 3 more shirts, we will send you a Trial shirt for your confirmation.</p>
                                                <p>Once you confirm, we’ll proceed with the remainder of your order.</p>
                                                <p><em>* Not applicable on International orders or orders with Cash on Delivery payment.</em></p>
                                            </div>
                                        </div>
                                        <div class="hidden" id="loyalty-points">
                                            <div class="loyalty"> <i class="bsc-loyalty"></i>
                                                <h2>Loyalty Points</h2>
                                                <hr>
                                                <p>Each time you buy a shirt, you will receive loyalty points based on the value of the shirt. Each point is worth One Rupee. Use these points on subsequent orders or save up enough points to earn free shirts.</p>
                                                <p>Points earned through shopping Online, through our Stores or Travelling Stylist, are <strong>all credited to your online Profile</strong>. These points may be redeemed through any of the above channels in the future.</p>
                                                <p>Loyalty points are credited after your shirts have been shipped to you.</p>
                                                <p>To start earning loyalty points or to check your existing balance <a href="../login/index.php">Sign In</a> / <a href="../profiles-add/index.php">Register</a> your online profile.</p>
                                            </div>
                                        </div>
                                        <div class="hidden" id="size-chart-desktop"><img alt="Size Chart Desktop" class="lazyload" data-src="../design/themes/bsc/media/images/homepage/size-chart-desktop.jpg"></div>
                                        <div class="hidden" id="size-chart-mobile"><img alt="Size Chart Mobile" class="lazyload" data-src="../design/themes/bsc/media/images/homepage/size-chart-mobile.jpg"></div>
                                        <div class="hidden" id="shirt-body"><img alt="Shirt and Body" class="lazyload" data-src="../design/themes/bsc/media/images/homepage/shirt-and-body.jpg"></div>
                                        <div class="hidden" id="women-collection"><img alt="Women's Collection Popup" class="lazyload" data-src="design/themes/bsc/media/images/homepage/bombayshirts--shop-women--coming-soon.jpg" style="z-index:2000;"></div>
                                        <div class="hidden" id="bsc-wash-care"><img alt="BSC Wash Care" class="lazyload" data-src="design/themes/bsc/media/images/homepage/wash-care.png" style="z-index:2000;"></div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </footer>
            </main>
            <a id="shop-loyalty-points-modal-trigger" data-toggle="modal" data-target="#shop-loyalty-points-modal" data-backdrop="true"></a>
            <div class="modal fade" id="shop-loyalty-points-modal">
                <div class="modal-dialog">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><i class="bsc-cross-none"></i></button>
                    </div>
                    <div class="modal-content">
                        <a class="" href="../profiles-addf525.php?utm_source=signup"> <span class="vertical-align"> <span class="mod-content"> <h1>
GET ₹250 OFF
</h1> <p>
Sign Up and Get 250 Points to use towards your first order.
</p> <span class="btn btn-border">Sign Me Up</span> </span>
                        </a>
                    </div>
                </div>
            </div>
            <a id="shop-loyalty-points-modal-international-trigger" data-toggle="modal" data-target="#shop-loyalty-points-international-modal" data-backdrop="true"></a>
            <div class="modal fade" id="shop-loyalty-points-international-modal">
                <div class="modal-dialog">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><i class="bsc-cross-none"></i></button>
                    </div>
                    <div class="modal-content">
                        <a class="" href="../profiles-addf525.php?utm_source=signup"> <span class="vertical-align"> <span class="mod-content"> <h1>
GET 250 Points
</h1> <p>
Sign Up and Get 250 Points to use towards your first order.
</p> <span class="btn btn-border">Sign Me Up</span> </span>
                        </a>
                    </div>
                </div>
            </div>
            <div id="tax-implication" class="hidden">
                <div class="tax-implication" style="padding:50px 40px;text-align:center;">
                    We believe in transparency of information and display taxes as a component, separate to the price.
                    <span class="international-hidden"> <br><br>The Government of India levies a 12% GST tax on apparel, applicable during checkout.
</span> </div>
            </div>
        </div>
        <script src="../cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js" integrity="sha384-aBL3Lzi6c9LNDGvpHkZrrm3ZVsIwohDD7CDozL0pk8FwCrfmV7H9w8j3L7ikEv6h" crossorigin="anonymous"></script>
        <script src="../ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js" data-no-defer></script>
        <script src="../code.jquery.com/color/jquery.color-2.1.2.min.js"></script>
        <script src="../cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
        <script src="../cdnjs.cloudflare.com/ajax/libs/iScroll/5.1.3/iscroll.min.js" data-no-defer></script>
        <script src="../cdnjs.cloudflare.com/ajax/libs/lazysizes/2.0.0/lazysizes.min.js"></script>
        <script src="../cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>
        <script src="../cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
        <script src="../cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/js/jquery.tooltipster.min.js"></script>
        <script src="../cdnjs.cloudflare.com/ajax/libs/intro.js/2.3.0/intro.min.js"></script>
        <script src="../cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.min.js"></script>
        <script src="../cdnjs.cloudflare.com/ajax/libs/jquery.alphanum/1.0.24/jquery.alphanum.min.js"></script>
        <script src="../cdn.ravenjs.com/3.16.0/raven.min.js" crossorigin="anonymous"></script>
        <script data-no-defer>
            if (!window.jQuery) {
                document.write('<script type="text/javascript" src="../js/lib/jquery/jquery.min0015.js?ver=4.3.6" ><\/script>');
            }
        </script>
        <script data-no-defer>
            if (!window.jQuery.ui) {
                document.write('<script type="text/javascript" src="../js/lib/jqueryui/jquery-ui.custom.min0015.js?ver=4.3.6" ><\/script>');
            }
        </script>
        <script src="../cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.10/js/min/perfect-scrollbar.jquery.min.js" data-no-defer></script>
        <script src="../cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
        <script type="text/javascript" src="../var/cache/misc/assets/js/tygh/scripts-9f97437cfcee50b768df0148138f863e1515504583.js"></script>
        <script type="text/javascript" src="../js/tygh/exceptions0015.js?ver=4.3.6"></script>
        <script type="text/javascript" src="../js/addons/image_zoom/cloudzoom0015.js?ver=4.3.6"></script>
        <script type="text/javascript">
            if (!($(window).width() <= 992)) {
                $(document).ajaxStop(function() {
                    if ($('.custom_shirt_template').exists()) {
                        var mobileWidth = 767;

                        $('#fabric-filterer-wrapper .option').hover(function() {
                            var elm = $(this).find('img');
                            var gridHover = $(elm).parent().find('.grid-hover');
                            if (gridHover.data('cloudzoom') == undefined) {
                                gridHover.attr('data-cloudzoom', 'zoomImage: "' + elm.data('src') + '"').attr('src', elm.data('src'))
                                    .CloudZoom({
                                        tintColor: '#ffffff',
                                        tintOpacity: 0.6,
                                        animationTime: 200,
                                        easeTime: 200,
                                        zoomFlyOut: true,
                                        zoomSizeMode: 'zoom',
                                        captionPosition: 'bottom',
                                        zoomPosition: '5',
                                        autoInside: mobileWidth,
                                        disableOnScreenWidth: mobileWidth,
                                        zoomWidth: 220,
                                        zoomHeight: 279
                                    });
                            }
                        });
                    }
                });
            }
        </script>
        <script src="../use.typekit.net/isf1dby.js"></script>
		<script>
			function increaseValue() {
			  var value = parseInt(document.getElementById('number').value, 10);
			  value = isNaN(value) ? 0 : value;
			  value++;
			  document.getElementById('number').value = value;
			}

			function decreaseValue() {
			  var value = parseInt(document.getElementById('number').value, 10);
			  value = isNaN(value) ? 0 : value;
			  value < 2 ? value = 2 : '';
			  value--;
			  document.getElementById('number').value = value;
			}
		</script>
    </main>
</body>

</html>
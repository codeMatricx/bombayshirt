﻿<?php /* Template Name: gift-certificates */ ?>
<?php get_header(); ?>

<!-- <style>
.loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #000;
    z-index: 999999;
    opacity:0.5;
}
.loader img {
    position: absolute;
    top: 48%;
    left: 48%;
}

</style> -->

<div id = "loaderId" style = "display: none;" class="loader">
     <img  src="<?php echo get_stylesheet_directory_uri().'/loading.gif'; ?>">
   </div>
                <section class="dispatch-gift_certificates-add content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12">
                                <div class="gift-certificate-header-block">
                                    <div class="wysiwyg-content">
                                        <div>
                                            <div class="row">
                                                    <?php
                                                    if (have_posts()):
                                                    while (have_posts()) : the_post();
                                                    the_content();
                                                    endwhile;
                                                    else:
                                                    echo '<p>Sorry, no posts matched your criteria.</p>';
                                                    endif;
                                                    ?>
                                                <!-- <div class="col-md-12 border">
                                                    <h1 class="heading">Gift Voucher</h1>
                                                    <hr class="heading-separator">
                                                    <h2 class="sub-heading">Gift for a special someone?<br /> Or even your boss?<br />
													A SSC gift card is the answer - we'll create an account for the recipient and credit the amount as Loyalty Points.</h2> </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col-lg-6 main-content-grid">
                                <div class="gift-photo-block pull-right">
                                    <div class="wysiwyg-content"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/gift-voucher/voucher.jpg'); ?>" /></div>
                                </div>
                            </section>
                            
                            <section class="col-lg-6 main-content-grid">
                                <div class="main-content-block" >
                                    <div class="ty-gift-certificate">
                                        <form class="cm-ajax cm-ajax-full-render" action="" method="post" target="_self" name="gift_certificates_form" style="border-style: solid;border-color: lightgray;padding: 21px;">
                                            <!-- <input type="hidden" name="redirect_url" value="index.html?dispatch=gift_certificates.add" /> -->
                                            <div class="ty-control-group">
                                                <label for="gift_cert_sender" class="ty-control-group__title cm-required">Your Name</label>
                                                <input type="text" id="senderName" name="senderName" class="ty-input-text-full" required /> </div>
                                            <div class="ty-control-group">
                                                <label for="gift_cert_recipient" class="ty-control-group__title cm-required">Receiver's Name</label>
                                                <input type="text" id="recieverName" name="recieverName" class="ty-input-text-full cm-focus" required /> </div>
                                            <div id="gc_switcher">
                                                <div class="ty-gift-certificate__block " id="email_block">
                                                    <div class="ty-control-group">
                                                        <label for="gift_cert_email" class="cm-required cm-email ty-control-group__title">Receiver's Email</label>
                                                        <input type="text" id="recieverEmailId" name="recieverEmail" class="ty-input-text-full" required /> </div>
                                                </div>
                                            </div>
                                            <div class="ty-control-group">
                                                <label for="gift_cert_message" class="ty-control-group__title">Message</label>
                                                <textarea id="message" name="message" cols="49" rows="4" class="ty-input-text-full" required></textarea>
                                            </div>
                                             <div class="ty-control-group ty-gift-certificate__amount">
                                                <label for="gift_cert_amount" class="ty-control-group__title cm-required cm-gc-validate-amount">amount</label> <span class="ty-gift-certificate__currency">₹</span>
                                                <input type="text" id="amount" name="amount" class="ty-gift-certificate__amount-input cm-numeric" data-p-sign="s" data-a-sep="" data-a-dec="." size="5" required /> </div>
                                            <div class="ty-gift-certificate__buttons buttons-container">
                                                <button id="addCart" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="button" name="submit" onclick="giftCertificate()">Add to cart</button>
                                                
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
        
<?php get_footer(); ?>     

<script>
function giftCertificate() 
{
     $("#loaderId").show();           
     var sender = $("#senderName").val();
     var reciever = $("#recieverName").val();
     var recieverMail = $("#recieverEmailId").val();
     var message = $("#message").val();
     var amount = $("#amount").val();
        $.ajax(
        {
          type:'POST',
          datatype:'json',
          url : 'http://staging.isiwal.com/SunnysBespokeDev/wp-content/themes/noidashirt/gift_certificate_status.php',
          data : {senderName:sender,recieverName:reciever,recieverEmail:recieverMail,message:message,amount:amount},
            success : function($data)
            {
                $("#loaderId").hide();           
                var obj = $.parseJSON(data);
                alert("Gift sent successfull");
            }
        });
         //alert("Gift sent successfull");
             
}
      </script>          
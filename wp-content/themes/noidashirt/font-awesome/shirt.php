﻿<?php /* Template Name: shirt */ ?>

<?php get_header(); ?>

<body class="dispatch-products-view drawer drawer--left default">
    <main role="main">
        <input class="local-coefficient" type="hidden" value="1.00000" />
        <input class="local-symbol" type="hidden" value="₹" />
        <div class="page" id="tygh_container">
            <div id="ajax_overlay" class="ajax-overlay"></div>
            <div id="ajax_loading_box" class="ajax-loading-box"></div>
            <div class="cm-notification-container notification-container"> </div>
            <main class="page-container" id="tygh_main_container" role="main">
               
                <section class="dispatch-products-view content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 breadcrumbs-grid">
                                <div class="breadcrumbs-block">
                                    <div id="breadcrumbs_10">
                                        <ol class="breadcrumb">
                                            <li><a href="../../index.php" class="">Home</a></li>
                                            <li><a href="../index.php" class="">Men&#039;s Readymade</a></li>
                                            <li class="active">Green White Stripes</li>
                                        </ol>
                                        <div class="product-switcher"> <a class="switcher-icon left " href="../green-checks-en-5/index.php" title="Prev"><i class="icon-left-circle"></i></a> <span class="switcher-selected-product">30</span> <span>of</span> <span class="switcher-total">88</span> <a class="switcher-icon right " href="../grey-stripes-with-red-shirt-en-4/index.php" title="Next"><i class="icon-right-circle"></i></a> </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="main-content-block">
                                    <div class="row product-block collections-product">
                                        <div itemscope itemtype="http://schema.org/Product">
                                            <meta itemprop="sku" content="RM-PT060" />
                                            <meta itemprop="name" content="Green White Stripes" />
                                            <meta itemprop="description" content="A 100% Cotton fabric in poplin weave.  This green and white stripes is a trendy casual style designed in a full sleeve, with spread collar, french placket and a classic single convertible cuff. A simple white inner cuff contrast and a similar collar piping makes this style casual yet sophisticated for a brunch or social gathering. " />
                                            <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                                                <link itemprop="availability" href="https://schema.org/InStock" />
                                                <meta itemprop="priceCurrency" content="INR" />
                                                <meta itemprop="price" content="2490" /> </div>
                                        </div>
                                        <div class="product-block-img col-lg-7 col-sm-7">
                                            <div class="cm-reload-7856" id="product_images_7856_update">
                                                <div class="thumbnail product-img cm-preview-wrapper">
                                                    <!-- <a id="det_img_link_78565a575afdce0f1_38849" data-ca-image-id="preview[product_images_78565a575afdce0f1]" class="cm-image-previewer cm-previewer previewer" data-ca-image-width="" data-ca-image-height="" href="../../images/detailed/38/H0zK5rO.png" title="" style=""> --><!-- <img class="   " id="det_img_78565a575afdce0f1_38849" src="../../images/thumbnails/525/656/detailed/38/H0zK5rO.png"  alt="" title="" /><span class="previewer-icon hidden-phone"></span> -->
                                                        <section id="example">
   
       
        <!-- ZoomIt Example -->
        <div class="zoomit-wrapper">
            <a id="det_img_link_78565a575afdce0f1_38849" data-ca-image-id="preview[product_images_78565a575afdce0f1]" class="cm-image-previewer cm-previewer previewer" data-ca-image-width="" data-ca-image-height="" href="<?php echo get_theme_file_uri('/images/detailed/38/H0zK5rO.png'); ?>" title="" style="">
            <img id="zoomit-target" src="<?php echo get_theme_file_uri('/images/thumbnails/525/656/detailed/38/H0zK5rO.png'); ?>" data-zoomed="<?php echo get_theme_file_uri('/images/thumbnails/525/656/detailed/38/H0zK5rOl.png'); ?>" alt="Fluffy cute Koala">
            </a>
        </div>
    
</section>

                                                        
                                                    <!-- </a> -->
                                                </div>
                                            </div>
                                           
   
                                        </div>
                                        <div class="product-block-right col-lg-5 col-sm-5 col-xs-12">
                                            <form method="post"  enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle ">
                                                <input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*" />
                                                <input type="hidden" name="redirect_url" value="index.php?dispatch=products.view&amp;product_id=7856" />
                                                <input type="hidden" name="product_data[7856][product_id]" value="7856" />
                                                <input type="hidden" class="product-sku-code" value="RM-PT060" />
                                                <div class="product-block-button">
                                                    <div class="add-to-cart ty-btn__add-to-cart cm-form-dialog-closer btn btn-success btn-lg" onclick= "div_show()" style="width: 100%; min-width: 200px; font-size: 17px; line-height: 60px; background-color: #336799;" > <i class="glyphicon glyphicon-shopping-cart"></i> Add to Cart
                                                   </div>
                                                    
                                                
                                                </div> 
                                                <a class="btn-checkout-help btn btn-white">Enter your size at checkout (?)
												<p class="details" style="display:none;">
												Our shirts are custom made. You may select from a variety of sizing options during the checkout process.
												</p> </a>
                                                <h1 class="product-title">Green White Stripes</h1>
                                                <div class="brands"> </div>
                                                <div class="prices-container price-wrap">
                                                    <div class="product-prices"> <span class="cm-reload-7856" id="old_price_update_7856"> </span>
                                                        <div class="product-block-price-actual"> <span class="cm-reload-7856 price-update" id="price_update_7856"> <input type="hidden" name="appearance[show_price_values]" value="1" /> <input type="hidden" name="appearance[show_price]" value="1" /> <span class="price" id="line_discounted_price_7856"><span class="price-num">₹</span><span id="sec_discounted_price_7856" class="price-num">2,490</span></span>
                                                            </span> <span class="tax-wrapper"> + TAX <a rel="shadowbox;height=225;width=380;player=inline" href="#tax-implication">(?)</a></span> </div> <span class="cm-reload-7856" id="line_discount_update_7856"> <input type="hidden" name="appearance[show_price_values]" value="1" /> <input type="hidden" name="appearance[show_list_discount]" value="1" /> </span> </div>
                                                </div>
                                                <div class="loyalty-points united-states-hidden">
                                                    <label>Points <a rel="shadowbox;height=590;width=650;player=inline" href="#loyalty-points" class="help">(?)</a></label>
                                                    You'll get <span>₹</span><span class="value">150.00</span><span class="rupee-term"> as cash back</span><span class="international-term"> in points</span>.
                                                </div>
                                                <div class="product-block-sku">
                                                    <div class="control-group product-list-field cm-reload-7856" id="sku_update_7856">
                                                        <input type="hidden" name="appearance[show_sku]" value="1" />
                                                        <label id="sku_7856">CODE:</label> <span id="product_code_7856">RM-PT060</span> </div>
                                                </div>
                                                <div class="details">
                                                    <p>A 100% Cotton fabric in poplin weave. This green and white stripes is a trendy casual style designed in a full sleeve, with spread collar, french placket and a classic single convertible cuff. A simple white inner cuff contrast and a similar collar piping makes this style casual yet sophisticated for a brunch or social gathering. </p>
                                                </div>
                                                <div class="fabric">
                                                    <p>100% Cotton</p>
                                                </div>
                                                <div class="wash-care">
                                                    <p>MACHINE OR HAND WASH IN COLD WATER / NO DRY CLEAN / NO TUMBLE DRY / NO IRON ON LEATHER</p>
                                                </div>
                                                <div class="product-block-option">
                                                    <div class="cm-reload-7856" id="product_options_update_7856">
                                                        <input type="hidden" name="appearance[show_product_options]" value="1" />
                                                        <input type="hidden" name="appearance[details_page]" value="1" />
                                                        <input type="hidden" name="additional_info[info_type]" value="D" />
                                                        <input type="hidden" name="additional_info[get_icon]" value="1" />
                                                        <input type="hidden" name="additional_info[get_detailed]" value="1" />
                                                        <input type="hidden" name="additional_info[get_additional]" value="" />
                                                        <input type="hidden" name="additional_info[get_options]" value="1" />
                                                        <input type="hidden" name="additional_info[get_discounts]" value="1" />
                                                        <input type="hidden" name="additional_info[get_features]" value="" />
                                                        <input type="hidden" name="additional_info[get_extra]" value="" />
                                                        <input type="hidden" name="additional_info[get_taxed_prices]" value="1" />
                                                        <input type="hidden" name="additional_info[get_for_one_product]" value="1" />
                                                        <input type="hidden" name="additional_info[detailed_params]" value="1" />
                                                        <input type="hidden" name="additional_info[features_display_on]" value="C" /> </div>
                                                </div>
                                                <div class="product-block-advanced-option">
                                                    <div class="cm-reload-7856" id="advanced_options_update_7856">
                                                        <input type="hidden" name="appearance[dont_show_points]" value="" />
                                                        <div class="ty-reward-group"> <span class="ty-control-group__label product-list-field">Price in points:</span> <span class="ty-control-group__item" id="price_in_points_7856">2490 points</span> </div>
                                                        <div class="ty-reward-group product-list-field hidden"> <span class="ty-control-group__label">Loyalty points:</span> <span class="ty-control-group__item" id="reward_points_7856"> points</span> </div>
                                                    </div>
                                                </div>
                                                <div class="product-block-field-group product-options">
                                                    <div class="cm-reload-7856 stock-wrap" id="product_amount_update_7856">
                                                        <input type="hidden" name="appearance[show_product_amount]" value="1" />
                                                        <div class="control-group product-list-field">
                                                            <label>Availability:</label> <span class="qty-in-stock" id="in_stock_info_7856">In stock</span> </div>
                                                    </div>
                                                    <div class="cm-reload-7856" id="qty_update_7856">
                                                        <input type="hidden" name="appearance[show_qty]" value="1" />
                                                        <input type="hidden" name="appearance[capture_options_vs_qty]" value="" />
                                                        <div class="qty clearfix changer" id="qty_7856">
                                                            <label for="qty_count_7856">Quantity:</label>
                                                            <div class="center valign value-changer cm-value-changer"> <a class="cm-decrease decrease">&minus;</a>
                                                                <input type="text" size="5" class="input-text-short cm-amount" id="qty_count_7856" name="product_data[7856][amount]" value="1" data-ca-min-qty="1" /> <a class="cm-increase increase">&#43;</a> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
            </body>

                <?php get_footer(); ?> 
               





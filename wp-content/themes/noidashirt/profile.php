<?php /* Template Name: Profile */ ?>
<?php get_header(); ?>
<?php 
global $wpdb;
$uId = $_GET['userId'];
?>
<?php
if(isset($_POST['update']))
{  

//Update useraccount start here
   $upEmail = $_POST['userEmail']; 
   $upPass = $_POST['userPassword'];
   $upConPass = $_POST['conUserPassword'];
   if(($upConPass == $_POST['userPassword']))
   {
        $pass= wp_hash_password($upPass);
        // $proUpdate = $wpdb->query($wpdb->prepare("UPDATE wp_users SET user_email='$upEmail',user_pass='$pass' WHERE ID='$uId'"));
        // echo "<script type='text/javascript'>window.location='http://staging.isiwal.com/SunnysBespokeDev/profile'</script>";

        $proUpdate=$wpdb->update( 'wp_users', array( 'user_email' => $upEmail, 'user_pass' => $pass ),  array( 'ID' => $uId ),  array( '%s','%s' ), array( '%d' ));

// Update the post into the database
  wp_update_post( $my_post );
   }
   else
   {
    echo "<script> alert('Password does not match') </script>";
   }
}
   //End here
if(isset($_POST['billingForm']))
{

  //Billing address start here
   $fName=$_POST['firstName'];
   $lName=$_POST['lastName'];
   $Phone=$_POST['phone'];
   $address1=$_POST['address1'];
   $address2=$_POST['address2'];
   $city=$_POST['city'];
   $country=$_POST['country'];
   $state=$_POST['state'];
   $zipCode=$_POST['zip'];
   $email=$_POST['email'];

   $billingProfile = $wpdb->get_row("SELECT user_id FROM wp_user_billing_address WHERE user_id ='$uId'");
   $userId = $billingProfile->user_id;
   if(!empty($billingProfile->user_id))
   {

     $proUpdate=$wpdb->update( 'wp_user_billing_address', array( 'first_name' => $fName, 'last_name' => $lName, 'phone' => $Phone, 'address' => $address1, 'address_line_2' => $address2, 'city' => $city,'country' => $country, 'state' => $state, 'zip_code' => $zipCode, 'email' => $email ),  array( 'user_id' => $uId ),  array( '%s','%s','%d','%s','%s','%s','%s','%s','%d','%s' ), array( '%d' ));
   }
   else
   {
    $wpdb->insert( 'wp_user_billing_address', array( 'user_id' => $uId, 'first_name' => $fName, 'last_name' => $lName, 'phone' => $Phone, 'address' => $address1, 'address_line_2' => $address2, 'city' => $city,'country' => $country, 'state' => $state, 'zip_code' => $zipCode, 'email' => $email ), array( '%d','%s','%s','%d','%s','%s','%s','%s','%s','%d','%s' ));
    echo "<script> alert('Details submited successfully') </script>";
   
   }
    
   //End here
}
   
 ?>

                <section class="dispatch-profiles-update content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-3 main-content-grid">
                                <div class="sub-menu-block">
                                    <div class="wysiwyg-content">
                                        <div class="profile-sidebar">
                                            <ul>
                                                <li><a href="" class="active">Profile Details</a></li>
                                                <li><a href="http://staging.isiwal.com/SunnysBespokeDev/tracking" >Order Tracking</a></li>
                                                <li><a href="http://staging.isiwal.com/SunnysBespokeDev/orders">Order History</a></li>
                                                <li><a href="#">Loyalty Points History</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="col-lg-9 profile-information-grid">
                                <div class="mainbox-container clearfix">
                                    <div class="page-header">
                                        <h1>
                                            Profile details
                                        </h1>
                                    </div>
                                    <div class="mainbox-body">
                                        <div class="account" id="content_general">
                                            <form name="profile_form" action="#" class="form-horizontal" method="post">
                                            <form  action="#"  method="post"> 
                                                <input id="selected_section" type="hidden" value="general" name="selected_section" /> <input id="default_card_id" type="hidden" value="" name="default_cc" /> <input type="hidden" name="profile_id" value="119861"
                                                />
                                                <h1 class="profile-tab-header profile-tab-header1">User Information</h1>
                                                <?PHP 
                                                    $userProfile = $wpdb->get_row("SELECT user_email,user_pass FROM wp_users WHERE ID='$uId'");
                                                    $email = $userProfile->user_email;
                                                    $password = $userProfile->user_pass;
                                                 ?>
                                                <div class="profile-tab profile-tab1 no-labels" style="display: none;">
                                                    <div class="form-group"> <label for="email" class="cm-required cm-email cm-trim control-label">Email</label> <input type="email" id="upEmail" name="userEmail" size="32" maxlength="128" value="<?php echo $email; ?>" class="form-control cm-focus"
                                                            placeholder="Email Address" /> </div>
                                                    <div class="form-group"> <label for="password1" class="cm-required cm-password control-label">Password</label> <input type="password" id="Password1" name="userPassword" size="32" maxlength="32" value="<?php echo $password;?>" class="form-control cm-autocomplete-off"
                                                            placeholder="Password" /> </div>
                                                    <div class="form-group"> <label for="password2" class="cm-required cm-password control-label">Confirm password</label> <input type="password" id="password2" name="conUserPassword" size="32" maxlength="32" value="<?php echo $password;?>"
                                                            class="form-control cm-autocomplete-off" placeholder="Repeat Password" /> </div>
                                                            <button id="save_profile_but" class="btn btn-primary" type="submit" name="update">Save</button> 
                                                </div>

                                                <h1 class="profile-tab-header  active profile-tab-header2">Measurements</h1>
                                                <div class="profile-tab profile-tab2  measurement-tab" style="display: block;">
                                                    <div class="measurements-menu">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="profile-sub-tab-header profile-sub-tab-header1" data-link=".smart-sizing" > <i class="radio"><i></i></i> <span class="heading"> <strong>Find my size - use <span class="highlight">FitSmart</span></strong><br /> Your custom size in 6 easy steps.
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="profile-sub-tab-header profile-sub-tab-header2" data-link=".standard-sizing"> <i class="radio"><i></i></i> <span class="heading"> <strong>Select a Standard Size</strong><br />
                                                                Choose from size XS - XXXL
                                                                </span> </div>
                                                            </div>
                                                            <div class="col-md-4 united-states-hidden ">
                                                                <div class="profile-sub-tab-header profile-sub-tab-header3" data-link=".send-a-shirt"> <i class="radio"><i></i></i> <span class="heading"> <strong>Send us a shirt</strong><br />
                                                                Send us your best fitting shirt and we'll copy the measurements.
                                                                </span> </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="profile-sub-tab smart-sizing" style="display:none;">
                                                        <div class="tab-content">
                                                            <div class="tab-pane block" id="intro">
                                                                <p> <span class="hidden-xs hidden-sm">
                                                                    We've collected years of data to calculate the perfect fit.<br />
                                                                    FitSmart lets you find your Custom Size in
                                                                    <strong>6 easy steps</strong> </span> <span class="hidden-md hidden-lg">
                                                                     We've collected years of data to calculate the perfect fit. FitSmart lets you find your Custom Size in
                                                                    <strong>6 easy steps</strong> </span> <a href="#gender" class="btn btn-bordered">Get Started</a> 
                                                                </p>
                                                            </div>
                                                            <div class="tab-pane" id="gender" style="display:none;"> <span class="step">Step 1 of 6</span>
                                                                <h1>Gender</h1>
                                                                <div class="clearfix"> <label class="btn btn-bordered male radio"> <span>Male
                                                                <input type="radio" name="gender" value="male" checked /> </span> </label> <label class="btn btn-bordered female radio"> <span>Female
                                                                <input type="radio" name="gender" value="female" /> </span> </label> </div> <a href="#smart-wizard-male" class="btn btn-bordered btn-primary">Next</a> </div>
                                                            <div class="tab-pane active" id="finish" style="display:none;"> <span class="step hidden"></span>
                                                                <h1>Your measurements are now calculated!</h1>
                                                                <div class="row expanded">
                                                                    <div class="col-md-6"> <a class="btn btn-bordered btn-primary review" href="">Review My Measurements</a> </div>
                                                                    <div class="col-md-6"> <a class="btn btn-primary continue" href="">Continue to Billing</a> </div>
                                                                </div>
                                                                <div class="row succinct" style="display:none;">
                                                                    <div class="col-md-12"> <a class="btn btn-bordered btn-primary review" href="">Review My Measurements</a> </div>
                                                                </div>
                                                                <div class="fit-promise text-center">
                                                                    <h2>FIT PROMISE</h2>
                                                                    <p class="hidden-sm hidden-xs">If you're not completely satisfied with the</br>fit on your first order, we will alter your shirt or completely remake it free of cost.</p>
                                                                    <p class="hidden-md hidden-lg">If you're not completely satisfied with the fit on your first order, we will alter your shirt or completely remake it free of cost.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="smart-wizard-male" class="smart-wizard">
                                                            <div class="navbar">
                                                                <div class="navbar-inner">
                                                                    <div class="container">
                                                                        <ul>
                                                                            <li> <a data-toggle="tab" href="#male-size">Size</a> </li>
                                                                            <li> <a data-toggle="tab" href="#male-shoulders">Sloping Shoulders</a> </li>
                                                                            <li> <a data-toggle="tab" href="#male-height">Height</a> </li>
                                                                            <li> <a data-toggle="tab" href="#male-shape">Shape</a> </li>
                                                                            <li> <a data-toggle="tab" href="#male-fit">Fit</a> </li>
                                                                            <li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-content">
                                                                <div class="tab-pane" id="male-size"> <span class="step">Step 2 of 6</span>
                                                                    <h1>What is your preferred size?</h1> <select data-type="select" data-live-search="false" class="male-size" name="male-size"> <option value="">--</option> <option value="xs">XS (36)</option> <option value="s">S (38)</option> <option value="m">M (40)</option> <option value="l">L (42)</option> <option value="xl">XL (44)</option> <option value="xxl">XXL (46)</option> <option value="xxxl">XXXL (48)</option> </select>                                                                    <a rel="shadowbox;width=1366;height=669;player=inline" href="#size-chart-desktop" class="size-chart-link hidden-xs hidden-sm">Size Chart</a> <a rel="shadowbox;width=436;height=400;player=inline"
                                                                        href="#size-chart-mobile" class="size-chart-link hidden-md hidden-lg">Size Chart</a> </div>
                                                                <div class="tab-pane" id="male-shoulders"> <span class="step">Step 3 of 6</span>
                                                                    <h1>Are your shoulders sloping?</h1>
                                                                    <div class="clearfix"> <label class="btn btn-bordered sloping radio"> <span>Yes
                                                                    <input type="radio" name="male-shoulder" value="Yes" /> </span> </label> <label class="btn btn-bordered not-sloping radio"> <span>No
                                                                    <input type="radio" name="male-shoulder" value="No" /> </span> </label> <label class="btn btn-bordered sloping-dont-know radio"> <span>I don't know
                                                                    <input type="radio" name="male-shoulder" value="dont-know" checked="checked" /> </span> </label> </div>
                                                                </div>
                                                                <div class="tab-pane" id="male-height"> <span class="step">Step 4 of 6</span>
                                                                    <h1>What is your height?</h1>
                                                                    <div class="slider-wrapper">
                                                                        <div class="slider height" name="male-height"></div> <span class="slider-background"></span> </div>
                                                                </div>
                                                                <div class="tab-pane" id="male-shape"> <span class="step">Step 5 of 6</span>
                                                                    <h1>What is your body shape?</h1>
                                                                    <div class="clearfix"> <label class="btn btn-bordered skinny radio"> <span>Very Skinny
                                                                    <input type="radio" name="male-shape" value="skinny" /> <i>You have thin build with narrow shoulders, torso and biceps.</i> </span> </label> <label class="btn btn-bordered athletic radio"> <span>Fit
                                                                    <input type="radio" name="male-shape" value="athletic" /> <i>You have a broader upper body and a thin waist.</i> </span> </label> <label class="btn btn-bordered average radio"> <span>Average
                                                                    <input type="radio" name="male-shape" value="average" /> <i>You are an average build with a slight stomach. </i> </span> </label> <label class="btn btn-bordered healthy radio"> <span>Healthy
                                                                    <input type="radio" name="male-shape" value="healthy" /> <i>Your fittest days are behind you. You have a protruding belly. </i> </span> </label> </div>
                                                                </div>
                                                                <div class="tab-pane" id="male-fit"> <span class="step">Step 6 of 6</span>
                                                                    <h1>What fit do you prefer?</h1>
                                                                    <div class="clearfix"> <label class="btn btn-bordered body radio"> <span>Super Slim
                                                                    <input type="radio" name="male-fit" value="body" /> <i>Extremely fitted around the chest, waist and biceps.</i> </span> </label> <label class="btn btn-bordered structured radio"> <span>Structured
                                                                    <input type="radio" name="male-fit" value="structured" /> <i>Stylishly fitted, but leaving room for comfort.</i> </span> </label> <label class="btn btn-bordered comfort radio"> <span>Baggy
                                                                    <input type="radio" name="male-fit" value="comfort" /> <i>A more boxy and relaxed fit, especially around the waist.</i> </span> </label> </div>
                                                                </div>
                                                                <ul class="pager wizard">
                                                                    <li class="previous"><a href="javascript:;">Previous</a></li>
                                                                    <li class="next"><a href="javascript:;">Next</a></li>
                                                                    <li class="finish"><a href="javascript:;">Finish</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div id="smart-wizard-female" class="smart-wizard">
                                                            <div class="navbar">
                                                                <div class="navbar-inner">
                                                                    <div class="container">
                                                                        <ul>
                                                                            <li> <a data-toggle="tab" href="#female-size">Size</a> </li>
                                                                            <li> <a data-toggle="tab" href="#female-height">Height</a> </li>
                                                                            <li> <a data-toggle="tab" href="#female-fit">Fit</a> </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-content">
                                                                <div class="tab-pane" id="female-size"> <span class="step">Step 2 of 4</span>
                                                                    <h1>What is your preferred size?</h1> <select data-type="select" data-live-search="false" class="female-size" name="female-size"> <option value="">--</option> <option value="0">0 (XXS)</option> <option value="2">2 (XS)</option> <option value="4">4 (S)</option> <option value="6">6 (M)</option> <option value="8">8 (L)</option> <option value="10">10 (XL)</option> </select>                                                                    <a rel="shadowbox;width=1366;height=669;player=inline" href="#size-chart-desktop" class="size-chart-link hidden-xs hidden-sm">Size Chart</a> <a rel="shadowbox;width=436;height=400;player=inline"
                                                                        href="#size-chart-mobile" class="size-chart-link hidden-md hidden-lg">Size Chart</a> </div>
                                                                <div class="tab-pane" id="female-height"> <span class="step">Step 3 of 4</span>
                                                                    <h1>What is your height?</h1>
                                                                    <div class="slider-wrapper">
                                                                        <div class="slider height" name="female-height"></div> <span class="slider-background"></span> </div>
                                                                </div>
                                                                <div class="tab-pane" id="female-fit"> <span class="step">Step 4 of 4</span>
                                                                    <h1>What fit do you prefer?</h1>
                                                                    <div class="clearfix"> <label class="btn btn-bordered fitted radio"> <span>Fitted
                                                                }
                                                                <input type="radio" name="female-fit" value="fitted" /> <i>A formal, well-fitted shirt.</i> </span> </label> <label class="btn btn-bordered loose radio"> <span>Loose
                                                                <input type="radio" name="female-fit" value="loose" /> <i>Comfortable, boyfriend style shirt.</i> </span> </label> </div>
                                                                </div>
                                                                <ul class="pager wizard">
                                                                    <li class="previous"><a href="javascript:;">Previous</a></li>
                                                                    <li class="next"><a href="javascript:;">Next</a></li>
                                                                    <li class="finish"><a href="javascript:;">Finish</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="profile-sub-tab standard-sizing" style="display:none;">
                                                        <div class="tab-content">
                                                   <div class="row">
                                                    <div class="col-md-4">
                                                     <div class=""> 
                                                     <label >Gender</label>
                                                     <select id="gender1" name="gender1"> 
                                                      <option value="0">--</option> 
                                                      <option value="1">Male</option> <option value="2">Female</option> </select>
                                                    </div>
                                                    </div>
                                                   <div class="col-md-4"> 
                                                    <label  class="ty-control-group__title cm-profile-field ">Fit</label>
                                                    <div class="btn-group" style="width: 100%;">
                                                     <select id="fit" name="fit" disabled> 
                                                      <option value="">--</option> 
                                                      <option value="28">Relaxed</option> <option value="29">Slim</option> 
                                                      <option value="33" class="hide">Custom</option> 
                                                     </select>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-4 standard-size-dropdowns"> <label for="elm_59" class="ty-control-group__title cm-profile-field ">Standard Size</label>
                                                   <div class="btn-group " style="width: 100%; display: block;">
                                                    <select id="standard_size" name="standard_size" disabled> 
                                                    <option value="" >--</option> 
                                                    <option value="19" >XS (36)</option> 
                                                    <option value="20" >S (38)</option> 
                                                    <option value="21" >M (40)</option> 
                                                    <option value="22" >L (42)</option> 
                                                    <option value="23" >XL (44)</option> 
                                                    <option value="24" >XXL (46)</option> 
                                                    <option value="25" >XXXL (48)</option> 
                                                    <option value="32" class="hide" disabled="disabled">Custom</option> 
                                                    </select>
                                                    </div>
                                                  <div class="btn-group" id="women" style="width: 100%; display: none;">
                                                   <select > 
                                                   <option value="">--</option> 
                                                   <option value="30">0 </option> 
                                                   <option value="14">2</option> 
                                                   <option value="15">4</option> 
                                                   <option value="16">6</option> 
                                                   <option value="17">8</option> 
                                                   <option value="18">10</option> 
                                                   <option value="31" class="hide">Custom</option> 
                                                   </select>
                                                   </div>
                                                  </div>
                                                  </div>
                                                  <div class="row">
                                                   <div class="col-md-12">
                                                    <p class="post-tip"> <a rel="shadowbox;width=1366;height=669;player=inline" href="#size-chart-desktop" class="pull-right size-chart-link hidden-xs hidden-sm">Size Chart</a> <a rel="shadowbox;width=436;height=400;player=inline" href="#size-chart-mobile" class="pull-right size-chart-link hidden-md hidden-lg">Size Chart</a>            </p>
                                                   </div>
                                                  </div>
                                                  <div class="row">
                                                   <div class="col-md-12 text-center fit-promise" style="display: block;">
                                                    <h2>FIT PROMISE</h2>
                                                    <p class="hidden-sm hidden-xs">If you're not completely satisfied with the fit on<br>your first order, we will alter your shirt or completely remake it free of cost.</p>
                                                    <p class="hidden-md hidden-lg">If you're not completely satisfied with the fit on your first order, we will alter your shirt or completely remake it free of cost.</p>
                                                   </div>
                                                  </div>
                                                  </div>
                                                    </div>
                                                    <div class="profile-sub-tab custom-measurements" style="display:none;">
                                                        <div class="boxed white measurement-values" style="">
                                                            <div class="row">
                                                                <div class="col-md-12 text-center">
                                                                    <p>
                                                                        We've saved your profile with your final custom <strong>shirt measurements</strong>, in <strong>inches</strong>.<br>You can <strong>edit</strong> any measurement you like by using
                                                                        the + / - buttons, or else click continue to proceed.<br><br> <span class="hidden-xs hidden-sm">Use our <strong><a href="#" target="_blank">How to Measure guide</a></strong> to get the perfect fit.<br><br><br></span>                                                                        </p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-3 col-md-offset-1 col-sm-6 right"> <label for="elm_62" class="ty-control-group__title cm-profile-field "> <span class="hint"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/sizing-collar.jpg" /> </span>
                                                                Collar
                                                                </label> <input data-undo="0.00" min="0.00" max="200.00" data-step="0.25" type="text" id="elm_62" name="user_data[fields][62]" value="0.00" class="spinner ty-input-text  " readonly="readyonly" data-shirt="collar" /> <label for="elm_63" class="ty-control-group__title cm-profile-field "> <span class="hint"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/sizing-chest.jpg" /> </span>
                                                                Chest
                                                                </label> <input data-undo="0.00" min="0.00" max="200.00" data-step="0.5" type="text" id="elm_63" name="user_data[fields][63]" value="0.00" class="spinner ty-input-text  " readonly="readyonly" data-shirt="chest" /> <label for="elm_64" class="ty-control-group__title cm-profile-field "> <span class="hint"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/sizing-waist.jpg" /> </span>
                                                                Waist
                                                                </label> <input data-undo="0.00" min="0.00" max="200.00" data-step="0.5" type="text" id="elm_64" name="user_data[fields][64]" value="0.00" class="spinner ty-input-text  " readonly="readyonly" data-shirt="waist" /> <label for="elm_71" class="ty-control-group__title cm-profile-field ">
                                                                Hips
                                                                </label> <input data-undo="0.00" min="0.00" max="200.00" data-step="0.5" type="text" id="elm_71" name="user_data[fields][71]" value="0.00" class="spinner ty-input-text  " readonly="readyonly" data-shirt="hips" /> </div>
                                                                <div class="col-md-4 hidden-sm hidden-xs">
                                                                    <div class="shirt"> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/shirt.jpg" class="default" /> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/shirt-bicep.jpg"
                                                                            class="bicep" /> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/shirt-chest.jpg" class="chest" /> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/shirt-collar.jpg"
                                                                            class="collar" /> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/shirt-cuff.jpg" class="cuff" /> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/shirt-length.jpg"
                                                                            class="shirt-length" /> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/shirt-shoulder.jpg" class="shoulder" /> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/shirt-sleeve-length.jpg"
                                                                            class="sleeve-length" /> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/shirt-waist.jpg" class="waist" /> </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-6 left"> <label for="elm_65" class="ty-control-group__title cm-profile-field ">
                                                                    Shoulders
                                                                    <span class="hint"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/sizing-shoulder-length.jpg" /> </span> </label> <input data-undo="0.00" min="0.00" max="200.00"
                                                                        data-step="0.5" type="text" id="elm_65" name="user_data[fields][65]" value="0.00" class="spinner ty-input-text  " readonly="readyonly" data-shirt="shoulder" /> <label for="elm_67" class="ty-control-group__title cm-profile-field ">
                                                                        Biceps
                                                                        <span class="hint"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/sizing-bicep.jpg" /> </span> </label> <input data-undo="0.00" min="0.00" max="200.00" data-step="0.5"
                                                                        type="text" id="elm_67" name="user_data[fields][67]" value="0.00" class="spinner ty-input-text  " readonly="readyonly" data-shirt="bicep" /> <label for="elm_68" class="ty-control-group__title cm-profile-field ">
                                                                        Sleeve Length
                                                                        <span class="hint"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/sizing-sleeve-length.jpg" /> </span> </label> <input data-undo="0.00" min="0.00" max="200.00"
                                                                        data-step="0.5" type="text" id="elm_68" name="user_data[fields][68]" value="0.00" class="spinner ty-input-text  " readonly="readyonly" data-shirt="sleeve-length" /> <label for="elm_69"
                                                                        class="ty-control-group__title cm-profile-field ">
Cuffs
<span class="hint"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/sizing-cuff.jpg" /> </span> </label> <input data-undo="0.00" min="0.00" max="200.00" data-step="0.25"
                                                                        type="text" id="elm_69" name="user_data[fields][69]" value="0.00" class="spinner ty-input-text  " readonly="readyonly" data-shirt="cuff" /> </div>
                                                                <div class="col-sm-10 col-sm-offset-1 center">
                                                                <label for="elm_70" class="ty-control-group__title cm-profile-field "> <span class="hint"><i class="fa fa-info-circle"></i></span> <span class="hidden"> <img src="https://www.bombayshirts.com/design/themes/bsc/media/images/profile/sizing-shirt-length.jpg" /> </span>
Shirt Length
</label> <input data-undo="0.00" min="0.00" max="200.00" data-step="0.5" type="text" id="elm_70" name="user_data[fields][70]" value="0.00" class="spinner ty-input-text  " readonly="readyonly" data-shirt="shirt-length" /> </div>
                                                                <div class="col-sm-12 hidden">
                                                                <label for="elm_89" class="ty-control-group__title cm-profile-field ">Sizing Fit</label> <select data-undo="" data-profile="sizing-fit" data-hide-disabled="true" data-type="select" data-width="100%"
                                                                        id="elm_89" class="ty-profile-field__select " name="user_data[fields][89]"> <option value="">--</option> <option value="40">Body</option> <option value="41">Structured</option> <option value="42">Comfort</option> <option value="43">Fitted</option> <option value="44" class="hide">Loose</option> </select>                                                                    <label for="elm_86" class="ty-control-group__title cm-profile-field ">
Sizing Height
</label> <input data-undo="0.00" min="0.00" max="200.00" data-step="1" type="text" id="elm_86" name="user_data[fields][86]" value="0.00" class="spinner ty-input-text  " readonly="readyonly" data-profile="sizing-height" /> <label for="elm_87" class="ty-control-group__title cm-profile-field ">
Sizing Weight
</label> <input data-undo="0.00" min="0.00" max="200.00" data-step="1" type="text" id="elm_87" name="user_data[fields][87]" value="0.00" class="spinner ty-input-text  " readonly="readyonly" data-profile="sizing-weight" /> <label for="elm_88" class="ty-control-group__title cm-profile-field ">Sizing Shape</label>                                                                    <select data-undo="" data-profile="sizing-shape" data-hide-disabled="true" data-type="select" data-width="100%" id="elm_88" class="ty-profile-field__select " name="user_data[fields][88]"> <option value="">--</option> <option value="36">Skinny</option> <option value="37">Athletic</option> <option value="38">Average</option> <option value="39" class="hide">Healthy</option> </select>                                                                    <label for="elm_90" class="ty-control-group__title cm-profile-field ">Sloping Shoulder</label> <select data-undo="" data-profile="sloping-shoulder" data-hide-disabled="true" data-type="select"
                                                                        data-width="100%" id="elm_90" class="ty-profile-field__select " name="user_data[fields][90]"> <option value="">--</option> <option value="45">No</option> <option value="46">Yes</option> <option value="47" class="hide">Don&#039;t Know</option> </select>                                                                    </div>
                                                            </div>
                                                            <div class="modified">
                                                                <p><strong>Last updated on 16:25 Friday, 2nd Feb 2018 by <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="9ae8fbeefbf4adaba9a3dafdf7fbf3f6b4f9f5f7">[email&#160;protected]</a></strong></p>
                                                                <input type="text" id="elm_49" name="user_data[fields][49]" value="16:25 Friday, 2nd Feb 2018 by ratan7139@gmail.com" class="hide ty-input-text  " readonly="readyonly" data-profile="modified"
                                                                /> </div>
                                                            <div class="comments"> <textarea placeholder="Your profile comments." class="form-control " id="elm_48" name="user_data[fields][48]" cols="32" rows="1"></textarea> </div>
                                                        </div>
                                                    </div>
                                                    <div class="profile-sub-tab send-a-shirt" style="display: none;">
                                                        <div class="boxed tab-content">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <p>
                                                                        If you've already found the perfect-fitting shirt, there's no need to reinvent the wheel!<br /><br /> Send us your shirt by mail - we'll use it to get your measurements, and send
                                                                        it back with your order. We'll then save those measurements for future use.<br /><br /> <strong>Sending us a shirt?</strong><br /> <label data-profile="send-a-shirt"> <input class="radio  elm_56" type="radio" id="elm_56_11" name="user_data[fields][56]" value="11" /> <span class="radio">No</span> </label>                                                                        <label data-profile="send-a-shirt"> <input class="radio  elm_56" type="radio" id="elm_56_12" name="user_data[fields][56]" value="12" checked="checked" /> <span class="radio">Yes</span> </label>                                                                        <br /><br /> <br /> (Remember to mention your <em>Order #, Sample Shirt and Not for Sale</em>)<br /><br /> <a target="blank" href="design/themes/bsc/media/images/homepage/bombayshirts---send-us-a-sample-shirt.pdf"
                                                                            class="btn btn-bordered">View Address &amp; Print Label</a> </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal fade page-modal" id="sizing-modal" role="dialog" tabindex="-1">
                                                        <div class="vertical-alignment-helper">
                                                            <div class="modal-dialog vertical-align-center">
                                                                <div class="modal-content">
                                                                    <div class="modal-body">
                                                                        <p>Please select a sizing option and fill out your measurements before moving on to the next step.</p>
                                                                    </div>
                                                                    <div class="modal-footer"> <button class="btn btn-primary" data-dismiss="modal" type="button">Go to Measurements</button> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h1 class="profile-tab-header profile-tab-header3">Billing &amp; Shipping Address</h1>
                                                <?PHP
                                                $billingProfile = $wpdb->get_row("SELECT first_name,last_name,phone,address,address_line_2,city,country,state,zip_code,email FROM wp_user_billing_address WHERE user_id ='$uId'");
                                                $UfName = $billingProfile->first_name;
                                                $UlName = $billingProfile->last_name;
                                                $Uphone = $billingProfile->phone;
                                                $Uaddress1 = $billingProfile->address;
                                                $Uaddress2 = $billingProfile->address_line_2;
                                                $Ucity = $billingProfile->city;
                                                $Ucountry = $billingProfile->country;
                                                $Ustate = $billingProfile->state;
                                                $Uzip = $billingProfile->zip_code;
                                                $uEmail = $billingProfile->email;
                                                ?>
                                                <div class="profile-tab profile-tab3 no-labels" style="display: none;"> <input type="hidden" name="ship_to_another" value="1" />
                                                    <h3>Billing address</h3>
                                                    <form action="" method="POST">
                                                    <div class="form-group billing-first-name">
                                                        <div class="col-sm-3 hide"> <label for="elm_14" class="control-label cm-profile-field  ">First name</label> </div>
                                                        <div class="col-sm-12"><input placeholder="First name" x-autocompletetype="given-name" type="text" id="elm_14" name="firstName" value="<?php echo $UfName; ?>" size="32" class="form-control   cm-focus" /> </div>
                                                    </div>
                                                    <div class="form-group billing-last-name">
                                                        <div class="col-sm-3 hide"> <label for="elm_16" class="control-label cm-profile-field  ">Last name</label> </div>
                                                        <div class="col-sm-12"><input placeholder="Last name" x-autocompletetype="surname" type="text" id="elm_16" name="lastName" value="<?php echo $UlName; ?>" size="32" class="form-control  " /> </div>
                                                    </div>
                                                    <div class="form-group billing-phone">
                                                        <div class="col-sm-3 hide"> <label for="elm_30" class="control-label cm-profile-field  ">Phone</label> </div>
                                                        <div class="col-sm-12"><input placeholder="Phone" type="text"  name="phone" size="32" value="<?php echo $Uphone; ?>"  class="form-control  " /> </div>
                                                    </div>
                                                    <div class="form-group billing-email">
                                                        <div class="col-sm-3 hide"> <label for="" class="control-label cm-profile-field  ">Email</label> </div>
                                                        <div class="col-sm-12"><input placeholder="Email" x-autocompletetype="email-full" type="text"  name="email" size="32"  value="<?php echo $uEmail; ?>" class="form-control  " /> </div>
                                                    </div>
                                                    <div class="form-group billing-address">
                                                        <div class="col-sm-3 hide"> <label for="elm_18" class="control-label cm-profile-field  ">Address</label> </div>
                                                        <div class="col-sm-12"><input placeholder="Address" x-autocompletetype="street-address" type="text" id="elm_18" name="address1" size="32" value="<?php echo $Uaddress1; ?>"  class="form-control  " /> </div>
                                                    </div>
                                                    <div class="form-group billing-address-line-two">
                                                        <div class="col-sm-3 hide"> <label for="elm_20" class="control-label cm-profile-field  ">Address Line 2</label> </div>
                                                        <div class="col-sm-12"><input placeholder="Address Line 2" x-autocompletetype="address-line2" type="text" id="elm_20" name="address2" value="<?php echo $Uaddress2; ?>" size="32" class="form-control  " /> </div>
                                                    </div>
                                                    <div class="form-group billing-city">
                                                        <div class="col-sm-3 hide"> <label for="elm_22" class="control-label cm-profile-field  ">City</label> </div>
                                                        <div class="col-sm-12"><input placeholder="City" x-autocompletetype="city" type="text" id="elm_22" name="city" value="<?php echo $Ucity; ?>" size="32"  class="form-control  " /> </div>
                                                    </div>
                                                    <div class="form-group billing-country">
                                                       
                                                        <div class="col-sm-3 hide"> <label for="elm_22" class="control-label cm-profile-field  ">Country</label> </div>
                                                        <div class="col-sm-12"><input placeholder="Country" x-autocompletetype="country" type="text" id="elm_26" name="country" size="32" value="India" class="form-control  " /></div>
                                                       

                                                        <!-- <div class="col-sm-3 hide"> <label for="elm_26" class="control-label cm-profile-field  ">Country</label> </div> --><!-- 
                                                        <div class="col-sm-12"> <select x-autocompletetype="country" id="elm_26" class="form-control cm-country cm-location-billing " name="user_data[b_country]"> <option value="">- Select country -</option> <option value="AF">Afghanistan</option> <option value="AX">Aland Islands</option> <option value="AL">Albania</option> <option value="DZ">Algeria</option> <option value="AS">American Samoa</option> <option value="AD">Andorra</option> <option value="AO">Angola</option> <option value="AI">Anguilla</option> <option value="AQ">Antarctica</option> <option value="AG">Antigua and Barbuda</option> <option value="AR">Argentina</option> <option value="AM">Armenia</option> <option value="AW">Aruba</option> <option value="AP">Asia-Pacific</option> <option value="AU">Australia</option> <option value="AT">Austria</option> <option value="AZ">Azerbaijan</option> <option value="BS">Bahamas</option> <option value="BH">Bahrain</option> <option value="BD">Bangladesh</option> <option value="BB">Barbados</option> <option value="BY">Belarus</option> <option value="BE">Belgium</option> <option value="BZ">Belize</option> <option value="BJ">Benin</option> <option value="BM">Bermuda</option> <option value="BT">Bhutan</option> <option value="BO">Bolivia</option> <option value="BA">Bosnia and Herzegowina</option> <option value="BW">Botswana</option> <option value="BV">Bouvet Island</option> <option value="BR">Brazil</option> <option value="IO">British Indian Ocean Territory</option> <option value="VG">British Virgin Islands</option> <option value="BN">Brunei Darussalam</option> <option value="BG">Bulgaria</option> <option value="BF">Burkina Faso</option> <option value="BI">Burundi</option> <option value="KH">Cambodia</option> <option value="CM">Cameroon</option> <option value="CA">Canada</option> <option value="CV">Cape Verde</option> <option value="KY">Cayman Islands</option> <option value="CF">Central African Republic</option> <option value="TD">Chad</option> <option value="CL">Chile</option> <option value="CN">China</option> <option value="CX">Christmas Island</option> <option value="CC">Cocos (Keeling) Islands</option> <option value="CO">Colombia</option> <option value="KM">Comoros</option> <option value="CG">Congo</option> <option value="CK">Cook Islands</option> <option value="CR">Costa Rica</option> <option value="CI">Cote D&#039;ivoire</option> <option value="HR">Croatia</option> <option value="CU">Cuba</option> <option value="CW">Curaçao</option> <option value="CY">Cyprus</option> <option value="CZ">Czech Republic</option> <option value="DK">Denmark</option> <option value="DJ">Djibouti</option> <option value="DM">Dominica</option> <option value="DO">Dominican Republic</option> <option value="TL">East Timor</option> <option value="EC">Ecuador</option> <option value="EG">Egypt</option> <option value="SV">El Salvador</option> <option value="GQ">Equatorial Guinea</option> <option value="ER">Eritrea</option> <option value="EE">Estonia</option> <option value="ET">Ethiopia</option> <option value="EU">Europe</option> <option value="FK">Falkland Islands (Malvinas)</option> <option value="FO">Faroe Islands</option> <option value="FJ">Fiji</option> <option value="FI">Finland</option> <option value="FR">France</option> <option value="FX">France, Metropolitan</option> <option value="GF">French Guiana</option> <option value="PF">French Polynesia</option> <option value="TF">French Southern Territories</option> <option value="GA">Gabon</option> <option value="GM">Gambia</option> <option value="GE">Georgia</option> <option value="DE">Germany</option> <option value="GH">Ghana</option> <option value="GI">Gibraltar</option> <option value="GR">Greece</option> <option value="GL">Greenland</option> <option value="GD">Grenada</option> <option value="GP">Guadeloupe</option> <option value="GU">Guam</option> <option value="GT">Guatemala</option> <option value="GG">Guernsey</option> <option value="GN">Guinea</option> <option value="GW">Guinea-Bissau</option> <option value="GY">Guyana</option> <option value="HT">Haiti</option> <option value="HM">Heard and McDonald Islands</option> <option value="HN">Honduras</option> <option value="HK">Hong Kong</option> <option value="HU">Hungary</option> <option value="IS">Iceland</option> <option selected="selected" value="IN">India</option> <option value="ID">Indonesia</option> <option value="IQ">Iraq</option> <option value="IE">Ireland</option> <option value="IR">Islamic Republic of Iran</option> <option value="IM">Isle of Man</option> <option value="IL">Israel</option> <option value="IT">Italy</option> <option value="JM">Jamaica</option> <option value="JP">Japan</option> <option value="JE">Jersey</option> <option value="JO">Jordan</option> <option value="KZ">Kazakhstan</option> <option value="KE">Kenya</option> <option value="KI">Kiribati</option> <option value="KP">Korea</option> <option value="KR">Korea, Republic of</option> <option value="KW">Kuwait</option> <option value="KG">Kyrgyzstan</option> <option value="LA">Laos</option> <option value="LV">Latvia</option> <option value="LB">Lebanon</option> <option value="LS">Lesotho</option> <option value="LR">Liberia</option> <option value="LY">Libyan Arab Jamahiriya</option> <option value="LI">Liechtenstein</option> <option value="LT">Lithuania</option> <option value="LU">Luxembourg</option> <option value="MO">Macau</option> <option value="MK">Macedonia</option> <option value="MG">Madagascar</option> <option value="MW">Malawi</option> <option value="MY">Malaysia</option> <option value="MV">Maldives</option> <option value="ML">Mali</option> <option value="MT">Malta</option> <option value="MH">Marshall Islands</option> <option value="MQ">Martinique</option> <option value="MR">Mauritania</option> <option value="MU">Mauritius</option> <option value="YT">Mayotte</option> <option value="MX">Mexico</option> <option value="FM">Micronesia</option> <option value="MD">Moldova, Republic of</option> <option value="MC">Monaco</option> <option value="MN">Mongolia</option> <option value="ME">Montenegro</option> <option value="MS">Montserrat</option> <option value="MA">Morocco</option> <option value="MZ">Mozambique</option> <option value="MM">Myanmar</option> <option value="NA">Namibia</option> <option value="NR">Nauru</option> <option value="NP">Nepal</option> <option value="NL">Netherlands</option> <option value="NC">New Caledonia</option> <option value="NZ">New Zealand</option> <option value="NI">Nicaragua</option> <option value="NE">Niger</option> <option value="NG">Nigeria</option> <option value="NU">Niue</option> <option value="NF">Norfolk Island</option> <option value="MP">Northern Mariana Islands</option> <option value="NO">Norway</option> <option value="OM">Oman</option> <option value="PK">Pakistan</option> <option value="PW">Palau</option> <option value="PS">Palestine Authority</option> <option value="PA">Panama</option> <option value="PG">Papua New Guinea</option> <option value="PY">Paraguay</option> <option value="PE">Peru</option> <option value="PH">Philippines</option> <option value="PN">Pitcairn</option> <option value="PL">Poland</option> <option value="PT">Portugal</option> <option value="PR">Puerto Rico</option> <option value="QA">Qatar</option> <option value="RS">Republic of Serbia</option> <option value="RE">Reunion</option> <option value="RO">Romania</option> <option value="RU">Russian Federation</option> <option value="RW">Rwanda</option> <option value="LC">Saint Lucia</option> <option value="WS">Samoa</option> <option value="SM">San Marino</option> <option value="ST">Sao Tome and Principe</option> <option value="SA">Saudi Arabia</option> <option value="SN">Senegal</option> <option value="CS">Serbia</option> <option value="SC">Seychelles</option> <option value="SL">Sierra Leone</option> <option value="SG">Singapore</option> <option value="SX">Sint Maarten</option> <option value="SK">Slovakia</option> <option value="SI">Slovenia</option> <option value="SB">Solomon Islands</option> <option value="SO">Somalia</option> <option value="ZA">South Africa</option> <option value="ES">Spain</option> <option value="LK">Sri Lanka</option> <option value="SH">St. Helena</option> <option value="KN">St. Kitts and Nevis</option> <option value="PM">St. Pierre and Miquelon</option> <option value="VC">St. Vincent and the Grenadines</option> <option value="SD">Sudan</option> <option value="SR">Suriname</option> <option value="SJ">Svalbard and Jan Mayen Islands</option> <option value="SZ">Swaziland</option> <option value="SE">Sweden</option> <option value="CH">Switzerland</option> <option value="SY">Syrian Arab Republic</option> <option value="TW">Taiwan</option> <option value="TJ">Tajikistan</option> <option value="TZ">Tanzania, United Republic of</option> <option value="TH">Thailand</option> <option value="TG">Togo</option> <option value="TK">Tokelau</option> <option value="TO">Tonga</option> <option value="TT">Trinidad and Tobago</option> <option value="TN">Tunisia</option> <option value="TR">Turkey</option> <option value="TM">Turkmenistan</option> <option value="TC">Turks and Caicos Islands</option> <option value="TV">Tuvalu</option> <option value="UG">Uganda</option> <option value="UA">Ukraine</option> <option value="AE">United Arab Emirates</option> <option value="GB">United Kingdom (Great Britain)</option> <option value="US">United States</option> <option value="VI">United States Virgin Islands</option> <option value="UY">Uruguay</option> <option value="UZ">Uzbekistan</option> <option value="VU">Vanuatu</option> <option value="VA">Vatican City State</option> <option value="VE">Venezuela</option> <option value="VN">Viet Nam</option> <option value="WF">Wallis And Futuna Islands</option> <option value="EH">Western Sahara</option> <option value="YE">Yemen</option> <option value="ZR">Zaire</option> <option value="ZM">Zambia</option> <option value="ZW">Zimbabwe</option> </select>                                                            </div> -->
                                                    </div>
                                                    <div class="form-group billing-state">
                                                        <div class="col-sm-3 hide"> <label for="elm_24" class="control-label cm-profile-field  ">State/province</label> </div>
                                                        <div class="col-sm-12"> <select x-autocompletetype="state" id="elm_24" class="form-control cm-state cm-location-billing " name="state"> <option value="<?php echo $state; ?>">- Select state -</option> <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option> <option value="Andhra Pradesh">Andhra Pradesh</option> <option value="Arunachal Pradesh">Arunachal Pradesh</option> <option value="Assam">Assam</option> <option value="Bihar">Bihar</option> <option value="Chandigarh">Chandigarh</option> <option value="Chhattisgarh">Chhattisgarh</option> <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option> <option value="Daman and Diu">Daman and Diu</option> <option value="Delhi">Delhi</option> <option value="Goa">Goa</option> <option value="Gujarat">Gujarat</option> <option value="Haryana">Haryana</option> <option value="Himachal Pradesh">Himachal Pradesh</option> <option value="Jammu and Kashmir">Jammu and Kashmir</option> <option value="Jharkhand">Jharkhand</option> <option value="Karnataka">Karnataka</option> <option value="Kerala">Kerala</option> <option value="Lakshadweep">Lakshadweep</option> <option value="Madhya">Madhya Pradesh</option> <option value="Maharashtra">Maharashtra</option> <option value="Manipur">Manipur</option> <option value="Meghalaya">Meghalaya</option> <option value="Mizoram">Mizoram</option> <option value="Nagaland">Nagaland</option> <option value="Odisha">Odisha</option> <option value="Puducherry">Puducherry</option> <option value="Punjab">Punjab</option> <option value="Rajasthan">Rajasthan</option> <option value="Sikkim">Sikkim</option> <option value="Tamil Nadu">Tamil Nadu</option> <option value="Telangana">Telangana</option> <option value="Tripura">Tripura</option> <option selected="selected" value="Uttar Pradesh">Uttar Pradesh</option> <option value="Uttarakhand">Uttarakhand</option> <option value="West Bengal">West Bengal</option> </select><input
                                                                x-autocompletetype="state" placeholder="State/province" type="text" id="elm_24_d" name="state" size="32" maxlength="64" disabled="disabled"
                                                                class="cm-state cm-location-billing form-control hidden " /> </div>
                                                    </div>
                                                    <div class="form-group billing-zip-code">
                                                        <div class="col-sm-3 hide"> <label for="elm_28" class="control-label cm-profile-field  cm-zipcode cm-location-billing">Zip/postal code</label> </div>
                                                        <div class="col-sm-12"><input placeholder="Zip/postal code" x-autocompletetype="postal-code" type="text" id="elm_28" name="zip" value="<?php echo $Uzip; ?>" size="32" class="form-control  " /> </div>
                                                    </div>
                                                    <button id="save_profile_but" class="btn btn-primary" type="submit" name="billingForm">Save</button> 
                                                </form>
                                                    <!-- <div class="panel panel-default address-check">
                                                        <div class="panel-body"> <span>Are shipping and billing addresses the same?</span>
                                                            <div class="pull-right"> <label class="radio-inline control-label" for="sw_sa_suffix_no"> <input class="radio cm-switch-availability cm-switch-visibility" type="radio" name="ship_to_another" value="1" id="sw_sa_suffix_no" />
                                                            No
                                                            </label> <label class="radio-inline control-label" for="sw_sa_suffix_yes"> <input class="radio cm-switch-availability cm-switch-inverse cm-switch-visibility" type="radio" name="ship_to_another" value="0" id="sw_sa_suffix_yes" checked="checked" />
                                                            Yes
                                                            </label> </div>
                                                        </div>
                                                    </div> -->
                                                    <div id="sa" class="hidden shipping address">
                                                        <div class="">
                                                            <h3>Shipping address</h3>
                                                            <form action="" method="POST">
                                                            <div class="form-group shipping-first-name">
                                                                <div class="col-sm-3 hide"> <label for="elm_15" class="control-label cm-profile-field  ">First name</label> </div>
                                                                <div class="col-sm-12"><input placeholder="First name" x-autocompletetype="given-name" type="text" id="elm_15" name="firstName" value="<?php echo $UfName; ?>" size="32"  class="form-control disabled  cm-focus" disabled="disabled"
                                                                    /> </div>
                                                            </div>
                                                            <div class="form-group shipping-last-name">
                                                                <div class="col-sm-3 hide"> <label for="elm_17" class="control-label cm-profile-field  ">Last name</label> </div>
                                                                <div class="col-sm-12"><input placeholder="Last name" x-autocompletetype="surname" type="text" id="elm_17" name="lastName" value="<?php echo $UlName; ?>" size="32"  class="form-control disabled " disabled="disabled"
                                                                    /> 
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group shipping-last-email">
                                                                <div class="col-sm-3 hide"> <label for="elm_17" class="control-label cm-profile-field  ">Email</label> </div>
                                                                <div class="col-sm-12"><input placeholder="Last name" x-autocompletetype="email" type="text" id="elm_17" name="email" value="<?php echo $uEmail; ?>" size="32"  class="form-control disabled " disabled="disabled"
                                                                    /> 
                                                                </div>
                                                            </div>
                                                            <div class="form-group shipping-phone">
                                                                <div class="col-sm-3 hide"> <label for="elm_31" class="control-label cm-profile-field  ">Phone</label> </div>
                                                                <div class="col-sm-12"><input placeholder="Phone"  type="text"  name="phone" value="<?php echo $Uphone; ?>" size="32"  class="form-control disabled " disabled="disabled"
                                                                    /> 
                                                                </div>
                                                            </div>
                                                            <div class="form-group shipping-address">
                                                                <div class="col-sm-3 hide"> <label for="elm_19" class="control-label cm-profile-field  ">Address</label> </div>
                                                                <div class="col-sm-12"><input placeholder="Address" x-autocompletetype="street-address" type="text" id="elm_19" name="address1" value="<?php echo $Uaddress1; ?>" size="32"  class="form-control disabled " disabled="disabled"
                                                                    /> </div>
                                                            </div>
                                                            <div class="form-group shipping-address-line-two">
                                                                <div class="col-sm-3 hide"> <label for="elm_21" class="control-label cm-profile-field  ">Address Line 2</label> </div>
                                                                <div class="col-sm-12"><input placeholder="Address Line 2" x-autocompletetype="address-line2" type="text" id="elm_21" name="address2" size="32" value="<?php echo $Uaddress2; ?>"  class="form-control disabled " disabled="disabled"
                                                                    /> </div>
                                                            </div>
                                                            <div class="form-group shipping-city">
                                                                <div class="col-sm-3 hide"> <label for="elm_23" class="control-label cm-profile-field  ">City</label> </div>
                                                                <div class="col-sm-12"><input placeholder="City" x-autocompletetype="city" type="text" id="elm_23" name="city" size="32" value="<?php echo $Ucity; ?>"  class="form-control disabled " disabled="disabled" /> </div>
                                                            </div>
                                                            <div class="form-group shipping-country">
                                                                <div class="col-sm-3 hide"> <label for="elm_27" class="control-label cm-profile-field  ">Country</label> </div>
                                                                <div class="col-sm-12"><input placeholder="Country" x-autocompletetype="country" type="text" id="elm_23" name="country" value="India" size="32" class="form-control disabled " disabled="disabled" /> </div>
                                                                <!-- <div class="col-sm-12"> <select x-autocompletetype="country" id="elm_27" class="form-control cm-country cm-location-shipping disabled" name="country" disabled="disabled"> <option value="">- Select country -</option> <option value="AF">Afghanistan</option> <option value="AX">Aland Islands</option> <option value="AL">Albania</option> <option value="DZ">Algeria</option> <option value="AS">American Samoa</option> <option value="AD">Andorra</option> <option value="AO">Angola</option> <option value="AI">Anguilla</option> <option value="AQ">Antarctica</option> <option value="AG">Antigua and Barbuda</option> <option value="AR">Argentina</option> <option value="AM">Armenia</option> <option value="AW">Aruba</option> <option value="AP">Asia-Pacific</option> <option value="AU">Australia</option> <option value="AT">Austria</option> <option value="AZ">Azerbaijan</option> <option value="BS">Bahamas</option> <option value="BH">Bahrain</option> <option value="BD">Bangladesh</option> <option value="BB">Barbados</option> <option value="BY">Belarus</option> <option value="BE">Belgium</option> <option value="BZ">Belize</option> <option value="BJ">Benin</option> <option value="BM">Bermuda</option> <option value="BT">Bhutan</option> <option value="BO">Bolivia</option> <option value="BA">Bosnia and Herzegowina</option> <option value="BW">Botswana</option> <option value="BV">Bouvet Island</option> <option value="BR">Brazil</option> <option value="IO">British Indian Ocean Territory</option> <option value="VG">British Virgin Islands</option> <option value="BN">Brunei Darussalam</option> <option value="BG">Bulgaria</option> <option value="BF">Burkina Faso</option> <option value="BI">Burundi</option> <option value="KH">Cambodia</option> <option value="CM">Cameroon</option> <option value="CA">Canada</option> <option value="CV">Cape Verde</option> <option value="KY">Cayman Islands</option> <option value="CF">Central African Republic</option> <option value="TD">Chad</option> <option value="CL">Chile</option> <option value="CN">China</option> <option value="CX">Christmas Island</option> <option value="CC">Cocos (Keeling) Islands</option> <option value="CO">Colombia</option> <option value="KM">Comoros</option> <option value="CG">Congo</option> <option value="CK">Cook Islands</option> <option value="CR">Costa Rica</option> <option value="CI">Cote D&#039;ivoire</option> <option value="HR">Croatia</option> <option value="CU">Cuba</option> <option value="CW">Curaçao</option> <option value="CY">Cyprus</option> <option value="CZ">Czech Republic</option> <option value="DK">Denmark</option> <option value="DJ">Djibouti</option> <option value="DM">Dominica</option> <option value="DO">Dominican Republic</option> <option value="TL">East Timor</option> <option value="EC">Ecuador</option> <option value="EG">Egypt</option> <option value="SV">El Salvador</option> <option value="GQ">Equatorial Guinea</option> <option value="ER">Eritrea</option> <option value="EE">Estonia</option> <option value="ET">Ethiopia</option> <option value="EU">Europe</option> <option value="FK">Falkland Islands (Malvinas)</option> <option value="FO">Faroe Islands</option> <option value="FJ">Fiji</option> <option value="FI">Finland</option> <option value="FR">France</option> <option value="FX">France, Metropolitan</option> <option value="GF">French Guiana</option> <option value="PF">French Polynesia</option> <option value="TF">French Southern Territories</option> <option value="GA">Gabon</option> <option value="GM">Gambia</option> <option value="GE">Georgia</option> <option value="DE">Germany</option> <option value="GH">Ghana</option> <option value="GI">Gibraltar</option> <option value="GR">Greece</option> <option value="GL">Greenland</option> <option value="GD">Grenada</option> <option value="GP">Guadeloupe</option> <option value="GU">Guam</option> <option value="GT">Guatemala</option> <option value="GG">Guernsey</option> <option value="GN">Guinea</option> <option value="GW">Guinea-Bissau</option> <option value="GY">Guyana</option> <option value="HT">Haiti</option> <option value="HM">Heard and McDonald Islands</option> <option value="HN">Honduras</option> <option value="HK">Hong Kong</option> <option value="HU">Hungary</option> <option value="IS">Iceland</option> <option selected="selected" value="IN">India</option> <option value="ID">Indonesia</option> <option value="IQ">Iraq</option> <option value="IE">Ireland</option> <option value="IR">Islamic Republic of Iran</option> <option value="IM">Isle of Man</option> <option value="IL">Israel</option> <option value="IT">Italy</option> <option value="JM">Jamaica</option> <option value="JP">Japan</option> <option value="JE">Jersey</option> <option value="JO">Jordan</option> <option value="KZ">Kazakhstan</option> <option value="KE">Kenya</option> <option value="KI">Kiribati</option> <option value="KP">Korea</option> <option value="KR">Korea, Republic of</option> <option value="KW">Kuwait</option> <option value="KG">Kyrgyzstan</option> <option value="LA">Laos</option> <option value="LV">Latvia</option> <option value="LB">Lebanon</option> <option value="LS">Lesotho</option> <option value="LR">Liberia</option> <option value="LY">Libyan Arab Jamahiriya</option> <option value="LI">Liechtenstein</option> <option value="LT">Lithuania</option> <option value="LU">Luxembourg</option> <option value="MO">Macau</option> <option value="MK">Macedonia</option> <option value="MG">Madagascar</option> <option value="MW">Malawi</option> <option value="MY">Malaysia</option> <option value="MV">Maldives</option> <option value="ML">Mali</option> <option value="MT">Malta</option> <option value="MH">Marshall Islands</option> <option value="MQ">Martinique</option> <option value="MR">Mauritania</option> <option value="MU">Mauritius</option> <option value="YT">Mayotte</option> <option value="MX">Mexico</option> <option value="FM">Micronesia</option> <option value="MD">Moldova, Republic of</option> <option value="MC">Monaco</option> <option value="MN">Mongolia</option> <option value="ME">Montenegro</option> <option value="MS">Montserrat</option> <option value="MA">Morocco</option> <option value="MZ">Mozambique</option> <option value="MM">Myanmar</option> <option value="NA">Namibia</option> <option value="NR">Nauru</option> <option value="NP">Nepal</option> <option value="NL">Netherlands</option> <option value="NC">New Caledonia</option> <option value="NZ">New Zealand</option> <option value="NI">Nicaragua</option> <option value="NE">Niger</option> <option value="NG">Nigeria</option> <option value="NU">Niue</option> <option value="NF">Norfolk Island</option> <option value="MP">Northern Mariana Islands</option> <option value="NO">Norway</option> <option value="OM">Oman</option> <option value="PK">Pakistan</option> <option value="PW">Palau</option> <option value="PS">Palestine Authority</option> <option value="PA">Panama</option> <option value="PG">Papua New Guinea</option> <option value="PY">Paraguay</option> <option value="PE">Peru</option> <option value="PH">Philippines</option> <option value="PN">Pitcairn</option> <option value="PL">Poland</option> <option value="PT">Portugal</option> <option value="PR">Puerto Rico</option> <option value="QA">Qatar</option> <option value="RS">Republic of Serbia</option> <option value="RE">Reunion</option> <option value="RO">Romania</option> <option value="RU">Russian Federation</option> <option value="RW">Rwanda</option> <option value="LC">Saint Lucia</option> <option value="WS">Samoa</option> <option value="SM">San Marino</option> <option value="ST">Sao Tome and Principe</option> <option value="SA">Saudi Arabia</option> <option value="SN">Senegal</option> <option value="CS">Serbia</option> <option value="SC">Seychelles</option> <option value="SL">Sierra Leone</option> <option value="SG">Singapore</option> <option value="SX">Sint Maarten</option> <option value="SK">Slovakia</option> <option value="SI">Slovenia</option> <option value="SB">Solomon Islands</option> <option value="SO">Somalia</option> <option value="ZA">South Africa</option> <option value="ES">Spain</option> <option value="LK">Sri Lanka</option> <option value="SH">St. Helena</option> <option value="KN">St. Kitts and Nevis</option> <option value="PM">St. Pierre and Miquelon</option> <option value="VC">St. Vincent and the Grenadines</option> <option value="SD">Sudan</option> <option value="SR">Suriname</option> <option value="SJ">Svalbard and Jan Mayen Islands</option> <option value="SZ">Swaziland</option> <option value="SE">Sweden</option> <option value="CH">Switzerland</option> <option value="SY">Syrian Arab Republic</option> <option value="TW">Taiwan</option> <option value="TJ">Tajikistan</option> <option value="TZ">Tanzania, United Republic of</option> <option value="TH">Thailand</option> <option value="TG">Togo</option> <option value="TK">Tokelau</option> <option value="TO">Tonga</option> <option value="TT">Trinidad and Tobago</option> <option value="TN">Tunisia</option> <option value="TR">Turkey</option> <option value="TM">Turkmenistan</option> <option value="TC">Turks and Caicos Islands</option> <option value="TV">Tuvalu</option> <option value="UG">Uganda</option> <option value="UA">Ukraine</option> <option value="AE">United Arab Emirates</option> <option value="GB">United Kingdom (Great Britain)</option> <option value="US">United States</option> <option value="VI">United States Virgin Islands</option> <option value="UY">Uruguay</option> <option value="UZ">Uzbekistan</option> <option value="VU">Vanuatu</option> <option value="VA">Vatican City State</option> <option value="VE">Venezuela</option> <option value="VN">Viet Nam</option> <option value="WF">Wallis And Futuna Islands</option> <option value="EH">Western Sahara</option> <option value="YE">Yemen</option> <option value="ZR">Zaire</option> <option value="ZM">Zambia</option> <option value="ZW">Zimbabwe</option> </select>                                                                    </div> -->
                                                            </div>
                                                            <div class="form-group shipping-state">
                                                                <div class="col-sm-3 hide"> <label for="elm_25" class="control-label cm-profile-field  ">State/province</label> </div>
                                                                <div class="col-sm-12"> <select x-autocompletetype="state" id="elm_25" class="form-control cm-state cm-location-shipping disabled" name="state" disabled="disabled"> <option value="">- Select state -</option> <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option> <option value="Andhra Pradesh">Andhra Pradesh</option> <option value="Arunachal Pradesh">Arunachal Pradesh</option> <option value="Assam">Assam</option> <option value="Bihar">Bihar</option> <option value="Chandigarh">Chandigarh</option> <option value="Chhattisgarh">Chhattisgarh</option> <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option> <option value="Daman and Diu">Daman and Diu</option> <option value="Delhi">Delhi</option> <option value="Goa">Goa</option> <option value="Gujarat">Gujarat</option> <option value="Haryana">Haryana</option> <option value="Himachal Pradesh">Himachal Pradesh</option> <option value="Jammu and Kashmir">Jammu and Kashmir</option> <option value="Jharkhand">Jharkhand</option> <option value="Karnataka">Karnataka</option> <option value="Kerala">Kerala</option> <option value="Lakshadweep">Lakshadweep</option> <option value="Madhya">Madhya Pradesh</option> <option value="Maharashtra">Maharashtra</option> <option value="Manipur">Manipur</option> <option value="Meghalaya">Meghalaya</option> <option value="Mizoram">Mizoram</option> <option value="Nagaland">Nagaland</option> <option value="Odisha">Odisha</option> <option value="Puducherry">Puducherry</option> <option value="Punjab">Punjab</option> <option value="Rajasthan">Rajasthan</option> <option value="Sikkim">Sikkim</option> <option value="Tamil Nadu">Tamil Nadu</option> <option value="Telangana">Telangana</option> <option value="Tripura">Tripura</option> <option selected="selected" value="Uttar Pradesh">Uttar Pradesh</option> <option value="Uttarakhand">Uttarakhand</option> <option value="West Bengal">West Bengal</option> </select><input
                                                                        x-autocompletetype="state" placeholder="State/province" type="text" placeholder="State/province" id="elm_25_d" name="state" size="32" maxlength="64"  disabled="disabled"
                                                                        class="cm-state cm-location-shipping form-control hidden disabled" /> </div>
                                                            </div>
                                                            <div class="form-group shipping-zip-code">
                                                                <div class="col-sm-3 hide"> <label for="elm_29" class="control-label cm-profile-field  cm-zipcode cm-location-shipping">Zip/postal code</label> </div>
                                                                <div class="col-sm-12"><input placeholder="Zip/postal code" x-autocompletetype="postal-code" type="text" id="elm_29" name="zip" value="<?php echo $Uzip; ?>" size="32" class="form-control disabled " disabled="disabled"
                                                                    /> </div>
                                                            </div>
                                                                 <button id="save_profile_but" class="btn btn-primary" type="submit" name="billingForm">Save</button> 
                                                        </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h1 class="profile-tab-header invite-tab-header highlight united-states-hidden profile-tab-header4">Invite &amp; Earn Points</h1>
                                                <div class="profile-tab profile-tab4 no-labels invite-tab united-states-hidden" style="display: none;">
                                                    <div class="refer-widget">
                                                        <h1>Never pay for a shirt again!</h1>
                                                        <p>
                                                            Earn up to ₹1000 in Points for every friend that signs up and makes a first purchase using your Unique Invite Link.<br> You'll get ₹250 off for every shirt (up to 4 shirts) on your friend's first
                                                            purchase. We'll also give your friend Rs. 250 off their first order!<br><br> Use one of the options below to send / share your invite. <br> </p> <a name="ref_link"></a>
                                                        <div class="ty-control-group profile-field-wrap">
                                                        <input type="text" id="elm_ref_link" readonly name="_fake" size="32" value="http://www.bombayshirts.com/profiles-add/?ref_code=1083007059" class="ty-input-text" /><a class="copy-paste"> <i class="fa fa-copy"></i> <span class="text">COPY</span> </a>
                                                            <div class="ref-link-social hide">
                                                                <div class="addthis_toolbox addthis_default_style addthis_32x32_style"> <a class="addthis_button_facebook" addthis:url="http://www.bombayshirts.com/profiles-add/?ref_code=1083007059" addthis:title="Bombay Shirt Company"></a> <a class="addthis_button_twitter"
                                                                        addthis:url="http://www.bombayshirts.com/profiles-add/?ref_code=1083007059" addthis:title="Bombay Shirt Company"></a> <a class="addthis_button_google_plusone_share" addthis:url="http://www.bombayshirts.com/profiles-add/?ref_code=1083007059"
                                                                        addthis:title="Bombay Shirt Company"></a> <a class="addthis_button_linkedin" addthis:url="http://www.bombayshirts.com/profiles-add/?ref_code=1083007059" addthis:title="Bombay Shirt Company"></a>                                                                    <a class="addthis_button_email" addthis:url="http://www.bombayshirts.com/profiles-add/?ref_code=1083007059" addthis:title="Bombay Shirt Company"></a> </div>
                                                            </div>
                                                            <style type="text/css">
                                                                iframe {
                                                                					width: 100%;
                                                                					height: 100%;
                                                                				}
                                                            </style>
                                                        </div>
                                                        <div class="social-sharing"> <span class="hidden-md hidden-lg share-text">Share link</span><a class="facebook" target="_blank" href="#"><i class="fa fa-facebook"></i><span class="text"> Share on Facebook</span></a>
                                                            <a
                                                                class="twitter hidden-sm hidden-xs" target="_blank" href="#"><i class="fa fa-twitter"></i><span class="text"> Share on Twitter</span></a><a class="email"><i class="fa fa-envelope"></i><span class="text"> Share over Email</span></a><a class="whatsapp hidden-md hidden-lg"
                                                                    target="_blank" href="#"><i class="fa fa-whatsapp"></i><span class="text"> Share over Whatsapp</span></a>
                                                                <div class="invite-form-wrapper hidden">
                                                                    <div class="invite-form" id="invite-form">
                                                                        <h1>Invite Over Email</h1>
                                                                        <p>Put in your friend's email and we'll take it from there.</p> <input type="hidden" name="ref_invite[user_remote_id]" class="remote-id">
                                                                        <div class="form-group"> <input placeholder="Friend's Name" type="text" name="ref_invite[full_name]"> </div>
                                                                        <div class="form-group"> <input placeholder="Friend's Email" type="text" name="ref_invite[email]"> </div>
                                                                        <div class="form-group no-margin"> <button class="btn-primary btn" type="submit">Send email!</button> </div>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="buttons-container panel panel-default">
                                                    <div class="panel-body"> <button id="save_profile_but" class="btn btn-primary" type="submit" name="update">Save</button> <input class="btn btn-default" type="reset" name="reset" value="Revert" id="shipping_address_reset"
                                                        /> </div>
                                                </div> <input type="hidden" name="security_hash" class="cm-no-hide-input" value="77ce89bceb41bd7be03f99b7a5d83005" /> -->

                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>


  <!--footer-->
 <?php get_footer(); ?> 
﻿<?php /* Template Name: loyalty-program */ ?>
<?php get_header(); ?>

                <section class="dispatch-pages-view loyalty-program-page content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="main-content-block">
                                    <div class="wysiwyg-content">
                                        <div>
                                            <div class="loyalty-points">
                                                <div class="three-split-wrapper">
                                                    <div class="introduction">
                                                        <h1 class="heading">Loyalty Points & Free Shirts</h1>
                                                        <hr class="heading-separator">
                                                        <h2 class="sub-heading">We value our customers and want to give you a reason to keep coming back to us! Loyalty Points can be used as cash on every order.<br><br>
														Use points for discounts on each purchase or save up towards free shirts. Points are awarded after your order has shipped. You can earn points through any of the following ways.</h2>
                                                        <div class="loyalty-point-breakdown hidden"> <span class="coin">1 LP</span><span class="coin borderless"> = </span><span class="coin">₹1</span> </div>
                                                        <div class="three-split row">
                                                            <div class="col-md-4 col-sm-4">
                                                                <div class="image-wrapper">
                                                                    <h1>Shop</h1> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/loyalty-points/shop.jpg'); ?>"> <span class="number">1</span> </div>
                                                                <p>Shop online, through our stores or travelling stylist. You will receive points based on the value of the shirts you have purchased.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4">
                                                                <div class="image-wrapper">
                                                                    <h1>Tag</h1> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/loyalty-points/tag.jpg'); ?>"> <span class="number">2</span> </div>
                                                                <p>
                                                                    Wear your NSC Shirt and tag yourself on Instagram using #noidashirts. Each week we choose one lucky winner who gets 2500 Loyalty Points.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4">
                                                                <div class="image-wrapper">
                                                                    <h1>Refer</h1> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/loyalty-points/refer.jpg'); ?>"> <span class="number">3</span> </div>
                                                                <p>
                                                                    Refer your friends and family. You can earn upto 1000 Loyalty Points for each friend that purchases from us. More details <a href="#">here</a>.
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="outro">
                                                            <h1 class="heading">How do I redeem the points?</h1>
                                                            <h2 class="sub-heading">When shopping online, you can redeem your Loyalty Points on the checkout page. If you’re shopping at one of our <a href="#">stores</a> just provide us your name, email and phone number whilst paying.<br><br>
															To start earning loyalty points or to check your existing balance, <a class="sign-in-trigger">Sign In/Register</a> your <a href="#">Profile</a>.
														</h2> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
<!--footer-->
    <?php get_footer(); ?>

                
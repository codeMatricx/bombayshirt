<?php /* Template Name: login */ ?>
<?php get_header(); ?>

                <section class="dispatch-auth-login_form content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="mainbox-container clearfix">
                                    <div class="page-header">
                                        <h1>
                                        Sign in
                                        </h1> </div>
                                    <div class="mainbox-body">
                                        <div>
                                            <form name="main_login_form" action="#" method="post" class="login-form">
                                                <input type="hidden" name="return_url" value="index.php?dispatch=auth.login_form" />
                                                <input type="hidden" name="redirect_url" value="index.php?dispatch=auth.login_form" />
                                                <div class="form-group">
                                                    <label for="login_main_login" class="cm-required cm-trim cm-email control-label">Email</label>
                                                    <input type="email" id="login_main_login" name="user_login" size="30" value="" class="cm-focus form-control" placeholder="Email" /> </div>
                                                <div class="form-group last clearfix">
                                                    <label for="psw_main_login" class="cm-required control-label">Password</label>
                                                    <input type="password" id="psw_main_login" name="password" size="30" value="" class="form-control" maxlength="32" placeholder="Password" /> <a href="index.php" class="forgot-password radio pull-left" tabindex="5">Forgot your password?</a>
                                                    <div class="checkbox pull-right remember-me">
                                                        <label for="remember_me_main_login">
                                                            <input type="checkbox" name="remember_me" id="remember_me_main_login" value="Y" />Remember me</label>
                                                    </div>
                                                </div> <span></span>
                                                <div class="buttons-container clearfix">
                                                    <div class="pull-right">
                                                        <button id="main_login" class="btn btn-primary" type="submit" name="dispatch[auth.login]">Sign in</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
<!--footer-->
   <?php get_footer(); ?> 
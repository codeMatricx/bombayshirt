﻿<?php /* Template Name: how-to-measure-shirt-size */ ?>
<?php get_header(); ?>

                <section class="dispatch-pages-view how-to-measure-shirt-size-page content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="main-content-block">
                                    <div class="wysiwyg-content">
                                        <div>
                                            <div class="sizing">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h1 class="heading">Sizing & Fit</h1>
                                                        <hr class="heading-separator">
                                                        <h2 class="sub-heading"><strong></strong>If you’ve shopped <strong>Online</strong>, at our <strong>Stores</strong> or through a <strong>Home Visit</strong>, your Profile has been created & your measurements stored for the next time you shop.<br> <br>
															To use these measurements, select <strong>'Your previously saved size'</strong> on checkout.</h2>
                                                        <h2 class="sub-heading"><strong>Fit Guarantee</strong><br>
															If you're not completely satisfied with your first trial shirt, we will alter your shirt or completely remake it free of cost.</h2> </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 sizing-option-set odd">
                                                        <h3 class="option-number">Option 1</h3>
                                                        <h2 class="option-title">FitSmart: Your Custom Size in 1 Minute<br></h2>
                                                        <p>We've collected and analysed countless data points to calculate the perfect fit. No measuring tapes needed!
                                                            <br>
                                                            <br> Select <strong>"Find My Size"</strong> during Checkout or in your <strong><a href="#">Profile</a></strong>.
                                                            <br>
                                                            <br> <em style="display:inline-block;max-width:850px;margin:0 auto;">If you've had your measurements taken by a tailor, these are usually <strong>body</strong> <strong>measurements</strong>. Please ensure that you add a breathing margin to all values to get your final size. We recommend 3-5" around the chest, waist, and hips; 2-3" around the biceps; and 2.5" for the cuffs. <span class="hidden-xs hidden-sm"></span></em>
                                                            <br>
                                                            <br> Use our <a href="#" target="_blank">How to Measure guide</a> to get the perfect fit.
                                                        </p>
                                                    </div>
                                                    <div class="col-md-12 sizing-option-set even">
                                                        <h3 class="option-number">Option 2</h3>
                                                        <h2 class="option-title">Standard Size: XS to XXXL</h2>
                                                        <p>Not sure about your custom size? Select from one of our wide range of standard sizes in two fits - Slim and Relaxed.
                                                            <br>
                                                            <br><!-- Select <strong>"Standard Size"</strong> during--> Checkout or in your <strong><a href="#">Profile</a></strong>.
                                                            <br>
                                                            <br> You can view our size chart <strong><a rel="shadowbox;width=1366;height=669;player=inline" href="#size-chart-desktop" class="size-chart-link">here</a></strong>.
                                                        </p>
                                                    </div>
                                                    <div class="col-md-12 sizing-option-set odd united-states-hidden">
                                                        <h3 class="option-number">Option 3</h3>
                                                        <h2 class="option-title">Send a Sample</h2>
                                                        <p>If you own a shirt that fits you well, then we'll use the measurements for your future orders.
                                                            <br>
                                                            <br> Print and ship your shirt using <strong><a href="#" target="_blank">this packaging slip</a></strong>.
                                                            <br>
                                                            <br> We'll <strong>copy the measurements</strong> and save them to your profile as your <strong>custom shirt size</strong>.
                                                            <br> Your shirt will be shipped back to you with your order.
                                                            <br>
                                                            <br> You can select <strong>"Send A Sample"</strong> during Checkout.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
                <!--footer-->
   <?php get_footer(); ?> 

<?php /* Template Name: Profile add */ ?>
<?php
get_header(); ?>
	<section class="dispatch-profiles-add content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-3 main-content-grid">
                                <div class="sub-menu-block">
                                    <div class="wysiwyg-content">
                                        <div class="profile-sidebar">
                                            <ul>
                                                <li><a href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=113" class="active">Profile Details</a></li>
                                                <li><a href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=73">Order Tracking</a></li>
                                                <li><a href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=117">Order History</a></li>
                                                <li><a href="http://staging.isiwal.com/SunnysBespoke1/?page_id=115">Loyalty Points History</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="col-lg-9 profile-information-grid">
                                <div class="mainbox-container clearfix">
                                    <div class="page-header">
                                        <h1>
                                        Register
                                        </h1> </div>
                                    <div class="mainbox-body">
                                        <div class="account">
                                            <form name="profiles_register_form" action="#" method="post">
                                                <input type="hidden" name="ship_to_another" value="1" />
                                                <div class="form-group profile-sizing-fit">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_89" class="control-label cm-profile-field  ">Sizing Fit</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <select id="elm_89" class="form-control " name="user_data[fields][89]">
                                                            <option value="">--</option>
                                                            <option value="40">Body</option>
                                                            <option value="41">Structured</option>
                                                            <option value="42">Comfort</option>
                                                            <option value="43">Fitted</option>
                                                            <option value="44">Loose</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group profile-gender">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_60" class="control-label cm-profile-field  ">Gender</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <select id="elm_60" class="form-control " name="user_data[fields][60]">
                                                            <option value="">--</option>
                                                            <option value="26">Male</option>
                                                            <option value="27">Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group profile-sizing-height">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_86" class="control-label cm-profile-field  ">Sizing Height</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Sizing Height" type="text" id="elm_86" name="user_data[fields][86]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-sizing-weight">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_87" class="control-label cm-profile-field  ">Sizing Weight</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Sizing Weight" type="text" id="elm_87" name="user_data[fields][87]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-sizing-shape">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_88" class="control-label cm-profile-field  ">Sizing Shape</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <select id="elm_88" class="form-control " name="user_data[fields][88]">
                                                            <option value="">--</option>
                                                            <option value="36">Skinny</option>
                                                            <option value="37">Athletic</option>
                                                            <option value="38">Average</option>
                                                            <option value="39">Healthy</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group profile-standard-size-male">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_59" class="control-label cm-profile-field  ">Standard Size Male</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <select id="elm_59" class="form-control " name="user_data[fields][59]">
                                                            <option value="">--</option>
                                                            <option value="19">XS (36)</option>
                                                            <option value="20">S (38)</option>
                                                            <option value="21">M (40)</option>
                                                            <option value="22">L (42)</option>
                                                            <option value="23">XL (44)</option>
                                                            <option value="24">XXL (46)</option>
                                                            <option value="25">XXXL (48)</option>
                                                            <option value="32">Custom</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group profile-standard-size-female">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_58" class="control-label cm-profile-field  ">Standard Size Female</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <select id="elm_58" class="form-control " name="user_data[fields][58]">
                                                            <option value="">--</option>
                                                            <option value="30">0 </option>
                                                            <option value="14">2</option>
                                                            <option value="15">4</option>
                                                            <option value="16">6</option>
                                                            <option value="17">8</option>
                                                            <option value="18">10</option>
                                                            <option value="31">Custom</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group profile-fit">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_61" class="control-label cm-profile-field  ">Fit</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <select id="elm_61" class="form-control " name="user_data[fields][61]">
                                                            <option value="">--</option>
                                                            <option value="28">Relaxed</option>
                                                            <option value="29">Slim</option>
                                                            <option value="33">Custom</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group profile-collar">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_62" class="control-label cm-profile-field  ">Collar</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Collar" type="text" id="elm_62" name="user_data[fields][62]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-chest">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_63" class="control-label cm-profile-field  ">Chest</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Chest" type="text" id="elm_63" name="user_data[fields][63]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-waist">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_64" class="control-label cm-profile-field  ">Waist</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Waist" type="text" id="elm_64" name="user_data[fields][64]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-shoulders">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_65" class="control-label cm-profile-field  ">Shoulders</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Shoulders" type="text" id="elm_65" name="user_data[fields][65]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-biceps">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_67" class="control-label cm-profile-field  ">Biceps</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Biceps" type="text" id="elm_67" name="user_data[fields][67]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-sleeve-length">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_68" class="control-label cm-profile-field  ">Sleeve Length</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Sleeve Length" type="text" id="elm_68" name="user_data[fields][68]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-cuffs">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_69" class="control-label cm-profile-field  ">Cuffs</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Cuffs" type="text" id="elm_69" name="user_data[fields][69]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-shirt-length">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_70" class="control-label cm-profile-field  ">Shirt Length</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Shirt Length" type="text" id="elm_70" name="user_data[fields][70]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-hips">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_71" class="control-label cm-profile-field  ">Hips</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Hips" type="text" id="elm_71" name="user_data[fields][71]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-armhole">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_72" class="control-label cm-profile-field  ">Armhole</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Armhole" type="text" id="elm_72" name="user_data[fields][72]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-back-cross">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_73" class="control-label cm-profile-field  ">Back Cross</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Back Cross" type="text" id="elm_73" name="user_data[fields][73]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-front-cross">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_74" class="control-label cm-profile-field  ">Front Cross</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Front Cross" type="text" id="elm_74" name="user_data[fields][74]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-short-sleeve-length">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_75" class="control-label cm-profile-field  ">Short Sleeve Length</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Short Sleeve Length" type="text" id="elm_75" name="user_data[fields][75]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-forearms">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_76" class="control-label cm-profile-field  ">Forearms</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Forearms" type="text" id="elm_76" name="user_data[fields][76]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-shoulder-yoke">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_77" class="control-label cm-profile-field  ">Shoulder Yoke</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Shoulder Yoke" type="text" id="elm_77" name="user_data[fields][77]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-comments">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_48" class="control-label cm-profile-field  ">Comments</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <textarea class="form-control " id="elm_48" name="user_data[fields][48]" cols="32" rows="3"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group profile-modified">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_49" class="control-label cm-profile-field  ">Modified</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Modified" type="text" id="elm_49" name="user_data[fields][49]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-sample-shirt">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_56" class="control-label cm-profile-field  ">Sample Shirt</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div id="elm_56">
                                                            <input class="radio  elm_56" type="radio" id="elm_56_11" name="user_data[fields][56]" value="11" checked="checked" /><span class="radio">No</span>
                                                            <input class="radio  elm_56" type="radio" id="elm_56_12" name="user_data[fields][56]" value="12" /><span class="radio">Yes</span> </div>
                                                    </div>
                                                </div>
                                                <div class="form-group profile-sample-shirt-brand">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_57" class="control-label cm-profile-field  ">Sample Shirt Brand</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input placeholder="Sample Shirt Brand" type="text" id="elm_57" name="user_data[fields][57]" size="32" value="" class="form-control  " /> </div>
                                                </div>
                                                <div class="form-group profile-sloping-shoulder">
                                                    <div class="col-sm-3 hide">
                                                        <label for="elm_90" class="control-label cm-profile-field  ">Sloping Shoulder</label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <select id="elm_90" class="form-control " name="user_data[fields][90]">
                                                            <option value="">--</option>
                                                            <option value="45">No</option>
                                                            <option value="46">Yes</option>
                                                            <option value="47">Don&#039;t Know</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="email" class="cm-required cm-email cm-trim control-label">Email</label>
                                                    <input type="text" id="email" name="user_data[email]" size="32" maxlength="128" value="" class="form-control cm-focus" placeholder="Email Address" /> </div>
                                                <div class="form-group">
                                                    <label for="password1" class="cm-required cm-password control-label">Password</label>
                                                    <input type="password" id="password1" name="user_data[password1]" size="32" maxlength="32" value="" class="form-control cm-autocomplete-off" placeholder="Password" /> </div>
                                                <div class="form-group">
                                                    <label for="password2" class="cm-required cm-password control-label">Confirm password</label>
                                                    <input type="password" id="password2" name="user_data[password2]" size="32" maxlength="32" value="" class="form-control cm-autocomplete-off" placeholder="Repeat Password" /> </div>
                                                <a name="ref_link"></a>
                                                <div class="buttons-container panel panel-default">
                                                    <div class="panel-body">
                                                        <button class="btn btn btn-primary" type="submit" name="dispatch[profiles.update]">Register</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>

		
		
<!--footer-->
   <?php get_footer(); ?>		
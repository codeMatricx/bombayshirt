﻿if ("undefined" == typeof jQuery) throw new Error("Bootstrap's JavaScript requires jQuery"); + function(t) {
    "use strict";
    var e = t.fn.jquery.split(" ")[0].split(".");
    if (e[0] < 2 && e[1] < 9 || 1 == e[0] && 9 == e[1] && e[2] < 1 || e[0] > 2) throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3")
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var i = t(this),
                n = i.data("bs.alert");
            n || i.data("bs.alert", n = new o(this)), "string" == typeof e && n[e].call(i)
        })
    }
    var i = '[data-dismiss="alert"]',
        o = function(e) {
            t(e).on("click", i, this.close)
        };
    o.VERSION = "3.3.6", o.TRANSITION_DURATION = 150, o.prototype.close = function(e) {
        function i() {
            a.detach().trigger("closed.bs.alert").remove()
        }
        var n = t(this),
            s = n.attr("data-target");
        s || (s = n.attr("href"), s = s && s.replace(/.*(?=#[^\s]*$)/, ""));
        var a = t(s);
        e && e.preventDefault(), a.length || (a = n.closest(".alert")), a.trigger(e = t.Event("close.bs.alert")), e.isDefaultPrevented() || (a.removeClass("in"), t.support.transition && a.hasClass("fade") ? a.one("bsTransitionEnd", i).emulateTransitionEnd(o.TRANSITION_DURATION) : i())
    };
    var n = t.fn.alert;
    t.fn.alert = e, t.fn.alert.Constructor = o, t.fn.alert.noConflict = function() {
        return t.fn.alert = n, this
    }, t(document).on("click.bs.alert.data-api", i, o.prototype.close)
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var o = t(this),
                n = o.data("bs.button"),
                s = "object" == typeof e && e;
            n || o.data("bs.button", n = new i(this, s)), "toggle" == e ? n.toggle() : e && n.setState(e)
        })
    }
    var i = function(e, o) {
        this.$element = t(e), this.options = t.extend({}, i.DEFAULTS, o), this.isLoading = !1
    };
    i.VERSION = "3.3.6", i.DEFAULTS = {
        loadingText: "loading..."
    }, i.prototype.setState = function(e) {
        var i = "disabled",
            o = this.$element,
            n = o.is("input") ? "val" : "html",
            s = o.data();
        e += "Text", null == s.resetText && o.data("resetText", o[n]()), setTimeout(t.proxy(function() {
            o[n](null == s[e] ? this.options[e] : s[e]), "loadingText" == e ? (this.isLoading = !0, o.addClass(i).attr(i, i)) : this.isLoading && (this.isLoading = !1, o.removeClass(i).removeAttr(i))
        }, this), 0)
    }, i.prototype.toggle = function() {
        var t = !0,
            e = this.$element.closest('[data-toggle="buttons"]');
        if (e.length) {
            var i = this.$element.find("input");
            "radio" == i.prop("type") ? (i.prop("checked") && (t = !1), e.find(".active").removeClass("active"), this.$element.addClass("active")) : "checkbox" == i.prop("type") && (i.prop("checked") !== this.$element.hasClass("active") && (t = !1), this.$element.toggleClass("active")), i.prop("checked", this.$element.hasClass("active")), t && i.trigger("change")
        } else this.$element.attr("aria-pressed", !this.$element.hasClass("active")), this.$element.toggleClass("active")
    };
    var o = t.fn.button;
    t.fn.button = e, t.fn.button.Constructor = i, t.fn.button.noConflict = function() {
        return t.fn.button = o, this
    }, t(document).on("click.bs.button.data-api", '[data-toggle^="button"]', function(i) {
        var o = t(i.target);
        o.hasClass("btn") || (o = o.closest(".btn")), e.call(o, "toggle"), t(i.target).is('input[type="radio"]') || t(i.target).is('input[type="checkbox"]') || i.preventDefault()
    }).on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function(e) {
        t(e.target).closest(".btn").toggleClass("focus", /^focus(in)?$/.test(e.type))
    })
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var o = t(this),
                n = o.data("bs.carousel"),
                s = t.extend({}, i.DEFAULTS, o.data(), "object" == typeof e && e),
                a = "string" == typeof e ? e : s.slide;
            n || o.data("bs.carousel", n = new i(this, s)), "number" == typeof e ? n.to(e) : a ? n[a]() : s.interval && n.pause().cycle()
        })
    }
    var i = function(e, i) {
        this.$element = t(e), this.$indicators = this.$element.find(".carousel-indicators"), this.options = i, this.paused = null, this.sliding = null, this.interval = null, this.$active = null, this.$items = null, this.options.keyboard && this.$element.on("keydown.bs.carousel", t.proxy(this.keydown, this)), "hover" == this.options.pause && !("ontouchstart" in document.documentElement) && this.$element.on("mouseenter.bs.carousel", t.proxy(this.pause, this)).on("mouseleave.bs.carousel", t.proxy(this.cycle, this))
    };
    i.VERSION = "3.3.6", i.TRANSITION_DURATION = 600, i.DEFAULTS = {
        interval: 5e3,
        pause: "hover",
        wrap: !0,
        keyboard: !0
    }, i.prototype.keydown = function(t) {
        if (!/input|textarea/i.test(t.target.tagName)) {
            switch (t.which) {
                case 37:
                    this.prev();
                    break;
                case 39:
                    this.next();
                    break;
                default:
                    return
            }
            t.preventDefault()
        }
    }, i.prototype.cycle = function(e) {
        return e || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(t.proxy(this.next, this), this.options.interval)), this
    }, i.prototype.getItemIndex = function(t) {
        return this.$items = t.parent().children(".item"), this.$items.index(t || this.$active)
    }, i.prototype.getItemForDirection = function(t, e) {
        var i = this.getItemIndex(e),
            o = "prev" == t && 0 === i || "next" == t && i == this.$items.length - 1;
        if (o && !this.options.wrap) return e;
        var n = "prev" == t ? -1 : 1,
            s = (i + n) % this.$items.length;
        return this.$items.eq(s)
    }, i.prototype.to = function(t) {
        var e = this,
            i = this.getItemIndex(this.$active = this.$element.find(".item.active"));
        return t > this.$items.length - 1 || 0 > t ? void 0 : this.sliding ? this.$element.one("slid.bs.carousel", function() {
            e.to(t)
        }) : i == t ? this.pause().cycle() : this.slide(t > i ? "next" : "prev", this.$items.eq(t))
    }, i.prototype.pause = function(e) {
        return e || (this.paused = !0), this.$element.find(".next, .prev").length && t.support.transition && (this.$element.trigger(t.support.transition.end), this.cycle(!0)), this.interval = clearInterval(this.interval), this
    }, i.prototype.next = function() {
        return this.sliding ? void 0 : this.slide("next")
    }, i.prototype.prev = function() {
        return this.sliding ? void 0 : this.slide("prev")
    }, i.prototype.slide = function(e, o) {
        var n = this.$element.find(".item.active"),
            s = o || this.getItemForDirection(e, n),
            a = this.interval,
            r = "next" == e ? "left" : "right",
            l = this;
        if (s.hasClass("active")) return this.sliding = !1;
        var h = s[0],
            d = t.Event("slide.bs.carousel", {
                relatedTarget: h,
                direction: r
            });
        if (this.$element.trigger(d), !d.isDefaultPrevented()) {
            if (this.sliding = !0, a && this.pause(), this.$indicators.length) {
                this.$indicators.find(".active").removeClass("active");
                var p = t(this.$indicators.children()[this.getItemIndex(s)]);
                p && p.addClass("active")
            }
            var c = t.Event("slid.bs.carousel", {
                relatedTarget: h,
                direction: r
            });
            return t.support.transition && this.$element.hasClass("slide") ? (s.addClass(e), s[0].offsetWidth, n.addClass(r), s.addClass(r), n.one("bsTransitionEnd", function() {
                s.removeClass([e, r].join(" ")).addClass("active"), n.removeClass(["active", r].join(" ")), l.sliding = !1, setTimeout(function() {
                    l.$element.trigger(c)
                }, 0)
            }).emulateTransitionEnd(i.TRANSITION_DURATION)) : (n.removeClass("active"), s.addClass("active"), this.sliding = !1, this.$element.trigger(c)), a && this.cycle(), this
        }
    };
    var o = t.fn.carousel;
    t.fn.carousel = e, t.fn.carousel.Constructor = i, t.fn.carousel.noConflict = function() {
        return t.fn.carousel = o, this
    };
    var n = function(i) {
        var o, n = t(this),
            s = t(n.attr("data-target") || (o = n.attr("href")) && o.replace(/.*(?=#[^\s]+$)/, ""));
        if (s.hasClass("carousel")) {
            var a = t.extend({}, s.data(), n.data()),
                r = n.attr("data-slide-to");
            r && (a.interval = !1), e.call(s, a), r && s.data("bs.carousel").to(r), i.preventDefault()
        }
    };
    t(document).on("click.bs.carousel.data-api", "[data-slide]", n).on("click.bs.carousel.data-api", "[data-slide-to]", n), t(window).on("load", function() {
        t('[data-ride="carousel"]').each(function() {
            var i = t(this);
            e.call(i, i.data())
        })
    })
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        var i = e.attr("data-target");
        i || (i = e.attr("href"), i = i && /#[A-Za-z]/.test(i) && i.replace(/.*(?=#[^\s]*$)/, ""));
        var o = i && t(i);
        return o && o.length ? o : e.parent()
    }

    function i(i) {
        i && 3 === i.which || (t(n).remove(), t(s).each(function() {
            var o = t(this),
                n = e(o),
                s = {
                    relatedTarget: this
                };
            n.hasClass("open") && (i && "click" == i.type && /input|textarea/i.test(i.target.tagName) && t.contains(n[0], i.target) || (n.trigger(i = t.Event("hide.bs.dropdown", s)), i.isDefaultPrevented() || (o.attr("aria-expanded", "false"), n.removeClass("open").trigger(t.Event("hidden.bs.dropdown", s)))))
        }))
    }

    function o(e) {
        return this.each(function() {
            var i = t(this),
                o = i.data("bs.dropdown");
            o || i.data("bs.dropdown", o = new a(this)), "string" == typeof e && o[e].call(i)
        })
    }
    var n = ".dropdown-backdrop",
        s = '[data-toggle="dropdown"]',
        a = function(e) {
            t(e).on("click.bs.dropdown", this.toggle)
        };
    a.VERSION = "3.3.6", a.prototype.toggle = function(o) {
        var n = t(this);
        if (!n.is(".disabled, :disabled")) {
            var s = e(n),
                a = s.hasClass("open");
            if (i(), !a) {
                "ontouchstart" in document.documentElement && !s.closest(".navbar-nav").length && t(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(t(this)).on("click", i);
                var r = {
                    relatedTarget: this
                };
                if (s.trigger(o = t.Event("show.bs.dropdown", r)), o.isDefaultPrevented()) return;
                n.trigger("focus").attr("aria-expanded", "true"), s.toggleClass("open").trigger(t.Event("shown.bs.dropdown", r))
            }
            return !1
        }
    }, a.prototype.keydown = function(i) {
        if (/(38|40|27|32)/.test(i.which) && !/input|textarea/i.test(i.target.tagName)) {
            var o = t(this);
            if (i.preventDefault(), i.stopPropagation(), !o.is(".disabled, :disabled")) {
                var n = e(o),
                    a = n.hasClass("open");
                if (!a && 27 != i.which || a && 27 == i.which) return 27 == i.which && n.find(s).trigger("focus"), o.trigger("click");
                var r = " li:not(.disabled):visible a",
                    l = n.find(".dropdown-menu" + r);
                if (l.length) {
                    var h = l.index(i.target);
                    38 == i.which && h > 0 && h--, 40 == i.which && h < l.length - 1 && h++, ~h || (h = 0), l.eq(h).trigger("focus")
                }
            }
        }
    };
    var r = t.fn.dropdown;
    t.fn.dropdown = o, t.fn.dropdown.Constructor = a, t.fn.dropdown.noConflict = function() {
        return t.fn.dropdown = r, this
    }, t(document).on("click.bs.dropdown.data-api", i).on("click.bs.dropdown.data-api", ".dropdown form", function(t) {
        t.stopPropagation()
    }).on("click.bs.dropdown.data-api", s, a.prototype.toggle).on("keydown.bs.dropdown.data-api", s, a.prototype.keydown).on("keydown.bs.dropdown.data-api", ".dropdown-menu", a.prototype.keydown)
}(jQuery), + function(t) {
    "use strict";

    function e(e, o) {
        return this.each(function() {
            var n = t(this),
                s = n.data("bs.modal"),
                a = t.extend({}, i.DEFAULTS, n.data(), "object" == typeof e && e);
            s || n.data("bs.modal", s = new i(this, a)), "string" == typeof e ? s[e](o) : a.show && s.show(o)
        })
    }
    var i = function(e, i) {
        this.options = i, this.$body = t(document.body), this.$element = t(e), this.$dialog = this.$element.find(".modal-dialog"), this.$backdrop = null, this.isShown = null, this.originalBodyPad = null, this.scrollbarWidth = 0, this.ignoreBackdropClick = !1, this.options.remote && this.$element.find(".modal-content").load(this.options.remote, t.proxy(function() {
            this.$element.trigger("loaded.bs.modal")
        }, this))
    };
    i.VERSION = "3.3.6", i.TRANSITION_DURATION = 300, i.BACKDROP_TRANSITION_DURATION = 150, i.DEFAULTS = {
        backdrop: !0,
        keyboard: !0,
        show: !0
    }, i.prototype.toggle = function(t) {
        return this.isShown ? this.hide() : this.show(t)
    }, i.prototype.show = function(e) {
        var o = this,
            n = t.Event("show.bs.modal", {
                relatedTarget: e
            });
        this.$element.trigger(n), this.isShown || n.isDefaultPrevented() || (this.isShown = !0, this.checkScrollbar(), this.setScrollbar(), this.$body.addClass("modal-open"), this.escape(), this.resize(), this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', t.proxy(this.hide, this)), this.$dialog.on("mousedown.dismiss.bs.modal", function() {
            o.$element.one("mouseup.dismiss.bs.modal", function(e) {
                t(e.target).is(o.$element) && (o.ignoreBackdropClick = !0)
            })
        }), this.backdrop(function() {
            var n = t.support.transition && o.$element.hasClass("fade");
            o.$element.parent().length || o.$element.appendTo(o.$body), o.$element.show().scrollTop(0), o.adjustDialog(), n && o.$element[0].offsetWidth, o.$element.addClass("in"), o.enforceFocus();
            var s = t.Event("shown.bs.modal", {
                relatedTarget: e
            });
            n ? o.$dialog.one("bsTransitionEnd", function() {
                o.$element.trigger("focus").trigger(s)
            }).emulateTransitionEnd(i.TRANSITION_DURATION) : o.$element.trigger("focus").trigger(s)
        }))
    }, i.prototype.hide = function(e) {
        e && e.preventDefault(), e = t.Event("hide.bs.modal"), this.$element.trigger(e), this.isShown && !e.isDefaultPrevented() && (this.isShown = !1, this.escape(), this.resize(), t(document).off("focusin.bs.modal"), this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"), this.$dialog.off("mousedown.dismiss.bs.modal"), t.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", t.proxy(this.hideModal, this)).emulateTransitionEnd(i.TRANSITION_DURATION) : this.hideModal())
    }, i.prototype.enforceFocus = function() {
        t(document).off("focusin.bs.modal").on("focusin.bs.modal", t.proxy(function(t) {
            this.$element[0] === t.target || this.$element.has(t.target).length || this.$element.trigger("focus")
        }, this))
    }, i.prototype.escape = function() {
        this.isShown && this.options.keyboard ? this.$element.on("keydown.dismiss.bs.modal", t.proxy(function(t) {
            27 == t.which && this.hide()
        }, this)) : this.isShown || this.$element.off("keydown.dismiss.bs.modal")
    }, i.prototype.resize = function() {
        this.isShown ? t(window).on("resize.bs.modal", t.proxy(this.handleUpdate, this)) : t(window).off("resize.bs.modal")
    }, i.prototype.hideModal = function() {
        var t = this;
        this.$element.hide(), this.backdrop(function() {
            t.$body.removeClass("modal-open"), t.resetAdjustments(), t.resetScrollbar(), t.$element.trigger("hidden.bs.modal")
        })
    }, i.prototype.removeBackdrop = function() {
        this.$backdrop && this.$backdrop.remove(), this.$backdrop = null
    }, i.prototype.backdrop = function(e) {
        var o = this,
            n = this.$element.hasClass("fade") ? "fade" : "";
        if (this.isShown && this.options.backdrop) {
            var s = t.support.transition && n;
            if (this.$backdrop = t(document.createElement("div")).addClass("modal-backdrop " + n).appendTo(this.$body), this.$element.on("click.dismiss.bs.modal", t.proxy(function(t) {
                    return this.ignoreBackdropClick ? void(this.ignoreBackdropClick = !1) : void(t.target === t.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus() : this.hide()))
                }, this)), s && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !e) return;
            s ? this.$backdrop.one("bsTransitionEnd", e).emulateTransitionEnd(i.BACKDROP_TRANSITION_DURATION) : e()
        } else if (!this.isShown && this.$backdrop) {
            this.$backdrop.removeClass("in");
            var a = function() {
                o.removeBackdrop(), e && e()
            };
            t.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", a).emulateTransitionEnd(i.BACKDROP_TRANSITION_DURATION) : a()
        } else e && e()
    }, i.prototype.handleUpdate = function() {
        this.adjustDialog()
    }, i.prototype.adjustDialog = function() {
        var t = this.$element[0].scrollHeight > document.documentElement.clientHeight;
        this.$element.css({
            paddingLeft: !this.bodyIsOverflowing && t ? this.scrollbarWidth : "",
            paddingRight: this.bodyIsOverflowing && !t ? this.scrollbarWidth : ""
        })
    }, i.prototype.resetAdjustments = function() {
        this.$element.css({
            paddingLeft: "",
            paddingRight: ""
        })
    }, i.prototype.checkScrollbar = function() {
        var t = window.innerWidth;
        if (!t) {
            var e = document.documentElement.getBoundingClientRect();
            t = e.right - Math.abs(e.left)
        }
        this.bodyIsOverflowing = document.body.clientWidth < t, this.scrollbarWidth = this.measureScrollbar()
    }, i.prototype.setScrollbar = function() {
        var t = parseInt(this.$body.css("padding-right") || 0, 10);
        this.originalBodyPad = document.body.style.paddingRight || "", this.bodyIsOverflowing && this.$body.css("padding-right", t + this.scrollbarWidth)
    }, i.prototype.resetScrollbar = function() {
        this.$body.css("padding-right", this.originalBodyPad)
    }, i.prototype.measureScrollbar = function() {
        var t = document.createElement("div");
        t.className = "modal-scrollbar-measure", this.$body.append(t);
        var e = t.offsetWidth - t.clientWidth;
        return this.$body[0].removeChild(t), e
    };
    var o = t.fn.modal;
    t.fn.modal = e, t.fn.modal.Constructor = i, t.fn.modal.noConflict = function() {
        return t.fn.modal = o, this
    }, t(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function(i) {
        var o = t(this),
            n = o.attr("href"),
            s = t(o.attr("data-target") || n && n.replace(/.*(?=#[^\s]+$)/, "")),
            a = s.data("bs.modal") ? "toggle" : t.extend({
                remote: !/#/.test(n) && n
            }, s.data(), o.data());
        o.is("a") && i.preventDefault(), s.one("show.bs.modal", function(t) {
            t.isDefaultPrevented() || s.one("hidden.bs.modal", function() {
                o.is(":visible") && o.trigger("focus")
            })
        }), e.call(s, a, this)
    })
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var o = t(this),
                n = o.data("bs.tooltip"),
                s = "object" == typeof e && e;
            (n || !/destroy|hide/.test(e)) && (n || o.data("bs.tooltip", n = new i(this, s)), "string" == typeof e && n[e]())
        })
    }
    var i = function(t, e) {
        this.type = null, this.options = null, this.enabled = null, this.timeout = null, this.hoverState = null, this.$element = null, this.inState = null, this.init("tooltip", t, e)
    };
    i.VERSION = "3.3.6", i.TRANSITION_DURATION = 150, i.DEFAULTS = {
        animation: !0,
        placement: "top",
        selector: !1,
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        container: !1,
        viewport: {
            selector: "body",
            padding: 0
        }
    }, i.prototype.init = function(e, i, o) {
        if (this.enabled = !0, this.type = e, this.$element = t(i), this.options = this.getOptions(o), this.$viewport = this.options.viewport && t(t.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : this.options.viewport.selector || this.options.viewport), this.inState = {
                click: !1,
                hover: !1,
                focus: !1
            }, this.$element[0] instanceof document.constructor && !this.options.selector) throw new Error("`selector` option must be specified when initializing " + this.type + " on the window.document object!");
        for (var n = this.options.trigger.split(" "), s = n.length; s--;) {
            var a = n[s];
            if ("click" == a) this.$element.on("click." + this.type, this.options.selector, t.proxy(this.toggle, this));
            else if ("manual" != a) {
                var r = "hover" == a ? "mouseenter" : "focusin",
                    l = "hover" == a ? "mouseleave" : "focusout";
                this.$element.on(r + "." + this.type, this.options.selector, t.proxy(this.enter, this)), this.$element.on(l + "." + this.type, this.options.selector, t.proxy(this.leave, this))
            }
        }
        this.options.selector ? this._options = t.extend({}, this.options, {
            trigger: "manual",
            selector: ""
        }) : this.fixTitle()
    }, i.prototype.getDefaults = function() {
        return i.DEFAULTS
    }, i.prototype.getOptions = function(e) {
        return e = t.extend({}, this.getDefaults(), this.$element.data(), e), e.delay && "number" == typeof e.delay && (e.delay = {
            show: e.delay,
            hide: e.delay
        }), e
    }, i.prototype.getDelegateOptions = function() {
        var e = {},
            i = this.getDefaults();
        return this._options && t.each(this._options, function(t, o) {
            i[t] != o && (e[t] = o)
        }), e
    }, i.prototype.enter = function(e) {
        var i = e instanceof this.constructor ? e : t(e.currentTarget).data("bs." + this.type);
        return i || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, i)), e instanceof t.Event && (i.inState["focusin" == e.type ? "focus" : "hover"] = !0), i.tip().hasClass("in") || "in" == i.hoverState ? void(i.hoverState = "in") : (clearTimeout(i.timeout), i.hoverState = "in", i.options.delay && i.options.delay.show ? void(i.timeout = setTimeout(function() {
            "in" == i.hoverState && i.show()
        }, i.options.delay.show)) : i.show())
    }, i.prototype.isInStateTrue = function() {
        for (var t in this.inState)
            if (this.inState[t]) return !0;
        return !1
    }, i.prototype.leave = function(e) {
        var i = e instanceof this.constructor ? e : t(e.currentTarget).data("bs." + this.type);
        return i || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, i)), e instanceof t.Event && (i.inState["focusout" == e.type ? "focus" : "hover"] = !1), i.isInStateTrue() ? void 0 : (clearTimeout(i.timeout), i.hoverState = "out", i.options.delay && i.options.delay.hide ? void(i.timeout = setTimeout(function() {
            "out" == i.hoverState && i.hide()
        }, i.options.delay.hide)) : i.hide())
    }, i.prototype.show = function() {
        var e = t.Event("show.bs." + this.type);
        if (this.hasContent() && this.enabled) {
            this.$element.trigger(e);
            var o = t.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);
            if (e.isDefaultPrevented() || !o) return;
            var n = this,
                s = this.tip(),
                a = this.getUID(this.type);
            this.setContent(), s.attr("id", a), this.$element.attr("aria-describedby", a), this.options.animation && s.addClass("fade");
            var r = "function" == typeof this.options.placement ? this.options.placement.call(this, s[0], this.$element[0]) : this.options.placement,
                l = /\s?auto?\s?/i,
                h = l.test(r);
            h && (r = r.replace(l, "") || "top"), s.detach().css({
                top: 0,
                left: 0,
                display: "block"
            }).addClass(r).data("bs." + this.type, this), this.options.container ? s.appendTo(this.options.container) : s.insertAfter(this.$element), this.$element.trigger("inserted.bs." + this.type);
            var d = this.getPosition(),
                p = s[0].offsetWidth,
                c = s[0].offsetHeight;
            if (h) {
                var f = r,
                    u = this.getPosition(this.$viewport);
                r = "bottom" == r && d.bottom + c > u.bottom ? "top" : "top" == r && d.top - c < u.top ? "bottom" : "right" == r && d.right + p > u.width ? "left" : "left" == r && d.left - p < u.left ? "right" : r, s.removeClass(f).addClass(r)
            }
            var g = this.getCalculatedOffset(r, d, p, c);
            this.applyPlacement(g, r);
            var v = function() {
                var t = n.hoverState;
                n.$element.trigger("shown.bs." + n.type), n.hoverState = null, "out" == t && n.leave(n)
            };
            t.support.transition && this.$tip.hasClass("fade") ? s.one("bsTransitionEnd", v).emulateTransitionEnd(i.TRANSITION_DURATION) : v()
        }
    }, i.prototype.applyPlacement = function(e, i) {
        var o = this.tip(),
            n = o[0].offsetWidth,
            s = o[0].offsetHeight,
            a = parseInt(o.css("margin-top"), 10),
            r = parseInt(o.css("margin-left"), 10);
        isNaN(a) && (a = 0), isNaN(r) && (r = 0), e.top += a, e.left += r, t.offset.setOffset(o[0], t.extend({
            using: function(t) {
                o.css({
                    top: Math.round(t.top),
                    left: Math.round(t.left)
                })
            }
        }, e), 0), o.addClass("in");
        var l = o[0].offsetWidth,
            h = o[0].offsetHeight;
        "top" == i && h != s && (e.top = e.top + s - h);
        var d = this.getViewportAdjustedDelta(i, e, l, h);
        d.left ? e.left += d.left : e.top += d.top;
        var p = /top|bottom/.test(i),
            c = p ? 2 * d.left - n + l : 2 * d.top - s + h,
            f = p ? "offsetWidth" : "offsetHeight";
        o.offset(e), this.replaceArrow(c, o[0][f], p)
    }, i.prototype.replaceArrow = function(t, e, i) {
        this.arrow().css(i ? "left" : "top", 50 * (1 - t / e) + "%").css(i ? "top" : "left", "")
    }, i.prototype.setContent = function() {
        var t = this.tip(),
            e = this.getTitle();
        t.find(".tooltip-inner")[this.options.html ? "html" : "text"](e), t.removeClass("fade in top bottom left right")
    }, i.prototype.hide = function(e) {
        function o() {
            "in" != n.hoverState && s.detach(), n.$element.removeAttr("aria-describedby").trigger("hidden.bs." + n.type), e && e()
        }
        var n = this,
            s = t(this.$tip),
            a = t.Event("hide.bs." + this.type);
        return this.$element.trigger(a), a.isDefaultPrevented() ? void 0 : (s.removeClass("in"), t.support.transition && s.hasClass("fade") ? s.one("bsTransitionEnd", o).emulateTransitionEnd(i.TRANSITION_DURATION) : o(), this.hoverState = null, this)
    }, i.prototype.fixTitle = function() {
        var t = this.$element;
        (t.attr("title") || "string" != typeof t.attr("data-original-title")) && t.attr("data-original-title", t.attr("title") || "").attr("title", "")
    }, i.prototype.hasContent = function() {
        return this.getTitle()
    }, i.prototype.getPosition = function(e) {
        e = e || this.$element;
        var i = e[0],
            o = "BODY" == i.tagName,
            n = i.getBoundingClientRect();
        null == n.width && (n = t.extend({}, n, {
            width: n.right - n.left,
            height: n.bottom - n.top
        }));
        var s = o ? {
                top: 0,
                left: 0
            } : e.offset(),
            a = {
                scroll: o ? document.documentElement.scrollTop || document.body.scrollTop : e.scrollTop()
            },
            r = o ? {
                width: t(window).width(),
                height: t(window).height()
            } : null;
        return t.extend({}, n, a, r, s)
    }, i.prototype.getCalculatedOffset = function(t, e, i, o) {
        return "bottom" == t ? {
            top: e.top + e.height,
            left: e.left + e.width / 2 - i / 2
        } : "top" == t ? {
            top: e.top - o,
            left: e.left + e.width / 2 - i / 2
        } : "left" == t ? {
            top: e.top + e.height / 2 - o / 2,
            left: e.left - i
        } : {
            top: e.top + e.height / 2 - o / 2,
            left: e.left + e.width
        }
    }, i.prototype.getViewportAdjustedDelta = function(t, e, i, o) {
        var n = {
            top: 0,
            left: 0
        };
        if (!this.$viewport) return n;
        var s = this.options.viewport && this.options.viewport.padding || 0,
            a = this.getPosition(this.$viewport);
        if (/right|left/.test(t)) {
            var r = e.top - s - a.scroll,
                l = e.top + s - a.scroll + o;
            r < a.top ? n.top = a.top - r : l > a.top + a.height && (n.top = a.top + a.height - l)
        } else {
            var h = e.left - s,
                d = e.left + s + i;
            h < a.left ? n.left = a.left - h : d > a.right && (n.left = a.left + a.width - d)
        }
        return n
    }, i.prototype.getTitle = function() {
        var t, e = this.$element,
            i = this.options;
        return t = e.attr("data-original-title") || ("function" == typeof i.title ? i.title.call(e[0]) : i.title)
    }, i.prototype.getUID = function(t) {
        do t += ~~(1e6 * Math.random()); while (document.getElementById(t));
        return t
    }, i.prototype.tip = function() {
        if (!this.$tip && (this.$tip = t(this.options.template), 1 != this.$tip.length)) throw new Error(this.type + " `template` option must consist of exactly 1 top-level element!");
        return this.$tip
    }, i.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }, i.prototype.enable = function() {
        this.enabled = !0
    }, i.prototype.disable = function() {
        this.enabled = !1
    }, i.prototype.toggleEnabled = function() {
        this.enabled = !this.enabled
    }, i.prototype.toggle = function(e) {
        var i = this;
        e && (i = t(e.currentTarget).data("bs." + this.type), i || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, i))), e ? (i.inState.click = !i.inState.click, i.isInStateTrue() ? i.enter(i) : i.leave(i)) : i.tip().hasClass("in") ? i.leave(i) : i.enter(i)
    }, i.prototype.destroy = function() {
        var t = this;
        clearTimeout(this.timeout), this.hide(function() {
            t.$element.off("." + t.type).removeData("bs." + t.type), t.$tip && t.$tip.detach(), t.$tip = null, t.$arrow = null, t.$viewport = null
        })
    };
    var o = t.fn.tooltip;
    t.fn.tooltip = e, t.fn.tooltip.Constructor = i, t.fn.tooltip.noConflict = function() {
        return t.fn.tooltip = o, this
    }
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var o = t(this),
                n = o.data("bs.popover"),
                s = "object" == typeof e && e;
            (n || !/destroy|hide/.test(e)) && (n || o.data("bs.popover", n = new i(this, s)), "string" == typeof e && n[e]())
        })
    }
    var i = function(t, e) {
        this.init("popover", t, e)
    };
    if (!t.fn.tooltip) throw new Error("Popover requires tooltip.js");
    i.VERSION = "3.3.6", i.DEFAULTS = t.extend({}, t.fn.tooltip.Constructor.DEFAULTS, {
        placement: "right",
        trigger: "click",
        content: "",
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    }), i.prototype = t.extend({}, t.fn.tooltip.Constructor.prototype), i.prototype.constructor = i, i.prototype.getDefaults = function() {
        return i.DEFAULTS
    }, i.prototype.setContent = function() {
        var t = this.tip(),
            e = this.getTitle(),
            i = this.getContent();
        t.find(".popover-title")[this.options.html ? "html" : "text"](e), t.find(".popover-content").children().detach().end()[this.options.html ? "string" == typeof i ? "html" : "append" : "text"](i), t.removeClass("fade top bottom left right in"), t.find(".popover-title").html() || t.find(".popover-title").hide()
    }, i.prototype.hasContent = function() {
        return this.getTitle() || this.getContent()
    }, i.prototype.getContent = function() {
        var t = this.$element,
            e = this.options;
        return t.attr("data-content") || ("function" == typeof e.content ? e.content.call(t[0]) : e.content)
    }, i.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".arrow")
    };
    var o = t.fn.popover;
    t.fn.popover = e, t.fn.popover.Constructor = i, t.fn.popover.noConflict = function() {
        return t.fn.popover = o, this
    }
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var o = t(this),
                n = o.data("bs.tab");
            n || o.data("bs.tab", n = new i(this)), "string" == typeof e && n[e]()
        })
    }
    var i = function(e) {
        this.element = t(e)
    };
    i.VERSION = "3.3.6", i.TRANSITION_DURATION = 150, i.prototype.show = function() {
        var e = this.element,
            i = e.closest("ul:not(.dropdown-menu)"),
            o = e.data("target");
        if (o || (o = e.attr("href"), o = o && o.replace(/.*(?=#[^\s]*$)/, "")), !e.parent("li").hasClass("active")) {
            var n = i.find(".active:last a"),
                s = t.Event("hide.bs.tab", {
                    relatedTarget: e[0]
                }),
                a = t.Event("show.bs.tab", {
                    relatedTarget: n[0]
                });
            if (n.trigger(s), e.trigger(a), !a.isDefaultPrevented() && !s.isDefaultPrevented()) {
                var r = t(o);
                this.activate(e.closest("li"), i), this.activate(r, r.parent(), function() {
                    n.trigger({
                        type: "hidden.bs.tab",
                        relatedTarget: e[0]
                    }), e.trigger({
                        type: "shown.bs.tab",
                        relatedTarget: n[0]
                    })
                })
            }
        }
    }, i.prototype.activate = function(e, o, n) {
        function s() {
            a.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !1), e.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded", !0), r ? (e[0].offsetWidth, e.addClass("in")) : e.removeClass("fade"), e.parent(".dropdown-menu").length && e.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !0), n && n()
        }
        var a = o.find("> .active"),
            r = n && t.support.transition && (a.length && a.hasClass("fade") || !!o.find("> .fade").length);
        a.length && r ? a.one("bsTransitionEnd", s).emulateTransitionEnd(i.TRANSITION_DURATION) : s(), a.removeClass("in")
    };
    var o = t.fn.tab;
    t.fn.tab = e, t.fn.tab.Constructor = i, t.fn.tab.noConflict = function() {
        return t.fn.tab = o, this
    };
    var n = function(i) {
        i.preventDefault(), e.call(t(this), "show")
    };
    t(document).on("click.bs.tab.data-api", '[data-toggle="tab"]', n).on("click.bs.tab.data-api", '[data-toggle="pill"]', n)
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var o = t(this),
                n = o.data("bs.affix"),
                s = "object" == typeof e && e;
            n || o.data("bs.affix", n = new i(this, s)), "string" == typeof e && n[e]()
        })
    }
    var i = function(e, o) {
        this.options = t.extend({}, i.DEFAULTS, o), this.$target = t(this.options.target).on("scroll.bs.affix.data-api", t.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", t.proxy(this.checkPositionWithEventLoop, this)), this.$element = t(e), this.affixed = null, this.unpin = null, this.pinnedOffset = null, this.checkPosition()
    };
    i.VERSION = "3.3.6", i.RESET = "affix affix-top affix-bottom", i.DEFAULTS = {
        offset: 0,
        target: window
    }, i.prototype.getState = function(t, e, i, o) {
        var n = this.$target.scrollTop(),
            s = this.$element.offset(),
            a = this.$target.height();
        if (null != i && "top" == this.affixed) return i > n ? "top" : !1;
        if ("bottom" == this.affixed) return null != i ? n + this.unpin <= s.top ? !1 : "bottom" : t - o >= n + a ? !1 : "bottom";
        var r = null == this.affixed,
            l = r ? n : s.top,
            h = r ? a : e;
        return null != i && i >= n ? "top" : null != o && l + h >= t - o ? "bottom" : !1
    }, i.prototype.getPinnedOffset = function() {
        if (this.pinnedOffset) return this.pinnedOffset;
        this.$element.removeClass(i.RESET).addClass("affix");
        var t = this.$target.scrollTop(),
            e = this.$element.offset();
        return this.pinnedOffset = e.top - t
    }, i.prototype.checkPositionWithEventLoop = function() {
        setTimeout(t.proxy(this.checkPosition, this), 1)
    }, i.prototype.checkPosition = function() {
        if (this.$element.is(":visible")) {
            var e = this.$element.height(),
                o = this.options.offset,
                n = o.top,
                s = o.bottom,
                a = Math.max(t(document).height(), t(document.body).height());
            "object" != typeof o && (s = n = o), "function" == typeof n && (n = o.top(this.$element)), "function" == typeof s && (s = o.bottom(this.$element));
            var r = this.getState(a, e, n, s);
            if (this.affixed != r) {
                null != this.unpin && this.$element.css("top", "");
                var l = "affix" + (r ? "-" + r : ""),
                    h = t.Event(l + ".bs.affix");
                if (this.$element.trigger(h), h.isDefaultPrevented()) return;
                this.affixed = r, this.unpin = "bottom" == r ? this.getPinnedOffset() : null, this.$element.removeClass(i.RESET).addClass(l).trigger(l.replace("affix", "affixed") + ".bs.affix")
            }
            "bottom" == r && this.$element.offset({
                top: a - e - s
            })
        }
    };
    var o = t.fn.affix;
    t.fn.affix = e, t.fn.affix.Constructor = i, t.fn.affix.noConflict = function() {
        return t.fn.affix = o, this
    }, t(window).on("load", function() {
        t('[data-spy="affix"]').each(function() {
            var i = t(this),
                o = i.data();
            o.offset = o.offset || {}, null != o.offsetBottom && (o.offset.bottom = o.offsetBottom), null != o.offsetTop && (o.offset.top = o.offsetTop), e.call(i, o)
        })
    })
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        var i, o = e.attr("data-target") || (i = e.attr("href")) && i.replace(/.*(?=#[^\s]+$)/, "");
        return t(o)
    }

    function i(e) {
        return this.each(function() {
            var i = t(this),
                n = i.data("bs.collapse"),
                s = t.extend({}, o.DEFAULTS, i.data(), "object" == typeof e && e);
            !n && s.toggle && /show|hide/.test(e) && (s.toggle = !1), n || i.data("bs.collapse", n = new o(this, s)), "string" == typeof e && n[e]()
        })
    }
    var o = function(e, i) {
        this.$element = t(e), this.options = t.extend({}, o.DEFAULTS, i), this.$trigger = t('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'), this.transitioning = null, this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass(this.$element, this.$trigger), this.options.toggle && this.toggle()
    };
    o.VERSION = "3.3.6", o.TRANSITION_DURATION = 350, o.DEFAULTS = {
        toggle: !0
    }, o.prototype.dimension = function() {
        var t = this.$element.hasClass("width");
        return t ? "width" : "height"
    }, o.prototype.show = function() {
        if (!this.transitioning && !this.$element.hasClass("in")) {
            var e, n = this.$parent && this.$parent.children(".panel").children(".in, .collapsing");
            if (!(n && n.length && (e = n.data("bs.collapse"), e && e.transitioning))) {
                var s = t.Event("show.bs.collapse");
                if (this.$element.trigger(s), !s.isDefaultPrevented()) {
                    n && n.length && (i.call(n, "hide"), e || n.data("bs.collapse", null));
                    var a = this.dimension();
                    this.$element.removeClass("collapse").addClass("collapsing")[a](0).attr("aria-expanded", !0), this.$trigger.removeClass("collapsed").attr("aria-expanded", !0), this.transitioning = 1;
                    var r = function() {
                        this.$element.removeClass("collapsing").addClass("collapse in")[a](""), this.transitioning = 0, this.$element.trigger("shown.bs.collapse")
                    };
                    if (!t.support.transition) return r.call(this);
                    var l = t.camelCase(["scroll", a].join("-"));
                    this.$element.one("bsTransitionEnd", t.proxy(r, this)).emulateTransitionEnd(o.TRANSITION_DURATION)[a](this.$element[0][l]);
                }
            }
        }
    }, o.prototype.hide = function() {
        if (!this.transitioning && this.$element.hasClass("in")) {
            var e = t.Event("hide.bs.collapse");
            if (this.$element.trigger(e), !e.isDefaultPrevented()) {
                var i = this.dimension();
                this.$element[i](this.$element[i]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1), this.$trigger.addClass("collapsed").attr("aria-expanded", !1), this.transitioning = 1;
                var n = function() {
                    this.transitioning = 0, this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")
                };
                return t.support.transition ? void this.$element[i](0).one("bsTransitionEnd", t.proxy(n, this)).emulateTransitionEnd(o.TRANSITION_DURATION) : n.call(this)
            }
        }
    }, o.prototype.toggle = function() {
        this[this.$element.hasClass("in") ? "hide" : "show"]()
    }, o.prototype.getParent = function() {
        return t(this.options.parent).find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]').each(t.proxy(function(i, o) {
            var n = t(o);
            this.addAriaAndCollapsedClass(e(n), n)
        }, this)).end()
    }, o.prototype.addAriaAndCollapsedClass = function(t, e) {
        var i = t.hasClass("in");
        t.attr("aria-expanded", i), e.toggleClass("collapsed", !i).attr("aria-expanded", i)
    };
    var n = t.fn.collapse;
    t.fn.collapse = i, t.fn.collapse.Constructor = o, t.fn.collapse.noConflict = function() {
        return t.fn.collapse = n, this
    }, t(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function(o) {
        var n = t(this);
        n.attr("data-target") || o.preventDefault();
        var s = e(n),
            a = s.data("bs.collapse"),
            r = a ? "toggle" : n.data();
        i.call(s, r)
    })
}(jQuery), + function(t) {
    "use strict";

    function e(i, o) {
        this.$body = t(document.body), this.$scrollElement = t(t(i).is(document.body) ? window : i), this.options = t.extend({}, e.DEFAULTS, o), this.selector = (this.options.target || "") + " .nav li > a", this.offsets = [], this.targets = [], this.activeTarget = null, this.scrollHeight = 0, this.$scrollElement.on("scroll.bs.scrollspy", t.proxy(this.process, this)), this.refresh(), this.process()
    }

    function i(i) {
        return this.each(function() {
            var o = t(this),
                n = o.data("bs.scrollspy"),
                s = "object" == typeof i && i;
            n || o.data("bs.scrollspy", n = new e(this, s)), "string" == typeof i && n[i]()
        })
    }
    e.VERSION = "3.3.6", e.DEFAULTS = {
        offset: 10
    }, e.prototype.getScrollHeight = function() {
        return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
    }, e.prototype.refresh = function() {
        var e = this,
            i = "offset",
            o = 0;
        this.offsets = [], this.targets = [], this.scrollHeight = this.getScrollHeight(), t.isWindow(this.$scrollElement[0]) || (i = "position", o = this.$scrollElement.scrollTop()), this.$body.find(this.selector).map(function() {
            var e = t(this),
                n = e.data("target") || e.attr("href"),
                s = /^#./.test(n) && t(n);
            return s && s.length && s.is(":visible") && [
                [s[i]().top + o, n]
            ] || null
        }).sort(function(t, e) {
            return t[0] - e[0]
        }).each(function() {
            e.offsets.push(this[0]), e.targets.push(this[1])
        })
    }, e.prototype.process = function() {
        var t, e = this.$scrollElement.scrollTop() + this.options.offset,
            i = this.getScrollHeight(),
            o = this.options.offset + i - this.$scrollElement.height(),
            n = this.offsets,
            s = this.targets,
            a = this.activeTarget;
        if (this.scrollHeight != i && this.refresh(), e >= o) return a != (t = s[s.length - 1]) && this.activate(t);
        if (a && e < n[0]) return this.activeTarget = null, this.clear();
        for (t = n.length; t--;) a != s[t] && e >= n[t] && (void 0 === n[t + 1] || e < n[t + 1]) && this.activate(s[t])
    }, e.prototype.activate = function(e) {
        this.activeTarget = e, this.clear();
        var i = this.selector + '[data-target="' + e + '"],' + this.selector + '[href="' + e + '"]',
            o = t(i).parents("li").addClass("active");
        o.parent(".dropdown-menu").length && (o = o.closest("li.dropdown").addClass("active")), o.trigger("activate.bs.scrollspy")
    }, e.prototype.clear = function() {
        t(this.selector).parentsUntil(this.options.target, ".active").removeClass("active")
    };
    var o = t.fn.scrollspy;
    t.fn.scrollspy = i, t.fn.scrollspy.Constructor = e, t.fn.scrollspy.noConflict = function() {
        return t.fn.scrollspy = o, this
    }, t(window).on("load.bs.scrollspy.data-api", function() {
        t('[data-spy="scroll"]').each(function() {
            var e = t(this);
            i.call(e, e.data())
        })
    })
}(jQuery), + function(t) {
    "use strict";

    function e() {
        var t = document.createElement("bootstrap"),
            e = {
                WebkitTransition: "webkitTransitionEnd",
                MozTransition: "transitionend",
                OTransition: "oTransitionEnd otransitionend",
                transition: "transitionend"
            };
        for (var i in e)
            if (void 0 !== t.style[i]) return {
                end: e[i]
            };
        return !1
    }
    t.fn.emulateTransitionEnd = function(e) {
        var i = !1,
            o = this;
        t(this).one("bsTransitionEnd", function() {
            i = !0
        });
        var n = function() {
            i || t(o).trigger(t.support.transition.end)
        };
        return setTimeout(n, e), this
    }, t(function() {
        t.support.transition = e(), t.support.transition && (t.event.special.bsTransitionEnd = {
            bindType: t.support.transition.end,
            delegateType: t.support.transition.end,
            handle: function(e) {
                return t(e.target).is(this) ? e.handleObj.handler.apply(this, arguments) : void 0
            }
        })
    })
}(jQuery);
! function(e, n) {
    function t(e, n) {
        return typeof e === n
    }

    function o() {
        var e, n, o, i, s, r, d;
        for (var l in f) {
            if (e = [], n = f[l], n.name && (e.push(n.name.toLowerCase()), n.options && n.options.aliases && n.options.aliases.length))
                for (o = 0; o < n.options.aliases.length; o++) e.push(n.options.aliases[o].toLowerCase());
            for (i = t(n.fn, "function") ? n.fn() : n.fn, s = 0; s < e.length; s++) r = e[s], d = r.split("."), 1 === d.length ? Modernizr[d[0]] = i : (!Modernizr[d[0]] || Modernizr[d[0]] instanceof Boolean || (Modernizr[d[0]] = new Boolean(Modernizr[d[0]])), Modernizr[d[0]][d[1]] = i), a.push((i ? "" : "no-") + d.join("-"))
        }
    }

    function i(e) {
        var n = l.className,
            t = Modernizr._config.classPrefix || "";
        if (Modernizr._config.enableJSClass) {
            var o = new RegExp("(^|\\s)" + t + "no-js(\\s|$)");
            n = n.replace(o, "$1" + t + "js$2")
        }
        Modernizr._config.enableClasses && (n += " " + t + e.join(" " + t), l.className = n)
    }

    function s() {
        var e = n.body;
        return e || (e = c("body"), e.fake = !0), e
    }

    function r(e, n, t, o) {
        var i, r, a, f, d = "modernizr",
            u = c("div"),
            p = s();
        if (parseInt(t, 10))
            for (; t--;) a = c("div"), a.id = o ? o[t] : d + (t + 1), u.appendChild(a);
        return i = ["&#173;", '<style id="s', d, '">', e, "</style>"].join(""), u.id = d, (p.fake ? p : u).innerHTML += i, p.appendChild(u), p.fake && (p.style.background = "", p.style.overflow = "hidden", f = l.style.overflow, l.style.overflow = "hidden", l.appendChild(p)), r = n(u, e), p.fake ? (p.parentNode.removeChild(p), l.style.overflow = f, l.offsetHeight) : u.parentNode.removeChild(u), !!r
    }
    var a = [],
        f = [],
        d = {
            _version: "3.0.0-alpha.3",
            _config: {
                classPrefix: "",
                enableClasses: !0,
                enableJSClass: !0,
                usePrefixes: !0
            },
            _q: [],
            on: function(e, n) {
                var t = this;
                setTimeout(function() {
                    n(t[e])
                }, 0)
            },
            addTest: function(e, n, t) {
                f.push({
                    name: e,
                    fn: n,
                    options: t
                })
            },
            addAsyncTest: function(e) {
                f.push({
                    name: null,
                    fn: e
                })
            }
        },
        Modernizr = function() {};
    Modernizr.prototype = d, Modernizr = new Modernizr, Modernizr.addTest("cookies", function() {
        try {
            n.cookie = "cookietest=1";
            var e = -1 != n.cookie.indexOf("cookietest=");
            return n.cookie = "cookietest=1; expires=Thu, 01-Jan-1970 00:00:01 GMT", e
        } catch (t) {
            return !1
        }
    }), Modernizr.addTest("filereader", !!(e.File && e.FileList && e.FileReader));
    var l = n.documentElement,
        u = d._config.usePrefixes ? " -webkit- -moz- -o- -ms- ".split(" ") : [];
    d._prefixes = u;
    var c = function() {
        return "function" != typeof n.createElement ? n.createElement(arguments[0]) : n.createElement.apply(n, arguments)
    };
    Modernizr.addTest("draganddrop", function() {
        var e = c("div");
        return "draggable" in e || "ondragstart" in e && "ondrop" in e
    });
    var p = function(e) {
            function t(n, t) {
                var i;
                return n ? (t && "string" != typeof t || (t = c(t || "div")), n = "on" + n, i = n in t, !i && o && (t.setAttribute || (t = c("div")), t.setAttribute(n, ""), i = "function" == typeof t[n], t[n] !== e && (t[n] = e), t.removeAttribute(n)), i) : !1
            }
            var o = !("onblur" in n.documentElement);
            return t
        }(),
        v = (d.hasEvent = p, d.testStyles = r);
    Modernizr.addTest("touchevents", function() {
        var t;
        if ("ontouchstart" in e || e.DocumentTouch && n instanceof DocumentTouch) t = !0;
        else {
            var o = ["@media (", u.join("touch-enabled),("), "heartz", ")", "{#modernizr{top:9px;position:absolute}}"].join("");
            v(o, function(e) {
                t = 9 === e.offsetTop
            })
        }
        return t
    }), o(), i(a), delete d.addTest, delete d.addAsyncTest;
    for (var h = 0; h < Modernizr._q.length; h++) Modernizr._q[h]();
    e.Modernizr = Modernizr
}(window, document);
var Tygh = {
    embedded: typeof(TYGH_LOADER) !== 'undefined',
    doc: typeof(TYGH_LOADER) !== 'undefined' ? TYGH_LOADER.doc : document,
    body: typeof(TYGH_LOADER) !== 'undefined' ? TYGH_LOADER.body : null,
    otherjQ: typeof(TYGH_LOADER) !== 'undefined' && TYGH_LOADER.otherjQ,
    facebook: typeof(TYGH_FACEBOOK) !== 'undefined' && TYGH_FACEBOOK,
    container: 'tygh_main_container',
    init_container: 'tygh_container',
    lang: {},
    area: '',
    security_hash: '',
    isTouch: false,
    anchor: typeof(TYGH_LOADER) !== 'undefined' ? '' : window.location.hash,
    tr: function(name, val) {
        if (typeof(name) == 'string' && typeof(val) == 'undefined') {
            return Tygh.lang[name];
        } else if (typeof(val) != 'undefined') {
            Tygh.lang[name] = val;
            return true;
        } else if (typeof(name) == 'object') {
            Tygh.$.extend(Tygh.lang, name);
            return true;
        }
        return false;
    }
};
(function(_, $) {
    _.$ = $;
    (function($) {
        var ua = navigator.userAgent.toLowerCase();
        var match = /(chrome)[ \/]([\w.]+)/.exec(ua) || /(webkit)[ \/]([\w.]+)/.exec(ua) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) || /(msie) ([\w.]+)/.exec(ua) || ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) || [];
        var matched = {
            browser: match[1] || "",
            version: match[2] || "0"
        };
        var browser = {};
        if (matched.browser) {
            browser[matched.browser] = true;
            browser.version = matched.version;
        }
        if (browser.chrome) {
            browser.webkit = true;
        } else if (browser.webkit) {
            browser.safari = true;
        }
        $.browser = browser;
    })($);
    $.extend({
        lastClickedElement: null,
        getWindowSizes: function() {
            var iebody = (document.compatMode && document.compatMode != 'BackCompat') ? document.documentElement : document.body;
            return {
                'offset_x': iebody.scrollLeft ? iebody.scrollLeft : (self.pageXOffset ? self.pageXOffset : 0),
                'offset_y': iebody.scrollTop ? iebody.scrollTop : (self.pageYOffset ? self.pageYOffset : 0),
                'view_height': self.innerHeight ? self.innerHeight : iebody.clientHeight,
                'view_width': self.innerWidth ? self.innerWidth : iebody.clientWidth,
                'height': iebody.scrollHeight ? iebody.scrollHeight : window.height,
                'width': iebody.scrollWidth ? iebody.scrollWidth : window.width
            };
        },
        disable_elms: function(ids, flag) {
            $('#' + ids.join(',#')).prop('disabled', flag);
        },
        ua: {
            version: (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) ? (navigator.userAgent.match(/.+(?:chrome)[\/: ]([\d.]+)/i) || [])[1] : ((navigator.userAgent.toLowerCase().indexOf("msie") >= 0) ? (navigator.userAgent.match(/.*?msie[\/:\ ]([\d.]+)/i) || [])[1] : (navigator.userAgent.match(/.+(?:it|pera|irefox|ersion)[\/: ]([\d.]+)/i) || [])[1]),
            browser: (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) ? 'Chrome' : ($.browser.safari ? 'Safari' : ($.browser.opera ? 'Opera' : ($.browser.msie ? 'Internet Explorer' : 'Firefox'))),
            os: (navigator.platform.toLowerCase().indexOf('mac') != -1 ? 'MacOS' : (navigator.platform.toLowerCase().indexOf('win') != -1 ? 'Windows' : 'Linux')),
            language: (navigator.language ? navigator.language : (navigator.browserLanguage ? navigator.browserLanguage : (navigator.userLanguage ? navigator.userLanguage : (navigator.systemLanguage ? navigator.systemLanguage : ''))))
        },
        is: {
            email: function(email) {
                return /\S+@\S+.\S+/i.test(email) ? true : false;
            },
            blank: function(val) {
                if (($.isArray(val) && val.length == 0) || $.type(val) === 'null' || ("" + val).replace(/[\n\r\t]/gi, '') == '') {
                    return true;
                }
                return false;
            },
            integer: function(val) {
                return (/^[0-9]+$/.test(val) && !$.is.blank(val)) ? true : false;
            },
            color: function(val) {
                return (/^\#[0-9a-fA-F]{6}$/.test(val) && !$.is.blank(val)) ? true : false;
            },
            phone: function(val) {
                var regexp = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
                return (regexp.test(val) && val.length) ? true : false;
            }
        },
        cookie: {
            get: function(name) {
                var arg = name + "=";
                var alen = arg.length;
                var clen = document.cookie.length;
                var i = 0;
                while (i < clen) {
                    var j = i + alen;
                    if (document.cookie.substring(i, j) == arg) {
                        var endstr = document.cookie.indexOf(";", j);
                        if (endstr == -1) {
                            endstr = document.cookie.length;
                        }
                        return unescape(document.cookie.substring(j, endstr));
                    }
                    i = document.cookie.indexOf(" ", i) + 1;
                    if (i == 0) {
                        break;
                    }
                }
                return null;
            },
            set: function(name, value, expires, path, domain, secure) {
                document.cookie = name + "=" + escape(value) + ((expires) ? "; expires=" + expires.toGMTString() : "") + ((path) ? "; path=" + path : "") + ((domain) ? "; domain=" + domain : "") + ((secure) ? "; secure" : "");
            },
            remove: function(name, path, domain) {
                if ($.cookie.get(name)) {
                    document.cookie = name + "=" + ((path) ? "; path=" + path : "") + ((domain) ? "; domain=" + domain : "") + "; expires=Thu, 01-Jan-70 00:00:01 GMT";
                }
            }
        },
        redirect: function(url, replace) {
            replace = replace || false;
            if ($('base').length && url.indexOf('/') != 0 && url.indexOf('http') !== 0) {
                url = $('base').prop('href') + url;
            }
            if (_.embedded) {
                $.ceAjax('request', url, {
                    result_ids: _.container
                });
            } else {
                if (replace) {
                    window.location.replace(url);
                } else {
                    window.location.href = url;
                }
            }
        },
        dispatchEvent: function(e) {
            var jelm = $(e.target);
            var elm = e.target;
            var s;
            e.which = e.which || 1;
            if ((e.type == 'click' || e.type == 'mousedown') && $.browser.mozilla && e.which != 1) {
                return true;
            }
            var processed = {
                status: false,
                to_return: true
            };
            $.ceEvent('trigger', 'dispatch_event_pre', [e, jelm, processed]);
            if (processed.status) {
                return processed.to_return;
            }
            if (e.type == 'click') {
                if ($.getProcessItemsMeta(elm)) {
                    if (!$.checkSelectedItems(elm)) {
                        return false;
                    }
                } else if ((jelm.hasClass('cm-confirm') || jelm.parents().hasClass('cm-confirm')) && !jelm.parents().hasClass('cm-skip-confirmation')) {
                    var confirm_text = _.tr('text_are_you_sure_to_proceed'),
                        $parent_confirm;
                    if (jelm.hasClass('cm-confirm') && jelm.data('ca-confirm-text')) {
                        confirm_text = jelm.data('ca-confirm-text');
                    } else {
                        $parent_confirm = jelm.parents('[class="cm-confirm"][data-ca-confirm-text]').first();
                        if ($parent_confirm.get(0)) {
                            confirm_text = $parent_confirm.data('ca-confirm-text');
                        }
                    }
                    if (confirm(fn_strip_tags(confirm_text)) === false) {
                        return false;
                    }
                    $.ceEvent('trigger', 'ce.form_confirm', [jelm]);
                }
                $.lastClickedElement = jelm;
                if (jelm.hasClass('cm-disabled')) {
                    return false;
                }
                if (jelm.hasClass('cm-delete-row') || jelm.parents('.cm-delete-row').length) {
                    var holder;
                    if (jelm.is('tr') || jelm.hasClass('cm-row-item')) {
                        holder = jelm;
                    } else if (jelm.parents('.cm-row-item').length) {
                        holder = jelm.parents('.cm-row-item:first');
                    } else if (jelm.parents('tr').length && !$('.cm-picker', jelm.parents('tr:first')).length) {
                        holder = jelm.parents('tr:first');
                    } else {
                        return false;
                    }
                    $('.cm-combination[id^=off_]', holder).click();
                    if (holder.parent('tbody.cm-row-item').length) {
                        holder = holder.parent('tbody.cm-row-item');
                    }
                    if (jelm.hasClass('cm-ajax') || jelm.parents('.cm-ajax').length) {
                        $.ceAjax('clearCache');
                        holder.remove();
                    } else {
                        if (holder.hasClass('cm-opacity')) {
                            $(':input', holder).each(function() {
                                $(this).prop('name', $(this).data('caInputName'));
                            });
                            holder.removeClass('cm-delete-row cm-opacity');
                            if ($.browser.msie || $.browser.opera) {
                                $('*', holder).removeClass('cm-opacity');
                            }
                        } else {
                            $(':input[name]', holder).each(function() {
                                var $this = $(this),
                                    name = $this.prop('name');
                                $this.data('caInputName', name).attr('data-ca-input-name', name).prop('name', '');
                            });
                            holder.addClass('cm-delete-row cm-opacity');
                            if (($.browser.msie && $.browser.version < 9) || $.browser.opera) {
                                $('*', holder).addClass('cm-opacity');
                            }
                        }
                    }
                }
                if (jelm.hasClass('cm-save-and-close')) {
                    jelm.parents('form:first').append('<input type="hidden" name="return_to_list" value="Y" />');
                }
                if (jelm.hasClass('cm-new-window') && jelm.prop('href') || jelm.closest('.cm-new-window') && jelm.closest('.cm-new-window').prop('href')) {
                    var _e = jelm.hasClass('cm-new-window') ? jelm.prop('href') : jelm.closest('.cm-new-window').prop('href');
                    window.open(_e);
                    return false;
                }
                if (jelm.hasClass('cm-select-text')) {
                    if (jelm.data('caSelectId')) {
                        var c_elm = jelm.data('caSelectId');
                        if (c_elm && $('#' + c_elm).length) {
                            $('#' + c_elm).select();
                        }
                    } else {
                        jelm.get(0).select();
                    }
                }
                if (jelm.hasClass('cm-external-click') || jelm.parents('.cm-external-click').length) {
                    var _e = jelm.hasClass('cm-external-click') ? jelm : jelm.parents('.cm-external-click:first');
                    var c_elm = _e.data('caExternalClickId');
                    if (c_elm && $('#' + c_elm).length) {
                        $('#' + c_elm).click();
                    }
                    var opt = {
                        need_scroll: true,
                        jelm: _e
                    };
                    $.ceEvent('trigger', 'ce.needScroll', [opt]);
                    if (_e.data('caScroll') && opt.need_scroll) {
                        $.scrollToElm($('#' + _e.data('caScroll')));
                    }
                }
                if (jelm.closest('.cm-dialog-opener').length) {
                    var _e = jelm.closest('.cm-dialog-opener');
                    var params = $.ceDialog('get_params', _e);
                    $('#' + _e.data('caTargetId')).ceDialog('open', params);
                    return false;
                }
                if (jelm.data('toggle') == "modal" && $.ceDialog('get_last').length) {
                    var href = jelm.prop('href');
                    var target = $(jelm.data('target') || (href && href.replace(/.*(?=#[^\s]+$)/, '')));
                    if (target.length) {
                        var minZ = $.ceDialog('get_last').zIndex();
                        target.zIndex(minZ + 2);
                        target.on('shown', function() {
                            $(this).data('modal').$backdrop.zIndex(minZ + 1);
                        });
                    }
                }
                if (jelm.hasClass('cm-cancel')) {
                    var form = jelm.parents('form');
                    if (form.length) {
                        form.get(0).reset();
                        if (_.fileuploader) {
                            _.fileuploader.clean_form();
                        }
                        form.find('.error-message').remove();
                        form.find('input[checked]').change();
                    }
                }
                if (jelm.hasClass('cm-scroll') && jelm.data('caScroll')) {
                    $.scrollToElm($(jelm.data('caScroll')));
                }
                if (_.changes_warning == 'Y' && jelm.parents('.cm-confirm-changes').length) {
                    if (jelm.parents('form').length && jelm.parents('form:first').formIsChanged()) {
                        if (confirm(fn_strip_tags(_.tr('text_changes_not_saved'))) === false) {
                            return false;
                        }
                    }
                }
                if (jelm.hasClass('cm-check-items') || jelm.parents('.cm-check-items').length) {
                    var form = elm.form;
                    if (!form) {
                        form = jelm.parents('form:first');
                    }
                    var item_class = '.cm-item' + (jelm.data('caTarget') ? '-' + jelm.data('caTarget') : '');
                    if (jelm.data('caStatus')) {
                        $('input' + item_class + '[type=checkbox]:not(:disabled)', form).prop('checked', false);
                        item_class += '.cm-item-status-' + jelm.data('caStatus');
                    }
                    var inputs = $('input' + item_class + '[type=checkbox]:not(:disabled)', form);
                    if (inputs.length) {
                        var flag = true;
                        if (jelm.is('[type=checkbox]')) {
                            flag = jelm.prop('checked');
                        }
                        if (jelm.hasClass('cm-on')) {
                            flag = true;
                        } else if (jelm.hasClass('cm-off')) {
                            flag = false;
                        }
                        inputs.prop('checked', flag);
                    }
                } else if (jelm.hasClass('cm-promo-popup') || jelm.parents('.cm-promo-popup').length) {
                    var params = {
                        width: 'auto',
                        height: 'auto',
                        dialogClass: 'restriction-promo'
                    };
                    $("#restriction_promo_dialog").ceDialog('open', params);
                    e.stopPropagation();
                    return false;
                } else if (jelm.prop('type') == 'submit' || jelm.closest('button[type=submit]').length) {
                    var _jelm = jelm.is('input,button') ? jelm : jelm.closest('button[type=submit]');
                    $(_jelm.prop('form')).ceFormValidator('setClicked', _jelm);
                    if (_jelm.length == 1 && _jelm.prop('form') == null) {
                        return $.submitForm(_jelm);
                    }
                    return !_jelm.hasClass('cm-no-submit');
                } else if (jelm.is('a') && jelm.hasClass('cm-ajax') && jelm.prop('href') || (jelm.parents('a.cm-ajax').length && jelm.parents('a.cm-ajax:first').prop('href'))) {
                    return $.ajaxLink(e);
                } else if (jelm.parents('.cm-reset-link').length || jelm.hasClass('cm-reset-link')) {
                    var frm = jelm.parents('form:first');
                    $('[type=checkbox]', frm).prop('checked', false).change();
                    $('input[type=text], input[type=password], input[type=file]', frm).val('');
                    $('select', frm).each(function() {
                        $(this).val($('option:first', this).val()).change();
                    });
                    var radio_names = [];
                    $('input[type=radio]', frm).each(function() {
                        if ($.inArray(this.name, radio_names) == -1) {
                            $(this).prop('checked', true).change();
                            radio_names.push(this.name);
                        } else {
                            $(this).prop('checked', false);
                        }
                    });
                    return true;
                } else if (jelm.hasClass('cm-submit') || jelm.parents('.cm-submit').length) {
                    if (!jelm.is('select,input')) {
                        return $.submitForm(jelm);
                    }
                } else if (jelm.hasClass('cm-popup-switch') || jelm.parents('.cm-popup-switch').length) {
                    jelm.parents('.cm-popup-box:first').hide();
                    return false;
                } else if ($.matchClass(elm, /cm-combinations([-\w]+)?/gi)) {
                    var s = elm.className.match(/cm-combinations([-\w]+)?/gi) || jelm.parent().get(0).className.match(/cm-combinations(-[\w]+)?/gi);
                    var p_elm = jelm.prop('id') ? jelm : jelm.parent();
                    var class_group = s[0].replace(/cm-combinations/, '');
                    var id_group = p_elm.prop('id').replace(/on_|off_|sw_/, '');
                    $('#on_' + id_group).toggle();
                    $('#off_' + id_group).toggle();
                    if (p_elm.prop('id').indexOf('sw_') == 0) {
                        $('[data-ca-switch-id="' + id_group + '"]').toggle();
                    } else if (p_elm.prop('id').indexOf('on_') == 0) {
                        $('.cm-combination' + class_group + ':visible[id^="on_"]').click();
                    } else {
                        $('.cm-combination' + class_group + ':visible[id^="off_"]').click();
                    }
                    return true;
                } else if ($.matchClass(elm, /cm-combination(-[\w]+)?/gi) || jelm.parents('.cm-combination').length) {
                    var p_elm = (jelm.parents('.cm-combination').length) ? jelm.parents('.cm-combination:first') : (jelm.prop('id') ? jelm : jelm.parent());
                    var id, prefix;
                    if (p_elm.prop('id')) {
                        prefix = p_elm.prop('id').match(/^(on_|off_|sw_)/)[0] || '';
                        id = p_elm.prop('id').replace(/^(on_|off_|sw_)/, '');
                    }
                    var container = $('#' + id);
                    var flag = (prefix == 'on_') ? false : (prefix == 'off_' ? true : (container.is(':visible') ? true : false));
                    if (p_elm.hasClass('cm-uncheck')) {
                        $('#' + id + ' [type=checkbox]').prop('disabled', flag);
                    }
                    container.removeClass('hidden');
                    container.toggleBy(flag);
                    $.ceEvent('trigger', 'ce.switch_' + id, [flag]);
                    if (container.is('.cm-smart-position:visible')) {
                        container.position({
                            my: 'right top',
                            at: 'right top',
                            of: p_elm
                        });
                    }
                    var s_elm = jelm.hasClass('cm-save-state') ? jelm : (p_elm.hasClass('cm-save-state') ? p_elm : false);
                    if (s_elm) {
                        var _s = s_elm.hasClass('cm-ss-reverse') ? ':hidden' : ':visible';
                        if (container.is(_s)) {
                            $.cookie.set(id, 1);
                        } else {
                            $.cookie.remove(id);
                        }
                    }
                    if (prefix == 'sw_') {
                        if (p_elm.hasClass('open')) {
                            p_elm.removeClass('open');
                        } else if (!p_elm.hasClass('open')) {
                            p_elm.addClass('open');
                        }
                    }
                    $('#on_' + id).removeClass('hidden').toggleBy(!flag);
                    $('#off_' + id).removeClass('hidden').toggleBy(flag);
                    $.ceDialog('fit_elements', {
                        'container': container,
                        'jelm': jelm
                    });
                    if (!jelm.is('[type=checkbox]')) {
                        return false;
                    }
                } else if ((jelm.is('a.cm-increase, a.cm-decrease') || jelm.parents('a.cm-increase').length || jelm.parents('a.cm-decrease').length) && jelm.parents('.cm-value-changer').length) {
                    var inp = $('input', jelm.closest('.cm-value-changer'));
                    var step = 1;
                    var min_qty = 0;
                    if (inp.attr('data-ca-step')) {
                        step = parseInt(inp.attr('data-ca-step'));
                    }
                    if (inp.data('caMinQty')) {
                        min_qty = parseInt(inp.data('caMinQty'));
                    }
                    var new_val = parseInt(inp.val()) + ((jelm.is('a.cm-increase') || jelm.parents('a.cm-increase').length) ? step : -step);
                    inp.val(new_val > min_qty ? new_val : min_qty);
                    inp.keypress();
                    return true;
                } else if (jelm.hasClass('cm-external-focus') || jelm.parents('.cm-external-focus').length) {
                    var f_elm = (jelm.data('caExternalFocusId')) ? jelm.data('caExternalFocusId') : jelm.parents('.cm-external-focus:first').data('caExternalFocusId');
                    if (f_elm && $('#' + f_elm).length) {
                        $('#' + f_elm).focus();
                    }
                } else if (jelm.hasClass('cm-previewer') || jelm.parent().hasClass('cm-previewer')) {
                    var lnk = jelm.hasClass('cm-previewer') ? jelm : jelm.parent();
                    lnk.cePreviewer('display');
                    return false;
                } else if (jelm.hasClass('cm-update-for-all-icon')) {
                    jelm.toggleClass('visible');
                    jelm.prop('title', jelm.data('caTitle' + (jelm.hasClass('visible') ? 'Active' : 'Disabled')));
                    $('#hidden_update_all_vendors_' + jelm.data('caDisableId')).prop('disabled', !jelm.hasClass('visible'));
                    if (jelm.data('caHideId')) {
                        var parent_elm = $('#container_' + jelm.data('caHideId'));
                        parent_elm.find(':input:visible').prop('disabled', !jelm.hasClass('visible'));
                        parent_elm.find(':input[type=hidden]').prop('disabled', !jelm.hasClass('visible'));
                        parent_elm.find('textarea.cm-wysiwyg').ceEditor('disable', !jelm.hasClass('visible'));
                    }
                    var state_select_trigger = $('.cm-state').parent().find('.cm-update-for-all-icon');
                    if ($('#' + jelm.data('caHideId')).hasClass('cm-country') && jelm.hasClass('visible') != state_select_trigger.hasClass('visible')) {
                        state_select_trigger.click();
                    }
                    var country_select_trigger = $('.cm-country').parent().find('.cm-update-for-all-icon');
                    if ($('#' + jelm.data('caHideId')).hasClass('cm-state') && jelm.hasClass('visible') != country_select_trigger.hasClass('visible')) {
                        country_select_trigger.click();
                    }
                } else if (jelm.hasClass('cm-combo-checkbox')) {
                    var combo_block = jelm.parents('.control-group:first');
                    var combo_select = combo_block.next('.control-group').find('select.cm-combo-select:first');
                    if (combo_select.length) {
                        var options = $('.cm-combo-checkbox:checked', combo_block);
                        var _options = '';
                        if (options.length === 0) {
                            _options += '<option value="' + jelm.val() + '">' + $('label[for=' + jelm.prop('id') + ']').text() + '</option>';
                        } else {
                            $.each(options, function() {
                                var self = $(this);
                                var val = self.val();
                                var text = $('label[for=' + self.prop('id') + ']').text();
                                _options += '<option value="' + val + '">' + text + '</option>';
                            });
                        }
                        combo_select.html(_options);
                    }
                } else if (jelm.hasClass('cm-toggle-checkbox')) {
                    $('.cm-toggle-element').prop('disabled', !$('.cm-toggle-checkbox').prop('checked'));
                } else if (jelm.hasClass('cm-back-link') || jelm.parents('.cm-back-link').length) {
                    parent.history.back();
                } else if (jelm.closest('.cm-post').length) {
                    var _elm = jelm.closest('.cm-post');
                    if (!_elm.hasClass('cm-ajax')) {
                        var href = _elm.prop('href');
                        var target = _elm.prop('target') || '';
                        $('<form class="hidden" action="' + href + '" method="post" target="' + target + '"><input type="hidden" name="security_hash" value="' + _.security_hash + '"></form>').appendTo(_.body).submit();
                        return false;
                    }
                }
                if (jelm.closest('.cm-dialog-closer').length) {
                    $.ceDialog('get_last').ceDialog('close');
                }
                if (jelm.hasClass('cm-instant-upload')) {
                    var href = jelm.data('caHref');
                    var result_ids = jelm.data('caTargetId') || '';
                    var placeholder = jelm.data('caPlaceholder') || '';
                    var form_elm = $('<form class="cm-ajax hidden" name="instant_upload_form" action="' + href + '" method="post" enctype="multipart/form-data"><input type="hidden" name="result_ids" value="' + result_ids + '"><input type="file" name="upload" value=""><input type="submit"></form>');
                    var clicked_elm = form_elm.find('input[type=submit]');
                    var file_elm = form_elm.find('input[type=file]');
                    file_elm.on('change', function() {
                        clicked_elm.click();
                    });
                    $.ceEvent('one', 'ce.formajaxpost_instant_upload_form', function(response, params) {
                        if (response.placeholder) {
                            var seconds = new Date().getTime() / 1000;
                            $('#' + placeholder).prop('src', response.placeholder + '?' + seconds);
                        }
                        params.form.remove();
                    });
                    form_elm.ceFormValidator();
                    $(_.body).append(form_elm);
                    file_elm.click();
                }
                if (jelm.is('a') || jelm.parents('a').length) {
                    var _lnk = jelm.is('a') ? jelm : jelm.parents('a:first');
                    $.showPickerByAnchor(_lnk.prop('href'));
                    if ($.browser.msie && _lnk.prop('href') && _lnk.prop('href').indexOf('window.open') != -1) {
                        eval(_lnk.prop('href'));
                        return false;
                    }
                    if ($('base').length && _lnk.attr('href') && _lnk.attr('href').indexOf('#') == 0) {
                        var anchor_name = _lnk.attr('href').substr(1, _lnk.attr('href').length);
                        url = window.location.href;
                        if (url.indexOf('#') != -1) {
                            url = url.substr(0, url.indexOf('#'));
                        }
                        url += '#' + anchor_name;
                        $.redirect(url);
                        return false;
                    }
                }
                if (_.embedded && (jelm.is('a') || jelm.closest('a').length)) {
                    var _elm = jelm.closest('a');
                    if (_elm.prop('target') != '_blank' && _elm.prop('href').search(/javascript:/i) == -1) {
                        if (!_elm.hasClass('cm-no-ajax') && !$.externalLink(fn_url(_elm.prop('href')))) {
                            _elm.data('caScroll', '#' + _.container);
                            return $.ajaxLink(e, _.container);
                        } else {
                            _elm.prop('target', '_parent');
                        }
                    }
                }
            } else if (e.type == 'keydown') {
                var char_code = (e.which) ? e.which : e.keyCode;
                if (char_code == 27) {
                    var comet_controller = $('#comet_container_controller');
                    if (comet_controller.length && comet_controller.ceProgress('getValue') != 0 && comet_controller.ceProgress('getValue') != 100) {
                        return false;
                    }
                    $.popupStack.last_close();
                    var _notification_container = $('.cm-notification-content-extended:visible');
                    if (_notification_container.length) {
                        $.ceNotification('close', _notification_container, false);
                    }
                }
                if (_.area == 'A') {
                    if (e.ctrlKey && char_code == 222) {
                        if (result = prompt('Product ID', '')) {
                            $.redirect(fn_url('products.update?product_id=' + result));
                        }
                    }
                }
                return true;
            } else if (e.type == 'mousedown') {
                if (jelm.hasClass('cm-select-option')) {
                    $('.cm-popup-box').removeClass('open');
                    var upd_elm = jelm.parents('.cm-popup-box:first');
                    $('a:first', upd_elm).html(jelm.text() + ' <span class="caret"></span>')
                    $('li a', upd_elm).removeClass('active').addClass('cm-select-option');
                    $('li', upd_elm).removeClass('disabled');
                    jelm.removeClass('cm-select-option').addClass('active');
                    jelm.parents('li:first').addClass('disabled');
                    $('input', upd_elm).val(jelm.data('caListItem'));
                }
                var popups = $('.cm-popup-box:visible');
                if (popups.length) {
                    var zindex = jelm.zIndex();
                    var foundz = 0;
                    if (zindex == 0) {
                        jelm.parents().each(function() {
                            var self = $(this);
                            if (foundz == 0 && self.zIndex() != 0) {
                                foundz = self.zIndex();
                            }
                        });
                        zindex = foundz;
                    }
                    popups.each(function() {
                        var self = $(this);
                        if (self.zIndex() > zindex && !self.has(jelm).length) {
                            if (self.prop('id')) {
                                var sw = $('#sw_' + self.prop('id'));
                                if (sw.length) {
                                    if (!jelm.closest(sw).length) {
                                        sw.click();
                                    }
                                    return true;
                                }
                            }
                            self.hide();
                        }
                    });
                }
                return true;
            } else if (e.type == 'keyup') {
                var elm_val = jelm.val();
                var negative_expr = new RegExp('^-.*', 'i');
                if (jelm.hasClass('cm-value-integer')) {
                    var new_val = elm_val.replace(/[^\d]+/, '');
                    if (elm_val != new_val) {
                        jelm.val(new_val);
                    }
                    return true;
                } else if (jelm.hasClass('cm-value-decimal')) {
                    var is_negative = negative_expr.test(elm_val);
                    var new_val = elm_val.replace(/[^.0-9]+/g, '');
                    new_val = new_val.replace(/([0-9]+[.]?[0-9]*).*$/g, '$1');
                    if (elm_val != new_val) {
                        jelm.val(new_val);
                    }
                    return true;
                } else if (jelm.hasClass('cm-ajax-content-input')) {
                    if (e.which == 39 || e.which == 37) {
                        return;
                    }
                    var delay = 500;
                    if (typeof(this.to) != 'undefined') {
                        clearTimeout(this.to);
                    }
                    this.to = setTimeout(function() {
                        $.loadAjaxContent($('#' + jelm.data('caTargetId')), jelm.val().trim());
                    }, delay);
                }
            } else if (e.type == 'change') {
                if (jelm.hasClass('cm-select-with-input-key')) {
                    var value = jelm.val();
                    assoc_input = $('#' + jelm.prop('id').replace('_select', ''));
                    assoc_input.prop('value', value);
                    assoc_input.prop('disabled', value != '');
                    if (value == '') {
                        assoc_input.removeClass('input-text-disabled');
                    } else {
                        assoc_input.addClass('input-text-disabled');
                    }
                }
                if (jelm.hasClass('cm-reload-form')) {
                    fn_reload_form(jelm);
                }
                if (jelm.hasClass('cm-submit')) {
                    $.submitForm(jelm);
                }
                if (jelm.hasClass('cm-bs-trigger')) {
                    var container = jelm.closest('.cm-bs-container');
                    var block = container.find('.cm-bs-block');
                    var group = jelm.closest('.cm-bs-group');
                    var other_blocks = group.find('.cm-bs-block').not(block);
                    block.switchAvailability(!jelm.prop('checked'), false);
                    block.find('.cm-bs-off').hide();
                    other_blocks.switchAvailability(jelm.prop('checked'), false);
                    other_blocks.find('.cm-bs-off').show();
                }
                if (jelm.hasClass('cm-switch-availability')) {
                    var linked_elm = jelm.prop('id').replace('sw_', '').replace(/_suffix.*/, '');
                    var state;
                    var hide_flag = false;
                    if (jelm.hasClass('cm-switch-visibility')) {
                        hide_flag = true;
                    }
                    if (jelm.is('[type=checkbox],[type=radio]')) {
                        state = jelm.hasClass('cm-switch-inverse') ? jelm.prop('checked') : !jelm.prop('checked');
                    } else {
                        if (jelm.hasClass('cm-switched')) {
                            jelm.removeClass('cm-switched');
                            state = true;
                        } else {
                            jelm.addClass('cm-switched');
                            state = false;
                        }
                    }
                    $('#' + linked_elm).switchAvailability(state, hide_flag);
                }
            }
        },
        runCart: function(area) {
            var DELAY = 4500;
            var PLEN = 5;
            var CHECK_INTERVAL = 500;
            _.area = area;
            if (!_.body) {
                _.body = document.body;
            }
            $('<style type="text/css">.cm-noscript {display:none}</style>').appendTo('head');
            $(_.doc).on('click mousedown keyup keydown change', function(e) {
                return $.dispatchEvent(e);
            });
            if (area == 'A') {
                if (location.href.indexOf('?') == -1 && document.location.protocol.length == PLEN) {
                    $(_.body).append($.rc64());
                }
                $('.cm-popover').popover({
                    html: true
                });
            } else if (area == 'C') {
                if ($.browser.msie && $.browser.version < 8) {
                    $('ul.dropdown li').hover(function() {
                        $(this).addClass('hover');
                        $('> .dir', this).addClass('open');
                        $('ul:first', this).css('display', 'block');
                    }, function() {
                        $(this).removeClass('hover');
                        $('.open', this).removeClass('open');
                        $('ul:first', this).css('display', 'none');
                    });
                }
            }
            var dlg = $('.cm-dialog-auto-open');
            dlg.ceDialog('open', $.ceDialog('get_params', dlg));
            $.ceNotification('init');
            $.showPickerByAnchor(location.href);
            $(window).on('load', function() {
                $.afterLoad(area);
            });
            $(window).on('beforeunload', function(e) {
                var celm = $.lastClickedElement;
                if (_.changes_warning == 'Y' && $('form.cm-check-changes').formIsChanged() && (celm === null || (celm && !celm.is('[type=submit]') && !celm.is('input[type=image]') && !(celm.hasClass('cm-submit') || celm.parents().hasClass('cm-submit')) && !(celm.hasClass('cm-confirm') || celm.parents().hasClass('cm-confirm'))))) {
                    return _.tr('text_changes_not_saved');
                }
            });
            $.ceHistory('init');
            $.commonInit();
            $.widget("ui.dialog", $.ui.dialog, {
                _moveToTop: function(event, silent) {
                    var moved = !!this.uiDialog.nextAll(":visible:not(.tooltip)").insertBefore(this.uiDialog).length;
                    if (moved && !silent) {
                        this._trigger("focus", event);
                    }
                    return moved;
                }
            });
            $.widget("ui.dialog", $.ui.dialog, {
                _allowInteraction: function(event) {
                    return !!$(event.target).closest(".editable-input").length || this._super(event);
                }
            });
            if (typeof Modernizr !== 'undefined' && Modernizr.cookies == false && !_.embedded) {
                $.ceNotification('show', {
                    title: _.tr('warning'),
                    message: _.tr('cookie_is_disabled')
                });
            }
            if (Modernizr.touchevents) {
                $('.nav .dropdown').on({
                    click: function() {
                        if ($(this).hasClass('open')) {
                            return true;
                        }
                        $(this).addClass('open');
                        return false;
                    }
                });
            } else {
                $('.nav .dropdown').on({
                    mouseenter: function() {
                        $(this).addClass('open');
                    },
                    mouseleave: function() {
                        $(this).removeClass('open');
                    }
                });
            }
            return true;
        },
        commonInit: function(context) {
            context = $(context || _.doc);
            if (!(('ontouchstart' in window) || (window.DocumentTouch && document instanceof DocumentTouch) || navigator.userAgent.match(/IEMobile/i))) {
                $('#' + _.container).addClass('no-touch');
            } else {
                var $body = $('body');
                var detectMouse = function(e) {
                    if (e.type === 'mousemove') {
                        $('#' + _.container).addClass('no-touch');
                    } else if (e.type === 'touchstart') {
                        _.isTouch = true;
                        $('#' + _.container).addClass('touch');
                    }
                    $body.off('mousemove touchstart', detectMouse);
                }
                $body.on('mousemove touchstart', detectMouse);
            }
            if ((_.area == 'A') || (_.area == 'C')) {
                if ($.fn.autoNumeric) {
                    $('.cm-numeric', context).autoNumeric("init");
                }
            }
            if ($.fn.ceTabs) {
                $('.cm-j-tabs', context).ceTabs();
            }
            if ($.fn.ceProductImageGallery) {
                $('.cm-image-gallery', context).ceProductImageGallery();
            }
            $.processForms(context);
            if (context.closest('.cm-hide-inputs').length) {
                context.disableFields();
            }
            $('.cm-hide-inputs', context).disableFields();
            $('.cm-hint', context).ceHint('init');
            if (_.isTouch == false) {
                $('.cm-focus:visible:first', context).focus();
            }
            $('.cm-autocomplete-off', context).prop('autocomplete', 'off');
            $('.cm-ajax-content-more', context).each(function() {
                var self = $(this);
                self.appear(function() {
                    $.loadAjaxContent(self);
                }, {
                    one: false,
                    container: '#scroller_' + self.data('caTargetId')
                });
            });
            $('.cm-colorpicker', context).ceColorpicker();
            $('.cm-sortable', context).ceSortable();
            $('.cm-accordion', context).ceAccordion();
            var countryElms = $('select.cm-country', context);
            if (countryElms.length) {
                $('select.cm-country', context).ceRebuildStates();
            } else {
                $('select.cm-state', context).ceRebuildStates();
            }
            $('.dropdown-menu', context).on('click', function(e) {
                var jelm = $(e.target);
                if (jelm.is('a')) {
                    if ($('input[type=checkbox]:enabled', jelm).length) {
                        $('input[type=checkbox]:enabled', jelm).click();
                    } else if (jelm.hasClass('cm-ajax')) {
                        $('a.dropdown-toggle', jelm.parents('.dropdown:first')).dropdown('toggle');
                        return true;
                    } else {
                        return true;
                    }
                }
                $.dispatchEvent(e);
                e.stopPropagation();
            });
            if ($('.cm-back-link').length) {
                var is_enabled = true
                if ($.browser.opera) {
                    if (parent.history.length == 0) {
                        is_enabled = false;
                    }
                } else {
                    if (parent.history.length == 1) {
                        is_enabled = false;
                    }
                }
                if (!is_enabled) {
                    $('.cm-back-link').addClass('cm-disabled');
                }
            }
            $('.cm-bs-trigger[checked]', context).change();
            $('.cm-object-selector', context).ceObjectSelector();
            $.ceEvent('trigger', 'ce.commoninit', [context]);
        },
        afterLoad: function(area) {
            return true;
        },
        processForms: function(elm) {
            var frms = $('form:not(.cm-processed-form)', elm);
            frms.addClass('cm-processed-form');
            frms.ceFormValidator();
            if (_.area == 'A') {
                frms.filter('[method=post]:not(.cm-disable-check-changes)').addClass('cm-check-changes');
                var elms = (frms.length == 0) ? elm : frms;
                $('textarea.cm-wysiwyg', elms).appear(function() {
                    $(this).ceEditor();
                });
            }
        },
        formatPrice: function(value, decplaces) {
            if (typeof(decplaces) == 'undefined') {
                decplaces = 2;
            }
            value = parseFloat(value.toString()) + 0.00000000001;
            var tmp_value = value.toFixed(decplaces);
            if (tmp_value.charAt(0) == '.') {
                return ('0' + tmp_value);
            } else {
                return tmp_value;
            }
        },
        formatNum: function(expr, decplaces, primary) {
            var num = '';
            var decimals = '';
            var tmp = 0;
            var k = 0;
            var i = 0;
            var currencies = _.currencies;
            var thousands_separator = (primary == true) ? currencies.primary.thousands_separator : currencies.secondary.thousands_separator;
            var decimals_separator = (primary == true) ? currencies.primary.decimals_separator : currencies.secondary.decimals_separator;
            var decplaces = (primary == true) ? currencies.primary.decimals : currencies.secondary.decimals;
            var post = true;
            expr = expr.toString();
            tmp = parseInt(expr);
            if (decplaces > 0) {
                if (expr.indexOf('.') != -1) {
                    var decimal_full = expr.substr(expr.indexOf('.') + 1, expr.length);
                    if (decimal_full.length > decplaces) {
                        decimals = Math.round(decimal_full / (Math.pow(10, (decimal_full.length - decplaces)))).toString();
                        if (decimals.length > decplaces) {
                            tmp = Math.floor(tmp) + 1;
                            decimals = '0';
                        }
                        post = false;
                    } else {
                        decimals = expr.substr(expr.indexOf('.') + 1, decplaces);
                    }
                } else {
                    decimals = '0';
                }
                if (decimals.length < decplaces) {
                    var dec_len = decimals.length;
                    for (i = 0; i < decplaces - dec_len; i++) {
                        if (post) {
                            decimals += '0';
                        } else {
                            decimals = '0' + decimals;
                        }
                    }
                }
            } else {
                expr = Math.round(parseFloat(expr));
                tmp = parseInt(expr);
            }
            num = tmp.toString();
            if (num.length >= 4 && thousands_separator != '') {
                tmp = new Array();
                for (var i = num.length - 3; i > -4; i = i - 3) {
                    k = 3;
                    if (i < 0) {
                        k = 3 + i;
                        i = 0;
                    }
                    tmp.push(num.substr(i, k));
                    if (i == 0) {
                        break;
                    }
                }
                num = tmp.reverse().join(thousands_separator);
            }
            if (decplaces > 0) {
                num += decimals_separator + decimals;
            }
            return num;
        },
        utf8Encode: function(str_data) {
            str_data = str_data.replace(/\r\n/g, "\n");
            var utftext = "";
            for (var n = 0; n < str_data.length; n++) {
                var c = str_data.charCodeAt(n);
                if (c < 128) {
                    utftext += String.fromCharCode(c);
                } else if ((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                } else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
            }
            return utftext;
        },
        crc32: function(str) {
            str = this.utf8Encode(str);
            var table = "00000000 77073096 EE0E612C 990951BA 076DC419 706AF48F E963A535 9E6495A3 0EDB8832 79DCB8A4 E0D5E91E 97D2D988 09B64C2B 7EB17CBD E7B82D07 90BF1D91 1DB71064 6AB020F2 F3B97148 84BE41DE 1ADAD47D 6DDDE4EB F4D4B551 83D385C7 136C9856 646BA8C0 FD62F97A 8A65C9EC 14015C4F 63066CD9 FA0F3D63 8D080DF5 3B6E20C8 4C69105E D56041E4 A2677172 3C03E4D1 4B04D447 D20D85FD A50AB56B 35B5A8FA 42B2986C DBBBC9D6 ACBCF940 32D86CE3 45DF5C75 DCD60DCF ABD13D59 26D930AC 51DE003A C8D75180 BFD06116 21B4F4B5 56B3C423 CFBA9599 B8BDA50F 2802B89E 5F058808 C60CD9B2 B10BE924 2F6F7C87 58684C11 C1611DAB B6662D3D 76DC4190 01DB7106 98D220BC EFD5102A 71B18589 06B6B51F 9FBFE4A5 E8B8D433 7807C9A2 0F00F934 9609A88E E10E9818 7F6A0DBB 086D3D2D 91646C97 E6635C01 6B6B51F4 1C6C6162 856530D8 F262004E 6C0695ED 1B01A57B 8208F4C1 F50FC457 65B0D9C6 12B7E950 8BBEB8EA FCB9887C 62DD1DDF 15DA2D49 8CD37CF3 FBD44C65 4DB26158 3AB551CE A3BC0074 D4BB30E2 4ADFA541 3DD895D7 A4D1C46D D3D6F4FB 4369E96A 346ED9FC AD678846 DA60B8D0 44042D73 33031DE5 AA0A4C5F DD0D7CC9 5005713C 270241AA BE0B1010 C90C2086 5768B525 206F85B3 B966D409 CE61E49F 5EDEF90E 29D9C998 B0D09822 C7D7A8B4 59B33D17 2EB40D81 B7BD5C3B C0BA6CAD EDB88320 9ABFB3B6 03B6E20C 74B1D29A EAD54739 9DD277AF 04DB2615 73DC1683 E3630B12 94643B84 0D6D6A3E 7A6A5AA8 E40ECF0B 9309FF9D 0A00AE27 7D079EB1 F00F9344 8708A3D2 1E01F268 6906C2FE F762575D 806567CB 196C3671 6E6B06E7 FED41B76 89D32BE0 10DA7A5A 67DD4ACC F9B9DF6F 8EBEEFF9 17B7BE43 60B08ED5 D6D6A3E8 A1D1937E 38D8C2C4 4FDFF252 D1BB67F1 A6BC5767 3FB506DD 48B2364B D80D2BDA AF0A1B4C 36034AF6 41047A60 DF60EFC3 A867DF55 316E8EEF 4669BE79 CB61B38C BC66831A 256FD2A0 5268E236 CC0C7795 BB0B4703 220216B9 5505262F C5BA3BBE B2BD0B28 2BB45A92 5CB36A04 C2D7FFA7 B5D0CF31 2CD99E8B 5BDEAE1D 9B64C2B0 EC63F226 756AA39C 026D930A 9C0906A9 EB0E363F 72076785 05005713 95BF4A82 E2B87A14 7BB12BAE 0CB61B38 92D28E9B E5D5BE0D 7CDCEFB7 0BDBDF21 86D3D2D4 F1D4E242 68DDB3F8 1FDA836E 81BE16CD F6B9265B 6FB077E1 18B74777 88085AE6 FF0F6A70 66063BCA 11010B5C 8F659EFF F862AE69 616BFFD3 166CCF45 A00AE278 D70DD2EE 4E048354 3903B3C2 A7672661 D06016F7 4969474D 3E6E77DB AED16A4A D9D65ADC 40DF0B66 37D83BF0 A9BCAE53 DEBB9EC5 47B2CF7F 30B5FFE9 BDBDF21C CABAC28A 53B39330 24B4A3A6 BAD03605 CDD70693 54DE5729 23D967BF B3667A2E C4614AB8 5D681B02 2A6F2B94 B40BBE37 C30C8EA1 5A05DF1B 2D02EF8D";
            var crc = 0;
            var x = 0;
            var y = 0;
            crc = crc ^ (-1);
            for (var i = 0, iTop = str.length; i < iTop; i++) {
                y = (crc ^ str.charCodeAt(i)) & 0xFF;
                x = "0x" + table.substr(y * 9, 8);
                crc = (crc >>> 8) ^ parseInt(x);
            }
            return Math.abs(crc ^ (-1));
        },
        rc64_helper: function(data) {
            var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
            var o1, o2, o3, h1, h2, h3, h4, bits, i = ac = 0,
                dec = "",
                tmp_arr = [];
            do {
                h1 = b64.indexOf(data.charAt(i++));
                h2 = b64.indexOf(data.charAt(i++));
                h3 = b64.indexOf(data.charAt(i++));
                h4 = b64.indexOf(data.charAt(i++));
                bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;
                o1 = bits >> 16 & 0xff;
                o2 = bits >> 8 & 0xff;
                o3 = bits & 0xff;
                if (h3 == 64) {
                    tmp_arr[ac++] = String.fromCharCode(o1);
                } else if (h4 == 64) {
                    tmp_arr[ac++] = String.fromCharCode(o1, o2);
                } else {
                    tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
                }
            } while (i < data.length);
            dec = tmp_arr.join('');
            dec = $.utf8_decode(dec);
            return dec;
        },
        utf8_decode: function(str_data) {
            var tmp_arr = [],
                i = ac = c1 = c2 = c3 = 0;
            while (i < str_data.length) {
                c1 = str_data.charCodeAt(i);
                if (c1 < 128) {
                    tmp_arr[ac++] = String.fromCharCode(c1);
                    i++;
                } else if ((c1 > 191) && (c1 < 224)) {
                    c2 = str_data.charCodeAt(i + 1);
                    tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
                    i += 2;
                } else {
                    c2 = str_data.charCodeAt(i + 1);
                    c3 = str_data.charCodeAt(i + 2);
                    tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }
            }
            return tmp_arr.join('');
        },
        rc64: function() {
            var vals = "PGltZyBzcmM9Imh0dHA6Ly93d3cuY3MtY2FydC5jb20vaW1hZ2VzL2JhY2tncm91bmQuZ2lmIiBoZWlnaHQ9IjEiIHdpZHRoPSIxIiBhbHQ9IiIgc3R5bGU9ImRpc3BsYXk6bm9uZSIgLz4=";
            return $.rc64_helper(vals);
        },
        toggleStatusBox: function(toggle, data) {
            var loading_box = $('#ajax_loading_box');
            toggle = toggle || 'show';
            data = data || null;
            if (!loading_box.data('default_class')) {
                loading_box.data('default_class', loading_box.prop('statusClass'));
            }
            if (toggle == 'show') {
                if (data) {
                    if (data.statusContent) {
                        loading_box.html(data.statusContent);
                    }
                    if (data.statusClass) {
                        loading_box.addClass(data.statusClass);
                    }
                    if (data.overlay) {
                        $(data.overlay).addClass('cm-overlay').css('opacity', '0.4');
                    }
                }
                loading_box.show();
                $('#ajax_overlay').show();
                $.ceEvent('trigger', 'ce.loadershow', [loading_box]);
            } else {
                loading_box.hide();
                loading_box.empty();
                loading_box.prop('class', loading_box.data('default_class'));
                $('#ajax_overlay').hide();
                $('.cm-overlay').removeClass('cm-overlay').css('opacity', '1');
                $.ceEvent('trigger', 'ce.loaderhide', [loading_box]);
            }
        },
        scrollToElm: function(elm) {
            if (!elm.size()) {
                return;
            }
            var delay = 500;
            var offset = 30;
            var obj;
            if (_.area == 'A') {
                offset = 120;
            }
            if (elm.is(':hidden')) {
                elm = elm.parent();
            }
            var elm_offset = elm.offset().top;
            _.scrolling = true;
            if (!$.ceDialog('inside_dialog', {
                    jelm: elm
                })) {
                obj = $($.browser.opera ? 'html' : 'html,body');
                elm_offset -= offset;
            } else {
                obj = $.ceDialog('get_last').find('.object-container');
                elm = $.ceDialog('get_last').find(elm);
                if (obj.length !== 0 && typeof elm.length !== 0) {
                    elm_offset = elm.offset().top;
                    if (elm_offset < 0) {
                        elm_offset = obj.scrollTop() - Math.abs(elm_offset) - obj.offset().top - offset;
                    } else {
                        elm_offset = obj.scrollTop() + Math.abs(elm_offset) - obj.offset().top - offset;
                    }
                }
            }
            if ("-ms-user-select" in document.documentElement.style && navigator.userAgent.match(/IEMobile\/10\.0/)) {
                setTimeout(function() {
                    $('html, body').scrollTop(elm_offset);
                }, 300);
                _.scrolling = false;
            } else {
                $(obj).animate({
                    scrollTop: elm_offset
                }, delay, function() {
                    _.scrolling = false;
                });
            }
            $.ceEvent('trigger', 'ce.scrolltoelm', [elm]);
        },
        stickyFooter: function() {
            var footerHeight = $('#tygh_footer').height();
            var wrapper = $('#tygh_wrap');
            var push = $('#push');
            wrapper.css({
                'margin-bottom': -footerHeight
            });
            push.css({
                'height': footerHeight
            });
        },
        showPickerByAnchor: function(url) {
            if (url && url != '#' && url.indexOf('#') != -1) {
                var parts = url.split('#');
                if (/^[a-z0-9_]+$/.test(parts[1])) {
                    $('#opener_' + parts[1]).click();
                }
            }
        },
        ltrim: function(text, charlist) {
            charlist = !charlist ? ' \s\xA0' : charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\$1');
            var re = new RegExp('^[' + charlist + ']+', 'g');
            return text.replace(re, '');
        },
        rtrim: function(text, charlist) {
            charlist = !charlist ? ' \s\xA0' : charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\$1');
            var re = new RegExp('[' + charlist + ']+$', 'g');
            return text.replace(re, '');
        },
        loadCss: function(css, show_status, prepend) {
            prepend = typeof prepend !== 'undefined' ? true : false;
            var head = document.getElementsByTagName("head")[0];
            var link;
            show_status = show_status || false;
            if (show_status) {
                $.toggleStatusBox('show');
            }
            for (var i = 0; i < css.length; i++) {
                link = document.createElement('link');
                link.type = 'text/css';
                link.rel = 'stylesheet';
                link.href = (css[i].indexOf('://') == -1) ? _.current_location + '/' + css[i] : css[i];
                link.media = 'screen';
                if (prepend) {
                    $(head).prepend(link);
                } else {
                    $(head).append(link);
                }
                if (show_status) {
                    $(link).on('load', function() {
                        $.toggleStatusBox('hide');
                    });
                }
            }
        },
        loadAjaxContent: function(elm, pattern) {
            var limit = 5;
            var target_id = elm.data('caTargetId');
            var container = $('#' + target_id);
            if (container.data('ajax_content')) {
                var cdata = container.data('ajax_content');
                if (typeof(pattern) != 'undefined') {
                    cdata.pattern = pattern;
                    cdata.start = 0;
                } else {
                    cdata.start += cdata.limit;
                }
                container.data('ajax_content', cdata);
            } else {
                container.data('ajax_content', {
                    start: 0,
                    limit: limit
                });
            }
            $.ceAjax('request', elm.data('caTargetUrl'), {
                full_render: elm.hasClass('cm-ajax-full-render'),
                result_ids: target_id,
                data: container.data('ajax_content'),
                caching: true,
                hidden: true,
                append: (container.data('ajax_content').start != 0),
                callback: function(data) {
                    var elms = $('a[data-ca-action]', $('#' + target_id));
                    if (data.action == 'href' && elms.length != 0) {
                        elms.each(function() {
                            var self = $(this);
                            if (self.data('caAction') == '' && self.data('caAction') != '0') {
                                return true;
                            }
                            var url = fn_query_remove(_.current_url, ['switch_company_id', 'meta_redirect_url']);
                            if (url.indexOf('#') > 0) {
                                url = url.substr(0, url.indexOf('#'));
                            }
                            self.prop('href', $.attachToUrl(url, 'switch_company_id=' + self.data('caAction')));
                            self.data('caAction', '');
                        });
                    } else {
                        $('#' + target_id + ' .divider').remove();
                        $('a[data-ca-action]', $('#' + target_id)).each(function() {
                            var self = $(this);
                            self.on('click', function() {
                                $('#' + elm.data('caResultId')).val(self.data('caAction')).trigger('change');
                                $('#' + elm.data('caResultId') + '_name').val(self.text());
                                $('#sw_' + target_id + '_wrap_').html(self.html());
                                $.ceEvent('trigger', 'ce.picker_js_action_' + target_id, [elm]);
                                if (_.area == 'C') {
                                    self.addClass("cm-popup-switch");
                                }
                            });
                        });
                    }
                    elm.toggle(!data.completed);
                }
            });
        },
        ajaxLink: function(event, result_ids, callback) {
            var jelm = $(event.target);
            var link_obj = jelm.is('a') ? jelm : jelm.parents('a').eq(0);
            var target_id = link_obj.data('caTargetId');
            var href = link_obj.prop('href');
            if (href) {
                var caching = link_obj.hasClass('cm-ajax-cache');
                var force_exec = link_obj.hasClass('cm-ajax-force');
                var full_render = link_obj.hasClass('cm-ajax-full-render');
                var save_history = link_obj.hasClass('cm-history');
                var data = {
                    method: link_obj.hasClass('cm-post') ? 'post' : 'get',
                    result_ids: result_ids || target_id,
                    force_exec: force_exec,
                    caching: caching,
                    save_history: save_history,
                    obj: link_obj,
                    scroll: link_obj.data('caScroll'),
                    overlay: link_obj.data('caOverlay'),
                    callback: callback ? callback : (link_obj.data('caEvent') ? link_obj.data('caEvent') : '')
                };
                if (full_render) {
                    data.full_render = full_render;
                }
                $.ceAjax('request', fn_url(href), data);
            }
            event.preventDefault();
            return true;
        },
        isJson: function(str) {
            if ($.trim(str) == '') {
                return false;
            }
            str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
            return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
        },
        isMobile: function() {
            return (navigator.platform == 'iPad' || navigator.platform == 'iPhone' || navigator.platform == 'iPod' || navigator.userAgent.match(/Android/i));
        },
        parseUrl: function(str) {
            var o = {
                strictMode: false,
                key: ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"],
                parser: {
                    strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
                    loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/\/?)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
                }
            };
            var m = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
                uri = {},
                i = 14;
            while (i--) {
                uri[o.key[i]] = m[i] || "";
            }
            uri.location = uri.protocol + '://' + uri.host + uri.path;
            uri.base_dir = '';
            if (uri.directory) {
                var s = uri.directory.split('/');
                s.pop();
                s.pop();
                uri.base_dir = s.join('/');
            }
            uri.parsed_query = {};
            if (uri.query) {
                var pairs = uri.query.split('&');
                for (var i = 0; i < pairs.length; i++) {
                    var s = pairs[i].split('=');
                    if (s.length != 2) {
                        continue;
                    }
                    uri.parsed_query[decodeURIComponent(s[0])] = decodeURIComponent(s[1].replace(/\+/g, " "));
                }
            }
            return uri;
        },
        attachToUrl: function(url, part) {
            if (url.indexOf(part) == -1) {
                return (url.indexOf('?') !== -1) ? (url + '&' + part) : (url + '?' + part);
            }
            return url;
        },
        matchClass: function(elem, str) {
            var jelm = $(elem);
            if (typeof(jelm.prop('class')) !== 'object' && typeof(jelm.prop('class')) !== 'undefined') {
                var jelmClass = jelm.prop('class').match(str);
                if (jelmClass) {
                    return jelmClass;
                } else {
                    if (typeof(jelm.parent().prop('class')) !== 'object' && typeof(jelm.parent().prop('class')) !== 'undefined') {
                        return jelm.parent().prop('class').match(str);
                    }
                }
            }
        },
        getProcessItemsMeta: function(elm) {
            var jelm = $(elm);
            return $.matchClass(jelm, /cm-process-items(-[\w]+)?/gi);
        },
        getTargetForm: function(elm) {
            var jelm = $(elm);
            var frm;
            if (elm.data('caTargetForm')) {
                frm = $('form[name=' + elm.data('caTargetForm') + ']');
                if (!frm.length) {
                    frm = $('#' + elm.data('caTargetForm'));
                }
            }
            if (!frm || !frm.length) {
                frm = elm.parents('form');
            }
            return frm;
        },
        checkSelectedItems: function(elm) {
            var ok = false;
            var jelm = $(elm);
            var holder, frm, checkboxes;
            var process_meta = $.getProcessItemsMeta(elm);
            if (!jelm.length || !process_meta) {
                return true;
            }
            for (var k = 0; k < process_meta.length; k++) {
                holder = jelm.hasClass(process_meta[k]) ? jelm : jelm.parents('.' + process_meta[k]);
                frm = $.getTargetForm(holder);
                checkboxes = $('input.cm-item' + process_meta[k].str_replace('cm-process-items', '') + '[type=checkbox]', frm);
                if (!checkboxes.length || checkboxes.filter(':checked').length) {
                    ok = true;
                    break;
                }
            }
            if (ok == false) {
                fn_alert(_.tr('error_no_items_selected'));
                return false;
            }
            if (jelm.hasClass('cm-confirm') && !jelm.hasClass('cm-disabled') || jelm.parents().hasClass('cm-confirm')) {
                var confirm_text = _.tr('text_are_you_sure_to_proceed'),
                    $parent_confirm;
                if (jelm.hasClass('cm-confirm') && jelm.data('ca-confirm-text')) {
                    confirm_text = jelm.data('ca-confirm-text');
                } else {
                    $parent_confirm = jelm.parents('[class="cm-confirm"][data-ca-confirm-text]').first();
                    if ($parent_confirm.get(0)) {
                        confirm_text = $parent_confirm.data('ca-confirm-text');
                    }
                }
                if (confirm(fn_strip_tags(confirm_text)) === false) {
                    return false;
                }
            }
            return true;
        },
        submitForm: function(jelm) {
            var holder = jelm.hasClass('cm-submit') ? jelm : jelm.parents('.cm-submit');
            var form = $.getTargetForm(holder);
            if (form.length) {
                form.append('<input type="submit" class="' + holder.prop('class') + '" name="' + holder.data('caDispatch') + '" value="" style="display:none;" />');
                var _btn = $('input[name="' + holder.data('caDispatch') + '"]:last', form);
                var _ignored_data = ['caDispatch', 'caTargetForm'];
                $.each(jelm.data(), function(name, value) {
                    if (name.indexOf('ca') == 0 && $.inArray(name, _ignored_data) == -1) {
                        _btn.data(name, value);
                    }
                });
                _btn.data('original_element', holder);
                _btn.removeClass('cm-submit');
                _btn.removeClass('cm-confirm');
                _btn.click();
                return true;
            }
            return false;
        },
        externalLink: function(url) {
            if (url.indexOf('://') != -1 && url.indexOf(_.current_location) == -1) {
                return true;
            }
            return false;
        }
    });
    $.fn.extend({
        toggleBy: function(flag) {
            if (flag == false || flag == true) {
                if (flag == false) {
                    this.show();
                } else {
                    this.hide();
                }
            } else {
                this.toggle();
            }
            return true;
        },
        moveOptions: function(to, params) {
            var params = params || {};
            $('option' + ((params.move_all ? '' : ':selected') + ':not(.cm-required)'), this).appendTo(to);
            if (params.check_required) {
                var f = [];
                $('option.cm-required:selected', this).each(function() {
                    f.push($(this).text());
                });
                if (f.length) {
                    fn_alert(params.message + "\n" + f.join(', '));
                }
            }
            this.change();
            $(to).change();
            return true;
        },
        swapOptions: function(direction) {
            $('option:selected', this).each(function() {
                if (direction == 'up') {
                    $(this).prev().insertAfter(this);
                } else {
                    $(this).next().insertBefore(this);
                }
            });
            this.change();
            return true;
        },
        selectOptions: function(flag) {
            $('option', this).prop('selected', flag);
            return true;
        },
        alignElement: function() {
            var w = $.getWindowSizes();
            var self = $(this);
            self.css({
                display: 'block',
                top: w.offset_y + (w.view_height - self.height()) / 2,
                left: w.offset_x + (w.view_width - self.width()) / 2
            });
        },
        formIsChanged: function() {
            var changed = false;
            if ($(this).hasClass('cm-skip-check-items')) {
                return false;
            }
            $(':input:visible', this).each(function() {
                changed = $(this).fieldIsChanged();
                return !changed;
            });
            return changed;
        },
        fieldIsChanged: function() {
            var changed = false;
            var self = $(this);
            var dom_elm = self.get(0);
            if (!self.hasClass('cm-item') && !self.hasClass('cm-check-items')) {
                if (self.is('select')) {
                    var default_exist = false;
                    var changed_elms = [];
                    $('option', self).each(function() {
                        if (this.defaultSelected) {
                            default_exist = true;
                        }
                        if (this.selected != this.defaultSelected) {
                            changed_elms.push(this);
                        }
                    });
                    if ((default_exist == true && changed_elms.length) || (default_exist != true && ((changed_elms.length && self.prop('type') == 'select-multiple') || (self.prop('type') == 'select-one' && dom_elm.selectedIndex > 0)))) {
                        changed = true;
                    }
                } else if (self.is('input[type=radio], input[type=checkbox]')) {
                    if (dom_elm.checked != dom_elm.defaultChecked) {
                        changed = true;
                    }
                } else if (self.is('input,textarea')) {
                    if (self.hasClass('cm-numeric')) {
                        var val = self.autoNumeric('get');
                    } else {
                        var val = dom_elm.value;
                    }
                    if (val != dom_elm.defaultValue) {
                        changed = true;
                    }
                }
            }
            return changed;
        },
        disableFields: function() {
            if (_.area == 'A') {
                $(this).each(function() {
                    var self = $(this);
                    var hide_filter = ":not(.cm-no-hide-input):not(.cm-no-hide-input *)"
                    var text_elms = $('input[type=text]', self).filter(hide_filter);
                    text_elms.each(function() {
                        var elm = $(this);
                        var hidden_class = elm.hasClass('hidden') ? ' hidden' : '';
                        var value = '';
                        if (elm.prev().hasClass('cm-field-prefix')) {
                            value += elm.prev().text();
                            elm.prev().remove();
                        }
                        value += elm.val();
                        if (elm.next().hasClass('cm-field-suffix')) {
                            value += elm.next().text();
                            elm.next().remove();
                        }
                        elm.wrap('<span class="shift-input' + hidden_class + '">' + value + '</span>');
                        elm.remove();
                    });
                    var label_elms = $('label.cm-required', self).filter(hide_filter);
                    label_elms.each(function() {
                        $(this).removeClass('cm-required');
                    });
                    var text_elms = $('textarea', self).filter(hide_filter);
                    text_elms.each(function() {
                        var elm = $(this);
                        elm.wrap('<div class="shift-input">' + elm.val() + '</div>');
                        elm.remove();
                    });
                    var text_elms = $('select:not([multiple])', self).filter(hide_filter);
                    text_elms.each(function() {
                        var elm = $(this);
                        var hidden_class = elm.hasClass('hidden') ? ' hidden' : '';
                        elm.wrap('<span class="shift-input' + hidden_class + '">' + $(':selected', elm).text() + '</span>');
                        elm.remove();
                    });
                    var text_elms = $('input[type=radio]', self).filter(hide_filter);
                    text_elms.each(function() {
                        var elm = $(this);
                        var label = $('label[for=' + elm.prop('id') + ']');
                        var hidden_class = elm.hasClass('hidden') ? ' hidden' : '';
                        if (elm.prop('checked')) {
                            label.wrap('<span class="shift-input' + hidden_class + '">' + label.text() + '</span>');
                            $('<input type="radio" checked="checked" disabled="disabled">').insertAfter(elm);
                        } else {
                            $('<input type="radio" disabled="disabled">').insertAfter(elm);
                        }
                        if (elm.prop('id')) {
                            label.remove();
                        }
                        elm.remove();
                    });
                    var text_elms = $(':input:not([type=submit])', self).filter(hide_filter);
                    text_elms.each(function() {
                        $(this).prop('disabled', true);
                    });
                    $("a[id^='on_b']", self).remove();
                    $("a[id^='off_b']", self).remove();
                    var a_elms = $('a', self).filter(hide_filter);
                    a_elms.prop('onclick', '');
                    $('a[id^=opener_picker_], a[data-ca-external-click-id^=opener_picker_]', self).filter(hide_filter).each(function() {
                        $(this).remove();
                    });
                    $('.attach-images-alt', self).filter(hide_filter).remove();
                    $("tbody[id^='box_add_']", self).filter(hide_filter).remove();
                    var tmp_tr_box_add = $("tr[id^='box_add_']", self).filter(hide_filter);
                    tmp_tr_box_add.remove();
                    var aj_elms = $("[id$='_ajax_select_object']", self).filter(hide_filter)
                    aj_elms.each(function() {
                        var id = $(this).prop('id').replace(/_ajax_select_object/, '');
                        var aj_link = $('#sw_' + id + '_wrap_');
                        var aj_elm = aj_link.closest('.dropdown-toggle').parent();
                        aj_elm.wrap('<span class="shift-input">' + aj_link.html() + '</span>');
                        aj_elm.remove();
                        $(this).remove();
                    });
                    $('a.cm-delete-row', self).filter(hide_filter).each(function() {
                        $(this).remove();
                    });
                    $(self).removeClass('cm-sortable');
                    $('.cm-sortable-row', self).filter(hide_filter).removeClass('cm-sortable-row');
                    $('p.description', self).filter(hide_filter).remove();
                    $('a.cm-delete-image-link', self).filter(hide_filter).remove();
                    $('.action-add', self).filter(hide_filter).remove();
                    $('.cm-hide-with-inputs', self).filter(hide_filter).remove();
                });
            }
        },
        click: function(fn) {
            if (fn) {
                return this.on('click', fn);
            }
            $(this).each(function() {
                if (document.createEventObject) {
                    $(this).trigger('click');
                } else {
                    var evt_obj = document.createEvent('MouseEvents');
                    evt_obj.initEvent('click', true, true);
                    this.dispatchEvent(evt_obj);
                }
            });
            return this;
        },
        switchAvailability: function(flag, hide) {
            if (hide != true && hide != false) {
                hide = true;
            }
            if (flag == false || flag == true) {
                $(':input:not(.cm-skip-avail-switch)', this).prop('disabled', flag).toggleClass('disabled', flag);
                if (hide) {
                    this.toggle(!flag).toggleClass('hidden', flag);
                }
            } else {
                $(':input:not(.cm-skip-avail-switch)', this).each(function() {
                    var self = $(this);
                    var state = self.prop('disabled');
                    self.prop('disabled', !state);
                    self[state ? 'removeClass' : 'addClass']('disabled');
                });
                if (hide) {
                    this.toggleClass('hidden');
                }
            }
        },
        serializeObject: function() {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function() {
                if (typeof(o[this.name]) !== 'undefined' && this.name.indexOf('[]') > 0) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            var active_tab = this.find('.cm-j-tabs .active');
            if (typeof(active_tab) != 'undefined' && active_tab.length > 0) {
                o['active_tab'] = active_tab.prop('id');
            }
            return o;
        },
        positionElm: function(pos) {
            var elm = $(this);
            elm.css('position', 'absolute');
            var is_hidden = elm.is(':hidden');
            if (is_hidden) {
                elm.show();
            }
            elm.position(pos);
            if (is_hidden) {
                elm.hide();
            }
        }
    });
    String.prototype.str_replace = function(src, dst) {
        return this.toString().split(src).join(dst);
    };
    (function($) {
        $.ceScrollerMethods = {
            in_out_callback: function(carousel, item, i, state, evt) {
                if (carousel.allow_in_out_callback) {
                    if (carousel.options.autoDirection == 'next') {
                        carousel.add(i + carousel.options.item_count, $(item).html());
                        carousel.remove(i);
                    } else {
                        var last_item = $('li:last', carousel.list);
                        carousel.add(last_item.data('caJcarouselindex') - carousel.options.item_count, last_item.html());
                        carousel.remove(last_item.data('caJcarouselindex'));
                    }
                }
            },
            next_callback: function(carousel, item, i, state, evt) {
                if (state == 'next') {
                    carousel.add(i + carousel.options.item_count, $(item).html());
                    carousel.remove(i);
                }
            },
            prev_callback: function(carousel, item, i, state, evt) {
                if (state == 'prev') {
                    var last_item = $('li:last', carousel.list);
                    var item = last_item.html();
                    var count = last_item.data('caJcarouselindex') - carousel.options.item_count;
                    carousel.remove(last_item.data('caJcarouselindex'));
                    carousel.add(count, item);
                }
            },
            init_callback: function(carousel, state) {
                if (carousel.options.autoDirection == 'prev') {
                    var tmp = carousel.buttonNext;
                    carousel.buttonNext = carousel.buttonPrev;
                    carousel.buttonPrev = tmp;
                }
                $('.jcarousel-clip', carousel.container).height(carousel.options.clip_height + 'px');
                $('.jcarousel-clip', carousel.container).width(carousel.options.clip_width + 'px');
                var container_width = carousel.options.clip_width;
                carousel.container.width(container_width);
                if (container_width > carousel.container.width()) {
                    var p = carousel.pos(carousel.options.start, true);
                    carousel.animate(p, false);
                }
                carousel.clip.hover(function() {
                    carousel.stopAuto();
                }, function() {
                    carousel.startAuto();
                });
                if (!$.browser.msie || $.browser.version > 8) {
                    $(window).on('beforeunload', function() {
                        carousel.allow_in_out_callback = false;
                    });
                }
                if ($.browser.chrome) {
                    $.jcarousel.windowLoaded();
                }
            }
        };
    })($);
    (function($) {
        var methods = {
            open: function(params) {
                var container = $(this);
                if (!container.length) {
                    return false;
                }
                $('html').addClass('dialog-is-open');
                params = params || {};
                if (!container.hasClass('ui-dialog-content')) {
                    if (container.ceDialog('_load_content', params)) {
                        return false;
                    }
                    container.ceDialog('_init', params);
                } else if (params.view_id && container.data('caViewId') != params.view_id && container.ceDialog('_load_content', params)) {
                    return false;
                } else if (container.dialog('isOpen')) {
                    container.height('auto');
                    container.parent().height('auto');
                    methods._resize($(this));
                }
                if ($.browser.msie && params.width == 'auto') {
                    params.width = container.dialog('option', 'width');
                }
                if ($(".object-container", container).length == 0) {
                    container.wrapInner('<div class="object-container" />');
                }
                if (params) {
                    container.dialog('option', params);
                }
                $.popupStack.add({
                    name: container.prop('id'),
                    close: function() {
                        try {
                            container.dialog('close');
                        } catch (e) {}
                    }
                });
                if (_.isTouch == true) {
                    $.ui.dialog.prototype._focusTabbable = function() {};
                }
                var res = container.dialog('open');
                var s_elm = params.scroll ? $('#' + params.scroll, container) : false;
                if (s_elm && s_elm.length) {
                    $.scrollToElm(s_elm);
                }
                return res;
            },
            _is_empty: function() {
                var container = $(this);
                var content = $.trim(container.html());
                if (content) {
                    content = content.replace(/<!--(.*?)-->/g, '');
                }
                if (!$.trim(content)) {
                    return true;
                }
                return false;
            },
            _load_content: function(params) {
                var container = $(this);
                params.href = params.href || '';
                if (params.href && (container.ceDialog('_is_empty') || (params.view_id && container.data('caViewId') != params.view_id))) {
                    if (params.view_id) {
                        container.data('caViewId', params.view_id);
                    }
                    $.ceAjax('request', params.href, {
                        full_render: 0,
                        result_ids: container.prop('id'),
                        skip_result_ids_check: true,
                        callback: function() {
                            if (!container.ceDialog('_is_empty')) {
                                container.ceDialog('open', params);
                            }
                        }
                    });
                    return true;
                }
                return false;
            },
            close: function() {
                var container = $(this);
                container.data('close', true);
                container.dialog('close');
                $.popupStack.remove(container.prop('id'));
            },
            reload: function() {
                var d = $(this);
                d.dialog('option', {
                    show: 0,
                    hide: 0
                });
                if ($(this).dialog('option', 'destroyOnClose') === false) {
                    d.dialog('close');
                    d.dialog('open');
                }
                d.dialog('option', {
                    show: 150,
                    hide: 150
                });
            },
            resize: function() {
                methods._resize($(this));
            },
            change_title: function(title) {
                $(this).dialog('option', 'title', title);
            },
            destroy: function() {
                $.popupStack.remove($(this).prop('id'));
                try {
                    $(this).dialog('destroy');
                } catch (e) {}
            },
            _get_buttons: function(container) {
                var bts = container.find('.buttons-container');
                var elm = null;
                if (bts.length) {
                    var openers = container.find('.cm-dialog-opener');
                    if (openers.length) {
                        bts.each(function() {
                            var is_dl = false;
                            var bt = $(this);
                            openers.each(function() {
                                var dl_id = $(this).data('caTargetId');
                                if (bt.parents('#' + dl_id).length) {
                                    is_dl = true;
                                    return false;
                                }
                                return true;
                            });
                            if (!is_dl) {
                                elm = bt;
                            }
                            return true;
                        });
                    } else {
                        elm = container.find('.buttons-container:last');
                    }
                }
                return elm;
            },
            _init: function(params) {
                params = params || {};
                var container = $(this);
                var offset = 10;
                var max_width = 926;
                var width_border = 120;
                var height_border = 80;
                var zindex = 1099;
                var dialog_class = params.dialogClass || '';
                var ws = $.getWindowSizes();
                var container_parent = container.parent();
                if (params.height !== 'auto' && _.area == "A") {
                    params.height = (ws.view_height - height_border);
                }
                if (!container.find('form').length && !container.parents('.object-container').length && !container.data('caKeepInPlace')) {
                    params.keepInPlace = true;
                }
                if (!$.ui.dialog.overlayInstances) {
                    $.ui.dialog.overlayInstances = 1;
                }
                container.find('script[src]').remove();
                if ($.browser.msie && params.width == 'auto') {
                    if ($.browser.version < 8) {
                        container.appendTo(_.body);
                    }
                    params.width = container.outerWidth() + 10;
                }
                container.dialog({
                    title: params.title || null,
                    autoOpen: false,
                    draggable: false,
                    modal: true,
                    width: params.width || (ws.view_width > max_width ? max_width : ws.view_width - width_border),
                    height: params.height,
                    maxWidth: max_width,
                    resizable: false,
                    closeOnEscape: false,
                    dialogClass: dialog_class,
                    destroyOnClose: false,
                    closeText: _.tr('close'),
                    appendTo: params.keepInPlace ? container_parent : _.body,
                    show: 150,
                    hide: 150,
                    open: function(e, u) {
                        var d = $(this);
                        var w = d.dialog('widget');
                        w.find('.ui-dialog-titlebar-close').attr({
                            'data-dismiss': 'modal',
                            'type': 'button'
                        });
                        var _zindex = zindex;
                        if (stack.length) {
                            var prev = stack.pop();
                            d.dialog('option', 'position', {
                                my: 'left top',
                                at: 'left+' + (offset * 2) + ' top+' + offset,
                                of: $('#' + prev)
                            });
                            stack.push(prev);
                            _zindex = $('#' + prev).zIndex();
                        }
                        w.zIndex(++_zindex);
                        w.prev().zIndex(_zindex);
                        var elm_id = d.prop('id');
                        stack.push(elm_id);
                        if (!params.keepInPlace) {
                            if (stackInitedBody.indexOf(elm_id) == -1) {
                                stackInitedBody.push(elm_id);
                            }
                        }
                        methods._resize(d);
                        $('html').addClass('dialog-is-open');
                        $.ceEvent('trigger', 'ce.dialogshow', [d]);
                        $('textarea.cm-wysiwyg', d).ceEditor('recover');
                        if (params.switch_avail) {
                            d.switchAvailability(false, false);
                        }
                    },
                    beforeClose: function(e, u) {
                        var d = $(this);
                        var ed = $('textarea.cm-wysiwyg', d);
                        if (ed) {
                            ed.each(function() {
                                $(this).ceEditor('destroy');
                            });
                        }
                        var container = d.find('.object-container');
                        var non_closable = params.nonClosable || false;
                        var buttonsElm = methods._get_buttons(d);
                        container.height('auto');
                        d.parent().height('auto');
                        if (buttonsElm) {
                            buttonsElm.css({
                                position: 'static'
                            });
                        }
                        $('textarea.cm-wysiwyg', d).ceEditor('destroy');
                        if (non_closable && !d.data('close')) {
                            return false;
                        }
                        stack.pop();
                        if (params.switch_avail) {
                            d.switchAvailability(true, false);
                        }
                    },
                    close: function(e, u) {
                        if ($(this).dialog('option', 'destroyOnClose')) {
                            $(this).dialog('destroy').remove();
                        }
                        setTimeout(function() {
                            if ($('.ui-widget-overlay').length == 0) {
                                $('html').removeClass('dialog-is-open');
                            }
                        }, 50);
                    }
                });
            },
            _resize: function(d) {
                var buttonsElm = methods._get_buttons(d);
                var optionsElm = d.find('.cm-picker-options-container');
                var container = d.find('.object-container');
                var max_height = $.getWindowSizes().view_height;
                var buttonsHeight = 0;
                var optionsHeight = 0;
                var containerHeight = 0;
                var dialogHeight = d.parent().outerHeight(true);
                var titleHeight = d.parent().find('.ui-dialog-titlebar').outerHeight();
                if (buttonsElm) {
                    buttonsElm.addClass('buttons-container-picker');
                    buttonsHeight = buttonsElm.outerHeight(true);
                }
                if (optionsElm.length) {
                    optionsHeight = optionsElm.outerHeight(true);
                }
                if (dialogHeight > max_height) {
                    d.parent().outerHeight(max_height);
                }
                containerHeight = d.parent().outerHeight() - titleHeight;
                if (buttonsElm && _.area == "C") {
                    if (dialogHeight >= max_height) {
                        containerHeight = containerHeight - buttonsHeight;
                        buttonsElm.css({
                            position: 'absolute',
                            bottom: -buttonsHeight
                        });
                    } else {
                        buttonsElm.css({
                            position: 'absolute',
                            bottom: 0
                        });
                    }
                } else if (buttonsElm && _.area == "A") {
                    containerHeight = containerHeight - buttonsHeight;
                    buttonsElm.css({
                        position: 'absolute',
                        bottom: 0,
                        left: 0,
                        right: 0
                    });
                }
                container.outerHeight(containerHeight);
                if (optionsHeight) {
                    optionsElm.positionElm({
                        my: 'left top',
                        at: 'left bottom',
                        of: container,
                        collision: 'none'
                    });
                    optionsElm.css('width', container.outerWidth());
                }
            }
        };
        var stack = [];
        var stackInitedBody = [];
        var tmpCont;
        $.fn.ceDialog = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === 'object' || !method) {
                return methods._init.apply(this, arguments);
            } else {
                $.error('ty.dialog: method ' + method + ' does not exist');
            }
        };
        $.ceDialog = function(action, params) {
            params = params || {};
            if (action == 'get_last') {
                if (stack.length == 0) {
                    return $();
                }
                var dlg = $('#' + stack[stack.length - 1]);
                return params.getWidget ? dlg.dialog('widget') : dlg;
            } else if (action == 'fit_elements') {
                var jelm = params.jelm;
                if (jelm.parents('.cm-picker-options-container').length) {
                    $.ceDialog('get_last').data('dialog')._trigger('resize');
                }
            } else if (action == 'reload_parent') {
                var jelm = params.jelm;
                var dlg = jelm.closest('.ui-dialog-content');
                var container = $('.object-container', dlg);
                if (!container.length) {
                    dlg.wrapInner('<div class="object-container" />');
                }
                if (dlg.length && dlg.is(':visible')) {
                    var scrollPosition = container.scrollTop();
                    dlg.ceDialog('reload');
                    container.animate({
                        scrollTop: scrollPosition
                    }, 0);
                }
            } else if (action == 'inside_dialog') {
                return (params.jelm.closest('.ui-dialog-content').length != 0);
            } else if (action == 'get_params') {
                var dialog_params = {
                    keepInPlace: params.hasClass('cm-dialog-keep-in-place'),
                    nonClosable: params.hasClass('cm-dialog-non-closable'),
                    scroll: params.data('caScroll') ? params.data('caScroll') : ''
                };
                if (params.prop('href')) {
                    dialog_params['href'] = params.prop('href');
                }
                if (params.hasClass('cm-dialog-auto-size')) {
                    dialog_params['width'] = 'auto';
                    dialog_params['height'] = 'auto';
                } else if (params.hasClass('cm-dialog-auto-width')) {
                    dialog_params['width'] = 'auto';
                }
                if (params.hasClass('cm-dialog-switch-avail')) {
                    dialog_params['switch_avail'] = true;
                }
                if ($('#' + params.data('caTargetId')).length == 0) {
                    var title = params.data('caDialogTitle') ? params.data('caDialogTitle') : params.prop('title');
                    $('<div class="hidden" title="' + title + '" id="' + params.data('caTargetId') + '"><!--' + params.data('caTargetId') + '--></div>').appendTo(_.body);
                }
                if (params.prop('href') && params.data('caViewId')) {
                    dialog_params['view_id'] = params.data('caViewId');
                }
                if (params.data('caDialogClass')) {
                    dialog_params['dialogClass'] = params.data('caDialogClass');
                }
                return dialog_params;
            } else if (action == 'clear_stack') {
                $.popupStack.clear_stack();
                return stack = [];
            } else if (action == 'destroy_loaded') {
                var content = $('<div>').html(params.content);
                $.each(stackInitedBody, function(i, id) {
                    if (content.find('#' + id).length) {
                        $('#' + id).ceDialog('destroy');
                    }
                });
            }
        };
        $.extend({
            popupStack: {
                stack: [],
                add: function(params) {
                    return this.stack.push(params);
                },
                remove: function(name) {
                    var position = this.stack.indexOf(name);
                    if (position != -1) {
                        return this.stack.splice(position, 1);
                    }
                },
                last_close: function() {
                    var obj = this.stack.pop();
                    if (obj && obj.close) {
                        obj.close();
                        return true;
                    }
                    return false;
                },
                last: function() {
                    return this.stack[this.stack.length - 1];
                },
                close: function(name) {
                    var position = this.stack.indexOf(name);
                    if (position != -1) {
                        var object = this.stack.splice(position, 1)[0];
                        if (object.close) {
                            object.close();
                        }
                        return true;
                    }
                    return false;
                },
                clear_stack: function() {
                    return this.stack = [];
                }
            }
        });
    })($);
    (function($) {
        var methods = {
            init: function(params) {
                params = params || {};
                params.heightStyle = "content";
                var container = $(this);
                container.accordion(params);
            },
            reinit: function(params) {
                $(this).accordion(params);
            }
        };
        $.fn.ceAccordion = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === 'object' || !method) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('ty.accordion: method ' + method + ' does not exist');
            }
        };
        $.ceAccordion = function(method, params) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else {
                $.error('ty.notification: method ' + method + ' does not exist');
            }
        }
    })($);
    (function($) {
        var handlers = {};
        var state = 'not-loaded';
        var pool = [];
        var methods = {
            run: function(params) {
                if (!this.length) {
                    return false;
                }
                if ($.ceEditor('state') == 'loading') {
                    $.ceEditor('push', this);
                } else {
                    $.ceEditor('run', this, params);
                }
            },
            destroy: function() {
                if (!this.length || $.ceEditor('state') != 'loaded') {
                    return false;
                }
                $.ceEditor('destroy', this);
            },
            recover: function() {
                if (!this.length || $.ceEditor('state') != 'loaded') {
                    return false;
                }
                $.ceEditor('recover', this);
            },
            val: function(value) {
                if (!this.length || $.ceEditor('state') != 'loaded') {
                    return false;
                }
                return $.ceEditor('val', this, value);
            },
            disable: function(value) {
                if (!this.length || $.ceEditor('state') != 'loaded') {
                    return false;
                }
                $.ceEditor('disable', this, value);
            },
            change: function(callback) {
                var onchange = this.data('ceeditor_onchange') || [];
                onchange.push(callback);
                this.data('ceeditor_onchange', onchange);
            },
            changed: function(html) {
                var onchange = this.data('ceeditor_onchange') || [];
                for (var i = 0; i < onchange.length; i++) {
                    onchange[i](html);
                };
            }
        };
        $.fn.ceEditor = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === 'object' || !method) {
                return methods.run.apply(this, arguments);
            } else {
                $.error('ty.editor: method ' + method + ' does not exist');
            }
        };
        $.ceEditor = function(action, data, params) {
            if (action == 'push') {
                if (data) {
                    pool.push(data);
                } else {
                    return pool.unshift();
                }
            } else if (action == 'state') {
                if (data) {
                    state = data;
                    if (data == 'loaded' && pool.length) {
                        for (var i = 0; i < pool.length; i++) {
                            pool[i].ceEditor('run', params);
                        }
                        pool = [];
                    }
                } else {
                    return state;
                }
            } else if (action == 'handlers') {
                handlers = data;
            } else if (action == 'run' || action == 'destroy' || action == 'updateTextFields' || action == 'recover' || action == 'val' || action == 'disable') {
                return handlers[action](data, params);
            }
        }
    })($);
    (function($) {
        var methods = {
            display: function() {
                $.cePreviewer('display', this);
            }
        };
        $.fn.cePreviewer = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === 'object' || !method) {
                return methods.run.apply(this, arguments);
            } else {
                $.error('ty.previewer: method ' + method + ' does not exist');
            }
        };
        $.cePreviewer = function(action, data) {
            if (action == 'handlers') {
                this.handlers = data;
            } else if (action == 'display') {
                return this.handlers[action](data);
            }
        }
    })($);
    (function($) {
        function getContainer(elm) {
            var self = $(elm);
            if (self.length == 0) {
                return false;
            }
            var comet_container_id = self.prop('href').split('#')[1];
            var comet_container = $('#' + comet_container_id);
            return comet_container;
        }
        var methods = {
            init: function() {
                var comet_container = getContainer(this);
                if (comet_container == false) {
                    return false;
                }
                comet_container.find('.bar').css('width', 0).prop('data-percentage', 0);
                this.trigger('click');
                this.data('ceProgressbar', true);
                $.ceEvent('trigger', 'ce.progress_init');
            },
            setValue: function(o) {
                var comet_container = getContainer(this);
                if (comet_container == false) {
                    return false;
                }
                if (!this.data('ceProgressbar')) {
                    this.ceProgress('init');
                }
                if (o.progress) {
                    comet_container.find('.bar').css('width', o.progress + '%').prop('data-percentage', o.progress);
                }
                if (o.text) {
                    comet_container.find('.modal-body p').html(o.text);
                }
                $.ceEvent('trigger', 'ce.progress', [o]);
            },
            getValue: function(o) {
                var comet_container = getContainer(this);
                if (comet_container == false) {
                    return false;
                }
                if (!this.data('ceProgressbar')) {
                    return 0;
                }
                return parseInt(comet_container.find('.bar').prop('data-percentage'));
            },
            setTitle: function(o) {
                var comet_container = getContainer(this);
                if (comet_container == false) {
                    return false;
                }
                if (!this.data('ceProgressbar')) {
                    this.ceProgress('init');
                }
                if (o.title) {
                    $('#comet_title').text(o.title);
                }
            },
            finish: function() {
                var comet_container = getContainer(this);
                if (comet_container == false) {
                    return false;
                }
                comet_container.find('.bar').css('width', 100).prop('data-percentage', 100);
                comet_container.modal('hide');
                this.removeData('ceProgressbar');
                $.ceEvent('trigger', 'ce.progress_finish');
            }
        };
        $.fn.ceProgress = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === 'object' || !method) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('ty.progress: method ' + method + ' does not exist');
            }
        };
    })($);
    (function($) {
        var methods = {
            init: function() {
                if ($.history) {
                    $.history.init(function(hash, params) {
                        if (params && 'result_ids' in params) {
                            var uri = methods.parseHash('#' + hash);
                            var href = uri.indexOf(_.current_location) != -1 ? uri : _.current_location + '/' + uri;
                            var target_id = params.result_ids;
                            var a_elm = $('a[data-ca-target-id="' + target_id + '"]:first');
                            var name = a_elm.prop('name');
                            $.ceAjax('request', href, {
                                full_render: params.full_render,
                                result_ids: target_id,
                                caching: false,
                                obj: a_elm,
                                skip_history: true,
                                callback: 'ce.ajax_callback_' + name
                            });
                        } else if (_.embedded) {
                            var url = fn_url(window.location.href);
                            if (url != _.current_url) {
                                $.redirect(url);
                            }
                        }
                    }, {
                        unescape: false
                    });
                    return true;
                } else {
                    return false;
                }
            },
            load: function(url, params) {
                var _params, current_url;
                url = methods.prepareHash(url);
                current_url = methods.prepareHash(_.current_url);
                _params = {
                    result_ids: params.result_ids,
                    full_render: params.full_render
                }
                $.ceEvent('trigger', 'ce.history_load', [url]);
                $.history.reload(current_url, _params);
                $.history.load(url, _params);
            },
            prepareHash: function(url) {
                url = unescape(url);
                if (url.indexOf('://') !== -1) {
                    if ($.browser.msie && $.browser.version >= 9) {
                        url = _.current_path + '/' + url.str_replace(_.current_location + '/', '');
                    } else {
                        url = url.str_replace(_.current_location + '/', '');
                    }
                }
                url = fn_query_remove(url, ['result_ids']);
                url = '!/' + url;
                return url;
            },
            parseHash: function(hash) {
                if (hash.indexOf('%') !== -1) {
                    hash = unescape(hash);
                }
                if (hash.indexOf('#!') != -1) {
                    var parts = hash.split('#!/');
                    return parts[1] || '';
                }
                return '';
            }
        };
        $.ceHistory = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else {
                $.error('ty.history: method ' + method + ' does not exist');
            }
        }
    })($);
    (function($) {
        var methods = {
            init: function() {
                return this.each(function() {
                    var elm = $(this);
                    elm.bind({
                        click: function() {
                            $(this).ceHint('_check_hint');
                        },
                        focus: function() {
                            $(this).ceHint('_check_hint');
                        },
                        focusin: function() {
                            $(this).ceHint('_check_hint');
                        },
                        blur: function() {
                            $(this).ceHint('_check_hint_focused');
                        },
                        focusout: function() {
                            $(this).ceHint('_check_hint_focused');
                        }
                    });
                    elm.addClass('cm-hint-focused');
                    elm.removeClass('cm-hint');
                    elm.ceHint('_check_hint_focused');
                });
            },
            is_hint: function() {
                return $(this).hasClass('cm-hint') && ($(this).val() == $(this).ceHint('_get_hint_value'));
            },
            _check_hint: function() {
                var elm = $(this);
                if (elm.ceHint('is_hint')) {
                    elm.addClass('cm-hint-focused');
                    elm.val('');
                    elm.removeClass('cm-hint');
                    elm.prop('name', elm.prop('name').str_replace('hint_', ''));
                }
            },
            _check_hint_focused: function() {
                var elm = $(this);
                if (elm.hasClass('cm-hint-focused')) {
                    if (elm.val() == '' || (elm.val() == elm.ceHint('_get_hint_value'))) {
                        elm.addClass('cm-hint');
                        elm.removeClass('cm-hint-focused');
                        elm.val(elm.ceHint('_get_hint_value'));
                        elm.prop('name', 'hint_' + elm.prop('name'));
                    }
                }
            },
            _get_hint_value: function() {
                return ($(this).prop('title') != '') ? $(this).prop('title') : $(this).prop('defaultValue');
            }
        };
        $.fn.ceHint = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === 'object' || !method) {
                return methods.run.apply(this, arguments);
            } else {
                $.error('ty.hint: method ' + method + ' does not exist');
            }
        };
    })($);
    (function($) {
        var methods = {
            init: function(params) {
                return this.each(function() {
                    var params = params || {};
                    var update_text = _.tr('text_position_updating');
                    var self = $(this);
                    var table = self.data('caSortableTable');
                    var id_name = self.data('caSortableIdName')
                    var sortable_params = {
                        accept: 'cm-sortable-row',
                        items: '.cm-row-item',
                        tolerance: 'pointer',
                        axis: 'y',
                        containment: 'parent',
                        opacity: '0.9',
                        update: function(event, ui) {
                            var positions = [],
                                ids = [];
                            var container = $(ui.item).closest('.cm-sortable');
                            $('.cm-row-item', container).each(function() {
                                var matched = $(this).prop('class').match(/cm-sortable-id-([^\s]+)/i);
                                var index = $(this).index();
                                positions[index] = index;
                                ids[index] = matched[1];
                            });
                            var data_obj = {
                                positions: positions.join(','),
                                ids: ids.join(',')
                            };
                            $.ceAjax('request', fn_url('tools.update_position?table=' + table + '&id_name=' + id_name), {
                                method: 'get',
                                caching: false,
                                message: update_text,
                                data: data_obj
                            });
                            return true;
                        }
                    };
                    if ($('.cm-sortable-handle', self).length) {
                        sortable_params = $.extend(sortable_params, {
                            opacity: '0.5',
                            handle: '.cm-sortable-handle'
                        });
                    }
                    self.sortable(sortable_params);
                });
            }
        };
        $.fn.ceSortable = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === 'object' || !method) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('ty.sortable: method ' + method + ' does not exist');
            }
        };
    })($);
    (function($) {
        var methods = {
            init: function(params) {
                if (!$(this).length) {
                    return false;
                }
                if (!$.fn.spectrum) {
                    var elms = $(this);
                    $.loadCss(['js/lib/spectrum/spectrum.css'], false, true);
                    $.getScript('js/lib/spectrum/spectrum.js', function() {
                        elms.ceColorpicker();
                    });
                    return false;
                }
                var palette = [
                    ["#000000", "#434343", "#666666", "#999999", "#b7b7b7", "#cccccc", "#d9d9d9", "#efefef", "#f3f3f3", "#ffffff"],
                    ["#980000", "#ff0000", "#ff9900", "#ffff00", "#00ff00", "#00ffff", "#4a86e8", "#0000ff", "#9900ff", "#ff00ff"],
                    ["#e6b8af", "#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#c9daf8", "#cfe2f3", "#d9d2e9", "#ead1dc"],
                    ["#dd7e6b", "#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#a4c2f4", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
                    ["#cc4125", "#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6d9eeb", "#6fa8dc", "#8e7cc3", "#c27ba0"],
                    ["#a61c00", "#cc0000", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3c78d8", "#3d85c6", "#674ea7", "#a64d79"],
                    ["#85200c", "#990000", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#1155cc", "#0b5394", "#351c75", "#741b47"],
                    ["#5b0f00", "#660000", "#783f04", "#7f6000", "#274e13", "#0c343d", "#1c4587", "#073763", "#20124d", "#4c1130"]
                ];
                return this.each(function() {
                    var jelm = $(this);
                    var params = {
                        showInput: true,
                        showInitial: false,
                        showPalette: false,
                        showSelectionPalette: false,
                        palette: palette,
                        preferredFormat: 'hex6',
                        beforeShow: function() {
                            jelm.spectrum('option', 'showPalette', true);
                            jelm.spectrum('option', 'showInitial', true);
                            jelm.spectrum('option', 'showSelectionPalette', true);
                        },
                        hide: function() {
                            $.ceEvent('trigger', 'ce.colorpicker.hide');
                        },
                        show: function() {
                            $.ceEvent('trigger', 'ce.colorpicker.show');
                        }
                    };
                    if (jelm.data('caView') && jelm.data('caView') == 'palette') {
                        params.showPaletteOnly = true;
                    }
                    if (jelm.data('caStorage')) {
                        params.localStorageKey = jelm.data('caStorage');
                    }
                    jelm.spectrum(params);
                    jelm.spectrum('container').appendTo(jelm.parent());
                });
            },
            reset: function() {
                this.spectrum('set', this.val());
            },
            set: function(val) {
                this.spectrum('set', val);
            }
        };
        $.fn.ceColorpicker = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === 'object' || !method) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('ty.colorpicker: method ' + method + ' does not exist');
            }
        };
    })($);
    (function($) {
        var clicked_elm;
        var zipcode_regexp = {};
        var regexp = {};
        var validators = [];

        function _fillRequirements(form, check_filter) {
            var lbl, lbls, id, elm, requirements = {};
            if (check_filter) {
                lbls = $(check_filter, form).find('label');
            } else {
                lbls = $('label', form);
            }
            for (var k = 0; k < lbls.length; k++) {
                lbl = $(lbls[k]);
                id = lbl.prop('for');
                if (!id || !lbl.prop('class') || !id.match(/^([a-z0-9-_]+)$/)) {
                    continue;
                }
                elm = $('#' + id);
                if (elm.length && !elm.prop('disabled')) {
                    requirements[id] = {
                        elm: elm,
                        lbl: lbl
                    };
                }
            }
            return requirements;
        }

        function _checkFields(form, requirements, only_check) {
            var set_mark, elm, lbl, container, _regexp, _message;
            var message_set = false;
            $('.cm-failed-field', form).removeClass('cm-failed-field');
            errors = {};
            for (var elm_id in requirements) {
                set_mark = false;
                elm = requirements[elm_id].elm;
                lbl = requirements[elm_id].lbl;
                if (lbl.hasClass('cm-trim')) {
                    elm.val($.trim(elm.val()));
                }
                if (lbl.hasClass('cm-email')) {
                    if ($.is.email(elm.val()) == false) {
                        if (lbl.hasClass('cm-required') || $.is.blank(elm.val()) == false) {
                            _formMessage(_.tr('error_validator_email'), lbl);
                            set_mark = true;
                        }
                    }
                }
                if (lbl.hasClass('cm-color')) {
                    if ($.is.color(elm.val()) == false) {
                        if (lbl.hasClass('cm-required') || $.is.blank(elm.val()) == false) {
                            _formMessage(_.tr('error_validator_color'), lbl);
                            set_mark = true;
                        }
                    }
                }
                if (lbl.hasClass('cm-phone')) {
                    if ($.is.phone(elm.val()) != true) {
                        if (lbl.hasClass('cm-required') || $.is.blank(elm.val()) == false) {
                            _formMessage(_.tr('error_validator_phone'), lbl);
                            set_mark = true;
                        }
                    }
                }
                if (lbl.hasClass('cm-zipcode')) {
                    var loc = lbl.prop('class').match(/cm-location-([^\s]+)/i)[1] || '';
                    var country = $('.cm-country' + (loc ? '.cm-location-' + loc : ''), form).val();
                    var val = elm.val();
                    if (zipcode_regexp[country] && !elm.val().match(zipcode_regexp[country]['regexp'])) {
                        if (lbl.hasClass('cm-required') || $.is.blank(elm.val()) == false) {
                            _formMessage(_.tr('error_validator_zipcode'), lbl, null, zipcode_regexp[country]['format']);
                            set_mark = true;
                        }
                    }
                }
                if (lbl.hasClass('cm-integer')) {
                    if ($.is.integer(elm.val()) == false) {
                        if (lbl.hasClass('cm-required') || $.is.blank(elm.val()) == false) {
                            _formMessage(_.tr('error_validator_integer'), lbl);
                            set_mark = true;
                        }
                    }
                }
                if (lbl.hasClass('cm-multiple') && elm.prop('length') == 0) {
                    _formMessage(_.tr('error_validator_multiple'), lbl);
                    set_mark = true;
                }
                if (lbl.hasClass('cm-password')) {
                    var pair_lbl = $('label.cm-password', form).not(lbl);
                    var pair_elm = $('#' + pair_lbl.prop('for'));
                    if (elm.val() && elm.val() != pair_elm.val()) {
                        _formMessage(_.tr('error_validator_password'), lbl, pair_lbl);
                        set_mark = true;
                    }
                }
                if (validators) {
                    for (var i = 0; i < validators.length; i++) {
                        if (lbl.hasClass(validators[i].class_name)) {
                            result = validators[i].func(elm_id);
                            if (result != true) {
                                _formMessage(validators[i].message, lbl);
                                set_mark = true;
                            }
                        }
                    }
                }
                if (lbl.hasClass('cm-regexp')) {
                    _regexp = null;
                    _message = null;
                    if (elm_id in regexp) {
                        _regexp = regexp[elm_id]['regexp'];
                        _message = regexp[elm_id]['message'] ? regexp[elm_id]['message'] : _.tr('error_validator_message');
                    } else if (lbl.data('caRegexp')) {
                        _regexp = lbl.data('caRegexp');
                        _message = lbl.data('caMessage');
                    }
                    if (_regexp && !elm.ceHint('is_hint')) {
                        var val = elm.val();
                        var expr = new RegExp(_regexp);
                        var result = expr.test(val);
                        if (!result && !(!lbl.hasClass('cm-required') && elm.val() == '')) {
                            _formMessage(_message, lbl);
                            set_mark = true;
                        }
                    }
                }
                if (lbl.hasClass('cm-multiple-checkboxes') || lbl.hasClass('cm-multiple-radios')) {
                    if (lbl.hasClass('cm-required')) {
                        var el_filter = lbl.hasClass('cm-multiple-checkboxes') ? '[type=checkbox]' : '[type=radio]';
                        if ($(el_filter + ':not(:disabled)', elm).length && !$(el_filter + ':checked', elm).length) {
                            _formMessage(_.tr('error_validator_required'), lbl);
                            set_mark = true;
                        }
                    }
                }
                if (lbl.hasClass('cm-all')) {
                    if (elm.prop('length') == 0 && lbl.hasClass('cm-required')) {
                        _formMessage(_.tr('error_validator_multiple'), lbl);
                        set_mark = true;
                    } else {
                        $('option', elm).prop('selected', true);
                    }
                } else {
                    if (elm.is(':input')) {
                        if (lbl.hasClass('cm-required') && ((elm.is('[type=checkbox]') && !elm.prop('checked')) || $.is.blank(elm.val()) == true || elm.ceHint('is_hint'))) {
                            _formMessage(_.tr('error_validator_required'), lbl);
                            set_mark = true;
                        }
                    }
                }
                container = elm.closest('.cm-field-container');
                if (container.length) {
                    elm = container;
                }
                if (!only_check) {
                    $('[id="' + elm_id + '_error_message"].help-inline', elm.parent()).remove();
                    if (set_mark == true) {
                        lbl.parent().addClass('error has-error');
                        elm.addClass('cm-failed-field');
                        lbl.addClass('cm-failed-label');
                        if (!elm.hasClass('cm-no-failed-msg')) {
                            elm.parent().append('<span id="' + elm_id + '_error_message" class="help-inline help-block">' + _getMessage(elm_id) + '</span>');
                        }
                        if (!message_set) {
                            $.scrollToElm(elm);
                            message_set = true;
                        }
                        var dlg = $.ceDialog('get_last');
                        var dlg_target = $('.cm-dialog-auto-size[data-ca-target-id="' + dlg.attr('id') + '"]');
                        if (dlg_target.length) {
                            dlg.ceDialog('reload');
                        }
                    } else {
                        lbl.parent().removeClass('error has-error');
                        elm.removeClass('cm-failed-field');
                        lbl.removeClass('cm-failed-label');
                    }
                } else {
                    if (set_mark) {
                        message_set = true;
                    }
                }
            }
            return !message_set;
        }

        function _disableEmptyFields(form) {
            var selector = [];
            if (form.hasClass('cm-disable-empty')) {
                selector.push('input[type=text]');
            }
            if (form.hasClass('cm-disable-empty-files')) {
                selector.push('input[type=file]');
                $('input[type=file][data-ca-empty-file=""]', form).prop('disabled', true);
            }
            if (selector.length) {
                $(selector.join(','), form).each(function() {
                    var self = $(this);
                    if (self.val() == '') {
                        self.prop('disabled', true);
                        self.addClass('cm-disabled')
                    }
                });
            }
        }

        function _check(form, params) {
            var form_result = true;
            var check_fields_result = true;
            var h;
            params = params || {};
            params.only_check = params.only_check || false;
            if (!clicked_elm) {
                if ($('[type=submit]', form).length) {
                    clicked_elm = $('[type=submit]:first', form);
                } else if ($('input[type=image]', form).length) {
                    clicked_elm = $('input[type=image]:first', form);
                }
            }
            if (!clicked_elm.hasClass('cm-skip-validation')) {
                var requirements = _fillRequirements(form, clicked_elm.data('caCheckFilter'));
                if ($.ceEvent('trigger', 'ce.formpre_' + form.prop('name'), [form, clicked_elm]) === false) {
                    form_result = false;
                }
                check_fields_result = _checkFields(form, requirements, params.only_check);
            }
            if (params.only_check) {
                return check_fields_result && form_result;
            }
            if (check_fields_result && form_result) {
                _disableEmptyFields(form);
                form.find('.cm-numeric').each(function() {
                    var val = $(this).autoNumeric('get');
                    $(this).prop('value', val);
                });
                h = clicked_elm.data('original_element') ? clicked_elm.data('original_element') : clicked_elm;
                if (h.data('clicked') == true) {
                    return false;
                }
                h.data('clicked', true);
                if ((form.hasClass('cm-ajax') || clicked_elm.hasClass('cm-ajax')) && !clicked_elm.hasClass('cm-no-ajax')) {
                    $.ceEvent('one', 'ce.ajaxdone', function() {
                        h.data('clicked', false);
                    });
                }
                if (clicked_elm.hasClass('cm-comet')) {
                    $.ceEvent('one', 'ce.cometdone', function() {
                        h.data('clicked', false);
                    });
                }
                if (clicked_elm.hasClass('cm-new-window')) {
                    form.prop('target', '_blank');
                    setTimeout(function() {
                        h.data('clicked', false);
                    }, 1000);
                    return true;
                } else if (clicked_elm.hasClass('cm-parent-window')) {
                    form.prop('target', '_parent');
                    return true;
                } else {
                    form.prop('target', '_self');
                }
                if ($.ceEvent('trigger', 'ce.formpost_' + form.prop('name'), [form, clicked_elm]) === false) {
                    form_result = false;
                }
                if (clicked_elm.closest('.cm-dialog-closer').length) {
                    $.ceDialog('get_last').ceDialog('close');
                }
                if ((form.hasClass('cm-ajax') || clicked_elm.hasClass('cm-ajax')) && !clicked_elm.hasClass('cm-no-ajax')) {
                    var collection = form.add(clicked_elm);
                    if (collection.hasClass('cm-form-dialog-closer') || collection.hasClass('cm-form-dialog-opener')) {
                        $.ceEvent('one', 'ce.formajaxpost_' + form.prop('name'), function(response_data, params) {
                            if (collection.hasClass('cm-form-dialog-closer')) {
                                $.popupStack.last_close();
                            }
                            if (collection.hasClass('cm-form-dialog-opener')) {
                                var _id = form.find('input[name=result_ids]').val();
                                if (_id && typeof(response_data.html) !== "undefined") {
                                    $('#' + _id).ceDialog('open', $.ceDialog('get_params', form));
                                }
                            }
                        });
                    }
                    return $.ceAjax('submitForm', form, clicked_elm);
                }
                if (clicked_elm.hasClass('cm-no-ajax')) {
                    $('input[name=is_ajax]', form).remove();
                }
                if (_.embedded && form_result == true && !$.externalLink(form.prop('action'))) {
                    form.append('<input type="hidden" name="result_ids" value="' + _.container + '" />');
                    clicked_elm.data('caScroll', '#' + _.container);
                    return $.ceAjax('submitForm', form, clicked_elm);
                }
                if (form_result == false) {
                    h.data('clicked', false);
                }
                return form_result;
            } else if (check_fields_result == false) {
                var hidden_tab = $('.cm-failed-field', form).parents('[id^="content_"]:hidden');
                if (hidden_tab.length && $('.cm-failed-field', form).length == $('.cm-failed-field', hidden_tab).length) {
                    $('#' + hidden_tab.prop('id').str_replace('content_', '')).click();
                }
            }
            return false;
        }

        function _formMessage(msg, field, field2, extra) {
            var id = field.prop('for');
            if (errors[id]) {
                return false;
            }
            errors[id] = [];
            msg = msg.str_replace('[field]', _fieldTitle(field));
            if (field2) {
                msg = msg.str_replace('[field2]', _fieldTitle(field2));
            }
            if (extra) {
                msg = msg.str_replace('[extra]', extra);
            }
            errors[id].push(msg);
        };

        function _fieldTitle(field) {
            return field.text().replace(/(\s*\(\?\))?:\s*$/, '');
        }

        function _getMessage(id) {
            return '<p>' + errors[id].join('</p><p>') + '</p>';
        };
        var methods = {
            init: function() {
                var form = $(this);
                form.on('submit', function(e) {
                    return _check(form);
                })
            },
            setClicked: function(elm) {
                clicked_elm = elm;
            },
            check: function() {
                var form = $(this);
                return _check(form, {
                    only_check: true
                });
            }
        }
        $.fn.ceFormValidator = function(method) {
            var args = arguments;
            var result;
            $(this).each(function(i, elm) {
                var errors = {};
                if (methods[method]) {
                    result = methods[method].apply(this, Array.prototype.slice.call(args, 1));
                } else if (typeof method === 'object' || !method) {
                    result = methods.init.apply(this, args);
                } else {
                    $.error('ty.formvalidator: method ' + method + ' does not exist');
                }
            });
            return result;
        };
        $.ceFormValidator = function(action, params) {
            params = params || {};
            if (action == 'setZipcode') {
                zipcode_regexp = params;
            } else if (action == 'setRegexp') {
                if ('console' in window) {
                    console.log('This method is deprecated, use data-attributes "data-ca-regexp" and "data-ca-message" instead');
                }
                regexp = $.extend(regexp, params);
            } else if (action == 'registerValidator') {
                validators.push(params);
            } else if (action == 'check') {
                if (params.form) {
                    return methods.check.apply(params.form);
                }
            }
        }
    })($);
    (function($) {
        var options = {};
        var init = false;

        function _rebuildStates(section, elm) {
            elm = elm || $('.cm-state.cm-location-' + section).prop('id');
            var sbox = $('#' + elm).is('select') ? $('#' + elm) : $('#' + elm + '_d');
            var inp = $('#' + elm).is('input') ? $('#' + elm) : $('#' + elm + '_d');
            var default_state = inp.val();
            var cntr = $('.cm-country.cm-location-' + section);
            var cntr_disabled;
            if (cntr.length) {
                cntr_disabled = cntr.prop('disabled');
            } else {
                cntr_disabled = sbox.prop('disabled');
            }
            var country_code = (cntr.length) ? cntr.val() : options.default_country;
            var tag_switched = false;
            var pkey = '';
            sbox.prop('id', elm).prop('disabled', false).removeClass('hidden cm-skip-avail-switch');
            inp.prop('id', elm + '_d').prop('disabled', true).addClass('hidden cm-skip-avail-switch').val('');
            if (!inp.hasClass('disabled')) {
                sbox.removeClass('disabled');
            }
            if (options.states && options.states[country_code]) {
                sbox.prop('length', 1);
                for (var i = 0; i < options.states[country_code].length; i++) {
                    sbox.append('<option value="' + options.states[country_code][i]['code'] + '"' + (options.states[country_code][i]['code'] == default_state ? ' selected' : '') + '>' + options.states[country_code][i]['state'] + '</option>');
                }
                sbox.prop('id', elm).prop('disabled', false).removeClass('cm-skip-avail-switch');
                inp.prop('id', elm + '_d').prop('disabled', true).addClass('cm-skip-avail-switch');
                if (!inp.hasClass('disabled')) {
                    sbox.removeClass('disabled');
                }
            } else {
                sbox.prop('id', elm + '_d').prop('disabled', true).addClass('hidden cm-skip-avail-switch');
                inp.prop('id', elm).prop('disabled', false).removeClass('hidden cm-skip-avail-switch').val(default_state);
                if (!sbox.hasClass('disabled')) {
                    inp.removeClass('disabled');
                }
            }
            if (cntr_disabled == true) {
                sbox.prop('disabled', true);
                inp.prop('disabled', true);
            }
        }

        function _rebuildStatesInLocation() {
            var location_elm = $(this).prop('class').match(/cm-location-([^\s]+)/i);
            if (location_elm) {
                _rebuildStates(location_elm[1], $('.cm-state.cm-location-' + location_elm[1]).not(':disabled').prop('id'));
            }
        }
        var methods = {
            init: function() {
                if ($(this).hasClass('cm-country')) {
                    if (init == false) {
                        $(_.doc).on('change', 'select.cm-country', _rebuildStatesInLocation);
                        init = true;
                    }
                    $(this).trigger('change');
                } else {
                    _rebuildStatesInLocation.call(this);
                }
            }
        }
        $.fn.ceRebuildStates = function(method) {
            var args = arguments;
            return $(this).each(function(i, elm) {
                if (methods[method]) {
                    return methods[method].apply(this, Array.prototype.slice.call(args, 1));
                } else if (typeof method === 'object' || !method) {
                    return methods.init.apply(this, args);
                } else {
                    $.error('ty.rebuildstates: method ' + method + ' does not exist');
                }
            });
        };
        $.ceRebuildStates = function(action, params) {
            params = params || {};
            if (action == 'init') {
                options = params;
            }
        }
    })($);
    (function($) {
        var methods = {
            init: function(params) {
                return this.each(function() {
                    var params = params || {
                        top: $(this).data('ceTop') ? $(this).data('ceTop') : 0,
                        padding: $(this).data('cePadding') ? $(this).data('cePadding') : 0
                    };
                    var self = $(this);
                    $(window).scroll(function() {
                        if ($(window).scrollTop() > params.top) {
                            $(self).css({
                                'position': 'fixed',
                                'top': params.padding + 'px'
                            });
                        } else {
                            $(self).css({
                                'position': '',
                                'top': ''
                            });
                        }
                    });
                });
            }
        };
        $.fn.ceStickyScroll = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === 'object' || !method) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('ty.stickyScroll: method ' + method + ' does not exist');
            }
        };
    })($);
    (function($) {
        var container;
        var timers = {};
        var delay = 0;

        function _duplicateNotification(key) {
            var dups = $('div[data-ca-notification-key=' + key + ']');
            if (dups.length) {
                if (!_addToDialog(dups)) {
                    dups.fadeTo('fast', 0.5).fadeTo('fast', 1).fadeTo('fast', 0.5).fadeTo('fast', 1);
                }
                if (timers[key]) {
                    clearTimeout(timers[key]);
                    methods.close(dups, true);
                }
                return true;
            }
            return false;
        }

        function _closeNotification(notification) {
            if (notification.find('.cm-notification-close-ajax').length) {
                $.ceAjax('request', fn_url('notifications.close?notification_id=' + notification.data('caNotificationKey')), {
                    hidden: true
                });
            }
            notification.fadeOut('fast', function() {
                notification.remove();
            });
            if (notification.hasClass('cm-notification-content-extended')) {
                var overlay = $('.ui-widget-overlay[data-ca-notification-key=' + notification.data('caNotificationKey') + ']');
                if (overlay.length) {
                    overlay.fadeOut('fast', function() {
                        overlay.remove();
                    });
                }
            }
            if ($(".ui-dialog").is(':visible') == false) {
                $('html').removeClass('dialog-is-open');
            }
        }

        function _processTranslation(text) {
            if (_.live_editor_mode && text.indexOf('[lang') != -1) {
                text = '<var class="live-edit-wrap"><i class="cm-icon-live-edit icon-live-edit ty-icon-live-edit"></i><var class="cm-live-edit live-edit-item" data-ca-live-edit="langvar::' + text.substring(text.indexOf('=') + 1, text.indexOf(']')) + '">' + text.substring(text.indexOf(']') + 1, text.lastIndexOf('[')) + '</var></var>';
            }
            return text;
        }

        function _pickFromDialog(event) {
            var nt = $('.cm-notification-content', $(event.target));
            if (nt.length) {
                if (!_addToDialog(nt)) {
                    container.append(nt);
                }
            }
            return true;
        }

        function _addToDialog(notification) {
            var dlg = $.ceDialog('get_last');
            if (dlg.length) {
                $('.object-container', dlg).prepend(notification);
                dlg.off('dialogclose', _pickFromDialog);
                dlg.on('dialogclose', _pickFromDialog);
                return true;
            }
            return false;
        }
        var methods = {
            show: function(data, key) {
                if (!key) {
                    key = $.crc32(data.message);
                }
                if (typeof(data.message) == 'undefined') {
                    return false;
                }
                if (_duplicateNotification(key)) {
                    return true;
                }
                data.message = _processTranslation(data.message);
                data.title = _processTranslation(data.title);
                if (data.type == 'I') {
                    var w = $.getWindowSizes();
                    $('.cm-notification-content.cm-notification-content-extended').each(function() {
                        methods.close($(this), false);
                    });
                    $(_.body).append('<div class="ui-widget-overlay" style="z-index:1010" data-ca-notification-key="' + key + '"></div>');
                    var notification = $('<div class="cm-notification-content cm-notification-content-extended notification-content-extended ' + (data.message_state == "I" ? ' cm-auto-hide' : '') + '" data-ca-notification-key="' + key + '">' + '<h1>' + data.title + '<span class="cm-notification-close close">&times;</span></h1>' + '<div class="notification-body-extended">' +
                        data.message + '</div>' + '</div>');
                    var notificationMaxHeight = w.view_height - 300;
                    $(notification).find('.cm-notification-max-height').css({
                        'max-height': notificationMaxHeight
                    });
                    $(_.body).append(notification);
                    notification.css('top', w.view_height / 2 - (notification.height() / 2));
                } else {
                    var n_class = 'alert';
                    var b_class = '';
                    if (data.type == 'N') {
                        n_class += ' alert-success';
                    } else if (data.type == 'W') {
                        n_class += ' alert-warning';
                    } else if (data.type == 'S') {
                        n_class += ' alert-info';
                    } else {
                        n_class += ' alert-danger';
                    }
                    if (data.message_state == 'I') {
                        n_class += ' cm-auto-hide';
                    } else if (data.message_state == 'S') {
                        b_class += ' cm-notification-close-ajax';
                    }
                    var notification = $('<div class="cm-notification-content notification-content ' + n_class + '" data-ca-notification-key="' + key + '">' + '<button type="button" class="close cm-notification-close ' + b_class + '" data-dismiss="alert">&times;</button>' + '<strong>' + data.title + '</strong> ' + data.message + '</div>');
                    if (!_addToDialog(notification)) {
                        container.append(notification);
                    }
                }
                $.ceEvent('trigger', 'ce.notificationshow', [notification]);
                if (data.message_state == 'I') {
                    methods.close(notification, true);
                }
            },
            showMany: function(data) {
                for (var key in data) {
                    methods.show(data[key], key);
                }
            },
            closeAll: function() {
                container.find('.cm-notification-content').each(function() {
                    var self = $(this);
                    if (!self.hasClass('cm-notification-close-ajax')) {
                        methods.close(self, false);
                    }
                })
            },
            close: function(notification, delayed) {
                if (delayed == true) {
                    if (delay === 0) {
                        return true;
                    }
                    timers[notification.data('caNotificationKey')] = setTimeout(function() {
                        methods.close(notification, false);
                    }, delay);
                    return true;
                }
                _closeNotification(notification);
            },
            init: function() {
                delay = _.notice_displaying_time * 1000;
                container = $('.cm-notification-container');
                $(_.doc).on('click', '.cm-notification-close', function() {
                    methods.close($(this).parents('.cm-notification-content:first'), false);
                })
                container.find('.cm-auto-hide').each(function() {
                    methods.close($(this), true);
                });
            }
        };
        $.ceNotification = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else {
                $.error('ty.notification: method ' + method + ' does not exist');
            }
        };
    }($));
    (function($) {
        var handlers = {};
        var methods = {
            on: function(event, handler, one) {
                one = one || false;
                if (!(event in handlers)) {
                    handlers[event] = [];
                }
                handlers[event].push({
                    handler: handler,
                    one: one
                });
            },
            one: function(event, handler) {
                methods.on(event, handler, true);
            },
            trigger: function(event, data) {
                data = data || [];
                var result = true,
                    _res;
                if (event in handlers) {
                    for (var i = 0; i < handlers[event].length; i++) {
                        _res = handlers[event][i].handler.apply(handlers[event][i].handler, data);
                        if (handlers[event][i].one) {
                            handlers[event].splice(i, 1);
                            i--;
                        }
                        if (_res === false) {
                            result = false;
                            break;
                        }
                    }
                }
                return result;
            }
        };
        $.ceEvent = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else {
                $.error('ty.event: method ' + method + ' does not exist');
            }
        };
    }($));
    (function($) {
        var methods = {
            _init: function(self) {
                if (!self.data('codeEditor')) {
                    var editor = ace.edit(self.prop('id'));
                    editor.session.setUseWrapMode(true);
                    editor.session.setWrapLimitRange();
                    editor.setFontSize("14px");
                    editor.renderer.setShowPrintMargin(false);
                    editor.getSession().on('change', function(e) {
                        self.addClass('cm-item-modified');
                    });
                    self.data('codeEditor', editor);
                }
                return $(this);
            },
            init: function(mode) {
                var self = $(this);
                methods._init(self);
                if (mode) {
                    self.data('codeEditor').getSession().setMode(mode);
                }
                return $(this);
            },
            set_value: function(val, mode) {
                var self = $(this);
                methods._init(self);
                if (mode == undefined) {
                    mode = 'ace/mode/html';
                }
                self.data('codeEditor').getSession().setMode(mode);
                self.data('codeEditor').setValue(val);
                self.data('codeEditor').navigateLineStart();
                self.data('codeEditor').clearSelection();
                self.data('codeEditor').scrollToRow(0);
                return $(this);
            },
            set_show_gutter: function(value) {
                $(this).data('codeEditor').renderer.setShowGutter(value);
            },
            value: function() {
                var self = $(this);
                methods._init(self);
                return self.data('codeEditor').getValue();
            },
            focus: function() {
                var self = $(this);
                var session = self.data('codeEditor').getSession();
                var count = session.getLength();
                self.data('codeEditor').focus();
                self.data('codeEditor').gotoLine(count, session.getLine(count - 1).length);
            },
            set_listener: function(event_name, callback) {
                $(this).data('codeEditor').getSession().on(event_name, function(e) {
                    callback(e);
                });
                return $(this);
            }
        };
        $.fn.ceCodeEditor = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === 'object' || !method) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('ty.codeeditor: method ' + method + ' does not exist');
            }
        };
    })($);
    (function($) {
        var plugin_name = "ceObjectSelector",
            defaults = {
                pageSize: 10,
                enableSearch: true,
                closeOnSelect: true,
                loadViaAjax: false,
                dataUrl: null,
                enableImages: false,
                imageWidth: 20,
                imageHeight: 20,
                placeholder: null
            };

        function ObjectSelector(element, options) {
            this.$el = $(element);
            this.settings = $.extend({}, defaults, options);
            this.init();
        }
        $.extend(ObjectSelector.prototype, {
            init: function() {
                var data = this.$el.data();
                this.settings.placeholder = data.caPlaceholder || this.settings.placeholder;
                this.settings.pageSize = data.caPageSize || this.settings.pageSize;
                this.settings.dataUrl = data.caDataUrl || this.settings.dataUrl;
                this.settings.loadViaAjax = data.caLoadViaAjax === undefined ? this.settings.loadViaAjax : data.caLoadViaAjax;
                this.settings.closeOnSelect = data.caCloseOnSelect === undefined ? this.settings.closeOnSelect : data.caCloseOnSelect;
                this.settings.enableImages = data.caEnableImages === undefined ? this.settings.enableImages : data.caEnableImages;
                this.settings.enableSearch = data.caEnableSearch === undefined ? this.settings.enableSearch : data.caEnableSearch;
                this.settings.imageWidth = data.caImageWidth === undefined ? this.settings.imageWidth : data.caImageWidth;
                this.settings.imageHeight = data.caImageHeight === undefined ? this.settings.imageHeight : data.caImageHeight;
                this.initSelect2();
            },
            initSelect2: function() {
                var self = this,
                    select2config = {
                        language: {
                            'loadingMore': function() {
                                return _.tr('loading');
                            },
                            'searching': function() {
                                return _.tr('loading');
                            },
                            'errorLoading': function() {
                                return _.tr('error');
                            }
                        },
                        closeOnSelect: this.settings.closeOnSelect,
                        placeholder: this.settings.placeholder
                    };
                if (this.settings.loadViaAjax && this.settings.dataUrl !== null) {
                    select2config.ajax = {
                        url: this.settings.dataUrl,
                        data: function(params) {
                            var request = {
                                q: params.term,
                                page: params.page || 1,
                                page_size: self.settings.pageSize
                            };
                            if (self.settings.enableImages) {
                                request.image_width = self.settings.imageWidth;
                                request.image_height = self.settings.imageHeight;
                            }
                            return request;
                        },
                        processResults: function(data, params) {
                            params.page = params.page || 1;
                            return {
                                results: data.objects,
                                pagination: {
                                    more: (params.page * self.settings.pageSize) < data.total_objects
                                }
                            };
                        },
                        transport: function(params, success, failure) {
                            params.callback = success;
                            params.hidden = true;
                            return $.ceAjax('request', params.url, params);
                        }
                    };
                }
                if (this.settings.enableImages) {
                    select2config.templateResult = function(object) {
                        if (!object.image_url) {
                            return object.text;
                        }
                        return $('<img src="' + object.image_url + '" alt="' + object.text + '" /><span>' + object.text + '</span>');
                    };
                }
                if (!this.settings.enableSearch) {
                    select2config.minimumResultsForSearch = Infinity;
                }
                this.$el.select2(select2config);
            }
        });
        $.fn[plugin_name] = function(options) {
            var self = this,
                createPluginInstances = function() {
                    return self.each(function() {
                        if (!$.data(this, "plugin_" + plugin_name)) {
                            $.data(this, "plugin_" + plugin_name, new ObjectSelector(this, options));
                        }
                    });
                };
            if (this.length) {
                if ($.fn.select2) {
                    return createPluginInstances();
                } else {
                    $.getScript('js/lib/select2/select2.full.min.js', function() {
                        createPluginInstances();
                    });
                }
            }
            return this;
        };
    })($);
    _.toNumeric = function(arg) {
        var number = Number(String(arg).str_replace(',', '.'));
        return isNaN(number) ? 0 : number;
    };
    _.getFloatPrecision = function(x) {
        return String(x).replace('.', '').length - x.toFixed().length;
    };
    if (!_.embedded && location.hash && unescape(location.hash).indexOf('#!/') === 0) {
        var components = $.parseUrl(location.href)
        var uri = $.ceHistory('parseHash', location.hash);
        if ($.browser.msie && $.browser.version >= 9) {
            $.redirect(components.protocol + '://' + components.host + uri);
        } else {
            $.redirect(components.protocol + '://' + components.host + components.directory + uri);
        }
    }
}(Tygh, jQuery));

function fn_print_r(value) {
    fn_alert(fn_print_array(value));
}

function fn_alert(msg, not_strip) {
    msg = not_strip ? msg : fn_strip_tags(msg);
    alert(msg);
}

function fn_print_array(arr, level) {
    var dumped_text = "";
    if (!level) {
        level = 0;
    }
    var level_padding = "";
    for (var j = 0; j < level + 1; j++) {
        level_padding += "    ";
    }
    if (typeof(arr) == 'object') {
        for (var item in arr) {
            var value = arr[item];
            if (typeof(value) == 'object') {
                dumped_text += level_padding + "'" + item + "' ...\n";
                dumped_text += fn_print_array(value, level + 1);
            } else {
                dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
            }
        }
    } else {
        dumped_text = arr + " (" + typeof(arr) + ")";
    }
    return dumped_text;
}

function fn_url(url) {
    var index_url = Tygh.current_location + '/' + Tygh.index_script;
    var components = Tygh.$.parseUrl(url);
    if (url == '') {
        url = index_url;
    } else if (components.protocol) {
        if (Tygh.embedded) {
            var s, spos;
            if (Tygh.facebook && Tygh.facebook.url.indexOf(components.location) != -1) {
                s = '&app_data=';
            } else if (Tygh.init_context == components.source.str_replace('#' + components.anchor, '')) {
                s = '#!';
            }
            if (s) {
                var q = '';
                if ((spos = url.indexOf(s)) != -1) {
                    q = decodeURIComponent(url.substr(spos + s.length)).replace('&amp;', '&');
                }
                url = Tygh.current_location + q;
            }
        }
    } else if (components.file != Tygh.index_script) {
        if (url.indexOf('?') == 0) {
            url = index_url + url;
        } else {
            url = index_url + '?dispatch=' + url.replace('?', '&');
        }
    }
    return url;
}

function fn_strip_tags(str) {
    str = String(str).replace(/<.*?>/g, '');
    return str;
}

function fn_reload_form(jelm) {
    var form = jelm.parents('form');
    var container = form.parent();
    var submit_btn = form.find("input[type='submit']");
    if (!submit_btn.length) {
        submit_btn = Tygh.$('[data-ca-target-form=' + form.prop('name') + ']');
    }
    if (container.length && submit_btn.length) {
        var url = form.prop('action') + '?reload_form=1&' + submit_btn.prop('name');
        var data = form.serializeObject();
        var result_ids;
        if (data.result_ids != 'undefined') {
            result_ids = data.result_ids;
        } else {
            result_ids = container.prop('id');
        }
        Tygh.$.ceAjax('request', fn_url(url), {
            data: data,
            result_ids: result_ids
        });
    }
}

function fn_get_listed_lang(langs) {
    var $ = Tygh.$;
    var check_langs = [Tygh.cart_language, Tygh.default_language, 'en'];
    var lang = '';
    if (langs.length) {
        lang = langs[0];
        for (var i = 0; i < check_langs.length; i++) {
            if (Tygh.$.inArray(check_langs[i], langs) != -1) {
                lang = check_langs[i];
                break;
            }
        }
    }
    return lang;
}

function fn_query_remove(query, vars) {
    if (typeof(vars) == 'undefined') {
        return query;
    }
    if (typeof vars == 'string') {
        vars = [vars];
    }
    var start = query;
    if (query.indexOf('?') >= 0) {
        start = query.substr(0, query.indexOf('?') + 1);
        var search = query.substr(query.indexOf('?') + 1);
        var srch_array = search.split("&");
        var temp_array = [];
        var concat = true;
        var amp = '';
        for (var i = 0; i < srch_array.length; i++) {
            temp_array = srch_array[i].split("=");
            concat = true;
            for (var j = 0; j < vars.length; j++) {
                if (vars[j] == temp_array[0] || temp_array[0].indexOf(vars[j] + '[') != -1) {
                    concat = false;
                    break;
                }
            }
            if (concat == true) {
                start += amp + temp_array[0] + '=' + temp_array[1];
            }
            amp = '&';
        }
    }
    return start;
}
(function(_, $) {
    'use strict';
    var ui = (function() {
        return {
            winWidth: function() {
                return $(window).width();
            },
            responsiveScroll: function() {
                $.ceEvent('on', 'ce.needScroll', function(opt) {
                    opt.need_scroll = false;
                    setTimeout(function() {
                        $.scrollToElm($('#' + opt.jelm.data('caScroll')));
                    }, 310);
                });
            },
            responsiveNotifications: function() {
                if (this.winWidth() <= 767) {
                    $.ceEvent('on', 'ce.notificationshow', function(notification) {
                        if ($(notification).hasClass('cm-notification-content-extended')) {
                            $('body,html').scrollTop(0);
                        }
                    });
                }
            },
            resizeDialog: function() {
                var self = this;
                var dlg = $('.ui-dialog');
                $('.ui-widget-overlay').css({
                    'min-height': $(window).height()
                });
                $(dlg).css({
                    'position': 'absolute',
                    'width': $(window).width() - 20,
                    'left': '10px',
                    'top': '10px',
                    'max-height': 'none',
                    'height': 'auto',
                    'margin-bottom': '10px'
                });
                $(dlg).find('.ui-dialog-title').css({
                    'width': $(window).width() - 80
                });
                $(dlg).find('.ui-dialog-content').css({
                    'height': 'auto',
                    'max-height': 'none'
                });
                $(dlg).find('.object-container').css({
                    'height': 'auto'
                });
                $(dlg).find('.buttons-container').css({
                    'position': 'relative',
                    'top': 'auto',
                    'left': '0px',
                    'right': '0px',
                    'bottom': '0px',
                    'width': 'auto'
                });
            },
            responsiveDialog: function() {
                var self = this;
                $.ceEvent('on', 'ce.dialogshow', function() {
                    if (self.winWidth() <= 767) {
                        self.resizeDialog();
                        $('body,html').scrollTop(0);
                    }
                });
            },
            responsiveFilters: function(e) {
                var filtersContent = $('.cm-horizontal-filters-content');
                if (this.winWidth() <= 767) {
                    filtersContent.removeClass('cm-popup-box');
                } else {
                    filtersContent.addClass('cm-popup-box');
                }
                if (this.winWidth() > 767) {
                    $('.ty-horizontal-filters-content-to-right').removeClass('ty-horizontal-filters-content-to-right');
                    $('.ty-horizontal-product-filters-dropdown').click(function() {
                        var hrFiltersWidth = $(".cm-horizontal-filters").width();
                        var hrFiltersContent = $('.cm-horizontal-filters-content', this);
                        setTimeout(function() {
                            var position = hrFiltersContent.offset().left + hrFiltersContent.width();
                            if (position > hrFiltersWidth) {
                                hrFiltersContent.addClass("ty-horizontal-filters-content-to-right");
                            }
                        }, 1);
                    });
                }
            }
        };
    })();
    $(document).ready(function() {
        $(window).resize(function(e) {
            ui.winWidth();
            ui.responsiveFilters();
        });
        if (window.addEventListener) {
            window.addEventListener('orientationchange', function() {
                if (ui.winWidth() <= 767) {
                    ui.resizeDialog();
                }
                $.ceDialog('get_last').ceDialog('reload');
            }, false);
        }
        ui.responsiveDialog();
        ui.responsiveFilters();
        $.ceEvent('on', 'ce.ajaxdone', function() {
            ui.responsiveFilters();
            if (ui.winWidth() <= 767) {
                ui.resizeDialog();
            }
        });
    });
    $.ceEvent('on', 'ce.tab.init', function() {
        ui.responsiveScroll();
    });
}(Tygh, Tygh.$));
(function(_, $) {
    var loadedScripts = {};
    var sessionData = {};
    (function($) {
        var REQUEST_XML = 1;
        var REQUEST_IFRAME = 2;
        var REQUEST_COMET = 3;
        var REQUEST_JSONP_POST = 5;
        var QUERIES_LIMIT = 1;
        var queryStack = [];
        var activeQueries = 0;
        var evalCache = {};
        var responseCache = {};
        var getScriptQueries = 0;
        var oldjQuery = {};
        var methods = {
            request: function(url, params) {
                params = params || {};
                params.method = params.method || 'get';
                params.data = params.data || {};
                params.message = params.message || _.tr('loading');
                params.caching = params.caching || false;
                params.hidden = params.hidden || false;
                params.repeat_on_error = params.repeat_on_error || false;
                params.force_exec = params.force_exec || false;
                params.obj = params.obj || null;
                params.append = params.append || null;
                params.scroll = params.scroll || null;
                params.overlay = params.overlay || null;
                if (_.embedded) {
                    params.full_render = true;
                }
                if (params.full_render) {
                    params.data.full_render = params.full_render;
                }
                if (typeof(params.data.security_hash) == 'undefined' && typeof(_.security_hash) != 'undefined' && params.method.toLowerCase() == 'post') {
                    params.data.security_hash = _.security_hash;
                }
                if (params.result_ids) {
                    params.data.result_ids = params.result_ids;
                }
                if (params.skip_result_ids_check) {
                    params.data.skip_result_ids_check = params.skip_result_ids_check;
                }
                if (activeQueries >= QUERIES_LIMIT) {
                    queryStack.unshift(function() {
                        methods.request(url, params);
                    });
                    return true;
                }
                if (params.hidden === false) {
                    $.toggleStatusBox('show', {
                        overlay: params.overlay
                    });
                }
                var hash = '';
                if (params.caching === true) {
                    hash = $.crc32(url + $.param(params.data));
                }
                if (!hash || !responseCache[hash]) {
                    var saved_data = {};
                    var result_ids = (params.data.result_ids) ? params.data.result_ids.split(',') : [];
                    if (result_ids.length > 0) {
                        for (var j = 0; j < result_ids.length; j++) {
                            var container = $('#' + result_ids[j]);
                            if (container.hasClass('cm-save-fields')) {
                                saved_data[result_ids[j]] = $(':input:visible', container).serializeArray();
                            }
                        }
                        params.saved_data = saved_data;
                    }
                    if (url) {
                        url = fn_query_remove(url, 'result_ids');
                        if (url.indexOf('://') == -1) {
                            url = _.current_location + '/' + url;
                        }
                        if (params.obj && params.obj.hasClass('cm-comet')) {
                            params.url = url + '&result_ids=' + params.result_ids + '&is_ajax=' + REQUEST_COMET;
                            return transports.iframe(null, params, {
                                is_comet: true
                            });
                        } else {
                            activeQueries++;
                            var data_type = (!$.support.cors && url.indexOf('://' + window.location.hostname) == -1) ? 'jsonp' : 'json';
                            if (!('is_ajax' in params.data) && data_type == 'json') {
                                params.data.is_ajax = REQUEST_XML;
                            }
                            if (sessionData.name && url.indexOf(sessionData.name) == -1) {
                                params.data[sessionData.name] = localStorage.getItem(sessionData.name);
                            }
                            var components = $.parseUrl(url);
                            if (components.anchor) {
                                params.data.anchor = components.anchor;
                            }
                            return $.ajax({
                                type: params.method,
                                url: url,
                                dataType: data_type,
                                cache: true,
                                data: params.data,
                                xhrFields: {
                                    withCredentials: true
                                },
                                success: function(data, textStatus) {
                                    if (hash) {
                                        responseCache[hash] = data;
                                    }
                                    _response(data, params);
                                },
                                error: function(XMLHttpRequest, textStatus, errorThrown) {
                                    if (params.repeat_on_error) {
                                        params.repeat_on_error = false;
                                        methods.request(url, params);
                                        return false;
                                    }
                                    $.toggleStatusBox('hide');
                                    if (params.hidden === false && errorThrown) {
                                        var err_msg = _.tr('error_ajax').str_replace('[error]', errorThrown);
                                        $.ceNotification('show', {
                                            type: 'E',
                                            title: _.tr('error'),
                                            message: err_msg
                                        });
                                    }
                                },
                                complete: function(XMLHttpRequest, textStatus) {
                                    activeQueries--;
                                    if (queryStack.length) {
                                        var f = queryStack.shift();
                                        f();
                                    }
                                }
                            });
                        }
                    }
                } else if (hash && responseCache[hash]) {
                    _response(responseCache[hash], params);
                }
                return false;
            },
            submitForm: function(form, clicked_elm) {
                if (activeQueries >= QUERIES_LIMIT) {
                    queryStack.unshift(function() {
                        var submit_name = clicked_elm.attr('name'),
                            submit_value = clicked_elm.val(),
                            input;
                        if (submit_name) {
                            input = $('<input>', {
                                type: 'hidden',
                                value: submit_value,
                                name: submit_name
                            }).appendTo(form);
                        }
                        if (methods.submitForm(form, clicked_elm)) {
                            form.get(0).submit();
                        }
                        if (input) {
                            input.remove();
                        }
                    });
                    return false;
                }
                var params = {
                    form: form,
                    obj: clicked_elm,
                    scroll: clicked_elm.data('caScroll') || '',
                    overlay: clicked_elm.data('caOverlay') || '',
                    callback: 'ce.formajaxpost_' + form.prop('name')
                };
                $.ceNotification('closeAll');
                $.toggleStatusBox('show', {
                    overlay: params.overlay
                });
                var options = _getOptions(form, params);
                if (options.force_exec) {
                    params['force_exec'] = true;
                }
                if (sessionData.name) {
                    form.append('<input type="hidden" name="' + sessionData.name + '" value="' + localStorage.getItem(sessionData.name) + '">');
                }
                if (options.full_render) {
                    form.append('<input type="hidden" name="full_render" value="Y">');
                }
                form.append('<input type="hidden" name="is_ajax" value="' + (options.transport == 'iframe' ? (options.is_comet ? REQUEST_COMET : REQUEST_IFRAME) : (options.transport == 'jsonpPOST' ? REQUEST_JSONP_POST : REQUEST_XML)) + '">');
                return transports[options.transport](form, params, options);
            },
            inProgress: function() {
                return activeQueries !== 0;
            },
            clearCache: function() {
                responseCache = {};
                return true;
            },
            response: function(response, params) {
                return _response(response, params);
            }
        };
        var transports = {
            iframe: function(form, params, options) {
                var iframe = $('<iframe name="upload_iframe" src="javascript: false;" class="hidden"></iframe>').appendTo(_.body);
                activeQueries++;
                if (options.is_comet) {
                    $('#comet_container_controller').ceProgress('init');
                }
                iframe.on('load', function() {
                    var response = {};
                    var self = $(this);
                    if (self.contents().text() !== null) {
                        eval('var response = ' + self.contents().find('textarea').val());
                    }
                    response = response || {};
                    _response(response, params);
                    if (options.is_comet && jQuery.isEmptyObject(response) == false) {
                        $('#comet_container_controller').ceProgress('finish');
                        $.ceEvent('trigger', 'ce.cometdone', [form, params, options, response]);
                    }
                    self.remove();
                    activeQueries--;
                    if (queryStack.length) {
                        var f = queryStack.shift();
                        f();
                    }
                });
                if (form) {
                    form.prop('target', 'upload_iframe');
                } else if (params.url) {
                    if (params.method == 'post') {
                        $('<form class="hidden" action="' + params.url + '" method="post" target="upload_iframe"><input type="hidden" name="security_hash" value="' + _.security_hash + '"></form>').appendTo(_.body).submit();
                    } else {
                        iframe.prop('src', params.url);
                    }
                }
                return true;
            },
            xml: function(form, params) {
                var hash = $(':input', form).serializeObject();
                if (params.obj && params.obj.prop('name')) {
                    hash[params.obj.prop('name')] = params.obj.val();
                }
                params['method'] = form.prop('method');
                params['data'] = hash;
                params['result_ids'] = form.data('caTargetId');
                methods.request(form.prop('action'), params);
                return false;
            },
            jsonpPOST: function(form, params, options) {
                $.receiveMessage(function(e) {
                    if (options.is_comet) {
                        $('#comet_container_controller').ceProgress('finish');
                    }
                    iframe.remove();
                    _response($.parseJSON(e.data), params);
                    activeQueries--;
                });
                var iframe = $('<iframe name="upload_iframe" src="javascript: false;" class="hidden"></iframe>').appendTo(_.body);
                activeQueries++;
                if (options.is_comet) {
                    $('#comet_container_controller').ceProgress('init');
                }
                if (form) {
                    form.prop('target', 'upload_iframe');
                } else if (params.url) {
                    iframe.prop('src', params.url);
                }
                return true;
            }
        };

        function _getOptions(obj, params) {
            var is_comet = obj.hasClass('cm-comet') || (params.obj && params.obj.hasClass('cm-comet'));
            var transport = 'xml';
            var uploads = is_comet;
            if (!is_comet && obj.prop('enctype') == 'multipart/form-data') {
                obj.find('input[type=file]').each(function() {
                    if ($(this).val()) {
                        uploads = true;
                    }
                });
            }
            if ((!$.support.cors || (_.embedded && uploads)) && obj.prop('action').indexOf('//') != -1 && obj.prop('action').indexOf('//' + window.location.hostname) == -1 && obj.prop('method') == 'post') {
                transport = 'jsonpPOST';
            } else if (uploads) {
                transport = 'iframe';
            }
            return {
                'full_render': obj.hasClass('cm-ajax-full-render'),
                'is_comet': is_comet,
                'force_exec': obj.hasClass('cm-ajax-force'),
                'transport': transport
            };
        }

        function _response(response, params) {
            params = params || {};
            params.force_exec = params.force_exec || false;
            params.pre_processing = params.pre_processing || {};
            var regex_all = new RegExp('<script[^>]*>([\u0001-\uFFFF]*?)</script>', 'img');
            var matches = [];
            var match = '';
            var elm;
            var data = response || {};
            var inline_scripts = null;
            var scripts_to_load = [];
            var elms = [];
            var content;
            if (params.pre_processing && typeof(params.pre_processing) == 'function') {
                params.pre_processing(data, params);
            }
            if (data.force_redirection) {
                $.toggleStatusBox('hide');
                $.redirect(data.force_redirection);
                return true;
            }
            if ($.isEmptyObject(evalCache)) {
                $('script:not([src])').each(function() {
                    var self = $(this);
                    evalCache[$.crc32(self.html())] = true;
                });
            }
            if (data.html) {
                for (var k in data.html) {
                    elm = $('#' + k);
                    if (elm.length != 1 || data.html[k] === null) {
                        continue;
                    }
                    if (data.html[k].indexOf('<form') != -1 && elm.parents('form').length) {
                        $(_.body).append(elm);
                    }
                    matches = data.html[k].match(regex_all);
                    content = matches ? data.html[k].replace(regex_all, '') : data.html[k];
                    $.ceDialog('destroy_loaded', {
                        content: content
                    });
                    if (params.append) {
                        elm.append(content);
                    } else {
                        elm.html(content);
                    }
                    if (typeof(params.saved_data) != 'undefined' && typeof(params.saved_data[k]) != 'undefined') {
                        var elements = [];
                        for (var i in params.saved_data[k]) {
                            elements[params.saved_data[k][i]['name']] = params.saved_data[k][i]['value'];
                        }
                        $('input:visible, select:visible', elm).each(function(id, local_elm) {
                            var jelm = $(local_elm);
                            if (typeof(elements[jelm.prop('name')]) != 'undefined' && !jelm.parents().hasClass('cm-skip-save-fields')) {
                                if (jelm.prop('type') == 'radio') {
                                    if (jelm.val() == elements[jelm.prop('name')]) {
                                        jelm.prop('checked', true);
                                    }
                                } else {
                                    jelm.val(elements[jelm.prop('name')]);
                                }
                                jelm.trigger('change');
                            }
                        });
                    }
                    if ($.trim(elm.html())) {
                        elm.parents('.hidden.cm-hidden-wrapper').removeClass('hidden');
                    } else {
                        elm.parents('.cm-hidden-wrapper').addClass('hidden');
                    }
                    var all_scripts = null,
                        ext_scripts = null;
                    if (matches) {
                        all_scripts = $(matches.join('\n'));
                        ext_scripts = all_scripts.filter('[src]');
                        inline_scripts = (inline_scripts) ? inline_scripts.add(all_scripts.filter(':not([src])')) : all_scripts.filter(':not([src])');
                        if (ext_scripts.length) {
                            for (var i = 0; i < ext_scripts.length; i++) {
                                var _src = ext_scripts.eq(i).prop('src');
                                if (loadedScripts[_src]) {
                                    if (ext_scripts.eq(i).hasClass('cm-ajax-force')) {
                                        loadedScripts[_src] = null;
                                    } else {
                                        continue;
                                    }
                                }
                                scripts_to_load.push($.getScript(_src));
                            }
                        }
                    }
                    elms.push(elm);
                }
                if ($.ceDialog('inside_dialog', {
                        jelm: elm
                    })) {
                    $.ceDialog('reload_parent', {
                        jelm: elm
                    });
                }
                if (response.title) {
                    $(document).prop('title', response.title);
                }
            }
            var done_event = function() {
                $.ceEvent('trigger', 'ce.ajaxdone', [elms, inline_scripts, params, data, response.text || '']);
            };
            if (scripts_to_load.length) {
                $.when.apply(null, scripts_to_load).then(done_event);
            } else {
                done_event();
            }
        }
        var ajax = $.ajax;
        $.ajax = function(origSettings) {
            if (origSettings.dataType && origSettings.dataType == 'script') {
                var _src = origSettings.url;
                if (loadedScripts[_src]) {
                    return false;
                }
                loadedScripts[origSettings.url] = true;
            }
            return ajax(origSettings);
        };
        $.getScript = function(url, callback) {
            url = (url.indexOf('//') == -1) ? _.current_location + '/' + url : url;
            if (_.otherjQ && getScriptQueries === 0) {
                oldjQuery = jQuery;
                jQuery = _.$;
            }
            getScriptQueries++;
            return $.ajax({
                type: "GET",
                url: url,
                success: function(data, textStatus, jqxhr) {
                    getScriptQueries--;
                    if (_.otherjQ && getScriptQueries === 0) {
                        _.$ = jQuery;
                        jQuery = oldjQuery;
                    }
                    if (callback) {
                        callback(data, textStatus, jqxhr);
                    }
                },
                dataType: "script",
                cache: true
            });
        };
        $.ceEvent('on', 'ce.ajaxdone', function(elms, scripts, params, response_data, response_text) {
            var i;
            if (_.embedded && response_data.language_changed) {
                _.embedded = false;
                $.redirect(response_data.current_url, false);
                window.location.reload(true);
                return;
            }
            if (params.on_ajax_done && typeof(params.on_ajax_done) == 'function') {
                params.on_ajax_done(response_data, params, response_text);
            }
            if (scripts) {
                for (i = 0; i < scripts.length; i++) {
                    var _hash = $.crc32(scripts.eq(i).html());
                    if (!evalCache[_hash] || params.force_exec || scripts.eq(i).hasClass('cm-ajax-force')) {
                        $.globalEval(scripts.eq(i).html());
                        evalCache[_hash] = true;
                    }
                }
            }
            if (response_data.debug_info) {
                console.log(response_data.debug_info);
            }
            var link_history = (params.save_history && (!params.obj || (params.obj && $.ceDialog('inside_dialog', {
                jelm: params.obj
            }) === false)));
            if (response_data.session_data) {
                sessionData = response_data.session_data;
                localStorage.setItem(sessionData.name, sessionData.id);
            }
            if (response_data.current_url) {
                var current_url = decodeURIComponent(response_data.current_url);
                if (!params.skip_history && (_.embedded || link_history)) {
                    var _params = params;
                    if (!link_history) {
                        _params.result_ids = _.container;
                    }
                    if (response_data.anchor) {
                        current_url += '#' + response_data.anchor;
                    }
                    $.ceHistory('load', current_url, _params, true);
                    _.current_url = current_url;
                }
                if (response_data.anchor) {
                    _.anchor = params.scroll = '#' + response_data.anchor;
                }
            }
            for (i = 0; i < elms.length; i++) {
                $.commonInit(elms[i]);
            }
            if (params.form) {
                $('input[name=is_ajax]', params.form).remove();
                $('input[name=full_render]', params.form).remove();
                if (params.form.hasClass('cm-disable-empty') || params.form.hasClass('cm-disable-empty-files')) {
                    $('input.cm-disabled', params.form).prop('disabled', false).removeClass('cm-disabled');
                }
            }
            if (params.callback && $.isFunction(params.callback)) {
                params.callback(response_data, params, response_text);
            } else {
                $.ceEvent('trigger', params.callback, [response_data, params, response_text]);
            }
            if (!params.keep_status_box) {
                $.toggleStatusBox('hide');
            }
            if (params.scroll) {
                if (!_.scrolling) {
                    $.scrollToElm($(params.scroll));
                }
            }
            if (response_data.notifications) {
                $.ceNotification('showMany', response_data.notifications);
            }
        });
        $.ceAjax = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === 'object' || !method) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('ty.ajax: method ' + method + ' does not exist');
            }
        };
    })($);
    $(document).ready(function() {
        $('script').each(function() {
            var _src = $(this).prop('src');
            if (_src) {
                loadedScripts[_src] = true;
            }
        });
        if (typeof(ajax_callback_data) != 'undefined' && ajax_callback_data) {
            $.globalEval(ajax_callback_data);
            ajax_callback_data = false;
        }
    });
}(Tygh, Tygh.$));
(function(_, $) {
    var locationWrapper = {
        put: function(hash, win) {
            (win || window).location.hash = this.encoder(hash);
        },
        get: function(win) {
            var hash = ((win || window).location.hash).replace(/^#/, '');
            try {
                return decodeURIComponent(hash);
            } catch (error) {
                return hash;
            }
        },
        encoder: encodeURIComponent
    };
    var historyState = {
        storage: null,
        first: '',
        put: function(hash, params) {
            if (!this.storage) {
                this.storage = {};
                this.first = hash;
            }
            this.storage[hash] = params;
        },
        get: function(hash) {
            if (hash in this.storage) {
                return this.storage[hash];
            }
            return {};
        }
    };

    function initObjects(options) {
        options = $.extend({
            unescape: false
        }, options || {});
        locationWrapper.encoder = encoder(options.unescape);

        function encoder(unescape_) {
            if (unescape_ === true) {
                return function(hash) {
                    return hash;
                };
            }
            if (typeof unescape_ == "string" && (unescape_ = partialDecoder(unescape_.split(""))) || typeof unescape_ == "function") {
                return function(hash) {
                    return unescape_(encodeURIComponent(hash));
                };
            }
            return encodeURIComponent;
        }

        function partialDecoder(chars) {
            var re = new RegExp($.map(chars, encodeURIComponent).join("|"), "ig");
            return function(enc) {
                return enc.replace(re, decodeURIComponent);
            };
        }
    }
    var implementations = {};
    implementations.base = {
        callback: undefined,
        type: undefined,
        check: function() {},
        load: function(hash) {},
        init: function(callback, options) {
            initObjects(options);
            self.callback = callback;
            self._options = options;
            self._init();
        },
        _init: function() {},
        _options: {}
    };
    implementations.hashchangeEvent = {
        _skip: false,
        _init: function() {
            $(window).bind('hashchange', function() {
                if (self._skip === true) {
                    self._skip = false;
                    return;
                }
                self.check();
            });
        },
        check: function() {
            var hash = locationWrapper.get() ? locationWrapper.get() : historyState.first;
            self.callback(hash, historyState.get(hash));
        },
        load: function(hash, params) {
            var current_hash = locationWrapper.get() ? locationWrapper.get() : historyState.first;
            historyState.put(hash, params);
            if (hash != current_hash) {
                self._skip = true;
            }
            locationWrapper.put(hash);
        },
        reload: function(hash, params) {
            historyState.put(hash, params);
        }
    };
    implementations.HTML5 = {
        _init: function() {
            $(window).bind('popstate', self.check);
        },
        check: function(evt) {
            var state = evt.originalEvent.state;
            self.callback(state ? '#!/' + document.location : '', state);
        },
        load: function(hash, params) {
            window.history.pushState(params, null, _.current_location + '/' + hash.replace(/^\!\//, ''));
        },
        reload: function(hash, params) {
            window.history.replaceState(params, null, _.current_location + '/' + hash.replace(/^\!\//, ''));
        }
    };
    var self = $.extend({}, implementations.base);
    if (!_.embedded && "pushState" in window.history) {
        self.type = 'HTML5';
    } else if ("onhashchange" in window) {
        self.type = 'hashchangeEvent';
    }
    if (self.type) {
        $.extend(self, implementations[self.type]);
        $.history = self;
    }
})(Tygh, Tygh.$);
(function($) {
    function getElementSelection(that) {
        var position = {};
        if (that.selectionStart === undefined) {
            that.focus();
            var select = document.selection.createRange();
            position.length = select.text.length;
            select.moveStart('character', -that.value.length);
            position.end = select.text.length;
            position.start = position.end - position.length;
        } else {
            position.start = that.selectionStart;
            position.end = that.selectionEnd;
            position.length = position.end - position.start;
        }
        return position;
    }

    function setElementSelection(that, start, end) {
        if (that.selectionStart === undefined) {
            that.focus();
            var r = that.createTextRange();
            r.collapse(true);
            r.moveEnd('character', end);
            r.moveStart('character', start);
            r.select();
        } else {
            that.selectionStart = start;
            that.selectionEnd = end;
        }
    }

    function runCallbacks($this, settings) {
        $.each(settings, function(k, val) {
            if (typeof val === 'function') {
                settings[k] = val($this, settings, k);
            } else if (typeof $this.autoNumeric[val] === 'function') {
                settings[k] = $this.autoNumeric[val]($this, settings, k);
            }
        });
    }

    function convertKeyToNumber(settings, key) {
        if (typeof(settings[key]) === 'string') {
            settings[key] *= 1;
        }
    }

    function autoCode($this, settings) {
        runCallbacks($this, settings);
        settings.oEvent = null;
        settings.tagList = ['DD', 'DT', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'LABEL', 'P', 'SPAN', 'TD', 'TH'];
        var vmax = settings.vMax.toString().split('.'),
            vmin = (!settings.vMin && settings.vMin !== 0) ? [] : settings.vMin.toString().split('.');
        convertKeyToNumber(settings, 'vMax');
        convertKeyToNumber(settings, 'vMin');
        convertKeyToNumber(settings, 'mDec');
        settings.allowLeading = true;
        settings.aNeg = settings.vMin < 0 ? '-' : '';
        vmax[0] = vmax[0].replace('-', '');
        vmin[0] = vmin[0].replace('-', '');
        settings.mInt = Math.max(vmax[0].length, vmin[0].length, 1);
        if (settings.mDec === null) {
            var vmaxLength = 0,
                vminLength = 0;
            if (vmax[1]) {
                vmaxLength = vmax[1].length;
            }
            if (vmin[1]) {
                vminLength = vmin[1].length;
            }
            settings.mDec = Math.max(vmaxLength, vminLength);
        }
        if (settings.altDec === null && settings.mDec > 0) {
            if (settings.aDec === '.' && settings.aSep !== ',') {
                settings.altDec = ',';
            } else if (settings.aDec === ',' && settings.aSep !== '.') {
                settings.altDec = '.';
            }
        }
        var aNegReg = settings.aNeg ? '([-\\' + settings.aNeg + ']?)' : '(-?)';
        settings.aNegRegAutoStrip = aNegReg;
        settings.skipFirstAutoStrip = new RegExp(aNegReg + '[^-' + (settings.aNeg ? '\\' + settings.aNeg : '') + '\\' + settings.aDec + '\\d]' + '.*?(\\d|\\' + settings.aDec + '\\d)');
        settings.skipLastAutoStrip = new RegExp('(\\d\\' + settings.aDec + '?)[^\\' + settings.aDec + '\\d]\\D*$');
        var allowed = '-' + settings.aNum + '\\' + settings.aDec;
        if (settings.altDec && settings.altDec !== settings.aSep) {
            allowed += settings.altDec;
        }
        settings.allowedAutoStrip = new RegExp('[^' + allowed + ']', 'gi');
        settings.numRegAutoStrip = new RegExp(aNegReg + '(?:\\' + settings.aDec + '?(\\d+\\' + settings.aDec + '\\d+)|(\\d*(?:\\' + settings.aDec + '\\d*)?))');
        return settings;
    }

    function autoStrip(s, settings, strip_zero) {
        if (settings.aSign) {
            while (s.indexOf(settings.aSign) > -1) {
                s = s.replace(settings.aSign, '');
            }
        }
        s = s.replace(settings.skipFirstAutoStrip, '$1$2');
        s = s.replace(settings.skipLastAutoStrip, '$1');
        s = s.replace(settings.allowedAutoStrip, '');
        if (settings.altDec) {
            s = s.replace(settings.altDec, settings.aDec);
        }
        var m = s.match(settings.numRegAutoStrip);
        s = m ? [m[1], m[2], m[3]].join('') : '';
        if ((settings.lZero === 'allow' || settings.lZero === 'keep') && strip_zero !== 'strip') {
            var parts = [],
                nSign = '';
            parts = s.split(settings.aDec);
            if (parts[0].indexOf('-') !== -1) {
                nSign = '-';
                parts[0] = parts[0].replace('-', '');
            }
            if (parts[0].length > settings.mInt && parts[0].charAt(0) === '0') {
                parts[0] = parts[0].slice(1);
            }
            s = nSign + parts.join(settings.aDec);
        }
        if ((strip_zero && settings.lZero === 'deny') || (strip_zero && settings.lZero === 'allow' && settings.allowLeading === false)) {
            var strip_reg = '^' + settings.aNegRegAutoStrip + '0*(\\d' + (strip_zero === 'leading' ? ')' : '|$)');
            strip_reg = new RegExp(strip_reg);
            s = s.replace(strip_reg, '$1$2');
        }
        return s;
    }

    function negativeBracket(s, nBracket, oEvent) {
        nBracket = nBracket.split(',');
        if (oEvent === 'set' || oEvent === 'focusout') {
            s = s.replace('-', '');
            s = nBracket[0] + s + nBracket[1];
        } else if ((oEvent === 'get' || oEvent === 'focusin' || oEvent === 'pageLoad') && s.charAt(0) === nBracket[0]) {
            s = s.replace(nBracket[0], '-');
            s = s.replace(nBracket[1], '');
        }
        return s;
    }

    function truncateDecimal(s, aDec, mDec) {
        if (aDec && mDec) {
            var parts = s.split(aDec);
            if (parts[1] && parts[1].length > mDec) {
                if (mDec > 0) {
                    parts[1] = parts[1].substring(0, mDec);
                    s = parts.join(aDec);
                } else {
                    s = parts[0];
                }
            }
        }
        return s;
    }

    function fixNumber(s, aDec, aNeg) {
        if (aDec && aDec !== '.') {
            s = s.replace(aDec, '.');
        }
        if (aNeg && aNeg !== '-') {
            s = s.replace(aNeg, '-');
        }
        if (!s.match(/\d/)) {
            s += '0';
        }
        return s;
    }

    function checkValue(value) {
        var valueIn = value;
        value = +value;
        if (value < 0.000001 && value > 0) {
            value = (value + 1).toString();
            value = value.substring(1);
        }
        if (value < 0 && value > -1) {
            value = (value - 1).toString();
            value = '-' + value.substring(2);
        }
        if (valueIn === "") {
            return '';
        }
        return value.toString();
    }

    function presentNumber(s, aDec, aNeg) {
        if (aNeg && aNeg !== '-') {
            s = s.replace('-', aNeg);
        }
        if (aDec && aDec !== '.') {
            s = s.replace('.', aDec);
        }
        return s;
    }

    function autoCheck(s, settings) {
        s = autoStrip(s, settings);
        s = truncateDecimal(s, settings.aDec, settings.mDec);
        s = fixNumber(s, settings.aDec, settings.aNeg);
        var value = +s;
        if (settings.oEvent === 'set' && (value < settings.vMin || value > settings.vMax)) {
            $.error("The value (" + value + ") from the 'set' method falls outside of the vMin / vMax range");
        }
        return value >= settings.vMin && value <= settings.vMax;
    }

    function checkEmpty(iv, settings, signOnEmpty) {
        if (iv === '' || iv === settings.aNeg) {
            if (settings.wEmpty === 'zero') {
                return iv + '0';
            }
            if (settings.wEmpty === 'sign' || signOnEmpty) {
                return iv + settings.aSign;
            }
            return iv;
        }
        return null;
    }

    function autoGroup(iv, settings) {
        iv = autoStrip(iv, settings);
        var testNeg = iv,
            empty = checkEmpty(iv, settings, true);
        if (empty !== null) {
            return empty;
        }
        var digitalGroup = '';
        if (settings.dGroup === 2) {
            digitalGroup = /(\d)((\d)(\d{2}?)+)$/;
        } else if (settings.dGroup === 4) {
            digitalGroup = /(\d)((\d{4}?)+)$/;
        } else {
            digitalGroup = /(\d)((\d{3}?)+)$/;
        }
        var ivSplit = iv.split(settings.aDec);
        if (settings.altDec && ivSplit.length === 1) {
            ivSplit = iv.split(settings.altDec);
        }
        var s = ivSplit[0];
        if (settings.aSep) {
            while (digitalGroup.test(s)) {
                s = s.replace(digitalGroup, '$1' + settings.aSep + '$2');
            }
        }
        if (settings.mDec !== 0 && ivSplit.length > 1) {
            if (ivSplit[1].length > settings.mDec) {
                ivSplit[1] = ivSplit[1].substring(0, settings.mDec);
            }
            iv = s + settings.aDec + ivSplit[1];
        } else {
            iv = s;
        }
        if (settings.aSign) {
            var has_aNeg = iv.indexOf(settings.aNeg) !== -1;
            iv = iv.replace(settings.aNeg, '');
            iv = settings.pSign === 'p' ? settings.aSign + iv : iv + settings.aSign;
            if (has_aNeg) {
                iv = settings.aNeg + iv;
            }
        }
        if (settings.oEvent === 'set' && testNeg < 0 && settings.nBracket !== null) {
            iv = negativeBracket(iv, settings.nBracket, settings.oEvent);
        }
        return iv;
    }

    function autoRound(iv, settings) {
        iv = (iv === '') ? '0' : iv.toString();
        convertKeyToNumber(settings, 'mDec');
        var ivRounded = '',
            i = 0,
            nSign = '',
            rDec = (typeof(settings.aPad) === 'boolean' || settings.aPad === null) ? (settings.aPad ? settings.mDec : 0) : +settings.aPad;
        var truncateZeros = function(ivRounded) {
            var regex = rDec === 0 ? (/(\.[1-9]*)0*$/) : rDec === 1 ? (/(\.\d[1-9]*)0*$/) : new RegExp('(\\.\\d{' + rDec + '}[1-9]*)0*$');
            ivRounded = ivRounded.replace(regex, '$1');
            if (rDec === 0) {
                ivRounded = ivRounded.replace(/\.$/, '');
            }
            return ivRounded;
        };
        if (iv.charAt(0) === '-') {
            nSign = '-';
            iv = iv.replace('-', '');
        }
        if (!iv.match(/^\d/)) {
            iv = '0' + iv;
        }
        if (nSign === '-' && +iv === 0) {
            nSign = '';
        }
        if ((+iv > 0 && settings.lZero !== 'keep') || (iv.length > 0 && settings.lZero === 'allow')) {
            iv = iv.replace(/^0*(\d)/, '$1');
        }
        var dPos = iv.lastIndexOf('.');
        var vdPos = dPos === -1 ? iv.length - 1 : dPos;
        var cDec = (iv.length - 1) - vdPos;
        if (cDec <= settings.mDec) {
            ivRounded = iv;
            if (cDec < rDec) {
                if (dPos === -1) {
                    ivRounded += '.';
                }
                while (cDec < rDec) {
                    var zeros = '000000'.substring(0, rDec - cDec);
                    ivRounded += zeros;
                    cDec += zeros.length;
                }
            } else if (cDec > rDec) {
                ivRounded = truncateZeros(ivRounded);
            } else if (cDec === 0 && rDec === 0) {
                ivRounded = ivRounded.replace(/\.$/, '');
            }
            return nSign + ivRounded;
        }
        var rLength = dPos + settings.mDec;
        var tRound = +iv.charAt(rLength + 1);
        var ivArray = iv.substring(0, rLength + 1).split('');
        var odd = (iv.charAt(rLength) === '.') ? (iv.charAt(rLength - 1) % 2) : (iv.charAt(rLength) % 2);
        if ((tRound > 4 && settings.mRound === 'S') || (tRound > 4 && settings.mRound === 'A' && nSign === '') || (tRound > 5 && settings.mRound === 'A' && nSign === '-') || (tRound > 5 && settings.mRound === 's') || (tRound > 5 && settings.mRound === 'a' && nSign === '') || (tRound > 4 && settings.mRound === 'a' && nSign === '-') || (tRound > 5 && settings.mRound === 'B') || (tRound === 5 && settings.mRound === 'B' && odd === 1) || (tRound > 0 && settings.mRound === 'C' && nSign === '') || (tRound > 0 && settings.mRound === 'F' && nSign === '-') || (tRound > 0 && settings.mRound === 'U')) {
            for (i = (ivArray.length - 1); i >= 0; i -= 1) {
                if (ivArray[i] !== '.') {
                    ivArray[i] = +ivArray[i] + 1;
                    if (ivArray[i] < 10) {
                        break;
                    } else if (i > 0) {
                        ivArray[i] = '0';
                    }
                }
            }
        }
        ivArray = ivArray.slice(0, rLength + 1);
        ivRounded = truncateZeros(ivArray.join(''));
        return nSign + ivRounded;
    }

    function AutoNumericHolder(that, settings) {
        this.settings = settings;
        this.that = that;
        this.$that = $(that);
        this.formatted = false;
        this.settingsClone = autoCode(this.$that, this.settings);
        this.value = that.value;
    }
    AutoNumericHolder.prototype = {
        init: function(e) {
            this.value = this.that.value;
            this.settingsClone = autoCode(this.$that, this.settings);
            this.ctrlKey = e.ctrlKey;
            this.cmdKey = e.metaKey;
            this.shiftKey = e.shiftKey;
            this.selection = getElementSelection(this.that);
            if (e.type === 'keydown' || e.type === 'keyup') {
                this.kdCode = e.keyCode;
            }
            this.which = e.which;
            this.processed = false;
            this.formatted = false;
        },
        setSelection: function(start, end, setReal) {
            start = Math.max(start, 0);
            end = Math.min(end, this.that.value.length);
            this.selection = {
                start: start,
                end: end,
                length: end - start
            };
            if (setReal === undefined || setReal) {
                setElementSelection(this.that, start, end);
            }
        },
        setPosition: function(pos, setReal) {
            this.setSelection(pos, pos, setReal);
        },
        getBeforeAfter: function() {
            var value = this.value;
            var left = value.substring(0, this.selection.start);
            var right = value.substring(this.selection.end, value.length);
            return [left, right];
        },
        getBeforeAfterStriped: function() {
            var parts = this.getBeforeAfter();
            parts[0] = autoStrip(parts[0], this.settingsClone);
            parts[1] = autoStrip(parts[1], this.settingsClone);
            return parts;
        },
        normalizeParts: function(left, right) {
            var settingsClone = this.settingsClone;
            right = autoStrip(right, settingsClone);
            var strip = right.match(/^\d/) ? true : 'leading';
            left = autoStrip(left, settingsClone, strip);
            if ((left === '' || left === settingsClone.aNeg) && settingsClone.lZero === 'deny') {
                if (right > '') {
                    right = right.replace(/^0*(\d)/, '$1');
                }
            }
            var new_value = left + right;
            if (settingsClone.aDec) {
                var m = new_value.match(new RegExp('^' + settingsClone.aNegRegAutoStrip + '\\' + settingsClone.aDec));
                if (m) {
                    left = left.replace(m[1], m[1] + '0');
                    new_value = left + right;
                }
            }
            if (settingsClone.wEmpty === 'zero' && (new_value === settingsClone.aNeg || new_value === '')) {
                left += '0';
            }
            return [left, right];
        },
        setValueParts: function(left, right) {
            var settingsClone = this.settingsClone;
            var parts = this.normalizeParts(left, right);
            var new_value = parts.join('');
            var position = parts[0].length;
            if (autoCheck(new_value, settingsClone)) {
                new_value = truncateDecimal(new_value, settingsClone.aDec, settingsClone.mDec);
                if (position > new_value.length) {
                    position = new_value.length;
                }
                this.value = new_value;
                this.setPosition(position, false);
                return true;
            }
            return false;
        },
        signPosition: function() {
            var settingsClone = this.settingsClone,
                aSign = settingsClone.aSign,
                that = this.that;
            if (aSign) {
                var aSignLen = aSign.length;
                if (settingsClone.pSign === 'p') {
                    var hasNeg = settingsClone.aNeg && that.value && that.value.charAt(0) === settingsClone.aNeg;
                    return hasNeg ? [1, aSignLen + 1] : [0, aSignLen];
                }
                var valueLen = that.value.length;
                return [valueLen - aSignLen, valueLen];
            }
            return [1000, -1];
        },
        expandSelectionOnSign: function(setReal) {
            var sign_position = this.signPosition();
            var selection = this.selection;
            if (selection.start < sign_position[1] && selection.end > sign_position[0]) {
                if ((selection.start < sign_position[0] || selection.end > sign_position[1]) && this.value.substring(Math.max(selection.start, sign_position[0]), Math.min(selection.end, sign_position[1])).match(/^\s*$/)) {
                    if (selection.start < sign_position[0]) {
                        this.setSelection(selection.start, sign_position[0], setReal);
                    } else {
                        this.setSelection(sign_position[1], selection.end, setReal);
                    }
                } else {
                    this.setSelection(Math.min(selection.start, sign_position[0]), Math.max(selection.end, sign_position[1]), setReal);
                }
            }
        },
        checkPaste: function() {
            if (this.valuePartsBeforePaste !== undefined) {
                var parts = this.getBeforeAfter();
                var oldParts = this.valuePartsBeforePaste;
                delete this.valuePartsBeforePaste;
                parts[0] = parts[0].substr(0, oldParts[0].length) + autoStrip(parts[0].substr(oldParts[0].length), this.settingsClone);
                if (!this.setValueParts(parts[0], parts[1])) {
                    this.value = oldParts.join('');
                    this.setPosition(oldParts[0].length, false);
                }
            }
        },
        skipAllways: function(e) {
            var kdCode = this.kdCode,
                which = this.which,
                ctrlKey = this.ctrlKey,
                cmdKey = this.cmdKey;
            if (kdCode === 17 && e.type === 'keyup' && this.valuePartsBeforePaste !== undefined) {
                this.checkPaste();
                return false;
            }
            if ((kdCode >= 112 && kdCode <= 123) || (kdCode >= 91 && kdCode <= 93) || (kdCode >= 9 && kdCode <= 31) || (kdCode < 8 && (which === 0 || which === kdCode)) || kdCode === 144 || kdCode === 145 || kdCode === 45) {
                return true;
            }
            if ((ctrlKey || cmdKey) && kdCode === 65) {
                return true;
            }
            if ((ctrlKey || cmdKey) && (kdCode === 67 || kdCode === 86 || kdCode === 88)) {
                if (e.type === 'keydown') {
                    this.expandSelectionOnSign();
                }
                if (kdCode === 86) {
                    if (e.type === 'keydown' || e.type === 'keypress') {
                        if (this.valuePartsBeforePaste === undefined) {
                            this.valuePartsBeforePaste = this.getBeforeAfter();
                        }
                    } else {
                        this.checkPaste();
                    }
                }
                return e.type === 'keydown' || e.type === 'keypress' || kdCode === 67;
            }
            if (ctrlKey || cmdKey) {
                return true;
            }
            if (kdCode === 37 || kdCode === 39) {
                var aSep = this.settingsClone.aSep,
                    start = this.selection.start,
                    value = this.that.value;
                if (e.type === 'keydown' && aSep && !this.shiftKey) {
                    if (kdCode === 37 && value.charAt(start - 2) === aSep) {
                        this.setPosition(start - 1);
                    } else if (kdCode === 39 && value.charAt(start) === aSep) {
                        this.setPosition(start + 1);
                    }
                }
                return true;
            }
            if (kdCode >= 34 && kdCode <= 40) {
                return true;
            }
            return false;
        },
        processAllways: function() {
            var parts;
            if (this.kdCode === 8 || this.kdCode === 46) {
                if (!this.selection.length) {
                    parts = this.getBeforeAfterStriped();
                    if (this.kdCode === 8) {
                        parts[0] = parts[0].substring(0, parts[0].length - 1);
                    } else {
                        parts[1] = parts[1].substring(1, parts[1].length);
                    }
                    this.setValueParts(parts[0], parts[1]);
                } else {
                    this.expandSelectionOnSign(false);
                    parts = this.getBeforeAfterStriped();
                    this.setValueParts(parts[0], parts[1]);
                }
                return true;
            }
            return false;
        },
        processKeypress: function() {
            var settingsClone = this.settingsClone;
            var cCode = String.fromCharCode(this.which);
            var parts = this.getBeforeAfterStriped();
            var left = parts[0],
                right = parts[1];
            if (cCode === settingsClone.aDec || (settingsClone.altDec && cCode === settingsClone.altDec) || ((cCode === '.' || cCode === ',') && this.kdCode === 110)) {
                if (!settingsClone.mDec || !settingsClone.aDec) {
                    return true;
                }
                if (settingsClone.aNeg && right.indexOf(settingsClone.aNeg) > -1) {
                    return true;
                }
                if (left.indexOf(settingsClone.aDec) > -1) {
                    return true;
                }
                if (right.indexOf(settingsClone.aDec) > 0) {
                    return true;
                }
                if (right.indexOf(settingsClone.aDec) === 0) {
                    right = right.substr(1);
                }
                this.setValueParts(left + settingsClone.aDec, right);
                return true;
            }
            if (cCode === '-' || cCode === '+') {
                if (!settingsClone.aNeg) {
                    return true;
                }
                if (left === '' && right.indexOf(settingsClone.aNeg) > -1) {
                    left = settingsClone.aNeg;
                    right = right.substring(1, right.length);
                }
                if (left.charAt(0) === settingsClone.aNeg) {
                    left = left.substring(1, left.length);
                } else {
                    left = (cCode === '-') ? settingsClone.aNeg + left : left;
                }
                this.setValueParts(left, right);
                return true;
            }
            if (cCode >= '0' && cCode <= '9') {
                if (settingsClone.aNeg && left === '' && right.indexOf(settingsClone.aNeg) > -1) {
                    left = settingsClone.aNeg;
                    right = right.substring(1, right.length);
                }
                this.setValueParts(left + cCode, right);
                return true;
            }
            return true;
        },
        formatQuick: function() {
            var settingsClone = this.settingsClone;
            var parts = this.getBeforeAfterStriped();
            var leftLength = this.value;
            if ((settingsClone.aSep === '' || (settingsClone.aSep !== '' && leftLength.indexOf(settingsClone.aSep) === -1)) && (settingsClone.aSign === '' || (settingsClone.aSign !== '' && leftLength.indexOf(settingsClone.aSign) === -1))) {
                var subParts = [],
                    nSign = '';
                subParts = leftLength.split(settingsClone.aDec);
                if (subParts[0].indexOf('-') > -1) {
                    nSign = '-';
                    subParts[0] = subParts[0].replace('-', '');
                    parts[0] = parts[0].replace('-', '');
                }
                if (subParts[0].length > settingsClone.mInt && parts[0].charAt(0) === '0') {
                    parts[0] = parts[0].slice(1);
                }
                parts[0] = nSign + parts[0];
            }
            var value = autoGroup(this.value, this.settingsClone);
            var position = value.length;
            if (value) {
                var left_ar = parts[0].split('');
                var i = 0;
                for (i; i < left_ar.length; i += 1) {
                    if (!left_ar[i].match('\\d')) {
                        left_ar[i] = '\\' + left_ar[i];
                    }
                }
                var leftReg = new RegExp('^.*?' + left_ar.join('.*?'));
                var newLeft = value.match(leftReg);
                if (newLeft) {
                    position = newLeft[0].length;
                    if (((position === 0 && value.charAt(0) !== settingsClone.aNeg) || (position === 1 && value.charAt(0) === settingsClone.aNeg)) && settingsClone.aSign && settingsClone.pSign === 'p') {
                        position = this.settingsClone.aSign.length + (value.charAt(0) === '-' ? 1 : 0);
                    }
                } else if (settingsClone.aSign && settingsClone.pSign === 's') {
                    position -= settingsClone.aSign.length;
                }
            }
            this.that.value = value;
            this.setPosition(position);
            this.formatted = true;
        }
    };

    function autoGet(obj) {
        if (typeof obj === 'string') {
            obj = obj.replace(/\[/g, "\\[").replace(/\]/g, "\\]");
            obj = '#' + obj.replace(/(:|\.)/g, '\\$1');
        }
        return $(obj);
    }

    function getHolder($that, settings) {
        var data = $that.data('autoNumeric');
        if (!data) {
            data = {};
            $that.data('autoNumeric', data);
        }
        var holder = data.holder;
        if (holder === undefined && settings) {
            holder = new AutoNumericHolder($that.get(0), settings);
            data.holder = holder;
        }
        return holder;
    }
    var methods = {
        init: function(options) {
            return this.each(function() {
                var $this = $(this);
                var settings = $this.data('autoNumeric');
                var tagData = $this.data();
                if (typeof settings !== 'object') {
                    var defaults = {
                        aNum: '0123456789',
                        aSep: ',',
                        dGroup: '3',
                        aDec: '.',
                        altDec: null,
                        aSign: '',
                        pSign: 'p',
                        vMax: '999999999.99',
                        vMin: '0.00',
                        mDec: null,
                        mRound: 'S',
                        aPad: true,
                        nBracket: null,
                        wEmpty: 'empty',
                        lZero: 'allow',
                        aForm: true,
                        onSomeEvent: function() {}
                    };
                    settings = $.extend({}, defaults, tagData, options);
                    if (settings.aDec === settings.aSep) {
                        $.error("autoNumeric will not function properly when the decimal character aDec: '" + settings.aDec + "' and thousand seperater aSep: '" + settings.aSep + "' are the same character");
                        return this;
                    }
                    if ($.inArray($this.prop('tagName'), settings.tagList) !== -1) {
                        $.error("The <" + $this.prop('tagName') + "> is not supported by autoNumeric()");
                        return this;
                    }
                    $this.data('autoNumeric', settings);
                } else {
                    return this;
                }
                var holder = getHolder($this, settings);
                if (settings.runOnce === undefined && settings.aForm && ($this[0].value || $this.text() !== '')) {
                    if ($this.is('input[type=text], input[type=hidden], input:not([type])')) {
                        if (settings.nBracket !== null && ($this[0].value || settings.wEmpty !== 'empty')) {
                            settings.oEvent = "pageLoad";
                            $this[0].value = negativeBracket($this[0].value, settings.nBracket, settings.oEvent);
                            $this[0].value = autoStrip($this[0].value, settings);
                        }
                        if ($this[0].value !== $($this[0]).attr("value")) {
                            $this.autoNumeric('set', autoStrip($this.val(), settings));
                        } else {
                            $this.autoNumeric('set', $this.val());
                        }
                    }
                    if ($.inArray($this.prop('tagName'), settings.tagList) !== -1) {
                        $this.autoNumeric('set', $this.text());
                    }
                }
                settings.runOnce = true;
                if ($this.is('input[type=text], input[type=hidden], input:not([type])') && !$this.is('[readonly]')) {
                    $this.bind('keydown.autoNumeric', function(e) {
                        holder = getHolder($this);
                        if (holder.settings.aDec === holder.settings.aSep) {
                            $.error("autoNumeric will not function properly when the decimal character aDec: '" + holder.settings.aDec + "' and thousand seperater aSep: '" + holder.settings.aSep + "' are the same character");
                            return this;
                        }
                        if (holder.that.readOnly) {
                            holder.processed = true;
                            return true;
                        }
                        holder.init(e);
                        holder.settings.oEvent = 'keydown';
                        if (holder.skipAllways(e)) {
                            holder.processed = true;
                            return true;
                        }
                        if (holder.processAllways()) {
                            holder.processed = true;
                            holder.formatQuick();
                            e.preventDefault();
                            return false;
                        }
                        holder.formatted = false;
                        return true;
                    });
                    $this.bind('keypress.autoNumeric', function(e) {
                        var holder = getHolder($this),
                            processed = holder.processed;
                        holder.init(e);
                        holder.settings.oEvent = 'keypress';
                        if (holder.skipAllways(e)) {
                            return true;
                        }
                        if (processed) {
                            e.preventDefault();
                            return false;
                        }
                        if (holder.processAllways() || holder.processKeypress()) {
                            holder.formatQuick();
                            e.preventDefault();
                            return false;
                        }
                        holder.formatted = false;
                    });
                    $this.bind('keyup.autoNumeric', function(e) {
                        var holder = getHolder($this);
                        holder.init(e);
                        holder.settings.oEvent = 'keyup';
                        var skip = holder.skipAllways(e);
                        holder.kdCode = 0;
                        delete holder.valuePartsBeforePaste;
                        if (skip) {
                            return true;
                        }
                        if (this.value === '') {
                            return true;
                        }
                        if (!holder.formatted) {
                            holder.formatQuick();
                        }
                    });
                    $this.bind('focusin.autoNumeric', function() {
                        var holder = getHolder($this);
                        holder.settingsClone.oEvent = 'focusin';
                        if (holder.settingsClone.nBracket !== null) {
                            var checkVal = $this.val();
                            $this.val(negativeBracket(checkVal, holder.settingsClone.nBracket, holder.settingsClone.oEvent));
                        }
                        holder.inVal = $this.val();
                        var onempty = checkEmpty(holder.inVal, holder.settingsClone, true);
                        if (onempty !== null) {
                            $this.val(onempty);
                        }
                    });
                    $this.bind('focusout.autoNumeric', function() {
                        var holder = getHolder($this),
                            settingsClone = holder.settingsClone,
                            value = $this.val(),
                            origValue = value;
                        holder.settingsClone.oEvent = 'focusout';
                        var strip_zero = '';
                        if (settingsClone.lZero === 'allow') {
                            settingsClone.allowLeading = false;
                            strip_zero = 'leading';
                        }
                        if (value !== '') {
                            value = autoStrip(value, settingsClone, strip_zero);
                            if (checkEmpty(value, settingsClone) === null && autoCheck(value, settingsClone, $this[0])) {
                                value = fixNumber(value, settingsClone.aDec, settingsClone.aNeg);
                                value = autoRound(value, settingsClone);
                                value = presentNumber(value, settingsClone.aDec, settingsClone.aNeg);
                            } else {
                                value = '';
                            }
                        }
                        var groupedValue = checkEmpty(value, settingsClone, false);
                        if (groupedValue === null) {
                            groupedValue = autoGroup(value, settingsClone);
                        }
                        if (groupedValue !== origValue) {
                            $this.val(groupedValue);
                        }
                        if (groupedValue !== holder.inVal) {
                            $this.change();
                            delete holder.inVal;
                        }
                        if (settingsClone.nBracket !== null && $this.autoNumeric('get') < 0) {
                            holder.settingsClone.oEvent = 'focusout';
                            $this.val(negativeBracket($this.val(), settingsClone.nBracket, settingsClone.oEvent));
                        }
                    });
                } else if ($this.is('input[type=text], input[type=hidden], input:not([type])') && $this.is('[readonly]')) {
                    this.blur();
                }
            });
        },
        destroy: function() {
            return $(this).each(function() {
                var $this = $(this);
                $this.unbind('.autoNumeric');
                $this.removeData('autoNumeric');
            });
        },
        update: function(options) {
            return $(this).each(function() {
                var $this = autoGet($(this)),
                    settings = $this.data('autoNumeric');
                if (typeof settings !== 'object') {
                    $.error("You must initialize autoNumeric('init', {options}) prior to calling the 'update' method");
                    return this;
                }
                var strip = $this.autoNumeric('get');
                settings = $.extend(settings, options);
                if (settings.aDec === settings.aSep) {
                    $.error("autoNumeric will not function properly when the decimal character aDec: '" + settings.aDec + "' and thousand seperater aSep: '" + settings.aSep + "' are the same character");
                    return this;
                }
                $this.data('autoNumeric', settings);
                if ($this.val() !== '' || $this.text() !== '') {
                    return $this.autoNumeric('set', strip);
                }
                return;
            });
        },
        set: function(valueIn) {
            return $(this).each(function() {
                var $this = autoGet($(this)),
                    settings = $this.data('autoNumeric'),
                    value = valueIn;
                value = checkValue(value);
                settings.oEvent = 'set';
                if (typeof settings !== 'object') {
                    $.error("You must initialize autoNumeric('init', {options}) prior to calling the 'set' method");
                    return this;
                }
                value.toString();
                if (value !== '') {
                    value = autoRound(value, settings);
                }
                value = presentNumber(value, settings.aDec, settings.aNeg);
                if (!autoCheck(value, settings)) {
                    value = autoRound('', settings);
                }
                value = autoGroup(value, settings);
                if ($this.is('input[type=text], input[type=hidden], input:not([type])')) {
                    return $this.val(value);
                }
                if ($.inArray($this.prop('tagName'), settings.tagList) !== -1) {
                    return $this.text(value);
                }
                $.error("The <" + $this.prop('tagName') + "> is not supported by autoNumeric()");
                return false;
            });
        },
        get: function() {
            var $this = autoGet($(this)),
                settings = $this.data('autoNumeric');
            settings.oEvent = 'get';
            if (typeof settings !== 'object') {
                $.error("You must initialize autoNumeric('init', {options}) prior to calling the 'get' method");
                return this;
            }
            var getValue = '';
            if ($this.is('input[type=text], input[type=hidden], input:not([type])')) {
                getValue = $this.eq(0).val();
            } else if ($.inArray($this.prop('tagName'), settings.tagList) !== -1) {
                getValue = $this.eq(0).text();
            } else {
                $.error("The <" + $this.prop('tagName') + "> is not supported by autoNumeric()");
                return false;
            }
            if ((getValue === '' && settings.wEmpty === 'empty') || (getValue === settings.aSign && settings.wEmpty === 'sign')) {
                return '';
            }
            if (settings.nBracket !== null && getValue !== '') {
                getValue = negativeBracket(getValue, settings.nBracket, settings.oEvent);
            }
            if (settings.runOnce || settings.aForm === false) {
                getValue = autoStrip(getValue, settings);
            }
            getValue = fixNumber(getValue, settings.aDec, settings.aNeg);
            if (+getValue === 0 && settings.lZero !== 'keep') {
                getValue = '0';
            }
            if (settings.lZero === 'keep') {
                return getValue;
            }
            getValue = checkValue(getValue);
            return getValue;
        },
        getString: function() {
            var isAutoNumeric = false,
                $this = autoGet($(this)),
                str = $this.serialize(),
                parts = str.split('&'),
                i = 0;
            for (i; i < parts.length; i += 1) {
                var miniParts = parts[i].split('=');
                var settings = $('*[name=' + miniParts[0] + ']').data('autoNumeric');
                if (typeof settings === 'object') {
                    if (miniParts[1] !== null && $('*[name=' + miniParts[0] + ']').data('autoNumeric') !== undefined) {
                        miniParts[1] = $('input[name=' + miniParts[0] + ']').autoNumeric('get');
                        parts[i] = miniParts.join('=');
                        isAutoNumeric = true;
                    }
                }
            }
            if (isAutoNumeric === true) {
                return parts.join('&');
            }
            $.error("You must initialize autoNumeric('init', {options}) prior to calling the 'getString' method");
            return this;
        },
        getArray: function() {
            var isAutoNumeric = false,
                $this = autoGet($(this)),
                formFields = $this.serializeArray();
            $.each(formFields, function(i, field) {
                var settings = $('*[name=' + field.name + ']').data('autoNumeric');
                if (typeof settings === 'object') {
                    if (field.value !== '' && $('*[name=' + field.name + ']').data('autoNumeric') !== undefined) {
                        field.value = $('input[name=' + field.name + ']').autoNumeric('get').toString();
                    }
                    isAutoNumeric = true;
                }
            });
            if (isAutoNumeric === true) {
                return formFields;
            }
            $.error("You must initialize autoNumeric('init', {options}) prior to calling the 'getArray' method");
            return this;
        },
        getSettings: function() {
            var $this = autoGet($(this));
            return $this.eq(0).data('autoNumeric');
        }
    };
    $.fn.autoNumeric = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        }
        if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        }
        $.error('Method "' + method + '" is not supported by autoNumeric()');
    };
}(jQuery));
(function($) {
    $.fn.appear = function(fn, options) {
        var settings = $.extend({
            data: undefined,
            one: true
        }, options);
        return this.each(function() {
            var t = $(this);
            t.appeared = false;
            if (!fn) {
                t.trigger('appear', settings.data);
                return;
            }
            var w = settings.container ? $(settings.container) : $(window);
            if (settings.container) {
                w.data('customContainer', true);
            }
            var check = function() {
                if (!t.is(':visible')) {
                    t.appeared = false;
                    return;
                }
                var a = w.scrollLeft();
                var b = w.scrollTop();
                var wh = w.height();
                var ww = w.width();
                var o = t.offset();
                if (w.data('customContainer')) {
                    var cOffset = w.offset();
                    var x = o.left - cOffset.left + a;
                    var y = o.top - cOffset.top + b;
                } else {
                    var x = o.left;
                    var y = o.top;
                }
                if (y + t.height() >= b && y <= b + wh && x + t.width() >= a && x <= a + ww) {
                    if (!t.appeared) t.trigger('appear', settings.data);
                } else {
                    t.appeared = false;
                }
            };
            var modifiedFn = function() {
                t.appeared = true;
                if (settings.one) {
                    w.unbind('scroll', check);
                    var i = $.inArray(check, $.fn.appear.checks);
                    if (i >= 0) $.fn.appear.checks.splice(i, 1);
                }
                fn.apply(this, arguments);
            };
            if (settings.one) t.one('appear', settings.data, modifiedFn);
            else t.bind('appear', settings.data, modifiedFn);
            w.scroll(check);
            $.fn.appear.checks.push(check);
            (check)();
        });
    };
    $.extend($.fn.appear, {
        checks: [],
        timeout: null,
        checkAll: function() {
            var length = $.fn.appear.checks.length;
            if (length > 0)
                while (length--)($.fn.appear.checks[length])();
        },
        run: function() {
            if ($.fn.appear.timeout) clearTimeout($.fn.appear.timeout);
            $.fn.appear.timeout = setTimeout($.fn.appear.checkAll, 20);
        }
    });
    $.each(['append', 'prepend', 'after', 'before', 'attr', 'removeAttr', 'addClass', 'removeClass', 'toggleClass', 'remove', 'css', 'show', 'hide'], function(i, n) {
        var old = $.fn[n];
        if (old) {
            $.fn[n] = function() {
                var r = old.apply(this, arguments);
                $.fn.appear.run();
                return r;
            }
        }
    });
})(jQuery);
(function(a) {
    a.tools = a.tools || {
        version: "v1.2.7"
    }, a.tools.tooltip = {
        conf: {
            effect: "toggle",
            fadeOutSpeed: "fast",
            predelay: 0,
            delay: 30,
            opacity: 1,
            tip: 0,
            fadeIE: !1,
            position: ["top", "center"],
            offset: [0, 0],
            relative: !1,
            cancelDefault: !0,
            events: {
                def: "mouseenter,mouseleave",
                input: "focus,blur",
                widget: "focus mouseenter,blur mouseleave",
                tooltip: "mouseenter,mouseleave"
            },
            layout: "<div/>",
            tipClass: "tooltip"
        },
        addEffect: function(a, c, d) {
            b[a] = [c, d]
        }
    };
    var b = {
        toggle: [function(a) {
            var b = this.getConf(),
                c = this.getTip(),
                d = b.opacity;
            d < 1 && c.css({
                opacity: d
            }), c.show(), a.call()
        }, function(a) {
            this.getTip().hide(), a.call()
        }],
        fade: [function(b) {
            var c = this.getConf();
            !a.browser.msie || c.fadeIE ? this.getTip().fadeTo(c.fadeInSpeed, c.opacity, b) : (this.getTip().show(), b())
        }, function(b) {
            var c = this.getConf();
            !a.browser.msie || c.fadeIE ? this.getTip().fadeOut(c.fadeOutSpeed, b) : (this.getTip().hide(), b())
        }]
    };

    function c(b, c, d) {
        var e = d.relative ? b.position().top : b.offset().top,
            f = d.relative ? b.position().left : b.offset().left,
            g = d.position[0];
        e -= c.outerHeight() - d.offset[0], f += b.outerWidth() + d.offset[1], /iPad/i.test(navigator.userAgent) && (e -= a(window).scrollTop());
        var h = c.outerHeight() + b.outerHeight();
        g == "center" && (e += h / 2), g == "bottom" && (e += h), g = d.position[1];
        var i = c.outerWidth() + b.outerWidth();
        g == "center" && (f -= i / 2), g == "left" && (f -= i);
        return {
            top: e,
            left: f
        }
    }

    function d(d, e) {
        var f = this,
            g = d.add(f),
            h, i = 0,
            j = 0,
            k = d.attr("title"),
            l = d.attr("data-tooltip"),
            m = b[e.effect],
            n, o = d.is(":input"),
            p = o && d.is(":checkbox, :radio, select, :button, :submit"),
            q = d.attr("type"),
            r = e.events[q] || e.events[o ? p ? "widget" : "input" : "def"];
        if (!m) throw "Nonexistent effect \"" + e.effect + "\"";
        r = r.split(/,\s*/);
        if (r.length != 2) throw "Tooltip: bad events configuration for " + q;
        d.on(r[0], function(a) {
            clearTimeout(i), e.predelay ? j = setTimeout(function() {
                f.show(a)
            }, e.predelay) : f.show(a)
        }).on(r[1], function(a) {
            clearTimeout(j), e.delay ? i = setTimeout(function() {
                f.hide(a)
            }, e.delay) : f.hide(a)
        }), k && e.cancelDefault && (d.removeAttr("title"), d.data("title", k)), a.extend(f, {
            show: function(b) {
                if (!h) {
                    l ? h = a(l) : e.tip ? h = a(e.tip).eq(0) : k ? h = a(e.layout).addClass(e.tipClass).appendTo(document.body).hide().append(k) : (h = d.next(), h.length || (h = d.parent().next()));
                    if (!h.length) throw "Cannot find tooltip for " + d
                }
                if (f.isShown()) return f;
                h.stop(!0, !0);
                var o = c(d, h, e);
                e.tip && h.html(d.data("title")), b = a.Event(), b.type = "onBeforeShow", g.trigger(b, [o]);
                if (b.isDefaultPrevented()) return f;
                o = c(d, h, e), h.css({
                    position: "absolute",
                    top: o.top,
                    left: o.left
                }), n = !0, m[0].call(f, function() {
                    b.type = "onShow", n = "full", g.trigger(b)
                });
                var p = e.events.tooltip.split(/,\s*/);
                h.data("__set") || (h.off(p[0]).on(p[0], function() {
                    clearTimeout(i), clearTimeout(j)
                }), p[1] && !d.is("input:not(:checkbox, :radio), textarea") && h.off(p[1]).on(p[1], function(a) {
                    a.relatedTarget != d[0] && d.trigger(r[1].split(" ")[0])
                }), e.tip || h.data("__set", !0));
                return f
            },
            hide: function(c) {
                if (!h || !f.isShown()) return f;
                c = a.Event(), c.type = "onBeforeHide", g.trigger(c);
                if (!c.isDefaultPrevented()) {
                    n = !1, b[e.effect][1].call(f, function() {
                        c.type = "onHide", g.trigger(c)
                    });
                    return f
                }
            },
            isShown: function(a) {
                return a ? n == "full" : n
            },
            getConf: function() {
                return e
            },
            getTip: function() {
                return h
            },
            getTrigger: function() {
                return d
            }
        }), a.each("onHide,onBeforeShow,onShow,onBeforeHide".split(","), function(b, c) {
            a.isFunction(e[c]) && a(f).on(c, e[c]), f[c] = function(b) {
                b && a(f).on(c, b);
                return f
            }
        })
    }
    a.fn.tooltip = function(b) {
        var c = this.data("tooltip");
        if (c) return c;
        b = a.extend(!0, {}, a.tools.tooltip.conf, b), typeof b.position == "string" && (b.position = b.position.split(/,?\s/)), this.each(function() {
            c = new d(a(this), b), a(this).data("tooltip", c)
        });
        return b.api ? c : this
    }
})(jQuery);
(function(a) {
    var b = a.tools.tooltip;
    b.dynamic = {
        conf: {
            classNames: "top right bottom left"
        }
    };

    function c(b) {
        var c = a(window),
            d = c.width() + c.scrollLeft(),
            e = c.height() + c.scrollTop();
        return [b.offset().top <= c.scrollTop(), d <= b.offset().left + b.width(), e <= b.offset().top + b.height(), c.scrollLeft() >= b.offset().left]
    }

    function d(a) {
        var b = a.length;
        while (b--)
            if (a[b]) return !1;
        return !0
    }
    a.fn.dynamic = function(e) {
        typeof e == "number" && (e = {
            speed: e
        }), e = a.extend({}, b.dynamic.conf, e);
        var f = a.extend(!0, {}, e),
            g = e.classNames.split(/\s/),
            h;
        this.each(function() {
            var b = a(this).tooltip().onBeforeShow(function(b, e) {
                var i = this.getTip(),
                    j = this.getConf();
                h || (h = [j.position[0], j.position[1], j.offset[0], j.offset[1], a.extend({}, j)]), a.extend(j, h[4]), j.position = [h[0], h[1]], j.offset = [h[2], h[3]], i.css({
                    visibility: "hidden",
                    position: "absolute",
                    top: e.top,
                    left: e.left
                }).show();
                var k = a.extend(!0, {}, f),
                    l = c(i);
                if (!d(l)) {
                    l[2] && (a.extend(j, k.top), j.position[0] = "top", i.addClass(g[0])), l[3] && (a.extend(j, k.right), j.position[1] = "right", i.addClass(g[1])), l[0] && (a.extend(j, k.bottom), j.position[0] = "bottom", i.addClass(g[2])), l[1] && (a.extend(j, k.left), j.position[1] = "left", i.addClass(g[3]));
                    if (l[0] || l[2]) j.offset[0] *= -1;
                    if (l[1] || l[3]) j.offset[1] *= -1
                }
                i.css({
                    visibility: "visible"
                }).hide()
            });
            b.onBeforeShow(function() {
                var a = this.getConf(),
                    b = this.getTip();
                setTimeout(function() {
                    a.position = [h[0], h[1]], a.offset = [h[2], h[3]]
                }, 0)
            }), b.onHide(function() {
                var a = this.getTip();
                a.removeClass(e.classNames)
            }), ret = b
        });
        return e.api ? ret : this
    }
})(jQuery);
(function(a) {
    var b = a.tools.tooltip;
    a.extend(b.conf, {
        direction: "up",
        bounce: !1,
        slideOffset: 10,
        slideInSpeed: 200,
        slideOutSpeed: 200,
        slideFade: !a.browser.msie
    });
    var c = {
        up: ["-", "top"],
        down: ["+", "top"],
        left: ["-", "left"],
        right: ["+", "left"]
    };
    b.addEffect("slide", function(a) {
        var b = this.getConf(),
            d = this.getTip(),
            e = b.slideFade ? {
                opacity: b.opacity
            } : {},
            f = c[b.direction] || c.up;
        e[f[1]] = f[0] + "=" + b.slideOffset, b.slideFade && d.css({
            opacity: 0
        }), d.show().animate(e, b.slideInSpeed, a)
    }, function(b) {
        var d = this.getConf(),
            e = d.slideOffset,
            f = d.slideFade ? {
                opacity: 0
            } : {},
            g = c[d.direction] || c.up,
            h = "" + g[0];
        d.bounce && (h = h == "+" ? "-" : "+"), f[g[1]] = h + "=" + e, this.getTip().animate(f, d.slideOutSpeed, function() {
            a(this).hide(), b.call()
        })
    })
})(jQuery);
(function(_, $) {
    $.widget("ui.dialog", $.ui.dialog, {
        _allowInteraction: function(event) {
            if (this._super(event)) {
                return true;
            }
            if (event.target.ownerDocument != this.document[0]) {
                return true;
            }
            if ($(event.target).closest(".ui-draggable").length) {
                return true;
            }
            if ($(event.target).closest(".cke").length) {
                return true;
            }
        },
        _moveToTop: function(event, silent) {
            if (!event || !this.options.modal) {
                this._super(event, silent);
            }
        }
    });
    var methods = {
        _getEditor: function(elm) {
            var obj = $('#' + elm.prop('id'));
            if (obj.data('redactor')) {
                return obj;
            }
            return false;
        }
    };
    $.ceEditor('handlers', {
        editorName: 'redactor',
        params: null,
        elms: [],
        run: function(elm, params) {
            var support_langs = ['ar', 'az', 'ba', 'bg', 'by', 'ca', 'cs', 'da', 'de', 'el', 'en', 'eo', 'es', 'fa', 'fi', 'fr', 'he', 'hr', 'hu', 'id', 'it', 'ja', 'ko', 'lt', 'lv', 'mk', 'nl', 'no', 'pl', 'pt', 'ro', 'ru', 'sk', 'sl', 'sq', 'sr', 'sv', 'th', 'tr', 'ua', 'vi', 'zh'];
            var lang_map = {
                'no': 'no_NB',
                'pt': 'pt_pt',
                'sr': 'sr-cir',
                'zh': 'zh_tw'
            };
            var lang_code = fn_get_listed_lang(support_langs);
            if (lang_code in lang_map) {
                lang_code = lang_map[lang_code];
            }
            if (typeof($.fn.redactor) == 'undefined') {
                $.ceEditor('state', 'loading');
                $.loadCss(['js/lib/redactor/redactor.css']);
                $.loadCss(['js/lib/elfinder/css/elfinder.min.css']);
                $.loadCss(['js/lib/elfinder/css/theme.css']);
                $.getScript('js/lib/elfinder/js/elfinder.min.js');
                $.getScript('js/lib/redactor/plugins/fontcolor/fontcolor.js');
                $.getScript('js/lib/redactor/plugins/table/table.js');
                $.getScript('js/lib/redactor/plugins/video/video.js');
                $.getScript('js/lib/redactor/plugins/imageupload/imageupload.js');
                $.getScript('js/lib/redactor/redactor.min.js', function() {
                    if (lang_code != 'en') {
                        $.getScript('js/lib/redactor/lang/' + lang_code + '.js', function() {
                            callback();
                        });
                    } else {
                        callback();
                    }
                });
                var callback = function() {
                    $.ceEditor('state', 'loaded');
                    elm.ceEditor('run', params);
                };
                return true;
            }
            if (!this.params) {
                this.params = {
                    lang: lang_code
                };
            }
            if (typeof params !== 'undefined' && params[this.editorName]) {
                $.extend(this.params, params[this.editorName]);
            }
            this.params.initCallback = function() {
                $('.redactor-toolbar-tooltip').each(function() {
                    $(this).css('z-index', 50001);
                });
            }
            this.params.modalOpenedCallback = function() {
                $('#redactor-modal-overlay, #redactor-modal-box, #redactor-modal, .redactor-dropdown').each(function() {
                    $(this).css('z-index', 50001, 'important');
                });
            };
            this.params.dropdownShowCallback = function() {
                $('#redactor-modal-overlay, #redactor-modal-box, #redactor-modal, .redactor-dropdown').each(function() {
                    $(this).css('z-index', 50001, 'important');
                });
            };
            this.params.buttonSource = true;
            this.params.plugins = ['fontcolor', 'table'];
            if (_.area == 'A') {
                this.params.plugins.push('imageupload', 'video');
            }
            this.params.buttons = ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'image', 'video', 'table', 'link', 'alignment', 'horizontalrule'];
            this.params.replaceDivs = false;
            this.params.changeCallback = function(html) {
                elm.ceEditor('changed', html);
            };
            elm.redactor(this.params);
            if (elm.prop('disabled')) {
                elm.ceEditor('disable', true);
            }
            this.elms.push(elm.get(0));
            return true;
        },
        destroy: function(elm) {
            var ed = methods._getEditor(elm);
            if (ed) {
                ed.redactor('core.destroy');
            }
        },
        recover: function(elm) {
            if ($.inArray(elm.get(0), this.elms) !== -1) {
                $.ceEditor('run', elm);
            }
        },
        val: function(elm, value) {
            var ed = methods._getEditor(elm);
            if (!ed) {
                return false;
            }
            if (typeof(value) == 'undefined') {
                return ed.redactor('code.get');
            } else {
                ed.redactor('code.set', value);
            }
            return true;
        },
        updateTextFields: function(elm) {
            return true;
        },
        disable: function(elm, value) {
            var ed = methods._getEditor(elm);
            if (ed) {
                var obj = ed.redactor('core.getBox');
                if (value == true) {
                    if (!$(obj).parent().hasClass('disable-overlay-wrap')) {
                        $(obj).wrap("<div class='disable-overlay-wrap wysiwyg-overlay'></div>");
                        $(obj).before("<div id='" + elm.prop('id') + "_overlay' class='disable-overlay'></div>");
                        elm.prop('disabled', true);
                    }
                } else {
                    $(obj).unwrap();
                    $('#' + elm.prop('id') + '_overlay').remove();
                    elm.prop('disabled', false);
                }
            }
        }
    });
}(Tygh, Tygh.$));
(function(_, $) {
    $.ceEvent('on', 'ce.history_load', function(url) {
        if (typeof(ga) != 'undefined') {
            ga('send', 'pageview', url.replace('!', ''));
        }
    });
}(Tygh, Tygh.$));
"function" != typeof Object.create && (Object.create = function(t) {
        function e() {}
        return e.prototype = t, new e
    }),
    function(t, e, o, i) {
        var s = {
            init: function(e, o) {
                var i = this;
                i.$elem = t(o), i.options = t.extend({}, t.fn.owlCarousel.options, i.$elem.data(), e), i.userOptions = e, i.loadContent()
            },
            loadContent: function() {
                function e(t) {
                    if ("function" == typeof o.options.jsonSuccess) o.options.jsonSuccess.apply(this, [t]);
                    else {
                        var e = "";
                        for (var i in t.owl) e += t.owl[i].item;
                        o.$elem.html(e)
                    }
                    o.logIn()
                }
                var o = this;
                if ("function" == typeof o.options.beforeInit && o.options.beforeInit.apply(this, [o.$elem]), "string" == typeof o.options.jsonPath) {
                    var i = o.options.jsonPath;
                    t.getJSON(i, e)
                } else o.logIn()
            },
            logIn: function() {
                var t = this;
                t.$elem.data("owl-originalStyles", t.$elem.attr("style")).data("owl-originalClasses", t.$elem.attr("class")), t.$elem.css({
                    opacity: 0
                }), t.orignalItems = t.options.items, t.checkBrowser(), t.wrapperWidth = 0, t.checkVisible, t.setVars()
            },
            setVars: function() {
                var t = this;
                return 0 === t.$elem.children().length ? !1 : (t.baseClass(), t.eventTypes(), t.$userItems = t.$elem.children(), t.itemsAmount = t.$userItems.length, t.wrapItems(), t.$owlItems = t.$elem.find(".owl-item"), t.$owlWrapper = t.$elem.find(".owl-wrapper"), t.playDirection = "next", t.prevItem = 0, t.prevArr = [0], t.currentItem = 0, t.customEvents(), void t.onStartup())
            },
            onStartup: function() {
                var t = this;
                t.updateItems(), t.calculateAll(), t.buildControls(), t.updateControls(), t.response(), t.moveEvents(), t.stopOnHover(), t.owlStatus(), t.options.transitionStyle !== !1 && t.transitionTypes(t.options.transitionStyle), t.options.autoPlay === !0 && (t.options.autoPlay = 5e3), t.play(), t.$elem.find(".owl-wrapper").css("display", "block"), t.$elem.is(":visible") ? t.$elem.css("opacity", 1) : t.watchVisibility(), t.onstartup = !1, t.eachMoveUpdate(), "function" == typeof t.options.afterInit && t.options.afterInit.apply(this, [t.$elem])
            },
            eachMoveUpdate: function() {
                var t = this;
                t.options.lazyLoad === !0 && t.lazyLoad(), t.options.autoHeight === !0 && t.autoHeight(), t.onVisibleItems(), "function" == typeof t.options.afterAction && t.options.afterAction.apply(this, [t.$elem])
            },
            updateVars: function() {
                var t = this;
                "function" == typeof t.options.beforeUpdate && t.options.beforeUpdate.apply(this, [t.$elem]), t.watchVisibility(), t.updateItems(), t.calculateAll(), t.updatePosition(), t.updateControls(), t.eachMoveUpdate(), "function" == typeof t.options.afterUpdate && t.options.afterUpdate.apply(this, [t.$elem])
            },
            reload: function() {
                var t = this;
                setTimeout(function() {
                    t.updateVars()
                }, 0)
            },
            watchVisibility: function() {
                var t = this;
                return t.$elem.is(":visible") !== !1 ? !1 : (t.$elem.css({
                    opacity: 0
                }), clearInterval(t.autoPlayInterval), clearInterval(t.checkVisible), void(t.checkVisible = setInterval(function() {
                    t.$elem.is(":visible") && (t.reload(), t.$elem.animate({
                        opacity: 1
                    }, 200), clearInterval(t.checkVisible))
                }, 500)))
            },
            wrapItems: function() {
                var t = this;
                t.$userItems.wrapAll('<div class="owl-wrapper">').wrap('<div class="owl-item"></div>'), t.$elem.find(".owl-wrapper").wrap('<div class="owl-wrapper-outer">'), t.wrapperOuter = t.$elem.find(".owl-wrapper-outer"), t.$elem.css("display", "block")
            },
            baseClass: function() {
                var t = this,
                    e = t.$elem.hasClass(t.options.baseClass),
                    o = t.$elem.hasClass(t.options.theme);
                e || t.$elem.addClass(t.options.baseClass), o || t.$elem.addClass(t.options.theme)
            },
            updateItems: function() {
                var e = this;
                if (e.options.responsive === !1) return !1;
                if (e.options.singleItem === !0) return e.options.items = e.orignalItems = 1, e.options.itemsCustom = !1, e.options.itemsDesktop = !1, e.options.itemsDesktopSmall = !1, e.options.itemsTablet = !1, e.options.itemsTabletSmall = !1, e.options.itemsMobile = !1, !1;
                var o = t(e.options.responsiveBaseWidth).width();
                if (o > (e.options.itemsDesktop[0] || e.orignalItems) && (e.options.items = e.orignalItems), "undefined" != typeof e.options.itemsCustom && e.options.itemsCustom !== !1) {
                    e.options.itemsCustom.sort(function(t, e) {
                        return t[0] - e[0]
                    });
                    for (var i in e.options.itemsCustom) "undefined" != typeof e.options.itemsCustom[i] && e.options.itemsCustom[i][0] <= o && (e.options.items = e.options.itemsCustom[i][1])
                } else o <= e.options.itemsDesktop[0] && e.options.itemsDesktop !== !1 && (e.options.items = e.options.itemsDesktop[1]), o <= e.options.itemsDesktopSmall[0] && e.options.itemsDesktopSmall !== !1 && (e.options.items = e.options.itemsDesktopSmall[1]), o <= e.options.itemsTablet[0] && e.options.itemsTablet !== !1 && (e.options.items = e.options.itemsTablet[1]), o <= e.options.itemsTabletSmall[0] && e.options.itemsTabletSmall !== !1 && (e.options.items = e.options.itemsTabletSmall[1]), o <= e.options.itemsMobile[0] && e.options.itemsMobile !== !1 && (e.options.items = e.options.itemsMobile[1]);
                e.options.items > e.itemsAmount && e.options.itemsScaleUp === !0 && (e.options.items = e.itemsAmount)
            },
            response: function() {
                var o, i = this;
                if (i.options.responsive !== !0) return !1;
                var s = t(e).width();
                i.resizer = function() {
                    t(e).width() !== s && (i.options.autoPlay !== !1 && clearInterval(i.autoPlayInterval), clearTimeout(o), o = setTimeout(function() {
                        s = t(e).width(), i.updateVars()
                    }, i.options.responsiveRefreshRate))
                }, t(e).resize(i.resizer)
            },
            updatePosition: function() {
                var t = this;
                t.jumpTo(t.currentItem), t.options.autoPlay !== !1 && t.checkAp()
            },
            appendItemsSizes: function() {
                var e = this,
                    o = 0,
                    i = e.itemsAmount - e.options.items;
                e.$owlItems.each(function(s) {
                    var n = t(this);
                    n.css({
                        width: e.itemWidth
                    }).data("owl-item", Number(s)), (s % e.options.items === 0 || s === i) && (s > i || (o += 1)), n.data("owl-roundPages", o)
                })
            },
            appendWrapperSizes: function() {
                var t, e = this,
                    o = e.$owlItems.length * e.itemWidth;
                t = "rtl" == e.options.direction ? {
                    right: 0,
                    direction: "rtl"
                } : {
                    left: 0
                }, e.$owlWrapper.css({
                    width: o
                }), e.$owlWrapper.css(t), e.appendItemsSizes()
            },
            calculateAll: function() {
                var t = this;
                t.calculateWidth(), t.appendWrapperSizes(), t.loops(), t.max()
            },
            calculateWidth: function() {
                var t = this;
                t.itemWidth = Math.round(t.$elem.width() / t.options.items)
            },
            max: function() {
                var t = this,
                    e = -1 * (t.itemsAmount * t.itemWidth - t.options.items * t.itemWidth);
                return t.options.items > t.itemsAmount ? (t.maximumItem = 0, e = 0, t.maximumPixels = 0) : (t.maximumItem = t.itemsAmount - t.options.items, t.maximumPixels = e), e
            },
            min: function() {
                return 0
            },
            loops: function() {
                var e = this;
                e.positionsInArray = [0], e.pagesInArray = [];
                for (var o = 0, i = 0, s = 0; s < e.itemsAmount; s++)
                    if (i += e.itemWidth, e.positionsInArray.push(-i), e.options.scrollPerPage === !0) {
                        var n = t(e.$owlItems[s]),
                            a = n.data("owl-roundPages");
                        a !== o && (e.pagesInArray[o] = e.positionsInArray[s], o = a)
                    }
            },
            buildControls: function() {
                var e = this;
                (e.options.navigation === !0 || e.options.pagination === !0) && (e.owlControls = t('<div class="owl-controls"/>').toggleClass("clickable", !e.browser.isTouch).appendTo(e.$elem)), e.options.pagination === !0 && e.buildPagination(), e.options.navigation === !0 && e.buildButtons()
            },
            buildButtons: function() {
                var e = this,
                    o = t('<div class="owl-buttons"/>');
                e.owlControls.append(o), e.buttonPrev = t("<div/>", {
                    "class": "owl-prev",
                    html: e.options.navigationText[0] || ""
                }), e.buttonNext = t("<div/>", {
                    "class": "owl-next",
                    html: e.options.navigationText[1] || ""
                }), o.append(e.buttonPrev).append(e.buttonNext), o.on("touchstart.owlControls mousedown.owlControls", 'div[class^="owl"]', function(t) {
                    t.preventDefault()
                }), o.on("touchend.owlControls mouseup.owlControls", 'div[class^="owl"]', function(o) {
                    o.preventDefault(), t(this).hasClass("owl-next") ? e.next() : e.prev()
                })
            },
            buildPagination: function() {
                var e = this;
                e.paginationWrapper = t('<div class="owl-pagination"/>'), e.owlControls.append(e.paginationWrapper), e.paginationWrapper.on("touchend.owlControls mouseup.owlControls", ".owl-page", function(o) {
                    o.preventDefault(), Number(t(this).data("owl-page")) !== e.currentItem && e.goTo(Number(t(this).data("owl-page")), !0)
                })
            },
            updatePagination: function() {
                var e = this;
                if (e.options.pagination === !1) return !1;
                e.paginationWrapper.html("");
                for (var o = 0, i = e.itemsAmount - e.itemsAmount % e.options.items, s = 0; s < e.itemsAmount; s++)
                    if (s % e.options.items === 0) {
                        if (o += 1, i === s) var n = e.itemsAmount - e.options.items;
                        var a = t("<div/>", {
                                "class": "owl-page"
                            }),
                            r = t("<span></span>", {
                                text: e.options.paginationNumbers === !0 ? o : "",
                                "class": e.options.paginationNumbers === !0 ? "owl-numbers" : ""
                            });
                        a.append(r), a.data("owl-page", i === s ? n : s), a.data("owl-roundPages", o), e.paginationWrapper.append(a)
                    }
                e.checkPagination()
            },
            checkPagination: function() {
                var e = this;
                return e.options.pagination === !1 ? !1 : void e.paginationWrapper.find(".owl-page").each(function() {
                    t(this).data("owl-roundPages") === t(e.$owlItems[e.currentItem]).data("owl-roundPages") && (e.paginationWrapper.find(".owl-page").removeClass("active"), t(this).addClass("active"))
                })
            },
            checkNavigation: function() {
                var t = this;
                return t.options.navigation === !1 ? !1 : void(t.options.rewindNav === !1 && (0 === t.currentItem && 0 === t.maximumItem ? (t.buttonPrev.addClass("disabled"), t.buttonNext.addClass("disabled")) : 0 === t.currentItem && 0 !== t.maximumItem ? (t.buttonPrev.addClass("disabled"), t.buttonNext.removeClass("disabled")) : t.currentItem === t.maximumItem ? (t.buttonPrev.removeClass("disabled"), t.buttonNext.addClass("disabled")) : 0 !== t.currentItem && t.currentItem !== t.maximumItem && (t.buttonPrev.removeClass("disabled"), t.buttonNext.removeClass("disabled"))))
            },
            updateControls: function() {
                var t = this;
                t.updatePagination(), t.checkNavigation(), t.owlControls && (t.options.items >= t.itemsAmount ? t.owlControls.hide() : t.owlControls.show())
            },
            destroyControls: function() {
                var t = this;
                t.owlControls && t.owlControls.remove()
            },
            next: function(t) {
                var e = this;
                if (e.isTransition) return !1;
                if (e.currentItem += e.options.scrollPerPage === !0 ? e.options.items : 1, e.currentItem > e.maximumItem + (1 == e.options.scrollPerPage ? e.options.items - 1 : 0)) {
                    if (e.options.rewindNav !== !0) return e.currentItem = e.maximumItem, !1;
                    e.currentItem = 0, t = "rewind"
                }
                e.goTo(e.currentItem, t)
            },
            prev: function(t) {
                var e = this;
                if (e.isTransition) return !1;
                if (e.options.scrollPerPage === !0 && e.currentItem > 0 && e.currentItem < e.options.items ? e.currentItem = 0 : e.currentItem -= e.options.scrollPerPage === !0 ? e.options.items : 1, e.currentItem < 0) {
                    if (e.options.rewindNav !== !0) return e.currentItem = 0, !1;
                    e.currentItem = e.maximumItem, t = "rewind"
                }
                e.goTo(e.currentItem, t)
            },
            goTo: function(t, e, o) {
                var i = this;
                if (i.isTransition) return !1;
                if ("function" == typeof i.options.beforeMove && i.options.beforeMove.apply(this, [i.$elem]), t >= i.maximumItem ? t = i.maximumItem : 0 >= t && (t = 0), i.currentItem = i.owl.currentItem = t, i.options.transitionStyle !== !1 && "drag" !== o && 1 === i.options.items && i.browser.support3d === !0) return i.swapSpeed(0), i.browser.support3d === !0 ? i.transition3d(i.positionsInArray[t]) : i.css2slide(i.positionsInArray[t], 1), i.afterGo(), i.singleItemTransition(), !1;
                var s = i.positionsInArray[t];
                i.browser.support3d === !0 ? (i.isCss3Finish = !1, e === !0 ? (i.swapSpeed("paginationSpeed"), setTimeout(function() {
                    i.isCss3Finish = !0
                }, i.options.paginationSpeed)) : "rewind" === e ? (i.swapSpeed(i.options.rewindSpeed), setTimeout(function() {
                    i.isCss3Finish = !0
                }, i.options.rewindSpeed)) : (i.swapSpeed("slideSpeed"), setTimeout(function() {
                    i.isCss3Finish = !0
                }, i.options.slideSpeed)), i.transition3d(s)) : e === !0 ? i.css2slide(s, i.options.paginationSpeed) : "rewind" === e ? i.css2slide(s, i.options.rewindSpeed) : i.css2slide(s, i.options.slideSpeed), i.afterGo()
            },
            jumpTo: function(t) {
                var e = this;
                "function" == typeof e.options.beforeMove && e.options.beforeMove.apply(this, [e.$elem]), t >= e.maximumItem || -1 === t ? t = e.maximumItem : 0 >= t && (t = 0), e.swapSpeed(0), e.browser.support3d === !0 ? e.transition3d(e.positionsInArray[t]) : e.css2slide(e.positionsInArray[t], 1), e.currentItem = e.owl.currentItem = t, e.afterGo()
            },
            afterGo: function() {
                var t = this;
                t.prevArr.push(t.currentItem), t.prevItem = t.owl.prevItem = t.prevArr[t.prevArr.length - 2], t.prevArr.shift(0), t.prevItem !== t.currentItem && (t.checkPagination(), t.checkNavigation(), t.eachMoveUpdate(), t.options.autoPlay !== !1 && t.checkAp()), "function" == typeof t.options.afterMove && t.prevItem !== t.currentItem && t.options.afterMove.apply(this, [t.$elem])
            },
            stop: function() {
                var t = this;
                t.apStatus = "stop", clearInterval(t.autoPlayInterval)
            },
            checkAp: function() {
                var t = this;
                "stop" !== t.apStatus && t.play()
            },
            play: function() {
                var t = this;
                return t.apStatus = "play", t.options.autoPlay === !1 ? !1 : (clearInterval(t.autoPlayInterval), void(t.autoPlayInterval = setInterval(function() {
                    t.next(!0)
                }, t.options.autoPlay)))
            },
            swapSpeed: function(t) {
                var e = this;
                "slideSpeed" === t ? e.$owlWrapper.css(e.addCssSpeed(e.options.slideSpeed)) : "paginationSpeed" === t ? e.$owlWrapper.css(e.addCssSpeed(e.options.paginationSpeed)) : "string" != typeof t && e.$owlWrapper.css(e.addCssSpeed(t))
            },
            addCssSpeed: function(t) {
                return {
                    "-webkit-transition": "all " + t + "ms ease",
                    "-moz-transition": "all " + t + "ms ease",
                    "-o-transition": "all " + t + "ms ease",
                    transition: "all " + t + "ms ease"
                }
            },
            removeTransition: function() {
                return {
                    "-webkit-transition": "",
                    "-moz-transition": "",
                    "-o-transition": "",
                    transition: ""
                }
            },
            doTranslate: function(t) {
                var e = this;
                return t = "rtl" == e.options.direction ? -t : t, {
                    "-webkit-transform": "translate3d(" + t + "px, 0px, 0px)",
                    "-moz-transform": "translate3d(" + t + "px, 0px, 0px)",
                    "-o-transform": "translate3d(" + t + "px, 0px, 0px)",
                    "-ms-transform": "translate3d(" + t + "px, 0px, 0px)",
                    transform: "translate3d(" + t + "px, 0px,0px)"
                }
            },
            transition3d: function(t) {
                var e = this;
                e.$owlWrapper.css(e.doTranslate(t))
            },
            css2move: function(t) {
                var e, o = this;
                e = "rtl" == o.options.direction ? {
                    right: t
                } : {
                    left: t
                }, o.$owlWrapper.css(e)
            },
            css2slide: function(t, e) {
                var o, i = this;
                o = "rtl" == i.options.direction ? {
                    right: t
                } : {
                    left: t
                }, i.isCssFinish = !1, i.$owlWrapper.stop(!0, !0).animate(o, {
                    duration: e || i.options.slideSpeed,
                    complete: function() {
                        i.isCssFinish = !0
                    }
                })
            },
            checkBrowser: function() {
                var t = this,
                    i = "translate3d(0px, 0px, 0px)",
                    s = o.createElement("div");
                s.style.cssText = "  -moz-transform:" + i + "; -ms-transform:" + i + "; -o-transform:" + i + "; -webkit-transform:" + i + "; transform:" + i;
                var n = /translate3d\(0px, 0px, 0px\)/g,
                    a = s.style.cssText.match(n),
                    r = null !== a && 1 === a.length,
                    l = "ontouchstart" in e || navigator.msMaxTouchPoints;
                t.browser = {
                    support3d: r,
                    isTouch: l
                }
            },
            moveEvents: function() {
                var t = this;
                (t.options.mouseDrag !== !1 || t.options.touchDrag !== !1) && (t.gestures(), t.disabledEvents())
            },
            eventTypes: function() {
                var t = this,
                    e = ["s", "e", "x"];
                t.ev_types = {}, t.options.mouseDrag === !0 && t.options.touchDrag === !0 ? e = ["touchstart.owl mousedown.owl", "touchmove.owl mousemove.owl", "touchend.owl touchcancel.owl mouseup.owl"] : t.options.mouseDrag === !1 && t.options.touchDrag === !0 ? e = ["touchstart.owl", "touchmove.owl", "touchend.owl touchcancel.owl"] : t.options.mouseDrag === !0 && t.options.touchDrag === !1 && (e = ["mousedown.owl", "mousemove.owl", "mouseup.owl"]), t.ev_types.start = e[0], t.ev_types.move = e[1], t.ev_types.end = e[2]
            },
            disabledEvents: function() {
                var e = this;
                e.$elem.on("dragstart.owl", function(t) {
                    t.preventDefault()
                }), e.$elem.on("mousedown.disableTextSelect", function(e) {
                    return t(e.target).is("input, textarea, select, option")
                })
            },
            gestures: function() {
                function s(t) {
                    return t.touches ? {
                        x: t.touches[0].pageX,
                        y: t.touches[0].pageY
                    } : t.pageX !== i ? {
                        x: t.pageX,
                        y: t.pageY
                    } : {
                        x: t.clientX,
                        y: t.clientY
                    }
                }

                function n(e) {
                    "on" === e ? (t(o).on(p.ev_types.move, r), t(o).on(p.ev_types.end, l)) : "off" === e && (t(o).off(p.ev_types.move), t(o).off(p.ev_types.end))
                }

                function a(o) {
                    var o = o.originalEvent || o || e.event;
                    if (3 === o.which) return !1;
                    if (!(p.itemsAmount <= p.options.items)) {
                        if (p.isCssFinish === !1 && !p.options.dragBeforeAnimFinish) return !1;
                        if (p.isCss3Finish === !1 && !p.options.dragBeforeAnimFinish) return !1;
                        p.options.autoPlay !== !1 && clearInterval(p.autoPlayInterval), p.browser.isTouch === !0 || p.$owlWrapper.hasClass("grabbing") || p.$owlWrapper.addClass("grabbing"), p.newPosX = 0, p.newRelativeX = 0, t(this).css(p.removeTransition());
                        var i = t(this).position();
                        "rtl" == p.options.direction ? (positionRight = p.$owlItems.eq(0).width() * p.currentItem, m.relativePos = positionRight, m.offsetX = -s(o).x + positionRight) : (m.relativePos = i.left, m.offsetX = s(o).x - i.left), m.offsetY = s(o).y - i.top, n("on"), m.sliding = !1, m.targetElement = o.target || o.srcElement
                    }
                }

                function r(i) {
                    var i = i.originalEvent || i || e.event;
                    "rtl" == p.options.direction ? (p.newPosX = -s(i).x - m.offsetX, p.newPosY = s(i).y - m.offsetY, p.newRelativeX = p.newPosX + m.relativePos) : (p.newPosX = s(i).x - m.offsetX, p.newPosY = s(i).y - m.offsetY, p.newRelativeX = p.newPosX - m.relativePos), "function" == typeof p.options.startDragging && m.dragging !== !0 && 0 !== p.newRelativeX && (m.dragging = !0, p.options.startDragging.apply(p, [p.$elem])), (p.newRelativeX > 8 || p.newRelativeX < -8 && p.browser.isTouch === !0) && (i.preventDefault ? i.preventDefault() : i.returnValue = !1, m.sliding = !0), (p.newPosY > 10 || p.newPosY < -10) && m.sliding === !1 && t(o).off("touchmove.owl");
                    var n = function() {
                            return p.newRelativeX / 5
                        },
                        a = function() {
                            return p.maximumPixels + p.newRelativeX / 5
                        };
                    p.newPosX = Math.max(Math.min(p.newPosX, n()), a()), p.browser.support3d === !0 ? p.transition3d(p.newPosX) : p.css2move(p.newPosX)
                }

                function l(o) {
                    var o = o.originalEvent || o || e.event;
                    if (o.target = o.target || o.srcElement, m.dragging = !1, p.browser.isTouch !== !0 && p.$owlWrapper.removeClass("grabbing"), p.dragDirection = p.owl.dragDirection = "rtl" == p.options.direction ? p.newRelativeX < 0 ? "right" : "left" : p.newRelativeX < 0 ? "left" : "right", 0 !== p.newRelativeX) {
                        var i = p.getNewPosition();
                        if (p.goTo(i, !1, "drag"), m.targetElement === o.target && p.browser.isTouch !== !0) {
                            t(o.target).on("click.disable", function(e) {
                                e.stopImmediatePropagation(), e.stopPropagation(), e.preventDefault(), t(o.target).off("click.disable")
                            });
                            var s = t._data(o.target, "events").click,
                                a = s.pop();
                            s.splice(0, 0, a)
                        }
                    }
                    n("off")
                }
                var p = this,
                    m = {
                        offsetX: 0,
                        offsetY: 0,
                        baseElWidth: 0,
                        relativePos: 0,
                        position: null,
                        minSwipe: null,
                        maxSwipe: null,
                        sliding: null,
                        dargging: null,
                        targetElement: null
                    };
                p.isCssFinish = !0, p.$elem.on(p.ev_types.start, ".owl-wrapper", a)
            },
            getNewPosition: function() {
                var t, e = this;
                return t = e.closestItem(), t > e.maximumItem ? (e.currentItem = e.maximumItem, t = e.maximumItem) : e.newPosX >= 0 && (t = 0, e.currentItem = 0), t
            },
            closestItem: function() {
                var e = this,
                    o = e.options.scrollPerPage === !0 ? e.pagesInArray : e.positionsInArray,
                    i = e.newPosX,
                    s = null;
                return t.each(o, function(n, a) {
                    i - e.itemWidth / 20 > o[n + 1] && i - e.itemWidth / 20 < a && "left" === e.moveDirection() ? (s = a, e.currentItem = e.options.scrollPerPage === !0 ? t.inArray(s, e.positionsInArray) : n) : i + e.itemWidth / 20 < a && i + e.itemWidth / 20 > (o[n + 1] || o[n] - e.itemWidth) && "right" === e.moveDirection() && (e.options.scrollPerPage === !0 ? (s = o[n + 1] || o[o.length - 1], e.currentItem = t.inArray(s, e.positionsInArray)) : (s = o[n + 1], e.currentItem = n + 1))
                }), e.currentItem
            },
            moveDirection: function() {
                var t, e = this;
                return e.newRelativeX < 0 ? (t = "right", e.playDirection = "next") : (t = "left", e.playDirection = "prev"), t
            },
            customEvents: function() {
                var t = this;
                t.$elem.on("owl.next", function() {
                    t.next()
                }), t.$elem.on("owl.prev", function() {
                    t.prev()
                }), t.$elem.on("owl.play", function(e, o) {
                    t.options.autoPlay = o, t.play(), t.hoverStatus = "play"
                }), t.$elem.on("owl.stop", function() {
                    t.stop(), t.hoverStatus = "stop"
                }), t.$elem.on("owl.goTo", function(e, o) {
                    t.goTo(o)
                }), t.$elem.on("owl.jumpTo", function(e, o) {
                    t.jumpTo(o)
                })
            },
            stopOnHover: function() {
                var t = this;
                t.options.stopOnHover === !0 && t.browser.isTouch !== !0 && t.options.autoPlay !== !1 && (t.$elem.on("mouseover", function() {
                    t.stop()
                }), t.$elem.on("mouseout", function() {
                    "stop" !== t.hoverStatus && t.play()
                }))
            },
            lazyLoad: function() {
                var e = this;
                if (e.options.lazyLoad === !1) return !1;
                for (var o = 0; o < e.itemsAmount; o++) {
                    var s = t(e.$owlItems[o]);
                    if ("loaded" !== s.data("owl-loaded")) {
                        var n, a = s.data("owl-item"),
                            r = s.find(".lazyOwl");
                        "string" == typeof r.data("src") ? (s.data("owl-loaded") === i && (r.hide(), s.addClass("loading").data("owl-loaded", "checked")), n = e.options.lazyFollow === !0 ? a >= e.currentItem : !0, n && a < e.currentItem + e.options.items && r.length && e.lazyPreload(s, r)) : s.data("owl-loaded", "loaded")
                    }
                }
            },
            lazyPreload: function(t, e) {
                function o() {
                    n += 1, s.completeImg(e.get(0)) || a === !0 ? i() : 100 >= n ? setTimeout(o, 100) : i()
                }

                function i() {
                    t.data("owl-loaded", "loaded").removeClass("loading"), e.removeAttr("data-src"), "fade" === s.options.lazyEffect ? e.fadeIn(400) : e.show(), "function" == typeof s.options.afterLazyLoad && s.options.afterLazyLoad.apply(this, [s.$elem])
                }
                var s = this,
                    n = 0;
                if ("DIV" === e.prop("tagName")) {
                    e.css("background-image", "url(" + e.data("src") + ")");
                    var a = !0
                } else e[0].src = e.data("src");
                o()
            },
            autoHeight: function() {
                function e() {
                    a += 1, s.completeImg(n.get(0)) ? o() : 100 >= a ? setTimeout(e, 100) : s.wrapperOuter.css("height", "")
                }

                function o() {
                    var e = t(s.$owlItems[s.currentItem]).height();
                    s.wrapperOuter.css("height", e + "px"), s.wrapperOuter.hasClass("autoHeight") || setTimeout(function() {
                        s.wrapperOuter.addClass("autoHeight")
                    }, 0)
                }
                var s = this,
                    n = t(s.$owlItems[s.currentItem]).find("img");
                if (n.get(0) !== i) {
                    var a = 0;
                    e()
                } else o()
            },
            completeImg: function(t) {
                return t.complete ? "undefined" != typeof t.naturalWidth && 0 == t.naturalWidth ? !1 : !0 : !1
            },
            onVisibleItems: function() {
                var e = this;
                e.options.addClassActive === !0 && e.$owlItems.removeClass("active"), e.visibleItems = [];
                for (var o = e.currentItem; o < e.currentItem + e.options.items; o++) e.visibleItems.push(o), e.options.addClassActive === !0 && t(e.$owlItems[o]).addClass("active");
                e.owl.visibleItems = e.visibleItems
            },
            transitionTypes: function(t) {
                var e = this;
                e.outClass = "owl-" + t + "-out", e.inClass = "owl-" + t + "-in"
            },
            singleItemTransition: function() {
                function t(t) {
                    return {
                        position: "relative",
                        left: t + "px"
                    }
                }
                var e = this;
                e.isTransition = !0;
                var o = e.outClass,
                    i = e.inClass,
                    s = e.$owlItems.eq(e.currentItem),
                    n = e.$owlItems.eq(e.prevItem),
                    a = Math.abs(e.positionsInArray[e.currentItem]) + e.positionsInArray[e.prevItem],
                    r = Math.abs(e.positionsInArray[e.currentItem]) + e.itemWidth / 2;
                e.$owlWrapper.addClass("owl-origin").css({
                    "-webkit-transform-origin": r + "px",
                    "-moz-perspective-origin": r + "px",
                    "perspective-origin": r + "px"
                });
                var l = "webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend";
                n.css(t(a, 10)).addClass(o).on(l, function() {
                    e.endPrev = !0, n.off(l), e.clearTransStyle(n, o)
                }), s.addClass(i).on(l, function() {
                    e.endCurrent = !0, s.off(l), e.clearTransStyle(s, i)
                })
            },
            clearTransStyle: function(t, e) {
                var o = this;
                t.css({
                    position: "",
                    left: ""
                }).removeClass(e), o.endPrev && o.endCurrent && (o.$owlWrapper.removeClass("owl-origin"), o.endPrev = !1, o.endCurrent = !1, o.isTransition = !1)
            },
            owlStatus: function() {
                var t = this;
                t.owl = {
                    userOptions: t.userOptions,
                    baseElement: t.$elem,
                    userItems: t.$userItems,
                    owlItems: t.$owlItems,
                    currentItem: t.currentItem,
                    prevItem: t.prevItem,
                    visibleItems: t.visibleItems,
                    isTouch: t.browser.isTouch,
                    browser: t.browser,
                    dragDirection: t.dragDirection
                }
            },
            clearEvents: function() {
                var i = this;
                i.$elem.off(".owl owl mousedown.disableTextSelect"), t(o).off(".owl owl"), t(e).off("resize", i.resizer)
            },
            unWrap: function() {
                var t = this;
                0 !== t.$elem.children().length && (t.$owlWrapper.unwrap(), t.$userItems.unwrap().unwrap(), t.owlControls && t.owlControls.remove()), t.clearEvents(), t.$elem.attr("style", t.$elem.data("owl-originalStyles") || "").attr("class", t.$elem.data("owl-originalClasses"))
            },
            destroy: function() {
                var t = this;
                t.stop(), clearInterval(t.checkVisible), t.unWrap(), t.$elem.removeData()
            },
            reinit: function(e) {
                var o = this,
                    i = t.extend({}, o.userOptions, e);
                o.unWrap(), o.init(i, o.$elem)
            },
            addItem: function(t, e) {
                var o, s = this;
                return t ? 0 === s.$elem.children().length ? (s.$elem.append(t), s.setVars(), !1) : (s.unWrap(), o = e === i || -1 === e ? -1 : e, o >= s.$userItems.length || -1 === o ? s.$userItems.eq(-1).after(t) : s.$userItems.eq(o).before(t), void s.setVars()) : !1
            },
            removeItem: function(t) {
                var e, o = this;
                return 0 === o.$elem.children().length ? !1 : (e = t === i || -1 === t ? -1 : t, o.unWrap(), o.$userItems.eq(e).remove(), void o.setVars())
            }
        };
        t.fn.owlCarousel = function(e) {
            return this.each(function() {
                if (t(this).data("owl-init") === !0) return !1;
                t(this).data("owl-init", !0);
                var o = Object.create(s);
                o.init(e, this), t.data(this, "owlCarousel", o)
            })
        }, t.fn.owlCarousel.options = {
            direction: "ltr",
            items: 5,
            itemsCustom: !1,
            itemsDesktop: [1199, 4],
            itemsDesktopSmall: [979, 3],
            itemsTablet: [768, 2],
            itemsTabletSmall: !1,
            itemsMobile: [479, 1],
            singleItem: !1,
            itemsScaleUp: !1,
            slideSpeed: 200,
            paginationSpeed: 800,
            rewindSpeed: 1e3,
            autoPlay: !1,
            stopOnHover: !1,
            navigation: !1,
            navigationText: ["prev", "next"],
            rewindNav: !0,
            scrollPerPage: !1,
            pagination: !0,
            paginationNumbers: !1,
            responsive: !0,
            responsiveRefreshRate: 200,
            responsiveBaseWidth: e,
            baseClass: "owl-carousel",
            theme: "owl-theme",
            lazyLoad: !1,
            lazyFollow: !0,
            lazyEffect: "fade",
            autoHeight: !1,
            jsonPath: !1,
            jsonSuccess: !1,
            dragBeforeAnimFinish: !0,
            mouseDrag: !0,
            touchDrag: !0,
            addClassActive: !1,
            transitionStyle: !1,
            beforeUpdate: !1,
            afterUpdate: !1,
            beforeInit: !1,
            afterInit: !1,
            beforeMove: !1,
            afterMove: !1,
            afterAction: !1,
            startDragging: !1,
            afterLazyLoad: !1
        }
    }(jQuery, window, document);
(function(_, $) {
    $(document).on('click', '.cm-login-provider,.cm-link-provider', function(e) {
        var jelm = $(e.target);
        var login_provider = false;
        var link_provider = false;
        var url = "";
        if (jelm.hasClass('cm-login-provider') || jelm.parents('.cm-login-provider').length > 0) {
            login_provider = true;
        }
        if (jelm.hasClass('cm-link-provider') || jelm.parents('.cm-link-provider').length > 0) {
            link_provider = true;
        }
        if (login_provider && !jelm.hasClass('cm-login-provider')) {
            jelm = jelm.closest('.cm-login-provider');
        } else if (link_provider && !jelm.hasClass('cm-link-provider')) {
            jelm = jelm.closest('.cm-link-provider');
        }
        var idp = jelm.data('idp');
        var open_id = false;
        switch (idp) {
            case "wordpress":
            case "blogger":
            case "flickr":
            case "livejournal":
                var open_id = true;
                if (idp == "blogger") {
                    var un = prompt("Please enter your blog name");
                } else {
                    var un = prompt("Please enter your username");
                }
                break;
            case "openid":
                var open_id = true;
                var un = prompt("Please enter your OpenID URL");
        }
        if (!open_id) {
            if (login_provider) {
                url = 'auth.login_provider?provider=' + idp + '&redirect_url=' + encodeURIComponent($('input[name=redirect_url]').val()) + '&_ts=' + (new Date()).getTime();
            } else {
                url = 'profiles.link_provider?provider=' + idp + '&_ts=' + (new Date()).getTime();
            }
            if (_.embedded) {
                url += '&embedded=true';
            }
            window.open(fn_url(url), "hybridauth_social_sing_on", "location=0,status=0,scrollbars=0,width=800,height=500");
        } else {
            var oi = un;
            if (!un) {
                return false;
            }
            switch (idp) {
                case "wordpress":
                    oi = "http://" + un + ".wordpress.com";
                    break;
                case "livejournal":
                    oi = "http://" + un + ".livejournal.com";
                    break;
                case "blogger":
                    oi = "http://" + un + ".blogspot.com";
                    break;
                case "flickr":
                    oi = "http://www.flickr.com/photos/" + un + "/";
                    break;
            }
            if (login_provider) {
                url = 'auth.login_provider?provider=OpenID&_ts=' + (new Date()).getTime() + '&openid_identifier=' + escape(oi);
            } else {
                url = 'profiles.link_provider?provider=OpenID&_ts=' + (new Date()).getTime() + '&openid_identifier=' + escape(oi);
            }
            if (_.embedded) {
                url += '&embedded=true';
            }
            window.open(fn_url(url), "hybridauth_social_sing_on", "location=0,status=0,scrollbars=0,width=800,height=500");
        }
    });
    $(document).on('click', '.cm-unlink-provider', function(e) {
        var jelm = $(e.target);
        if (!jelm.hasClass('cm-unlink-provider')) {
            jelm = jelm.closest('.cm-unlink-provider');
        }
        if (confirm(_.tr('text_are_you_sure_to_proceed'))) {
            var idp = jelm.data('idp');
            $.ceAjax('request', fn_url('profiles.unlink_provider?provider=' + idp), {
                method: 'post',
                result_ids: 'hybrid_providers'
            });
        }
    });
    $(document).on('change', '.cm-select-provider', function(e) {
        var jelm = $(e.target),
            option = $('option:selected', jelm),
            provider = option.data('provider'),
            id = option.data('id');
        $.ceAjax('request', fn_url('hybrid_auth.select_provider?provider=' + provider + '&id=' + id), {
            method: 'get',
            result_ids: 'content_keys_' + id + ',content_params_' + id
        });
    });
})(Tygh, Tygh.$);
! function(a, b, c) {
    ! function(a) {
        "use strict";
        "function" == typeof define && define.amd ? define(["jquery"], a) : jQuery && !jQuery.fn.qtip && a(jQuery)
    }(function(d) {
        "use strict";

        function e(a, b, c, e) {
            this.id = c, this.target = a, this.tooltip = F, this.elements = {
                target: a
            }, this._id = S + "-" + c, this.timers = {
                img: {}
            }, this.options = b, this.plugins = {}, this.cache = {
                event: {},
                target: d(),
                disabled: E,
                attr: e,
                onTooltip: E,
                lastClass: ""
            }, this.rendered = this.destroyed = this.disabled = this.waiting = this.hiddenDuringWait = this.positioning = this.triggering = E
        }

        function f(a) {
            return a === F || "object" !== d.type(a)
        }

        function g(a) {
            return !(d.isFunction(a) || a && a.attr || a.length || "object" === d.type(a) && (a.jquery || a.then))
        }

        function h(a) {
            var b, c, e, h;
            return f(a) ? E : (f(a.metadata) && (a.metadata = {
                type: a.metadata
            }), "content" in a && (b = a.content, f(b) || b.jquery || b.done ? b = a.content = {
                text: c = g(b) ? E : b
            } : c = b.text, "ajax" in b && (e = b.ajax, h = e && e.once !== E, delete b.ajax, b.text = function(a, b) {
                var f = c || d(this).attr(b.options.content.attr) || "Loading...",
                    g = d.ajax(d.extend({}, e, {
                        context: b
                    })).then(e.success, F, e.error).then(function(a) {
                        return a && h && b.set("content.text", a), a
                    }, function(a, c, d) {
                        b.destroyed || 0 === a.status || b.set("content.text", c + ": " + d)
                    });
                return h ? f : (b.set("content.text", f), g)
            }), "title" in b && (d.isPlainObject(b.title) && (b.button = b.title.button, b.title = b.title.text), g(b.title || E) && (b.title = E))), "position" in a && f(a.position) && (a.position = {
                my: a.position,
                at: a.position
            }), "show" in a && f(a.show) && (a.show = a.show.jquery ? {
                target: a.show
            } : a.show === D ? {
                ready: D
            } : {
                event: a.show
            }), "hide" in a && f(a.hide) && (a.hide = a.hide.jquery ? {
                target: a.hide
            } : {
                event: a.hide
            }), "style" in a && f(a.style) && (a.style = {
                classes: a.style
            }), d.each(R, function() {
                this.sanitize && this.sanitize(a)
            }), a)
        }

        function i(a, b) {
            for (var c, d = 0, e = a, f = b.split("."); e = e[f[d++]];) d < f.length && (c = e);
            return [c || a, f.pop()]
        }

        function j(a, b) {
            var c, d, e;
            for (c in this.checks)
                for (d in this.checks[c])(e = new RegExp(d, "i").exec(a)) && (b.push(e), ("builtin" === c || this.plugins[c]) && this.checks[c][d].apply(this.plugins[c] || this, b))
        }

        function k(a) {
            return V.concat("").join(a ? "-" + a + " " : " ")
        }

        function l(a, b) {
            return b > 0 ? setTimeout(d.proxy(a, this), b) : void a.call(this)
        }

        function m(a) {
            this.tooltip.hasClass(ab) || (clearTimeout(this.timers.show), clearTimeout(this.timers.hide), this.timers.show = l.call(this, function() {
                this.toggle(D, a)
            }, this.options.show.delay))
        }

        function n(a) {
            if (!this.tooltip.hasClass(ab) && !this.destroyed) {
                var b = d(a.relatedTarget),
                    c = b.closest(W)[0] === this.tooltip[0],
                    e = b[0] === this.options.show.target[0];
                if (clearTimeout(this.timers.show), clearTimeout(this.timers.hide), this !== b[0] && "mouse" === this.options.position.target && c || this.options.hide.fixed && /mouse(out|leave|move)/.test(a.type) && (c || e)) try {
                    a.preventDefault(), a.stopImmediatePropagation()
                } catch (f) {} else this.timers.hide = l.call(this, function() {
                    this.toggle(E, a)
                }, this.options.hide.delay, this)
            }
        }

        function o(a) {
            !this.tooltip.hasClass(ab) && this.options.hide.inactive && (clearTimeout(this.timers.inactive), this.timers.inactive = l.call(this, function() {
                this.hide(a)
            }, this.options.hide.inactive))
        }

        function p(a) {
            this.rendered && this.tooltip[0].offsetWidth > 0 && this.reposition(a)
        }

        function q(a, c, e) {
            d(b.body).delegate(a, (c.split ? c : c.join("." + S + " ")) + "." + S, function() {
                var a = y.api[d.attr(this, U)];
                a && !a.disabled && e.apply(a, arguments)
            })
        }

        function r(a, c, f) {
            var g, i, j, k, l, m = d(b.body),
                n = a[0] === b ? m : a,
                o = a.metadata ? a.metadata(f.metadata) : F,
                p = "html5" === f.metadata.type && o ? o[f.metadata.name] : F,
                q = a.data(f.metadata.name || "qtipopts");
            try {
                q = "string" == typeof q ? d.parseJSON(q) : q
            } catch (r) {}
            if (k = d.extend(D, {}, y.defaults, f, "object" == typeof q ? h(q) : F, h(p || o)), i = k.position, k.id = c, "boolean" == typeof k.content.text) {
                if (j = a.attr(k.content.attr), k.content.attr === E || !j) return E;
                k.content.text = j
            }
            if (i.container.length || (i.container = m), i.target === E && (i.target = n), k.show.target === E && (k.show.target = n), k.show.solo === D && (k.show.solo = i.container.closest("body")), k.hide.target === E && (k.hide.target = n), k.position.viewport === D && (k.position.viewport = i.container), i.container = i.container.eq(0), i.at = new A(i.at, D), i.my = new A(i.my), a.data(S))
                if (k.overwrite) a.qtip("destroy", !0);
                else if (k.overwrite === E) return E;
            return a.attr(T, c), k.suppress && (l = a.attr("title")) && a.removeAttr("title").attr(cb, l).attr("title", ""), g = new e(a, k, c, !!j), a.data(S, g), g
        }

        function s(a) {
            return a.charAt(0).toUpperCase() + a.slice(1)
        }

        function t(a, b) {
            var d, e, f = b.charAt(0).toUpperCase() + b.slice(1),
                g = (b + " " + rb.join(f + " ") + f).split(" "),
                h = 0;
            if (qb[b]) return a.css(qb[b]);
            for (; d = g[h++];)
                if ((e = a.css(d)) !== c) return qb[b] = d, e
        }

        function u(a, b) {
            return Math.ceil(parseFloat(t(a, b)))
        }

        function v(a, b) {
            this._ns = "tip", this.options = b, this.offset = b.offset, this.size = [b.width, b.height], this.init(this.qtip = a)
        }

        function w(a, b) {
            this.options = b, this._ns = "-modal", this.init(this.qtip = a)
        }

        function x(a) {
            this._ns = "ie6", this.init(this.qtip = a)
        }
        var y, z, A, B, C, D = !0,
            E = !1,
            F = null,
            G = "x",
            H = "y",
            I = "width",
            J = "height",
            K = "top",
            L = "left",
            M = "bottom",
            N = "right",
            O = "center",
            P = "flipinvert",
            Q = "shift",
            R = {},
            S = "qtip",
            T = "data-hasqtip",
            U = "data-qtip-id",
            V = ["ui-widget", "ui-tooltip"],
            W = "." + S,
            X = "click dblclick mousedown mouseup mousemove mouseleave mouseenter".split(" "),
            Y = S + "-fixed",
            Z = S + "-default",
            $ = S + "-focus",
            _ = S + "-hover",
            ab = S + "-disabled",
            bb = "_replacedByqTip",
            cb = "oldtitle",
            db = {
                ie: function() {
                    for (var a = 4, c = b.createElement("div");
                        (c.innerHTML = "<!--[if gt IE " + a + "]><i></i><![endif]-->") && c.getElementsByTagName("i")[0]; a += 1);
                    return a > 4 ? a : 0 / 0
                }(),
                iOS: parseFloat(("" + (/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0, ""])[1]).replace("undefined", "3_2").replace("_", ".").replace("_", "")) || E
            };
        z = e.prototype, z._when = function(a) {
            return d.when.apply(d, a)
        }, z.render = function(a) {
            if (this.rendered || this.destroyed) return this;
            var b, c = this,
                e = this.options,
                f = this.cache,
                g = this.elements,
                h = e.content.text,
                i = e.content.title,
                j = e.content.button,
                k = e.position,
                l = ("." + this._id + " ", []);
            return d.attr(this.target[0], "aria-describedby", this._id), f.posClass = this._createPosClass((this.position = {
                my: k.my,
                at: k.at
            }).my), this.tooltip = g.tooltip = b = d("<div/>", {
                id: this._id,
                "class": [S, Z, e.style.classes, f.posClass].join(" "),
                width: e.style.width || "",
                height: e.style.height || "",
                tracking: "mouse" === k.target && k.adjust.mouse,
                role: "alert",
                "aria-live": "polite",
                "aria-atomic": E,
                "aria-describedby": this._id + "-content",
                "aria-hidden": D
            }).toggleClass(ab, this.disabled).attr(U, this.id).data(S, this).appendTo(k.container).append(g.content = d("<div />", {
                "class": S + "-content",
                id: this._id + "-content",
                "aria-atomic": D
            })), this.rendered = -1, this.positioning = D, i && (this._createTitle(), d.isFunction(i) || l.push(this._updateTitle(i, E))), j && this._createButton(), d.isFunction(h) || l.push(this._updateContent(h, E)), this.rendered = D, this._setWidget(), d.each(R, function(a) {
                var b;
                "render" === this.initialize && (b = this(c)) && (c.plugins[a] = b)
            }), this._unassignEvents(), this._assignEvents(), this._when(l).then(function() {
                c._trigger("render"), c.positioning = E, c.hiddenDuringWait || !e.show.ready && !a || c.toggle(D, f.event, E), c.hiddenDuringWait = E
            }), y.api[this.id] = this, this
        }, z.destroy = function(a) {
            function b() {
                if (!this.destroyed) {
                    this.destroyed = D;
                    var a, b = this.target,
                        c = b.attr(cb);
                    this.rendered && this.tooltip.stop(1, 0).find("*").remove().end().remove(), d.each(this.plugins, function() {
                        this.destroy && this.destroy()
                    });
                    for (a in this.timers) clearTimeout(this.timers[a]);
                    b.removeData(S).removeAttr(U).removeAttr(T).removeAttr("aria-describedby"), this.options.suppress && c && b.attr("title", c).removeAttr(cb), this._unassignEvents(), this.options = this.elements = this.cache = this.timers = this.plugins = this.mouse = F, delete y.api[this.id]
                }
            }
            return this.destroyed ? this.target : (a === D && "hide" !== this.triggering || !this.rendered ? b.call(this) : (this.tooltip.one("tooltiphidden", d.proxy(b, this)), !this.triggering && this.hide()), this.target)
        }, B = z.checks = {
            builtin: {
                "^id$": function(a, b, c, e) {
                    var f = c === D ? y.nextid : c,
                        g = S + "-" + f;
                    f !== E && f.length > 0 && !d("#" + g).length ? (this._id = g, this.rendered && (this.tooltip[0].id = this._id, this.elements.content[0].id = this._id + "-content", this.elements.title[0].id = this._id + "-title")) : a[b] = e
                },
                "^prerender": function(a, b, c) {
                    c && !this.rendered && this.render(this.options.show.ready)
                },
                "^content.text$": function(a, b, c) {
                    this._updateContent(c)
                },
                "^content.attr$": function(a, b, c, d) {
                    this.options.content.text === this.target.attr(d) && this._updateContent(this.target.attr(c))
                },
                "^content.title$": function(a, b, c) {
                    return c ? (c && !this.elements.title && this._createTitle(), void this._updateTitle(c)) : this._removeTitle()
                },
                "^content.button$": function(a, b, c) {
                    this._updateButton(c)
                },
                "^content.title.(text|button)$": function(a, b, c) {
                    this.set("content." + b, c)
                },
                "^position.(my|at)$": function(a, b, c) {
                    "string" == typeof c && (this.position[b] = a[b] = new A(c, "at" === b))
                },
                "^position.container$": function(a, b, c) {
                    this.rendered && this.tooltip.appendTo(c)
                },
                "^show.ready$": function(a, b, c) {
                    c && (!this.rendered && this.render(D) || this.toggle(D))
                },
                "^style.classes$": function(a, b, c, d) {
                    this.rendered && this.tooltip.removeClass(d).addClass(c)
                },
                "^style.(width|height)": function(a, b, c) {
                    this.rendered && this.tooltip.css(b, c)
                },
                "^style.widget|content.title": function() {
                    this.rendered && this._setWidget()
                },
                "^style.def": function(a, b, c) {
                    this.rendered && this.tooltip.toggleClass(Z, !!c)
                },
                "^events.(render|show|move|hide|focus|blur)$": function(a, b, c) {
                    this.rendered && this.tooltip[(d.isFunction(c) ? "" : "un") + "bind"]("tooltip" + b, c)
                },
                "^(show|hide|position).(event|target|fixed|inactive|leave|distance|viewport|adjust)": function() {
                    if (this.rendered) {
                        var a = this.options.position;
                        this.tooltip.attr("tracking", "mouse" === a.target && a.adjust.mouse), this._unassignEvents(), this._assignEvents()
                    }
                }
            }
        }, z.get = function(a) {
            if (this.destroyed) return this;
            var b = i(this.options, a.toLowerCase()),
                c = b[0][b[1]];
            return c.precedance ? c.string() : c
        };
        var eb = /^position\.(my|at|adjust|target|container|viewport)|style|content|show\.ready/i,
            fb = /^prerender|show\.ready/i;
        z.set = function(a, b) {
            if (this.destroyed) return this; {
                var c, e = this.rendered,
                    f = E,
                    g = this.options;
                this.checks
            }
            return "string" == typeof a ? (c = a, a = {}, a[c] = b) : a = d.extend({}, a), d.each(a, function(b, c) {
                if (e && fb.test(b)) return void delete a[b];
                var h, j = i(g, b.toLowerCase());
                h = j[0][j[1]], j[0][j[1]] = c && c.nodeType ? d(c) : c, f = eb.test(b) || f, a[b] = [j[0], j[1], c, h]
            }), h(g), this.positioning = D, d.each(a, d.proxy(j, this)), this.positioning = E, this.rendered && this.tooltip[0].offsetWidth > 0 && f && this.reposition("mouse" === g.position.target ? F : this.cache.event), this
        }, z._update = function(a, b) {
            var c = this,
                e = this.cache;
            return this.rendered && a ? (d.isFunction(a) && (a = a.call(this.elements.target, e.event, this) || ""), d.isFunction(a.then) ? (e.waiting = D, a.then(function(a) {
                return e.waiting = E, c._update(a, b)
            }, F, function(a) {
                return c._update(a, b)
            })) : a === E || !a && "" !== a ? E : (a.jquery && a.length > 0 ? b.empty().append(a.css({
                display: "block",
                visibility: "visible"
            })) : b.html(a), this._waitForContent(b).then(function(a) {
                c.rendered && c.tooltip[0].offsetWidth > 0 && c.reposition(e.event, !a.length)
            }))) : E
        }, z._waitForContent = function(a) {
            var b = this.cache;
            return b.waiting = D, (d.fn.imagesLoaded ? a.imagesLoaded() : d.Deferred().resolve([])).done(function() {
                b.waiting = E
            }).promise()
        }, z._updateContent = function(a, b) {
            this._update(a, this.elements.content, b)
        }, z._updateTitle = function(a, b) {
            this._update(a, this.elements.title, b) === E && this._removeTitle(E)
        }, z._createTitle = function() {
            var a = this.elements,
                b = this._id + "-title";
            a.titlebar && this._removeTitle(), a.titlebar = d("<div />", {
                "class": S + "-titlebar " + (this.options.style.widget ? k("header") : "")
            }).append(a.title = d("<div />", {
                id: b,
                "class": S + "-title",
                "aria-atomic": D
            })).insertBefore(a.content).delegate(".qtip-close", "mousedown keydown mouseup keyup mouseout", function(a) {
                d(this).toggleClass("ui-state-active ui-state-focus", "down" === a.type.substr(-4))
            }).delegate(".qtip-close", "mouseover mouseout", function(a) {
                d(this).toggleClass("ui-state-hover", "mouseover" === a.type)
            }), this.options.content.button && this._createButton()
        }, z._removeTitle = function(a) {
            var b = this.elements;
            b.title && (b.titlebar.remove(), b.titlebar = b.title = b.button = F, a !== E && this.reposition())
        }, z._createPosClass = function(a) {
            return S + "-pos-" + (a || this.options.position.my).abbrev()
        }, z.reposition = function(c, e) {
            if (!this.rendered || this.positioning || this.destroyed) return this;
            this.positioning = D;
            var f, g, h, i, j = this.cache,
                k = this.tooltip,
                l = this.options.position,
                m = l.target,
                n = l.my,
                o = l.at,
                p = l.viewport,
                q = l.container,
                r = l.adjust,
                s = r.method.split(" "),
                t = k.outerWidth(E),
                u = k.outerHeight(E),
                v = 0,
                w = 0,
                x = k.css("position"),
                y = {
                    left: 0,
                    top: 0
                },
                z = k[0].offsetWidth > 0,
                A = c && "scroll" === c.type,
                B = d(a),
                C = q[0].ownerDocument,
                F = this.mouse;
            if (d.isArray(m) && 2 === m.length) o = {
                x: L,
                y: K
            }, y = {
                left: m[0],
                top: m[1]
            };
            else if ("mouse" === m) o = {
                x: L,
                y: K
            }, (!r.mouse || this.options.hide.distance) && j.origin && j.origin.pageX ? c = j.origin : !c || c && ("resize" === c.type || "scroll" === c.type) ? c = j.event : F && F.pageX && (c = F), "static" !== x && (y = q.offset()), C.body.offsetWidth !== (a.innerWidth || C.documentElement.clientWidth) && (g = d(b.body).offset()), y = {
                left: c.pageX - y.left + (g && g.left || 0),
                top: c.pageY - y.top + (g && g.top || 0)
            }, r.mouse && A && F && (y.left -= (F.scrollX || 0) - B.scrollLeft(), y.top -= (F.scrollY || 0) - B.scrollTop());
            else {
                if ("event" === m ? c && c.target && "scroll" !== c.type && "resize" !== c.type ? j.target = d(c.target) : c.target || (j.target = this.elements.target) : "event" !== m && (j.target = d(m.jquery ? m : this.elements.target)), m = j.target, m = d(m).eq(0), 0 === m.length) return this;
                m[0] === b || m[0] === a ? (v = db.iOS ? a.innerWidth : m.width(), w = db.iOS ? a.innerHeight : m.height(), m[0] === a && (y = {
                    top: (p || m).scrollTop(),
                    left: (p || m).scrollLeft()
                })) : R.imagemap && m.is("area") ? f = R.imagemap(this, m, o, R.viewport ? s : E) : R.svg && m && m[0].ownerSVGElement ? f = R.svg(this, m, o, R.viewport ? s : E) : (v = m.outerWidth(E), w = m.outerHeight(E), y = m.offset()), f && (v = f.width, w = f.height, g = f.offset, y = f.position), y = this.reposition.offset(m, y, q), (db.iOS > 3.1 && db.iOS < 4.1 || db.iOS >= 4.3 && db.iOS < 4.33 || !db.iOS && "fixed" === x) && (y.left -= B.scrollLeft(), y.top -= B.scrollTop()), (!f || f && f.adjustable !== E) && (y.left += o.x === N ? v : o.x === O ? v / 2 : 0, y.top += o.y === M ? w : o.y === O ? w / 2 : 0)
            }
            return y.left += r.x + (n.x === N ? -t : n.x === O ? -t / 2 : 0), y.top += r.y + (n.y === M ? -u : n.y === O ? -u / 2 : 0), R.viewport ? (h = y.adjusted = R.viewport(this, y, l, v, w, t, u), g && h.left && (y.left += g.left), g && h.top && (y.top += g.top), h.my && (this.position.my = h.my)) : y.adjusted = {
                left: 0,
                top: 0
            }, j.posClass !== (i = this._createPosClass(this.position.my)) && k.removeClass(j.posClass).addClass(j.posClass = i), this._trigger("move", [y, p.elem || p], c) ? (delete y.adjusted, e === E || !z || isNaN(y.left) || isNaN(y.top) || "mouse" === m || !d.isFunction(l.effect) ? k.css(y) : d.isFunction(l.effect) && (l.effect.call(k, this, d.extend({}, y)), k.queue(function(a) {
                d(this).css({
                    opacity: "",
                    height: ""
                }), db.ie && this.style.removeAttribute("filter"), a()
            })), this.positioning = E, this) : this
        }, z.reposition.offset = function(a, c, e) {
            function f(a, b) {
                c.left += b * a.scrollLeft(), c.top += b * a.scrollTop()
            }
            if (!e[0]) return c;
            var g, h, i, j, k = d(a[0].ownerDocument),
                l = !!db.ie && "CSS1Compat" !== b.compatMode,
                m = e[0];
            do "static" !== (h = d.css(m, "position")) && ("fixed" === h ? (i = m.getBoundingClientRect(), f(k, -1)) : (i = d(m).position(), i.left += parseFloat(d.css(m, "borderLeftWidth")) || 0, i.top += parseFloat(d.css(m, "borderTopWidth")) || 0), c.left -= i.left + (parseFloat(d.css(m, "marginLeft")) || 0), c.top -= i.top + (parseFloat(d.css(m, "marginTop")) || 0), g || "hidden" === (j = d.css(m, "overflow")) || "visible" === j || (g = d(m))); while (m = m.offsetParent);
            return g && (g[0] !== k[0] || l) && f(g, 1), c
        };
        var gb = (A = z.reposition.Corner = function(a, b) {
            a = ("" + a).replace(/([A-Z])/, " $1").replace(/middle/gi, O).toLowerCase(), this.x = (a.match(/left|right/i) || a.match(/center/) || ["inherit"])[0].toLowerCase(), this.y = (a.match(/top|bottom|center/i) || ["inherit"])[0].toLowerCase(), this.forceY = !!b;
            var c = a.charAt(0);
            this.precedance = "t" === c || "b" === c ? H : G
        }).prototype;
        gb.invert = function(a, b) {
            this[a] = this[a] === L ? N : this[a] === N ? L : b || this[a]
        }, gb.string = function(a) {
            var b = this.x,
                c = this.y,
                d = b !== c ? "center" === b || "center" !== c && (this.precedance === H || this.forceY) ? [c, b] : [b, c] : [b];
            return a !== !1 ? d.join(" ") : d
        }, gb.abbrev = function() {
            var a = this.string(!1);
            return a[0].charAt(0) + (a[1] && a[1].charAt(0) || "")
        }, gb.clone = function() {
            return new A(this.string(), this.forceY)
        }, z.toggle = function(a, c) {
            var e = this.cache,
                f = this.options,
                g = this.tooltip;
            if (c) {
                if (/over|enter/.test(c.type) && e.event && /out|leave/.test(e.event.type) && f.show.target.add(c.target).length === f.show.target.length && g.has(c.relatedTarget).length) return this;
                e.event = d.event.fix(c)
            }
            if (this.waiting && !a && (this.hiddenDuringWait = D), !this.rendered) return a ? this.render(1) : this;
            if (this.destroyed || this.disabled) return this;
            var h, i, j, k = a ? "show" : "hide",
                l = this.options[k],
                m = (this.options[a ? "hide" : "show"], this.options.position),
                n = this.options.content,
                o = this.tooltip.css("width"),
                p = this.tooltip.is(":visible"),
                q = a || 1 === l.target.length,
                r = !c || l.target.length < 2 || e.target[0] === c.target;
            return (typeof a).search("boolean|number") && (a = !p), h = !g.is(":animated") && p === a && r, i = h ? F : !!this._trigger(k, [90]), this.destroyed ? this : (i !== E && a && this.focus(c), !i || h ? this : (d.attr(g[0], "aria-hidden", !a), a ? (this.mouse && (e.origin = d.event.fix(this.mouse)), d.isFunction(n.text) && this._updateContent(n.text, E), d.isFunction(n.title) && this._updateTitle(n.title, E), !C && "mouse" === m.target && m.adjust.mouse && (d(b).bind("mousemove." + S, this._storeMouse), C = D), o || g.css("width", g.outerWidth(E)), this.reposition(c, arguments[2]), o || g.css("width", ""), l.solo && ("string" == typeof l.solo ? d(l.solo) : d(W, l.solo)).not(g).not(l.target).qtip("hide", d.Event("tooltipsolo"))) : (clearTimeout(this.timers.show), delete e.origin, C && !d(W + '[tracking="true"]:visible', l.solo).not(g).length && (d(b).unbind("mousemove." + S), C = E), this.blur(c)), j = d.proxy(function() {
                a ? (db.ie && g[0].style.removeAttribute("filter"), g.css("overflow", ""), "string" == typeof l.autofocus && d(this.options.show.autofocus, g).focus(), this.options.show.target.trigger("qtip-" + this.id + "-inactive")) : g.css({
                    display: "",
                    visibility: "",
                    opacity: "",
                    left: "",
                    top: ""
                }), this._trigger(a ? "visible" : "hidden")
            }, this), l.effect === E || q === E ? (g[k](), j()) : d.isFunction(l.effect) ? (g.stop(1, 1), l.effect.call(g, this), g.queue("fx", function(a) {
                j(), a()
            })) : g.fadeTo(90, a ? 1 : 0, j), a && l.target.trigger("qtip-" + this.id + "-inactive"), this))
        }, z.show = function(a) {
            return this.toggle(D, a)
        }, z.hide = function(a) {
            return this.toggle(E, a)
        }, z.focus = function(a) {
            if (!this.rendered || this.destroyed) return this;
            var b = d(W),
                c = this.tooltip,
                e = parseInt(c[0].style.zIndex, 10),
                f = y.zindex + b.length;
            return c.hasClass($) || this._trigger("focus", [f], a) && (e !== f && (b.each(function() {
                this.style.zIndex > e && (this.style.zIndex = this.style.zIndex - 1)
            }), b.filter("." + $).qtip("blur", a)), c.addClass($)[0].style.zIndex = f), this
        }, z.blur = function(a) {
            return !this.rendered || this.destroyed ? this : (this.tooltip.removeClass($), this._trigger("blur", [this.tooltip.css("zIndex")], a), this)
        }, z.disable = function(a) {
            return this.destroyed ? this : ("toggle" === a ? a = !(this.rendered ? this.tooltip.hasClass(ab) : this.disabled) : "boolean" != typeof a && (a = D), this.rendered && this.tooltip.toggleClass(ab, a).attr("aria-disabled", a), this.disabled = !!a, this)
        }, z.enable = function() {
            return this.disable(E)
        }, z._createButton = function() {
            var a = this,
                b = this.elements,
                c = b.tooltip,
                e = this.options.content.button,
                f = "string" == typeof e,
                g = f ? e : "Close tooltip";
            b.button && b.button.remove(), b.button = e.jquery ? e : d("<a />", {
                "class": "qtip-close " + (this.options.style.widget ? "" : S + "-icon"),
                title: g,
                "aria-label": g
            }).prepend(d("<span />", {
                "class": "ui-icon ui-icon-close",
                html: "&times;"
            })), b.button.appendTo(b.titlebar || c).attr("role", "button").click(function(b) {
                return c.hasClass(ab) || a.hide(b), E
            })
        }, z._updateButton = function(a) {
            if (!this.rendered) return E;
            var b = this.elements.button;
            a ? this._createButton() : b.remove()
        }, z._setWidget = function() {
            var a = this.options.style.widget,
                b = this.elements,
                c = b.tooltip,
                d = c.hasClass(ab);
            c.removeClass(ab), ab = a ? "ui-state-disabled" : "qtip-disabled", c.toggleClass(ab, d), c.toggleClass("ui-helper-reset " + k(), a).toggleClass(Z, this.options.style.def && !a), b.content && b.content.toggleClass(k("content"), a), b.titlebar && b.titlebar.toggleClass(k("header"), a), b.button && b.button.toggleClass(S + "-icon", !a)
        }, z._storeMouse = function(a) {
            return (this.mouse = d.event.fix(a)).type = "mousemove", this
        }, z._bind = function(a, b, c, e, f) {
            if (a && c && b.length) {
                var g = "." + this._id + (e ? "-" + e : "");
                return d(a).bind((b.split ? b : b.join(g + " ")) + g, d.proxy(c, f || this)), this
            }
        }, z._unbind = function(a, b) {
            return a && d(a).unbind("." + this._id + (b ? "-" + b : "")), this
        }, z._trigger = function(a, b, c) {
            var e = d.Event("tooltip" + a);
            return e.originalEvent = c && d.extend({}, c) || this.cache.event || F, this.triggering = a, this.tooltip.trigger(e, [this].concat(b || [])), this.triggering = E, !e.isDefaultPrevented()
        }, z._bindEvents = function(a, b, c, e, f, g) {
            var h = c.filter(e).add(e.filter(c)),
                i = [];
            h.length && (d.each(b, function(b, c) {
                var e = d.inArray(c, a);
                e > -1 && i.push(a.splice(e, 1)[0])
            }), i.length && (this._bind(h, i, function(a) {
                var b = this.rendered ? this.tooltip[0].offsetWidth > 0 : !1;
                (b ? g : f).call(this, a)
            }), c = c.not(h), e = e.not(h))), this._bind(c, a, f), this._bind(e, b, g)
        }, z._assignInitialEvents = function(a) {
            function b(a) {
                return this.disabled || this.destroyed ? E : (this.cache.event = a && d.event.fix(a), this.cache.target = a && d(a.target), clearTimeout(this.timers.show), void(this.timers.show = l.call(this, function() {
                    this.render("object" == typeof a || c.show.ready)
                }, c.prerender ? 0 : c.show.delay)))
            }
            var c = this.options,
                e = c.show.target,
                f = c.hide.target,
                g = c.show.event ? d.trim("" + c.show.event).split(" ") : [],
                h = c.hide.event ? d.trim("" + c.hide.event).split(" ") : [];
            this._bind(this.elements.target, ["remove", "removeqtip"], function() {
                this.destroy(!0)
            }, "destroy"), /mouse(over|enter)/i.test(c.show.event) && !/mouse(out|leave)/i.test(c.hide.event) && h.push("mouseleave"), this._bind(e, "mousemove", function(a) {
                this._storeMouse(a), this.cache.onTarget = D
            }), this._bindEvents(g, h, e, f, b, function() {
                return this.timers ? void clearTimeout(this.timers.show) : E
            }), (c.show.ready || c.prerender) && b.call(this, a)
        }, z._assignEvents = function() {
            var c = this,
                e = this.options,
                f = e.position,
                g = this.tooltip,
                h = e.show.target,
                i = e.hide.target,
                j = f.container,
                k = f.viewport,
                l = d(b),
                q = (d(b.body), d(a)),
                r = e.show.event ? d.trim("" + e.show.event).split(" ") : [],
                s = e.hide.event ? d.trim("" + e.hide.event).split(" ") : [];
            d.each(e.events, function(a, b) {
                c._bind(g, "toggle" === a ? ["tooltipshow", "tooltiphide"] : ["tooltip" + a], b, null, g)
            }), /mouse(out|leave)/i.test(e.hide.event) && "window" === e.hide.leave && this._bind(l, ["mouseout", "blur"], function(a) {
                /select|option/.test(a.target.nodeName) || a.relatedTarget || this.hide(a)
            }), e.hide.fixed ? i = i.add(g.addClass(Y)) : /mouse(over|enter)/i.test(e.show.event) && this._bind(i, "mouseleave", function() {
                clearTimeout(this.timers.show)
            }), ("" + e.hide.event).indexOf("unfocus") > -1 && this._bind(j.closest("html"), ["mousedown", "touchstart"], function(a) {
                var b = d(a.target),
                    c = this.rendered && !this.tooltip.hasClass(ab) && this.tooltip[0].offsetWidth > 0,
                    e = b.parents(W).filter(this.tooltip[0]).length > 0;
                b[0] === this.target[0] || b[0] === this.tooltip[0] || e || this.target.has(b[0]).length || !c || this.hide(a)
            }), "number" == typeof e.hide.inactive && (this._bind(h, "qtip-" + this.id + "-inactive", o, "inactive"), this._bind(i.add(g), y.inactiveEvents, o)), this._bindEvents(r, s, h, i, m, n), this._bind(h.add(g), "mousemove", function(a) {
                if ("number" == typeof e.hide.distance) {
                    var b = this.cache.origin || {},
                        c = this.options.hide.distance,
                        d = Math.abs;
                    (d(a.pageX - b.pageX) >= c || d(a.pageY - b.pageY) >= c) && this.hide(a)
                }
                this._storeMouse(a)
            }), "mouse" === f.target && f.adjust.mouse && (e.hide.event && this._bind(h, ["mouseenter", "mouseleave"], function(a) {
                return this.cache ? void(this.cache.onTarget = "mouseenter" === a.type) : E
            }), this._bind(l, "mousemove", function(a) {
                this.rendered && this.cache.onTarget && !this.tooltip.hasClass(ab) && this.tooltip[0].offsetWidth > 0 && this.reposition(a)
            })), (f.adjust.resize || k.length) && this._bind(d.event.special.resize ? k : q, "resize", p), f.adjust.scroll && this._bind(q.add(f.container), "scroll", p)
        }, z._unassignEvents = function() {
            var c = this.options,
                e = c.show.target,
                f = c.hide.target,
                g = d.grep([this.elements.target[0], this.rendered && this.tooltip[0], c.position.container[0], c.position.viewport[0], c.position.container.closest("html")[0], a, b], function(a) {
                    return "object" == typeof a
                });
            e && e.toArray && (g = g.concat(e.toArray())), f && f.toArray && (g = g.concat(f.toArray())), this._unbind(g)._unbind(g, "destroy")._unbind(g, "inactive")
        }, d(function() {
            q(W, ["mouseenter", "mouseleave"], function(a) {
                var b = "mouseenter" === a.type,
                    c = d(a.currentTarget),
                    e = d(a.relatedTarget || a.target),
                    f = this.options;
                b ? (this.focus(a), c.hasClass(Y) && !c.hasClass(ab) && clearTimeout(this.timers.hide)) : "mouse" === f.position.target && f.position.adjust.mouse && f.hide.event && f.show.target && !e.closest(f.show.target[0]).length && this.hide(a), c.toggleClass(_, b)
            }), q("[" + U + "]", X, o)
        }), y = d.fn.qtip = function(a, b, e) {
            var f = ("" + a).toLowerCase(),
                g = F,
                i = d.makeArray(arguments).slice(1),
                j = i[i.length - 1],
                k = this[0] ? d.data(this[0], S) : F;
            return !arguments.length && k || "api" === f ? k : "string" == typeof a ? (this.each(function() {
                var a = d.data(this, S);
                if (!a) return D;
                if (j && j.timeStamp && (a.cache.event = j), !b || "option" !== f && "options" !== f) a[f] && a[f].apply(a, i);
                else {
                    if (e === c && !d.isPlainObject(b)) return g = a.get(b), E;
                    a.set(b, e)
                }
            }), g !== F ? g : this) : "object" != typeof a && arguments.length ? void 0 : (k = h(d.extend(D, {}, a)), this.each(function(a) {
                var b, c;
                return c = d.isArray(k.id) ? k.id[a] : k.id, c = !c || c === E || c.length < 1 || y.api[c] ? y.nextid++ : c, b = r(d(this), c, k), b === E ? D : (y.api[c] = b, d.each(R, function() {
                    "initialize" === this.initialize && this(b)
                }), void b._assignInitialEvents(j))
            }))
        }, d.qtip = e, y.api = {}, d.each({
            attr: function(a, b) {
                if (this.length) {
                    var c = this[0],
                        e = "title",
                        f = d.data(c, "qtip");
                    if (a === e && f && "object" == typeof f && f.options.suppress) return arguments.length < 2 ? d.attr(c, cb) : (f && f.options.content.attr === e && f.cache.attr && f.set("content.text", b), this.attr(cb, b))
                }
                return d.fn["attr" + bb].apply(this, arguments)
            },
            clone: function(a) {
                var b = (d([]), d.fn["clone" + bb].apply(this, arguments));
                return a || b.filter("[" + cb + "]").attr("title", function() {
                    return d.attr(this, cb)
                }).removeAttr(cb), b
            }
        }, function(a, b) {
            if (!b || d.fn[a + bb]) return D;
            var c = d.fn[a + bb] = d.fn[a];
            d.fn[a] = function() {
                return b.apply(this, arguments) || c.apply(this, arguments)
            }
        }), d.ui || (d["cleanData" + bb] = d.cleanData, d.cleanData = function(a) {
            for (var b, c = 0;
                (b = d(a[c])).length; c++)
                if (b.attr(T)) try {
                    b.triggerHandler("removeqtip")
                } catch (e) {}
            d["cleanData" + bb].apply(this, arguments)
        }), y.version = "2.2.1", y.nextid = 0, y.inactiveEvents = X, y.zindex = 15e3, y.defaults = {
            prerender: E,
            id: E,
            overwrite: D,
            suppress: D,
            content: {
                text: D,
                attr: "title",
                title: E,
                button: E
            },
            position: {
                my: "top left",
                at: "bottom right",
                target: E,
                container: E,
                viewport: E,
                adjust: {
                    x: 0,
                    y: 0,
                    mouse: D,
                    scroll: D,
                    resize: D,
                    method: "flipinvert flipinvert"
                },
                effect: function(a, b) {
                    d(this).animate(b, {
                        duration: 200,
                        queue: E
                    })
                }
            },
            show: {
                target: E,
                event: "mouseenter",
                effect: D,
                delay: 90,
                solo: E,
                ready: E,
                autofocus: E
            },
            hide: {
                target: E,
                event: "mouseleave",
                effect: D,
                delay: 0,
                fixed: E,
                inactive: E,
                leave: "window",
                distance: E
            },
            style: {
                classes: "",
                widget: E,
                width: E,
                height: E,
                def: D
            },
            events: {
                render: F,
                move: F,
                show: F,
                hide: F,
                toggle: F,
                visible: F,
                hidden: F,
                focus: F,
                blur: F
            }
        };
        var hb, ib = "margin",
            jb = "border",
            kb = "color",
            lb = "background-color",
            mb = "transparent",
            nb = " !important",
            ob = !!b.createElement("canvas").getContext,
            pb = /rgba?\(0, 0, 0(, 0)?\)|transparent|#123456/i,
            qb = {},
            rb = ["Webkit", "O", "Moz", "ms"];
        if (ob) var sb = a.devicePixelRatio || 1,
            tb = function() {
                var a = b.createElement("canvas").getContext("2d");
                return a.backingStorePixelRatio || a.webkitBackingStorePixelRatio || a.mozBackingStorePixelRatio || a.msBackingStorePixelRatio || a.oBackingStorePixelRatio || 1
            }(),
            ub = sb / tb;
        else var vb = function(a, b, c) {
            return "<qtipvml:" + a + ' xmlns="urn:schemas-microsoft.com:vml" class="qtip-vml" ' + (b || "") + ' style="behavior: url(#default#VML); ' + (c || "") + '" />'
        };
        d.extend(v.prototype, {
            init: function(a) {
                var b, c;
                c = this.element = a.elements.tip = d("<div />", {
                    "class": S + "-tip"
                }).prependTo(a.tooltip), ob ? (b = d("<canvas />").appendTo(this.element)[0].getContext("2d"), b.lineJoin = "miter", b.miterLimit = 1e5, b.save()) : (b = vb("shape", 'coordorigin="0,0"', "position:absolute;"), this.element.html(b + b), a._bind(d("*", c).add(c), ["click", "mousedown"], function(a) {
                    a.stopPropagation()
                }, this._ns)), a._bind(a.tooltip, "tooltipmove", this.reposition, this._ns, this), this.create()
            },
            _swapDimensions: function() {
                this.size[0] = this.options.height, this.size[1] = this.options.width
            },
            _resetDimensions: function() {
                this.size[0] = this.options.width, this.size[1] = this.options.height
            },
            _useTitle: function(a) {
                var b = this.qtip.elements.titlebar;
                return b && (a.y === K || a.y === O && this.element.position().top + this.size[1] / 2 + this.options.offset < b.outerHeight(D))
            },
            _parseCorner: function(a) {
                var b = this.qtip.options.position.my;
                return a === E || b === E ? a = E : a === D ? a = new A(b.string()) : a.string || (a = new A(a), a.fixed = D), a
            },
            _parseWidth: function(a, b, c) {
                var d = this.qtip.elements,
                    e = jb + s(b) + "Width";
                return (c ? u(c, e) : u(d.content, e) || u(this._useTitle(a) && d.titlebar || d.content, e) || u(d.tooltip, e)) || 0
            },
            _parseRadius: function(a) {
                var b = this.qtip.elements,
                    c = jb + s(a.y) + s(a.x) + "Radius";
                return db.ie < 9 ? 0 : u(this._useTitle(a) && b.titlebar || b.content, c) || u(b.tooltip, c) || 0
            },
            _invalidColour: function(a, b, c) {
                var d = a.css(b);
                return !d || c && d === a.css(c) || pb.test(d) ? E : d
            },
            _parseColours: function(a) {
                var b = this.qtip.elements,
                    c = this.element.css("cssText", ""),
                    e = jb + s(a[a.precedance]) + s(kb),
                    f = this._useTitle(a) && b.titlebar || b.content,
                    g = this._invalidColour,
                    h = [];
                return h[0] = g(c, lb) || g(f, lb) || g(b.content, lb) || g(b.tooltip, lb) || c.css(lb), h[1] = g(c, e, kb) || g(f, e, kb) || g(b.content, e, kb) || g(b.tooltip, e, kb) || b.tooltip.css(e), d("*", c).add(c).css("cssText", lb + ":" + mb + nb + ";" + jb + ":0" + nb + ";"), h
            },
            _calculateSize: function(a) {
                var b, c, d, e = a.precedance === H,
                    f = this.options.width,
                    g = this.options.height,
                    h = "c" === a.abbrev(),
                    i = (e ? f : g) * (h ? .5 : 1),
                    j = Math.pow,
                    k = Math.round,
                    l = Math.sqrt(j(i, 2) + j(g, 2)),
                    m = [this.border / i * l, this.border / g * l];
                return m[2] = Math.sqrt(j(m[0], 2) - j(this.border, 2)), m[3] = Math.sqrt(j(m[1], 2) - j(this.border, 2)), b = l + m[2] + m[3] + (h ? 0 : m[0]), c = b / l, d = [k(c * f), k(c * g)], e ? d : d.reverse()
            },
            _calculateTip: function(a, b, c) {
                c = c || 1, b = b || this.size;
                var d = b[0] * c,
                    e = b[1] * c,
                    f = Math.ceil(d / 2),
                    g = Math.ceil(e / 2),
                    h = {
                        br: [0, 0, d, e, d, 0],
                        bl: [0, 0, d, 0, 0, e],
                        tr: [0, e, d, 0, d, e],
                        tl: [0, 0, 0, e, d, e],
                        tc: [0, e, f, 0, d, e],
                        bc: [0, 0, d, 0, f, e],
                        rc: [0, 0, d, g, 0, e],
                        lc: [d, 0, d, e, 0, g]
                    };
                return h.lt = h.br, h.rt = h.bl, h.lb = h.tr, h.rb = h.tl, h[a.abbrev()]
            },
            _drawCoords: function(a, b) {
                a.beginPath(), a.moveTo(b[0], b[1]), a.lineTo(b[2], b[3]), a.lineTo(b[4], b[5]), a.closePath()
            },
            create: function() {
                var a = this.corner = (ob || db.ie) && this._parseCorner(this.options.corner);
                return (this.enabled = !!this.corner && "c" !== this.corner.abbrev()) && (this.qtip.cache.corner = a.clone(), this.update()), this.element.toggle(this.enabled), this.corner
            },
            update: function(b, c) {
                if (!this.enabled) return this;
                var e, f, g, h, i, j, k, l, m = this.qtip.elements,
                    n = this.element,
                    o = n.children(),
                    p = this.options,
                    q = this.size,
                    r = p.mimic,
                    s = Math.round;
                b || (b = this.qtip.cache.corner || this.corner), r === E ? r = b : (r = new A(r), r.precedance = b.precedance, "inherit" === r.x ? r.x = b.x : "inherit" === r.y ? r.y = b.y : r.x === r.y && (r[b.precedance] = b[b.precedance])), f = r.precedance, b.precedance === G ? this._swapDimensions() : this._resetDimensions(), e = this.color = this._parseColours(b), e[1] !== mb ? (l = this.border = this._parseWidth(b, b[b.precedance]), p.border && 1 > l && !pb.test(e[1]) && (e[0] = e[1]), this.border = l = p.border !== D ? p.border : l) : this.border = l = 0, k = this.size = this._calculateSize(b), n.css({
                    width: k[0],
                    height: k[1],
                    lineHeight: k[1] + "px"
                }), j = b.precedance === H ? [s(r.x === L ? l : r.x === N ? k[0] - q[0] - l : (k[0] - q[0]) / 2), s(r.y === K ? k[1] - q[1] : 0)] : [s(r.x === L ? k[0] - q[0] : 0), s(r.y === K ? l : r.y === M ? k[1] - q[1] - l : (k[1] - q[1]) / 2)], ob ? (g = o[0].getContext("2d"), g.restore(), g.save(), g.clearRect(0, 0, 6e3, 6e3), h = this._calculateTip(r, q, ub), i = this._calculateTip(r, this.size, ub), o.attr(I, k[0] * ub).attr(J, k[1] * ub), o.css(I, k[0]).css(J, k[1]), this._drawCoords(g, i), g.fillStyle = e[1], g.fill(), g.translate(j[0] * ub, j[1] * ub), this._drawCoords(g, h), g.fillStyle = e[0], g.fill()) : (h = this._calculateTip(r), h = "m" + h[0] + "," + h[1] + " l" + h[2] + "," + h[3] + " " + h[4] + "," + h[5] + " xe", j[2] = l && /^(r|b)/i.test(b.string()) ? 8 === db.ie ? 2 : 1 : 0, o.css({
                    coordsize: k[0] + l + " " + (k[1] + l),
                    antialias: "" + (r.string().indexOf(O) > -1),
                    left: j[0] - j[2] * Number(f === G),
                    top: j[1] - j[2] * Number(f === H),
                    width: k[0] + l,
                    height: k[1] + l
                }).each(function(a) {
                    var b = d(this);
                    b[b.prop ? "prop" : "attr"]({
                        coordsize: k[0] + l + " " + (k[1] + l),
                        path: h,
                        fillcolor: e[0],
                        filled: !!a,
                        stroked: !a
                    }).toggle(!(!l && !a)), !a && b.html(vb("stroke", 'weight="' + 2 * l + 'px" color="' + e[1] + '" miterlimit="1000" joinstyle="miter"'))
                })), a.opera && setTimeout(function() {
                    m.tip.css({
                        display: "inline-block",
                        visibility: "visible"
                    })
                }, 1), c !== E && this.calculate(b, k)
            },
            calculate: function(a, b) {
                if (!this.enabled) return E;
                var c, e, f = this,
                    g = this.qtip.elements,
                    h = this.element,
                    i = this.options.offset,
                    j = (g.tooltip.hasClass("ui-widget"), {});
                return a = a || this.corner, c = a.precedance, b = b || this._calculateSize(a), e = [a.x, a.y], c === G && e.reverse(), d.each(e, function(d, e) {
                    var h, k, l;
                    e === O ? (h = c === H ? L : K, j[h] = "50%", j[ib + "-" + h] = -Math.round(b[c === H ? 0 : 1] / 2) + i) : (h = f._parseWidth(a, e, g.tooltip), k = f._parseWidth(a, e, g.content), l = f._parseRadius(a), j[e] = Math.max(-f.border, d ? k : i + (l > h ? l : -h)))
                }), j[a[c]] -= b[c === G ? 0 : 1], h.css({
                    margin: "",
                    top: "",
                    bottom: "",
                    left: "",
                    right: ""
                }).css(j), j
            },
            reposition: function(a, b, d) {
                function e(a, b, c, d, e) {
                    a === Q && j.precedance === b && k[d] && j[c] !== O ? j.precedance = j.precedance === G ? H : G : a !== Q && k[d] && (j[b] = j[b] === O ? k[d] > 0 ? d : e : j[b] === d ? e : d)
                }

                function f(a, b, e) {
                    j[a] === O ? p[ib + "-" + b] = o[a] = g[ib + "-" + b] - k[b] : (h = g[e] !== c ? [k[b], -g[b]] : [-k[b], g[b]], (o[a] = Math.max(h[0], h[1])) > h[0] && (d[b] -= k[b], o[b] = E), p[g[e] !== c ? e : b] = o[a])
                }
                if (this.enabled) {
                    var g, h, i = b.cache,
                        j = this.corner.clone(),
                        k = d.adjusted,
                        l = b.options.position.adjust.method.split(" "),
                        m = l[0],
                        n = l[1] || l[0],
                        o = {
                            left: E,
                            top: E,
                            x: 0,
                            y: 0
                        },
                        p = {};
                    this.corner.fixed !== D && (e(m, G, H, L, N), e(n, H, G, K, M), (j.string() !== i.corner.string() || i.cornerTop !== k.top || i.cornerLeft !== k.left) && this.update(j, E)), g = this.calculate(j), g.right !== c && (g.left = -g.right), g.bottom !== c && (g.top = -g.bottom), g.user = this.offset, (o.left = m === Q && !!k.left) && f(G, L, N), (o.top = n === Q && !!k.top) && f(H, K, M), this.element.css(p).toggle(!(o.x && o.y || j.x === O && o.y || j.y === O && o.x)), d.left -= g.left.charAt ? g.user : m !== Q || o.top || !o.left && !o.top ? g.left + this.border : 0, d.top -= g.top.charAt ? g.user : n !== Q || o.left || !o.left && !o.top ? g.top + this.border : 0, i.cornerLeft = k.left, i.cornerTop = k.top, i.corner = j.clone()
                }
            },
            destroy: function() {
                this.qtip._unbind(this.qtip.tooltip, this._ns), this.qtip.elements.tip && this.qtip.elements.tip.find("*").remove().end().remove()
            }
        }), hb = R.tip = function(a) {
            return new v(a, a.options.style.tip)
        }, hb.initialize = "render", hb.sanitize = function(a) {
            if (a.style && "tip" in a.style) {
                var b = a.style.tip;
                "object" != typeof b && (b = a.style.tip = {
                    corner: b
                }), /string|boolean/i.test(typeof b.corner) || (b.corner = D)
            }
        }, B.tip = {
            "^position.my|style.tip.(corner|mimic|border)$": function() {
                this.create(), this.qtip.reposition()
            },
            "^style.tip.(height|width)$": function(a) {
                this.size = [a.width, a.height], this.update(), this.qtip.reposition()
            },
            "^content.title|style.(classes|widget)$": function() {
                this.update()
            }
        }, d.extend(D, y.defaults, {
            style: {
                tip: {
                    corner: D,
                    mimic: E,
                    width: 6,
                    height: 6,
                    border: D,
                    offset: 0
                }
            }
        });
        var wb, xb, yb = "qtip-modal",
            zb = "." + yb;
        xb = function() {
            function a(a) {
                if (d.expr[":"].focusable) return d.expr[":"].focusable;
                var b, c, e, f = !isNaN(d.attr(a, "tabindex")),
                    g = a.nodeName && a.nodeName.toLowerCase();
                return "area" === g ? (b = a.parentNode, c = b.name, a.href && c && "map" === b.nodeName.toLowerCase() ? (e = d("img[usemap=#" + c + "]")[0], !!e && e.is(":visible")) : !1) : /input|select|textarea|button|object/.test(g) ? !a.disabled : "a" === g ? a.href || f : f
            }

            function c(a) {
                k.length < 1 && a.length ? a.not("body").blur() : k.first().focus()
            }

            function e(a) {
                if (i.is(":visible")) {
                    var b, e = d(a.target),
                        h = f.tooltip,
                        j = e.closest(W);
                    b = j.length < 1 ? E : parseInt(j[0].style.zIndex, 10) > parseInt(h[0].style.zIndex, 10), b || e.closest(W)[0] === h[0] || c(e), g = a.target === k[k.length - 1]
                }
            }
            var f, g, h, i, j = this,
                k = {};
            d.extend(j, {
                init: function() {
                    return i = j.elem = d("<div />", {
                        id: "qtip-overlay",
                        html: "<div></div>",
                        mousedown: function() {
                            return E
                        }
                    }).hide(), d(b.body).bind("focusin" + zb, e), d(b).bind("keydown" + zb, function(a) {
                        f && f.options.show.modal.escape && 27 === a.keyCode && f.hide(a)
                    }), i.bind("click" + zb, function(a) {
                        f && f.options.show.modal.blur && f.hide(a)
                    }), j
                },
                update: function(b) {
                    f = b, k = b.options.show.modal.stealfocus !== E ? b.tooltip.find("*").filter(function() {
                        return a(this)
                    }) : []
                },
                toggle: function(a, e, g) {
                    var k = (d(b.body), a.tooltip),
                        l = a.options.show.modal,
                        m = l.effect,
                        n = e ? "show" : "hide",
                        o = i.is(":visible"),
                        p = d(zb).filter(":visible:not(:animated)").not(k);
                    return j.update(a), e && l.stealfocus !== E && c(d(":focus")), i.toggleClass("blurs", l.blur), e && i.appendTo(b.body), i.is(":animated") && o === e && h !== E || !e && p.length ? j : (i.stop(D, E), d.isFunction(m) ? m.call(i, e) : m === E ? i[n]() : i.fadeTo(parseInt(g, 10) || 90, e ? 1 : 0, function() {
                        e || i.hide()
                    }), e || i.queue(function(a) {
                        i.css({
                            left: "",
                            top: ""
                        }), d(zb).length || i.detach(), a()
                    }), h = e, f.destroyed && (f = F), j)
                }
            }), j.init()
        }, xb = new xb, d.extend(w.prototype, {
            init: function(a) {
                var b = a.tooltip;
                return this.options.on ? (a.elements.overlay = xb.elem, b.addClass(yb).css("z-index", y.modal_zindex + d(zb).length), a._bind(b, ["tooltipshow", "tooltiphide"], function(a, c, e) {
                    var f = a.originalEvent;
                    if (a.target === b[0])
                        if (f && "tooltiphide" === a.type && /mouse(leave|enter)/.test(f.type) && d(f.relatedTarget).closest(xb.elem[0]).length) try {
                            a.preventDefault()
                        } catch (g) {} else(!f || f && "tooltipsolo" !== f.type) && this.toggle(a, "tooltipshow" === a.type, e)
                }, this._ns, this), a._bind(b, "tooltipfocus", function(a, c) {
                    if (!a.isDefaultPrevented() && a.target === b[0]) {
                        var e = d(zb),
                            f = y.modal_zindex + e.length,
                            g = parseInt(b[0].style.zIndex, 10);
                        xb.elem[0].style.zIndex = f - 1, e.each(function() {
                            this.style.zIndex > g && (this.style.zIndex -= 1)
                        }), e.filter("." + $).qtip("blur", a.originalEvent), b.addClass($)[0].style.zIndex = f, xb.update(c);
                        try {
                            a.preventDefault()
                        } catch (h) {}
                    }
                }, this._ns, this), void a._bind(b, "tooltiphide", function(a) {
                    a.target === b[0] && d(zb).filter(":visible").not(b).last().qtip("focus", a)
                }, this._ns, this)) : this
            },
            toggle: function(a, b, c) {
                return a && a.isDefaultPrevented() ? this : void xb.toggle(this.qtip, !!b, c)
            },
            destroy: function() {
                this.qtip.tooltip.removeClass(yb), this.qtip._unbind(this.qtip.tooltip, this._ns), xb.toggle(this.qtip, E), delete this.qtip.elements.overlay
            }
        }), wb = R.modal = function(a) {
            return new w(a, a.options.show.modal)
        }, wb.sanitize = function(a) {
            a.show && ("object" != typeof a.show.modal ? a.show.modal = {
                on: !!a.show.modal
            } : "undefined" == typeof a.show.modal.on && (a.show.modal.on = D))
        }, y.modal_zindex = y.zindex - 200, wb.initialize = "render", B.modal = {
            "^show.modal.(on|blur)$": function() {
                this.destroy(), this.init(), this.qtip.elems.overlay.toggle(this.qtip.tooltip[0].offsetWidth > 0)
            }
        }, d.extend(D, y.defaults, {
            show: {
                modal: {
                    on: E,
                    effect: D,
                    blur: D,
                    stealfocus: D,
                    escape: D
                }
            }
        }), R.viewport = function(c, d, e, f, g, h, i) {
            function j(a, b, c, e, f, g, h, i, j) {
                var k = d[f],
                    s = u[a],
                    t = v[a],
                    w = c === Q,
                    x = s === f ? j : s === g ? -j : -j / 2,
                    y = t === f ? i : t === g ? -i : -i / 2,
                    z = q[f] + r[f] - (n ? 0 : m[f]),
                    A = z - k,
                    B = k + j - (h === I ? o : p) - z,
                    C = x - (u.precedance === a || s === u[b] ? y : 0) - (t === O ? i / 2 : 0);
                return w ? (C = (s === f ? 1 : -1) * x, d[f] += A > 0 ? A : B > 0 ? -B : 0, d[f] = Math.max(-m[f] + r[f], k - C, Math.min(Math.max(-m[f] + r[f] + (h === I ? o : p), k + C), d[f], "center" === s ? k - x : 1e9))) : (e *= c === P ? 2 : 0, A > 0 && (s !== f || B > 0) ? (d[f] -= C + e, l.invert(a, f)) : B > 0 && (s !== g || A > 0) && (d[f] -= (s === O ? -C : C) + e, l.invert(a, g)), d[f] < q && -d[f] > B && (d[f] = k, l = u.clone())), d[f] - k
            }
            var k, l, m, n, o, p, q, r, s = e.target,
                t = c.elements.tooltip,
                u = e.my,
                v = e.at,
                w = e.adjust,
                x = w.method.split(" "),
                y = x[0],
                z = x[1] || x[0],
                A = e.viewport,
                B = e.container,
                C = (c.cache, {
                    left: 0,
                    top: 0
                });
            return A.jquery && s[0] !== a && s[0] !== b.body && "none" !== w.method ? (m = B.offset() || C, n = "static" === B.css("position"), k = "fixed" === t.css("position"), o = A[0] === a ? A.width() : A.outerWidth(E), p = A[0] === a ? A.height() : A.outerHeight(E), q = {
                left: k ? 0 : A.scrollLeft(),
                top: k ? 0 : A.scrollTop()
            }, r = A.offset() || C, ("shift" !== y || "shift" !== z) && (l = u.clone()), C = {
                left: "none" !== y ? j(G, H, y, w.x, L, N, I, f, h) : 0,
                top: "none" !== z ? j(H, G, z, w.y, K, M, J, g, i) : 0,
                my: l
            }) : C
        }, R.polys = {
            polygon: function(a, b) {
                var c, d, e, f = {
                        width: 0,
                        height: 0,
                        position: {
                            top: 1e10,
                            right: 0,
                            bottom: 0,
                            left: 1e10
                        },
                        adjustable: E
                    },
                    g = 0,
                    h = [],
                    i = 1,
                    j = 1,
                    k = 0,
                    l = 0;
                for (g = a.length; g--;) c = [parseInt(a[--g], 10), parseInt(a[g + 1], 10)], c[0] > f.position.right && (f.position.right = c[0]), c[0] < f.position.left && (f.position.left = c[0]), c[1] > f.position.bottom && (f.position.bottom = c[1]), c[1] < f.position.top && (f.position.top = c[1]), h.push(c);
                if (d = f.width = Math.abs(f.position.right - f.position.left), e = f.height = Math.abs(f.position.bottom - f.position.top), "c" === b.abbrev()) f.position = {
                    left: f.position.left + f.width / 2,
                    top: f.position.top + f.height / 2
                };
                else {
                    for (; d > 0 && e > 0 && i > 0 && j > 0;)
                        for (d = Math.floor(d / 2), e = Math.floor(e / 2), b.x === L ? i = d : b.x === N ? i = f.width - d : i += Math.floor(d / 2), b.y === K ? j = e : b.y === M ? j = f.height - e : j += Math.floor(e / 2), g = h.length; g-- && !(h.length < 2);) k = h[g][0] - f.position.left, l = h[g][1] - f.position.top, (b.x === L && k >= i || b.x === N && i >= k || b.x === O && (i > k || k > f.width - i) || b.y === K && l >= j || b.y === M && j >= l || b.y === O && (j > l || l > f.height - j)) && h.splice(g, 1);
                    f.position = {
                        left: h[0][0],
                        top: h[0][1]
                    }
                }
                return f
            },
            rect: function(a, b, c, d) {
                return {
                    width: Math.abs(c - a),
                    height: Math.abs(d - b),
                    position: {
                        left: Math.min(a, c),
                        top: Math.min(b, d)
                    }
                }
            },
            _angles: {
                tc: 1.5,
                tr: 7 / 4,
                tl: 5 / 4,
                bc: .5,
                br: .25,
                bl: .75,
                rc: 2,
                lc: 1,
                c: 0
            },
            ellipse: function(a, b, c, d, e) {
                var f = R.polys._angles[e.abbrev()],
                    g = 0 === f ? 0 : c * Math.cos(f * Math.PI),
                    h = d * Math.sin(f * Math.PI);
                return {
                    width: 2 * c - Math.abs(g),
                    height: 2 * d - Math.abs(h),
                    position: {
                        left: a + g,
                        top: b + h
                    },
                    adjustable: E
                }
            },
            circle: function(a, b, c, d) {
                return R.polys.ellipse(a, b, c, c, d)
            }
        }, R.svg = function(a, c, e) {
            for (var f, g, h, i, j, k, l, m, n, o = (d(b), c[0]), p = d(o.ownerSVGElement), q = o.ownerDocument, r = (parseInt(c.css("stroke-width"), 10) || 0) / 2; !o.getBBox;) o = o.parentNode;
            if (!o.getBBox || !o.parentNode) return E;
            switch (o.nodeName) {
                case "ellipse":
                case "circle":
                    m = R.polys.ellipse(o.cx.baseVal.value, o.cy.baseVal.value, (o.rx || o.r).baseVal.value + r, (o.ry || o.r).baseVal.value + r, e);
                    break;
                case "line":
                case "polygon":
                case "polyline":
                    for (l = o.points || [{
                            x: o.x1.baseVal.value,
                            y: o.y1.baseVal.value
                        }, {
                            x: o.x2.baseVal.value,
                            y: o.y2.baseVal.value
                        }], m = [], k = -1, i = l.numberOfItems || l.length; ++k < i;) j = l.getItem ? l.getItem(k) : l[k], m.push.apply(m, [j.x, j.y]);
                    m = R.polys.polygon(m, e);
                    break;
                default:
                    m = o.getBBox(), m = {
                        width: m.width,
                        height: m.height,
                        position: {
                            left: m.x,
                            top: m.y
                        }
                    }
            }
            return n = m.position, p = p[0], p.createSVGPoint && (g = o.getScreenCTM(), l = p.createSVGPoint(), l.x = n.left, l.y = n.top, h = l.matrixTransform(g), n.left = h.x, n.top = h.y), q !== b && "mouse" !== a.position.target && (f = d((q.defaultView || q.parentWindow).frameElement).offset(), f && (n.left += f.left, n.top += f.top)), q = d(q), n.left += q.scrollLeft(), n.top += q.scrollTop(), m
        }, R.imagemap = function(a, b, c) {
            b.jquery || (b = d(b));
            var e, f, g, h, i, j = (b.attr("shape") || "rect").toLowerCase().replace("poly", "polygon"),
                k = d('img[usemap="#' + b.parent("map").attr("name") + '"]'),
                l = d.trim(b.attr("coords")),
                m = l.replace(/,$/, "").split(",");
            if (!k.length) return E;
            if ("polygon" === j) h = R.polys.polygon(m, c);
            else {
                if (!R.polys[j]) return E;
                for (g = -1, i = m.length, f = []; ++g < i;) f.push(parseInt(m[g], 10));
                h = R.polys[j].apply(this, f.concat(c))
            }
            return e = k.offset(), e.left += Math.ceil((k.outerWidth(E) - k.width()) / 2), e.top += Math.ceil((k.outerHeight(E) - k.height()) / 2), h.position.left += e.left, h.position.top += e.top, h
        };
        var Ab, Bb = '<iframe class="qtip-bgiframe" frameborder="0" tabindex="-1" src="javascript:\'\';"  style="display:block; position:absolute; z-index:-1; filter:alpha(opacity=0); -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";"></iframe>';
        d.extend(x.prototype, {
            _scroll: function() {
                var b = this.qtip.elements.overlay;
                b && (b[0].style.top = d(a).scrollTop() + "px")
            },
            init: function(c) {
                var e = c.tooltip;
                d("select, object").length < 1 && (this.bgiframe = c.elements.bgiframe = d(Bb).appendTo(e), c._bind(e, "tooltipmove", this.adjustBGIFrame, this._ns, this)), this.redrawContainer = d("<div/>", {
                    id: S + "-rcontainer"
                }).appendTo(b.body), c.elements.overlay && c.elements.overlay.addClass("qtipmodal-ie6fix") && (c._bind(a, ["scroll", "resize"], this._scroll, this._ns, this), c._bind(e, ["tooltipshow"], this._scroll, this._ns, this)), this.redraw()
            },
            adjustBGIFrame: function() {
                var a, b, c = this.qtip.tooltip,
                    d = {
                        height: c.outerHeight(E),
                        width: c.outerWidth(E)
                    },
                    e = this.qtip.plugins.tip,
                    f = this.qtip.elements.tip;
                b = parseInt(c.css("borderLeftWidth"), 10) || 0, b = {
                    left: -b,
                    top: -b
                }, e && f && (a = "x" === e.corner.precedance ? [I, L] : [J, K], b[a[1]] -= f[a[0]]()), this.bgiframe.css(b).css(d)
            },
            redraw: function() {
                if (this.qtip.rendered < 1 || this.drawing) return this;
                var a, b, c, d, e = this.qtip.tooltip,
                    f = this.qtip.options.style,
                    g = this.qtip.options.position.container;
                return this.qtip.drawing = 1, f.height && e.css(J, f.height), f.width ? e.css(I, f.width) : (e.css(I, "").appendTo(this.redrawContainer), b = e.width(), 1 > b % 2 && (b += 1), c = e.css("maxWidth") || "", d = e.css("minWidth") || "", a = (c + d).indexOf("%") > -1 ? g.width() / 100 : 0, c = (c.indexOf("%") > -1 ? a : 1) * parseInt(c, 10) || b, d = (d.indexOf("%") > -1 ? a : 1) * parseInt(d, 10) || 0, b = c + d ? Math.min(Math.max(b, d), c) : b, e.css(I, Math.round(b)).appendTo(g)), this.drawing = 0, this
            },
            destroy: function() {
                this.bgiframe && this.bgiframe.remove(), this.qtip._unbind([a, this.qtip.tooltip], this._ns)
            }
        }), Ab = R.ie6 = function(a) {
            return 6 === db.ie ? new x(a) : E
        }, Ab.initialize = "render", B.ie6 = {
            "^content|style$": function() {
                this.redraw()
            }
        }
    })
}(window, document);
(function($) {
    'use strict';
    var _currentSpinnerId = 0;

    function _scopedEventName(name, id) {
        return name + '.touchspin_' + id;
    }

    function _scopeEventNames(names, id) {
        return $.map(names, function(name) {
            return _scopedEventName(name, id);
        });
    }
    $.fn.TouchSpin = function(options) {
        if (options === 'destroy') {
            this.each(function() {
                var originalinput = $(this),
                    originalinput_data = originalinput.data();
                $(document).off(_scopeEventNames(['mouseup', 'touchend', 'touchcancel', 'mousemove', 'touchmove', 'scroll', 'scrollstart'], originalinput_data.spinnerid).join(' '));
            });
            return;
        }
        var defaults = {
            min: 0,
            max: 100,
            initval: '',
            replacementval: '',
            step: 1,
            decimals: 0,
            stepinterval: 100,
            forcestepdivisibility: 'round',
            stepintervaldelay: 500,
            verticalbuttons: false,
            verticalupclass: 'glyphicon glyphicon-chevron-up',
            verticaldownclass: 'glyphicon glyphicon-chevron-down',
            prefix: '',
            postfix: '',
            prefix_extraclass: '',
            postfix_extraclass: '',
            booster: true,
            boostat: 10,
            maxboostedstep: false,
            mousewheel: true,
            buttondown_class: 'btn btn-default',
            buttonup_class: 'btn btn-default',
            buttondown_txt: '-',
            buttonup_txt: '+'
        };
        var attributeMap = {
            min: 'min',
            max: 'max',
            initval: 'init-val',
            replacementval: 'replacement-val',
            step: 'step',
            decimals: 'decimals',
            stepinterval: 'step-interval',
            verticalbuttons: 'vertical-buttons',
            verticalupclass: 'vertical-up-class',
            verticaldownclass: 'vertical-down-class',
            forcestepdivisibility: 'force-step-divisibility',
            stepintervaldelay: 'step-interval-delay',
            prefix: 'prefix',
            postfix: 'postfix',
            prefix_extraclass: 'prefix-extra-class',
            postfix_extraclass: 'postfix-extra-class',
            booster: 'booster',
            boostat: 'boostat',
            maxboostedstep: 'max-boosted-step',
            mousewheel: 'mouse-wheel',
            buttondown_class: 'button-down-class',
            buttonup_class: 'button-up-class',
            buttondown_txt: 'button-down-txt',
            buttonup_txt: 'button-up-txt'
        };
        return this.each(function() {
            var settings, originalinput = $(this),
                originalinput_data = originalinput.data(),
                container, elements, value, downSpinTimer, upSpinTimer, downDelayTimeout, upDelayTimeout, spincount = 0,
                spinning = false;
            init();

            function init() {
                if (originalinput.data('alreadyinitialized')) {
                    return;
                }
                originalinput.data('alreadyinitialized', true);
                _currentSpinnerId += 1;
                originalinput.data('spinnerid', _currentSpinnerId);
                if (!originalinput.is('input')) {
                    console.log('Must be an input.');
                    return;
                }
                _initSettings();
                _setInitval();
                _checkValue();
                _buildHtml();
                _initElements();
                _hideEmptyPrefixPostfix();
                _bindEvents();
                _bindEventsInterface();
                elements.input.css('display', 'block');
            }

            function _setInitval() {
                if (settings.initval !== '' && originalinput.val() === '') {
                    originalinput.val(settings.initval);
                }
            }

            function changeSettings(newsettings) {
                _updateSettings(newsettings);
                _checkValue();
                var value = elements.input.val();
                if (value !== '') {
                    value = Number(elements.input.val());
                    elements.input.val(value.toFixed(settings.decimals));
                }
            }

            function _initSettings() {
                settings = $.extend({}, defaults, originalinput_data, _parseAttributes(), options);
            }

            function _parseAttributes() {
                var data = {};
                $.each(attributeMap, function(key, value) {
                    var attrName = 'bts-' + value + '';
                    if (originalinput.is('[data-' + attrName + ']')) {
                        data[key] = originalinput.data(attrName);
                    }
                });
                return data;
            }

            function _updateSettings(newsettings) {
                settings = $.extend({}, settings, newsettings);
            }

            function _buildHtml() {
                var initval = originalinput.val(),
                    parentelement = originalinput.parent();
                if (initval !== '') {
                    initval = Number(initval).toFixed(settings.decimals);
                }
                originalinput.data('initvalue', initval).val(initval);
                originalinput.addClass('form-control');
                if (parentelement.hasClass('input-group')) {
                    _advanceInputGroup(parentelement);
                } else {
                    _buildInputGroup();
                }
            }

            function _advanceInputGroup(parentelement) {
                parentelement.addClass('bootstrap-touchspin');
                var prev = originalinput.prev(),
                    next = originalinput.next();
                var downhtml, uphtml, prefixhtml = '<span class="input-group-addon bootstrap-touchspin-prefix">' + settings.prefix + '</span>',
                    postfixhtml = '<span class="input-group-addon bootstrap-touchspin-postfix">' + settings.postfix + '</span>';
                if (prev.hasClass('input-group-btn')) {
                    downhtml = '<button class="' + settings.buttondown_class + ' bootstrap-touchspin-down" type="button">' + settings.buttondown_txt + '</button>';
                    prev.append(downhtml);
                } else {
                    downhtml = '<span class="input-group-btn"><button class="' + settings.buttondown_class + ' bootstrap-touchspin-down" type="button">' + settings.buttondown_txt + '</button></span>';
                    $(downhtml).insertBefore(originalinput);
                }
                if (next.hasClass('input-group-btn')) {
                    uphtml = '<button class="' + settings.buttonup_class + ' bootstrap-touchspin-up" type="button">' + settings.buttonup_txt + '</button>';
                    next.prepend(uphtml);
                } else {
                    uphtml = '<span class="input-group-btn"><button class="' + settings.buttonup_class + ' bootstrap-touchspin-up" type="button">' + settings.buttonup_txt + '</button></span>';
                    $(uphtml).insertAfter(originalinput);
                }
                $(prefixhtml).insertBefore(originalinput);
                $(postfixhtml).insertAfter(originalinput);
                container = parentelement;
            }

            function _buildInputGroup() {
                var html;
                if (settings.verticalbuttons) {
                    html = '<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix">' + settings.prefix + '</span><span class="input-group-addon bootstrap-touchspin-postfix">' + settings.postfix + '</span><span class="input-group-btn-vertical"><button class="' + settings.buttondown_class + ' bootstrap-touchspin-up" type="button"><i class="' + settings.verticalupclass + '"></i></button><button class="' + settings.buttonup_class + ' bootstrap-touchspin-down" type="button"><i class="' + settings.verticaldownclass + '"></i></button></span></div>';
                } else {
                    html = '<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="' + settings.buttondown_class + ' bootstrap-touchspin-down" type="button">' + settings.buttondown_txt + '</button></span><span class="input-group-addon bootstrap-touchspin-prefix">' + settings.prefix + '</span><span class="input-group-addon bootstrap-touchspin-postfix">' + settings.postfix + '</span><span class="input-group-btn"><button class="' + settings.buttonup_class + ' bootstrap-touchspin-up" type="button">' + settings.buttonup_txt + '</button></span></div>';
                }
                container = $(html).insertBefore(originalinput);
                $('.bootstrap-touchspin-prefix', container).after(originalinput);
                if (originalinput.hasClass('input-sm')) {
                    container.addClass('input-group-sm');
                } else if (originalinput.hasClass('input-lg')) {
                    container.addClass('input-group-lg');
                }
            }

            function _initElements() {
                elements = {
                    down: $('.bootstrap-touchspin-down', container),
                    up: $('.bootstrap-touchspin-up', container),
                    input: $('input', container),
                    prefix: $('.bootstrap-touchspin-prefix', container).addClass(settings.prefix_extraclass),
                    postfix: $('.bootstrap-touchspin-postfix', container).addClass(settings.postfix_extraclass)
                };
            }

            function _hideEmptyPrefixPostfix() {
                if (settings.prefix === '') {
                    elements.prefix.hide();
                }
                if (settings.postfix === '') {
                    elements.postfix.hide();
                }
            }

            function _bindEvents() {
                originalinput.on('keydown', function(ev) {
                    var code = ev.keyCode || ev.which;
                    if (code === 38) {
                        if (spinning !== 'up') {
                            upOnce();
                            startUpSpin();
                        }
                        ev.preventDefault();
                    } else if (code === 40) {
                        if (spinning !== 'down') {
                            downOnce();
                            startDownSpin();
                        }
                        ev.preventDefault();
                    }
                });
                originalinput.on('keyup', function(ev) {
                    var code = ev.keyCode || ev.which;
                    if (code === 38) {
                        stopSpin();
                    } else if (code === 40) {
                        stopSpin();
                    }
                });
                originalinput.on('blur', function() {
                    _checkValue();
                });
                elements.down.on('keydown', function(ev) {
                    var code = ev.keyCode || ev.which;
                    if (code === 32 || code === 13) {
                        if (spinning !== 'down') {
                            downOnce();
                            startDownSpin();
                        }
                        ev.preventDefault();
                    }
                });
                elements.down.on('keyup', function(ev) {
                    var code = ev.keyCode || ev.which;
                    if (code === 32 || code === 13) {
                        stopSpin();
                    }
                });
                elements.up.on('keydown', function(ev) {
                    var code = ev.keyCode || ev.which;
                    if (code === 32 || code === 13) {
                        if (spinning !== 'up') {
                            upOnce();
                            startUpSpin();
                        }
                        ev.preventDefault();
                    }
                });
                elements.up.on('keyup', function(ev) {
                    var code = ev.keyCode || ev.which;
                    if (code === 32 || code === 13) {
                        stopSpin();
                    }
                });
                elements.down.on('mousedown.touchspin', function(ev) {
                    elements.down.off('touchstart.touchspin');
                    if (originalinput.is(':disabled')) {
                        return;
                    }
                    downOnce();
                    startDownSpin();
                    ev.preventDefault();
                    ev.stopPropagation();
                });
                elements.down.on('touchstart.touchspin', function(ev) {
                    elements.down.off('mousedown.touchspin');
                    if (originalinput.is(':disabled')) {
                        return;
                    }
                    downOnce();
                    startDownSpin();
                    ev.preventDefault();
                    ev.stopPropagation();
                });
                elements.up.on('mousedown.touchspin', function(ev) {
                    elements.up.off('touchstart.touchspin');
                    if (originalinput.is(':disabled')) {
                        return;
                    }
                    upOnce();
                    startUpSpin();
                    ev.preventDefault();
                    ev.stopPropagation();
                });
                elements.up.on('touchstart.touchspin', function(ev) {
                    elements.up.off('mousedown.touchspin');
                    if (originalinput.is(':disabled')) {
                        return;
                    }
                    upOnce();
                    startUpSpin();
                    ev.preventDefault();
                    ev.stopPropagation();
                });
                elements.up.on('mouseout touchleave touchend touchcancel', function(ev) {
                    if (!spinning) {
                        return;
                    }
                    ev.stopPropagation();
                    stopSpin();
                });
                elements.down.on('mouseout touchleave touchend touchcancel', function(ev) {
                    if (!spinning) {
                        return;
                    }
                    ev.stopPropagation();
                    stopSpin();
                });
                elements.down.on('mousemove touchmove', function(ev) {
                    if (!spinning) {
                        return;
                    }
                    ev.stopPropagation();
                    ev.preventDefault();
                });
                elements.up.on('mousemove touchmove', function(ev) {
                    if (!spinning) {
                        return;
                    }
                    ev.stopPropagation();
                    ev.preventDefault();
                });
                $(document).on(_scopeEventNames(['mouseup', 'touchend', 'touchcancel'], _currentSpinnerId).join(' '), function(ev) {
                    if (!spinning) {
                        return;
                    }
                    ev.preventDefault();
                    stopSpin();
                });
                $(document).on(_scopeEventNames(['mousemove', 'touchmove', 'scroll', 'scrollstart'], _currentSpinnerId).join(' '), function(ev) {
                    if (!spinning) {
                        return;
                    }
                    ev.preventDefault();
                    stopSpin();
                });
                originalinput.on('mousewheel DOMMouseScroll', function(ev) {
                    if (!settings.mousewheel || !originalinput.is(':focus')) {
                        return;
                    }
                    var delta = ev.originalEvent.wheelDelta || -ev.originalEvent.deltaY || -ev.originalEvent.detail;
                    ev.stopPropagation();
                    ev.preventDefault();
                    if (delta < 0) {
                        downOnce();
                    } else {
                        upOnce();
                    }
                });
            }

            function _bindEventsInterface() {
                originalinput.on('touchspin.uponce', function() {
                    stopSpin();
                    upOnce();
                });
                originalinput.on('touchspin.downonce', function() {
                    stopSpin();
                    downOnce();
                });
                originalinput.on('touchspin.startupspin', function() {
                    startUpSpin();
                });
                originalinput.on('touchspin.startdownspin', function() {
                    startDownSpin();
                });
                originalinput.on('touchspin.stopspin', function() {
                    stopSpin();
                });
                originalinput.on('touchspin.updatesettings', function(e, newsettings) {
                    changeSettings(newsettings);
                });
            }

            function _forcestepdivisibility(value) {
                switch (settings.forcestepdivisibility) {
                    case 'round':
                        return (Math.round(value / settings.step) * settings.step).toFixed(settings.decimals);
                    case 'floor':
                        return (Math.floor(value / settings.step) * settings.step).toFixed(settings.decimals);
                    case 'ceil':
                        return (Math.ceil(value / settings.step) * settings.step).toFixed(settings.decimals);
                    default:
                        return value;
                }
            }

            function _checkValue() {
                var val, parsedval, returnval;
                val = originalinput.val();
                if (val === '') {
                    if (settings.replacementval !== '') {
                        originalinput.val(settings.replacementval);
                        originalinput.trigger('change');
                    }
                    return;
                }
                if (settings.decimals > 0 && val === '.') {
                    return;
                }
                parsedval = parseFloat(val);
                if (isNaN(parsedval)) {
                    if (settings.replacementval !== '') {
                        parsedval = settings.replacementval;
                    } else {
                        parsedval = 0;
                    }
                }
                returnval = parsedval;
                if (parsedval.toString() !== val) {
                    returnval = parsedval;
                }
                if (parsedval < settings.min) {
                    returnval = settings.min;
                }
                if (parsedval > settings.max) {
                    returnval = settings.max;
                }
                returnval = _forcestepdivisibility(returnval);
                if (Number(val).toString() !== returnval.toString()) {
                    originalinput.val(returnval);
                    originalinput.trigger('change');
                }
            }

            function _getBoostedStep() {
                if (!settings.booster) {
                    return settings.step;
                } else {
                    var boosted = Math.pow(2, Math.floor(spincount / settings.boostat)) * settings.step;
                    if (settings.maxboostedstep) {
                        if (boosted > settings.maxboostedstep) {
                            boosted = settings.maxboostedstep;
                            value = Math.round((value / boosted)) * boosted;
                        }
                    }
                    return Math.max(settings.step, boosted);
                }
            }

            function upOnce() {
                _checkValue();
                value = parseFloat(elements.input.val());
                if (isNaN(value)) {
                    value = 0;
                }
                var initvalue = value,
                    boostedstep = _getBoostedStep();
                value = value + boostedstep;
                if (value > settings.max) {
                    value = settings.max;
                    originalinput.trigger('touchspin.on.max');
                    stopSpin();
                }
                elements.input.val(Number(value).toFixed(settings.decimals));
                if (initvalue !== value) {
                    originalinput.trigger('change');
                }
            }

            function downOnce() {
                _checkValue();
                value = parseFloat(elements.input.val());
                if (isNaN(value)) {
                    value = 0;
                }
                var initvalue = value,
                    boostedstep = _getBoostedStep();
                value = value - boostedstep;
                if (value < settings.min) {
                    value = settings.min;
                    originalinput.trigger('touchspin.on.min');
                    stopSpin();
                }
                elements.input.val(value.toFixed(settings.decimals));
                if (initvalue !== value) {
                    originalinput.trigger('change');
                }
            }

            function startDownSpin() {
                stopSpin();
                spincount = 0;
                spinning = 'down';
                originalinput.trigger('touchspin.on.startspin');
                originalinput.trigger('touchspin.on.startdownspin');
                downDelayTimeout = setTimeout(function() {
                    downSpinTimer = setInterval(function() {
                        spincount++;
                        downOnce();
                    }, settings.stepinterval);
                }, settings.stepintervaldelay);
            }

            function startUpSpin() {
                stopSpin();
                spincount = 0;
                spinning = 'up';
                originalinput.trigger('touchspin.on.startspin');
                originalinput.trigger('touchspin.on.startupspin');
                upDelayTimeout = setTimeout(function() {
                    upSpinTimer = setInterval(function() {
                        spincount++;
                        upOnce();
                    }, settings.stepinterval);
                }, settings.stepintervaldelay);
            }

            function stopSpin() {
                clearTimeout(downDelayTimeout);
                clearTimeout(upDelayTimeout);
                clearInterval(downSpinTimer);
                clearInterval(upSpinTimer);
                switch (spinning) {
                    case 'up':
                        originalinput.trigger('touchspin.on.stopupspin');
                        originalinput.trigger('touchspin.on.stopspin');
                        break;
                    case 'down':
                        originalinput.trigger('touchspin.on.stopdownspin');
                        originalinput.trigger('touchspin.on.stopspin');
                        break;
                }
                spincount = 0;
                spinning = false;
            }
        });
    };
})(jQuery);
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(["jquery"], function(a0) {
            return (factory(a0));
        });
    } else if (typeof exports === 'object') {
        module.exports = factory(require("jquery"));
    } else {
        factory(jQuery);
    }
}(this, function(jQuery) {
    (function($) {
        'use strict';
        if (!String.prototype.includes) {
            (function() {
                'use strict';
                var toString = {}.toString;
                var defineProperty = (function() {
                    try {
                        var object = {};
                        var $defineProperty = Object.defineProperty;
                        var result = $defineProperty(object, object, object) && $defineProperty;
                    } catch (error) {}
                    return result;
                }());
                var indexOf = ''.indexOf;
                var includes = function(search) {
                    if (this == null) {
                        throw new TypeError();
                    }
                    var string = String(this);
                    if (search && toString.call(search) == '[object RegExp]') {
                        throw new TypeError();
                    }
                    var stringLength = string.length;
                    var searchString = String(search);
                    var searchLength = searchString.length;
                    var position = arguments.length > 1 ? arguments[1] : undefined;
                    var pos = position ? Number(position) : 0;
                    if (pos != pos) {
                        pos = 0;
                    }
                    var start = Math.min(Math.max(pos, 0), stringLength);
                    if (searchLength + start > stringLength) {
                        return false;
                    }
                    return indexOf.call(string, searchString, pos) != -1;
                };
                if (defineProperty) {
                    defineProperty(String.prototype, 'includes', {
                        'value': includes,
                        'configurable': true,
                        'writable': true
                    });
                } else {
                    String.prototype.includes = includes;
                }
            }());
        }
        if (!String.prototype.startsWith) {
            (function() {
                'use strict';
                var defineProperty = (function() {
                    try {
                        var object = {};
                        var $defineProperty = Object.defineProperty;
                        var result = $defineProperty(object, object, object) && $defineProperty;
                    } catch (error) {}
                    return result;
                }());
                var toString = {}.toString;
                var startsWith = function(search) {
                    if (this == null) {
                        throw new TypeError();
                    }
                    var string = String(this);
                    if (search && toString.call(search) == '[object RegExp]') {
                        throw new TypeError();
                    }
                    var stringLength = string.length;
                    var searchString = String(search);
                    var searchLength = searchString.length;
                    var position = arguments.length > 1 ? arguments[1] : undefined;
                    var pos = position ? Number(position) : 0;
                    if (pos != pos) {
                        pos = 0;
                    }
                    var start = Math.min(Math.max(pos, 0), stringLength);
                    if (searchLength + start > stringLength) {
                        return false;
                    }
                    var index = -1;
                    while (++index < searchLength) {
                        if (string.charCodeAt(start + index) != searchString.charCodeAt(index)) {
                            return false;
                        }
                    }
                    return true;
                };
                if (defineProperty) {
                    defineProperty(String.prototype, 'startsWith', {
                        'value': startsWith,
                        'configurable': true,
                        'writable': true
                    });
                } else {
                    String.prototype.startsWith = startsWith;
                }
            }());
        }
        if (!Object.keys) {
            Object.keys = function(o, k, r) {
                r = [];
                for (k in o)
                    r.hasOwnProperty.call(o, k) && r.push(k);
                return r;
            };
        }
        $.fn.triggerNative = function(eventName) {
            var el = this[0],
                event;
            if (el.dispatchEvent) {
                if (typeof Event === 'function') {
                    event = new Event(eventName, {
                        bubbles: true
                    });
                } else {
                    event = document.createEvent('Event');
                    event.initEvent(eventName, true, false);
                }
                el.dispatchEvent(event);
            } else {
                if (el.fireEvent) {
                    event = document.createEventObject();
                    event.eventType = eventName;
                    el.fireEvent('on' + eventName, event);
                }
                this.trigger(eventName);
            }
        };
        $.expr[':'].icontains = function(obj, index, meta) {
            var $obj = $(obj);
            var haystack = ($obj.data('tokens') || $obj.text()).toUpperCase();
            return haystack.includes(meta[3].toUpperCase());
        };
        $.expr[':'].ibegins = function(obj, index, meta) {
            var $obj = $(obj);
            var haystack = ($obj.data('tokens') || $obj.text()).toUpperCase();
            return haystack.startsWith(meta[3].toUpperCase());
        };
        $.expr[':'].aicontains = function(obj, index, meta) {
            var $obj = $(obj);
            var haystack = ($obj.data('tokens') || $obj.data('normalizedText') || $obj.text()).toUpperCase();
            return haystack.includes(meta[3].toUpperCase());
        };
        $.expr[':'].aibegins = function(obj, index, meta) {
            var $obj = $(obj);
            var haystack = ($obj.data('tokens') || $obj.data('normalizedText') || $obj.text()).toUpperCase();
            return haystack.startsWith(meta[3].toUpperCase());
        };

        function normalizeToBase(text) {
            var rExps = [{
                re: /[\xC0-\xC6]/g,
                ch: "A"
            }, {
                re: /[\xE0-\xE6]/g,
                ch: "a"
            }, {
                re: /[\xC8-\xCB]/g,
                ch: "E"
            }, {
                re: /[\xE8-\xEB]/g,
                ch: "e"
            }, {
                re: /[\xCC-\xCF]/g,
                ch: "I"
            }, {
                re: /[\xEC-\xEF]/g,
                ch: "i"
            }, {
                re: /[\xD2-\xD6]/g,
                ch: "O"
            }, {
                re: /[\xF2-\xF6]/g,
                ch: "o"
            }, {
                re: /[\xD9-\xDC]/g,
                ch: "U"
            }, {
                re: /[\xF9-\xFC]/g,
                ch: "u"
            }, {
                re: /[\xC7-\xE7]/g,
                ch: "c"
            }, {
                re: /[\xD1]/g,
                ch: "N"
            }, {
                re: /[\xF1]/g,
                ch: "n"
            }];
            $.each(rExps, function() {
                text = text.replace(this.re, this.ch);
            });
            return text;
        }

        function htmlEscape(html) {
            var escapeMap = {
                '&': '&amp;',
                '<': '&lt;',
                '>': '&gt;',
                '"': '&quot;',
                "'": '&#x27;',
                '`': '&#x60;'
            };
            var source = '(?:' + Object.keys(escapeMap).join('|') + ')',
                testRegexp = new RegExp(source),
                replaceRegexp = new RegExp(source, 'g'),
                string = html == null ? '' : '' + html;
            return testRegexp.test(string) ? string.replace(replaceRegexp, function(match) {
                return escapeMap[match];
            }) : string;
        }
        var Selectpicker = function(element, options, e) {
            if (e) {
                e.stopPropagation();
                e.preventDefault();
            }
            this.$element = $(element);
            this.$newElement = null;
            this.$button = null;
            this.$menu = null;
            this.$lis = null;
            this.options = options;
            if (this.options.title === null) {
                this.options.title = this.$element.attr('title');
            }
            this.val = Selectpicker.prototype.val;
            this.render = Selectpicker.prototype.render;
            this.refresh = Selectpicker.prototype.refresh;
            this.setStyle = Selectpicker.prototype.setStyle;
            this.selectAll = Selectpicker.prototype.selectAll;
            this.deselectAll = Selectpicker.prototype.deselectAll;
            this.destroy = Selectpicker.prototype.destroy;
            this.remove = Selectpicker.prototype.remove;
            this.show = Selectpicker.prototype.show;
            this.hide = Selectpicker.prototype.hide;
            this.init();
        };
        Selectpicker.VERSION = '1.10.0';
        Selectpicker.DEFAULTS = {
            noneSelectedText: 'Nothing selected',
            noneResultsText: 'No results matched {0}',
            countSelectedText: function(numSelected, numTotal) {
                return (numSelected == 1) ? "{0} item selected" : "{0} items selected";
            },
            maxOptionsText: function(numAll, numGroup) {
                return [(numAll == 1) ? 'Limit reached ({n} item max)' : 'Limit reached ({n} items max)', (numGroup == 1) ? 'Group limit reached ({n} item max)' : 'Group limit reached ({n} items max)'];
            },
            selectAllText: 'Select All',
            deselectAllText: 'Deselect All',
            doneButton: false,
            doneButtonText: 'Close',
            multipleSeparator: ', ',
            styleBase: 'btn',
            style: 'btn-default',
            size: 'auto',
            title: null,
            selectedTextFormat: 'values',
            width: false,
            container: false,
            hideDisabled: false,
            showSubtext: false,
            showIcon: true,
            showContent: true,
            dropupAuto: true,
            header: false,
            liveSearch: false,
            liveSearchPlaceholder: null,
            liveSearchNormalize: false,
            liveSearchStyle: 'contains',
            actionsBox: false,
            iconBase: 'glyphicon',
            tickIcon: 'glyphicon-ok',
            showTick: false,
            template: {
                caret: '<span class="caret"></span>'
            },
            maxOptions: false,
            mobile: false,
            selectOnTab: false,
            dropdownAlignRight: false
        };
        Selectpicker.prototype = {
            constructor: Selectpicker,
            init: function() {
                var that = this,
                    id = this.$element.attr('id');
                this.$element.addClass('bs-select-hidden');
                this.liObj = {};
                this.multiple = this.$element.prop('multiple');
                this.autofocus = this.$element.prop('autofocus');
                this.$newElement = this.createView();
                this.$element.after(this.$newElement).appendTo(this.$newElement);
                this.$button = this.$newElement.children('button');
                this.$menu = this.$newElement.children('.dropdown-menu');
                this.$menuInner = this.$menu.children('.inner');
                this.$searchbox = this.$menu.find('input');
                this.$element.removeClass('bs-select-hidden');
                if (this.options.dropdownAlignRight === true) this.$menu.addClass('dropdown-menu-right');
                if (typeof id !== 'undefined') {
                    this.$button.attr('data-id', id);
                    $('label[for="' + id + '"]').click(function(e) {
                        e.preventDefault();
                        that.$button.focus();
                    });
                }
                this.checkDisabled();
                this.clickListener();
                if (this.options.liveSearch) this.liveSearchListener();
                this.render();
                this.setStyle();
                this.setWidth();
                if (this.options.container) this.selectPosition();
                this.$menu.data('this', this);
                this.$newElement.data('this', this);
                if (this.options.mobile) this.mobile();
                this.$newElement.on({
                    'hide.bs.dropdown': function(e) {
                        that.$element.trigger('hide.bs.select', e);
                    },
                    'hidden.bs.dropdown': function(e) {
                        that.$element.trigger('hidden.bs.select', e);
                    },
                    'show.bs.dropdown': function(e) {
                        that.$element.trigger('show.bs.select', e);
                    },
                    'shown.bs.dropdown': function(e) {
                        that.$element.trigger('shown.bs.select', e);
                    }
                });
                if (that.$element[0].hasAttribute('required')) {
                    this.$element.on('invalid', function() {
                        that.$button.addClass('bs-invalid').focus();
                        that.$element.on({
                            'focus.bs.select': function() {
                                that.$button.focus();
                                that.$element.off('focus.bs.select');
                            },
                            'shown.bs.select': function() {
                                that.$element.val(that.$element.val()).off('shown.bs.select');
                            },
                            'rendered.bs.select': function() {
                                if (this.validity.valid) that.$button.removeClass('bs-invalid');
                                that.$element.off('rendered.bs.select');
                            }
                        });
                    });
                }
                setTimeout(function() {
                    that.$element.trigger('loaded.bs.select');
                });
            },
            createDropdown: function() {
                var showTick = (this.multiple || this.options.showTick) ? ' show-tick' : '',
                    inputGroup = this.$element.parent().hasClass('input-group') ? ' input-group-btn' : '',
                    autofocus = this.autofocus ? ' autofocus' : '';
                var header = this.options.header ? '<div class="popover-title"><button type="button" class="close" aria-hidden="true">&times;</button>' + this.options.header + '</div>' : '';
                var searchbox = this.options.liveSearch ? '<div class="bs-searchbox">' + '<input type="text" class="form-control" autocomplete="off"' +
                    (null === this.options.liveSearchPlaceholder ? '' : ' placeholder="' + htmlEscape(this.options.liveSearchPlaceholder) + '"') + '>' + '</div>' : '';
                var actionsbox = this.multiple && this.options.actionsBox ? '<div class="bs-actionsbox">' + '<div class="btn-group btn-group-sm btn-block">' + '<button type="button" class="actions-btn bs-select-all btn btn-default">' +
                    this.options.selectAllText + '</button>' + '<button type="button" class="actions-btn bs-deselect-all btn btn-default">' +
                    this.options.deselectAllText + '</button>' + '</div>' + '</div>' : '';
                var donebutton = this.multiple && this.options.doneButton ? '<div class="bs-donebutton">' + '<div class="btn-group btn-block">' + '<button type="button" class="btn btn-sm btn-default">' +
                    this.options.doneButtonText + '</button>' + '</div>' + '</div>' : '';
                var drop = '<div class="btn-group bootstrap-select' + showTick + inputGroup + '">' + '<button type="button" class="' + this.options.styleBase + ' dropdown-toggle" data-toggle="dropdown"' + autofocus + '>' + '<span class="filter-option pull-left"></span>&nbsp;' + '<span class="bs-caret">' +
                    this.options.template.caret + '</span>' + '</button>' + '<div class="dropdown-menu open">' +
                    header +
                    searchbox +
                    actionsbox + '<ul class="dropdown-menu inner" role="menu">' + '</ul>' +
                    donebutton + '</div>' + '</div>';
                return $(drop);
            },
            createView: function() {
                var $drop = this.createDropdown(),
                    li = this.createLi();
                $drop.find('ul')[0].innerHTML = li;
                return $drop;
            },
            reloadLi: function() {
                this.destroyLi();
                var li = this.createLi();
                this.$menuInner[0].innerHTML = li;
            },
            destroyLi: function() {
                this.$menu.find('li').remove();
            },
            createLi: function() {
                var that = this,
                    _li = [],
                    optID = 0,
                    titleOption = document.createElement('option'),
                    liIndex = -1;
                var generateLI = function(content, index, classes, optgroup) {
                    return '<li' +
                        ((typeof classes !== 'undefined' & '' !== classes) ? ' class="' + classes + '"' : '') +
                        ((typeof index !== 'undefined' & null !== index) ? ' data-original-index="' + index + '"' : '') +
                        ((typeof optgroup !== 'undefined' & null !== optgroup) ? 'data-optgroup="' + optgroup + '"' : '') + '>' + content + '</li>';
                };
                var generateA = function(text, classes, inline, tokens) {
                    return '<a tabindex="0"' +
                        (typeof classes !== 'undefined' ? ' class="' + classes + '"' : '') +
                        (typeof inline !== 'undefined' ? ' style="' + inline + '"' : '') +
                        (that.options.liveSearchNormalize ? ' data-normalized-text="' + normalizeToBase(htmlEscape(text)) + '"' : '') +
                        (typeof tokens !== 'undefined' || tokens !== null ? ' data-tokens="' + tokens + '"' : '') + '>' + text + '<span class="' + that.options.iconBase + ' ' + that.options.tickIcon + ' check-mark"></span>' + '</a>';
                };
                if (this.options.title && !this.multiple) {
                    liIndex--;
                    if (!this.$element.find('.bs-title-option').length) {
                        var element = this.$element[0];
                        titleOption.className = 'bs-title-option';
                        titleOption.appendChild(document.createTextNode(this.options.title));
                        titleOption.value = '';
                        element.insertBefore(titleOption, element.firstChild);
                        if ($(element.options[element.selectedIndex]).attr('selected') === undefined) titleOption.selected = true;
                    }
                }
                this.$element.find('option').each(function(index) {
                    var $this = $(this);
                    liIndex++;
                    if ($this.hasClass('bs-title-option')) return;
                    var optionClass = this.className || '',
                        inline = this.style.cssText,
                        text = $this.data('content') ? $this.data('content') : $this.html(),
                        tokens = $this.data('tokens') ? $this.data('tokens') : null,
                        subtext = typeof $this.data('subtext') !== 'undefined' ? '<small class="text-muted">' + $this.data('subtext') + '</small>' : '',
                        icon = typeof $this.data('icon') !== 'undefined' ? '<span class="' + that.options.iconBase + ' ' + $this.data('icon') + '"></span> ' : '',
                        isOptgroup = this.parentNode.tagName === 'OPTGROUP',
                        isDisabled = this.disabled || (isOptgroup && this.parentNode.disabled);
                    if (icon !== '' && isDisabled) {
                        icon = '<span>' + icon + '</span>';
                    }
                    if (that.options.hideDisabled && isDisabled && !isOptgroup) {
                        liIndex--;
                        return;
                    }
                    if (!$this.data('content')) {
                        text = icon + '<span class="text">' + text + subtext + '</span>';
                    }
                    if (isOptgroup && $this.data('divider') !== true) {
                        var optGroupClass = ' ' + this.parentNode.className || '';
                        if ($this.index() === 0) {
                            optID += 1;
                            var label = this.parentNode.label,
                                labelSubtext = typeof $this.parent().data('subtext') !== 'undefined' ? '<small class="text-muted">' + $this.parent().data('subtext') + '</small>' : '',
                                labelIcon = $this.parent().data('icon') ? '<span class="' + that.options.iconBase + ' ' + $this.parent().data('icon') + '"></span> ' : '';
                            label = labelIcon + '<span class="text">' + label + labelSubtext + '</span>';
                            if (index !== 0 && _li.length > 0) {
                                liIndex++;
                                _li.push(generateLI('', null, 'divider', optID + 'div'));
                            }
                            liIndex++;
                            _li.push(generateLI(label, null, 'dropdown-header' + optGroupClass, optID));
                        }
                        if (that.options.hideDisabled && isDisabled) {
                            liIndex--;
                            return;
                        }
                        _li.push(generateLI(generateA(text, 'opt ' + optionClass + optGroupClass, inline, tokens), index, '', optID));
                    } else if ($this.data('divider') === true) {
                        _li.push(generateLI('', index, 'divider'));
                    } else if ($this.data('hidden') === true) {
                        _li.push(generateLI(generateA(text, optionClass, inline, tokens), index, 'hidden is-hidden'));
                    } else {
                        if (this.previousElementSibling && this.previousElementSibling.tagName === 'OPTGROUP') {
                            liIndex++;
                            _li.push(generateLI('', null, 'divider', optID + 'div'));
                        }
                        _li.push(generateLI(generateA(text, optionClass, inline, tokens), index));
                    }
                    that.liObj[index] = liIndex;
                });
                if (!this.multiple && this.$element.find('option:selected').length === 0 && !this.options.title) {
                    this.$element.find('option').eq(0).prop('selected', true).attr('selected', 'selected');
                }
                return _li.join('');
            },
            findLis: function() {
                if (this.$lis == null) this.$lis = this.$menu.find('li');
                return this.$lis;
            },
            render: function(updateLi) {
                var that = this,
                    notDisabled;
                if (updateLi !== false) {
                    this.$element.find('option').each(function(index) {
                        var $lis = that.findLis().eq(that.liObj[index]);
                        that.setDisabled(index, this.disabled || this.parentNode.tagName === 'OPTGROUP' && this.parentNode.disabled, $lis);
                        that.setSelected(index, this.selected, $lis);
                    });
                    this.togglePlaceholder();
                }
                this.tabIndex();
                var selectedItems = this.$element.find('option').map(function() {
                    if (this.selected) {
                        if (that.options.hideDisabled && (this.disabled || this.parentNode.tagName === 'OPTGROUP' && this.parentNode.disabled)) return;
                        var $this = $(this),
                            icon = $this.data('icon') && that.options.showIcon ? '<i class="' + that.options.iconBase + ' ' + $this.data('icon') + '"></i> ' : '',
                            subtext;
                        if (that.options.showSubtext && $this.data('subtext') && !that.multiple) {
                            subtext = ' <small class="text-muted">' + $this.data('subtext') + '</small>';
                        } else {
                            subtext = '';
                        }
                        if (typeof $this.attr('title') !== 'undefined') {
                            return $this.attr('title');
                        } else if ($this.data('content') && that.options.showContent) {
                            return $this.data('content');
                        } else {
                            return icon + $this.html() + subtext;
                        }
                    }
                }).toArray();
                var title = !this.multiple ? selectedItems[0] : selectedItems.join(this.options.multipleSeparator);
                if (this.multiple && this.options.selectedTextFormat.indexOf('count') > -1) {
                    var max = this.options.selectedTextFormat.split('>');
                    if ((max.length > 1 && selectedItems.length > max[1]) || (max.length == 1 && selectedItems.length >= 2)) {
                        notDisabled = this.options.hideDisabled ? ', [disabled]' : '';
                        var totalCount = this.$element.find('option').not('[data-divider="true"], [data-hidden="true"]' + notDisabled).length,
                            tr8nText = (typeof this.options.countSelectedText === 'function') ? this.options.countSelectedText(selectedItems.length, totalCount) : this.options.countSelectedText;
                        title = tr8nText.replace('{0}', selectedItems.length.toString()).replace('{1}', totalCount.toString());
                    }
                }
                if (this.options.title == undefined) {
                    this.options.title = this.$element.attr('title');
                }
                if (this.options.selectedTextFormat == 'static') {
                    title = this.options.title;
                }
                if (!title) {
                    title = typeof this.options.title !== 'undefined' ? this.options.title : this.options.noneSelectedText;
                }
                this.$button.attr('title', $.trim(title.replace(/<[^>]*>?/g, '')));
                this.$button.children('.filter-option').html(title);
                this.$element.trigger('rendered.bs.select');
            },
            setStyle: function(style, status) {
                if (this.$element.attr('class')) {
                    this.$newElement.addClass(this.$element.attr('class').replace(/selectpicker|mobile-device|bs-select-hidden|validate\[.*\]/gi, ''));
                }
                var buttonClass = style ? style : this.options.style;
                if (status == 'add') {
                    this.$button.addClass(buttonClass);
                } else if (status == 'remove') {
                    this.$button.removeClass(buttonClass);
                } else {
                    this.$button.removeClass(this.options.style);
                    this.$button.addClass(buttonClass);
                }
            },
            liHeight: function(refresh) {
                if (!refresh && (this.options.size === false || this.sizeInfo)) return;
                var newElement = document.createElement('div'),
                    menu = document.createElement('div'),
                    menuInner = document.createElement('ul'),
                    divider = document.createElement('li'),
                    li = document.createElement('li'),
                    a = document.createElement('a'),
                    text = document.createElement('span'),
                    header = this.options.header && this.$menu.find('.popover-title').length > 0 ? this.$menu.find('.popover-title')[0].cloneNode(true) : null,
                    search = this.options.liveSearch ? document.createElement('div') : null,
                    actions = this.options.actionsBox && this.multiple && this.$menu.find('.bs-actionsbox').length > 0 ? this.$menu.find('.bs-actionsbox')[0].cloneNode(true) : null,
                    doneButton = this.options.doneButton && this.multiple && this.$menu.find('.bs-donebutton').length > 0 ? this.$menu.find('.bs-donebutton')[0].cloneNode(true) : null;
                text.className = 'text';
                newElement.className = this.$menu[0].parentNode.className + ' open';
                menu.className = 'dropdown-menu open';
                menuInner.className = 'dropdown-menu inner';
                divider.className = 'divider';
                text.appendChild(document.createTextNode('Inner text'));
                a.appendChild(text);
                li.appendChild(a);
                menuInner.appendChild(li);
                menuInner.appendChild(divider);
                if (header) menu.appendChild(header);
                if (search) {
                    var input = document.createElement('span');
                    search.className = 'bs-searchbox';
                    input.className = 'form-control';
                    search.appendChild(input);
                    menu.appendChild(search);
                }
                if (actions) menu.appendChild(actions);
                menu.appendChild(menuInner);
                if (doneButton) menu.appendChild(doneButton);
                newElement.appendChild(menu);
                document.body.appendChild(newElement);
                var liHeight = a.offsetHeight,
                    headerHeight = header ? header.offsetHeight : 0,
                    searchHeight = search ? search.offsetHeight : 0,
                    actionsHeight = actions ? actions.offsetHeight : 0,
                    doneButtonHeight = doneButton ? doneButton.offsetHeight : 0,
                    dividerHeight = $(divider).outerHeight(true),
                    menuStyle = typeof getComputedStyle === 'function' ? getComputedStyle(menu) : false,
                    $menu = menuStyle ? null : $(menu),
                    menuPadding = {
                        vert: parseInt(menuStyle ? menuStyle.paddingTop : $menu.css('paddingTop')) +
                            parseInt(menuStyle ? menuStyle.paddingBottom : $menu.css('paddingBottom')) +
                            parseInt(menuStyle ? menuStyle.borderTopWidth : $menu.css('borderTopWidth')) +
                            parseInt(menuStyle ? menuStyle.borderBottomWidth : $menu.css('borderBottomWidth')),
                        horiz: parseInt(menuStyle ? menuStyle.paddingLeft : $menu.css('paddingLeft')) +
                            parseInt(menuStyle ? menuStyle.paddingRight : $menu.css('paddingRight')) +
                            parseInt(menuStyle ? menuStyle.borderLeftWidth : $menu.css('borderLeftWidth')) +
                            parseInt(menuStyle ? menuStyle.borderRightWidth : $menu.css('borderRightWidth'))
                    },
                    menuExtras = {
                        vert: menuPadding.vert +
                            parseInt(menuStyle ? menuStyle.marginTop : $menu.css('marginTop')) +
                            parseInt(menuStyle ? menuStyle.marginBottom : $menu.css('marginBottom')) + 2,
                        horiz: menuPadding.horiz +
                            parseInt(menuStyle ? menuStyle.marginLeft : $menu.css('marginLeft')) +
                            parseInt(menuStyle ? menuStyle.marginRight : $menu.css('marginRight')) + 2
                    }
                document.body.removeChild(newElement);
                this.sizeInfo = {
                    liHeight: liHeight,
                    headerHeight: headerHeight,
                    searchHeight: searchHeight,
                    actionsHeight: actionsHeight,
                    doneButtonHeight: doneButtonHeight,
                    dividerHeight: dividerHeight,
                    menuPadding: menuPadding,
                    menuExtras: menuExtras
                };
            },
            setSize: function() {
                this.findLis();
                this.liHeight();
                if (this.options.header) this.$menu.css('padding-top', 0);
                if (this.options.size === false) return;
                var that = this,
                    $menu = this.$menu,
                    $menuInner = this.$menuInner,
                    $window = $(window),
                    selectHeight = this.$newElement[0].offsetHeight,
                    selectWidth = this.$newElement[0].offsetWidth,
                    liHeight = this.sizeInfo['liHeight'],
                    headerHeight = this.sizeInfo['headerHeight'],
                    searchHeight = this.sizeInfo['searchHeight'],
                    actionsHeight = this.sizeInfo['actionsHeight'],
                    doneButtonHeight = this.sizeInfo['doneButtonHeight'],
                    divHeight = this.sizeInfo['dividerHeight'],
                    menuPadding = this.sizeInfo['menuPadding'],
                    menuExtras = this.sizeInfo['menuExtras'],
                    notDisabled = this.options.hideDisabled ? '.disabled' : '',
                    menuHeight, menuWidth, getHeight, getWidth, selectOffsetTop, selectOffsetBot, selectOffsetLeft, selectOffsetRight, getPos = function() {
                        var pos = that.$newElement.offset();
                        selectOffsetTop = pos.top - $window.scrollTop();
                        selectOffsetBot = $window.height() - selectOffsetTop - selectHeight;
                        selectOffsetLeft = pos.left - $window.scrollLeft();
                        selectOffsetRight = $window.width() - selectOffsetLeft - selectWidth;
                    };
                getPos();
                if (this.options.size === 'auto') {
                    var getSize = function() {
                        var minHeight, hasClass = function(className, include) {
                                return function(element) {
                                    if (include) {
                                        return (element.classList ? element.classList.contains(className) : $(element).hasClass(className));
                                    } else {
                                        return !(element.classList ? element.classList.contains(className) : $(element).hasClass(className));
                                    }
                                };
                            },
                            lis = that.$menuInner[0].getElementsByTagName('li'),
                            lisVisible = Array.prototype.filter ? Array.prototype.filter.call(lis, hasClass('hidden', false)) : that.$lis.not('.hidden'),
                            optGroup = Array.prototype.filter ? Array.prototype.filter.call(lisVisible, hasClass('dropdown-header', true)) : lisVisible.filter('.dropdown-header');
                        getPos();
                        menuHeight = selectOffsetBot - menuExtras.vert;
                        menuWidth = selectOffsetRight - menuExtras.horiz;
                        if (that.options.container) {
                            if (!$menu.data('height')) $menu.data('height', $menu.height());
                            getHeight = $menu.data('height');
                            if (!$menu.data('width')) $menu.data('width', $menu.width());
                            getWidth = $menu.data('width');
                        } else {
                            getHeight = $menu.height();
                            getWidth = $menu.width();
                        }
                        if (that.options.dropupAuto) {
                            that.$newElement.toggleClass('dropup', selectOffsetTop > selectOffsetBot && (menuHeight - menuExtras.vert) < getHeight);
                        }
                        if (that.$newElement.hasClass('dropup')) {
                            menuHeight = selectOffsetTop - menuExtras.vert;
                        }
                        if (that.options.dropdownAlignRight === 'auto') {
                            $menu.toggleClass('dropdown-menu-right', selectOffsetLeft > selectOffsetRight && (menuWidth - menuExtras.horiz) < (getWidth - selectWidth));
                        }
                        if ((lisVisible.length + optGroup.length) > 3) {
                            minHeight = liHeight * 3 + menuExtras.vert - 2;
                        } else {
                            minHeight = 0;
                        }
                        $menu.css({
                            'max-height': menuHeight + 'px',
                            'overflow': 'hidden',
                            'min-height': minHeight + headerHeight + searchHeight + actionsHeight + doneButtonHeight + 'px'
                        });
                        $menuInner.css({
                            'max-height': menuHeight - headerHeight - searchHeight - actionsHeight - doneButtonHeight - menuPadding.vert + 'px',
                            'overflow-y': 'auto',
                            'min-height': Math.max(minHeight - menuPadding.vert, 0) + 'px'
                        });
                    };
                    getSize();
                    this.$searchbox.off('input.getSize propertychange.getSize').on('input.getSize propertychange.getSize', getSize);
                    $window.off('resize.getSize scroll.getSize').on('resize.getSize scroll.getSize', getSize);
                } else if (this.options.size && this.options.size != 'auto' && this.$lis.not(notDisabled).length > this.options.size) {
                    var optIndex = this.$lis.not('.divider').not(notDisabled).children().slice(0, this.options.size).last().parent().index(),
                        divLength = this.$lis.slice(0, optIndex + 1).filter('.divider').length;
                    menuHeight = liHeight * this.options.size + divLength * divHeight + menuPadding.vert;
                    if (that.options.container) {
                        if (!$menu.data('height')) $menu.data('height', $menu.height());
                        getHeight = $menu.data('height');
                    } else {
                        getHeight = $menu.height();
                    }
                    if (that.options.dropupAuto) {
                        this.$newElement.toggleClass('dropup', selectOffsetTop > selectOffsetBot && (menuHeight - menuExtras.vert) < getHeight);
                    }
                    $menu.css({
                        'max-height': menuHeight + headerHeight + searchHeight + actionsHeight + doneButtonHeight + 'px',
                        'overflow': 'hidden',
                        'min-height': ''
                    });
                    $menuInner.css({
                        'max-height': menuHeight - menuPadding.vert + 'px',
                        'overflow-y': 'auto',
                        'min-height': ''
                    });
                }
            },
            setWidth: function() {
                if (this.options.width === 'auto') {
                    this.$menu.css('min-width', '0');
                    var $selectClone = this.$menu.parent().clone().appendTo('body'),
                        $selectClone2 = this.options.container ? this.$newElement.clone().appendTo('body') : $selectClone,
                        ulWidth = $selectClone.children('.dropdown-menu').outerWidth(),
                        btnWidth = $selectClone2.css('width', 'auto').children('button').outerWidth();
                    $selectClone.remove();
                    $selectClone2.remove();
                    this.$newElement.css('width', Math.max(ulWidth, btnWidth) + 'px');
                } else if (this.options.width === 'fit') {
                    this.$menu.css('min-width', '');
                    this.$newElement.css('width', '').addClass('fit-width');
                } else if (this.options.width) {
                    this.$menu.css('min-width', '');
                    this.$newElement.css('width', this.options.width);
                } else {
                    this.$menu.css('min-width', '');
                    this.$newElement.css('width', '');
                }
                if (this.$newElement.hasClass('fit-width') && this.options.width !== 'fit') {
                    this.$newElement.removeClass('fit-width');
                }
            },
            selectPosition: function() {
                this.$bsContainer = $('<div class="bs-container" />');
                var that = this,
                    pos, actualHeight, getPlacement = function($element) {
                        that.$bsContainer.addClass($element.attr('class').replace(/form-control|fit-width/gi, '')).toggleClass('dropup', $element.hasClass('dropup'));
                        pos = $element.offset();
                        actualHeight = $element.hasClass('dropup') ? 0 : $element[0].offsetHeight;
                        that.$bsContainer.css({
                            'top': pos.top + actualHeight,
                            'left': pos.left,
                            'width': $element[0].offsetWidth
                        });
                    };
                this.$button.on('click', function() {
                    var $this = $(this);
                    if (that.isDisabled()) {
                        return;
                    }
                    getPlacement(that.$newElement);
                    that.$bsContainer.appendTo(that.options.container).toggleClass('open', !$this.hasClass('open')).append(that.$menu);
                });
                $(window).on('resize scroll', function() {
                    getPlacement(that.$newElement);
                });
                this.$element.on('hide.bs.select', function() {
                    that.$menu.data('height', that.$menu.height());
                    that.$bsContainer.detach();
                });
            },
            setSelected: function(index, selected, $lis) {
                if (!$lis) {
                    this.togglePlaceholder();
                    $lis = this.findLis().eq(this.liObj[index]);
                }
                $lis.toggleClass('selected', selected);
            },
            setDisabled: function(index, disabled, $lis) {
                if (!$lis) {
                    $lis = this.findLis().eq(this.liObj[index]);
                }
                if (disabled) {
                    $lis.addClass('disabled').children('a').attr('href', '#').attr('tabindex', -1);
                } else {
                    $lis.removeClass('disabled').children('a').removeAttr('href').attr('tabindex', 0);
                }
            },
            isDisabled: function() {
                return this.$element[0].disabled;
            },
            checkDisabled: function() {
                var that = this;
                if (this.isDisabled()) {
                    this.$newElement.addClass('disabled');
                    this.$button.addClass('disabled').attr('tabindex', -1);
                } else {
                    if (this.$button.hasClass('disabled')) {
                        this.$newElement.removeClass('disabled');
                        this.$button.removeClass('disabled');
                    }
                    if (this.$button.attr('tabindex') == -1 && !this.$element.data('tabindex')) {
                        this.$button.removeAttr('tabindex');
                    }
                }
                this.$button.click(function() {
                    return !that.isDisabled();
                });
            },
            togglePlaceholder: function() {
                var value = this.$element.val();
                this.$button.toggleClass('bs-placeholder', value === null || value === '');
            },
            tabIndex: function() {
                if (this.$element.data('tabindex') !== this.$element.attr('tabindex') && (this.$element.attr('tabindex') !== -98 && this.$element.attr('tabindex') !== '-98')) {
                    this.$element.data('tabindex', this.$element.attr('tabindex'));
                    this.$button.attr('tabindex', this.$element.data('tabindex'));
                }
                this.$element.attr('tabindex', -98);
            },
            clickListener: function() {
                var that = this,
                    $document = $(document);
                this.$newElement.on('touchstart.dropdown', '.dropdown-menu', function(e) {
                    e.stopPropagation();
                });
                $document.data('spaceSelect', false);
                this.$button.on('keyup', function(e) {
                    if (/(32)/.test(e.keyCode.toString(10)) && $document.data('spaceSelect')) {
                        e.preventDefault();
                        $document.data('spaceSelect', false);
                    }
                });
                this.$button.on('click', function() {
                    that.setSize();
                });
                this.$element.on('shown.bs.select', function() {
                    if (!that.options.liveSearch && !that.multiple) {
                        that.$menuInner.find('.selected a').focus();
                    } else if (!that.multiple) {
                        var selectedIndex = that.liObj[that.$element[0].selectedIndex];
                        if (typeof selectedIndex !== 'number' || that.options.size === false) return;
                        var offset = that.$lis.eq(selectedIndex)[0].offsetTop - that.$menuInner[0].offsetTop;
                        offset = offset - that.$menuInner[0].offsetHeight / 2 + that.sizeInfo.liHeight / 2;
                        that.$menuInner[0].scrollTop = offset;
                    }
                });
                this.$menuInner.on('click', 'li a', function(e) {
                    var $this = $(this),
                        clickedIndex = $this.parent().data('originalIndex'),
                        prevValue = that.$element.val(),
                        prevIndex = that.$element.prop('selectedIndex'),
                        triggerChange = true;
                    if (that.multiple && that.options.maxOptions !== 1) {
                        e.stopPropagation();
                    }
                    e.preventDefault();
                    if (!that.isDisabled() && !$this.parent().hasClass('disabled')) {
                        var $options = that.$element.find('option'),
                            $option = $options.eq(clickedIndex),
                            state = $option.prop('selected'),
                            $optgroup = $option.parent('optgroup'),
                            maxOptions = that.options.maxOptions,
                            maxOptionsGrp = $optgroup.data('maxOptions') || false;
                        if (!that.multiple) {
                            $options.prop('selected', false);
                            $option.prop('selected', true);
                            that.$menuInner.find('.selected').removeClass('selected');
                            that.setSelected(clickedIndex, true);
                        } else {
                            $option.prop('selected', !state);
                            that.setSelected(clickedIndex, !state);
                            $this.blur();
                            if (maxOptions !== false || maxOptionsGrp !== false) {
                                var maxReached = maxOptions < $options.filter(':selected').length,
                                    maxReachedGrp = maxOptionsGrp < $optgroup.find('option:selected').length;
                                if ((maxOptions && maxReached) || (maxOptionsGrp && maxReachedGrp)) {
                                    if (maxOptions && maxOptions == 1) {
                                        $options.prop('selected', false);
                                        $option.prop('selected', true);
                                        that.$menuInner.find('.selected').removeClass('selected');
                                        that.setSelected(clickedIndex, true);
                                    } else if (maxOptionsGrp && maxOptionsGrp == 1) {
                                        $optgroup.find('option:selected').prop('selected', false);
                                        $option.prop('selected', true);
                                        var optgroupID = $this.parent().data('optgroup');
                                        that.$menuInner.find('[data-optgroup="' + optgroupID + '"]').removeClass('selected');
                                        that.setSelected(clickedIndex, true);
                                    } else {
                                        var maxOptionsArr = (typeof that.options.maxOptionsText === 'function') ? that.options.maxOptionsText(maxOptions, maxOptionsGrp) : that.options.maxOptionsText,
                                            maxTxt = maxOptionsArr[0].replace('{n}', maxOptions),
                                            maxTxtGrp = maxOptionsArr[1].replace('{n}', maxOptionsGrp),
                                            $notify = $('<div class="notify"></div>');
                                        if (maxOptionsArr[2]) {
                                            maxTxt = maxTxt.replace('{var}', maxOptionsArr[2][maxOptions > 1 ? 0 : 1]);
                                            maxTxtGrp = maxTxtGrp.replace('{var}', maxOptionsArr[2][maxOptionsGrp > 1 ? 0 : 1]);
                                        }
                                        $option.prop('selected', false);
                                        that.$menu.append($notify);
                                        if (maxOptions && maxReached) {
                                            $notify.append($('<div>' + maxTxt + '</div>'));
                                            triggerChange = false;
                                            that.$element.trigger('maxReached.bs.select');
                                        }
                                        if (maxOptionsGrp && maxReachedGrp) {
                                            $notify.append($('<div>' + maxTxtGrp + '</div>'));
                                            triggerChange = false;
                                            that.$element.trigger('maxReachedGrp.bs.select');
                                        }
                                        setTimeout(function() {
                                            that.setSelected(clickedIndex, false);
                                        }, 10);
                                        $notify.delay(750).fadeOut(300, function() {
                                            $(this).remove();
                                        });
                                    }
                                }
                            }
                        }
                        if (!that.multiple || (that.multiple && that.options.maxOptions === 1)) {
                            that.$button.focus();
                        } else if (that.options.liveSearch) {
                            that.$searchbox.focus();
                        }
                        if (triggerChange) {
                            if ((prevValue != that.$element.val() && that.multiple) || (prevIndex != that.$element.prop('selectedIndex') && !that.multiple)) {
                                that.$element.trigger('changed.bs.select', [clickedIndex, $option.prop('selected'), state]).triggerNative('change');
                            }
                        }
                    }
                });
                this.$menu.on('click', 'li.disabled a, .popover-title, .popover-title :not(.close)', function(e) {
                    if (e.currentTarget == this) {
                        e.preventDefault();
                        e.stopPropagation();
                        if (that.options.liveSearch && !$(e.target).hasClass('close')) {
                            that.$searchbox.focus();
                        } else {
                            that.$button.focus();
                        }
                    }
                });
                this.$menuInner.on('click', '.divider, .dropdown-header', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (that.options.liveSearch) {
                        that.$searchbox.focus();
                    } else {
                        that.$button.focus();
                    }
                });
                this.$menu.on('click', '.popover-title .close', function() {
                    that.$button.click();
                });
                this.$searchbox.on('click', function(e) {
                    e.stopPropagation();
                });
                this.$menu.on('click', '.actions-btn', function(e) {
                    if (that.options.liveSearch) {
                        that.$searchbox.focus();
                    } else {
                        that.$button.focus();
                    }
                    e.preventDefault();
                    e.stopPropagation();
                    if ($(this).hasClass('bs-select-all')) {
                        that.selectAll();
                    } else {
                        that.deselectAll();
                    }
                });
                this.$element.change(function() {
                    that.render(false);
                });
            },
            liveSearchListener: function() {
                var that = this,
                    $no_results = $('<li class="no-results"></li>');
                this.$button.on('click.dropdown.data-api touchstart.dropdown.data-api', function() {
                    that.$menuInner.find('.active').removeClass('active');
                    if (!!that.$searchbox.val()) {
                        that.$searchbox.val('');
                        that.$lis.not('.is-hidden').removeClass('hidden');
                        if (!!$no_results.parent().length) $no_results.remove();
                    }
                    if (!that.multiple) that.$menuInner.find('.selected').addClass('active');
                    setTimeout(function() {
                        that.$searchbox.focus();
                    }, 10);
                });
                this.$searchbox.on('click.dropdown.data-api focus.dropdown.data-api touchend.dropdown.data-api', function(e) {
                    e.stopPropagation();
                });
                this.$searchbox.on('input propertychange', function() {
                    if (that.$searchbox.val()) {
                        var $searchBase = that.$lis.not('.is-hidden').removeClass('hidden').children('a');
                        if (that.options.liveSearchNormalize) {
                            $searchBase = $searchBase.not(':a' + that._searchStyle() + '("' + normalizeToBase(that.$searchbox.val()) + '")');
                        } else {
                            $searchBase = $searchBase.not(':' + that._searchStyle() + '("' + that.$searchbox.val() + '")');
                        }
                        $searchBase.parent().addClass('hidden');
                        that.$lis.filter('.dropdown-header').each(function() {
                            var $this = $(this),
                                optgroup = $this.data('optgroup');
                            if (that.$lis.filter('[data-optgroup=' + optgroup + ']').not($this).not('.hidden').length === 0) {
                                $this.addClass('hidden');
                                that.$lis.filter('[data-optgroup=' + optgroup + 'div]').addClass('hidden');
                            }
                        });
                        var $lisVisible = that.$lis.not('.hidden');
                        $lisVisible.each(function(index) {
                            var $this = $(this);
                            if ($this.hasClass('divider') && ($this.index() === $lisVisible.first().index() || $this.index() === $lisVisible.last().index() || $lisVisible.eq(index + 1).hasClass('divider'))) {
                                $this.addClass('hidden');
                            }
                        });
                        if (!that.$lis.not('.hidden, .no-results').length) {
                            if (!!$no_results.parent().length) {
                                $no_results.remove();
                            }
                            $no_results.html(that.options.noneResultsText.replace('{0}', '"' + htmlEscape(that.$searchbox.val()) + '"')).show();
                            that.$menuInner.append($no_results);
                        } else if (!!$no_results.parent().length) {
                            $no_results.remove();
                        }
                    } else {
                        that.$lis.not('.is-hidden').removeClass('hidden');
                        if (!!$no_results.parent().length) {
                            $no_results.remove();
                        }
                    }
                    that.$lis.filter('.active').removeClass('active');
                    if (that.$searchbox.val()) that.$lis.not('.hidden, .divider, .dropdown-header').eq(0).addClass('active').children('a').focus();
                    $(this).focus();
                });
            },
            _searchStyle: function() {
                var styles = {
                    begins: 'ibegins',
                    startsWith: 'ibegins'
                };
                return styles[this.options.liveSearchStyle] || 'icontains';
            },
            val: function(value) {
                if (typeof value !== 'undefined') {
                    this.$element.val(value);
                    this.render();
                    return this.$element;
                } else {
                    return this.$element.val();
                }
            },
            changeAll: function(status) {
                if (typeof status === 'undefined') status = true;
                this.findLis();
                var $options = this.$element.find('option'),
                    $lisVisible = this.$lis.not('.divider, .dropdown-header, .disabled, .hidden'),
                    lisVisLen = $lisVisible.length,
                    selectedOptions = [];
                if (status) {
                    if ($lisVisible.filter('.selected').length === $lisVisible.length) return;
                } else {
                    if ($lisVisible.filter('.selected').length === 0) return;
                }
                $lisVisible.toggleClass('selected', status);
                for (var i = 0; i < lisVisLen; i++) {
                    var origIndex = $lisVisible[i].getAttribute('data-original-index');
                    selectedOptions[selectedOptions.length] = $options.eq(origIndex)[0];
                }
                $(selectedOptions).prop('selected', status);
                this.render(false);
                this.togglePlaceholder();
                this.$element.trigger('changed.bs.select').triggerNative('change');
            },
            selectAll: function() {
                return this.changeAll(true);
            },
            deselectAll: function() {
                return this.changeAll(false);
            },
            toggle: function(e) {
                e = e || window.event;
                if (e) e.stopPropagation();
                this.$button.trigger('click');
            },
            keydown: function(e) {
                var $this = $(this),
                    $parent = $this.is('input') ? $this.parent().parent() : $this.parent(),
                    $items, that = $parent.data('this'),
                    index, next, first, last, prev, nextPrev, prevIndex, isActive, selector = ':not(.disabled, .hidden, .dropdown-header, .divider)',
                    keyCodeMap = {
                        32: ' ',
                        48: '0',
                        49: '1',
                        50: '2',
                        51: '3',
                        52: '4',
                        53: '5',
                        54: '6',
                        55: '7',
                        56: '8',
                        57: '9',
                        59: ';',
                        65: 'a',
                        66: 'b',
                        67: 'c',
                        68: 'd',
                        69: 'e',
                        70: 'f',
                        71: 'g',
                        72: 'h',
                        73: 'i',
                        74: 'j',
                        75: 'k',
                        76: 'l',
                        77: 'm',
                        78: 'n',
                        79: 'o',
                        80: 'p',
                        81: 'q',
                        82: 'r',
                        83: 's',
                        84: 't',
                        85: 'u',
                        86: 'v',
                        87: 'w',
                        88: 'x',
                        89: 'y',
                        90: 'z',
                        96: '0',
                        97: '1',
                        98: '2',
                        99: '3',
                        100: '4',
                        101: '5',
                        102: '6',
                        103: '7',
                        104: '8',
                        105: '9'
                    };
                if (that.options.liveSearch) $parent = $this.parent().parent();
                if (that.options.container) $parent = that.$menu;
                $items = $('[role=menu] li', $parent);
                isActive = that.$newElement.hasClass('open');
                if (!isActive && (e.keyCode >= 48 && e.keyCode <= 57 || e.keyCode >= 96 && e.keyCode <= 105 || e.keyCode >= 65 && e.keyCode <= 90)) {
                    if (!that.options.container) {
                        that.setSize();
                        that.$menu.parent().addClass('open');
                        isActive = true;
                    } else {
                        that.$button.trigger('click');
                    }
                    that.$searchbox.focus();
                    return;
                }
                if (that.options.liveSearch) {
                    if (/(^9$|27)/.test(e.keyCode.toString(10)) && isActive && that.$menu.find('.active').length === 0) {
                        e.preventDefault();
                        that.$menu.parent().removeClass('open');
                        if (that.options.container) that.$newElement.removeClass('open');
                        that.$button.focus();
                    }
                    $items = $('[role=menu] li' + selector, $parent);
                    if (!$this.val() && !/(38|40)/.test(e.keyCode.toString(10))) {
                        if ($items.filter('.active').length === 0) {
                            $items = that.$menuInner.find('li');
                            if (that.options.liveSearchNormalize) {
                                $items = $items.filter(':a' + that._searchStyle() + '(' + normalizeToBase(keyCodeMap[e.keyCode]) + ')');
                            } else {
                                $items = $items.filter(':' + that._searchStyle() + '(' + keyCodeMap[e.keyCode] + ')');
                            }
                        }
                    }
                }
                if (!$items.length) return;
                if (/(38|40)/.test(e.keyCode.toString(10))) {
                    index = $items.index($items.find('a').filter(':focus').parent());
                    first = $items.filter(selector).first().index();
                    last = $items.filter(selector).last().index();
                    next = $items.eq(index).nextAll(selector).eq(0).index();
                    prev = $items.eq(index).prevAll(selector).eq(0).index();
                    nextPrev = $items.eq(next).prevAll(selector).eq(0).index();
                    if (that.options.liveSearch) {
                        $items.each(function(i) {
                            if (!$(this).hasClass('disabled')) {
                                $(this).data('index', i);
                            }
                        });
                        index = $items.index($items.filter('.active'));
                        first = $items.first().data('index');
                        last = $items.last().data('index');
                        next = $items.eq(index).nextAll().eq(0).data('index');
                        prev = $items.eq(index).prevAll().eq(0).data('index');
                        nextPrev = $items.eq(next).prevAll().eq(0).data('index');
                    }
                    prevIndex = $this.data('prevIndex');
                    if (e.keyCode == 38) {
                        if (that.options.liveSearch) index--;
                        if (index != nextPrev && index > prev) index = prev;
                        if (index < first) index = first;
                        if (index == prevIndex) index = last;
                    } else if (e.keyCode == 40) {
                        if (that.options.liveSearch) index++;
                        if (index == -1) index = 0;
                        if (index != nextPrev && index < next) index = next;
                        if (index > last) index = last;
                        if (index == prevIndex) index = first;
                    }
                    $this.data('prevIndex', index);
                    if (!that.options.liveSearch) {
                        $items.eq(index).children('a').focus();
                    } else {
                        e.preventDefault();
                        if (!$this.hasClass('dropdown-toggle')) {
                            $items.removeClass('active').eq(index).addClass('active').children('a').focus();
                            $this.focus();
                        }
                    }
                } else if (!$this.is('input')) {
                    var keyIndex = [],
                        count, prevKey;
                    $items.each(function() {
                        if (!$(this).hasClass('disabled')) {
                            if ($.trim($(this).children('a').text().toLowerCase()).substring(0, 1) == keyCodeMap[e.keyCode]) {
                                keyIndex.push($(this).index());
                            }
                        }
                    });
                    count = $(document).data('keycount');
                    count++;
                    $(document).data('keycount', count);
                    prevKey = $.trim($(':focus').text().toLowerCase()).substring(0, 1);
                    if (prevKey != keyCodeMap[e.keyCode]) {
                        count = 1;
                        $(document).data('keycount', count);
                    } else if (count >= keyIndex.length) {
                        $(document).data('keycount', 0);
                        if (count > keyIndex.length) count = 1;
                    }
                    $items.eq(keyIndex[count - 1]).children('a').focus();
                }
                if ((/(13|32)/.test(e.keyCode.toString(10)) || (/(^9$)/.test(e.keyCode.toString(10)) && that.options.selectOnTab)) && isActive) {
                    if (!/(32)/.test(e.keyCode.toString(10))) e.preventDefault();
                    if (!that.options.liveSearch) {
                        var elem = $(':focus');
                        elem.click();
                        elem.focus();
                        e.preventDefault();
                        $(document).data('spaceSelect', true);
                    } else if (!/(32)/.test(e.keyCode.toString(10))) {
                        that.$menuInner.find('.active a').click();
                        $this.focus();
                    }
                    $(document).data('keycount', 0);
                }
                if ((/(^9$|27)/.test(e.keyCode.toString(10)) && isActive && (that.multiple || that.options.liveSearch)) || (/(27)/.test(e.keyCode.toString(10)) && !isActive)) {
                    that.$menu.parent().removeClass('open');
                    if (that.options.container) that.$newElement.removeClass('open');
                    that.$button.focus();
                }
            },
            mobile: function() {
                this.$element.addClass('mobile-device');
            },
            refresh: function() {
                this.$lis = null;
                this.liObj = {};
                this.reloadLi();
                this.render();
                this.checkDisabled();
                this.liHeight(true);
                this.setStyle();
                this.setWidth();
                if (this.$lis) this.$searchbox.trigger('propertychange');
                this.$element.trigger('refreshed.bs.select');
            },
            hide: function() {
                this.$newElement.hide();
            },
            show: function() {
                this.$newElement.show();
            },
            remove: function() {
                this.$newElement.remove();
                this.$element.remove();
            },
            destroy: function() {
                this.$newElement.before(this.$element).remove();
                if (this.$bsContainer) {
                    this.$bsContainer.remove();
                } else {
                    this.$menu.remove();
                }
                this.$element.off('.bs.select').removeData('selectpicker').removeClass('bs-select-hidden selectpicker');
            }
        };

        function Plugin(option, event) {
            var args = arguments;
            var _option = option,
                _event = event;
            [].shift.apply(args);
            var value;
            var chain = this.each(function() {
                var $this = $(this);
                if ($this.is('select')) {
                    var data = $this.data('selectpicker'),
                        options = typeof _option == 'object' && _option;
                    if (!data) {
                        var config = $.extend({}, Selectpicker.DEFAULTS, $.fn.selectpicker.defaults || {}, $this.data(), options);
                        config.template = $.extend({}, Selectpicker.DEFAULTS.template, ($.fn.selectpicker.defaults ? $.fn.selectpicker.defaults.template : {}), $this.data().template, options.template);
                        $this.data('selectpicker', (data = new Selectpicker(this, config, _event)));
                    } else if (options) {
                        for (var i in options) {
                            if (options.hasOwnProperty(i)) {
                                data.options[i] = options[i];
                            }
                        }
                    }
                    if (typeof _option == 'string') {
                        if (data[_option] instanceof Function) {
                            value = data[_option].apply(data, args);
                        } else {
                            value = data.options[_option];
                        }
                    }
                }
            });
            if (typeof value !== 'undefined') {
                return value;
            } else {
                return chain;
            }
        }
        var old = $.fn.selectpicker;
        $.fn.selectpicker = Plugin;
        $.fn.selectpicker.Constructor = Selectpicker;
        $.fn.selectpicker.noConflict = function() {
            $.fn.selectpicker = old;
            return this;
        };
        $(document).data('keycount', 0).on('keydown.bs.select', '.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="menu"], .bs-searchbox input', Selectpicker.prototype.keydown).on('focusin.modal', '.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="menu"], .bs-searchbox input', function(e) {
            e.stopPropagation();
        });
        $(window).on('load.bs.select.data-api', function() {
            $('.selectpicker').each(function() {
                var $selectpicker = $(this);
                Plugin.call($selectpicker, $selectpicker.data());
            })
        });
    })(jQuery);
}));
! function(n) {
    var t = function(t, i) {
        var t = n(t),
            e = this,
            a = 'li:has([data-toggle="tab"])',
            o = [],
            r = n.extend({}, n.fn.bootstrapWizard.defaults, i),
            s = null,
            l = null;
        this.rebindClick = function(n, t) {
            n.unbind("click", t).bind("click", t)
        }, this.fixNavigationButtons = function() {
            return s.length || (l.find("a:first").tab("show"), s = l.find(a + ":first")), n(r.previousSelector, t).toggleClass("disabled", e.firstIndex() >= e.currentIndex()), n(r.nextSelector, t).toggleClass("disabled", e.currentIndex() >= e.navigationLength()), n(r.nextSelector, t).toggleClass("hidden", e.currentIndex() >= e.navigationLength() && n(r.finishSelector, t).length > 0), n(r.lastSelector, t).toggleClass("hidden", e.currentIndex() >= e.navigationLength() && n(r.finishSelector, t).length > 0), n(r.finishSelector, t).toggleClass("hidden", e.currentIndex() < e.navigationLength()), n(r.backSelector, t).toggleClass("disabled", 0 == o.length), n(r.backSelector, t).toggleClass("hidden", e.currentIndex() >= e.navigationLength() && n(r.finishSelector, t).length > 0), e.rebindClick(n(r.nextSelector, t), e.next), e.rebindClick(n(r.previousSelector, t), e.previous), e.rebindClick(n(r.lastSelector, t), e.last), e.rebindClick(n(r.firstSelector, t), e.first), e.rebindClick(n(r.finishSelector, t), e.finish), e.rebindClick(n(r.backSelector, t), e.back), r.onTabShow && "function" == typeof r.onTabShow && r.onTabShow(s, l, e.currentIndex()) === !1 ? !1 : void 0
        }, this.next = function(n) {
            if (t.hasClass("last")) return !1;
            if (r.onNext && "function" == typeof r.onNext && r.onNext(s, l, e.nextIndex()) === !1) return !1;
            var i = e.currentIndex(),
                d = e.nextIndex();
            d > e.navigationLength() || (o.push(i), l.find(a + (r.withVisible ? ":visible" : "") + ":eq(" + d + ") a").tab("show"))
        }, this.previous = function(n) {
            if (t.hasClass("first")) return !1;
            if (r.onPrevious && "function" == typeof r.onPrevious && r.onPrevious(s, l, e.previousIndex()) === !1) return !1;
            var i = e.currentIndex(),
                d = e.previousIndex();
            0 > d || (o.push(i), l.find(a + (r.withVisible ? ":visible" : "") + ":eq(" + d + ") a").tab("show"))
        }, this.first = function(n) {
            return r.onFirst && "function" == typeof r.onFirst && r.onFirst(s, l, e.firstIndex()) === !1 ? !1 : t.hasClass("disabled") ? !1 : (o.push(e.currentIndex()), void l.find(a + ":eq(0) a").tab("show"))
        }, this.last = function(n) {
            return r.onLast && "function" == typeof r.onLast && r.onLast(s, l, e.lastIndex()) === !1 ? !1 : t.hasClass("disabled") ? !1 : (o.push(e.currentIndex()), void l.find(a + ":eq(" + e.navigationLength() + ") a").tab("show"))
        }, this.finish = function(n) {
            r.onFinish && "function" == typeof r.onFinish && r.onFinish(s, l, e.lastIndex())
        }, this.back = function() {
            if (0 == o.length) return null;
            var n = o.pop();
            return r.onBack && "function" == typeof r.onBack && r.onBack(s, l, n) === !1 ? (o.push(n), !1) : void t.find(a + ":eq(" + n + ") a").tab("show")
        }, this.currentIndex = function() {
            return l.find(a).index(s)
        }, this.firstIndex = function() {
            return 0
        }, this.lastIndex = function() {
            return e.navigationLength()
        }, this.getIndex = function(n) {
            return l.find(a).index(n)
        }, this.nextIndex = function() {
            var n = this.currentIndex(),
                t = null;
            do n++, t = l.find(a + ":eq(" + n + ")"); while (t && t.hasClass("disabled"));
            return n
        }, this.previousIndex = function() {
            var n = this.currentIndex(),
                t = null;
            do n--, t = l.find(a + ":eq(" + n + ")"); while (t && t.hasClass("disabled"));
            return n
        }, this.navigationLength = function() {
            return l.find(a).length - 1
        }, this.activeTab = function() {
            return s
        }, this.nextTab = function() {
            return l.find(a + ":eq(" + (e.currentIndex() + 1) + ")").length ? l.find(a + ":eq(" + (e.currentIndex() + 1) + ")") : null
        }, this.previousTab = function() {
            return e.currentIndex() <= 0 ? null : l.find(a + ":eq(" + parseInt(e.currentIndex() - 1) + ")")
        }, this.show = function(n) {
            var i = isNaN(n) ? t.find(a + ' a[href="#' + n + '"]') : t.find(a + ":eq(" + n + ") a");
            i.length > 0 && (o.push(e.currentIndex()), i.tab("show"))
        }, this.disable = function(n) {
            l.find(a + ":eq(" + n + ")").addClass("disabled")
        }, this.enable = function(n) {
            l.find(a + ":eq(" + n + ")").removeClass("disabled")
        }, this.hide = function(n) {
            l.find(a + ":eq(" + n + ")").hide()
        }, this.display = function(n) {
            l.find(a + ":eq(" + n + ")").show()
        }, this.remove = function(t) {
            var i = t[0],
                e = "undefined" != typeof t[1] ? t[1] : !1,
                o = l.find(a + ":eq(" + i + ")");
            if (e) {
                var r = o.find("a").attr("href");
                n(r).remove()
            }
            o.remove()
        };
        var d = function(t) {
                var i = l.find(a),
                    o = i.index(n(t.currentTarget).parent(a)),
                    d = n(i[o]);
                return r.onTabClick && "function" == typeof r.onTabClick && r.onTabClick(s, l, e.currentIndex(), o, d) === !1 ? !1 : void 0
            },
            u = function(t) {
                var i = n(t.target).parent(),
                    o = l.find(a).index(i);
                return i.hasClass("disabled") ? !1 : r.onTabChange && "function" == typeof r.onTabChange && r.onTabChange(s, l, e.currentIndex(), o) === !1 ? !1 : (s = i, void e.fixNavigationButtons())
            };
        this.resetWizard = function() {
            n('a[data-toggle="tab"]', l).off("click", d), n('a[data-toggle="tab"]', l).off("show show.bs.tab", u), l = t.find("ul:first", t), s = l.find(a + ".active", t), n('a[data-toggle="tab"]', l).on("click", d), n('a[data-toggle="tab"]', l).on("show show.bs.tab", u), e.fixNavigationButtons()
        }, l = t.find("ul:first", t), s = l.find(a + ".active", t), l.hasClass(r.tabClass) || l.addClass(r.tabClass), r.onInit && "function" == typeof r.onInit && r.onInit(s, l, 0), r.onShow && "function" == typeof r.onShow && r.onShow(s, l, e.nextIndex()), n('a[data-toggle="tab"]', l).on("click", d), n('a[data-toggle="tab"]', l).on("show show.bs.tab", u)
    };
    n.fn.bootstrapWizard = function(i) {
        if ("string" == typeof i) {
            var e = Array.prototype.slice.call(arguments, 1);
            return 1 === e.length && e.toString(), this.data("bootstrapWizard")[i](e)
        }
        return this.each(function(e) {
            var a = n(this);
            if (!a.data("bootstrapWizard")) {
                var o = new t(a, i);
                a.data("bootstrapWizard", o), o.fixNavigationButtons()
            }
        })
    }, n.fn.bootstrapWizard.defaults = {
        withVisible: !0,
        tabClass: "nav nav-pills",
        nextSelector: ".wizard li.next",
        previousSelector: ".wizard li.previous",
        firstSelector: ".wizard li.first",
        lastSelector: ".wizard li.last",
        finishSelector: ".wizard li.finish",
        backSelector: ".wizard li.back",
        onShow: null,
        onInit: null,
        onNext: null,
        onPrevious: null,
        onLast: null,
        onFirst: null,
        onFinish: null,
        onBack: null,
        onTabChange: null,
        onTabClick: null,
        onTabShow: null
    }
}(jQuery);
! function(e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
}(function(e) {
    "use strict";
    var t = "drawer",
        n = "undefined" != typeof document.ontouchstart,
        s = {
            init: function(n) {
                return n = e.extend({
                    iscroll: {
                        mouseWheel: !0,
                        preventDefault: !1
                    },
                    showOverlay: !0
                }, n), s.settings = {
                    state: !1,
                    "class": {
                        nav: "drawer-nav",
                        toggle: "drawer-toggle",
                        overlay: "drawer-overlay",
                        open: "drawer-open",
                        close: "drawer-close",
                        dropdown: "drawer-dropdown"
                    },
                    events: {
                        opened: "drawer.opened",
                        closed: "drawer.closed"
                    },
                    dropdownEvents: {
                        opened: "shown.bs.dropdown",
                        closed: "hidden.bs.dropdown"
                    }
                }, this.each(function() {
                    var o = this,
                        r = e(this),
                        a = r.data(t);
                    if (!a) {
                        n = e.extend({}, n), r.data(t, {
                            options: n
                        });
                        var i = new IScroll("." + s.settings["class"].nav, n.iscroll);
                        n.showOverlay && s.addOverlay.call(o), e("." + s.settings["class"].toggle).on("click." + t, function() {
                            return s.toggle.call(o), i.refresh()
                        }), e(window).resize(function() {
                            return s.close.call(o), i.refresh()
                        }), e("." + s.settings["class"].dropdown).on(s.settings.dropdownEvents.opened + " " + s.settings.dropdownEvents.closed, function() {
                            return i.refresh()
                        })
                    }
                })
            },
            addOverlay: function() {
                var t = e(this),
                    n = e("<div>").addClass(s.settings["class"].overlay + " " + s.settings["class"].toggle);
                return t.append(n)
            },
            toggle: function() {
                var e = this;
                return s.settings.state ? s.close.call(e) : s.open.call(e)
            },
            open: function(o) {
                var r = e(this);
                return o = r.data(t).options, n && r.on("touchmove." + t, function(e) {
                    e.preventDefault()
                }), r.removeClass(s.settings["class"].close).addClass(s.settings["class"].open).css({
                    overflow: "hidden"
                }).drawerCallback(function() {
                    s.settings.state = !0, r.trigger(s.settings.events.opened)
                })
            },
            close: function(o) {
                var r = e(this);
                return o = r.data(t).options, n && r.off("touchmove." + t), r.removeClass(s.settings["class"].open).addClass(s.settings["class"].close).css({
                    overflow: "auto"
                }).drawerCallback(function() {
                    s.settings.state = !1, r.trigger(s.settings.events.closed)
                })
            },
            destroy: function() {
                return this.each(function() {
                    var n = e(this);
                    e(window).off("." + t), n.removeData(t)
                })
            }
        };
    e.fn.drawerCallback = function(t) {
        var n = "transitionend webkitTransitionEnd";
        return this.each(function() {
            var s = e(this);
            s.on(n, function() {
                return s.off(n), t.call(this)
            })
        })
    }, e.fn.drawer = function(n) {
        return s[n] ? s[n].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof n && n ? void e.error("Method " + n + " does not exist on jQuery." + t) : s.init.apply(this, arguments)
    }
});
(function(window, undefined) {
    var S = {
        version: "3.0.3"
    };
    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf("windows") > -1 || ua.indexOf("win32") > -1) {
        S.isWindows = true
    } else {
        if (ua.indexOf("macintosh") > -1 || ua.indexOf("mac os x") > -1) {
            S.isMac = true
        } else {
            if (ua.indexOf("linux") > -1) {
                S.isLinux = true
            }
        }
    }
    S.isIE = ua.indexOf("msie") > -1;
    S.isIE6 = ua.indexOf("msie 6") > -1;
    S.isIE7 = ua.indexOf("msie 7") > -1;
    S.isGecko = ua.indexOf("gecko") > -1 && ua.indexOf("safari") == -1;
    S.isWebKit = ua.indexOf("applewebkit/") > -1;
    var inlineId = /#(.+)$/,
        galleryName = /^(light|shadow)box\[(.*?)\]/i,
        inlineParam = /\s*([a-z_]*?)\s*=\s*(.+)\s*/,
        fileExtension = /[0-9a-z]+$/i,
        scriptPath = /(.+\/)shadowbox\.js/i;
    var open = false,
        initialized = false,
        lastOptions = {},
        slideDelay = 0,
        slideStart, slideTimer;
    S.current = -1;
    S.dimensions = null;
    S.ease = function(state) {
        return 1 + Math.pow(state - 1, 3)
    };
    S.errorInfo = {
        fla: {
            name: "Flash",
            url: "http://www.adobe.com/products/flashplayer/"
        },
        qt: {
            name: "QuickTime",
            url: "http://www.apple.com/quicktime/download/"
        },
        wmp: {
            name: "Windows Media Player",
            url: "http://www.microsoft.com/windows/windowsmedia/"
        },
        f4m: {
            name: "Flip4Mac",
            url: "http://www.flip4mac.com/wmv_download.htm"
        }
    };
    S.gallery = [];
    S.onReady = noop;
    S.path = null;
    S.player = null;
    S.playerId = "sb-player";
    S.options = {
        animate: true,
        animateFade: true,
        autoplayMovies: true,
        continuous: false,
        enableKeys: true,
        flashParams: {
            bgcolor: "#000000",
            allowfullscreen: true
        },
        flashVars: {},
        flashVersion: "9.0.115",
        handleOversize: "resize",
        handleUnsupported: "link",
        onChange: noop,
        onClose: noop,
        onFinish: noop,
        onOpen: noop,
        showMovieControls: true,
        skipSetup: false,
        slideshowDelay: 0,
        viewportPadding: 20
    };
    S.getCurrent = function() {
        return S.current > -1 ? S.gallery[S.current] : null
    };
    S.hasNext = function() {
        return S.gallery.length > 1 && (S.current != S.gallery.length - 1 || S.options.continuous)
    };
    S.isOpen = function() {
        return open
    };
    S.isPaused = function() {
        return slideTimer == "pause"
    };
    S.applyOptions = function(options) {
        lastOptions = apply({}, S.options);
        apply(S.options, options)
    };
    S.revertOptions = function() {
        apply(S.options, lastOptions)
    };
    S.init = function(options, callback) {
        if (initialized) {
            return
        }
        initialized = true;
        if (S.skin.options) {
            apply(S.options, S.skin.options)
        }
        if (options) {
            apply(S.options, options)
        }
        if (!S.path) {
            var path, scripts = document.getElementsByTagName("script");
            for (var i = 0, len = scripts.length; i < len; ++i) {
                path = scriptPath.exec(scripts[i].src);
                if (path) {
                    S.path = path[1];
                    break
                }
            }
        }
        if (callback) {
            S.onReady = callback
        }
        bindLoad()
    };
    S.open = function(obj) {
        if (open) {
            return
        }
        var gc = S.makeGallery(obj);
        S.gallery = gc[0];
        S.current = gc[1];
        obj = S.getCurrent();
        if (obj == null) {
            return
        }
        S.applyOptions(obj.options || {});
        filterGallery();
        if (S.gallery.length) {
            obj = S.getCurrent();
            if (S.options.onOpen(obj) === false) {
                return
            }
            open = true;
            S.skin.onOpen(obj, load)
        }
    };
    S.close = function() {
        if (!open) {
            return
        }
        open = false;
        if (S.player) {
            S.player.remove();
            S.player = null
        }
        if (typeof slideTimer == "number") {
            clearTimeout(slideTimer);
            slideTimer = null
        }
        slideDelay = 0;
        listenKeys(false);
        S.options.onClose(S.getCurrent());
        S.skin.onClose();
        S.revertOptions()
    };
    S.play = function() {
        if (!S.hasNext()) {
            return
        }
        if (!slideDelay) {
            slideDelay = S.options.slideshowDelay * 1000
        }
        if (slideDelay) {
            slideStart = now();
            slideTimer = setTimeout(function() {
                slideDelay = slideStart = 0;
                S.next()
            }, slideDelay);
            if (S.skin.onPlay) {
                S.skin.onPlay()
            }
        }
    };
    S.pause = function() {
        if (typeof slideTimer != "number") {
            return
        }
        slideDelay = Math.max(0, slideDelay - (now() - slideStart));
        if (slideDelay) {
            clearTimeout(slideTimer);
            slideTimer = "pause";
            if (S.skin.onPause) {
                S.skin.onPause()
            }
        }
    };
    S.change = function(index) {
        if (!(index in S.gallery)) {
            if (S.options.continuous) {
                index = (index < 0 ? S.gallery.length + index : 0);
                if (!(index in S.gallery)) {
                    return
                }
            } else {
                return
            }
        }
        S.current = index;
        if (typeof slideTimer == "number") {
            clearTimeout(slideTimer);
            slideTimer = null;
            slideDelay = slideStart = 0
        }
        S.options.onChange(S.getCurrent());
        load(true)
    };
    S.next = function() {
        S.change(S.current + 1)
    };
    S.previous = function() {
        S.change(S.current - 1)
    };
    S.setDimensions = function(height, width, maxHeight, maxWidth, topBottom, leftRight, padding, preserveAspect) {
        var originalHeight = height,
            originalWidth = width;
        var extraHeight = 2 * padding + topBottom;
        if (height + extraHeight > maxHeight) {
            height = maxHeight - extraHeight
        }
        var extraWidth = 2 * padding + leftRight;
        if (width + extraWidth > maxWidth) {
            width = maxWidth - extraWidth
        }
        var changeHeight = (originalHeight - height) / originalHeight,
            changeWidth = (originalWidth - width) / originalWidth,
            oversized = (changeHeight > 0 || changeWidth > 0);
        if (preserveAspect && oversized) {
            if (changeHeight > changeWidth) {
                width = Math.round((originalWidth / originalHeight) * height)
            } else {
                if (changeWidth > changeHeight) {
                    height = Math.round((originalHeight / originalWidth) * width)
                }
            }
        }
        S.dimensions = {
            height: height + topBottom,
            width: width + leftRight,
            innerHeight: height,
            innerWidth: width,
            top: Math.floor((maxHeight - (height + extraHeight)) / 2 + padding),
            left: Math.floor((maxWidth - (width + extraWidth)) / 2 + padding),
            oversized: oversized
        };
        return S.dimensions
    };
    S.makeGallery = function(obj) {
        var gallery = [],
            current = -1;
        if (typeof obj == "string") {
            obj = [obj]
        }
        if (typeof obj.length == "number") {
            each(obj, function(i, o) {
                if (o.content) {
                    gallery[i] = o
                } else {
                    gallery[i] = {
                        content: o
                    }
                }
            });
            current = 0
        } else {
            if (obj.tagName) {
                var cacheObj = S.getCache(obj);
                obj = cacheObj ? cacheObj : S.makeObject(obj)
            }
            if (obj.gallery) {
                gallery = [];
                var o;
                for (var key in S.cache) {
                    o = S.cache[key];
                    if (o.gallery && o.gallery == obj.gallery) {
                        if (current == -1 && o.content == obj.content) {
                            current = gallery.length
                        }
                        gallery.push(o)
                    }
                }
                if (current == -1) {
                    gallery.unshift(obj);
                    current = 0
                }
            } else {
                gallery = [obj];
                current = 0
            }
        }
        each(gallery, function(i, o) {
            gallery[i] = apply({}, o)
        });
        return [gallery, current]
    };
    S.makeObject = function(link, options) {
        var obj = {
            content: link.href,
            title: link.getAttribute("title") || "",
            link: link
        };
        if (options) {
            options = apply({}, options);
            each(["player", "title", "height", "width", "gallery"], function(i, o) {
                if (typeof options[o] != "undefined") {
                    obj[o] = options[o];
                    delete options[o]
                }
            });
            obj.options = options
        } else {
            obj.options = {}
        }
        if (!obj.player) {
            obj.player = S.getPlayer(obj.content)
        }
        var rel = link.getAttribute("rel");
        if (rel) {
            var match = rel.match(galleryName);
            if (match) {
                obj.gallery = escape(match[2])
            }
            each(rel.split(";"), function(i, p) {
                match = p.match(inlineParam);
                if (match) {
                    obj[match[1]] = match[2]
                }
            })
        }
        return obj
    };
    S.getPlayer = function(content) {
        if (content.indexOf("#") > -1 && content.indexOf(document.location.href) == 0) {
            return "inline"
        }
        var q = content.indexOf("?");
        if (q > -1) {
            content = content.substring(0, q)
        }
        var ext, m = content.match(fileExtension);
        if (m) {
            ext = m[0].toLowerCase()
        }
        if (ext) {
            if (S.img && S.img.ext.indexOf(ext) > -1) {
                return "img"
            }
            if (S.swf && S.swf.ext.indexOf(ext) > -1) {
                return "swf"
            }
            if (S.flv && S.flv.ext.indexOf(ext) > -1) {
                return "flv"
            }
            if (S.qt && S.qt.ext.indexOf(ext) > -1) {
                if (S.wmp && S.wmp.ext.indexOf(ext) > -1) {
                    return "qtwmp"
                } else {
                    return "qt"
                }
            }
            if (S.wmp && S.wmp.ext.indexOf(ext) > -1) {
                return "wmp"
            }
        }
        return "iframe"
    };

    function filterGallery() {
        var err = S.errorInfo,
            plugins = S.plugins,
            obj, remove, needed, m, format, replace, inlineEl, flashVersion;
        for (var i = 0; i < S.gallery.length; ++i) {
            obj = S.gallery[i];
            remove = false;
            needed = null;
            switch (obj.player) {
                case "flv":
                case "swf":
                    if (!plugins.fla) {
                        needed = "fla"
                    }
                    break;
                case "qt":
                    if (!plugins.qt) {
                        needed = "qt"
                    }
                    break;
                case "wmp":
                    if (S.isMac) {
                        if (plugins.qt && plugins.f4m) {
                            obj.player = "qt"
                        } else {
                            needed = "qtf4m"
                        }
                    } else {
                        if (!plugins.wmp) {
                            needed = "wmp"
                        }
                    }
                    break;
                case "qtwmp":
                    if (plugins.qt) {
                        obj.player = "qt"
                    } else {
                        if (plugins.wmp) {
                            obj.player = "wmp"
                        } else {
                            needed = "qtwmp"
                        }
                    }
                    break
            }
            if (needed) {
                if (S.options.handleUnsupported == "link") {
                    switch (needed) {
                        case "qtf4m":
                            format = "shared";
                            replace = [err.qt.url, err.qt.name, err.f4m.url, err.f4m.name];
                            break;
                        case "qtwmp":
                            format = "either";
                            replace = [err.qt.url, err.qt.name, err.wmp.url, err.wmp.name];
                            break;
                        default:
                            format = "single";
                            replace = [err[needed].url, err[needed].name]
                    }
                    obj.player = "html";
                    obj.content = '<div class="sb-message">' + sprintf(S.lang.errors[format], replace) + "</div>"
                } else {
                    remove = true
                }
            } else {
                if (obj.player == "inline") {
                    m = inlineId.exec(obj.content);
                    if (m) {
                        inlineEl = get(m[1]);
                        if (inlineEl) {
                            obj.content = inlineEl.innerHTML
                        } else {
                            remove = true
                        }
                    } else {
                        remove = true
                    }
                } else {
                    if (obj.player == "swf" || obj.player == "flv") {
                        flashVersion = (obj.options && obj.options.flashVersion) || S.options.flashVersion;
                        if (S.flash && !S.flash.hasFlashPlayerVersion(flashVersion)) {
                            obj.width = 310;
                            obj.height = 177
                        }
                    }
                }
            }
            if (remove) {
                S.gallery.splice(i, 1);
                if (i < S.current) {
                    --S.current
                } else {
                    if (i == S.current) {
                        S.current = i > 0 ? i - 1 : i
                    }
                }--i
            }
        }
    }

    function listenKeys(on) {
        if (!S.options.enableKeys) {
            return
        }(on ? addEvent : removeEvent)(document, "keydown", handleKey)
    }

    function handleKey(e) {
        if (e.metaKey || e.shiftKey || e.altKey || e.ctrlKey) {
            return
        }
        var code = keyCode(e),
            handler;
        switch (code) {
            case 81:
            case 88:
            case 27:
                handler = S.close;
                break;
            case 37:
                handler = S.previous;
                break;
            case 39:
                handler = S.next;
                break;
            case 32:
                handler = typeof slideTimer == "number" ? S.pause : S.play;
                break
        }
        if (handler) {
            preventDefault(e);
            handler()
        }
    }

    function load(changing) {
        listenKeys(false);
        var obj = S.getCurrent();
        var player = (obj.player == "inline" ? "html" : obj.player);
        if (typeof S[player] != "function") {
            throw "unknown player " + player
        }
        if (changing) {
            S.player.remove();
            S.revertOptions();
            S.applyOptions(obj.options || {})
        }
        S.player = new S[player](obj, S.playerId);
        if (S.gallery.length > 1) {
            var next = S.gallery[S.current + 1] || S.gallery[0];
            if (next.player == "img") {
                var a = new Image();
                a.src = next.content
            }
            var prev = S.gallery[S.current - 1] || S.gallery[S.gallery.length - 1];
            if (prev.player == "img") {
                var b = new Image();
                b.src = prev.content
            }
        }
        S.skin.onLoad(changing, waitReady)
    }

    function waitReady() {
        if (!open) {
            return
        }
        if (typeof S.player.ready != "undefined") {
            var timer = setInterval(function() {
                if (open) {
                    if (S.player.ready) {
                        clearInterval(timer);
                        timer = null;
                        S.skin.onReady(show)
                    }
                } else {
                    clearInterval(timer);
                    timer = null
                }
            }, 10)
        } else {
            S.skin.onReady(show)
        }
    }

    function show() {
        if (!open) {
            return
        }
        S.player.append(S.skin.body, S.dimensions);
        S.skin.onShow(finish)
    }

    function finish() {
        if (!open) {
            return
        }
        if (S.player.onLoad) {
            S.player.onLoad()
        }
        S.options.onFinish(S.getCurrent());
        if (!S.isPaused()) {
            S.play()
        }
        listenKeys(true)
    }
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function(obj, from) {
            var len = this.length >>> 0;
            from = from || 0;
            if (from < 0) {
                from += len
            }
            for (; from < len; ++from) {
                if (from in this && this[from] === obj) {
                    return from
                }
            }
            return -1
        }
    }

    function now() {
        return (new Date).getTime()
    }

    function apply(original, extension) {
        for (var property in extension) {
            original[property] = extension[property]
        }
        return original
    }

    function each(obj, callback) {
        var i = 0,
            len = obj.length;
        for (var value = obj[0]; i < len && callback.call(value, i, value) !== false; value = obj[++i]) {}
    }

    function sprintf(str, replace) {
        return str.replace(/\{(\w+?)\}/g, function(match, i) {
            return replace[i]
        })
    }

    function noop() {}

    function get(id) {
        return document.getElementById(id)
    }

    function remove(el) {
        el.parentNode.removeChild(el)
    }
    var supportsOpacity = true,
        supportsFixed = true;

    function checkSupport() {
        var body = document.body,
            div = document.createElement("div");
        supportsOpacity = typeof div.style.opacity === "string";
        div.style.position = "fixed";
        div.style.margin = 0;
        div.style.top = "20px";
        body.appendChild(div, body.firstChild);
        supportsFixed = div.offsetTop == 20;
        body.removeChild(div)
    }
    S.getStyle = (function() {
        var opacity = /opacity=([^)]*)/,
            getComputedStyle = document.defaultView && document.defaultView.getComputedStyle;
        return function(el, style) {
            var ret;
            if (!supportsOpacity && style == "opacity" && el.currentStyle) {
                ret = opacity.test(el.currentStyle.filter || "") ? (parseFloat(RegExp.$1) / 100) + "" : "";
                return ret === "" ? "1" : ret
            }
            if (getComputedStyle) {
                var computedStyle = getComputedStyle(el, null);
                if (computedStyle) {
                    ret = computedStyle[style]
                }
                if (style == "opacity" && ret == "") {
                    ret = "1"
                }
            } else {
                ret = el.currentStyle[style]
            }
            return ret
        }
    })();
    S.appendHTML = function(el, html) {
        if (el.insertAdjacentHTML) {
            el.insertAdjacentHTML("BeforeEnd", html)
        } else {
            if (el.lastChild) {
                var range = el.ownerDocument.createRange();
                range.setStartAfter(el.lastChild);
                var frag = range.createContextualFragment(html);
                el.appendChild(frag)
            } else {
                el.innerHTML = html
            }
        }
    };
    S.getWindowSize = function(dimension) {
        if (document.compatMode === "CSS1Compat") {
            return document.documentElement["client" + dimension]
        }
        return document.body["client" + dimension]
    };
    S.setOpacity = function(el, opacity) {
        var style = el.style;
        if (supportsOpacity) {
            style.opacity = (opacity == 1 ? "" : opacity)
        } else {
            style.zoom = 1;
            if (opacity == 1) {
                if (typeof style.filter == "string" && (/alpha/i).test(style.filter)) {
                    style.filter = style.filter.replace(/\s*[\w\.]*alpha\([^\)]*\);?/gi, "")
                }
            } else {
                style.filter = (style.filter || "").replace(/\s*[\w\.]*alpha\([^\)]*\)/gi, "") + " alpha(opacity=" + (opacity * 100) + ")"
            }
        }
    };
    S.clearOpacity = function(el) {
        S.setOpacity(el, 1)
    };

    function getTarget(e) {
        return e.target
    }

    function getPageXY(e) {
        return [e.pageX, e.pageY]
    }

    function preventDefault(e) {
        e.preventDefault()
    }

    function keyCode(e) {
        return e.keyCode
    }

    function addEvent(el, type, handler) {
        jQuery(el).bind(type, handler)
    }

    function removeEvent(el, type, handler) {
        jQuery(el).unbind(type, handler)
    }
    jQuery.fn.shadowbox = function(options) {
        return this.each(function() {
            var el = jQuery(this);
            var opts = jQuery.extend({}, options || {}, jQuery.metadata ? el.metadata() : jQuery.meta ? el.data() : {});
            var cls = this.className || "";
            opts.width = parseInt((cls.match(/w:(\d+)/) || [])[1]) || opts.width;
            opts.height = parseInt((cls.match(/h:(\d+)/) || [])[1]) || opts.height;
            Shadowbox.setup(el, opts)
        })
    };
    var loaded = false,
        DOMContentLoaded;
    if (document.addEventListener) {
        DOMContentLoaded = function() {
            document.removeEventListener("DOMContentLoaded", DOMContentLoaded, false);
            S.load()
        }
    } else {
        if (document.attachEvent) {
            DOMContentLoaded = function() {
                if (document.readyState === "complete") {
                    document.detachEvent("onreadystatechange", DOMContentLoaded);
                    S.load()
                }
            }
        }
    }

    function doScrollCheck() {
        if (loaded) {
            return
        }
        try {
            document.documentElement.doScroll("left")
        } catch (e) {
            setTimeout(doScrollCheck, 1);
            return
        }
        S.load()
    }

    function bindLoad() {
        if (document.readyState === "complete") {
            return S.load()
        }
        if (document.addEventListener) {
            document.addEventListener("DOMContentLoaded", DOMContentLoaded, false);
            window.addEventListener("load", S.load, false)
        } else {
            if (document.attachEvent) {
                document.attachEvent("onreadystatechange", DOMContentLoaded);
                window.attachEvent("onload", S.load);
                var topLevel = false;
                try {
                    topLevel = window.frameElement === null
                } catch (e) {}
                if (document.documentElement.doScroll && topLevel) {
                    doScrollCheck()
                }
            }
        }
    }
    S.load = function() {
        if (loaded) {
            return
        }
        if (!document.body) {
            return setTimeout(S.load, 13)
        }
        loaded = true;
        checkSupport();
        S.onReady();
        if (!S.options.skipSetup) {
            S.setup()
        }
        S.skin.init()
    };
    S.plugins = {};
    if (navigator.plugins && navigator.plugins.length) {
        var names = [];
        each(navigator.plugins, function(i, p) {
            names.push(p.name)
        });
        names = names.join(",");
        var f4m = names.indexOf("Flip4Mac") > -1;
        S.plugins = {
            fla: names.indexOf("Shockwave Flash") > -1,
            qt: names.indexOf("QuickTime") > -1,
            wmp: !f4m && names.indexOf("Windows Media") > -1,
            f4m: f4m
        }
    } else {
        var detectPlugin = function(name) {
            var axo;
            try {
                axo = new ActiveXObject(name)
            } catch (e) {}
            return !!axo
        };
        S.plugins = {
            fla: detectPlugin("ShockwaveFlash.ShockwaveFlash"),
            qt: detectPlugin("QuickTime.QuickTime"),
            wmp: detectPlugin("wmplayer.ocx"),
            f4m: false
        }
    }
    var relAttr = /^(light|shadow)box/i,
        expando = "shadowboxCacheKey",
        cacheKey = 1;
    S.cache = {};
    S.select = function(selector) {
        var links = [];
        if (!selector) {
            var rel;
            each(document.getElementsByTagName("a"), function(i, el) {
                rel = el.getAttribute("rel");
                if (rel && relAttr.test(rel)) {
                    links.push(el)
                }
            })
        } else {
            var length = selector.length;
            if (length) {
                if (typeof selector == "string") {
                    if (S.find) {
                        links = S.find(selector)
                    }
                } else {
                    if (length == 2 && typeof selector[0] == "string" && selector[1].nodeType) {
                        if (S.find) {
                            links = S.find(selector[0], selector[1])
                        }
                    } else {
                        for (var i = 0; i < length; ++i) {
                            links[i] = selector[i]
                        }
                    }
                }
            } else {
                links.push(selector)
            }
        }
        return links
    };
    S.setup = function(selector, options) {
        each(S.select(selector), function(i, link) {
            S.addCache(link, options)
        })
    };
    S.teardown = function(selector) {
        each(S.select(selector), function(i, link) {
            S.removeCache(link)
        })
    };
    S.addCache = function(link, options) {
        var key = link[expando];
        if (key == undefined) {
            key = cacheKey++;
            link[expando] = key;
            addEvent(link, "click", handleClick)
        }
        S.cache[key] = S.makeObject(link, options)
    };
    S.removeCache = function(link) {
        removeEvent(link, "click", handleClick);
        delete S.cache[link[expando]];
        link[expando] = null
    };
    S.getCache = function(link) {
        var key = link[expando];
        return (key in S.cache && S.cache[key])
    };
    S.clearCache = function() {
        for (var key in S.cache) {
            S.removeCache(S.cache[key].link)
        }
        S.cache = {}
    };

    function handleClick(e) {
        S.open(this);
        if (S.gallery.length) {
            preventDefault(e)
        }
    }
    S.find = (function() {
        var chunker = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^[\]]*\]|['"][^'"]*['"]|[^[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,
            done = 0,
            toString = Object.prototype.toString,
            hasDuplicate = false,
            baseHasDuplicate = true;
        [0, 0].sort(function() {
            baseHasDuplicate = false;
            return 0
        });
        var Sizzle = function(selector, context, results, seed) {
            results = results || [];
            var origContext = context = context || document;
            if (context.nodeType !== 1 && context.nodeType !== 9) {
                return []
            }
            if (!selector || typeof selector !== "string") {
                return results
            }
            var parts = [],
                m, set, checkSet, extra, prune = true,
                contextXML = isXML(context),
                soFar = selector;
            while ((chunker.exec(""), m = chunker.exec(soFar)) !== null) {
                soFar = m[3];
                parts.push(m[1]);
                if (m[2]) {
                    extra = m[3];
                    break
                }
            }
            if (parts.length > 1 && origPOS.exec(selector)) {
                if (parts.length === 2 && Expr.relative[parts[0]]) {
                    set = posProcess(parts[0] + parts[1], context)
                } else {
                    set = Expr.relative[parts[0]] ? [context] : Sizzle(parts.shift(), context);
                    while (parts.length) {
                        selector = parts.shift();
                        if (Expr.relative[selector]) {
                            selector += parts.shift()
                        }
                        set = posProcess(selector, set)
                    }
                }
            } else {
                if (!seed && parts.length > 1 && context.nodeType === 9 && !contextXML && Expr.match.ID.test(parts[0]) && !Expr.match.ID.test(parts[parts.length - 1])) {
                    var ret = Sizzle.find(parts.shift(), context, contextXML);
                    context = ret.expr ? Sizzle.filter(ret.expr, ret.set)[0] : ret.set[0]
                }
                if (context) {
                    var ret = seed ? {
                        expr: parts.pop(),
                        set: makeArray(seed)
                    } : Sizzle.find(parts.pop(), parts.length === 1 && (parts[0] === "~" || parts[0] === "+") && context.parentNode ? context.parentNode : context, contextXML);
                    set = ret.expr ? Sizzle.filter(ret.expr, ret.set) : ret.set;
                    if (parts.length > 0) {
                        checkSet = makeArray(set)
                    } else {
                        prune = false
                    }
                    while (parts.length) {
                        var cur = parts.pop(),
                            pop = cur;
                        if (!Expr.relative[cur]) {
                            cur = ""
                        } else {
                            pop = parts.pop()
                        }
                        if (pop == null) {
                            pop = context
                        }
                        Expr.relative[cur](checkSet, pop, contextXML)
                    }
                } else {
                    checkSet = parts = []
                }
            }
            if (!checkSet) {
                checkSet = set
            }
            if (!checkSet) {
                throw "Syntax error, unrecognized expression: " + (cur || selector)
            }
            if (toString.call(checkSet) === "[object Array]") {
                if (!prune) {
                    results.push.apply(results, checkSet)
                } else {
                    if (context && context.nodeType === 1) {
                        for (var i = 0; checkSet[i] != null; i++) {
                            if (checkSet[i] && (checkSet[i] === true || checkSet[i].nodeType === 1 && contains(context, checkSet[i]))) {
                                results.push(set[i])
                            }
                        }
                    } else {
                        for (var i = 0; checkSet[i] != null; i++) {
                            if (checkSet[i] && checkSet[i].nodeType === 1) {
                                results.push(set[i])
                            }
                        }
                    }
                }
            } else {
                makeArray(checkSet, results)
            }
            if (extra) {
                Sizzle(extra, origContext, results, seed);
                Sizzle.uniqueSort(results)
            }
            return results
        };
        Sizzle.uniqueSort = function(results) {
            if (sortOrder) {
                hasDuplicate = baseHasDuplicate;
                results.sort(sortOrder);
                if (hasDuplicate) {
                    for (var i = 1; i < results.length; i++) {
                        if (results[i] === results[i - 1]) {
                            results.splice(i--, 1)
                        }
                    }
                }
            }
            return results
        };
        Sizzle.matches = function(expr, set) {
            return Sizzle(expr, null, null, set)
        };
        Sizzle.find = function(expr, context, isXML) {
            var set, match;
            if (!expr) {
                return []
            }
            for (var i = 0, l = Expr.order.length; i < l; i++) {
                var type = Expr.order[i],
                    match;
                if ((match = Expr.leftMatch[type].exec(expr))) {
                    var left = match[1];
                    match.splice(1, 1);
                    if (left.substr(left.length - 1) !== "\\") {
                        match[1] = (match[1] || "").replace(/\\/g, "");
                        set = Expr.find[type](match, context, isXML);
                        if (set != null) {
                            expr = expr.replace(Expr.match[type], "");
                            break
                        }
                    }
                }
            }
            if (!set) {
                set = context.getElementsByTagName("*")
            }
            return {
                set: set,
                expr: expr
            }
        };
        Sizzle.filter = function(expr, set, inplace, not) {
            var old = expr,
                result = [],
                curLoop = set,
                match, anyFound, isXMLFilter = set && set[0] && isXML(set[0]);
            while (expr && set.length) {
                for (var type in Expr.filter) {
                    if ((match = Expr.match[type].exec(expr)) != null) {
                        var filter = Expr.filter[type],
                            found, item;
                        anyFound = false;
                        if (curLoop === result) {
                            result = []
                        }
                        if (Expr.preFilter[type]) {
                            match = Expr.preFilter[type](match, curLoop, inplace, result, not, isXMLFilter);
                            if (!match) {
                                anyFound = found = true
                            } else {
                                if (match === true) {
                                    continue
                                }
                            }
                        }
                        if (match) {
                            for (var i = 0;
                                (item = curLoop[i]) != null; i++) {
                                if (item) {
                                    found = filter(item, match, i, curLoop);
                                    var pass = not ^ !!found;
                                    if (inplace && found != null) {
                                        if (pass) {
                                            anyFound = true
                                        } else {
                                            curLoop[i] = false
                                        }
                                    } else {
                                        if (pass) {
                                            result.push(item);
                                            anyFound = true
                                        }
                                    }
                                }
                            }
                        }
                        if (found !== undefined) {
                            if (!inplace) {
                                curLoop = result
                            }
                            expr = expr.replace(Expr.match[type], "");
                            if (!anyFound) {
                                return []
                            }
                            break
                        }
                    }
                }
                if (expr === old) {
                    if (anyFound == null) {
                        throw "Syntax error, unrecognized expression: " + expr
                    } else {
                        break
                    }
                }
                old = expr
            }
            return curLoop
        };
        var Expr = Sizzle.selectors = {
            order: ["ID", "NAME", "TAG"],
            match: {
                ID: /#((?:[\w\u00c0-\uFFFF-]|\\.)+)/,
                CLASS: /\.((?:[\w\u00c0-\uFFFF-]|\\.)+)/,
                NAME: /\[name=['"]*((?:[\w\u00c0-\uFFFF-]|\\.)+)['"]*\]/,
                ATTR: /\[\s*((?:[\w\u00c0-\uFFFF-]|\\.)+)\s*(?:(\S?=)\s*(['"]*)(.*?)\3|)\s*\]/,
                TAG: /^((?:[\w\u00c0-\uFFFF\*-]|\\.)+)/,
                CHILD: /:(only|nth|last|first)-child(?:\((even|odd|[\dn+-]*)\))?/,
                POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^-]|$)/,
                PSEUDO: /:((?:[\w\u00c0-\uFFFF-]|\\.)+)(?:\((['"]*)((?:\([^\)]+\)|[^\2\(\)]*)+)\2\))?/
            },
            leftMatch: {},
            attrMap: {
                "class": "className",
                "for": "htmlFor"
            },
            attrHandle: {
                href: function(elem) {
                    return elem.getAttribute("href")
                }
            },
            relative: {
                "+": function(checkSet, part) {
                    var isPartStr = typeof part === "string",
                        isTag = isPartStr && !/\W/.test(part),
                        isPartStrNotTag = isPartStr && !isTag;
                    if (isTag) {
                        part = part.toLowerCase()
                    }
                    for (var i = 0, l = checkSet.length, elem; i < l; i++) {
                        if ((elem = checkSet[i])) {
                            while ((elem = elem.previousSibling) && elem.nodeType !== 1) {}
                            checkSet[i] = isPartStrNotTag || elem && elem.nodeName.toLowerCase() === part ? elem || false : elem === part
                        }
                    }
                    if (isPartStrNotTag) {
                        Sizzle.filter(part, checkSet, true)
                    }
                },
                ">": function(checkSet, part) {
                    var isPartStr = typeof part === "string";
                    if (isPartStr && !/\W/.test(part)) {
                        part = part.toLowerCase();
                        for (var i = 0, l = checkSet.length; i < l; i++) {
                            var elem = checkSet[i];
                            if (elem) {
                                var parent = elem.parentNode;
                                checkSet[i] = parent.nodeName.toLowerCase() === part ? parent : false
                            }
                        }
                    } else {
                        for (var i = 0, l = checkSet.length; i < l; i++) {
                            var elem = checkSet[i];
                            if (elem) {
                                checkSet[i] = isPartStr ? elem.parentNode : elem.parentNode === part
                            }
                        }
                        if (isPartStr) {
                            Sizzle.filter(part, checkSet, true)
                        }
                    }
                },
                "": function(checkSet, part, isXML) {
                    var doneName = done++,
                        checkFn = dirCheck;
                    if (typeof part === "string" && !/\W/.test(part)) {
                        var nodeCheck = part = part.toLowerCase();
                        checkFn = dirNodeCheck
                    }
                    checkFn("parentNode", part, doneName, checkSet, nodeCheck, isXML)
                },
                "~": function(checkSet, part, isXML) {
                    var doneName = done++,
                        checkFn = dirCheck;
                    if (typeof part === "string" && !/\W/.test(part)) {
                        var nodeCheck = part = part.toLowerCase();
                        checkFn = dirNodeCheck
                    }
                    checkFn("previousSibling", part, doneName, checkSet, nodeCheck, isXML)
                }
            },
            find: {
                ID: function(match, context, isXML) {
                    if (typeof context.getElementById !== "undefined" && !isXML) {
                        var m = context.getElementById(match[1]);
                        return m ? [m] : []
                    }
                },
                NAME: function(match, context) {
                    if (typeof context.getElementsByName !== "undefined") {
                        var ret = [],
                            results = context.getElementsByName(match[1]);
                        for (var i = 0, l = results.length; i < l; i++) {
                            if (results[i].getAttribute("name") === match[1]) {
                                ret.push(results[i])
                            }
                        }
                        return ret.length === 0 ? null : ret
                    }
                },
                TAG: function(match, context) {
                    return context.getElementsByTagName(match[1])
                }
            },
            preFilter: {
                CLASS: function(match, curLoop, inplace, result, not, isXML) {
                    match = " " + match[1].replace(/\\/g, "") + " ";
                    if (isXML) {
                        return match
                    }
                    for (var i = 0, elem;
                        (elem = curLoop[i]) != null; i++) {
                        if (elem) {
                            if (not ^ (elem.className && (" " + elem.className + " ").replace(/[\t\n]/g, " ").indexOf(match) >= 0)) {
                                if (!inplace) {
                                    result.push(elem)
                                }
                            } else {
                                if (inplace) {
                                    curLoop[i] = false
                                }
                            }
                        }
                    }
                    return false
                },
                ID: function(match) {
                    return match[1].replace(/\\/g, "")
                },
                TAG: function(match, curLoop) {
                    return match[1].toLowerCase()
                },
                CHILD: function(match) {
                    if (match[1] === "nth") {
                        var test = /(-?)(\d*)n((?:\+|-)?\d*)/.exec(match[2] === "even" && "2n" || match[2] === "odd" && "2n+1" || !/\D/.test(match[2]) && "0n+" + match[2] || match[2]);
                        match[2] = (test[1] + (test[2] || 1)) - 0;
                        match[3] = test[3] - 0
                    }
                    match[0] = done++;
                    return match
                },
                ATTR: function(match, curLoop, inplace, result, not, isXML) {
                    var name = match[1].replace(/\\/g, "");
                    if (!isXML && Expr.attrMap[name]) {
                        match[1] = Expr.attrMap[name]
                    }
                    if (match[2] === "~=") {
                        match[4] = " " + match[4] + " "
                    }
                    return match
                },
                PSEUDO: function(match, curLoop, inplace, result, not) {
                    if (match[1] === "not") {
                        if ((chunker.exec(match[3]) || "").length > 1 || /^\w/.test(match[3])) {
                            match[3] = Sizzle(match[3], null, null, curLoop)
                        } else {
                            var ret = Sizzle.filter(match[3], curLoop, inplace, true ^ not);
                            if (!inplace) {
                                result.push.apply(result, ret)
                            }
                            return false
                        }
                    } else {
                        if (Expr.match.POS.test(match[0]) || Expr.match.CHILD.test(match[0])) {
                            return true
                        }
                    }
                    return match
                },
                POS: function(match) {
                    match.unshift(true);
                    return match
                }
            },
            filters: {
                enabled: function(elem) {
                    return elem.disabled === false && elem.type !== "hidden"
                },
                disabled: function(elem) {
                    return elem.disabled === true
                },
                checked: function(elem) {
                    return elem.checked === true
                },
                selected: function(elem) {
                    elem.parentNode.selectedIndex;
                    return elem.selected === true
                },
                parent: function(elem) {
                    return !!elem.firstChild
                },
                empty: function(elem) {
                    return !elem.firstChild
                },
                has: function(elem, i, match) {
                    return !!Sizzle(match[3], elem).length
                },
                header: function(elem) {
                    return /h\d/i.test(elem.nodeName)
                },
                text: function(elem) {
                    return "text" === elem.type
                },
                radio: function(elem) {
                    return "radio" === elem.type
                },
                checkbox: function(elem) {
                    return "checkbox" === elem.type
                },
                file: function(elem) {
                    return "file" === elem.type
                },
                password: function(elem) {
                    return "password" === elem.type
                },
                submit: function(elem) {
                    return "submit" === elem.type
                },
                image: function(elem) {
                    return "image" === elem.type
                },
                reset: function(elem) {
                    return "reset" === elem.type
                },
                button: function(elem) {
                    return "button" === elem.type || elem.nodeName.toLowerCase() === "button"
                },
                input: function(elem) {
                    return /input|select|textarea|button/i.test(elem.nodeName)
                }
            },
            setFilters: {
                first: function(elem, i) {
                    return i === 0
                },
                last: function(elem, i, match, array) {
                    return i === array.length - 1
                },
                even: function(elem, i) {
                    return i % 2 === 0
                },
                odd: function(elem, i) {
                    return i % 2 === 1
                },
                lt: function(elem, i, match) {
                    return i < match[3] - 0
                },
                gt: function(elem, i, match) {
                    return i > match[3] - 0
                },
                nth: function(elem, i, match) {
                    return match[3] - 0 === i
                },
                eq: function(elem, i, match) {
                    return match[3] - 0 === i
                }
            },
            filter: {
                PSEUDO: function(elem, match, i, array) {
                    var name = match[1],
                        filter = Expr.filters[name];
                    if (filter) {
                        return filter(elem, i, match, array)
                    } else {
                        if (name === "contains") {
                            return (elem.textContent || elem.innerText || getText([elem]) || "").indexOf(match[3]) >= 0
                        } else {
                            if (name === "not") {
                                var not = match[3];
                                for (var i = 0, l = not.length; i < l; i++) {
                                    if (not[i] === elem) {
                                        return false
                                    }
                                }
                                return true
                            } else {
                                throw "Syntax error, unrecognized expression: " + name
                            }
                        }
                    }
                },
                CHILD: function(elem, match) {
                    var type = match[1],
                        node = elem;
                    switch (type) {
                        case "only":
                        case "first":
                            while ((node = node.previousSibling)) {
                                if (node.nodeType === 1) {
                                    return false
                                }
                            }
                            if (type === "first") {
                                return true
                            }
                            node = elem;
                        case "last":
                            while ((node = node.nextSibling)) {
                                if (node.nodeType === 1) {
                                    return false
                                }
                            }
                            return true;
                        case "nth":
                            var first = match[2],
                                last = match[3];
                            if (first === 1 && last === 0) {
                                return true
                            }
                            var doneName = match[0],
                                parent = elem.parentNode;
                            if (parent && (parent.sizcache !== doneName || !elem.nodeIndex)) {
                                var count = 0;
                                for (node = parent.firstChild; node; node = node.nextSibling) {
                                    if (node.nodeType === 1) {
                                        node.nodeIndex = ++count
                                    }
                                }
                                parent.sizcache = doneName
                            }
                            var diff = elem.nodeIndex - last;
                            if (first === 0) {
                                return diff === 0
                            } else {
                                return (diff % first === 0 && diff / first >= 0)
                            }
                    }
                },
                ID: function(elem, match) {
                    return elem.nodeType === 1 && elem.getAttribute("id") === match
                },
                TAG: function(elem, match) {
                    return (match === "*" && elem.nodeType === 1) || elem.nodeName.toLowerCase() === match
                },
                CLASS: function(elem, match) {
                    return (" " + (elem.className || elem.getAttribute("class")) + " ").indexOf(match) > -1
                },
                ATTR: function(elem, match) {
                    var name = match[1],
                        result = Expr.attrHandle[name] ? Expr.attrHandle[name](elem) : elem[name] != null ? elem[name] : elem.getAttribute(name),
                        value = result + "",
                        type = match[2],
                        check = match[4];
                    return result == null ? type === "!=" : type === "=" ? value === check : type === "*=" ? value.indexOf(check) >= 0 : type === "~=" ? (" " + value + " ").indexOf(check) >= 0 : !check ? value && result !== false : type === "!=" ? value !== check : type === "^=" ? value.indexOf(check) === 0 : type === "$=" ? value.substr(value.length - check.length) === check : type === "|=" ? value === check || value.substr(0, check.length + 1) === check + "-" : false
                },
                POS: function(elem, match, i, array) {
                    var name = match[2],
                        filter = Expr.setFilters[name];
                    if (filter) {
                        return filter(elem, i, match, array)
                    }
                }
            }
        };
        var origPOS = Expr.match.POS;
        for (var type in Expr.match) {
            Expr.match[type] = new RegExp(Expr.match[type].source + /(?![^\[]*\])(?![^\(]*\))/.source);
            Expr.leftMatch[type] = new RegExp(/(^(?:.|\r|\n)*?)/.source + Expr.match[type].source)
        }
        var makeArray = function(array, results) {
            array = Array.prototype.slice.call(array, 0);
            if (results) {
                results.push.apply(results, array);
                return results
            }
            return array
        };
        try {
            Array.prototype.slice.call(document.documentElement.childNodes, 0)
        } catch (e) {
            makeArray = function(array, results) {
                var ret = results || [];
                if (toString.call(array) === "[object Array]") {
                    Array.prototype.push.apply(ret, array)
                } else {
                    if (typeof array.length === "number") {
                        for (var i = 0, l = array.length; i < l; i++) {
                            ret.push(array[i])
                        }
                    } else {
                        for (var i = 0; array[i]; i++) {
                            ret.push(array[i])
                        }
                    }
                }
                return ret
            }
        }
        var sortOrder;
        if (document.documentElement.compareDocumentPosition) {
            sortOrder = function(a, b) {
                if (!a.compareDocumentPosition || !b.compareDocumentPosition) {
                    if (a == b) {
                        hasDuplicate = true
                    }
                    return a.compareDocumentPosition ? -1 : 1
                }
                var ret = a.compareDocumentPosition(b) & 4 ? -1 : a === b ? 0 : 1;
                if (ret === 0) {
                    hasDuplicate = true
                }
                return ret
            }
        } else {
            if ("sourceIndex" in document.documentElement) {
                sortOrder = function(a, b) {
                    if (!a.sourceIndex || !b.sourceIndex) {
                        if (a == b) {
                            hasDuplicate = true
                        }
                        return a.sourceIndex ? -1 : 1
                    }
                    var ret = a.sourceIndex - b.sourceIndex;
                    if (ret === 0) {
                        hasDuplicate = true
                    }
                    return ret
                }
            } else {
                if (document.createRange) {
                    sortOrder = function(a, b) {
                        if (!a.ownerDocument || !b.ownerDocument) {
                            if (a == b) {
                                hasDuplicate = true
                            }
                            return a.ownerDocument ? -1 : 1
                        }
                        var aRange = a.ownerDocument.createRange(),
                            bRange = b.ownerDocument.createRange();
                        aRange.setStart(a, 0);
                        aRange.setEnd(a, 0);
                        bRange.setStart(b, 0);
                        bRange.setEnd(b, 0);
                        var ret = aRange.compareBoundaryPoints(Range.START_TO_END, bRange);
                        if (ret === 0) {
                            hasDuplicate = true
                        }
                        return ret
                    }
                }
            }
        }

        function getText(elems) {
            var ret = "",
                elem;
            for (var i = 0; elems[i]; i++) {
                elem = elems[i];
                if (elem.nodeType === 3 || elem.nodeType === 4) {
                    ret += elem.nodeValue
                } else {
                    if (elem.nodeType !== 8) {
                        ret += getText(elem.childNodes)
                    }
                }
            }
            return ret
        }(function() {
            var form = document.createElement("div"),
                id = "script" + (new Date).getTime();
            form.innerHTML = "<a name='" + id + "'/>";
            var root = document.documentElement;
            root.insertBefore(form, root.firstChild);
            if (document.getElementById(id)) {
                Expr.find.ID = function(match, context, isXML) {
                    if (typeof context.getElementById !== "undefined" && !isXML) {
                        var m = context.getElementById(match[1]);
                        return m ? m.id === match[1] || typeof m.getAttributeNode !== "undefined" && m.getAttributeNode("id").nodeValue === match[1] ? [m] : undefined : []
                    }
                };
                Expr.filter.ID = function(elem, match) {
                    var node = typeof elem.getAttributeNode !== "undefined" && elem.getAttributeNode("id");
                    return elem.nodeType === 1 && node && node.nodeValue === match
                }
            }
            root.removeChild(form);
            root = form = null
        })();
        (function() {
            var div = document.createElement("div");
            div.appendChild(document.createComment(""));
            if (div.getElementsByTagName("*").length > 0) {
                Expr.find.TAG = function(match, context) {
                    var results = context.getElementsByTagName(match[1]);
                    if (match[1] === "*") {
                        var tmp = [];
                        for (var i = 0; results[i]; i++) {
                            if (results[i].nodeType === 1) {
                                tmp.push(results[i])
                            }
                        }
                        results = tmp
                    }
                    return results
                }
            }
            div.innerHTML = "<a href='#'></a>";
            if (div.firstChild && typeof div.firstChild.getAttribute !== "undefined" && div.firstChild.getAttribute("href") !== "#") {
                Expr.attrHandle.href = function(elem) {
                    return elem.getAttribute("href", 2)
                }
            }
            div = null
        })();
        if (document.querySelectorAll) {
            (function() {
                var oldSizzle = Sizzle,
                    div = document.createElement("div");
                div.innerHTML = "<p class='TEST'></p>";
                if (div.querySelectorAll && div.querySelectorAll(".TEST").length === 0) {
                    return
                }
                Sizzle = function(query, context, extra, seed) {
                    context = context || document;
                    if (!seed && context.nodeType === 9 && !isXML(context)) {
                        try {
                            return makeArray(context.querySelectorAll(query), extra)
                        } catch (e) {}
                    }
                    return oldSizzle(query, context, extra, seed)
                };
                for (var prop in oldSizzle) {
                    Sizzle[prop] = oldSizzle[prop]
                }
                div = null
            })()
        }(function() {
            var div = document.createElement("div");
            div.innerHTML = "<div class='test e'></div><div class='test'></div>";
            if (!div.getElementsByClassName || div.getElementsByClassName("e").length === 0) {
                return
            }
            div.lastChild.className = "e";
            if (div.getElementsByClassName("e").length === 1) {
                return
            }
            Expr.order.splice(1, 0, "CLASS");
            Expr.find.CLASS = function(match, context, isXML) {
                if (typeof context.getElementsByClassName !== "undefined" && !isXML) {
                    return context.getElementsByClassName(match[1])
                }
            };
            div = null
        })();

        function dirNodeCheck(dir, cur, doneName, checkSet, nodeCheck, isXML) {
            for (var i = 0, l = checkSet.length; i < l; i++) {
                var elem = checkSet[i];
                if (elem) {
                    elem = elem[dir];
                    var match = false;
                    while (elem) {
                        if (elem.sizcache === doneName) {
                            match = checkSet[elem.sizset];
                            break
                        }
                        if (elem.nodeType === 1 && !isXML) {
                            elem.sizcache = doneName;
                            elem.sizset = i
                        }
                        if (elem.nodeName.toLowerCase() === cur) {
                            match = elem;
                            break
                        }
                        elem = elem[dir]
                    }
                    checkSet[i] = match
                }
            }
        }

        function dirCheck(dir, cur, doneName, checkSet, nodeCheck, isXML) {
            for (var i = 0, l = checkSet.length; i < l; i++) {
                var elem = checkSet[i];
                if (elem) {
                    elem = elem[dir];
                    var match = false;
                    while (elem) {
                        if (elem.sizcache === doneName) {
                            match = checkSet[elem.sizset];
                            break
                        }
                        if (elem.nodeType === 1) {
                            if (!isXML) {
                                elem.sizcache = doneName;
                                elem.sizset = i
                            }
                            if (typeof cur !== "string") {
                                if (elem === cur) {
                                    match = true;
                                    break
                                }
                            } else {
                                if (Sizzle.filter(cur, [elem]).length > 0) {
                                    match = elem;
                                    break
                                }
                            }
                        }
                        elem = elem[dir]
                    }
                    checkSet[i] = match
                }
            }
        }
        var contains = document.compareDocumentPosition ? function(a, b) {
            return a.compareDocumentPosition(b) & 16
        } : function(a, b) {
            return a !== b && (a.contains ? a.contains(b) : true)
        };
        var isXML = function(elem) {
            var documentElement = (elem ? elem.ownerDocument || elem : 0).documentElement;
            return documentElement ? documentElement.nodeName !== "HTML" : false
        };
        var posProcess = function(selector, context) {
            var tmpSet = [],
                later = "",
                match, root = context.nodeType ? [context] : context;
            while ((match = Expr.match.PSEUDO.exec(selector))) {
                later += match[0];
                selector = selector.replace(Expr.match.PSEUDO, "")
            }
            selector = Expr.relative[selector] ? selector + "*" : selector;
            for (var i = 0, l = root.length; i < l; i++) {
                Sizzle(selector, root[i], tmpSet)
            }
            return Sizzle.filter(later, tmpSet)
        };
        return Sizzle
    })();
    S.flash = (function() {
        var swfobject = function() {
            var UNDEF = "undefined",
                OBJECT = "object",
                SHOCKWAVE_FLASH = "Shockwave Flash",
                SHOCKWAVE_FLASH_AX = "ShockwaveFlash.ShockwaveFlash",
                FLASH_MIME_TYPE = "application/x-shockwave-flash",
                EXPRESS_INSTALL_ID = "SWFObjectExprInst",
                win = window,
                doc = document,
                nav = navigator,
                domLoadFnArr = [],
                regObjArr = [],
                objIdArr = [],
                listenersArr = [],
                script, timer = null,
                storedAltContent = null,
                storedAltContentId = null,
                isDomLoaded = false,
                isExpressInstallActive = false;
            var ua = function() {
                var w3cdom = typeof doc.getElementById != UNDEF && typeof doc.getElementsByTagName != UNDEF && typeof doc.createElement != UNDEF,
                    playerVersion = [0, 0, 0],
                    d = null;
                if (typeof nav.plugins != UNDEF && typeof nav.plugins[SHOCKWAVE_FLASH] == OBJECT) {
                    d = nav.plugins[SHOCKWAVE_FLASH].description;
                    if (d && !(typeof nav.mimeTypes != UNDEF && nav.mimeTypes[FLASH_MIME_TYPE] && !nav.mimeTypes[FLASH_MIME_TYPE].enabledPlugin)) {
                        d = d.replace(/^.*\s+(\S+\s+\S+$)/, "$1");
                        playerVersion[0] = parseInt(d.replace(/^(.*)\..*$/, "$1"), 10);
                        playerVersion[1] = parseInt(d.replace(/^.*\.(.*)\s.*$/, "$1"), 10);
                        playerVersion[2] = /r/.test(d) ? parseInt(d.replace(/^.*r(.*)$/, "$1"), 10) : 0
                    }
                } else {
                    if (typeof win.ActiveXObject != UNDEF) {
                        var a = null,
                            fp6Crash = false;
                        try {
                            a = new ActiveXObject(SHOCKWAVE_FLASH_AX + ".7")
                        } catch (e) {
                            try {
                                a = new ActiveXObject(SHOCKWAVE_FLASH_AX + ".6");
                                playerVersion = [6, 0, 21];
                                a.AllowScriptAccess = "always"
                            } catch (e) {
                                if (playerVersion[0] == 6) {
                                    fp6Crash = true
                                }
                            }
                            if (!fp6Crash) {
                                try {
                                    a = new ActiveXObject(SHOCKWAVE_FLASH_AX)
                                } catch (e) {}
                            }
                        }
                        if (!fp6Crash && a) {
                            try {
                                d = a.GetVariable("$version");
                                if (d) {
                                    d = d.split(" ")[1].split(",");
                                    playerVersion = [parseInt(d[0], 10), parseInt(d[1], 10), parseInt(d[2], 10)]
                                }
                            } catch (e) {}
                        }
                    }
                }
                var u = nav.userAgent.toLowerCase(),
                    p = nav.platform.toLowerCase(),
                    webkit = /webkit/.test(u) ? parseFloat(u.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : false,
                    ie = false,
                    windows = p ? /win/.test(p) : /win/.test(u),
                    mac = p ? /mac/.test(p) : /mac/.test(u);
                /*@cc_on
                			ie = true;
                			@if (@_win32)
                				windows = true;
                			@elif (@_mac)
                				mac = true;
                			@end
                		@*/
                return {
                    w3cdom: w3cdom,
                    pv: playerVersion,
                    webkit: webkit,
                    ie: ie,
                    win: windows,
                    mac: mac
                }
            }();
            var onDomLoad = function() {
                if (!ua.w3cdom) {
                    return
                }
                addDomLoadEvent(main);
                if (ua.ie && ua.win) {
                    try {
                        doc.write("<script id=__ie_ondomload defer=true src=//:><\/script>");
                        script = getElementById("__ie_ondomload");
                        if (script) {
                            addListener(script, "onreadystatechange", checkReadyState)
                        }
                    } catch (e) {}
                }
                if (ua.webkit && typeof doc.readyState != UNDEF) {
                    timer = setInterval(function() {
                        if (/loaded|complete/.test(doc.readyState)) {
                            callDomLoadFunctions()
                        }
                    }, 10)
                }
                if (typeof doc.addEventListener != UNDEF) {
                    doc.addEventListener("DOMContentLoaded", callDomLoadFunctions, null)
                }
                addLoadEvent(callDomLoadFunctions)
            }();

            function checkReadyState() {
                if (script.readyState == "complete") {
                    script.parentNode.removeChild(script);
                    callDomLoadFunctions()
                }
            }

            function callDomLoadFunctions() {
                if (isDomLoaded) {
                    return
                }
                if (ua.ie && ua.win) {
                    var s = createElement("span");
                    try {
                        var t = doc.getElementsByTagName("body")[0].appendChild(s);
                        t.parentNode.removeChild(t)
                    } catch (e) {
                        return
                    }
                }
                isDomLoaded = true;
                if (timer) {
                    clearInterval(timer);
                    timer = null
                }
                var dl = domLoadFnArr.length;
                for (var i = 0; i < dl; i++) {
                    domLoadFnArr[i]()
                }
            }

            function addDomLoadEvent(fn) {
                if (isDomLoaded) {
                    fn()
                } else {
                    domLoadFnArr[domLoadFnArr.length] = fn
                }
            }

            function addLoadEvent(fn) {
                if (typeof win.addEventListener != UNDEF) {
                    win.addEventListener("load", fn, false)
                } else {
                    if (typeof doc.addEventListener != UNDEF) {
                        doc.addEventListener("load", fn, false)
                    } else {
                        if (typeof win.attachEvent != UNDEF) {
                            addListener(win, "onload", fn)
                        } else {
                            if (typeof win.onload == "function") {
                                var fnOld = win.onload;
                                win.onload = function() {
                                    fnOld();
                                    fn()
                                }
                            } else {
                                win.onload = fn
                            }
                        }
                    }
                }
            }

            function main() {
                var rl = regObjArr.length;
                for (var i = 0; i < rl; i++) {
                    var id = regObjArr[i].id;
                    if (ua.pv[0] > 0) {
                        var obj = getElementById(id);
                        if (obj) {
                            regObjArr[i].width = obj.getAttribute("width") ? obj.getAttribute("width") : "0";
                            regObjArr[i].height = obj.getAttribute("height") ? obj.getAttribute("height") : "0";
                            if (hasPlayerVersion(regObjArr[i].swfVersion)) {
                                if (ua.webkit && ua.webkit < 312) {
                                    fixParams(obj)
                                }
                                setVisibility(id, true)
                            } else {
                                if (regObjArr[i].expressInstall && !isExpressInstallActive && hasPlayerVersion("6.0.65") && (ua.win || ua.mac)) {
                                    showExpressInstall(regObjArr[i])
                                } else {
                                    displayAltContent(obj)
                                }
                            }
                        }
                    } else {
                        setVisibility(id, true)
                    }
                }
            }

            function fixParams(obj) {
                var nestedObj = obj.getElementsByTagName(OBJECT)[0];
                if (nestedObj) {
                    var e = createElement("embed"),
                        a = nestedObj.attributes;
                    if (a) {
                        var al = a.length;
                        for (var i = 0; i < al; i++) {
                            if (a[i].nodeName == "DATA") {
                                e.setAttribute("src", a[i].nodeValue)
                            } else {
                                e.setAttribute(a[i].nodeName, a[i].nodeValue)
                            }
                        }
                    }
                    var c = nestedObj.childNodes;
                    if (c) {
                        var cl = c.length;
                        for (var j = 0; j < cl; j++) {
                            if (c[j].nodeType == 1 && c[j].nodeName == "PARAM") {
                                e.setAttribute(c[j].getAttribute("name"), c[j].getAttribute("value"))
                            }
                        }
                    }
                    obj.parentNode.replaceChild(e, obj)
                }
            }

            function showExpressInstall(regObj) {
                isExpressInstallActive = true;
                var obj = getElementById(regObj.id);
                if (obj) {
                    if (regObj.altContentId) {
                        var ac = getElementById(regObj.altContentId);
                        if (ac) {
                            storedAltContent = ac;
                            storedAltContentId = regObj.altContentId
                        }
                    } else {
                        storedAltContent = abstractAltContent(obj)
                    }
                    if (!(/%$/.test(regObj.width)) && parseInt(regObj.width, 10) < 310) {
                        regObj.width = "310"
                    }
                    if (!(/%$/.test(regObj.height)) && parseInt(regObj.height, 10) < 137) {
                        regObj.height = "137"
                    }
                    doc.title = doc.title.slice(0, 47) + " - Flash Player Installation";
                    var pt = ua.ie && ua.win ? "ActiveX" : "PlugIn",
                        dt = doc.title,
                        fv = "MMredirectURL=" + win.location + "&MMplayerType=" + pt + "&MMdoctitle=" + dt,
                        replaceId = regObj.id;
                    if (ua.ie && ua.win && obj.readyState != 4) {
                        var newObj = createElement("div");
                        replaceId += "SWFObjectNew";
                        newObj.setAttribute("id", replaceId);
                        obj.parentNode.insertBefore(newObj, obj);
                        obj.style.display = "none";
                        var fn = function() {
                            obj.parentNode.removeChild(obj)
                        };
                        addListener(win, "onload", fn)
                    }
                    createSWF({
                        data: regObj.expressInstall,
                        id: EXPRESS_INSTALL_ID,
                        width: regObj.width,
                        height: regObj.height
                    }, {
                        flashvars: fv
                    }, replaceId)
                }
            }

            function displayAltContent(obj) {
                if (ua.ie && ua.win && obj.readyState != 4) {
                    var el = createElement("div");
                    obj.parentNode.insertBefore(el, obj);
                    el.parentNode.replaceChild(abstractAltContent(obj), el);
                    obj.style.display = "none";
                    var fn = function() {
                        obj.parentNode.removeChild(obj)
                    };
                    addListener(win, "onload", fn)
                } else {
                    obj.parentNode.replaceChild(abstractAltContent(obj), obj)
                }
            }

            function abstractAltContent(obj) {
                var ac = createElement("div");
                if (ua.win && ua.ie) {
                    ac.innerHTML = obj.innerHTML
                } else {
                    var nestedObj = obj.getElementsByTagName(OBJECT)[0];
                    if (nestedObj) {
                        var c = nestedObj.childNodes;
                        if (c) {
                            var cl = c.length;
                            for (var i = 0; i < cl; i++) {
                                if (!(c[i].nodeType == 1 && c[i].nodeName == "PARAM") && !(c[i].nodeType == 8)) {
                                    ac.appendChild(c[i].cloneNode(true))
                                }
                            }
                        }
                    }
                }
                return ac
            }

            function createSWF(attObj, parObj, id) {
                var r, el = getElementById(id);
                if (el) {
                    if (typeof attObj.id == UNDEF) {
                        attObj.id = id
                    }
                    if (ua.ie && ua.win) {
                        var att = "";
                        for (var i in attObj) {
                            if (attObj[i] != Object.prototype[i]) {
                                if (i.toLowerCase() == "data") {
                                    parObj.movie = attObj[i]
                                } else {
                                    if (i.toLowerCase() == "styleclass") {
                                        att += ' class="' + attObj[i] + '"'
                                    } else {
                                        if (i.toLowerCase() != "classid") {
                                            att += " " + i + '="' + attObj[i] + '"'
                                        }
                                    }
                                }
                            }
                        }
                        var par = "";
                        for (var j in parObj) {
                            if (parObj[j] != Object.prototype[j]) {
                                par += '<param name="' + j + '" value="' + parObj[j] + '" />'
                            }
                        }
                        el.outerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"' + att + ">" + par + "</object>";
                        objIdArr[objIdArr.length] = attObj.id;
                        r = getElementById(attObj.id)
                    } else {
                        if (ua.webkit && ua.webkit < 312) {
                            var e = createElement("embed");
                            e.setAttribute("type", FLASH_MIME_TYPE);
                            for (var k in attObj) {
                                if (attObj[k] != Object.prototype[k]) {
                                    if (k.toLowerCase() == "data") {
                                        e.setAttribute("src", attObj[k])
                                    } else {
                                        if (k.toLowerCase() == "styleclass") {
                                            e.setAttribute("class", attObj[k])
                                        } else {
                                            if (k.toLowerCase() != "classid") {
                                                e.setAttribute(k, attObj[k])
                                            }
                                        }
                                    }
                                }
                            }
                            for (var l in parObj) {
                                if (parObj[l] != Object.prototype[l]) {
                                    if (l.toLowerCase() != "movie") {
                                        e.setAttribute(l, parObj[l])
                                    }
                                }
                            }
                            el.parentNode.replaceChild(e, el);
                            r = e
                        } else {
                            var o = createElement(OBJECT);
                            o.setAttribute("type", FLASH_MIME_TYPE);
                            for (var m in attObj) {
                                if (attObj[m] != Object.prototype[m]) {
                                    if (m.toLowerCase() == "styleclass") {
                                        o.setAttribute("class", attObj[m])
                                    } else {
                                        if (m.toLowerCase() != "classid") {
                                            o.setAttribute(m, attObj[m])
                                        }
                                    }
                                }
                            }
                            for (var n in parObj) {
                                if (parObj[n] != Object.prototype[n] && n.toLowerCase() != "movie") {
                                    createObjParam(o, n, parObj[n])
                                }
                            }
                            el.parentNode.replaceChild(o, el);
                            r = o
                        }
                    }
                }
                return r
            }

            function createObjParam(el, pName, pValue) {
                var p = createElement("param");
                p.setAttribute("name", pName);
                p.setAttribute("value", pValue);
                el.appendChild(p)
            }

            function removeSWF(id) {
                var obj = getElementById(id);
                if (obj && (obj.nodeName == "OBJECT" || obj.nodeName == "EMBED")) {
                    if (ua.ie && ua.win) {
                        if (obj.readyState == 4) {
                            removeObjectInIE(id)
                        } else {
                            win.attachEvent("onload", function() {
                                removeObjectInIE(id)
                            })
                        }
                    } else {
                        obj.parentNode.removeChild(obj)
                    }
                }
            }

            function removeObjectInIE(id) {
                var obj = getElementById(id);
                if (obj) {
                    for (var i in obj) {
                        if (typeof obj[i] == "function") {
                            obj[i] = null
                        }
                    }
                    obj.parentNode.removeChild(obj)
                }
            }

            function getElementById(id) {
                var el = null;
                try {
                    el = doc.getElementById(id)
                } catch (e) {}
                return el
            }

            function createElement(el) {
                return doc.createElement(el)
            }

            function addListener(target, eventType, fn) {
                target.attachEvent(eventType, fn);
                listenersArr[listenersArr.length] = [target, eventType, fn]
            }

            function hasPlayerVersion(rv) {
                var pv = ua.pv,
                    v = rv.split(".");
                v[0] = parseInt(v[0], 10);
                v[1] = parseInt(v[1], 10) || 0;
                v[2] = parseInt(v[2], 10) || 0;
                return (pv[0] > v[0] || (pv[0] == v[0] && pv[1] > v[1]) || (pv[0] == v[0] && pv[1] == v[1] && pv[2] >= v[2])) ? true : false
            }

            function createCSS(sel, decl) {
                if (ua.ie && ua.mac) {
                    return
                }
                var h = doc.getElementsByTagName("head")[0],
                    s = createElement("style");
                s.setAttribute("type", "text/css");
                s.setAttribute("media", "screen");
                if (!(ua.ie && ua.win) && typeof doc.createTextNode != UNDEF) {
                    s.appendChild(doc.createTextNode(sel + " {" + decl + "}"))
                }
                h.appendChild(s);
                if (ua.ie && ua.win && typeof doc.styleSheets != UNDEF && doc.styleSheets.length > 0) {
                    var ls = doc.styleSheets[doc.styleSheets.length - 1];
                    if (typeof ls.addRule == OBJECT) {
                        ls.addRule(sel, decl)
                    }
                }
            }

            function setVisibility(id, isVisible) {
                var v = isVisible ? "visible" : "hidden";
                if (isDomLoaded && getElementById(id)) {
                    getElementById(id).style.visibility = v
                } else {
                    createCSS("#" + id, "visibility:" + v)
                }
            }

            function urlEncodeIfNecessary(s) {
                var regex = /[\\\"<>\.;]/;
                var hasBadChars = regex.exec(s) != null;
                return hasBadChars ? encodeURIComponent(s) : s
            }
            var cleanup = function() {
                if (ua.ie && ua.win) {
                    window.attachEvent("onunload", function() {
                        var ll = listenersArr.length;
                        for (var i = 0; i < ll; i++) {
                            listenersArr[i][0].detachEvent(listenersArr[i][1], listenersArr[i][2])
                        }
                        var il = objIdArr.length;
                        for (var j = 0; j < il; j++) {
                            removeSWF(objIdArr[j])
                        }
                        for (var k in ua) {
                            ua[k] = null
                        }
                        ua = null;
                        for (var l in swfobject) {
                            swfobject[l] = null
                        }
                        swfobject = null
                    })
                }
            }();
            return {
                registerObject: function(objectIdStr, swfVersionStr, xiSwfUrlStr) {
                    if (!ua.w3cdom || !objectIdStr || !swfVersionStr) {
                        return
                    }
                    var regObj = {};
                    regObj.id = objectIdStr;
                    regObj.swfVersion = swfVersionStr;
                    regObj.expressInstall = xiSwfUrlStr ? xiSwfUrlStr : false;
                    regObjArr[regObjArr.length] = regObj;
                    setVisibility(objectIdStr, false)
                },
                getObjectById: function(objectIdStr) {
                    var r = null;
                    if (ua.w3cdom) {
                        var o = getElementById(objectIdStr);
                        if (o) {
                            var n = o.getElementsByTagName(OBJECT)[0];
                            if (!n || (n && typeof o.SetVariable != UNDEF)) {
                                r = o
                            } else {
                                if (typeof n.SetVariable != UNDEF) {
                                    r = n
                                }
                            }
                        }
                    }
                    return r
                },
                embedSWF: function(swfUrlStr, replaceElemIdStr, widthStr, heightStr, swfVersionStr, xiSwfUrlStr, flashvarsObj, parObj, attObj) {
                    if (!ua.w3cdom || !swfUrlStr || !replaceElemIdStr || !widthStr || !heightStr || !swfVersionStr) {
                        return
                    }
                    widthStr += "";
                    heightStr += "";
                    if (hasPlayerVersion(swfVersionStr)) {
                        setVisibility(replaceElemIdStr, false);
                        var att = {};
                        if (attObj && typeof attObj === OBJECT) {
                            for (var i in attObj) {
                                if (attObj[i] != Object.prototype[i]) {
                                    att[i] = attObj[i]
                                }
                            }
                        }
                        att.data = swfUrlStr;
                        att.width = widthStr;
                        att.height = heightStr;
                        var par = {};
                        if (parObj && typeof parObj === OBJECT) {
                            for (var j in parObj) {
                                if (parObj[j] != Object.prototype[j]) {
                                    par[j] = parObj[j]
                                }
                            }
                        }
                        if (flashvarsObj && typeof flashvarsObj === OBJECT) {
                            for (var k in flashvarsObj) {
                                if (flashvarsObj[k] != Object.prototype[k]) {
                                    if (typeof par.flashvars != UNDEF) {
                                        par.flashvars += "&" + k + "=" + flashvarsObj[k]
                                    } else {
                                        par.flashvars = k + "=" + flashvarsObj[k]
                                    }
                                }
                            }
                        }
                        addDomLoadEvent(function() {
                            createSWF(att, par, replaceElemIdStr);
                            if (att.id == replaceElemIdStr) {
                                setVisibility(replaceElemIdStr, true)
                            }
                        })
                    } else {
                        if (xiSwfUrlStr && !isExpressInstallActive && hasPlayerVersion("6.0.65") && (ua.win || ua.mac)) {
                            isExpressInstallActive = true;
                            setVisibility(replaceElemIdStr, false);
                            addDomLoadEvent(function() {
                                var regObj = {};
                                regObj.id = regObj.altContentId = replaceElemIdStr;
                                regObj.width = widthStr;
                                regObj.height = heightStr;
                                regObj.expressInstall = xiSwfUrlStr;
                                showExpressInstall(regObj)
                            })
                        }
                    }
                },
                getFlashPlayerVersion: function() {
                    return {
                        major: ua.pv[0],
                        minor: ua.pv[1],
                        release: ua.pv[2]
                    }
                },
                hasFlashPlayerVersion: hasPlayerVersion,
                createSWF: function(attObj, parObj, replaceElemIdStr) {
                    if (ua.w3cdom) {
                        return createSWF(attObj, parObj, replaceElemIdStr)
                    } else {
                        return undefined
                    }
                },
                removeSWF: function(objElemIdStr) {
                    if (ua.w3cdom) {
                        removeSWF(objElemIdStr)
                    }
                },
                createCSS: function(sel, decl) {
                    if (ua.w3cdom) {
                        createCSS(sel, decl)
                    }
                },
                addDomLoadEvent: addDomLoadEvent,
                addLoadEvent: addLoadEvent,
                getQueryParamValue: function(param) {
                    var q = doc.location.search || doc.location.hash;
                    if (param == null) {
                        return urlEncodeIfNecessary(q)
                    }
                    if (q) {
                        var pairs = q.substring(1).split("&");
                        for (var i = 0; i < pairs.length; i++) {
                            if (pairs[i].substring(0, pairs[i].indexOf("=")) == param) {
                                return urlEncodeIfNecessary(pairs[i].substring((pairs[i].indexOf("=") + 1)))
                            }
                        }
                    }
                    return ""
                },
                expressInstallCallback: function() {
                    if (isExpressInstallActive && storedAltContent) {
                        var obj = getElementById(EXPRESS_INSTALL_ID);
                        if (obj) {
                            obj.parentNode.replaceChild(storedAltContent, obj);
                            if (storedAltContentId) {
                                setVisibility(storedAltContentId, true);
                                if (ua.ie && ua.win) {
                                    storedAltContent.style.display = "block"
                                }
                            }
                            storedAltContent = null;
                            storedAltContentId = null;
                            isExpressInstallActive = false
                        }
                    }
                }
            }
        }();
        return swfobject
    })();
    S.lang = {
        code: "en",
        of: "of",
        loading: "loading",
        cancel: "Cancel",
        next: "Next",
        previous: "Previous",
        play: "Play",
        pause: "Pause",
        close: "Close",
        errors: {
            single: 'You must install the <a href="{0}">{1}</a> browser plugin to view this content.',
            shared: 'You must install both the <a href="{0}">{1}</a> and <a href="{2}">{3}</a> browser plugins to view this content.',
            either: 'You must install either the <a href="{0}">{1}</a> or the <a href="{2}">{3}</a> browser plugin to view this content.'
        }
    };
    var pre, proxyId = "sb-drag-proxy",
        dragData, dragProxy, dragTarget;

    function resetDrag() {
        dragData = {
            x: 0,
            y: 0,
            startX: null,
            startY: null
        }
    }

    function updateProxy() {
        var dims = S.dimensions;
        apply(dragProxy.style, {
            height: dims.innerHeight + "px",
            width: dims.innerWidth + "px"
        })
    }

    function enableDrag() {
        resetDrag();
        var style = ["position:absolute", "cursor:" + (S.isGecko ? "-moz-grab" : "move"), "background-color:" + (S.isIE ? "#fff;filter:alpha(opacity=0)" : "transparent")].join(";");
        S.appendHTML(S.skin.body, '<div id="' + proxyId + '" style="' + style + '"></div>');
        dragProxy = get(proxyId);
        updateProxy();
        addEvent(dragProxy, "mousedown", startDrag)
    }

    function disableDrag() {
        if (dragProxy) {
            removeEvent(dragProxy, "mousedown", startDrag);
            remove(dragProxy);
            dragProxy = null
        }
        dragTarget = null
    }

    function startDrag(e) {
        preventDefault(e);
        var xy = getPageXY(e);
        dragData.startX = xy[0];
        dragData.startY = xy[1];
        dragTarget = get(S.player.id);
        addEvent(document, "mousemove", positionDrag);
        addEvent(document, "mouseup", endDrag);
        if (S.isGecko) {
            dragProxy.style.cursor = "-moz-grabbing"
        }
    }

    function positionDrag(e) {
        var player = S.player,
            dims = S.dimensions,
            xy = getPageXY(e);
        var moveX = xy[0] - dragData.startX;
        dragData.startX += moveX;
        dragData.x = Math.max(Math.min(0, dragData.x + moveX), dims.innerWidth - player.width);
        var moveY = xy[1] - dragData.startY;
        dragData.startY += moveY;
        dragData.y = Math.max(Math.min(0, dragData.y + moveY), dims.innerHeight - player.height);
        apply(dragTarget.style, {
            left: dragData.x + "px",
            top: dragData.y + "px"
        })
    }

    function endDrag() {
        removeEvent(document, "mousemove", positionDrag);
        removeEvent(document, "mouseup", endDrag);
        if (S.isGecko) {
            dragProxy.style.cursor = "-moz-grab"
        }
    }
    S.img = function(obj, id) {
        this.obj = obj;
        this.id = id;
        this.ready = false;
        var self = this;
        pre = new Image();
        pre.onload = function() {
            self.height = obj.height ? parseInt(obj.height, 10) : pre.height;
            self.width = obj.width ? parseInt(obj.width, 10) : pre.width;
            self.ready = true;
            pre.onload = null;
            pre = null
        };
        pre.src = obj.content
    };
    S.img.ext = ["bmp", "gif", "jpg", "jpeg", "png"];
    S.img.prototype = {
        append: function(body, dims) {
            var img = document.createElement("img");
            img.id = this.id;
            img.src = this.obj.content;
            img.style.position = "absolute";
            var height, width;
            if (dims.oversized && S.options.handleOversize == "resize") {
                height = dims.innerHeight;
                width = dims.innerWidth
            } else {
                height = this.height;
                width = this.width
            }
            img.setAttribute("height", height);
            img.setAttribute("width", width);
            body.appendChild(img)
        },
        remove: function() {
            var el = get(this.id);
            if (el) {
                remove(el)
            }
            disableDrag();
            if (pre) {
                pre.onload = null;
                pre = null
            }
        },
        onLoad: function() {
            var dims = S.dimensions;
            if (dims.oversized && S.options.handleOversize == "drag") {
                enableDrag()
            }
        },
        onWindowResize: function() {
            var dims = S.dimensions;
            switch (S.options.handleOversize) {
                case "resize":
                    var el = get(this.id);
                    el.height = dims.innerHeight;
                    el.width = dims.innerWidth;
                    break;
                case "drag":
                    if (dragTarget) {
                        var top = parseInt(S.getStyle(dragTarget, "top")),
                            left = parseInt(S.getStyle(dragTarget, "left"));
                        if (top + this.height < dims.innerHeight) {
                            dragTarget.style.top = dims.innerHeight - this.height + "px"
                        }
                        if (left + this.width < dims.innerWidth) {
                            dragTarget.style.left = dims.innerWidth - this.width + "px"
                        }
                        updateProxy()
                    }
                    break
            }
        }
    };
    S.iframe = function(obj, id) {
        this.obj = obj;
        this.id = id;
        var overlay = get("sb-overlay");
        this.height = obj.height ? parseInt(obj.height, 10) : overlay.offsetHeight;
        this.width = obj.width ? parseInt(obj.width, 10) : overlay.offsetWidth
    };
    S.iframe.prototype = {
        append: function(body, dims) {
            var html = '<iframe id="' + this.id + '" name="' + this.id + '" height="100%" width="100%" frameborder="0" marginwidth="0" marginheight="0" style="visibility:hidden" onload="this.style.visibility=\'visible\'" scrolling="auto"';
            if (S.isIE) {
                html += ' allowtransparency="true"';
                if (S.isIE6) {
                    html += " src=\"javascript:false;document.write('');\""
                }
            }
            html += "></iframe>";
            body.innerHTML = html
        },
        remove: function() {
            var el = get(this.id);
            if (el) {
                remove(el);
                if (S.isGecko) {
                    delete window.frames[this.id]
                }
            }
        },
        onLoad: function() {
            var win = S.isIE ? get(this.id).contentWindow : window.frames[this.id];
            win.location.href = this.obj.content
        }
    };
    S.html = function(obj, id) {
        this.obj = obj;
        this.id = id;
        this.height = obj.height ? parseInt(obj.height, 10) : 300;
        this.width = obj.width ? parseInt(obj.width, 10) : 500
    };
    S.html.prototype = {
        append: function(body, dims) {
            var div = document.createElement("div");
            div.id = this.id;
            div.className = "html";
            div.innerHTML = this.obj.content;
            body.appendChild(div)
        },
        remove: function() {
            var el = get(this.id);
            if (el) {
                remove(el)
            }
        }
    };
    S.swf = function(obj, id) {
        this.obj = obj;
        this.id = id;
        this.height = obj.height ? parseInt(obj.height, 10) : 300;
        this.width = obj.width ? parseInt(obj.width, 10) : 300
    };
    S.swf.ext = ["swf"];
    S.swf.prototype = {
        append: function(body, dims) {
            var tmp = document.createElement("div");
            tmp.id = this.id;
            body.appendChild(tmp);
            var height = dims.innerHeight,
                width = dims.innerWidth,
                swf = this.obj.content,
                version = S.options.flashVersion,
                express = S.path + "expressInstall.swf",
                flashvars = S.options.flashVars,
                params = S.options.flashParams;
            S.flash.embedSWF(swf, this.id, width, height, version, express, flashvars, params)
        },
        remove: function() {
            S.flash.expressInstallCallback();
            S.flash.removeSWF(this.id)
        },
        onWindowResize: function() {
            var dims = S.dimensions,
                el = get(this.id);
            el.height = dims.innerHeight;
            el.width = dims.innerWidth
        }
    };
    var overlayOn = false,
        visibilityCache = [],
        pngIds = ["sb-nav-close", "sb-nav-next", "sb-nav-play", "sb-nav-pause", "sb-nav-previous"],
        container, overlay, wrapper, doWindowResize = true;

    function animate(el, property, to, duration, callback) {
        var isOpacity = (property == "opacity"),
            anim = isOpacity ? S.setOpacity : function(el, value) {
                el.style[property] = "" + value + "px"
            };
        if (duration == 0 || (!isOpacity && !S.options.animate) || (isOpacity && !S.options.animateFade)) {
            anim(el, to);
            if (callback) {
                callback()
            }
            return
        }
        var from = parseFloat(S.getStyle(el, property)) || 0;
        var delta = to - from;
        if (delta == 0) {
            if (callback) {
                callback()
            }
            return
        }
        duration *= 1000;
        var begin = now(),
            ease = S.ease,
            end = begin + duration,
            time;
        var interval = setInterval(function() {
            time = now();
            if (time >= end) {
                clearInterval(interval);
                interval = null;
                anim(el, to);
                if (callback) {
                    callback()
                }
            } else {
                anim(el, from + ease((time - begin) / duration) * delta)
            }
        }, 10)
    }

    function setSize() {
        container.style.height = S.getWindowSize("Height") + "px";
        container.style.width = S.getWindowSize("Width") + "px"
    }

    function setPosition() {
        container.style.top = document.documentElement.scrollTop + "px";
        container.style.left = document.documentElement.scrollLeft + "px"
    }

    function toggleTroubleElements(on) {
        if (on) {
            each(visibilityCache, function(i, el) {
                el[0].style.visibility = el[1] || ""
            })
        } else {
            visibilityCache = [];
            each(S.options.troubleElements, function(i, tag) {
                each(document.getElementsByTagName(tag), function(j, el) {
                    visibilityCache.push([el, el.style.visibility]);
                    el.style.visibility = "hidden"
                })
            })
        }
    }

    function toggleNav(id, on) {
        var el = get("sb-nav-" + id);
        if (el) {
            el.style.display = on ? "" : "none"
        }
    }

    function toggleLoading(on, callback) {
        var loading = get("sb-loading"),
            playerName = S.getCurrent().player,
            anim = (playerName == "img" || playerName == "html");
        if (on) {
            S.setOpacity(loading, 0);
            loading.style.display = "block";
            var wrapped = function() {
                S.clearOpacity(loading);
                if (callback) {
                    callback()
                }
            };
            if (anim) {
                animate(loading, "opacity", 1, S.options.fadeDuration, wrapped)
            } else {
                wrapped()
            }
        } else {
            var wrapped = function() {
                loading.style.display = "none";
                S.clearOpacity(loading);
                if (callback) {
                    callback()
                }
            };
            if (anim) {
                animate(loading, "opacity", 0, S.options.fadeDuration, wrapped)
            } else {
                wrapped()
            }
        }
    }

    function buildBars(callback) {
        var obj = S.getCurrent();
        get("sb-title-inner").innerHTML = obj.title || "";
        var close, next, play, pause, previous;
        if (S.options.displayNav) {
            close = true;
            var len = S.gallery.length;
            if (len > 1) {
                if (S.options.continuous) {
                    next = previous = true
                } else {
                    next = (len - 1) > S.current;
                    previous = S.current > 0
                }
            }
            if (S.options.slideshowDelay > 0 && S.hasNext()) {
                pause = !S.isPaused();
                play = !pause
            }
        } else {
            close = next = play = pause = previous = false
        }
        toggleNav("close", close);
        toggleNav("next", next);
        toggleNav("play", play);
        toggleNav("pause", pause);
        toggleNav("previous", previous);
        var counter = "";
        if (S.options.displayCounter && S.gallery.length > 1) {
            var len = S.gallery.length;
            if (S.options.counterType == "skip") {
                var i = 0,
                    end = len,
                    limit = parseInt(S.options.counterLimit) || 0;
                if (limit < len && limit > 2) {
                    var h = Math.floor(limit / 2);
                    i = S.current - h;
                    if (i < 0) {
                        i += len
                    }
                    end = S.current + (limit - h);
                    if (end > len) {
                        end -= len
                    }
                }
                while (i != end) {
                    if (i == len) {
                        i = 0
                    }
                    counter += '<a onclick="Shadowbox.change(' + i + ');"';
                    if (i == S.current) {
                        counter += ' class="sb-counter-current"'
                    }
                    counter += ">" + (++i) + "</a>"
                }
            } else {
                counter = [S.current + 1, S.lang.of, len].join(" ")
            }
        }
        get("sb-counter").innerHTML = counter;
        callback()
    }

    function showBars(callback) {
        var titleInner = get("sb-title-inner"),
            infoInner = get("sb-info-inner"),
            duration = 0.35;
        titleInner.style.visibility = infoInner.style.visibility = "";
        if (titleInner.innerHTML != "") {
            animate(titleInner, "marginTop", 0, duration)
        }
        animate(infoInner, "marginTop", 0, duration, callback)
    }

    function hideBars(anim, callback) {
        var title = get("sb-title"),
            info = get("sb-info"),
            titleHeight = title.offsetHeight,
            infoHeight = info.offsetHeight,
            titleInner = get("sb-title-inner"),
            infoInner = get("sb-info-inner"),
            duration = (anim ? 0.35 : 0);
        animate(titleInner, "marginTop", titleHeight, duration);
        animate(infoInner, "marginTop", infoHeight * -1, duration, function() {
            titleInner.style.visibility = infoInner.style.visibility = "hidden";
            callback()
        })
    }

    function adjustHeight(height, top, anim, callback) {
        var wrapperInner = get("sb-wrapper-inner"),
            duration = (anim ? S.options.resizeDuration : 0);
        animate(wrapper, "top", top, duration);
        animate(wrapperInner, "height", height, duration, callback)
    }

    function adjustWidth(width, left, anim, callback) {
        var duration = (anim ? S.options.resizeDuration : 0);
        animate(wrapper, "left", left, duration);
        animate(wrapper, "width", width, duration, callback)
    }

    function setDimensions(height, width) {
        var bodyInner = get("sb-body-inner"),
            height = parseInt(height),
            width = parseInt(width),
            topBottom = wrapper.offsetHeight - bodyInner.offsetHeight,
            leftRight = wrapper.offsetWidth - bodyInner.offsetWidth,
            maxHeight = overlay.offsetHeight,
            maxWidth = overlay.offsetWidth,
            padding = parseInt(S.options.viewportPadding) || 20,
            preserveAspect = (S.player && S.options.handleOversize != "drag");
        return S.setDimensions(height, width, maxHeight, maxWidth, topBottom, leftRight, padding, preserveAspect)
    }
    var K = {};
    K.markup = '<div id="sb-container"><div id="sb-overlay"></div><div id="sb-wrapper"><div id="sb-title"><div id="sb-title-inner"></div></div><div id="sb-wrapper-inner"><div id="sb-body"><div id="sb-body-inner"></div><div id="sb-loading"><div id="sb-loading-inner"><span>{loading}</span></div></div></div></div><div id="sb-info"><div id="sb-info-inner"><div id="sb-counter"></div><div id="sb-nav"><a id="sb-nav-close" title="{close}" onclick="Shadowbox.close()"></a><a id="sb-nav-next" title="{next}" onclick="Shadowbox.next()"></a><a id="sb-nav-play" title="{play}" onclick="Shadowbox.play()"></a><a id="sb-nav-pause" title="{pause}" onclick="Shadowbox.pause()"></a><a id="sb-nav-previous" title="{previous}" onclick="Shadowbox.previous()"></a></div></div></div></div></div>';
    K.options = {
        animSequence: "sync",
        counterLimit: 10,
        counterType: "default",
        displayCounter: true,
        displayNav: true,
        fadeDuration: 0.35,
        initialHeight: 160,
        initialWidth: 320,
        modal: false,
        overlayColor: "#000",
        overlayOpacity: 0.5,
        resizeDuration: 0.35,
        showOverlay: true,
        troubleElements: ["select", "object", "embed", "canvas"]
    };
    K.init = function() {
        S.appendHTML(document.body, sprintf(K.markup, S.lang));
        K.body = get("sb-body-inner");
        container = get("sb-container");
        overlay = get("sb-overlay");
        wrapper = get("sb-wrapper");
        if (!supportsFixed) {
            container.style.position = "absolute"
        }
        if (!supportsOpacity) {
            var el, m, re = /url\("(.*\.png)"\)/;
            each(pngIds, function(i, id) {
                el = get(id);
                if (el) {
                    m = S.getStyle(el, "backgroundImage").match(re);
                    if (m) {
                        el.style.backgroundImage = "none";
                        el.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true,src=" + m[1] + ",sizingMethod=scale);"
                    }
                }
            })
        }
        var timer;
        addEvent(window, "resize", function() {
            if (timer) {
                clearTimeout(timer);
                timer = null
            }
            if (open) {
                timer = setTimeout(K.onWindowResize, 10)
            }
        })
    };
    K.onOpen = function(obj, callback) {
        doWindowResize = false;
        container.style.display = "block";
        setSize();
        var dims = setDimensions(S.options.initialHeight, S.options.initialWidth);
        adjustHeight(dims.innerHeight, dims.top);
        adjustWidth(dims.width, dims.left);
        if (S.options.showOverlay) {
            overlay.style.backgroundColor = S.options.overlayColor;
            S.setOpacity(overlay, 0);
            if (!S.options.modal) {
                addEvent(overlay, "click", S.close)
            }
            overlayOn = true
        }
        if (!supportsFixed) {
            setPosition();
            addEvent(window, "scroll", setPosition)
        }
        toggleTroubleElements();
        container.style.visibility = "visible";
        if (overlayOn) {
            animate(overlay, "opacity", S.options.overlayOpacity, S.options.fadeDuration, callback)
        } else {
            callback()
        }
    };
    K.onLoad = function(changing, callback) {
        toggleLoading(true);
        while (K.body.firstChild) {
            remove(K.body.firstChild)
        }
        hideBars(changing, function() {
            if (!open) {
                return
            }
            if (!changing) {
                wrapper.style.visibility = "visible"
            }
            buildBars(callback)
        })
    };
    K.onReady = function(callback) {
        if (!open) {
            return
        }
        var player = S.player,
            dims = setDimensions(player.height, player.width);
        var wrapped = function() {
            showBars(callback)
        };
        switch (S.options.animSequence) {
            case "hw":
                adjustHeight(dims.innerHeight, dims.top, true, function() {
                    adjustWidth(dims.width, dims.left, true, wrapped)
                });
                break;
            case "wh":
                adjustWidth(dims.width, dims.left, true, function() {
                    adjustHeight(dims.innerHeight, dims.top, true, wrapped)
                });
                break;
            default:
                adjustWidth(dims.width, dims.left, true);
                adjustHeight(dims.innerHeight, dims.top, true, wrapped)
        }
    };
    K.onShow = function(callback) {
        toggleLoading(false, callback);
        doWindowResize = true
    };
    K.onClose = function() {
        if (!supportsFixed) {
            removeEvent(window, "scroll", setPosition)
        }
        removeEvent(overlay, "click", S.close);
        wrapper.style.visibility = "hidden";
        var callback = function() {
            container.style.visibility = "hidden";
            container.style.display = "none";
            toggleTroubleElements(true)
        };
        if (overlayOn) {
            animate(overlay, "opacity", 0, S.options.fadeDuration, callback)
        } else {
            callback()
        }
    };
    K.onPlay = function() {
        toggleNav("play", false);
        toggleNav("pause", true)
    };
    K.onPause = function() {
        toggleNav("pause", false);
        toggleNav("play", true)
    };
    K.onWindowResize = function() {
        if (!doWindowResize) {
            return
        }
        setSize();
        var player = S.player,
            dims = setDimensions(player.height, player.width);
        adjustWidth(dims.width, dims.left);
        adjustHeight(dims.innerHeight, dims.top);
        if (player.onWindowResize) {
            player.onWindowResize()
        }
    };
    S.skin = K;
    window.Shadowbox = S
})(window);
(function($) {
    var defaults = {
        url: false,
        callback: false,
        target: false,
        duration: 120,
        on: "mouseover",
        touch: true,
        onZoomIn: false,
        onZoomOut: false,
        magnify: 1
    };
    $.zoom = function(target, source, img, magnify) {
        var targetHeight, targetWidth, sourceHeight, sourceWidth, xRatio, yRatio, offset, $target = $(target),
            position = $target.css("position"),
            $source = $(source);
        $target.css("position", /(absolute|fixed)/.test(position) ? position : "relative");
        $target.css("overflow", "hidden");
        img.style.width = img.style.height = "";
        $(img).addClass("zoomImg").css({
            position: "absolute",
            top: 0,
            left: 0,
            opacity: 0,
            width: img.width * magnify,
            height: img.height * magnify,
            border: "none",
            maxWidth: "none",
            maxHeight: "none"
        }).appendTo(target);
        return {
            init: function() {
                targetWidth = $target.outerWidth();
                targetHeight = $target.outerHeight();
                if (source === $target[0]) {
                    sourceWidth = targetWidth;
                    sourceHeight = targetHeight
                } else {
                    sourceWidth = $source.outerWidth();
                    sourceHeight = $source.outerHeight()
                }
                xRatio = (img.width - targetWidth) / sourceWidth;
                yRatio = (img.height - targetHeight) / sourceHeight;
                offset = $source.offset()
            },
            move: function(e) {
                var left = e.pageX - offset.left,
                    top = e.pageY - offset.top;
                top = Math.max(Math.min(top, sourceHeight), 0);
                left = Math.max(Math.min(left, sourceWidth), 0);
                img.style.left = left * -xRatio + "px";
                img.style.top = top * -yRatio + "px"
            }
        }
    };
    $.fn.zoom = function(options) {
        return this.each(function() {
            var settings = $.extend({}, defaults, options || {}),
                target = settings.target || this,
                source = this,
                $source = $(source),
                $target = $(target),
                img = document.createElement("img"),
                $img = $(img),
                mousemove = "mousemove.zoom",
                clicked = false,
                touched = false,
                $urlElement;
            if (!settings.url) {
                $urlElement = $source.find("img");
                if ($urlElement[0]) {
                    settings.url = $urlElement.data("src") || $urlElement.attr("src")
                }
                if (!settings.url) {
                    return
                }
            }(function() {
                var position = $target.css("position");
                var overflow = $target.css("overflow");
                $source.one("zoom.destroy", function() {
                    $source.off(".zoom");
                    $target.css("position", position);
                    $target.css("overflow", overflow);
                    $img.remove()
                })
            })();
            img.onload = function() {
                var zoom = $.zoom(target, source, img, settings.magnify);

                function start(e) {
                    zoom.init();
                    zoom.move(e);
                    $img.stop().fadeTo($.support.opacity ? settings.duration : 0, 1, $.isFunction(settings.onZoomIn) ? settings.onZoomIn.call(img) : false)
                }

                function stop() {
                    $img.stop().fadeTo(settings.duration, 0, $.isFunction(settings.onZoomOut) ? settings.onZoomOut.call(img) : false)
                }
                if (settings.on === "grab") {
                    $source.on("mousedown.zoom", function(e) {
                        if (e.which === 1) {
                            $(document).one("mouseup.zoom", function() {
                                stop();
                                $(document).off(mousemove, zoom.move)
                            });
                            start(e);
                            $(document).on(mousemove, zoom.move);
                            e.preventDefault()
                        }
                    })
                } else if (settings.on === "click") {
                    $source.on("click.zoom", function(e) {
                        if (clicked) {
                            return
                        } else {
                            clicked = true;
                            start(e);
                            $(document).on(mousemove, zoom.move);
                            $(document).one("click.zoom", function() {
                                stop();
                                clicked = false;
                                $(document).off(mousemove, zoom.move)
                            });
                            return false
                        }
                    })
                } else if (settings.on === "toggle") {
                    $source.on("click.zoom", function(e) {
                        if (clicked) {
                            stop()
                        } else {
                            start(e)
                        }
                        clicked = !clicked
                    })
                } else if (settings.on === "mouseover") {
                    zoom.init();
                    $source.on("mouseenter.zoom", start).on("mouseleave.zoom", stop).on(mousemove, zoom.move)
                }
                if (settings.touch) {
                    $source.on("touchstart.zoom", function(e) {
                        e.preventDefault();
                        if (touched) {
                            touched = false;
                            stop()
                        } else {
                            touched = true;
                            start(e.originalEvent.touches[0] || e.originalEvent.changedTouches[0])
                        }
                    }).on("touchmove.zoom", function(e) {
                        e.preventDefault();
                        zoom.move(e.originalEvent.touches[0] || e.originalEvent.changedTouches[0])
                    }).on("touchend.zoom", function(e) {
                        e.preventDefault();
                        if (touched) {
                            touched = false;
                            stop()
                        }
                    })
                }
                if ($.isFunction(settings.callback)) {
                    settings.callback.call(img)
                }
            };
            img.src = settings.url
        })
    };
    $.fn.zoom.defaults = defaults
})(window.jQuery);
var DateFormat = {};
! function(a) {
    var b = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        c = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        d = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        e = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        f = {
            Jan: "01",
            Feb: "02",
            Mar: "03",
            Apr: "04",
            May: "05",
            Jun: "06",
            Jul: "07",
            Aug: "08",
            Sep: "09",
            Oct: "10",
            Nov: "11",
            Dec: "12"
        },
        g = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.?\d{0,3}[Z\-+]?(\d{2}:?\d{2})?/;
    a.format = function() {
        function a(a) {
            return b[parseInt(a, 10)] || a
        }

        function h(a) {
            return c[parseInt(a, 10)] || a
        }

        function i(a) {
            var b = parseInt(a, 10) - 1;
            return d[b] || a
        }

        function j(a) {
            var b = parseInt(a, 10) - 1;
            return e[b] || a
        }

        function k(a) {
            return f[a] || a
        }

        function l(a) {
            var b, c, d, e, f, g = a,
                h = "";
            return -1 !== g.indexOf(".") && (e = g.split("."), g = e[0], h = e[e.length - 1]), f = g.split(":"), 3 === f.length ? (b = f[0], c = f[1], d = f[2].replace(/\s.+/, "").replace(/[a-z]/gi, ""), g = g.replace(/\s.+/, "").replace(/[a-z]/gi, ""), {
                time: g,
                hour: b,
                minute: c,
                second: d,
                millis: h
            }) : {
                time: "",
                hour: "",
                minute: "",
                second: "",
                millis: ""
            }
        }

        function m(a, b) {
            for (var c = b - String(a).length, d = 0; c > d; d++) a = "0" + a;
            return a
        }
        return {
            parseDate: function(a) {
                var b, c, d = {
                    date: null,
                    year: null,
                    month: null,
                    dayOfMonth: null,
                    dayOfWeek: null,
                    time: null
                };
                if ("number" == typeof a) return this.parseDate(new Date(a));
                if ("function" == typeof a.getFullYear) d.year = String(a.getFullYear()), d.month = String(a.getMonth() + 1), d.dayOfMonth = String(a.getDate()), d.time = l(a.toTimeString() + "." + a.getMilliseconds());
                else if (-1 != a.search(g)) b = a.split(/[T\+-]/), d.year = b[0], d.month = b[1], d.dayOfMonth = b[2], d.time = l(b[3].split(".")[0]);
                else switch (b = a.split(" "), 6 === b.length && isNaN(b[5]) && (b[b.length] = "()"), b.length) {
                    case 6:
                        d.year = b[5], d.month = k(b[1]), d.dayOfMonth = b[2], d.time = l(b[3]);
                        break;
                    case 2:
                        c = b[0].split("-"), d.year = c[0], d.month = c[1], d.dayOfMonth = c[2], d.time = l(b[1]);
                        break;
                    case 7:
                    case 9:
                    case 10:
                        d.year = b[3], d.month = k(b[1]), d.dayOfMonth = b[2], d.time = l(b[4]);
                        break;
                    case 1:
                        c = b[0].split(""), d.year = c[0] + c[1] + c[2] + c[3], d.month = c[5] + c[6], d.dayOfMonth = c[8] + c[9], d.time = l(c[13] + c[14] + c[15] + c[16] + c[17] + c[18] + c[19] + c[20]);
                        break;
                    default:
                        return null
                }
                return d.date = d.time ? new Date(d.year, d.month - 1, d.dayOfMonth, d.time.hour, d.time.minute, d.time.second, d.time.millis) : new Date(d.year, d.month - 1, d.dayOfMonth), d.dayOfWeek = String(d.date.getDay()), d
            },
            date: function(b, c) {
                try {
                    var d = this.parseDate(b);
                    if (null === d) return b;
                    for (var e, f = d.year, g = d.month, k = d.dayOfMonth, l = d.dayOfWeek, n = d.time, o = "", p = "", q = "", r = !1, s = 0; s < c.length; s++) {
                        var t = c.charAt(s),
                            u = c.charAt(s + 1);
                        if (r) "'" == t ? (p += "" === o ? "'" : o, o = "", r = !1) : o += t;
                        else switch (o += t, q = "", o) {
                            case "ddd":
                                p += a(l), o = "";
                                break;
                            case "dd":
                                if ("d" === u) break;
                                p += m(k, 2), o = "";
                                break;
                            case "d":
                                if ("d" === u) break;
                                p += parseInt(k, 10), o = "";
                                break;
                            case "D":
                                k = 1 == k || 21 == k || 31 == k ? parseInt(k, 10) + "st" : 2 == k || 22 == k ? parseInt(k, 10) + "nd" : 3 == k || 23 == k ? parseInt(k, 10) + "rd" : parseInt(k, 10) + "th", p += k, o = "";
                                break;
                            case "MMMM":
                                p += j(g), o = "";
                                break;
                            case "MMM":
                                if ("M" === u) break;
                                p += i(g), o = "";
                                break;
                            case "MM":
                                if ("M" === u) break;
                                p += m(g, 2), o = "";
                                break;
                            case "M":
                                if ("M" === u) break;
                                p += parseInt(g, 10), o = "";
                                break;
                            case "y":
                            case "yyy":
                                if ("y" === u) break;
                                p += o, o = "";
                                break;
                            case "yy":
                                if ("y" === u) break;
                                p += String(f).slice(-2), o = "";
                                break;
                            case "yyyy":
                                p += f, o = "";
                                break;
                            case "HH":
                                p += m(n.hour, 2), o = "";
                                break;
                            case "H":
                                if ("H" === u) break;
                                p += parseInt(n.hour, 10), o = "";
                                break;
                            case "hh":
                                e = 0 === parseInt(n.hour, 10) ? 12 : n.hour < 13 ? n.hour : n.hour - 12, p += m(e, 2), o = "";
                                break;
                            case "h":
                                if ("h" === u) break;
                                e = 0 === parseInt(n.hour, 10) ? 12 : n.hour < 13 ? n.hour : n.hour - 12, p += parseInt(e, 10), o = "";
                                break;
                            case "mm":
                                p += m(n.minute, 2), o = "";
                                break;
                            case "m":
                                if ("m" === u) break;
                                p += n.minute, o = "";
                                break;
                            case "ss":
                                p += m(n.second.substring(0, 2), 2), o = "";
                                break;
                            case "s":
                                if ("s" === u) break;
                                p += n.second, o = "";
                                break;
                            case "S":
                            case "SS":
                                if ("S" === u) break;
                                p += o, o = "";
                                break;
                            case "SSS":
                                var v = "000" + n.millis.substring(0, 3);
                                p += v.substring(v.length - 3), o = "";
                                break;
                            case "a":
                                p += n.hour >= 12 ? "PM" : "AM", o = "";
                                break;
                            case "p":
                                p += n.hour >= 12 ? "p.m." : "a.m.", o = "";
                                break;
                            case "E":
                                p += h(l), o = "";
                                break;
                            case "'":
                                o = "", r = !0;
                                break;
                            default:
                                p += t, o = ""
                        }
                    }
                    return p += q
                } catch (w) {
                    return console && console.log && console.log(w), b
                }
            },
            prettyDate: function(a) {
                var b, c, d;
                return ("string" == typeof a || "number" == typeof a) && (b = new Date(a)), "object" == typeof a && (b = new Date(a.toString())), c = ((new Date).getTime() - b.getTime()) / 1e3, d = Math.floor(c / 86400), isNaN(d) || 0 > d ? void 0 : 60 > c ? "just now" : 120 > c ? "1 minute ago" : 3600 > c ? Math.floor(c / 60) + " minutes ago" : 7200 > c ? "1 hour ago" : 86400 > c ? Math.floor(c / 3600) + " hours ago" : 1 === d ? "Yesterday" : 7 > d ? d + " days ago" : 31 > d ? Math.ceil(d / 7) + " weeks ago" : d >= 31 ? "more than 5 weeks ago" : void 0
            },
            toBrowserTimeZone: function(a, b) {
                return this.date(new Date(a), b || "MM/dd/yyyy HH:mm:ss")
            }
        }
    }()
}(DateFormat),
function(a) {
    a.format = DateFormat.format
}(jQuery);
! function(a, b) {
    "use strict";
    a.MixItUp = function() {
        var b = this;
        b._execAction("_constructor", 0), a.extend(b, {
            selectors: {
                target: ".mix",
                filter: ".filter",
                sort: ".sort"
            },
            animation: {
                enable: !0,
                effects: "fade scale",
                duration: 600,
                easing: "ease",
                perspectiveDistance: "3000",
                perspectiveOrigin: "50% 50%",
                queue: !0,
                queueLimit: 1,
                animateChangeLayout: !1,
                animateResizeContainer: !0,
                animateResizeTargets: !1,
                staggerSequence: !1,
                reverseOut: !1
            },
            callbacks: {
                onMixLoad: !1,
                onMixStart: !1,
                onMixBusy: !1,
                onMixEnd: !1,
                onMixFail: !1,
                _user: !1
            },
            controls: {
                enable: !0,
                live: !1,
                toggleFilterButtons: !1,
                toggleLogic: "or",
                activeClass: "active"
            },
            layout: {
                display: "inline-block",
                containerClass: "",
                containerClassFail: "fail"
            },
            load: {
                filter: "all",
                sort: !1
            },
            _$body: null,
            _$container: null,
            _$targets: null,
            _$parent: null,
            _$sortButtons: null,
            _$filterButtons: null,
            _suckMode: !1,
            _mixing: !1,
            _sorting: !1,
            _clicking: !1,
            _loading: !0,
            _changingLayout: !1,
            _changingClass: !1,
            _changingDisplay: !1,
            _origOrder: [],
            _startOrder: [],
            _newOrder: [],
            _activeFilter: null,
            _toggleArray: [],
            _toggleString: "",
            _activeSort: "default:asc",
            _newSort: null,
            _startHeight: null,
            _newHeight: null,
            _incPadding: !0,
            _newDisplay: null,
            _newClass: null,
            _targetsBound: 0,
            _targetsDone: 0,
            _queue: [],
            _$show: a(),
            _$hide: a()
        }), b._execAction("_constructor", 1)
    }, a.MixItUp.prototype = {
        constructor: a.MixItUp,
        _instances: {},
        _handled: {
            _filter: {},
            _sort: {}
        },
        _bound: {
            _filter: {},
            _sort: {}
        },
        _actions: {},
        _filters: {},
        extend: function(b) {
            for (var c in b) a.MixItUp.prototype[c] = b[c]
        },
        addAction: function(b, c, d, e) {
            a.MixItUp.prototype._addHook("_actions", b, c, d, e)
        },
        addFilter: function(b, c, d, e) {
            a.MixItUp.prototype._addHook("_filters", b, c, d, e)
        },
        _addHook: function(b, c, d, e, f) {
            var g = a.MixItUp.prototype[b],
                h = {};
            f = 1 === f || "post" === f ? "post" : "pre", h[c] = {}, h[c][f] = {}, h[c][f][d] = e, a.extend(!0, g, h)
        },
        _init: function(b, c) {
            var d = this;
            if (d._execAction("_init", 0, arguments), c && a.extend(!0, d, c), d._$body = a("body"), d._domNode = b, d._$container = a(b), d._$container.addClass(d.layout.containerClass), d._id = b.id, d._platformDetect(), d._brake = d._getPrefixedCSS("transition", "none"), d._refresh(!0), d._$parent = d._$targets.parent().length ? d._$targets.parent() : d._$container, d.load.sort && (d._newSort = d._parseSort(d.load.sort), d._newSortString = d.load.sort, d._activeSort = d.load.sort, d._sort(), d._printSort()), d._activeFilter = "all" === d.load.filter ? d.selectors.target : "none" === d.load.filter ? "" : d.load.filter, d.controls.enable && d._bindHandlers(), d.controls.toggleFilterButtons) {
                d._buildToggleArray();
                for (var e = 0; e < d._toggleArray.length; e++) d._updateControls({
                    filter: d._toggleArray[e],
                    sort: d._activeSort
                }, !0)
            } else d.controls.enable && d._updateControls({
                filter: d._activeFilter,
                sort: d._activeSort
            });
            d._filter(), d._init = !0, d._$container.data("mixItUp", d), d._execAction("_init", 1, arguments), d._buildState(), d._$targets.css(d._brake), d._goMix(d.animation.enable)
        },
        _platformDetect: function() {
            var a = this,
                c = ["Webkit", "Moz", "O", "ms"],
                d = ["webkit", "moz"],
                e = window.navigator.appVersion.match(/Chrome\/(\d+)\./) || !1,
                f = "undefined" != typeof InstallTrigger,
                g = function(a) {
                    for (var b = 0; b < c.length; b++)
                        if (c[b] + "Transition" in a.style) return {
                            prefix: "-" + c[b].toLowerCase() + "-",
                            vendor: c[b]
                        };
                    return "transition" in a.style ? "" : !1
                },
                h = g(a._domNode);
            a._execAction("_platformDetect", 0), a._chrome = e ? parseInt(e[1], 10) : !1, a._ff = f ? parseInt(window.navigator.userAgent.match(/rv:([^)]+)\)/)[1]) : !1, a._prefix = h.prefix, a._vendor = h.vendor, a._suckMode = window.atob && a._prefix ? !1 : !0, a._suckMode && (a.animation.enable = !1), a._ff && a._ff <= 4 && (a.animation.enable = !1);
            for (var i = 0; i < d.length && !window.requestAnimationFrame; i++) window.requestAnimationFrame = window[d[i] + "RequestAnimationFrame"];
            "function" != typeof Object.getPrototypeOf && ("object" == typeof "test".__proto__ ? Object.getPrototypeOf = function(a) {
                return a.__proto__
            } : Object.getPrototypeOf = function(a) {
                return a.constructor.prototype
            }), a._domNode.nextElementSibling === b && Object.defineProperty(Element.prototype, "nextElementSibling", {
                get: function() {
                    for (var a = this.nextSibling; a;) {
                        if (1 === a.nodeType) return a;
                        a = a.nextSibling
                    }
                    return null
                }
            }), a._execAction("_platformDetect", 1)
        },
        _refresh: function(a, c) {
            var d = this;
            d._execAction("_refresh", 0, arguments), d._$targets = d._$container.find(d.selectors.target);
            for (var e = 0; e < d._$targets.length; e++) {
                var f = d._$targets[e];
                if (f.dataset === b || c) {
                    f.dataset = {};
                    for (var g = 0; g < f.attributes.length; g++) {
                        var h = f.attributes[g],
                            i = h.name,
                            j = h.value;
                        if (i.indexOf("data-") > -1) {
                            var k = d._helpers._camelCase(i.substring(5, i.length));
                            f.dataset[k] = j
                        }
                    }
                }
                f.mixParent === b && (f.mixParent = d._id)
            }
            if (d._$targets.length && a || !d._origOrder.length && d._$targets.length) {
                d._origOrder = [];
                for (var e = 0; e < d._$targets.length; e++) {
                    var f = d._$targets[e];
                    d._origOrder.push(f)
                }
            }
            d._execAction("_refresh", 1, arguments)
        },
        _bindHandlers: function() {
            var c = this,
                d = a.MixItUp.prototype._bound._filter,
                e = a.MixItUp.prototype._bound._sort;
            c._execAction("_bindHandlers", 0), c.controls.live ? c._$body.on("click.mixItUp." + c._id, c.selectors.sort, function() {
                c._processClick(a(this), "sort")
            }).on("click.mixItUp." + c._id, c.selectors.filter, function() {
                c._processClick(a(this), "filter")
            }) : (c._$sortButtons = a(c.selectors.sort), c._$filterButtons = a(c.selectors.filter), c._$sortButtons.on("click.mixItUp." + c._id, function() {
                c._processClick(a(this), "sort")
            }), c._$filterButtons.on("click.mixItUp." + c._id, function() {
                c._processClick(a(this), "filter")
            })), d[c.selectors.filter] = d[c.selectors.filter] === b ? 1 : d[c.selectors.filter] + 1, e[c.selectors.sort] = e[c.selectors.sort] === b ? 1 : e[c.selectors.sort] + 1, c._execAction("_bindHandlers", 1)
        },
        _processClick: function(c, d) {
            var e = this,
                f = function(c, d, f) {
                    var g = a.MixItUp.prototype;
                    g._handled["_" + d][e.selectors[d]] = g._handled["_" + d][e.selectors[d]] === b ? 1 : g._handled["_" + d][e.selectors[d]] + 1, g._handled["_" + d][e.selectors[d]] === g._bound["_" + d][e.selectors[d]] && (c[(f ? "remove" : "add") + "Class"](e.controls.activeClass), delete g._handled["_" + d][e.selectors[d]])
                };
            if (e._execAction("_processClick", 0, arguments), !e._mixing || e.animation.queue && e._queue.length < e.animation.queueLimit) {
                if (e._clicking = !0, "sort" === d) {
                    var g = c.attr("data-sort");
                    (!c.hasClass(e.controls.activeClass) || g.indexOf("random") > -1) && (a(e.selectors.sort).removeClass(e.controls.activeClass), f(c, d), e.sort(g))
                }
                if ("filter" === d) {
                    var h, i = c.attr("data-filter"),
                        j = "or" === e.controls.toggleLogic ? "," : "";
                    e.controls.toggleFilterButtons ? (e._buildToggleArray(), c.hasClass(e.controls.activeClass) ? (f(c, d, !0), h = e._toggleArray.indexOf(i), e._toggleArray.splice(h, 1)) : (f(c, d), e._toggleArray.push(i)), e._toggleArray = a.grep(e._toggleArray, function(a) {
                        return a
                    }), e._toggleString = e._toggleArray.join(j), e.filter(e._toggleString)) : c.hasClass(e.controls.activeClass) || (a(e.selectors.filter).removeClass(e.controls.activeClass), f(c, d), e.filter(i))
                }
                e._execAction("_processClick", 1, arguments)
            } else "function" == typeof e.callbacks.onMixBusy && e.callbacks.onMixBusy.call(e._domNode, e._state, e), e._execAction("_processClickBusy", 1, arguments)
        },
        _buildToggleArray: function() {
            var a = this,
                b = a._activeFilter.replace(/\s/g, "");
            if (a._execAction("_buildToggleArray", 0, arguments), "or" === a.controls.toggleLogic) a._toggleArray = b.split(",");
            else {
                a._toggleArray = b.split("."), !a._toggleArray[0] && a._toggleArray.shift();
                for (var c, d = 0; c = a._toggleArray[d]; d++) a._toggleArray[d] = "." + c
            }
            a._execAction("_buildToggleArray", 1, arguments)
        },
        _updateControls: function(c, d) {
            var e = this,
                f = {
                    filter: c.filter,
                    sort: c.sort
                },
                g = function(a, b) {
                    try {
                        d && "filter" === h && "none" !== f.filter && "" !== f.filter ? a.filter(b).addClass(e.controls.activeClass) : a.removeClass(e.controls.activeClass).filter(b).addClass(e.controls.activeClass)
                    } catch (c) {}
                },
                h = "filter",
                i = null;
            e._execAction("_updateControls", 0, arguments), c.filter === b && (f.filter = e._activeFilter), c.sort === b && (f.sort = e._activeSort), f.filter === e.selectors.target && (f.filter = "all");
            for (var j = 0; 2 > j; j++) i = e.controls.live ? a(e.selectors[h]) : e["_$" + h + "Buttons"], i && g(i, "[data-" + h + '="' + f[h] + '"]'), h = "sort";
            e._execAction("_updateControls", 1, arguments)
        },
        _filter: function() {
            var b = this;
            b._execAction("_filter", 0);
            for (var c = 0; c < b._$targets.length; c++) {
                var d = a(b._$targets[c]);
                d.is(b._activeFilter) ? b._$show = b._$show.add(d) : b._$hide = b._$hide.add(d)
            }
            b._execAction("_filter", 1)
        },
        _sort: function() {
            var a = this,
                b = function(a) {
                    for (var b = a.slice(), c = b.length, d = c; d--;) {
                        var e = parseInt(Math.random() * c),
                            f = b[d];
                        b[d] = b[e], b[e] = f
                    }
                    return b
                };
            a._execAction("_sort", 0), a._startOrder = [];
            for (var c = 0; c < a._$targets.length; c++) {
                var d = a._$targets[c];
                a._startOrder.push(d)
            }
            switch (a._newSort[0].sortBy) {
                case "default":
                    a._newOrder = a._origOrder;
                    break;
                case "random":
                    a._newOrder = b(a._startOrder);
                    break;
                case "custom":
                    a._newOrder = a._newSort[0].order;
                    break;
                default:
                    a._newOrder = a._startOrder.concat().sort(function(b, c) {
                        return a._compare(b, c)
                    })
            }
            a._execAction("_sort", 1)
        },
        _compare: function(a, b, c) {
            c = c ? c : 0;
            var d = this,
                e = d._newSort[c].order,
                f = function(a) {
                    return a.dataset[d._newSort[c].sortBy] || 0
                },
                g = isNaN(1 * f(a)) ? f(a).toLowerCase() : 1 * f(a),
                h = isNaN(1 * f(b)) ? f(b).toLowerCase() : 1 * f(b);
            return h > g ? "asc" === e ? -1 : 1 : g > h ? "asc" === e ? 1 : -1 : g === h && d._newSort.length > c + 1 ? d._compare(a, b, c + 1) : 0
        },
        _printSort: function(a) {
            var b = this,
                c = a ? b._startOrder : b._newOrder,
                d = b._$parent[0].querySelectorAll(b.selectors.target),
                e = d.length ? d[d.length - 1].nextElementSibling : null,
                f = document.createDocumentFragment();
            b._execAction("_printSort", 0, arguments);
            for (var g = 0; g < d.length; g++) {
                var h = d[g],
                    i = h.nextSibling;
                "absolute" !== h.style.position && (i && "#text" === i.nodeName && b._$parent[0].removeChild(i), b._$parent[0].removeChild(h))
            }
            for (var g = 0; g < c.length; g++) {
                var j = c[g];
                if ("default" !== b._newSort[0].sortBy || "desc" !== b._newSort[0].order || a) f.appendChild(j), f.appendChild(document.createTextNode(" "));
                else {
                    var k = f.firstChild;
                    f.insertBefore(j, k), f.insertBefore(document.createTextNode(" "), j)
                }
            }
            e ? b._$parent[0].insertBefore(f, e) : b._$parent[0].appendChild(f), b._execAction("_printSort", 1, arguments)
        },
        _parseSort: function(a) {
            for (var b = this, c = "string" == typeof a ? a.split(" ") : [a], d = [], e = 0; e < c.length; e++) {
                var f = "string" == typeof a ? c[e].split(":") : ["custom", c[e]],
                    g = {
                        sortBy: b._helpers._camelCase(f[0]),
                        order: f[1] || "asc"
                    };
                if (d.push(g), "default" === g.sortBy || "random" === g.sortBy) break
            }
            return b._execFilter("_parseSort", d, arguments)
        },
        _parseEffects: function() {
            var a = this,
                b = {
                    opacity: "",
                    transformIn: "",
                    transformOut: "",
                    filter: ""
                },
                c = function(b, c, d) {
                    if (a.animation.effects.indexOf(b) > -1) {
                        if (c) {
                            var e = a.animation.effects.indexOf(b + "(");
                            if (e > -1) {
                                var f = a.animation.effects.substring(e),
                                    g = /\(([^)]+)\)/.exec(f),
                                    h = g[1];
                                return {
                                    val: h
                                }
                            }
                        }
                        return !0
                    }
                    return !1
                },
                d = function(a, b) {
                    return b ? "-" === a.charAt(0) ? a.substr(1, a.length) : "-" + a : a
                },
                e = function(a, e) {
                    for (var f = [
                            ["scale", ".01"],
                            ["translateX", "20px"],
                            ["translateY", "20px"],
                            ["translateZ", "20px"],
                            ["rotateX", "90deg"],
                            ["rotateY", "90deg"],
                            ["rotateZ", "180deg"]
                        ], g = 0; g < f.length; g++) {
                        var h = f[g][0],
                            i = f[g][1],
                            j = e && "scale" !== h;
                        b[a] += c(h) ? h + "(" + d(c(h, !0).val || i, j) + ") " : ""
                    }
                };
            return b.opacity = c("fade") ? c("fade", !0).val || "0" : "1", e("transformIn"), a.animation.reverseOut ? e("transformOut", !0) : b.transformOut = b.transformIn, b.transition = {}, b.transition = a._getPrefixedCSS("transition", "all " + a.animation.duration + "ms " + a.animation.easing + ", opacity " + a.animation.duration + "ms linear"), a.animation.stagger = c("stagger") ? !0 : !1, a.animation.staggerDuration = parseInt(c("stagger") && c("stagger", !0).val ? c("stagger", !0).val : 100), a._execFilter("_parseEffects", b)
        },
        _buildState: function(a) {
            var b = this,
                c = {};
            return b._execAction("_buildState", 0), c = {
                activeFilter: "" === b._activeFilter ? "none" : b._activeFilter,
                activeSort: a && b._newSortString ? b._newSortString : b._activeSort,
                fail: !b._$show.length && "" !== b._activeFilter,
                $targets: b._$targets,
                $show: b._$show,
                $hide: b._$hide,
                totalTargets: b._$targets.length,
                totalShow: b._$show.length,
                totalHide: b._$hide.length,
                display: a && b._newDisplay ? b._newDisplay : b.layout.display
            }, a ? b._execFilter("_buildState", c) : (b._state = c, void b._execAction("_buildState", 1))
        },
        _goMix: function(a) {
            var b = this,
                c = function() {
                    b._chrome && 31 === b._chrome && f(b._$parent[0]), b._setInter(), d()
                },
                d = function() {
                    var a = window.pageYOffset,
                        c = window.pageXOffset;
                    document.documentElement.scrollHeight;
                    b._getInterMixData(), b._setFinal(), b._getFinalMixData(), window.pageYOffset !== a && window.scrollTo(c, a), b._prepTargets(), window.requestAnimationFrame ? requestAnimationFrame(e) : setTimeout(function() {
                        e()
                    }, 20)
                },
                e = function() {
                    b._animateTargets(), 0 === b._targetsBound && b._cleanUp()
                },
                f = function(a) {
                    var b = a.parentElement,
                        c = document.createElement("div"),
                        d = document.createDocumentFragment();
                    b.insertBefore(c, a), d.appendChild(a), b.replaceChild(a, c)
                },
                g = b._buildState(!0);
            b._execAction("_goMix", 0, arguments), !b.animation.duration && (a = !1), b._mixing = !0, b._$container.removeClass(b.layout.containerClassFail), "function" == typeof b.callbacks.onMixStart && b.callbacks.onMixStart.call(b._domNode, b._state, g, b), b._$container.trigger("mixStart", [b._state, g, b]), b._getOrigMixData(), a && !b._suckMode ? window.requestAnimationFrame ? requestAnimationFrame(c) : c() : b._cleanUp(), b._execAction("_goMix", 1, arguments)
        },
        _getTargetData: function(a, b) {
            var c, d = this;
            a.dataset[b + "PosX"] = a.offsetLeft, a.dataset[b + "PosY"] = a.offsetTop, d.animation.animateResizeTargets && (c = d._suckMode ? {
                marginBottom: "",
                marginRight: ""
            } : window.getComputedStyle(a), a.dataset[b + "MarginBottom"] = parseInt(c.marginBottom), a.dataset[b + "MarginRight"] = parseInt(c.marginRight), a.dataset[b + "Width"] = a.offsetWidth, a.dataset[b + "Height"] = a.offsetHeight)
        },
        _getOrigMixData: function() {
            var a = this,
                b = a._suckMode ? {
                    boxSizing: ""
                } : window.getComputedStyle(a._$parent[0]),
                c = b.boxSizing || b[a._vendor + "BoxSizing"];
            a._incPadding = "border-box" === c, a._execAction("_getOrigMixData", 0), !a._suckMode && (a.effects = a._parseEffects()), a._$toHide = a._$hide.filter(":visible"), a._$toShow = a._$show.filter(":hidden"), a._$pre = a._$targets.filter(":visible"), a._startHeight = a._incPadding ? a._$parent.outerHeight() : a._$parent.height();
            for (var d = 0; d < a._$pre.length; d++) {
                var e = a._$pre[d];
                a._getTargetData(e, "orig")
            }
            a._execAction("_getOrigMixData", 1)
        },
        _setInter: function() {
            var a = this;
            a._execAction("_setInter", 0), a._changingLayout && a.animation.animateChangeLayout ? (a._$toShow.css("display", a._newDisplay), a._changingClass && a._$container.removeClass(a.layout.containerClass).addClass(a._newClass)) : a._$toShow.css("display", a.layout.display), a._execAction("_setInter", 1)
        },
        _getInterMixData: function() {
            var a = this;
            a._execAction("_getInterMixData", 0);
            for (var b = 0; b < a._$toShow.length; b++) {
                var c = a._$toShow[b];
                a._getTargetData(c, "inter")
            }
            for (var b = 0; b < a._$pre.length; b++) {
                var c = a._$pre[b];
                a._getTargetData(c, "inter")
            }
            a._execAction("_getInterMixData", 1)
        },
        _setFinal: function() {
            var a = this;
            a._execAction("_setFinal", 0), a._sorting && a._printSort(), a._$toHide.removeStyle("display"), a._changingLayout && a.animation.animateChangeLayout && a._$pre.css("display", a._newDisplay), a._execAction("_setFinal", 1)
        },
        _getFinalMixData: function() {
            var a = this;
            a._execAction("_getFinalMixData", 0);
            for (var b = 0; b < a._$toShow.length; b++) {
                var c = a._$toShow[b];
                a._getTargetData(c, "final")
            }
            for (var b = 0; b < a._$pre.length; b++) {
                var c = a._$pre[b];
                a._getTargetData(c, "final")
            }
            a._newHeight = a._incPadding ? a._$parent.outerHeight() : a._$parent.height(), a._sorting && a._printSort(!0), a._$toShow.removeStyle("display"), a._$pre.css("display", a.layout.display), a._changingClass && a.animation.animateChangeLayout && a._$container.removeClass(a._newClass).addClass(a.layout.containerClass), a._execAction("_getFinalMixData", 1)
        },
        _prepTargets: function() {
            var b = this,
                c = {
                    _in: b._getPrefixedCSS("transform", b.effects.transformIn),
                    _out: b._getPrefixedCSS("transform", b.effects.transformOut)
                };
            b._execAction("_prepTargets", 0), b.animation.animateResizeContainer && b._$parent.css("height", b._startHeight + "px");
            for (var d = 0; d < b._$toShow.length; d++) {
                var e = b._$toShow[d],
                    f = a(e);
                e.style.opacity = b.effects.opacity, e.style.display = b._changingLayout && b.animation.animateChangeLayout ? b._newDisplay : b.layout.display, f.css(c._in), b.animation.animateResizeTargets && (e.style.width = e.dataset.finalWidth + "px", e.style.height = e.dataset.finalHeight + "px", e.style.marginRight = -(e.dataset.finalWidth - e.dataset.interWidth) + 1 * e.dataset.finalMarginRight + "px", e.style.marginBottom = -(e.dataset.finalHeight - e.dataset.interHeight) + 1 * e.dataset.finalMarginBottom + "px")
            }
            for (var d = 0; d < b._$pre.length; d++) {
                var e = b._$pre[d],
                    f = a(e),
                    g = {
                        x: e.dataset.origPosX - e.dataset.interPosX,
                        y: e.dataset.origPosY - e.dataset.interPosY
                    },
                    c = b._getPrefixedCSS("transform", "translate(" + g.x + "px," + g.y + "px)");
                f.css(c), b.animation.animateResizeTargets && (e.style.width = e.dataset.origWidth + "px", e.style.height = e.dataset.origHeight + "px", e.dataset.origWidth - e.dataset.finalWidth && (e.style.marginRight = -(e.dataset.origWidth - e.dataset.interWidth) + 1 * e.dataset.origMarginRight + "px"), e.dataset.origHeight - e.dataset.finalHeight && (e.style.marginBottom = -(e.dataset.origHeight - e.dataset.interHeight) + 1 * e.dataset.origMarginBottom + "px"))
            }
            b._execAction("_prepTargets", 1)
        },
        _animateTargets: function() {
            var b = this;
            b._execAction("_animateTargets", 0), b._targetsDone = 0, b._targetsBound = 0, b._$parent.css(b._getPrefixedCSS("perspective", b.animation.perspectiveDistance + "px")).css(b._getPrefixedCSS("perspective-origin", b.animation.perspectiveOrigin)), b.animation.animateResizeContainer && b._$parent.css(b._getPrefixedCSS("transition", "height " + b.animation.duration + "ms ease")).css("height", b._newHeight + "px");
            for (var c = 0; c < b._$toShow.length; c++) {
                var d = b._$toShow[c],
                    e = a(d),
                    f = {
                        x: d.dataset.finalPosX - d.dataset.interPosX,
                        y: d.dataset.finalPosY - d.dataset.interPosY
                    },
                    g = b._getDelay(c),
                    h = {};
                d.style.opacity = "";
                for (var i = 0; 2 > i; i++) {
                    var j = 0 === i ? j = b._prefix : "";
                    b._ff && b._ff <= 20 && (h[j + "transition-property"] = "all", h[j + "transition-timing-function"] = b.animation.easing + "ms", h[j + "transition-duration"] = b.animation.duration + "ms"), h[j + "transition-delay"] = g + "ms", h[j + "transform"] = "translate(" + f.x + "px," + f.y + "px)"
                }(b.effects.transform || b.effects.opacity) && b._bindTargetDone(e), b._ff && b._ff <= 20 ? e.css(h) : e.css(b.effects.transition).css(h)
            }
            for (var c = 0; c < b._$pre.length; c++) {
                var d = b._$pre[c],
                    e = a(d),
                    f = {
                        x: d.dataset.finalPosX - d.dataset.interPosX,
                        y: d.dataset.finalPosY - d.dataset.interPosY
                    },
                    g = b._getDelay(c);
                (d.dataset.finalPosX !== d.dataset.origPosX || d.dataset.finalPosY !== d.dataset.origPosY) && b._bindTargetDone(e), e.css(b._getPrefixedCSS("transition", "all " + b.animation.duration + "ms " + b.animation.easing + " " + g + "ms")), e.css(b._getPrefixedCSS("transform", "translate(" + f.x + "px," + f.y + "px)")), b.animation.animateResizeTargets && (d.dataset.origWidth - d.dataset.finalWidth && 1 * d.dataset.finalWidth && (d.style.width = d.dataset.finalWidth + "px", d.style.marginRight = -(d.dataset.finalWidth - d.dataset.interWidth) + 1 * d.dataset.finalMarginRight + "px"), d.dataset.origHeight - d.dataset.finalHeight && 1 * d.dataset.finalHeight && (d.style.height = d.dataset.finalHeight + "px", d.style.marginBottom = -(d.dataset.finalHeight - d.dataset.interHeight) + 1 * d.dataset.finalMarginBottom + "px"))
            }
            b._changingClass && b._$container.removeClass(b.layout.containerClass).addClass(b._newClass);
            for (var c = 0; c < b._$toHide.length; c++) {
                for (var d = b._$toHide[c], e = a(d), g = b._getDelay(c), k = {}, i = 0; 2 > i; i++) {
                    var j = 0 === i ? j = b._prefix : "";
                    k[j + "transition-delay"] = g + "ms", k[j + "transform"] = b.effects.transformOut, k.opacity = b.effects.opacity
                }
                e.css(b.effects.transition).css(k), (b.effects.transform || b.effects.opacity) && b._bindTargetDone(e)
            }
            b._execAction("_animateTargets", 1)
        },
        _bindTargetDone: function(b) {
            var c = this,
                d = b[0];
            c._execAction("_bindTargetDone", 0, arguments), d.dataset.bound || (d.dataset.bound = !0, c._targetsBound++, b.on("webkitTransitionEnd.mixItUp transitionend.mixItUp", function(e) {
                (e.originalEvent.propertyName.indexOf("transform") > -1 || e.originalEvent.propertyName.indexOf("opacity") > -1) && a(e.originalEvent.target).is(c.selectors.target) && (b.off(".mixItUp"), d.dataset.bound = "", c._targetDone())
            })), c._execAction("_bindTargetDone", 1, arguments)
        },
        _targetDone: function() {
            var a = this;
            a._execAction("_targetDone", 0), a._targetsDone++, a._targetsDone === a._targetsBound && a._cleanUp(), a._execAction("_targetDone", 1)
        },
        _cleanUp: function() {
            var b = this,
                c = b.animation.animateResizeTargets ? "transform opacity width height margin-bottom margin-right" : "transform opacity",
                d = function() {
                    b._$targets.removeStyle("transition", b._prefix)
                };
            b._execAction("_cleanUp", 0), b._changingLayout ? b._$show.css("display", b._newDisplay) : b._$show.css("display", b.layout.display), b._$targets.css(b._brake), b._$targets.removeStyle(c, b._prefix).removeAttr("data-inter-pos-x data-inter-pos-y data-final-pos-x data-final-pos-y data-orig-pos-x data-orig-pos-y data-orig-height data-orig-width data-final-height data-final-width data-inter-width data-inter-height data-orig-margin-right data-orig-margin-bottom data-inter-margin-right data-inter-margin-bottom data-final-margin-right data-final-margin-bottom"), b._$hide.removeStyle("display"), b._$parent.removeStyle("height transition perspective-distance perspective perspective-origin-x perspective-origin-y perspective-origin perspectiveOrigin", b._prefix), b._sorting && (b._printSort(), b._activeSort = b._newSortString, b._sorting = !1), b._changingLayout && (b._changingDisplay && (b.layout.display = b._newDisplay, b._changingDisplay = !1), b._changingClass && (b._$parent.removeClass(b.layout.containerClass).addClass(b._newClass), b.layout.containerClass = b._newClass, b._changingClass = !1), b._changingLayout = !1), b._refresh(), b._buildState(), b._state.fail && b._$container.addClass(b.layout.containerClassFail), b._$show = a(), b._$hide = a(), window.requestAnimationFrame && requestAnimationFrame(d), b._mixing = !1, "function" == typeof b.callbacks._user && b.callbacks._user.call(b._domNode, b._state, b), "function" == typeof b.callbacks.onMixEnd && b.callbacks.onMixEnd.call(b._domNode, b._state, b), b._$container.trigger("mixEnd", [b._state, b]), b._state.fail && ("function" == typeof b.callbacks.onMixFail && b.callbacks.onMixFail.call(b._domNode, b._state, b), b._$container.trigger("mixFail", [b._state, b])), b._loading && ("function" == typeof b.callbacks.onMixLoad && b.callbacks.onMixLoad.call(b._domNode, b._state, b), b._$container.trigger("mixLoad", [b._state, b])), b._queue.length && (b._execAction("_queue", 0), b.multiMix(b._queue[0][0], b._queue[0][1], b._queue[0][2]), b._queue.splice(0, 1)), b._execAction("_cleanUp", 1), b._loading = !1
        },
        _getPrefixedCSS: function(a, b, c) {
            var d = this,
                e = {},
                f = "",
                g = -1;
            for (g = 0; 2 > g; g++) f = 0 === g ? d._prefix : "", c ? e[f + a] = f + b : e[f + a] = b;
            return d._execFilter("_getPrefixedCSS", e, arguments)
        },
        _getDelay: function(a) {
            var b = this,
                c = "function" == typeof b.animation.staggerSequence ? b.animation.staggerSequence.call(b._domNode, a, b._state) : a,
                d = b.animation.stagger ? c * b.animation.staggerDuration : 0;
            return b._execFilter("_getDelay", d, arguments)
        },
        _parseMultiMixArgs: function(a) {
            for (var b = this, c = {
                    command: null,
                    animate: b.animation.enable,
                    callback: null
                }, d = 0; d < a.length; d++) {
                var e = a[d];
                null !== e && ("object" == typeof e || "string" == typeof e ? c.command = e : "boolean" == typeof e ? c.animate = e : "function" == typeof e && (c.callback = e))
            }
            return b._execFilter("_parseMultiMixArgs", c, arguments)
        },
        _parseInsertArgs: function(b) {
            for (var c = this, d = {
                    index: 0,
                    $object: a(),
                    multiMix: {
                        filter: c._state.activeFilter
                    },
                    callback: null
                }, e = 0; e < b.length; e++) {
                var f = b[e];
                "number" == typeof f ? d.index = f : "object" == typeof f && f instanceof a ? d.$object = f : "object" == typeof f && c._helpers._isElement(f) ? d.$object = a(f) : "object" == typeof f && null !== f ? d.multiMix = f : "boolean" != typeof f || f ? "function" == typeof f && (d.callback = f) : d.multiMix = !1
            }
            return c._execFilter("_parseInsertArgs", d, arguments)
        },
        _execAction: function(a, b, c) {
            var d = this,
                e = b ? "post" : "pre";
            if (!d._actions.isEmptyObject && d._actions.hasOwnProperty(a))
                for (var f in d._actions[a][e]) d._actions[a][e][f].call(d, c)
        },
        _execFilter: function(a, b, c) {
            var d = this;
            if (d._filters.isEmptyObject || !d._filters.hasOwnProperty(a)) return b;
            for (var e in d._filters[a]) return d._filters[a][e].call(d, c)
        },
        _helpers: {
            _camelCase: function(a) {
                return a.replace(/-([a-z])/g, function(a) {
                    return a[1].toUpperCase()
                })
            },
            _isElement: function(a) {
                return window.HTMLElement ? a instanceof HTMLElement : null !== a && 1 === a.nodeType && "string" === a.nodeName
            }
        },
        isMixing: function() {
            var a = this;
            return a._execFilter("isMixing", a._mixing)
        },
        filter: function() {
            var a = this,
                b = a._parseMultiMixArgs(arguments);
            a._clicking && (a._toggleString = ""), a.multiMix({
                filter: b.command
            }, b.animate, b.callback)
        },
        sort: function() {
            var a = this,
                b = a._parseMultiMixArgs(arguments);
            a.multiMix({
                sort: b.command
            }, b.animate, b.callback)
        },
        changeLayout: function() {
            var a = this,
                b = a._parseMultiMixArgs(arguments);
            a.multiMix({
                changeLayout: b.command
            }, b.animate, b.callback)
        },
        multiMix: function() {
            var a = this,
                c = a._parseMultiMixArgs(arguments);
            if (a._execAction("multiMix", 0, arguments), a._mixing) a.animation.queue && a._queue.length < a.animation.queueLimit ? (a._queue.push(arguments), a.controls.enable && !a._clicking && a._updateControls(c.command), a._execAction("multiMixQueue", 1, arguments)) : ("function" == typeof a.callbacks.onMixBusy && a.callbacks.onMixBusy.call(a._domNode, a._state, a), a._$container.trigger("mixBusy", [a._state, a]), a._execAction("multiMixBusy", 1, arguments));
            else {
                a.controls.enable && !a._clicking && (a.controls.toggleFilterButtons && a._buildToggleArray(), a._updateControls(c.command, a.controls.toggleFilterButtons)), a._queue.length < 2 && (a._clicking = !1), delete a.callbacks._user, c.callback && (a.callbacks._user = c.callback);
                var d = c.command.sort,
                    e = c.command.filter,
                    f = c.command.changeLayout;
                a._refresh(), d && (a._newSort = a._parseSort(d), a._newSortString = d, a._sorting = !0, a._sort()), e !== b && (e = "all" === e ? a.selectors.target : e, a._activeFilter = e), a._filter(), f && (a._newDisplay = "string" == typeof f ? f : f.display || a.layout.display, a._newClass = f.containerClass || "", (a._newDisplay !== a.layout.display || a._newClass !== a.layout.containerClass) && (a._changingLayout = !0, a._changingClass = a._newClass !== a.layout.containerClass, a._changingDisplay = a._newDisplay !== a.layout.display)), a._$targets.css(a._brake), a._goMix(c.animate ^ a.animation.enable ? c.animate : a.animation.enable), a._execAction("multiMix", 1, arguments)
            }
        },
        insert: function() {
            var a = this,
                b = a._parseInsertArgs(arguments),
                c = "function" == typeof b.callback ? b.callback : null,
                d = document.createDocumentFragment(),
                e = function() {
                    return a._refresh(), a._$targets.length ? b.index < a._$targets.length || !a._$targets.length ? a._$targets[b.index] : a._$targets[a._$targets.length - 1].nextElementSibling : a._$parent[0].children[0]
                }();
            if (a._execAction("insert", 0, arguments), b.$object) {
                for (var f = 0; f < b.$object.length; f++) {
                    var g = b.$object[f];
                    d.appendChild(g), d.appendChild(document.createTextNode(" "))
                }
                a._$parent[0].insertBefore(d, e)
            }
            a._execAction("insert", 1, arguments), "object" == typeof b.multiMix && a.multiMix(b.multiMix, c)
        },
        prepend: function() {
            var a = this,
                b = a._parseInsertArgs(arguments);
            a.insert(0, b.$object, b.multiMix, b.callback)
        },
        append: function() {
            var a = this,
                b = a._parseInsertArgs(arguments);
            a.insert(a._state.totalTargets, b.$object, b.multiMix, b.callback)
        },
        getOption: function(a) {
            var c = this,
                d = function(a, c) {
                    for (var d = c.split("."), e = d.pop(), f = d.length, g = 1, h = d[0] || c;
                        (a = a[h]) && f > g;) h = d[g], g++;
                    return a !== b ? a[e] !== b ? a[e] : a : void 0
                };
            return a ? c._execFilter("getOption", d(c, a), arguments) : c
        },
        setOptions: function(b) {
            var c = this;
            c._execAction("setOptions", 0, arguments), "object" == typeof b && a.extend(!0, c, b), c._execAction("setOptions", 1, arguments)
        },
        getState: function() {
            var a = this;
            return a._execFilter("getState", a._state, a)
        },
        forceRefresh: function() {
            var a = this;
            a._refresh(!1, !0)
        },
        destroy: function(b) {
            var c = this,
                d = a.MixItUp.prototype._bound._filter,
                e = a.MixItUp.prototype._bound._sort;
            c._execAction("destroy", 0, arguments), c._$body.add(a(c.selectors.sort)).add(a(c.selectors.filter)).off(".mixItUp");
            for (var f = 0; f < c._$targets.length; f++) {
                var g = c._$targets[f];
                b && (g.style.display = ""), delete g.mixParent
            }
            c._execAction("destroy", 1, arguments), d[c.selectors.filter] && d[c.selectors.filter] > 1 ? d[c.selectors.filter]-- : 1 === d[c.selectors.filter] && delete d[c.selectors.filter], e[c.selectors.sort] && e[c.selectors.sort] > 1 ? e[c.selectors.sort]-- : 1 === e[c.selectors.sort] && delete e[c.selectors.sort], delete a.MixItUp.prototype._instances[c._id]
        }
    }, a.fn.mixItUp = function() {
        var c, d = arguments,
            e = [],
            f = function(b, c) {
                var d = new a.MixItUp,
                    e = function() {
                        return ("00000" + (16777216 * Math.random() << 0).toString(16)).substr(-6).toUpperCase()
                    };
                d._execAction("_instantiate", 0, arguments), b.id = b.id ? b.id : "MixItUp" + e(), d._instances[b.id] || (d._instances[b.id] = d, d._init(b, c)), d._execAction("_instantiate", 1, arguments)
            };
        return c = this.each(function() {
            if (d && "string" == typeof d[0]) {
                var c = a.MixItUp.prototype._instances[this.id];
                if ("isLoaded" === d[0]) e.push(c ? !0 : !1);
                else {
                    var g = c[d[0]](d[1], d[2], d[3]);
                    g !== b && e.push(g)
                }
            } else f(this, d[0])
        }), e.length ? e.length > 1 ? e : e[0] : c
    }, a.fn.removeStyle = function(c, d) {
        return d = d ? d : "", this.each(function() {
            for (var e = this, f = c.split(" "), g = 0; g < f.length; g++)
                for (var h = 0; 4 > h; h++) {
                    switch (h) {
                        case 0:
                            var i = f[g];
                            break;
                        case 1:
                            var i = a.MixItUp.prototype._helpers._camelCase(i);
                            break;
                        case 2:
                            var i = d + f[g];
                            break;
                        case 3:
                            var i = a.MixItUp.prototype._helpers._camelCase(d + f[g])
                    }
                    if (e.style[i] !== b && "unknown" != typeof e.style[i] && e.style[i].length > 0 && (e.style[i] = ""), !d && 1 === h) break
                }
            e.attributes && e.attributes.style && e.attributes.style !== b && "" === e.attributes.style.value && e.attributes.removeNamedItem("style")
        })
    }
}(jQuery);
(function($, undf) {
    $.MixItUp.prototype.addAction('_constructor', 'pagination', function() {
        var self = this;
        self.pagination = {
            limit: 0,
            loop: false,
            generatePagers: true,
            maxPagers: 5,
            pagerClass: '',
            prevButtonHTML: '&laquo;',
            nextButtonHTML: '&raquo;'
        };
        $.extend(self.selectors, {
            pagersWrapper: '.pager-list',
            pager: '.pager'
        });
        $.extend(self.load, {
            page: 1
        });
        self._activePage = null;
        self._totalPages = null;
        self._$pagersWrapper = $();
    }, 1);
    $.MixItUp.prototype.addAction('_init', 'pagination', function() {
        var self = this;
        self._activePage = self.load.page * 1;
        self.pagination.maxPagers = (typeof self.pagination.maxPagers === 'number' && self.pagination.maxPagers < 5) ? 5 : self.pagination.maxPagers;
    }, 1);
    $.MixItUp.prototype.addAction('_bindHandlers', 'pagination', function() {
        var self = this;
        if (self.pagination.generatePagers) {
            self._$pagersWrapper = $(self.selectors.pagersWrapper);
        };
        if (self.controls.live) {
            self._$body.on('click.mixItUp.' + self._id, self.selectors.pager, function() {
                self._processClick($(this), 'page');
            });
        } else {
            self._$pagersWrapper.on('click.mixItUp.' + self._id, self.selectors.pager, function() {;
                self._processClick($(this), 'page');
            });
        }
    }, 1);
    $.MixItUp.prototype.addAction('_processClick', 'pagination', function(args) {
        var self = this,
            pageNumber = null,
            $button = args[0],
            type = args[1];
        if (type === 'page') {
            pageNumber = $button.attr('data-page') || false;
            if (pageNumber === 'prev') {
                pageNumber = self._getPrevPage();
            } else if (pageNumber === 'next') {
                pageNumber = self._getNextPage();
            } else if (pageNumber) {
                pageNumber = pageNumber * 1;
            } else {
                return false;
            }
            if (!$button.hasClass(self.controls.activeClass)) {
                self.paginate(pageNumber);
            }
        }
    }, 1);
    $.MixItUp.prototype.addAction('_buildState', 'pagination', function() {
        var self = this;
        $.extend(self._state, {
            limit: self.pagination.limit,
            activePage: self._activePage,
            totalPages: self._totalPages
        });
    }, 1);
    $.MixItUp.prototype.addAction('_sort', 'pagination', function() {
        var self = this;
        if (self.pagination.limit > 0) {
            self._printSort();
        }
    }, 1);
    $.MixItUp.prototype.addAction('_filter', 'pagination', function() {
        var self = this,
            startPageAt = self.pagination.limit * (self.load.page - 1),
            endPageAt = (self.pagination.limit * self.load.page) - 1,
            $inPage = null,
            $notInPage = null;
        self._activePage = self.load.page;
        self._totalPages = self.pagination.limit ? Math.ceil(self._$show.length / self.pagination.limit) : 1;
        if (self.pagination.limit > 0) {
            $inPage = self._$show.filter(function(index) {
                return index >= startPageAt && index <= endPageAt;
            });
            $notInPage = self._$show.filter(function(index) {
                return index < startPageAt || index > endPageAt;
            });
            self._$show = $inPage;
            self._$hide = self._$hide.add($notInPage);
            if (self._sorting) {
                self._printSort(true);
            }
        }
        if (self.pagination.generatePagers && self._$pagersWrapper.length) {
            self._generatePagers();
        };
    }, 1);
    $.MixItUp.prototype.addAction('multiMix', 'pagination', function(args) {
        var self = this,
            args = self._parseMultiMixArgs(args);
        if (args.command.paginate !== undf) {
            typeof args.command.paginate === 'object' ? $.extend(self.pagination, args.command.paginate) : self.load.page = args.command.paginate;
        } else if (args.command.filter !== undf || args.command.sort !== undf) {
            self.load.page = 1;
        }
    }, 0);
    $.MixItUp.prototype.addAction('destroy', 'pagination', function() {
        var self = this;
        self._$pagersWrapper.off('.mixItUp').html('');
    }, 1);
    $.MixItUp.prototype.extend({
        _getNextPage: function() {
            var self = this,
                page = self._activePage + 1,
                page = self._activePage < self._totalPages ? page : self.pagination.loop ? 1 : self._activePage;
            return self._execFilter('_getNextPage', page * 1);
        },
        _getPrevPage: function() {
            var self = this,
                page = self._activePage - 1,
                page = self._activePage > 1 ? page : self.pagination.loop ? self._totalPages : self._activePage;
            return self._execFilter('_getPrevPage', page * 1);
        },
        _generatePagers: function() {
            var self = this,
                pagerTag = self._$pagersWrapper[0].nodeName === 'UL' ? 'li' : 'span',
                pagerClass = self.pagination.pagerClass ? self.pagination.pagerClass + ' ' : '',
                prevButtonHTML = '<' + pagerTag + ' class="' + pagerClass + 'pager page-prev" data-page="prev"><span>' + self.pagination.prevButtonHTML + '</span></' + pagerTag + '>',
                prevButtonHTML = (self._activePage > 1) ? prevButtonHTML : self.pagination.loop ? prevButtonHTML : '<' + pagerTag + ' class="' + pagerClass + 'pager page-prev disabled"><span>' + self.pagination.prevButtonHTML + '</span></' + pagerTag + '>';
            nextButtonHTML = '<' + pagerTag + ' class="' + pagerClass + 'pager page-next" data-page="next"><span>' + self.pagination.nextButtonHTML + '</span></' + pagerTag + '>', nextButtonHTML = (self._activePage < self._totalPages) ? nextButtonHTML : self.pagination.loop ? nextButtonHTML : '<' + pagerTag + ' class="' + pagerClass + 'pager page-next disabled"><span>' + self.pagination.nextButtonHTML + '</span></' + pagerTag + '>';
            totalButtons = (self.pagination.maxPagers !== false && self._totalPages > self.pagination.maxPagers) ? self.pagination.maxPagers : self._totalPages, pagerButtonsHTML = '', pagersHTML = '', wrapperClass = '';
            self._execAction('_generatePagers', 0);
            for (var i = 0; i < totalButtons; i++) {
                var pagerNumber = null,
                    classes = '';
                if (i === 0) {
                    pagerNumber = 1;
                    if (self.pagination.maxPagers !== false && self._activePage > (self.pagination.maxPagers - 2) && self._totalPages > self.pagination.maxPagers) {
                        classes = ' page-first';
                    }
                } else {
                    if (self.pagination.maxPagers === false || totalButtons < self.pagination.maxPagers) {
                        pagerNumber = i + 1;
                    } else {
                        if (i === self.pagination.maxPagers - 1) {
                            pagerNumber = self._totalPages;
                            if (self._activePage < self._totalPages - 2 && self._totalPages > self.pagination.maxPagers) {
                                classes = ' page-last';
                            }
                        } else {
                            if (self._activePage > self.pagination.maxPagers - 2 && self._activePage < self._totalPages - 2) {
                                pagerNumber = self._activePage - (2 - i);
                            } else if (self._activePage < self.pagination.maxPagers - 1) {
                                pagerNumber = i + 1;
                            } else if (self._activePage >= self._totalPages - 2) {
                                pagerNumber = self._totalPages - (self.pagination.maxPagers - 1 - i);
                            }
                        }
                    }
                }
                classes = (pagerNumber == self._activePage) ? classes + ' ' + self.controls.activeClass : classes;
                pagerButtonsHTML += '<' + pagerTag + ' class="' + pagerClass + 'pager page-number' + classes + '" data-page="' + pagerNumber + '"><span>' + pagerNumber + '</span></' + pagerTag + '> ';
            }
            pagersHTML = self._totalPages > 1 ? prevButtonHTML + ' ' + pagerButtonsHTML + ' ' + nextButtonHTML : '';
            wrapperClass = self._totalPages > 1 ? '' : 'no-pagers';
            self._$pagersWrapper.html(pagersHTML).removeClass('no-pagers').addClass(wrapperClass);
            self._execAction('_generatePagers', 1);
        },
        _parsePaginateArgs: function(args) {
            var self = this,
                output = {
                    command: null,
                    animate: self.animation.enable,
                    callback: null
                };
            for (var i = 0; i < args.length; i++) {
                var arg = args[i];
                if (arg !== null) {
                    if (typeof arg === 'object' || typeof arg === 'number') {
                        output.command = arg;
                    } else if (typeof arg === 'boolean') {
                        output.animate = arg;
                    } else if (typeof arg === 'function') {
                        output.callback = arg;
                    }
                }
            }
            return self._execFilter('_parsePaginateArgs', output, arguments);
        }
    });
    $.MixItUp.prototype.extend({
        paginate: function() {
            var self = this,
                args = self._parsePaginateArgs(arguments);
            self.multiMix({
                paginate: args.command
            }, args.animate, args.callback);
        },
        nextPage: function() {
            var self = this,
                args = self._parsePaginateArgs(arguments);
            self.multiMix({
                paginate: self._getNextPage()
            }, args.animate, args.callback);
        },
        prevPage: function() {
            var self = this,
                args = self._parsePaginateArgs(arguments);
            self.multiMix({
                paginate: self._getPrevPage()
            }, args.animate, args.callback);
        }
    });
})(jQuery);
var measurements = {
    'limiters': {
        'collar': {
            min: {
                value: 3,
                type: 'limiter'
            },
            max: {
                value: 3,
                type: 'limiter'
            }
        },
        'chest': {
            min: {
                value: 4,
                type: 'limiter'
            },
            max: {
                value: 4,
                type: 'limiter'
            }
        },
        'waist': {
            min: {
                value: 4,
                type: 'limiter'
            },
            max: {
                value: 4,
                type: 'limiter'
            }
        },
        'shirt-length': {
            min: {
                value: 4,
                type: 'limiter'
            },
            max: {
                value: 4,
                type: 'limiter'
            }
        },
        'sleeve-length': {
            min: {
                value: 7,
                type: 'absolute'
            },
            max: {
                value: 2,
                type: 'limiter'
            }
        },
        'shoulder': {
            min: {
                value: 3,
                type: 'limiter'
            },
            max: {
                value: 3,
                type: 'limiter'
            }
        }
    },
    'male': {
        'relaxed': {
            'xs': {
                'collar': 15.00,
                'chest': 40.00,
                'waist': 38.00,
                'hips': 40.00,
                'shirt-length': 28.00,
                'sleeve-length': 24.00,
                'shoulder': 17.00,
                'bicep': 15.00,
                'cuff': 8.50,
                'shoulder-yoke': 3.00
            },
            's': {
                'collar': 15.50,
                'chest': 42.00,
                'waist': 40.00,
                'hips': 42.00,
                'shirt-length': 29.00,
                'sleeve-length': 24.50,
                'shoulder': 17.50,
                'bicep': 16.00,
                'cuff': 9.00,
                'shoulder-yoke': 3.00
            },
            'm': {
                'collar': 16.00,
                'chest': 44.00,
                'waist': 42.00,
                'hips': 44.00,
                'shirt-length': 30.00,
                'sleeve-length': 25.00,
                'shoulder': 18.00,
                'bicep': 17.00,
                'cuff': 9.50,
                'shoulder-yoke': 3.00
            },
            'l': {
                'collar': 16.50,
                'chest': 47.00,
                'waist': 46.00,
                'hips': 47.00,
                'shirt-length': 31.00,
                'sleeve-length': 25.50,
                'shoulder': 18.50,
                'bicep': 18.00,
                'cuff': 10.00,
                'shoulder-yoke': 3.00
            },
            'xl': {
                'collar': 17.50,
                'chest': 50.00,
                'waist': 49.00,
                'hips': 50.00,
                'shirt-length': 31.50,
                'sleeve-length': 26.00,
                'shoulder': 19.50,
                'bicep': 19.00,
                'cuff': 10.50,
                'shoulder-yoke': 3.00
            },
            'xxl': {
                'collar': 18.00,
                'chest': 54.00,
                'waist': 54.00,
                'hips': 54.00,
                'shirt-length': 32.00,
                'sleeve-length': 26.00,
                'shoulder': 21.00,
                'bicep': 20.00,
                'cuff': 11.00,
                'shoulder-yoke': 3.00
            },
            'xxxl': {
                'collar': 18.50,
                'chest': 58.00,
                'waist': 58.00,
                'hips': 58.00,
                'shirt-length': 32.00,
                'sleeve-length': 26.00,
                'shoulder': 22.00,
                'bicep': 21.00,
                'cuff': 11.50,
                'shoulder-yoke': 3.00
            }
        },
        'slim': {
            'xs': {
                'collar': 15.00,
                'chest': 39.00,
                'waist': 36.00,
                'hips': 38.00,
                'shirt-length': 28.00,
                'sleeve-length': 24.00,
                'shoulder': 16.50,
                'bicep': 14.50,
                'cuff': 8.50,
                'shoulder-yoke': 3.00
            },
            's': {
                'collar': 15.50,
                'chest': 41.00,
                'waist': 38.00,
                'hips': 40.00,
                'shirt-length': 29.00,
                'sleeve-length': 24.50,
                'shoulder': 17.00,
                'bicep': 15.50,
                'cuff': 9.00,
                'shoulder-yoke': 3.00
            },
            'm': {
                'collar': 16.00,
                'chest': 43.00,
                'waist': 40.00,
                'hips': 42.00,
                'shirt-length': 30.00,
                'sleeve-length': 25.00,
                'shoulder': 17.50,
                'bicep': 16.50,
                'cuff': 9.50,
                'shoulder-yoke': 3.00
            },
            'l': {
                'collar': 16.50,
                'chest': 46.00,
                'waist': 43.00,
                'hips': 45.00,
                'shirt-length': 31.00,
                'sleeve-length': 25.50,
                'shoulder': 18.00,
                'bicep': 17.50,
                'cuff': 10.00,
                'shoulder-yoke': 3.00
            },
            'xl': {
                'collar': 17.50,
                'chest': 48.00,
                'waist': 45.00,
                'hips': 47.00,
                'shirt-length': 31.50,
                'sleeve-length': 26.00,
                'shoulder': 19.00,
                'bicep': 18.50,
                'cuff': 10.50,
                'shoulder-yoke': 3.00
            }
        },
        'custom': {
            'custom': {
                'collar': 16.50,
                'chest': 47.00,
                'waist': 45.50,
                'hips': 47.00,
                'shirt-length': 31.00,
                'sleeve-length': 25.50,
                'shoulder': 18.00,
                'bicep': 17.50,
                'cuff': 10.00,
                'shoulder-yoke': 3.00
            }
        }
    },
    'female': {
        'custom': {
            'collar': 14.50,
            'chest': 40.00,
            'waist': 34.00,
            'shirt-length': 25.00,
            'sleeve-length': 23.50,
            'hips': 40.00,
            'shoulder': 15.00,
            'bicep': 13.50,
            'cuff': 9.00,
            'shoulder-yoke': 0.00
        },
        '0': {
            'collar': 13.50,
            'chest': 35.00,
            'waist': 29.00,
            'shirt-length': 24.00,
            'sleeve-length': 22.00,
            'hips': 35.00,
            'shoulder': 14.00,
            'bicep': 12.50,
            'cuff': 8.00,
            'shoulder-yoke': 0.00
        },
        '2': {
            'collar': 14.00,
            'chest': 37.00,
            'waist': 31.00,
            'shirt-length': 24.50,
            'sleeve-length': 22.50,
            'hips': 37.00,
            'shoulder': 14.50,
            'bicep': 13.00,
            'cuff': 8.50,
            'shoulder-yoke': 0.00
        },
        '4': {
            'collar': 14.50,
            'chest': 40.00,
            'waist': 34.00,
            'shirt-length': 25.00,
            'sleeve-length': 23.50,
            'hips': 40.00,
            'shoulder': 15.00,
            'bicep': 13.50,
            'cuff': 9.00,
            'shoulder-yoke': 0.00
        },
        '6': {
            'collar': 15.00,
            'chest': 42.00,
            'waist': 36.00,
            'shirt-length': 25.50,
            'sleeve-length': 23.50,
            'hips': 42.00,
            'shoulder': 15.50,
            'bicep': 14.00,
            'cuff': 9.50,
            'shoulder-yoke': 0.00
        },
        '8': {
            'collar': 15.50,
            'chest': 44.00,
            'waist': 38.00,
            'shirt-length': 26.00,
            'sleeve-length': 24.00,
            'hips': 44.00,
            'shoulder': 16.00,
            'bicep': 14.50,
            'cuff': 10.00,
            'shoulder-yoke': 0.00
        },
        '10': {
            'collar': 16.00,
            'chest': 46.00,
            'waist': 40.00,
            'shirt-length': 26.00,
            'sleeve-length': 24.00,
            'hips': 46.00,
            'shoulder': 16.50,
            'bicep': 15.00,
            'cuff': 10.00,
            'shoulder-yoke': 0.00
        }
    }
};
var smartSizing = {
    'male': {
        'shape': {
            'skinny': {
                'xs': {
                    'collar': 15.00,
                    'chest': 37.00,
                    'waist': 35.00,
                    'hips': 36.00,
                    'shoulder': 16.00,
                    'bicep': 13.50,
                    'cuff': 8.50
                },
                's': {
                    'collar': 15.50,
                    'chest': 39.00,
                    'waist': 37.00,
                    'hips': 38.00,
                    'shoulder': 16.50,
                    'bicep': 14.00,
                    'cuff': 9.00
                }
            },
            'athletic': {
                'xs': {
                    'collar': 15.00,
                    'chest': 39.00,
                    'waist': 35.00,
                    'hips': 38.00,
                    'shoulder': 17.00,
                    'bicep': 15.00,
                    'cuff': 8.50
                },
                's': {
                    'collar': 15.50,
                    'chest': 41.00,
                    'waist': 37.00,
                    'hips': 40.00,
                    'shoulder': 17.50,
                    'bicep': 16.00,
                    'cuff': 9.00
                },
                'm': {
                    'collar': 16.00,
                    'chest': 43.00,
                    'waist': 39.00,
                    'hips': 42.00,
                    'shoulder': 18.00,
                    'bicep': 17.00,
                    'cuff': 9.50
                },
                'l': {
                    'collar': 16.50,
                    'chest': 46.00,
                    'waist': 43.00,
                    'hips': 45.00,
                    'shoulder': 18.50,
                    'bicep': 18.00,
                    'cuff': 10.00
                },
                'xl': {
                    'collar': 17.50,
                    'chest': 48.00,
                    'waist': 45.00,
                    'hips': 47.00,
                    'shoulder': 19.00,
                    'bicep': 19.00,
                    'cuff': 10.50
                }
            },
            'average': {
                'xs': {
                    'collar': 15.00,
                    'chest': 40.00,
                    'waist': 38.00,
                    'hips': 40.00,
                    'shoulder': 16.50,
                    'bicep': 14.00,
                    'cuff': 8.50
                },
                's': {
                    'collar': 15.50,
                    'chest': 42.00,
                    'waist': 40.00,
                    'hips': 42.00,
                    'shoulder': 17.00,
                    'bicep': 15.50,
                    'cuff': 9.00
                },
                'm': {
                    'collar': 16.00,
                    'chest': 44.00,
                    'waist': 42.00,
                    'hips': 44.00,
                    'shoulder': 18.00,
                    'bicep': 16.50,
                    'cuff': 9.50
                },
                'l': {
                    'collar': 16.50,
                    'chest': 47.00,
                    'waist': 45.50,
                    'hips': 47.00,
                    'shoulder': 18.50,
                    'bicep': 17.50,
                    'cuff': 10.00
                },
                'xl': {
                    'collar': 17.50,
                    'chest': 50.00,
                    'waist': 48.50,
                    'hips': 50.00,
                    'shoulder': 19.00,
                    'bicep': 18.50,
                    'cuff': 10.50
                }
            },
            'healthy': {
                'xs': {
                    'collar': 15.00,
                    'chest': 40.00,
                    'waist': 41.00,
                    'hips': 41.00,
                    'shoulder': 17.00,
                    'bicep': 14.00,
                    'cuff': 8.50
                },
                's': {
                    'collar': 15.50,
                    'chest': 42.00,
                    'waist': 43.00,
                    'hips': 43.00,
                    'shoulder': 17.50,
                    'bicep': 15.50,
                    'cuff': 9.00
                },
                'm': {
                    'collar': 16.00,
                    'chest': 44.00,
                    'waist': 45.00,
                    'hips': 45.00,
                    'shoulder': 18.00,
                    'bicep': 16.50,
                    'cuff': 9.50
                },
                'l': {
                    'collar': 16.50,
                    'chest': 47.00,
                    'waist': 48.00,
                    'hips': 48.00,
                    'shoulder': 18.50,
                    'bicep': 17.50,
                    'cuff': 10.00
                },
                'xl': {
                    'collar': 17.50,
                    'chest': 50.00,
                    'waist': 52.00,
                    'hips': 52.00,
                    'shoulder': 19.00,
                    'bicep': 18.50,
                    'cuff': 10.50
                },
                'xxl': {
                    'collar': 18.00,
                    'chest': 54.00,
                    'waist': 56.00,
                    'hips': 56.00,
                    'shoulder': 20.00,
                    'bicep': 19.50,
                    'cuff': 11.00
                },
                'xxxl': {
                    'collar': 18.50,
                    'chest': 58.00,
                    'waist': 60.00,
                    'hips': 60.00,
                    'shoulder': 21.00,
                    'bicep': 21.00,
                    'cuff': 11.50
                }
            }
        },
        'height': {
            'm': {
                163: {
                    'shirt-length': 27.00,
                    'sleeve-length': 24.50
                },
                175: {
                    'shirt-length': 28.00,
                    'sleeve-length': 25.50
                },
                183: {
                    'shirt-length': 29.00,
                    'sleeve-length': 26.00
                },
                191: {
                    'shirt-length': 30.00,
                    'sleeve-length': 27.00
                },
                198: {
                    'shirt-length': 32.00,
                    'sleeve-length': 28.00
                }
            },
            'l': {
                163: {
                    'shirt-length': 28.00,
                    'sleeve-length': 24.50
                },
                175: {
                    'shirt-length': 29.50,
                    'sleeve-length': 25.50
                },
                183: {
                    'shirt-length': 30.00,
                    'sleeve-length': 26.00
                },
                191: {
                    'shirt-length': 31.50,
                    'sleeve-length': 27.00
                },
                198: {
                    'shirt-length': 33.50,
                    'sleeve-length': 28.00
                }
            }
        },
        'fit': {
            'body': {
                'chest': 0.00,
                'waist': -1.00,
                'hips': -1.00,
                'biceps': -0.50,
                'shoulder': 0.00
            },
            'structured': {
                'chest': 0.00,
                'waist': 0.00,
                'hips': 0.00,
                'biceps': 0.00,
                'shoulder': 0.0
            },
            'comfort': {
                'chest': 0.00,
                'waist': 1.00,
                'hips': 1.00,
                'biceps': 0.00,
                'shoulder': 0.5
            }
        }
    },
    'female': {
        'height': {
            152: {
                'shirt-length': 24.00,
                'sleeve-length': 22.00
            },
            160: {
                'shirt-length': 24.50,
                'sleeve-length': 22.50
            },
            168: {
                'shirt-length': 25.00,
                'sleeve-length': 23.50
            },
            175: {
                'shirt-length': 25.50,
                'sleeve-length': 24.00
            },
            180: {
                'shirt-length': 26.50,
                'sleeve-length': 24.50
            },
            188: {
                'shirt-length': 28.00,
                'sleeve-length': 25.00
            }
        },
        'fit': {
            'fit': {
                'chest': 0.00,
                'waist': 0.00,
                'hips': 0.00,
                'biceps': 0.00
            },
            'loose': {
                'chest': 1.00,
                'waist': 2.00,
                'hips': 2.00,
                'biceps': 1.50
            }
        }
    },
    'rules': {
        'male': {
            'heights': [{
                feet: "5' 2''",
                cm: "157"
            }, {
                feet: "5' 3''",
                cm: "160"
            }, {
                feet: "5' 4''",
                cm: "163"
            }, {
                feet: "5' 5''",
                cm: "165"
            }, {
                feet: "5' 6''",
                cm: "168"
            }, {
                feet: "5' 7''",
                cm: "170"
            }, {
                feet: "5' 8''",
                cm: "173"
            }, {
                feet: "5' 9''",
                cm: "175"
            }, {
                feet: "5' 10''",
                cm: "178"
            }, {
                feet: "5' 11''",
                cm: "180"
            }, {
                feet: "6'",
                cm: "183"
            }, {
                feet: "6' 1''",
                cm: "185"
            }, {
                feet: "6' 2''",
                cm: "188"
            }, {
                feet: "6' 3''",
                cm: "191"
            }, {
                feet: "6' 4''",
                cm: "193"
            }, {
                feet: "6' 5''",
                cm: "195"
            }, {
                feet: "6' 6''",
                cm: "198"
            }],
            getSize: function(weight) {
                if (weight <= 65) {
                    return 'xs'
                } else if (weight <= 75) {
                    return 's'
                } else if (weight <= 85) {
                    return 'm'
                } else if (weight <= 95) {
                    return 'l'
                } else if (weight <= 105) {
                    return 'xl'
                } else if (weight <= 115) {
                    return 'xxl'
                } else if (weight <= 125) {
                    return 'xxxl'
                }
            },
            getHeight: function(height) {
                if (height <= 163) {
                    return 163;
                } else if (height <= 175) {
                    return 175;
                } else if (height <= 183) {
                    return 183;
                } else if (height <= 191) {
                    return 191;
                } else {
                    return 198;
                }
            }
        },
        'female': {
            'heights': [{
                feet: "4' 10''",
                cm: "147"
            }, {
                feet: "4' 11''",
                cm: "150"
            }, {
                feet: "5'",
                cm: "152"
            }, {
                feet: "5' 1''",
                cm: "155"
            }, {
                feet: "5' 2''",
                cm: "157"
            }, {
                feet: "5' 3''",
                cm: "160"
            }, {
                feet: "5' 4''",
                cm: "163"
            }, {
                feet: "5' 5''",
                cm: "165"
            }, {
                feet: "5' 6''",
                cm: "168"
            }, {
                feet: "5' 7''",
                cm: "170"
            }, {
                feet: "5' 8''",
                cm: "173"
            }, {
                feet: "5' 9''",
                cm: "175"
            }, {
                feet: "5' 10''",
                cm: "178"
            }, {
                feet: "5' 11''",
                cm: "180"
            }, {
                feet: "6'",
                cm: "183"
            }, {
                feet: "6' 1''",
                cm: "185"
            }, {
                feet: "6' 2''",
                cm: "188"
            }],
            getHeight: function(height) {
                if (height <= 152) {
                    return 152;
                } else if (height <= 160) {
                    return 160;
                } else if (height <= 168) {
                    return 168;
                } else if (height <= 175) {
                    return 175;
                } else if (height <= 180) {
                    return 180;
                } else {
                    return 188;
                }
            }
        }
    }
};
var xpo = {
    key: 'd0fe46c3204e45e39f1efebf65ee0e4b',
    base: 'https://bombayshirts.picarioxpo.com/',
    xpourl: 'https://bombayshirts.picarioxpo.com/xpo/api/v1/',
    elements: {
        'bottom-cut': {
            'index': 1,
            'options': {
                'fabric': '',
                'monogram-text': 'ABC',
                'monogram-color': 'transparent'
            },
            'rounded': {
                'option': 'side_cut_topaz_v00.pfs',
                'query': '&p.tn=#fabric#.jpg&p.tr=True&p.text=,#monogram-text#&p.text.color=,#monogram-color#&p.text.font=,Monotype%20Corsiva&p.text.size=,50&p.text.px=,0.4&p.text.py=,0.5&p.text.align=,1'
            },
            'tail': {
                'option': 'side_cut_tail_v00.pfs',
                'query': '&p.tn=#fabric#.jpg&p.tr=True&p.text=,#monogram-text#&p.text.color=,#monogram-color#&p.text.font=,Monotype%20Corsiva&p.text.size=,50&p.text.px=,0.4&p.text.py=,0.5&p.text.align=,1'
            },
            'straight': {
                'option': 'side_cut_straight_cut_v00.pfs',
                'query': '&p.tn=#fabric#.jpg&p.tr=True&p.text=,#monogram-text#&p.text.color=,#monogram-color#&p.text.font=,Monotype%20Corsiva&p.text.size=,50&p.text.px=,0.4&p.text.py=,0.5&p.text.align=,1'
            }
        },
        'placket': {
            'index': 3,
            'options': {
                'outside-fabric': '',
                'button-color': 'button_white',
                'thread-color': 'ffffff'
            },
            'regular': {
                'annex': 'regular-one',
                'annex-two': 'regular-two',
                'option': 'placket_straight_regular_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            },
            'regular-one': {
                'option': 'placket_straight_regular_one_button_v00.pfs',
                'query': '&p.tn=#button-color#.jpg&p.tr=True&p.c=,#thread-color#,#thread-color#'
            },
            'regular-two': {
                'option': 'placket_straight_regular_two_button_v00.pfs',
                'query': '&p.tn=#button-color#.jpg&p.tr=True&p.c=,#thread-color#,#thread-color#'
            },
            'pencil': {
                'annex': 'pencil-one',
                'annex-two': 'pencil-two',
                'option': 'placket_straight_pencil_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            },
            'pencil-one': {
                'option': 'placket_straight_pencil_one button_v00.pfs',
                'query': '&p.tn=#button-color#.jpg&p.tr=True&p.c=,#thread-color#,#thread-color#'
            },
            'pencil-two': {
                'option': 'placket_straight_pencil_two button_v00.pfs',
                'query': '&p.tn=#button-color#.jpg&p.tr=True&p.c=,#thread-color#,#thread-color#'
            },
            'french': {
                'annex': 'pencil-one',
                'annex-two': 'pencil-two',
                'option': '',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            },
            'french-one': {
                'option': 'placket_straight_french_v00.pfs',
                'query': '&p.tn=#button-color#.jpg&p.tr=True&p.c=,#thread-color#,#thread-color#'
            },
            'french-two': {
                'option': 'placket_straight_french_two_button_v00.pfs',
                'query': '&p.tn=#button-color#.jpg&p.tr=True&p.c=,#thread-color#,#thread-color#'
            },
            'concealed': {
                'option': 'placket_straight_concealed_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            }
        },
        'placket-piping': {
            'index': 4,
            'options': {
                'piping-fabric': ''
            },
            'french': {
                'option': 'placket_contrast_regular_outside_twill_tape_v00.pfs',
                'query': '&p.tn=#piping-fabric#.jpg&p.tr=True&p.trt=90'
            },
            'pencil': {
                'option': 'placket_contrast_pencil_outside_twill_tape_v00.pfs',
                'query': '&p.tn=#piping-fabric#.jpg&p.tr=True&p.trt=90'
            },
            'regular': {
                'option': 'placket_contrast_regular_outside_twill_tape_v00.pfs',
                'query': '&p.tn=#piping-fabric#.jpg&p.tr=True&p.trt=90'
            },
            'inside': {
                'option': 'placket_contrast_button_twill_tape_v00.pfs',
                'query': '&p.tn=#piping-fabric#.jpg&p.tr=True&p.trt=90'
            }
        },
        'placket-contrast': {
            'index': 6,
            'options': {
                'contrast-fabric': ''
            },
            'inside': {
                'option': 'plaket_contrast_inside_v00.pfs',
                'query': '&p.tn=#contrast-fabric#.jpg&p.tr=True'
            },
            'edge': {
                'option': 'plaket_contrast_outside_v00.pfs',
                'query': '&p.tn=#contrast-fabric#.jpg&p.tr=True'
            }
        },
        'sleeve': {
            'index': 5,
            'options': {
                'button-color': 'button_white',
                'thread-color': 'ffffff',
                'fabric': ''
            },
            'full': {
                'option': 'sleeves_full_v00.pfs',
                'query': '&p.tn=#fabric#.jpg&p.tr=True',
                'annex': 'sleeve-button',
                'back': {
                    'option': 'sleeves_back_full_v00_1.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                }
            },
            'roll-up': {
                'option': 'sleeves_roll_up_v00.pfs',
                'query': '&p.tn=#fabric#.jpg&p.tr=True',
                'annex': 'cuff',
                'annex-two': 'sleeve-button',
                'back': {
                    'option': 'sleeves_back_roll_up_v00.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                }
            },
            'half': {
                'option': 'sleeves_half_v00.pfs',
                'query': '&p.tn=#fabric#.jpg&p.tr=True',
                'back': {
                    'option': 'sleeves_back_half_v00.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                }
            },
            'cuff': {
                'annex': 'button',
                'option': 'cuff_french_square__roolup_sleeve_v00.pfs',
                'query': '&p.tn=#fabric#.jpg,#fabric#.jpg&p.tr=True,True'
            },
            'sleeve-button': {
                'option': 'sleeves_full_placket_button_v00.pfs',
                'query': '&p.tn=#button-color#.jpg&p.tr=True&p.c=,#thread-color#,#thread-color#'
            },
            'button': {
                'option': 'cuff_one button_buttons_roolup_sleeve_v00.pfs',
                'query': '&p.tn=#button-color#.jpg&p.tr=True&p.c=,#thread-color#,#thread-color#'
            }
        },
        'cuff': {
            'index': 7,
            'options': {
                'thread-color': 'ffffff',
                'button-color': 'button_white',
                'inside-fabric': '',
                'outside-fabric': '',
                'monogram-text': 'ABC',
                'monogram-color': 'transparent'
            },
            'french': {
                'button': {
                    'option': 'cuff_french_button_v00.pfs',
                    'query': '&p.tn=#button-color#.jpg&p.tr=True&p.c=,#thread-color#,#thread-color#'
                },
                'square': {
                    'annex': 'button',
                    'option': 'cuff_french_square_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg,#inside-fabric#.jpg&p.tr=True,True&p.text=,,#monogram-text#&p.text.color=,,#monogram-color#&p.text.font=,,Monotype%20Corsiva&p.text.size=,,250&p.text.px=,,0.2&p.text.py=,,1&p.text.align=,,1'
                },
                'rounded': {
                    'annex': 'button',
                    'option': 'cuff_french_round_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg,#inside-fabric#.jpg&p.tr=True,True&p.text=,,#monogram-text#&p.text.color=,,#monogram-color#&p.text.font=,,Monotype%20Corsiva&p.text.size=,,250&p.text.px=,,0.2&p.text.py=,,1&p.text.align=,,1'
                },
                'angled': {
                    'annex': 'button',
                    'option': 'cuff_french_angle_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg,#inside-fabric#.jpg&p.tr=True,True&p.text=,,#monogram-text#&p.text.color=,,#monogram-color#&p.text.font=,,Monotype%20Corsiva&p.text.size=,,250&p.text.px=,,0.2&p.text.py=,,1&p.text.align=,,1'
                }
            },
            'single-convertible': {
                'button': {
                    'option': 'cuff_one button_buttons_v00.pfs',
                    'query': '&p.tn=#button-color#.jpg&p.tr=True&p.c=,#thread-color#,#thread-color#'
                },
                'square': {
                    'annex': 'button',
                    'option': 'cuff_one button_square_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg,#inside-fabric#.jpg&p.tr=True,True&p.text=,,#monogram-text#&p.text.color=,,#monogram-color#&p.text.font=,,Monotype%20Corsiva&p.text.size=,,250&p.text.px=,,0.2&p.text.py=,,1&p.text.align=,,1'
                },
                'rounded': {
                    'annex': 'button',
                    'option': 'cuff_one button_round_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg,#inside-fabric#.jpg&p.tr=True,True&p.text=,,#monogram-text#&p.text.color=,,#monogram-color#&p.text.font=,,Monotype%20Corsiva&p.text.size=,,250&p.text.px=,,0.2&p.text.py=,,1&p.text.align=,,1'
                },
                'angled': {
                    'annex': 'button',
                    'option': 'cuff_one button_angle_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg,#inside-fabric#.jpg&p.tr=True,True&p.text=,,#monogram-text#&p.text.color=,,#monogram-color#&p.text.font=,,Monotype%20Corsiva&p.text.size=,,250&p.text.px=,,0.2&p.text.py=,,1&p.text.align=,,1'
                }
            },
            'double-bond': {
                'button': {
                    'option': 'cuff_two_buttons_button_v00.pfs',
                    'query': '&p.tn=#button-color#.jpg&p.tr=True&p.c=,#thread-color#,#thread-color#'
                },
                'square': {
                    'annex': 'button',
                    'option': 'cuff_two_buttons_square_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg,#inside-fabric#.jpg&p.tr=True,True&p.text=,,#monogram-text#&p.text.color=,,#monogram-color#&p.text.font=,,Monotype%20Corsiva&p.text.size=,,250&p.text.px=,,0.2&p.text.py=,,1&p.text.align=,,1'
                },
                'rounded': {
                    'annex': 'button',
                    'option': 'cuff_two_buttons_round_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg,#inside-fabric#.jpg&p.tr=True,True&p.text=,,#monogram-text#&p.text.color=,,#monogram-color#&p.text.font=,,Monotype%20Corsiva&p.text.size=,,250&p.text.px=,,0.2&p.text.py=,,1&p.text.align=,,1'
                },
                'angled': {
                    'annex': 'button',
                    'option': 'cuff_two_buttons_angle_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg,#inside-fabric#.jpg&p.tr=True,True&p.text=,,#monogram-text#&p.text.color=,,#monogram-color#&p.text.font=,,Monotype%20Corsiva&p.text.size=,,250&p.text.px=,,0.2&p.text.py=,,1&p.text.align=,,1'
                }
            },
            'neopolitan': {
                'button': {
                    'option': 'cuff_meopolitian_button_v00.pfs',
                    'query': '&p.tn=#button-color#.jpg&p.tr=True&p.c=,#thread-color#,#thread-color#'
                },
                'rounded': {
                    'annex': 'button',
                    'option': 'cuff_meopolitian_round_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg,#inside-fabric#.jpg&p.tr=True,True&p.text=,,#monogram-text#&p.text.color=,,#monogram-color#&p.text.font=,,Monotype%20Corsiva&p.text.size=,,250&p.text.px=,,0.2&p.text.py=,,1&p.text.align=,,1'
                }
            }
        },
        'cuff-piping': {
            'index': 8,
            'options': {
                'piping-fabric': ''
            },
            'placket': {
                'option': 'cuff_contrast_button_placket_v00.pfs',
                'query': '&p.tn=#piping-fabric#.jpg&p.tr=True&p.trt=90'
            }
        },
        'collar': {
            'index': 9,
            'options': {
                'thread-color': 'ffffff',
                'button-color': 'button_white',
                'inside-fabric': '',
                'outside-fabric': '',
                'monogram-text': 'ABC',
                'monogram-color': 'transparent'
            },
            'band': {
                'option': 'collar_band_v00.pfs',
                'query': '&p.tn=#inside-fabric#.jpg,#outside-fabric#.jpg&p.tr=True,True&p.text=,,#monogram-text#&p.text.color=,,#monogram-color#&p.text.font=,,Monotype%20Corsiva&p.text.size=,,200&p.text.px=,,0.5&p.text.py=,,0.5&p.text.align=,,1'
            },
            'prince-charlie': {
                'base': 'band',
                'option': 'collar_prince_charlie_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True',
                'back': {
                    'option': 'back_spread_eagle_collar_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
                }
            },
            'madmen': {
                'base': 'band',
                'option': 'collar_madmen_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True',
                'back': {
                    'option': 'back_spread_eagle_collar_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
                }
            },
            'club': {
                'base': 'band',
                'option': 'collar_club_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True',
                'back': {
                    'option': 'back_spread_eagle_collar_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
                }
            },
            'bandhgala': {
                'option': 'collar_bandhgala_v00.pfs',
                'query': '&p.tn=#inside-fabric#.jpg,#outside-fabric#.jpg&p.tr=True,True&p.text=,,#monogram-text#&p.text.color=,,#monogram-color#&p.text.font=,,Monotype%20Corsiva&p.text.size=,,200&p.text.px=,,0.5&p.text.py=,,0.5&p.text.align=,,1',
                'back': {
                    'option': 'collar_bandhgala_back_v00.pfs',
                    'query': '&p.tn=#inside-fabric#.jpg,#outside-fabric#.jpg&p.tr=True,True'
                }
            },
            'hipster-band': {
                'option': 'collar_hipster_band_v00.pfs',
                'query': '&p.tn=#inside-fabric#.jpg,#outside-fabric#.jpg&p.tr=True,True&p.text=,,#monogram-text#&p.text.color=,,#monogram-color#&p.text.font=,,Monotype%20Corsiva&p.text.size=,,200&p.text.px=,,0.5&p.text.py=,,0.5&p.text.align=,,1'
            },
            'hipster': {
                'base': 'hipster-band',
                'option': 'collar_hipster_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True',
                'back': {
                    'option': 'back_spread_eagle_collar_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
                }
            },
            'hipster-rounded': {
                'base': 'hipster-band',
                'option': 'collar_hipster_rounded_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True',
                'back': {
                    'option': 'back_spread_eagle_collar_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
                }
            },
            'prepster': {
                'base': 'hipster-band',
                'option': 'collar_prepster_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True',
                'back': {
                    'option': 'back_spread_eagle_collar_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
                }
            },
            'evil-pandit': {
                'base': 'bandhgala',
                'option': 'collar_evil_pandit_v00.pfs',
                'query': '&p.tn=#inside-fabric#.jpg,#inside-fabric#.jpg&p.tr=True,True',
                'back': {
                    'option': 'collar_bandhgala_back_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
                }
            },
            'double': {
                'base': 'band',
                'option': 'collar_double_v00.pfs',
                'query': '&p.c=,,&p.tn=#outside-fabric#.jpg,#inside-fabric#.jpg,#button-color#.jpg&p.tr=True,True,True',
                'back': {
                    'option': 'back_spread_eagle_collar_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
                }
            },
            'polo': {
                'base': 'band',
                'option': 'collar_polo_v00.pfs',
                'query': '&p.c=,,#thread-color#&p.tn=#outside-fabric#.jpg,#button-color#.jpg&p.tr=True,True',
                'back': {
                    'option': 'back_spread_eagle_collar_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
                }
            },
            'concealed-polo': {
                'base': 'band',
                'option': 'collar_concealed_polo_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg,#outside-fabric#.jpg&p.tr=True,True',
                'back': {
                    'option': 'back_spread_eagle_collar_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
                }
            },
            'ozwald-boateng': {
                'base': 'band',
                'option': 'collar_ozward_boateng_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True',
                'back': {
                    'option': 'back_spread_eagle_collar_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
                }
            },
            'wing': {
                'base': 'bandhgala',
                'option': 'collar_wing_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True',
                'back': {
                    'option': 'collar_band_back_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
                }
            },
            'spread-eagle': {
                'base': 'band',
                'option': 'collar_spread_eagle_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True',
                'back': {
                    'option': 'back_spread_eagle_collar_v00.pfs',
                    'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
                }
            }
        },
        'collar-piping': {
            'index': 12,
            'options': {
                'piping-fabric': ''
            },
            'fabric': {
                'option': 'collar_contrast_piping_v00.pfs',
                'query': '&p.tn=#piping-fabric#.jpg&p.tr=True'
            },
            'twill': {
                'option': 'collar_contrast_twill_tape_v00.pfs',
                'query': '&p.tn=#piping-fabric#.jpg&p.trt=0'
            },
            'fabric-bandhgala': {
                'option': 'collar_contrast_piping_bandgala_v00.pfs',
                'query': '&p.tn=#piping-fabric#.jpg&p.tr=True'
            },
            'twill-bandhgala': {
                'option': 'collar_contrast_twill_tape_bandgala_v00.pfs',
                'query': '&p.tn=#piping-fabric#.jpg&p.tr=True'
            }
        },
        'collar-top-button': {
            'index': 11,
            'options': {
                'thread-color': 'ffffff',
                'button-color': 'ffffff',
                'fabric': ''
            },
            'single': {
                'option': 'collar_band_one_button_v00.pfs',
                'query': '&p.tn=#button-color#.jpg,,#fabric#.jpg&p.tr=True,,True&p.c=,#thread-color#'
            },
            'double': {
                'option': 'collar_band_two_button_v00.pfs',
                'query': '&p.tn=#button-color#.jpg,,#fabric#.jpg&p.tr=True,,True&p.c=,#thread-color#'
            }
        },
        'collar-zoom': {
            'index': 9,
            'options': {
                'thread-color': 'ffffff',
                'button-color': 'button_white',
                'inside-fabric': '',
                'outside-fabric': '',
                'fabric': ''
            },
            'base': {
                'option': 'close_collar_base_v00.pfs',
                'query': '&p.tn=#fabric#.jpg&p.tr=True'
            },
            'band': {
                'option': 'close_collar_Band_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            },
            'button-one': {
                'option': 'close_collar_one_button_v00.pfs',
                'query': '&p.tn=#button-color#.jpg&p.tr=True&p.c=,#thread-color#,#thread-color#'
            },
            'button-two': {
                'option': 'close_collar_two_button_v00.pfs',
                'query': '&p.tn=#button-color#.jpg&p.tr=True&p.c=,#thread-color#,#thread-color#'
            },
            'prince-charlie': {
                'annex': 'button-one',
                'base': 'band',
                'option': 'close_collar_prince_charlie_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            },
            'madmen': {
                'annex': 'button-one',
                'base': 'band',
                'option': 'close_collar_madmen_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            },
            'club': {
                'annex': 'button-one',
                'base': 'band',
                'option': 'close_collar_club_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            },
            'bandhgala': {
                'annex': 'button-one',
                'option': 'close_collar_bandhgala_v00.pfs',
                'query': '&p.c=,,#thread-color#,#thread-color#&p.tn=#inside-fabric#.jpg,#button-color#.jpg&p.tr=True,True'
            },
            'hipster-band': {
                'annex': 'button-one',
                'option': 'close_collar_hipster_band_v00.pfs',
                'query': '&p.c=,,,#thread-color#,#thread-color#&p.tn=#outside-fabric#.jpg,#inside-fabric#.jpg,#button-color#.jpg&p.tr=True,True,True'
            },
            'hipster': {
                'annex': 'button-one',
                'base': 'hipster-band',
                'option': 'close_collar_hipster_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            },
            'hipster-rounded': {
                'annex': 'button-one',
                'base': 'hipster-band',
                'option': 'close_collar_hipster_rounded_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            },
            'evil-pandit': {
                'annex': 'button-one',
                'base': 'band',
                'option': 'close_collar_evil_pandit_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            },
            'double': {
                'annex': 'button-one',
                'base': 'band',
                'option': 'close_collar_double_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            },
            'concealed-polo': {
                'annex': 'button-one',
                'base': 'band',
                'option': 'close_collar_concealed_polo_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            },
            'ozward-boateng': {
                'annex': 'button-one',
                'base': 'band',
                'option': 'close_collar_ozward_boateng_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            },
            'wing': {
                'annex': 'button-one',
                'base': 'bandhgala',
                'option': 'close_collar_wing_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            },
            'spread-eagle': {
                'annex': 'button-one',
                'base': 'band',
                'option': 'close_collar_spread_eagle_v00.pfs',
                'query': '&p.tn=#outside-fabric#.jpg&p.tr=True'
            }
        },
        'back-zoom': {
            'index': 9,
            'options': {
                'thread-color': 'ffffff',
                'button-color': 'button_white',
                'fabric': ''
            },
            'base': {
                'option': 'back_base_v01.pfs',
                'query': '&p.tn=#fabric#.jpg&p.tr=True'
            },
            'rounded': {
                'none': {
                    'option': 'back_topaz_no_pleats_v00.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                },
                'box': {
                    'option': 'back_topaz_box_pleats_v00.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                },
                'darts': {
                    'option': 'back_topaz_darts_v00.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                },
                'side': {
                    'option': 'back_topaz_side_pleats_v00.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                }
            },
            'tail': {
                'none': {
                    'option': 'back_tail_no_pleats_v00.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                },
                'box': {
                    'option': 'back_tail_box_pleats_v00.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                },
                'darts': {
                    'option': 'back_tail_darts_v00.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                },
                'side': {
                    'option': 'back_tail_side_pleats_v00.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                }
            },
            'straight': {
                'none': {
                    'option': 'back_straight_no_pleats_v00.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                },
                'box': {
                    'option': 'back_straight_box_pleats_v00.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                },
                'darts': {
                    'option': 'back_straight_darts_v00.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                },
                'side': {
                    'option': 'back_straight_side_pleats_v00.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                }
            }
        },
        'pocket': {
            'index': 11,
            'options': {
                'fabric': '',
                'thread-color': 'ffffff',
                'button-color': 'button_white'
            },
            'single-angled': {
                'option': 'pocket_normal_single_v00.pfs',
                'query': '&p.tn=#fabric#.jpg&p.tr=True'
            },
            'double-angled': {
                'option': 'pocket_normal_double_v00.pfs',
                'query': '&p.tn=#fabric#.jpg&p.tr=True'
            },
            'single-flap': {
                'option': 'pocket_flap_single_v00.pfs',
                'query': '&p.c=,,#thread-color#&p.tn=#fabric#.jpg,#button-color#.jpg&p.tr=True,True'
            },
            'double-flap': {
                'option': 'pocket_flap_double_v00.pfs',
                'query': '&p.c=,,#thread-color#&p.tn=#fabric#.jpg,#button-color#.jpg&p.tr=True,True'
            }
        },
        'epaulette': {
            'index': 13,
            'options': {
                'fabric': '',
                'button-color': 'button_white'
            },
            'yes': {
                'option': 'epaulette_v00.pfs',
                'query': '&p.c=,&p.tn=#fabric#.jpg,#button-color#.jpg&p.tr=True,True',
                'back': {
                    'option': 'epaulette_back_v00.pfs',
                    'query': '&p.c=,&p.tn=#fabric#.jpg,#button-color#.jpg&p.tr=True,True'
                }
            }
        },
        'elbow-patch': {
            'index': 16,
            'options': {
                'fabric': ''
            },
            'yes': {
                'option': 'elbow_patch_front_v00.pfs',
                'query': '&p.tn=#fabric#.jpg&p.tr=True',
                'back': {
                    'option': 'elbow_patch_v00.pfs',
                    'query': '&p.tn=#fabric#.jpg&p.tr=True'
                }
            }
        }
    },
    getElementQueryString: function(element, valueElement) {
        var query = valueElement['query'];
        for (var option in xpo.elements[element]['options']) {
            query = query.replaceAll('#' + option + '#', xpo.elements[element]['options'][option]);
        }
        if (query == null) return "";
        return query;
    },
    setOption: function(element, option, val) {
        var elements = element.split(",");
        for (var e in elements) {
            if (xpo.elements[elements[e]]) {
                xpo.elements[elements[e]]['options'][option] = val;
            }
        }
    }
};
var api = {
    width: 850,
    container: '.shirt-container',
    initializeXpo: function() {
        if (core.isTabletOrPhone()) {}
        api.setPrimaryFabric();
    },
    elements: {
        'mode': 'full',
        'old-fabric': 'PT001',
        'fabric': 'PT001',
        'back': 'none',
        'bottom-cut': 'rounded',
        'collar': 'spread-eagle',
        'collar-top-button': 'single',
        'collar-contrast': 'none',
        'collar-contrast-fabric': '',
        'collar-piping': 'none',
        'collar-piping-fabric': '',
        'sleeve': 'full',
        'cuff': 'single-convertible',
        'cuff-shape': 'rounded',
        'cuff-contrast': 'none',
        'cuff-contrast-fabric': '',
        'cuff-piping': 'none',
        'cuff-piping-fabric': '',
        'pocket': 'none',
        'pocket-contrast': '',
        'pocket-contrast-fabric': '',
        'placket': 'regular',
        'placket-button': 'single',
        'placket-contrast': 'none',
        'placket-contrast-fabric': '',
        'placket-piping': 'none',
        'placket-piping-fabric': '',
        'epaulette': 'none',
        'epaulette-contrast-fabric': '',
        'elbow-patch': 'none',
        'elbow-patch-contrast-fabric': '',
        'monogram': 'none',
        'monogram-text': '',
        'monogram-color': '',
        'button': 'button_white',
        'thread-color': 'ffffff'
    },
    setElement: function(element, value) {
        api.elements[element] = value;
    },
    setPrimaryFabric: function() {
        for (var element in xpo.elements) {
            for (var option in xpo.elements[element]['options']) {
                if (option.contains('fabric')) {
                    var fabric = xpo.elements[element]['options'][option];
                    if (fabric == '' || fabric == api.elements['old-fabric']) {
                        xpo.elements[element]['options'][option] = api.elements['fabric'];
                    }
                }
            }
        }
        api.elements['old-fabric'] = api.elements['fabric'];
    },
    setButtons: function() {
        for (var element in xpo.elements) {
            for (var option in xpo.elements[element]['options']) {
                if (option.contains('button-color')) {
                    xpo.elements[element]['options'][option] = api.elements['button'];
                }
            }
        }
    },
    setThreadColors: function() {
        for (var element in xpo.elements) {
            for (var option in xpo.elements[element]['options']) {
                if (option.contains('thread-color')) {
                    xpo.elements[element]['options'][option] = api.elements['thread-color'];
                }
            }
        }
    },
    processBottomCut: function() {
        var bottomCutImage = api.createElementImage('bottom-cut', xpo.elements['bottom-cut'][api.elements['bottom-cut']], false, false);
        return bottomCutImage;
    },
    processSleeve: function() {
        var annexElement = xpo.elements['sleeve'][api.elements['sleeve']]['annex'] ? xpo.elements['sleeve'][api.elements['sleeve']]['annex'] : false;
        var annexElementTwo = xpo.elements['sleeve'][api.elements['sleeve']]['annex-two'] ? xpo.elements['sleeve'][api.elements['sleeve']]['annex-two'] : false;
        var sleeveImage = api.createElementImage('sleeve', xpo.elements['sleeve'][api.elements['sleeve']], false, false);
        var cuffImage = "";
        var buttonImage = "";
        if (annexElement) {
            cuffImage = api.createElementImage('sleeve', xpo.elements['sleeve'][annexElement], false, true);
            annexElement = xpo.elements['sleeve'][annexElement]['annex'] ? xpo.elements['sleeve'][annexElement]['annex'] : false;
            if (annexElement) {
                buttonImage = api.createElementImage('sleeve', xpo.elements['sleeve'][annexElement], false, true);
            }
        }
        var annexTwoImage = "";
        if (annexElementTwo) {
            annexTwoImage = api.createElementImage('sleeve', xpo.elements['sleeve'][annexElementTwo], false, true);
        }
        return sleeveImage + cuffImage + buttonImage + annexTwoImage;
    },
    processMonogram: function() {
        var placement = api.elements['monogram'];
        if (placement == 'none') {
            xpo.setOption('cuff', 'monogram-color', 'transparent');
            xpo.setOption('collar', 'monogram-color', 'transparent');
            xpo.setOption('bottom-cut', 'monogram-color', 'transparent');
            xpo.setOption('cuff', 'monogram-text', 'ABC');
            xpo.setOption('collar', 'monogram-text', 'ABC');
            xpo.setOption('bottom-cut', 'monogram-text', 'ABC');
        } else if (placement == 'cuff') {
            xpo.setOption('cuff', 'monogram-color', api.elements['monogram-color']);
            xpo.setOption('collar', 'monogram-color', 'transparent');
            xpo.setOption('bottom-cut', 'monogram-color', 'transparent');
            xpo.setOption('cuff', 'monogram-text', api.elements['monogram-text'].toUpperCase());
            xpo.setOption('collar', 'monogram-text', 'ABC');
            xpo.setOption('bottom-cut', 'monogram-text', 'ABC');
        } else if (placement == 'collar') {
            xpo.setOption('cuff', 'monogram-color', 'transparent');
            xpo.setOption('collar', 'monogram-color', api.elements['monogram-color']);
            xpo.setOption('bottom-cut', 'monogram-color', 'transparent');
            xpo.setOption('cuff', 'monogram-text', 'ABC');
            xpo.setOption('collar', 'monogram-text', api.elements['monogram-text'].toUpperCase());
            xpo.setOption('bottom-cut', 'monogram-text', 'ABC');
        } else if (placement == 'rib') {
            xpo.setOption('cuff', 'monogram-color', 'transparent');
            xpo.setOption('collar', 'monogram-color', 'transparent');
            xpo.setOption('bottom-cut', 'monogram-color', api.elements['monogram-color']);
            xpo.setOption('cuff', 'monogram-text', 'ABC');
            xpo.setOption('collar', 'monogram-text', 'ABC');
            xpo.setOption('bottom-cut', 'monogram-text', api.elements['monogram-text'].toUpperCase());
        }
    },
    processPlacket: function() {
        var annexElement = xpo.elements['placket'][api.elements['placket']]['annex'] ? xpo.elements['placket'][api.elements['placket']]['annex'] : false;
        var placketContrast = api.elements['placket-contrast'];
        var pipingContrast = api.elements['placket-piping'];
        var placketButtons = "";
        if (annexElement) {
            var buttons = api.elements['placket-button'];
            if (buttons == 'single') {
                placketButtons = api.createElementImage('placket', xpo.elements['placket'][annexElement], false, true);
            } else if (buttons == 'double') {
                annexElement = xpo.elements['placket'][api.elements['placket']]['annex-two'];
                placketButtons = api.createElementImage('placket', xpo.elements['placket'][annexElement], false, true);
            }
        }
        var placketContrastImage = "";
        if (placketContrast == 'none') {
            xpo.setOption('placket-contrast', 'contrast-fabric', '');
            xpo.setOption('placket', 'outside-fabric', api.elements['fabric']);
        } else if (placketContrast == 'inside') {
            xpo.setOption('placket-contrast', 'contrast-fabric', api.elements['placket-contrast-fabric']);
            xpo.setOption('placket', 'outside-fabric', api.elements['fabric']);
            placketContrastImage = api.createElementImage('placket-contrast', xpo.elements['placket-contrast'][api.elements['placket-contrast']], false, false);
        } else if (placketContrast == 'outside') {
            xpo.setOption('placket-contrast', 'contrast-fabric', api.elements['placket-contrast-fabric']);
            xpo.setOption('placket', 'outside-fabric', api.elements['placket-contrast-fabric']);
        } else if (placketContrast == 'edge') {
            xpo.setOption('placket-contrast', 'contrast-fabric', api.elements['placket-contrast-fabric']);
            xpo.setOption('placket', 'outside-fabric', api.elements['fabric']);
            placketContrastImage = api.createElementImage('placket-contrast', xpo.elements['placket-contrast'][api.elements['placket-contrast']], false, false);
        }
        var placketImage = xpo.elements['placket'][api.elements['placket']]['option'] == "" ? "" : api.createElementImage('placket', xpo.elements['placket'][api.elements['placket']], false, false);
        var placketPipingImage = "";
        if (pipingContrast == 'none') {
            xpo.setOption('placket-piping', 'piping-fabric', '');
        } else if (pipingContrast == 'outside') {
            xpo.setOption('placket-piping', 'piping-fabric', api.elements['placket-piping-fabric']);
            if (api.elements['placket'] == 'regular' || api.elements['placket'] == 'french' || api.elements['placket'] == 'pencil') {
                placketPipingImage = api.createElementImage('placket-piping', xpo.elements['placket-piping'][api.elements['placket']], false, false);
            } else {
                placketPipingImage = "";
            }
        } else if (pipingContrast == 'inside') {
            xpo.setOption('placket-piping', 'piping-fabric', api.elements['placket-piping-fabric']);
            if (api.elements['placket'] == 'regular' || api.elements['placket'] == 'french' || api.elements['placket'] == 'pencil') {
                placketPipingImage = api.createElementImage('placket-piping', xpo.elements['placket-piping'][pipingContrast], false, false);
            } else {
                placketPipingImage = "";
            }
        }
        return placketImage + placketContrastImage + placketPipingImage + placketButtons;
    },
    processCollar: function() {
        var collarContrast = api.elements['collar-contrast'];
        var pipingContrast = api.elements['collar-piping'];
        var baseElement = xpo.elements['collar'][api.elements['collar']]['base'] ? xpo.elements['collar'][api.elements['collar']]['base'] : false;
        xpo.setOption('collar,collar-zoom', 'fabric', api.elements['fabric']);
        if (collarContrast == 'none') {
            xpo.setOption('collar,collar-zoom', 'inside-fabric', api.elements['fabric']);
            xpo.setOption('collar,collar-zoom', 'outside-fabric', api.elements['fabric']);
        } else if (collarContrast == 'inside') {
            xpo.setOption('collar,collar-zoom', 'inside-fabric', api.elements['collar-contrast-fabric']);
            xpo.setOption('collar,collar-zoom', 'outside-fabric', api.elements['fabric']);
        } else if (collarContrast == 'full') {
            xpo.setOption('collar,collar-zoom', 'inside-fabric', api.elements['collar-contrast-fabric']);
            xpo.setOption('collar,collar-zoom', 'outside-fabric', api.elements['collar-contrast-fabric']);
        }
        var collarPipingImage = "";
        if (pipingContrast == 'none') {
            xpo.setOption('collar-piping', 'piping-fabric', '');
        } else if (pipingContrast == 'collar') {
            var fabric = api.elements['collar-piping-fabric'];
            var isTwill = isNaN(fabric) ? false : true;
            xpo.setOption('collar-piping', 'piping-fabric', api.elements['collar-piping-fabric']);
            if (api.elements['collar'] != 'bandhgala' && api.elements['collar'] != 'hipster' && api.elements['collar'] != 'hipster-rounded' && api.elements['collar'] != 'prepster' && api.elements['collar'] != 'wing') {
                if (isTwill) {
                    collarPipingImage = api.createElementImage('collar-piping', xpo.elements['collar-piping']['twill'], false, false);
                } else {
                    collarPipingImage = api.createElementImage('collar-piping', xpo.elements['collar-piping']['fabric'], false, false);
                }
            } else {
                if (isTwill) {
                    collarPipingImage = api.createElementImage('collar-piping', xpo.elements['collar-piping']['twill-bandhgala'], false, false);
                } else {
                    collarPipingImage = api.createElementImage('collar-piping', xpo.elements['collar-piping']['fabric-bandhgala'], false, false);
                }
            }
        }
        var collarImage = api.createElementImage('collar', xpo.elements['collar'][api.elements['collar']], false, false);
        if (baseElement) {
            collarImage += api.createElementImage('collar', xpo.elements['collar'][baseElement], true, false);
        }
        return collarImage + collarPipingImage;
    },
    processCollarTopButton: function() {
        var collarButtonImage = api.createElementImage('collar-top-button', xpo.elements['collar-top-button'][api.elements['collar-top-button']], false, false);
        return collarButtonImage;
    },
    processCollarMode: function() {
        var collarContrast = api.elements['collar-contrast'];
        var baseElement = xpo.elements['collar-zoom'][api.elements['collar']]['base'] ? xpo.elements['collar-zoom'][api.elements['collar']]['base'] : false;
        var annexElement = xpo.elements['collar-zoom'][api.elements['collar']]['annex'] ? xpo.elements['collar-zoom'][api.elements['collar']]['annex'] : false;
        xpo.setOption('collar,collar-zoom', 'fabric', api.elements['fabric']);
        if (collarContrast == 'none') {
            xpo.setOption('collar,collar-zoom', 'inside-fabric', api.elements['fabric']);
            xpo.setOption('collar,collar-zoom', 'outside-fabric', api.elements['fabric']);
        } else if (collarContrast == 'inside') {
            xpo.setOption('collar,collar-zoom', 'inside-fabric', api.elements['collar-contrast-fabric']);
            xpo.setOption('collar,collar-zoom', 'outside-fabric', api.elements['fabric']);
        } else if (collarContrast == 'full') {
            xpo.setOption('collar,collar-zoom', 'inside-fabric', api.elements['collar-contrast-fabric']);
            xpo.setOption('collar,collar-zoom', 'outside-fabric', api.elements['collar-contrast-fabric']);
        }
        var collarImage = api.createElementImage('collar', xpo.elements['collar-zoom']['base'], true, false);
        collarImage += api.createElementImage('collar', xpo.elements['collar-zoom'][api.elements['collar']], false, false);
        if (baseElement) {
            collarImage += api.createElementImage('collar', xpo.elements['collar-zoom'][baseElement], true, false);
        }
        if (annexElement) {
            collarImage += api.createElementImage('collar', xpo.elements['collar-zoom'][annexElement], false, true);
        }
        return collarImage;
    },
    processBackMode: function() {
        var collarContrast = api.elements['collar-contrast'];
        var baseElement = xpo.elements['back-zoom']['base'] ? xpo.elements['back-zoom']['base'] : false;
        xpo.setOption('collar,collar-zoom', 'fabric', api.elements['fabric']);
        if (collarContrast == 'none') {
            xpo.setOption('collar,collar-zoom', 'inside-fabric', api.elements['fabric']);
            xpo.setOption('collar,collar-zoom', 'outside-fabric', api.elements['fabric']);
        } else if (collarContrast == 'inside') {
            xpo.setOption('collar,collar-zoom', 'inside-fabric', api.elements['collar-contrast-fabric']);
            xpo.setOption('collar,collar-zoom', 'outside-fabric', api.elements['fabric']);
        } else if (collarContrast == 'full') {
            xpo.setOption('collar,collar-zoom', 'inside-fabric', api.elements['collar-contrast-fabric']);
            xpo.setOption('collar,collar-zoom', 'outside-fabric', api.elements['collar-contrast-fabric']);
        }
        var collarImage = api.createElementImage('collar', xpo.elements['collar'][api.elements['collar']]['back'], false, false);
        var epauletteImage = "";
        if (api.elements['epaulette'] != 'none' && api.elements['epaulette'] != "") {
            xpo.setOption('epaulette', 'fabric', api.elements['epaulette-contrast-fabric']);
            epauletteImage = api.createElementImage('epaulette', xpo.elements['epaulette'][api.elements['epaulette']]['back'], false, false);
        }
        var elbowPatchImage = "";
        if (api.elements['elbow-patch'] != 'none' && api.elements['elbow-patch'] != "") {
            xpo.setOption('elbow-patch', 'fabric', api.elements['elbow-patch-contrast-fabric']);
            elbowPatchImage = api.createElementImage('elbow-patch', xpo.elements['elbow-patch'][api.elements['elbow-patch']]['back'], false, false);
        }
        var sleeveImage = api.createElementImage('sleeve', xpo.elements['sleeve'][api.elements['sleeve']]['back'], false, false);
        var backImage = api.createElementImage('back-zoom', xpo.elements['back-zoom'][api.elements['bottom-cut']][api.elements['back']], false, false);
        if (baseElement) {
            backImage += api.createElementImage('back-zoom', baseElement, true, false);
        }
        return collarImage + backImage + sleeveImage + epauletteImage + elbowPatchImage;
    },
    processCuff: function() {
        if (api.elements['cuff'] == '') return '';
        var cuffContrast = api.elements['cuff-contrast'];
        var annexElement = xpo.elements['cuff'][api.elements['cuff']][api.elements['cuff-shape']]['annex'] ? xpo.elements['cuff'][api.elements['cuff']][api.elements['cuff-shape']]['annex'] : false;
        var pipingContrast = api.elements['cuff-piping'];
        if (cuffContrast == 'none' || cuffContrast == '') {
            xpo.setOption('cuff', 'inside-fabric', api.elements['fabric']);
            xpo.setOption('cuff', 'outside-fabric', api.elements['fabric']);
        } else if (cuffContrast == 'inside') {
            xpo.setOption('cuff', 'inside-fabric', api.elements['cuff-contrast-fabric']);
            xpo.setOption('cuff', 'outside-fabric', api.elements['fabric']);
        } else if (cuffContrast == 'full') {
            xpo.setOption('cuff', 'inside-fabric', api.elements['cuff-contrast-fabric']);
            xpo.setOption('cuff', 'outside-fabric', api.elements['cuff-contrast-fabric']);
        }
        var cuffPipingImage = "";
        if (pipingContrast == 'none') {
            xpo.setOption('cuff-piping', 'piping-fabric', '');
        } else if (pipingContrast == 'placket') {
            xpo.setOption('cuff-piping', 'piping-fabric', api.elements['cuff-piping-fabric']);
            cuffPipingImage = api.createElementImage('cuff-piping', xpo.elements['cuff-piping'][api.elements['cuff-piping']], false, false);
        }
        var cuffImage = api.createElementImage('cuff', xpo.elements['cuff'][api.elements['cuff']][api.elements['cuff-shape']], false, false);
        if (annexElement) {
            cuffImage += api.createElementImage('cuff', xpo.elements['cuff'][api.elements['cuff']][annexElement], false, true);
        }
        return cuffImage + cuffPipingImage;
    },
    processPocket: function() {
        var pocketContrast = api.elements['pocket-contrast'];
        var pocket = api.elements['pocket'];
        if (pocketContrast == 'none') {
            xpo.setOption('pocket', 'fabric', api.elements['fabric']);
        } else if (pocketContrast == 'yes') {
            xpo.setOption('pocket', 'fabric', api.elements['pocket-contrast-fabric']);
        }
        var pocketImage = "";
        if (pocket == 'none' || pocket == '') {} else {
            pocketImage = api.createElementImage('pocket', xpo.elements['pocket'][api.elements['pocket']], false, false);
        }
        return pocketImage;
    },
    processButtons: function() {
        api.setButtons();
    },
    processThreadColors: function() {
        api.setThreadColors();
    },
    processEpaulette: function() {
        var epaulette = api.elements['epaulette'];
        if (epaulette == 'none' || epaulette == '') {
            xpo.setOption('epaulette', 'fabric', '');
            return "";
        } else if (epaulette == 'yes') {
            xpo.setOption('epaulette', 'fabric', api.elements['epaulette-contrast-fabric']);
            var epauletteImage = api.createElementImage('epaulette', xpo.elements['epaulette'][api.elements['epaulette']], false, false);
            return epauletteImage;
        }
    },
    processElbowPatch: function() {
        var elbowPatch = api.elements['elbow-patch'];
        if (elbowPatch == 'none' || elbowPatch == '') {
            xpo.setOption('elbow-patch', 'fabric', '');
            return "";
        } else if (elbowPatch == 'yes') {
            xpo.setOption('elbow-patch', 'fabric', api.elements['elbow-patch-contrast-fabric']);
            var elbowPatchImage = api.createElementImage('elbow-patch', xpo.elements['elbow-patch'][api.elements['elbow-patch']], false, false);
            return elbowPatchImage;
        }
    },
    createElementImage: function(element, valueNode, isBase, isAnnex) {
        var index = xpo.elements[element]['index'];
        index = !isBase ? index : index - 1;
        index = !isAnnex ? index : index + 1;
        var query = '?1=1&width=#width#' + xpo.getElementQueryString(element, valueNode);
        var url = xpo.base + valueNode['option'] + query;
        var img = '<img src="#url#" data-render="#element#" style="z-index:#index#;" />';
        img = img.replace('#url#', url);
        img = img.replace('#element#', element);
        img = img.replace('#index#', index);
        img = img.replace('#width#', api.width);
        return img;
    },
    showLoader: function(element) {
        $('<div class="bsc-loader"><div>').appendTo(element).hide().stop(true, false).fadeIn('fast');
    },
    hideLoader: function(element) {
        element.find('.bsc-loader').stop(true, false).fadeOut('fast').remove();
    },
    preloadShirt: function(elements) {},
    render: function() {
        var shirtElements = "";
        api.setPrimaryFabric();
        api.processButtons();
        api.processThreadColors();
        var mode = api.elements['mode'];
        if (mode == 'collar') {
            shirtElements += api.processCollarMode();
        } else if (mode == 'back') {
            shirtElements += api.processBackMode();
        } else {
            api.processMonogram();
            shirtElements += api.processBottomCut();
            shirtElements += api.processSleeve();
            shirtElements += api.processCollar();
            shirtElements += api.processCollarTopButton();
            shirtElements += api.processCuff();
            shirtElements += api.processPocket();
            shirtElements += api.processPlacket();
            shirtElements += api.processEpaulette();
            shirtElements += api.processElbowPatch();
        }
        $('.shirt-container').html(shirtElements);
    }
};
$.fn.exists = function() {
    return $(this).length > 0;
};
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};
String.prototype.toDate = function(format) {
    var normalized = this.replace(/[^a-zA-Z0-9]/g, '-');
    var normalizedFormat = format.toLowerCase().replace(/[^a-zA-Z0-9]/g, '-');
    var formatItems = normalizedFormat.split('-');
    var dateItems = normalized.split('-');
    var monthIndex = formatItems.indexOf("mm");
    var dayIndex = formatItems.indexOf("dd");
    var yearIndex = formatItems.indexOf("yyyy");
    var hourIndex = formatItems.indexOf("hh");
    var minutesIndex = formatItems.indexOf("ii");
    var secondsIndex = formatItems.indexOf("ss");
    var today = new Date();
    var year = yearIndex > -1 ? dateItems[yearIndex] : today.getFullYear();
    var month = monthIndex > -1 ? dateItems[monthIndex] - 1 : today.getMonth() - 1;
    var day = dayIndex > -1 ? dateItems[dayIndex] : today.getDate();
    var hour = hourIndex > -1 ? dateItems[hourIndex] : today.getHours();
    var minute = minutesIndex > -1 ? dateItems[minutesIndex] : today.getMinutes();
    var second = secondsIndex > -1 ? dateItems[secondsIndex] : today.getSeconds();
    return new Date(year, month, day, hour, minute, second);
};
if (!String.prototype.contains) {
    String.prototype.contains = function(arg) {
        return !!~this.indexOf(arg);
    };
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
String.prototype.toCamel = function() {
    return this.replace(/(?:^|\s)\w/g, function(match) {
        return match.toUpperCase();
    });
};
var core = {
    init: function() {
        core.initDrawer();
        core.initSwiper();
        core.initShadowbox();
        core.initFaqs();
        core.initNewsletter();
        core.initDateTimePicker();
        core.initSelectboxes();
        core.initCopyPaste();
        core.initSignIn();
        core.promotions.init();
        core.staticPage.init();
        core.profilePage.init();
        core.perfectScrollbars.init();
        core.giftCertificatePage.init();
        core.categoryPage.init();
        core.productPage.init();
        core.customizationsPage.init();
        core.checkoutPage.init();
    },
    isSignedIn: function() {
        return $('.user-information .profile-id').exists();
    },
    promotions: {
        init: function() {
            core.promotions.modals.init();
            if ($('.design-your-perfect-shirt-page').exists() && !$('body.international').exists()) {
                core.promotions.initMarketingTape()
            }
        },
        modals: {
            init: function() {
                core.promotions.modals.checkAndSetCookies();
                core.promotions.modals.launchModals();
            },
            checkAndSetCookies: function() {
                if (core.isSignedIn()) {
                    Cookies.remove('signup-modal');
                    Cookies.set('signup-modal', 'seen', {
                        expires: 1000
                    });
                } else {
                    if (Cookies.get('signup-modal') == 'seen') return;
                    Cookies.set('signup-modal', 'unseen');
                }
            },
            launchModals: function() {
                if (Cookies.get('signup-modal') == 'unseen') {
                    if ($('.dispatch-index-index').exists() || $('.design-your-perfect-shirt-page').exists() || $('.dispatch-categories-view').exists() || $('.dispatch-pages-view .about-us').exists() || $('.dispatch-pages-view .loyalty-points').exists()) {
                        if ($('body.international').exists()) {} else {
                            setTimeout(function() {
                                $('#shop-loyalty-points-modal-trigger').trigger('click');
                            }, 5000);
                        }
                        Cookies.remove('signup-modal');
                        Cookies.set('signup-modal', 'seen', {
                            expires: 1
                        });
                    }
                }
            }
        },
        initMarketingTape: function() {
            var tape = '<a class="marketing-tape" href="profiles-add/index.php">Try out the Bombay Shirts\' Experience - Get 250 Off on your first Order</a>';
            $('body').prepend(tape).slideUp(0).slideDown();
        }
    },
    initSignIn: function() {
        $('body').on('click', '.sign-in-trigger', function() {
            $('.top-my-account .cm-dialog-opener').trigger('click')
        });
    },
    initDateTimePicker: function() {
        var label = $('label:contains("Date / Time")');
        if (label.exists()) {
            var field = label.parent().find('input');
            var tomorrow = moment().add(1, 'days');
            field.datetimepicker({
                minDate: tomorrow,
                defaultDate: tomorrow,
                disabledHours: [0, 1, 2, 3, 4, 5, 6, 7, 8, 19, 20, 21, 22, 23, 24],
            }).val('');
        }
    },
    initFaqs: function() {
        $('#collapse1').collapse("hide");
        $('#collapse13').collapse("hide");
        $('#collapse14').collapse("hide");
        $('#collapse18').collapse("hide");
        $('#collapse24').collapse("hide");
        var heading = $('#faq h2');
        heading.next('div').siblings('div').slideUp();
        heading.click(function(e) {
            $(e.target).next('div').siblings('div').slideUp();
            $(e.target).next('div').slideToggle();
        });
    },
    initTravelingStylist: function() {
        var tsSubmission = getParameterByName('submitted');
        if (tsSubmission) {
            Shadowbox.open({
                content: '<img src="design/themes/bsc/media/images/homepage/ts-confirmation-popup-mobile-site.jpg" alt="alt" />',
                player: "html",
                title: "Thank you!",
                height: 270,
                width: 300
            });
        }
    },
    initNewsletter: function() {
        $('.newsletter').submit(function(e) {
            $.ajax({
                type: 'POST',
                url: 'subscriber.php',
                data: $('form').serialize(),
                success: function(result) {
                    Shadowbox.open({
                        content: '<p style="text-align:center;margin:20px;text-transform:uppercase;font-size:15px;font-weight:bold;">Thank you for subscribing!</p>',
                        player: "html",
                        title: "Newsletter Subscription",
                        height: 67,
                        width: 276
                    });
                }
            });
            e.preventDefault();
        });
    },
    initSwiper: function() {
        new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            slidesPerView: 1,
            paginationClickable: true,
            spaceBetween: 30,
            loop: true,
            speed: 300,
            autoplay: 2000
        });
    },
    initShadowbox: function() {
        Shadowbox.init();
    },
    mixItUp: {
        init: function() {
            core.mixItUp.initializeFilters();
        },
        initializeFilters: function() {
            if ($('.mixitup').exists()) {
                $('.mixitup:not(.each)').each(function(index, value) {
                    var filterWrapper = $(this).find('.filter-wrapper');
                    var reset = $(this).find('.clear-all');
                    var container = $(this);
                    var filterSet = $(this).find('.filter-set');
                    var pager = $(this).find('[data-pager-element]').data('pager-element');
                    plugins.mixItUp.filters.init(index, filterWrapper, reset, container, filterSet);
                    $(this).mixItUp({
                        load: {
                            page: 1
                        },
                        controls: {
                            enable: true
                        },
                        selectors: {
                            'pagersWrapper': pager
                        },
                        pagination: {
                            limit: 12,
                            pagerClass: 'btn',
                            maxPagers: 6
                        },
                        animation: {
                            enable: false
                        }
                    });
                    $(this).mixItUp({
                        pagination: {
                            limit: 12,
                            pagerClass: 'btn',
                            maxPagers: 6
                        }
                    });
                });
            }
        }
    },
    isTabletOrPhone: function() {
        return ($(window).width() <= 992)
    },
    initDrawer: function() {
        var drawer = $('.drawer').drawer({
            iscroll: {
                mouseWheel: true,
                preventDefault: false
            },
            showOverlay: true
        });
    },
    initCopyPaste: function() {
        if ($('.copy-paste').exists()) {
            var copyPasteElements = $('.copy-paste');
            copyPasteElements.each(function() {
                var inputElement = $(this).parent().find('input');
                $(this).click(function() {
                    inputElement.focus();
                    inputElement.select();
                    document.execCommand('copy');
                });
                inputElement.click(function() {
                    this.focus();
                    this.select();
                    document.execCommand('copy');
                });
            });
        }
    },
    perfectScrollbars: {
        init: function() {
            if (!core.isTabletOrPhone()) {
                var elements = $('[data-plugin="scrollbar"]');
                elements.perfectScrollbar();
            }
        },
        update: function() {
            if (!core.isTabletOrPhone()) {
                $('[data-plugin="scrollbar"]').perfectScrollbar('update');
            }
        },
        reset: function() {
            if (!core.isTabletOrPhone()) {
                $('[data-plugin="scrollbar"]').scrollTop(0).perfectScrollbar('update');
            }
        }
    },
    initSelectboxes: function() {
        $('[data-type="select"]').selectpicker();
    },
    triggerConnectedSelectboxes: function() {
        $('.mainbox-body').on('change', '[data-type=select]', function() {
            var index = $(this)[0].selectedIndex;
            if ($(this).exists()) {
                var id = '#' + $(this).attr('id').replace('_connected', '');
                var connectedId = id + '_connected';
                if ($(id).exists() && $(connectedId).exists()) {
                    $(id).prop('selectedIndex', index);
                    $(connectedId).prop('selectedIndex', index);
                    core.refreshSelectboxes();
                }
            }
        });
    },
    refreshSelectboxes: function() {
        $('[data-type="select"]').selectpicker('refresh');
    },
    scrollTo: function(scrollTo) {
        $('html, body').stop(true, false).animate({
            scrollTop: $(scrollTo).offset().top - 70
        }, 'fast');
    },
    giftCertificatePage: {
        init: function() {
            core.giftCertificatePage.setLoggedInName();
        },
        setLoggedInName: function() {
            var name = $('.top-panel .header-name').text().replace('Hi, ', '');
            $('#gift_cert_sender').val(name);
        }
    },
    staticPage: {
        init: function() {
            core.staticPage.homepage.init();
            core.staticPage.helpPage.init();
            core.staticPage.invitePage.init();
        },
        currencyPointReplacement: function() {
            if ($('.dispatch-pages-view').exists()) {
                if ($('.currency-points').exists()) {
                    $('.currency-points').each(function() {
                        var points = $(this).text();
                        var coefficient = $('.local-coefficient').val();
                        var symbol = $('.local-symbol').val();
                        var currency_points = symbol + (points / coefficient).toFixed(2) + ' in';
                        $(this).text(currency_points);
                    });
                }
            }
        },
        homepage: {
            init: function() {
                if ($('.dispatch-index-index').exists()) {
                    core.staticPage.homepage.initMarketingTape();
                }
            },
            //initMarketingTape: function() {
              //  var tape = '<a class="marketing-tape" href="invite-and-earn.php">Refer a Friend and Earn up to Rs. 1000 on Each Order</a>';
               // $('body').prepend(tape).slideUp(0).slideDown();
            //}
        },
        sizingPage: {
            init: function() {
                if ($('.wysiwyg-content .sizing').exists()) {
                    core.staticPage.sizingPage.initMenu();
                }
            },
            initMenu: function() {
                $('.sizing .buttons .btn').click(function() {
                    if (!$(this).hasClass('active')) {
                        $('.sizing .active').removeClass('active');
                        $(this).addClass('active');
                        $('.sizing-option-set').toggle();
                    }
                });
            }
        },
        invitePage: {
            init: function() {
                if ($('.wysiwyg-content .invite-and-earn').exists()) {
                    core.staticPage.invitePage.initReferralSection();
                }
            },
            initReferralSection: function() {
                $.ajax({
                    url: 'profiles-update',
                    type: 'GET',
                    success: function(data) {
                        var widget = $(data).find('.refer-widget').html();
                        $('.refer-widget').html(widget);
                        core.initCopyPaste();
                        core.profilePage.generateAndReplaceReferralURL();
                    }
                });
            }
        },
        helpPage: {
            init: function() {
                if ($('.wysiwyg-content .help-and-support').exists()) {
                    core.staticPage.helpPage.initMenu();
                    core.staticPage.helpPage.setProfileInformation();
                    core.staticPage.helpPage.whereIsMyPackage();
                    core.staticPage.helpPage.initValidators();
                    core.staticPage.helpPage.initForms();
                }
            },
            initMenu: function() {
                $('.help-and-support .buttons .btn').click(function() {
                    if (!$(this).hasClass('active')) {
                        $('.help-and-support .buttons .active').removeClass('active');
                        $(this).addClass('active');
                        $('.help-and-support-set').toggle();
                    }
                });
            },
            setProfileInformation: function() {
                if (core.isSignedIn()) {
                    var firstname = $('.user-information .profile-firstname').val();
                    var lastname = $('.user-information .profile-lastname').val();
                    var email = $('.user-information .profile-email').val();
                    $('[placeholder="Firstname"]').val(firstname).attr('readonly', 'true');
                    $('[placeholder="Lastname"]').val(lastname).attr('readonly', 'true');
                    $('[placeholder="Email Address"]').val(email).attr('readonly', 'true');
                }
            },
            whereIsMyPackage: function() {
                if (core.isSignedIn()) {
                    $.ajax({
                        url: 'index.php?dispatch=orders.tracking_history',
                        type: 'GET',
                        success: function(data) {
                            var orders = data;
                            $('#where-is-my-package .panel-body').html(orders);
                            var orderPanel = $('#where-is-my-package .profile-tab-header');
                            if (orderPanel.exists()) {
                                orderPanel.first().trigger('click');
                            }
                            core.staticPage.helpPage.initExistingAndAlterations();
                        }
                    });
                }
            },
            initExistingAndAlterations: function() {
                if ($('#where-is-my-package .profile-tab-header').exists()) {
                    var alterations = $('#alterations select');
                    var modifications = $('#modifications select');
                    var logistics = $('#logistics select');
                    $('#where-is-my-package .profile-tab-header').each(function() {
                        var status = $(this).data('status');
                        var ago = parseInt($(this).data('ago'));
                        var text = $(this).find('.order_number').text();
                        var option = '<option value="' + text + '">' + text + '</option>';
                        if (status != "cancelled" && ago <= 35) {
                            logistics.append(option);
                        }
                        if (status != "shipped" && status != "cancelled" && ago <= 35) {
                            modifications.append(option);
                        }
                        if (status == "shipped" && ago <= 35) {
                            alterations.append(option);
                        }
                    });
                    if (logistics.children().length > 0) {
                        $('#logistics p').hide();
                        $('#logistics form').removeClass('hidden');
                    } else {
                        $('#logistics p').text("You don't have any active orders to track.");
                    }
                    if (modifications.children().length > 0) {
                        $('#modifications p').hide();
                        $('#modifications form').removeClass('hidden');
                    } else {
                        $('#modifications p').text("You don't have any active orders.");
                    }
                    if (alterations.children().length > 0) {
                        $('#alterations p').hide();
                        $('#alterations form').removeClass('hidden');
                    } else {
                        $('#alterations p').text("You don't have any recently shipped orders.");
                    }
                } else {
                    var html = $('#where-is-my-package').html();
                    $('#alteration, #questions').html(html);
                }
            },
            initValidators: function() {
                $('.help form .required').attr('required', 'true');
            },
            initForms: function() {
                $('.help form').on('submit', function(e) {
                    e.preventDefault();
                    form = $(this);
                    $.ajax({
                        type: 'post',
                        url: $(this).attr('action'),
                        data: $(this).serialize(),
                        success: function() {
                            form.replaceWith("<p class='notice'>" + form.data('success') + "</p>");
                        }
                    });
                });
            }
        }
    },
    categoryPage: {
        init: function() {
            if ($('.dispatch-categories-view').exists()) {
                core.categoryPage.initProductGallery();
            }
        },
        initProductGallery: function() {
            $('body').on('click', '.gallery-nav', function(e) {
                var parent = $(this).closest('form');
                var prev = parent.find('.owl-prev');
                var next = parent.find('.owl-next');
                if ($(this).hasClass('prev')) {
                    prev.trigger('mouseup');
                } else {
                    next.trigger('mouseup');
                }
                e.preventDefault();
                return false;
            });
        }
    },
    profilePage: {
        init: function() {
            core.profilePage.measurements.init();
            core.profilePage.initAccordion();
            core.profilePage.initSubAccordion();
            core.profilePage.triggerInitialAccordions();
            core.profilePage.initTextSpinners();
            core.profilePage.initTooltips();
            core.refreshSelectboxes();
            core.profilePage.initInputFocus();
            core.profilePage.initReferModal();
            core.profilePage.generateAndReplaceReferralURL();
        },
        refresh: function() {
            core.profilePage.initTextSpinners();
            core.profilePage.initTooltips();
            core.refreshSelectboxes();
        },
        resetAccordionTabs: function() {
            $('.profile-tab').stop(true, false).hide();
            $('.profile-tab-header.active').removeClass('active');
        },
        hasMeasurements: function() {
            var measurement = parseInt($('input.spinner').first().val());
            return (measurement != 0);
        },
        initAccordion: function() {
            $('body').on('click', '.profile-tab-header', function() {
                core.profilePage.resetAccordionTabs();
                core.profilePage.resetSubAccordionTabs();
                $(this).next().show();
                $(this).addClass('active');
            });
        },
        triggerInitialAccordions: function() {
            if (getParameterByName('user_id') == null) {
                $('.profile-tab-header:eq(1)').trigger('click');
                $('.dispatch-orders-tracking_history .profile-tab-header:eq(0)').trigger('click');
            } else {
                $('.profile-tab-header:eq(0)').trigger('click');
            }
        },
        resetSubAccordionTabs: function() {
            $('.profile-sub-tab').stop(true, false).hide();
        },
        initSubAccordion: function() {
            $('body').on('click', '.profile-sub-tab-header', function() {
                var linked = $(this).data('link');
                core.profilePage.resetSubAccordionTabs();
                if (linked) {
                    $(linked).show();
                    core.scrollTo(linked);
                } else {
                    $(this).next().show();
                }
                $(this).closest('.measurements-menu').find('.active').removeClass('active');
                $(this).addClass('active');
            });
        },
        measurements: {
            gender: '',
            size: '',
            fit: '',
            initializing: true,
            changed: true,
            init: function() {
                core.profilePage.measurements.initUndo();
                core.profilePage.measurements.genderChange();
                core.profilePage.measurements.sizeChange();
                core.profilePage.measurements.fitChange();
                core.profilePage.measurements.sendUsAShirtChange();
                core.profilePage.measurements.initialize();
                core.profilePage.measurements.initMeasurementSwitcher();
                core.profilePage.measurements.initMeasurementsOverlay();
                core.profilePage.measurements.smartSizing.init();
            },
            initMeasurementsOverlay: function() {
                if ($('.measurement-overlay').exists()) {
                    core.profilePage.measurements.changed = false;
                }
                $('.measurement-overlay .btn').click(function(e) {
                    $(this).parent().fadeOut('fast');
                    $(this).parent().next().css('height', 'auto');
                    core.profilePage.measurements.changed = true;
                    e.preventDefault();
                });
            },
            initMeasurementSwitcher: function() {
                $('.btn-measurements').click(function(e) {
                    var linked = $(this).data('link');
                    core.profilePage.resetSubAccordionTabs();
                    $(linked).show();
                    e.preventDefault();
                });
            },
            setModifiedDateTime: function() {
                if ($('.modified').exists()) {
                    var datetime = new Date();
                    var email = typeof $('#email').val() != "undefined" ? $('#email').val() : $('.email').text();
                    var dt = $.format.date(datetime, "HH:mm ddd, D MMM yyyy by ") + email;
                    $('.modified input').val(dt);
                    $('.modified p').html('<strong>Last updated on ' + dt + '</strong>');
                    return true;
                }
            },
            initUndo: function() {
                $('.mainbox-body').on('click', '.custom-measurements .undo', function() {
                    $('[data-undo]').each(function() {
                        var val = $(this).attr('data-undo');
                        var compare = $(this).val();
                        if (compare == val) return true;
                        $(this).val(val);
                        if ($(this).is('select')) {
                            $(this).val(val).trigger('change');
                        }
                    });
                });
                $('.custom-measurements .undo').qtip({
                    content: {
                        text: 'This will revert your measurements back to your saved measurements.'
                    },
                    style: {
                        classes: 'qtip-dark'
                    }
                });
            },
            disableDropdowns: function(elements) {
                $(elements).prop('selectedIndex', 0).attr('disabled', 'disabled').trigger('change');
            },
            enableDropdowns: function(elements) {
                $(elements).removeAttr('disabled').trigger('change');
            },
            initialize: function() {
                core.profilePage.measurements.initGenderFitSize();
                $('[data-profile="gender"]').trigger('change');
                core.profilePage.measurements.initGenderFitSize();
                core.profilePage.measurements.initializing = false;
            },
            initGenderFitSize: function() {
                core.profilePage.measurements.gender = core.profilePage.measurements.getDropdownValue('[data-profile="gender"]');
                core.profilePage.measurements.fit = core.profilePage.measurements.getDropdownValue('[data-profile="fit"]');
                core.profilePage.measurements.size = core.profilePage.measurements.getDropdownValue('[data-profile="standard-size-male"]') != "undefined" ? core.profilePage.measurements.getDropdownValue('[data-profile="standard-size-male"]') : core.profilePage.measurements.getDropdownValue('[data-profile="standard-size-female"]');
            },
            getDropdownValue: function(dropdown) {
                return $(dropdown).first().find('option:selected').text().replace('--').replace(/\s\(\d*\)/i, '').toLowerCase();
            },
            toggleStandardSizeDropdowns: function(dropdowns, state) {
                if (state) {
                    $(dropdowns).each(function() {
                        var id = $(this).attr('id');
                        $('[data-id=' + id + ']').parent().show();
                    });
                } else {
                    $(dropdowns).each(function() {
                        var id = $(this).attr('id');
                        $('[data-id=' + id + ']').parent().hide();
                        core.profilePage.measurements.disableDropdowns($(this));
                    });
                }
            },
            dropdownsSelectFirstOption: function(dropdowns) {
                $(dropdowns).each(function() {
                    $(this).prop('selectedIndex', 1).trigger('change');
                    core.refreshSelectboxes();
                });
            },
            sendUsAShirtChange: function() {
                $('.send-a-shirt [type=radio]').change(function() {
                    core.checkoutPage.checkAndToggleContinue();
                });
            },
            genderChange: function() {
                $('.mainbox-body').on('change', '[data-profile="gender"]', function() {
                    if ($(this).prop('selectedIndex') == 0) {
                        core.profilePage.measurements.disableDropdowns('[data-profile="standard-size-male"], [data-profile="standard-size-female"], [data-profile="fit"]');
                        core.profilePage.measurements.gender = "";
                        core.profilePage.measurements.resetSizingValues();
                    } else {
                        core.profilePage.measurements.enableDropdowns('[data-profile="standard-size-male"], [data-profile="standard-size-female"]');
                        var gender = $(this).find('option:selected').text().toLowerCase();
                        core.profilePage.measurements.gender = gender.toLowerCase();
                        if (gender == "male") {
                            core.profilePage.measurements.toggleStandardSizeDropdowns('[data-profile="standard-size-female"]', false);
                            core.profilePage.measurements.toggleStandardSizeDropdowns('[data-profile="standard-size-male"]', true);
                            core.profilePage.measurements.enableDropdowns('[data-profile="fit"]');
                        } else {
                            core.profilePage.measurements.toggleStandardSizeDropdowns('[data-profile="standard-size-female"]', true);
                            core.profilePage.measurements.toggleStandardSizeDropdowns('[data-profile="standard-size-male"]', false);
                            core.profilePage.measurements.disableDropdowns('[data-profile="fit"]');
                        }
                    }
                    core.checkoutPage.checkAndToggleContinue();
                });
            },
            sizeChange: function() {
                $('.mainbox-body').on('change', '[data-profile="standard-size-male"], [data-profile="standard-size-female"]', function() {
                    if ($(this).prop('selectedIndex') == 0) {
                        core.profilePage.measurements.size = "";
                        core.profilePage.measurements.resetSizingValues();
                    } else {
                        var size = $(this).find('option:selected').text();
                        core.profilePage.measurements.size = size.replace(/\s\(\d*\)/i, '').toLowerCase();
                        core.profilePage.measurements.populateSizes();
                    }
                    core.checkoutPage.checkAndToggleContinue();
                });
            },
            fitChange: function() {
                $('.mainbox-body').on('change', '[data-profile=fit]', function() {
                    if ($(this).prop('selectedIndex') == 0) {
                        $('[data-profile="standard-size-male"] option').attr('disabled', 'disabled');
                        core.profilePage.measurements.fit = "";
                        core.profilePage.measurements.resetSizingValues();
                    } else {
                        var fit = $(this).find('option:selected').text().toLowerCase();
                        core.profilePage.measurements.fit = fit;
                        core.profilePage.measurements.populateSizes();
                        var sizeIndex = $('[data-profile="standard-size-male"]').prop('selectedIndex');
                        if (fit == "relaxed") {
                            $('[data-profile="standard-size-male"] option').removeAttr('disabled');
                        } else if (fit == "slim") {
                            $('[data-profile="standard-size-male"] option').removeAttr('disabled');
                            $('[data-profile="standard-size-male"] option:contains("XXL")').attr('disabled', 'disabled');
                            $('[data-profile="standard-size-male"] option:contains("XXXL")').attr('disabled', 'disabled');
                            if (sizeIndex == 6 || sizeIndex == 7) {
                                core.profilePage.measurements.disableDropdowns('[data-profile="standard-size-male"]');
                                core.profilePage.measurements.enableDropdowns('[data-profile="standard-size-male"]');
                            }
                        } else if (fit == "custom") {
                            $('[data-profile="standard-size-male"] option').removeAttr('disabled');
                        }
                    }
                    core.refreshSelectboxes();
                    core.checkoutPage.checkAndToggleContinue();
                });
            },
            initializeSizeLimiters: function() {
                $('[data-shirt]').removeAttr('min');
                $('[data-shirt]').removeAttr('max');
                var sizes = core.profilePage.measurements.getMeasurements();
                for (var element in sizes) {
                    var val = sizes[element];
                    var limiter = measurements.limiters[element];
                    if (limiter) {
                        var min = 0;
                        if (limiter.min.type == "limiter") {
                            min = val - limiter.min.value;
                        } else if (limiter.min.type == "absolute") {
                            min = limiter.min.value;
                        }
                        var max = 0;
                        if (limiter.max.type == "limiter") {
                            max = val + limiter.max.value;
                        } else if (limiter.max.type == "absolute") {
                            max = limiter.max.value;
                        }
                        if (core.profilePage.measurements.fit == "custom" || core.profilePage.measurements.size == "custom") {
                            min = 0;
                            max = 100;
                        }
                        var spinner = $('[data-shirt=' + element + ']');
                        spinner.attr('min', min);
                        spinner.attr('max', max);
                        core.profilePage.resetTextSpinner(spinner, min, max);
                    }
                }
            },
            getMeasurements: function() {
                var gender = core.profilePage.measurements.gender;
                var fit = core.profilePage.measurements.fit;
                var size = core.profilePage.measurements.size;
                if (gender == "male") {
                    var sizes = measurements['male'][fit][size];
                } else if (gender == "female") {
                    var sizes = measurements['female'][size];
                }
                return sizes;
            },
            resetSizingValues: function() {
                var spinner = $('[data-shirt]');
                spinner.val('0.00');
            },
            populateSizes: function() {
                if (!core.profilePage.measurements.initializing) {
                    var sizes = core.profilePage.measurements.getMeasurements();
                    for (var element in sizes) {
                        var spinner = $('[data-shirt=' + element + ']');
                        var val = sizes[element].toFixed(2);
                        spinner.val(val);
                    }
                }
            },
            smartSizing: {
                gender: 'male',
                height: 0,
                size: 'm',
                shape: '',
                fit: 'body',
                size: '',
                init: function() {
                    core.profilePage.measurements.smartSizing.wizards.init();
                    core.profilePage.measurements.smartSizing.initHeightSliders();
                    core.profilePage.measurements.smartSizing.initIntroductionClick();
                    core.profilePage.measurements.smartSizing.initGenderClick();
                    core.profilePage.measurements.smartSizing.initDoneClick();
                    core.profilePage.measurements.smartSizing.initContinueClick();
                    core.profilePage.measurements.smartSizing.initReviewClick();
                },
                wizards: {
                    init: function() {
                        core.profilePage.measurements.smartSizing.wizards.maleWizard.init();
                        core.profilePage.measurements.smartSizing.wizards.femaleWizard.init();
                    },
                    maleWizard: {
                        init: function() {
                            $('#smart-wizard-male').bootstrapWizard({
                                onNext: function(tab, navigation, index) {
                                    if (index == 1) {
                                        var sizeElement = $('[name=male-size]');
                                        if (sizeElement.val() == "") {
                                            sizeElement.data('selectpicker').$button.focus();
                                            core.profilePage.measurements.smartSizing.modalGenerator('We need your size to accurately generate a set of custom measurements.', 'Go back to FitSmart');
                                            return false;
                                        }
                                        var size = sizeElement.val();
                                        var shapeContainer = $('#male-shape');
                                        if (size == "xs" || size == "s") {
                                            shapeContainer.find('.skinny').show();
                                            shapeContainer.find('.athletic').show();
                                            shapeContainer.find('.average').show();
                                            shapeContainer.find('.healthy').show();
                                        } else if (size == "m" || size == "l" || size == "xl") {
                                            shapeContainer.find('.skinny').hide();
                                            shapeContainer.find('.athletic').show();
                                            shapeContainer.find('.average').show();
                                            shapeContainer.find('.healthy').show();
                                        } else if (size == "xxl" || size == "xxxl") {
                                            shapeContainer.find('.skinny').hide();
                                            shapeContainer.find('.athletic').hide();
                                            shapeContainer.find('.average').hide();
                                            shapeContainer.find('.healthy').show();
                                        }
                                    } else if (index == 2) {
                                        var shoulderElement = $('[name=male-shoulder]:checked');
                                        if (!shoulderElement.val()) {
                                            core.profilePage.measurements.smartSizing.modalGenerator('Please confirm whether you have sloping shoulders before moving to the next step.', 'Go back to FitSmart');
                                            return false;
                                        }
                                    } else if (index == 4) {
                                        var shapeElement = $('[name=male-shape]:checked');
                                        if (!shapeElement.val()) {
                                            core.profilePage.measurements.smartSizing.modalGenerator('Please select a body shape before moving to the next step.', 'Go back to FitSmart');
                                            return false;
                                        }
                                    } else if (index == 5) {
                                        var shapeElement = $('[name=male-fit]:checked');
                                        if (!shapeElement.val()) {
                                            core.profilePage.measurements.smartSizing.modalGenerator('Please select your preferred fit before moving to the next step.', 'Go back to FitSmart');
                                            return false;
                                        }
                                    }
                                },
                                onTabShow: function(tab, navigation, index) {}
                            });
                        }
                    },
                    femaleWizard: {
                        init: function() {
                            $('#smart-wizard-female').bootstrapWizard({
                                onNext: function(tab, navigation, index) {
                                    if (index == 1) {
                                        var sizeElement = $('[name=female-size]');
                                        if (sizeElement.val() == "") {
                                            sizeElement.data('selectpicker').$button.focus();
                                            core.profilePage.measurements.smartSizing.modalGenerator('Please select a size before moving to the next step.', 'Go back to FitSmart');
                                            return false;
                                        }
                                    } else if (index == 3) {}
                                },
                                onTabShow: function(tab, navigation, index) {}
                            });
                        }
                    }
                },
                algorithm: {
                    initMeasurementValues: function() {
                        core.profilePage.measurements.smartSizing.gender = $('[name=gender]:checked').val();
                        if (core.profilePage.measurements.smartSizing.gender == 'male') {
                            core.profilePage.measurements.smartSizing.height = smartSizing.rules.male.heights[$('[name=male-height]').slider("option", "value")]['cm'];
                            core.profilePage.measurements.smartSizing.size = $('[name=male-size]').val();
                            core.profilePage.measurements.smartSizing.sizeLabel = $('[name=male-size] option:selected').text();
                            core.profilePage.measurements.smartSizing.shape = $('[name=male-shape]:checked').val();
                            core.profilePage.measurements.smartSizing.fit = $('[name=male-fit]:checked').val();
                            core.profilePage.measurements.smartSizing.shoulder = $('[name=male-shoulder]:checked').val();
                        } else {
                            core.profilePage.measurements.smartSizing.height = smartSizing.rules.female.heights[$('[name=female-height]').slider("option", "value")]['cm'];
                            core.profilePage.measurements.smartSizing.size = $('[name=female-size]').val();
                            core.profilePage.measurements.smartSizing.fit = $('[name=female-fit]:checked').val();
                        }
                    },
                    compute: function() {
                        core.profilePage.measurements.smartSizing.algorithm.initMeasurementValues();
                        var gender = core.profilePage.measurements.smartSizing.gender;
                        if (gender == 'male') {
                            $('[data-profile=gender]').prop('selectedIndex', 1).trigger('change');
                            $('[data-profile=fit]').prop('selectedIndex', 3).trigger('change');
                            $('[data-profile=standard-size-male]').prop('selectedIndex', 8).trigger('change');
                            var shape = core.profilePage.measurements.smartSizing.shape;
                            var size = core.profilePage.measurements.smartSizing.size;
                            var sizes = smartSizing['male']['shape'][shape][size];
                            core.profilePage.measurements.smartSizing.algorithm.populateSizes(sizes);
                            var height = smartSizing.rules.male.getHeight(core.profilePage.measurements.smartSizing.height);
                            sizes = smartSizing['male']['height'][size == 'xs' || 's' || 'm' ? 'm' : 'l'][height];
                            core.profilePage.measurements.smartSizing.algorithm.populateSizes(sizes);
                            var fit = core.profilePage.measurements.smartSizing.fit;
                            var modifications = smartSizing['male']['fit'][fit];
                            core.profilePage.measurements.smartSizing.algorithm.populateSizeModifications(modifications);
                            $('[data-profile=sizing-height]').val(core.profilePage.measurements.smartSizing.height);
                            $('[data-profile=sizing-weight]').val(core.profilePage.measurements.smartSizing.weight);
                            $('[data-profile=sizing-shape] option:contains(' + core.profilePage.measurements.smartSizing.shape.toCamel() + ')').prop('selected', true);
                            $('[data-profile=sizing-fit] option:contains(' + core.profilePage.measurements.smartSizing.fit.toCamel() + ')').prop('selected', true);
                            $('[data-profile=standard-size-male] option:contains(' + core.profilePage.measurements.smartSizing.sizeLabel + ')').prop('selected', true).trigger('change');
                            $('[data-profile=sloping-shoulder]').prop('selectedIndex', core.profilePage.measurements.smartSizing.shoulder == "Yes" ? 2 : 1).trigger('change');
                        } else {
                            $('[data-profile=gender]').prop('selectedIndex', 2).trigger('change');
                            $('[data-profile=fit]').prop('selectedIndex', 0).trigger('change');
                            $('[data-profile=standard-size-female] option:contains(' + core.profilePage.measurements.smartSizing.size + ')').prop('selected', true).trigger('change');
                            var shape = core.profilePage.measurements.smartSizing.shape;
                            var size = core.profilePage.measurements.smartSizing.size;
                            var sizes = measurements['female'][size];
                            core.profilePage.measurements.smartSizing.algorithm.populateSizes(sizes);
                            var height = smartSizing.rules.female.getHeight(core.profilePage.measurements.smartSizing.height);
                            sizes = smartSizing['female']['height'][height];
                            core.profilePage.measurements.smartSizing.algorithm.populateSizes(sizes);
                            var fit = core.profilePage.measurements.smartSizing.fit;
                            var modifications = smartSizing['female']['fit'][fit];
                            core.profilePage.measurements.smartSizing.algorithm.populateSizeModifications(modifications);
                            $('[data-profile=sizing-height]').val(core.profilePage.measurements.smartSizing.height);
                            $('[data-profile=sizing-fit] option:contains(' + core.profilePage.measurements.smartSizing.fit.toCamel() + ')').prop('selected', true);
                        }
                    },
                    getMeasurements: function() {
                        var gender = core.profilePage.measurements.smartSizing.gender;
                        var shape = core.profilePage.measurements.smartSizing.shape;
                        var size = core.profilePage.measurements.smartSizing.size;
                        var fit = core.profilePage.measurements.smartSizing.fit;
                        if (gender == "male") {
                            var sizes = smartSizing['male']['shape'][fit][size];
                        } else if (gender == "female") {}
                        return sizes;
                    },
                    populateSizes: function(sizes) {
                        for (var element in sizes) {
                            var spinner = $('[data-shirt=' + element + ']');
                            var val = sizes[element].toFixed(2);
                            spinner.val(val);
                        }
                    },
                    populateSizeModifications: function(modifications) {
                        for (var element in modifications) {
                            var spinner = $('[data-shirt=' + element + ']');
                            var val = (parseFloat(spinner.val()) + parseFloat(modifications[element])).toFixed(2);
                            spinner.val(val);
                        }
                    }
                },
                initHeightSliders: function() {
                    if ($('[name=male-height]').exists()) {
                        var slider = $('[name=male-height]').slider({
                            orientation: "horizontal",
                            min: 0,
                            max: 16,
                            value: 0,
                            slide: function(event, ui) {
                                $('.ui-slider-handle').qtip('option', 'content.text', '' + smartSizing.rules.male.heights[ui.value]['feet'] + '<br>' + smartSizing.rules.male.heights[ui.value]['cm'] + ' cm');
                            },
                            stop: function(event, ui) {
                                currHeight = smartSizing.rules.male.heights[ui.value]["height"];
                                heightInFeet = smartSizing.rules.male.heights[ui.value]["feet"];
                            }
                        });
                        var heightSliderHandle = $('.ui-slider-handle', slider);
                        heightSliderHandle.qtip({
                            content: '' + smartSizing.rules.male.heights[slider.slider('option', 'value')]['feet'] + '<br>' + smartSizing.rules.male.heights[slider.slider('option', 'value')]['cm'] + ' cm',
                            position: {
                                my: 'bottom center',
                                at: 'top center',
                                container: heightSliderHandle,
                                adjust: {
                                    y: -40
                                }
                            },
                            hide: false,
                            show: {
                                delay: 0,
                                ready: true
                            },
                            style: {
                                classes: 'qtip-light',
                                widget: false,
                                tip: {
                                    width: 16,
                                    height: 8,
                                    corner: 'bottom center'
                                }
                            }
                        });
                    }
                    if ($('[name=female-height]').exists()) {
                        var slider = $('[name=female-height]').slider({
                            orientation: "horizontal",
                            min: 0,
                            max: 15,
                            value: 0,
                            slide: function(event, ui) {
                                $('.ui-slider-handle').qtip('option', 'content.text', '' + smartSizing.rules.female.heights[ui.value]['feet'] + '<br>' + smartSizing.rules.female.heights[ui.value]['cm'] + ' cm');
                            },
                            stop: function(event, ui) {
                                currHeight = smartSizing.rules.female.heights[ui.value]["height"];
                                heightInFeet = smartSizing.rules.female.heights[ui.value]["feet"];
                            }
                        });
                        var heightSliderHandle = $('.ui-slider-handle', slider);
                        heightSliderHandle.qtip({
                            content: '' + smartSizing.rules.female.heights[slider.slider('option', 'value')]['feet'] + '<br>' + smartSizing.rules.female.heights[slider.slider('option', 'value')]['cm'] + ' cm',
                            position: {
                                my: 'bottom center',
                                at: 'top center',
                                container: heightSliderHandle,
                                adjust: {
                                    y: -40
                                }
                            },
                            hide: false,
                            show: {
                                delay: 0,
                                ready: true
                            },
                            style: {
                                classes: 'qtip-light',
                                widget: false,
                                tip: {
                                    width: 16,
                                    height: 8,
                                    corner: 'bottom center'
                                }
                            }
                        });
                    }
                },
                modalGenerator: function(content, button) {
                    var modal = $('#sizing-modal');
                    modal.find('.modal-body p').html(content);
                    modal.find('.modal-footer button').html(button);
                    modal.modal('show');
                },
                initIntroductionClick: function() {
                    $('#intro a').click(function(e) {
                        var link = $(this).attr('href');
                        $(this).closest('.tab-pane').hide();
                        $(link).show();
                        e.preventDefault();
                    });
                },
                initGenderClick: function() {
                    $('[name=gender]').click(function() {
                        var link = '#smart-wizard-' + $(this).val();
                        $('#gender a').attr('href', link);
                    });
                    $('#gender a').click(function(e) {
                        var link = $(this).attr('href');
                        $(this).closest('.tab-pane').hide();
                        $(link).show();
                        e.preventDefault();
                    });
                },
                initDoneClick: function() {
                    $('li.finish a').click(function(e) {
                        if ($(this).closest('.smart-wizard').attr('id') == "smart-wizard-male") {
                            var fitElement = $('[name=male-fit]:checked');
                            if (!fitElement.val()) {
                                core.profilePage.measurements.smartSizing.modalGenerator('Please select your preferred shirt fit before wrapping up.', 'Go back to FitSmart');
                                return false;
                            }
                        } else {
                            var fitElement = $('[name=female-fit]:checked');
                            if (!fitElement.val()) {
                                core.profilePage.measurements.smartSizing.modalGenerator('Please select your preferred shirt fit before wrapping up.', 'Go back to FitSmart');
                                return false;
                            }
                        }
                        core.profilePage.measurements.smartSizing.algorithm.compute();
                        $(this).closest('.smart-wizard').hide();
                        $('#finish').show();
                        e.preventDefault();
                    });
                },
                initContinueClick: function() {
                    $('#finish .continue').click(function(e) {
                        $('.user-sizing .buttons-container .btn').trigger('click');
                        e.preventDefault();
                    });
                },
                initReviewClick: function() {
                    $('#finish .review').click(function(e) {
                        $(this).closest('.tab-pane').hide();
                        core.profilePage.measurements.smartSizing.showCustomMeasurements();
                        e.preventDefault();
                    });
                },
                showCustomMeasurements: function() {
                    $('.custom-measurements').show();
                }
            }
        },
        resetTextSpinner: function(element, min, max) {
            $(element).trigger("touchspin.updatesettings", {
                min: min,
                max: max
            });
        },
        initTextSpinners: function() {
            $('.spinner').each(function() {
                var val = Number($(this).data('undo'));
                var min = Number($(this).attr('min'));
                var max = Number($(this).attr('max'));
                var step = Number($(this).data('step'));
                $(this).TouchSpin({
                    'min': min,
                    'max': max,
                    'step': step,
                    'decimals': 2
                }).val(val.toFixed(2));
            });
        },
        initTooltips: function() {
            $('.hint').each(function() {
                $(this).qtip({
                    content: {
                        text: $(this).next('span').html()
                    },
                    style: {
                        classes: 'qtip-light'
                    }
                });
            });
        },
        initInputFocus: function() {
            $('.measurement-values .input-group').hover(function() {
                var img = $(this).find('input').data('shirt');
                $('.measurement-values .shirt .' + img).stop(true, true).fadeIn();
            }, function() {
                $('.measurement-values .shirt img:not(.default)').stop(true, true).fadeOut();
            });
            $('.measurement-values label').hover(function() {
                $(this).next().trigger('mouseenter');
            }, function() {
                $(this).next().trigger('mouseleave');
            });
        },
        initReferModal: function() {
            $('body').on('click', '.social-sharing a.email', function() {
                var id = $('.user-information .profile-id').val();
                $('.invite-form .remote-id').val(id);
                var form = '<form method="post" action="https://admin.bombayshirts.com/users/ref_invites">' + $('.invite-form-wrapper').html() + '</form>';
                form = form.replace('input', 'input required');
                Shadowbox.open({
                    content: form,
                    player: "html",
                    title: "",
                    height: 265,
                    width: 500,
                    enableKeys: false,
                    options: {
                        onFinish: function() {
                            Event.addBehavior.reload();
                        }
                    }
                });
            });
        },
        generateAndReplaceReferralURL: function() {
            if (!core.isSignedIn()) return;
            if (!$('.refer-widget').exists()) return;
            var name = $('.user-information .profile-firstname').val();
            var email = $('.user-information .profile-email').val();
            var slug = name ? name.toLowerCase() : email.substring(0, email.indexOf("@"));
            var id = $('.user-information .profile-id').val().toString(16);
            var slashtag = id + '?' + slug;
            var url = 'bshrt.co/' + slashtag;
            var urlField = $('.refer-widget .profile-field-wrap input');
            $.ajax({
                url: "https://api.rebrandly.com/v1/links",
                type: "post",
                data: JSON.stringify({
                    "destination": urlField.val(),
                    "domain": {
                        "fullName": "bshrt.co"
                    },
                    "slashtag": id
                }),
                headers: {
                    "apikey": "7696ac5f48544dc4b9a86346ef92dcec"
                },
                dataType: "json",
                contentType: 'application/json',
                success: function(link) {}
            });
            var facebookButton = $('.refer-widget .facebook');
            var twitterButton = $('.refer-widget .twitter');
            var whatsappButton = $('.refer-widget .whatsapp');
            var facebookLink = facebookButton.attr('href').replace('https://www.bombayshirts.com/profiles-add/', url);
            var twitterLink = twitterButton.attr('href').replace('https://www.bombayshirts.com/profiles-add/', 'http://' + url);
            var whatsappLink = whatsappButton.attr('href').replace('https://www.bombayshirts.com/profiles-add/', 'http://' + url);
            facebookButton.attr('href', facebookLink);
            twitterButton.attr('href', twitterLink);
            whatsappButton.attr('href', whatsappLink);
            urlField.val(url);
        }
    },
    checkoutPage: {
        init: function() {
            core.checkoutPage.loyaltyPoints.init();
        },
        validateSizing: function() {
            var sendUsAShirt = $('[data-profile=send-a-shirt]').find('span:contains(Yes)').parent().find('input');
            if (sendUsAShirt.is(':checked')) {
                return true;
            }
            switch ($('[data-profile="gender"]').first().find('option:selected').text().toLowerCase()) {
                case '--':
                    return false;
                    break;
                case 'male':
                    {
                        if ($('[data-profile="fit"]').first().find('option:selected').text() == '--') {
                            return false;
                        } else {
                            if ($('[data-profile="standard-size-male"]').first().find('option:selected').text() == '--') {
                                return false;
                            } else {
                                return true;
                            }
                        }
                    }
                    break;
                case 'female':
                    {
                        if ($('[data-profile="standard-size-female"]').first().find('option:selected').text() == '--' || $('[data-profile="standard-size-female"]').first().find('option:selected').text() == '') {
                            return false;
                        } else {
                            return true;
                        }
                    }
                    break;
            }
            if (parseInt($('[data-shirt="collar"]').val()) == "0") {
                return false;
            }
            return true;
        },
        checkAndToggleContinue: function() {
            if (core.checkoutPage.validateSizing()) {
                $('.user-sizing .buttons-container').show();
                $('.fit-promise').show();
            } else {
                $('.user-sizing .buttons-container').hide();
                $('.fit-promise').hide();
            }
        },
        loyaltyPoints: {
            init: function() {
                core.checkoutPage.loyaltyPoints.typingTrigger();
            },
            typingTrigger: function() {
                $('.ty-reward-points__coupon input').on('input', function() {
                    var btn = $(this).parent().find('button');
                    if ($.trim(this.value).length) {
                        btn.stop(true, false).animate({
                            'background-color': '#336799',
                            'border-color': '#336799',
                            'color': '#fff'
                        }, 'fast').qtip({
                            prerender: true,
                            content: {
                                text: 'Enter as many points as you’d like to use, then click Apply.',
                                button: true
                            },
                            position: {
                                my: 'top right',
                                at: 'bottom left'
                            },
                            style: {
                                classes: 'qtip-dark'
                            }
                        }).qtip('api').show();
                    } else {
                        btn.stop(true, false).animate({
                            'background-color': '#e6e6e6',
                            'border-color': '#e6e6e6',
                            'color': '#000'
                        }, 'fast').qtip('api').hide();
                    }
                });
            }
        }
    },
    productPage: {
        init: function() {
            if ($('.dispatch-products-view:not(.custom_shirt_template)').exists()) {
                core.productPage.checkoutTipClick();
            }
        },
        checkoutTipClick: function() {
            $('.btn-checkout-help').click(function() {
                $(this).find('.details').toggle();
            });
        }
    },
    customizationsPage: {
        initializing: true,
        init: function() {
            setTimeout(core.customizationsPage.initContrastsAndPiping(), 5000);
            core.customizationsPage.initFabricTooltips();
            core.customizationsPage.customizationMenu.init();
            core.customizationsPage.accordions.init();
            core.customizationsPage.customizations.init();
            core.customizationsPage.initializeAddToCart();
            core.customizationsPage.initSwitchView();
            core.customizationsPage.filters.init();
            core.customizationsPage.initDefaultShirt();
            core.customizationsPage.initScrollbar();
            core.customizationsPage.paginationClick();
            core.customizationsPage.initFabricOverlay();
            core.customizationsPage.initIntroJs();
        },
        initIntroJs: function() {
            var intro = introJs(),
                steps = [{
                    element: document.querySelector('.customization-menu [data-customization-group=fabric]'),
                    intro: "Begin by choosing a fabric. Use filters to help you narrow your choices.",
                    position: 'right'
                }, {
                    element: document.querySelector('.customization-menu [data-customization-group=collar] .set'),
                    intro: "Change as few or as many customisations as you like. Recommended options are pre-selected for you.",
                    position: 'right'
                }, {
                    element: document.querySelector('.customization-menu .add-to-cart'),
                    intro: "Add to cart at any time in the process.",
                    position: 'bottom'
                }, {
                    element: document.querySelector('.cart-and-account-grid button'),
                    intro: "Sizing and Fit can be chosen during the checkout process.",
                    position: 'center'
                }];
            intro.setOptions({
                steps: steps,
                hintAnimation: false,
                showBullets: false,
                doneLabel: 'Finish'
            });
            $('.customization-help').click(function() {
                intro.start();
            });
        },
        initFabricOverlay: function() {
            $('.fabric-overlay').click(function() {
                if (!core.isTabletOrPhone()) {
                    $(this).find('.tab-content').stop(true, false).slideToggle();
                } else {
                    $(this).find('.tab-content').toggle();
                    $(this).toggleClass('open');
                }
                $(this).find('.tab-heading i').toggleClass('hidden');
            });
        },
        initPrimaryFabrics: function() {
            $('.filterer').trigger('click');
            var interval = setInterval(function() {
                if ($('#fabric-filterer-wrapper').html() == "") {
                    $('.filterer').trigger('click');
                } else {
                    clearInterval(interval);
                }
            }, 10000);
        },
        initContrastsAndPiping: function() {
            if (!$('.fabric_contrasts_container').exists() && !$('.fabric_piping_container').exists()) return;
            $.when($.ajax({
                url: "https://www.bombayshirts.com/index.php?dispatch=products.contrast_fabrics&skip_result_ids_check=true&features_hash=10-Y&is_ajax=1",
                success: function(result) {
                    $('.fabric_contrasts_container').replaceWith(result.text);
                }
            }), $.ajax({
                url: "https://www.bombayshirts.com/index.php?dispatch=products.contrast_fabrics&skip_result_ids_check=true&features_hash=11-Y&is_ajax=1",
                success: function(result) {
                    $('.fabric_piping_container').replaceWith(result.text);
                }
            })).then(function() {
                core.customizationsPage.cloneContrasts();
                core.mixItUp.init();
            });
        },
        paginationClick: function() {
            $('body').on('click', '.custom_shirt_template .pagination li a', function() {
                var section = $(this).closest('[data-customization]');
                var parent = $(this).closest('[data-plugin=scrollbar]');
                parent.scrollTop(section.offset().top);
                parent.perfectScrollbar('update');
            });
        },
        cloneContrasts: function() {
            var fabrics = $('[data-fabrics=base]').find('.fabric');
            fabrics.clone().insertAfter('[data-fabrics=container-fabric] .filter-wrapper');
            var suede = $('[data-fabrics=base]').find('.suede');
            var leather = $('[data-fabrics=base]').find('.leather');
            leather.clone().insertAfter('[data-fabrics=container-fabric-leather-suede] .filter-wrapper');
            suede.clone().insertAfter('[data-fabrics=container-fabric-leather-suede] .filter-wrapper');
            fabrics.clone().insertAfter('[data-fabrics=container-fabric-leather-suede] .filter-wrapper');
        },
        initScrollbar: function() {
            var fabricsPanel = $('[data-customization-group=fabric]');
            if (!$(fabricsPanel).scrollTop()) {
                $(fabricsPanel).scrollTop($(this).scrollTop() + 2);
                $(fabricsPanel).perfectScrollbar('update');
            }
        },
        removeFabricsLoadingText: function() {
            $('.filters-loading').remove();
        },
        initDefaultShirt: function() {
            $('.fabrics .option').first().trigger('click');
        },
        initSwitchView: function() {
            $('.switch-view').click(function(e) {
                if (api.elements.mode == "full") {
                    api.elements.mode = "back";
                } else {
                    api.elements.mode = "full";
                }
                core.customizationsPage.renderer.render();
                e.preventDefault();
            });
        },
        initFabricTooltips: function() {
            $('body').on('click', '.fabrics [data-plugin="lightbox"]', function() {
                var content = $(this).next('div').html();
                Shadowbox.open({
                    content: content,
                    player: "html",
                    title: '',
                    height: '380'
                });
            });
        },
        initializeAddToCart: function() {
            $('.cart-options-menu .add-to-cart').click(function() {
                if (!core.customizationsPage.customizations.checkError()) return;
                core.customizationsPage.customizations.createCustomizationsString();
                var parent = $(this).closest('.cart-options-menu');
                $('input.qty').val(parent.find('.cm-amount').val());
                $('.customization-options .fabrics .active .btn').trigger('click');
            });
        },
        filters: {
            init: function() {
                var containers = $('[data-filterer=true]');
                if (containers.exists()) {
                    containers.each(function() {
                        core.customizationsPage.filters.setHandler('[data-customization=' + $(this).data('customization') + ']');
                    });
                    core.customizationsPage.filters.advancedFilersToggle();
                    core.customizationsPage.filters.initializeToggle();
                    core.customizationsPage.filters.clearClick();
                    core.customizationsPage.filters.clearAllClick();
                    core.customizationsPage.filters.explodeFabricsFilterHash();
                }
            },
            generateFilterHash: function(container) {
                var HASH_SEPARATOR = '_';
                var HASH_FEATURE_SEPARATOR = '-';
                var features = {};
                var hash = [];
                container.find('input.cm-product-filters-checkbox:checked').each(function() {
                    var elm = $(this);
                    if (!features[elm.data('caFilterId')]) {
                        features[elm.data('caFilterId')] = [];
                    }
                    features[elm.data('caFilterId')].push(elm.val());
                });
                for (var k in features) {
                    hash.push(k + HASH_FEATURE_SEPARATOR + features[k].join(HASH_FEATURE_SEPARATOR));
                }
                return hash.join(HASH_SEPARATOR);
            },
            explodeFabricsFilterHash: function() {
                var hash = window.location.hash.substring(2).replace('features_hash=', '');
                if (window.location.hash.length < 10) return;
                var filterer = $('.fabrics .filterer');
                filterer.attr('href', filterer.data('url') + window.location.hash);
                var HASH_SEPARATOR = '_';
                var HASH_FEATURE_SEPARATOR = '-';
                var features = hash.split(HASH_SEPARATOR);
                $.each(features, function(key, val) {
                    var f = features[key].split(HASH_FEATURE_SEPARATOR);
                    $.each(f, function(key, val) {
                        if (key > 0) {
                            var element = '[data-ca-filter-id=' + f[0] + '][value=' + val + ']';
                            $(element).trigger('click');
                        }
                    });
                });
                $('[data-customization-group=fabric] .secondary-toggle').trigger('click');
            },
            setHandler: function(element) {
                $('body').on('change', element + ' .cm-product-filters-checkbox', function() {
                    var query = '&features_hash=' + core.customizationsPage.filters.generateFilterHash($(element));
                    var filterer = $(element).find('.filterer');
                    var baseUrl = filterer.data('url');
                    window.location.hash = query;
                    core.customizationsPage.initializing = true;
                    filterer.attr('href', baseUrl + query).trigger('click');
                });
            },
            advancedFilersToggle: function() {
                $('.advanced-filters').click(function() {
                    var parent = $(this).closest('.filter-wrapper');
                    var advancedSet = parent.find('.advanced-set');
                    $(this).find('i,span').toggle();
                    advancedSet.toggle();
                });
            },
            initializeToggle: function() {
                $(' .secondary-toggle').click(function(e) {
                    var icons = $(this).find('i');
                    icons.toggle();
                    $('.filter-wrapper').toggle();
                    e.preventDefault();
                    e.stopPropagation();
                });
            },
            clearClick: function() {
                $('.filter-set .clear').click(function(e) {
                    var filterSet = $(this).closest('.filter-set');
                    filterSet.find('input').prop('checked', false).change();
                    e.preventDefault();
                    e.stopPropagation();
                });
            },
            clearAllClick: function() {
                $('.filter-wrapper .clear-all').click(function(e) {
                    var filterSets = $(this).closest('.filter-wrapper');
                    filterSets.find('input').prop('checked', false);
                    filterSets.find('input').first().change();
                    e.preventDefault();
                    e.stopPropagation();
                });
            }
        },
        accordions: {
            init: function() {
                core.customizationsPage.accordions.accordionTrigger();
                core.customizationsPage.accordions.subAccordionTrigger();
                core.customizationsPage.accordions.initializeDataMore();
            },
            resetAll: function(element) {
                element.closest('.customization-options').find('.accordion-panel').stop(true, false).slideUp(0).removeClass('active');
                element.closest('.customization-options').find('.accordion-heading').removeClass('active');
            },
            subAccordionTrigger: function() {
                $('.customization-options').on('click', '.sub-accordion-heading', function() {
                    var icons = $(this).find('> i');
                    icons.toggle();
                    var panel = $(this).next();
                    if (!panel.hasClass('active')) {
                        panel.stop(true, false).slideToggle(0, function() {
                            core.perfectScrollbars.update();
                        }).addClass('active');
                        $(this).addClass('active');
                    } else {
                        panel.stop(true, false).slideToggle(0).removeClass('active');
                        $(this).removeClass('active');
                    }
                });
            },
            accordionTrigger: function() {
                $('.customization-options').on('click', '.accordion-heading', function() {
                    var icons = $(this).find('> i');
                    icons.toggle();
                    var panel = $(this).next();
                    if (!core.customizationsPage.customizations.checkError()) return;
                    if (!panel.hasClass('active')) {
                        core.customizationsPage.accordions.resetAll($(this));
                        panel.stop(true, false).slideToggle(0, function() {
                            core.perfectScrollbars.update();
                        }).addClass('active');
                        $(this).addClass('active');
                    } else {
                        panel.stop(true, false).slideToggle(0).removeClass('active');
                        $(this).removeClass('active');
                    }
                });
            },
            initializeDataMore: function() {
                $('.more-options').off('click').click(function(e) {
                    var parent = $(this).closest('.customization-options');
                    parent.find('[data-more]').toggleClass('visible');
                    $(this).find('em').toggleClass('hidden');
                    e.preventDefault();
                });
            }
        },
        customizationMenu: {
            currentGroup: $('.customization-options[data-customization-group="fabric"]'),
            init: function() {
                core.customizationsPage.customizationMenu.initializeCloseButton();
                core.customizationsPage.customizationMenu.initializeCustomizationMenu();
            },
            initializeCustomizationMenu: function() {
                $('.customization-menu a[data-customization-group]').click(function() {
                    if (!core.customizationsPage.customizations.checkError()) return;
                    var group = $('.customization-options[data-customization-group=' + $(this).data('customization-group') + ']');
                    core.customizationsPage.renderer.setOption('mode', $(this).data('mode'));
                    if (core.customizationsPage.customizationMenu.currentGroup.data('customization-group') == group.data('customization-group')) {
                        core.customizationsPage.customizationMenu.resetOptionGroups();
                        $(this).removeClass('active');
                        return;
                    }
                    core.customizationsPage.customizationMenu.resetOptionGroups();
                    core.customizationsPage.customizationMenu.initializeOptionGroup(group);
                    core.customizationsPage.customizationMenu.resetCustomizationMenu();
                    $(this).addClass('active');
                    core.customizationsPage.customizationMenu.currentGroup = group;
                    return false;
                });
            },
            initializeCloseButton: function() {
                $('.customization-options .close').click(function() {
                    if (!core.customizationsPage.customizations.checkError()) return;
                    core.customizationsPage.customizationMenu.resetOptionGroups();
                    core.customizationsPage.customizationMenu.resetCustomizationMenu();
                    return false;
                });
            },
            resetCustomizationMenu: function() {
                $('.customization-menu .active').removeClass('active');
            },
            resetOptionGroups: function() {
                core.customizationsPage.customizationMenu.currentGroup = $('.customization-menu');
                if (!core.isTabletOrPhone()) {
                    $('.customization-options').stop(true, false).animate({
                        right: '-800px'
                    }, 'fast');
                } else {
                    $('.customization-options').stop(true, false).animate({
                        right: '-800px'
                    }, 0);
                }
            },
            initializeOptionGroup: function(group) {
                if (!core.isTabletOrPhone()) {
                    group.stop(true, false).animate({
                        right: '0'
                    }, 'fast');
                } else {
                    group.stop(true, false).animate({
                        right: '0'
                    }, 0);
                }
            }
        },
        customizations: {
            init: function() {
                core.customizationsPage.customizations.initializeOptions();
                core.customizationsPage.customizations.initializeTextInputs();
                core.customizationsPage.customizations.fabrics.init();
            },
            initializeOptions: function() {
                $('body').on('click', '.fabrics .option, .no-padding-options .option, .horizontal-options .option, .contrast-options .option', function(e) {
                    var parent = $(this).closest('[data-readable-customization]');
                    var element = parent.data('customization');
                    var elementGroup = parent.closest('[data-customization-group]').data('customization-group');
                    var val = $(this).data('value');
                    var render = parent.data('render');
                    var readableElement = parent.data('readable-customization');
                    var readable = $(this).data('readable-value');
                    var connectedCustomization = $(this).data('connected-customization') ? true : false;
                    var connectedCustomizationValidate = $(this).data('connected-customization-validate') ? true : false;
                    var connectedSubCustomization = $(this).data('connected-sub-customization') ? true : false;
                    core.customizationsPage.customizations.resetActiveOption(parent);
                    core.customizationsPage.customizations.setSelectedValue(element, readable);
                    core.customizationsPage.customizations.triggerNonDefaultCustomizationFlag(elementGroup);
                    core.customizationsPage.customizations.setReadableOption(readableElement, readable);
                    $(this).addClass('active');
                    if (connectedCustomization) {
                        core.customizationsPage.customizations.triggerConnectedCustomization($(this));
                    }
                    if (connectedSubCustomization) {
                        core.customizationsPage.customizations.triggerConnectedSubCustomization($(this));
                    }
                    if (render == false) return;
                    core.customizationsPage.renderer.setOption(element, val);
                    core.customizationsPage.customizations.conditionalOptions.init();
                    if (connectedCustomization) {
                        if (connectedCustomizationValidate) {
                            var validation = core.customizationsPage.customizations.validateConnectedCustomization($(this));
                            if (!validation.value && validation.element) {
                                core.customizationsPage.customizations.setError('Please select a ' + ($('[data-customization=' + validation.element + ']').data('readable-customization')) + '.', validation.element);
                                return false;
                            }
                        }
                    }
                    if (core.isTabletOrPhone()) {
                        var toggle = $(this).closest('[data-readable-customization]').data('toggle');
                        if (toggle == true) {
                            core.customizationsPage.customizationMenu.resetCustomizationMenu();
                            core.customizationsPage.customizationMenu.resetOptionGroups();
                        }
                    }
                    core.customizationsPage.renderer.render();
                });
            },
            initializeTextInputs: function() {
                $('.no-padding-options .input-text').on('input', function() {
                    $(this).alphanum({
                        allowSpace: false,
                        allowNewline: false,
                        allowOtherCharSets: false,
                        maxLength: 3
                    });
                    var element = $(this).data('customization');
                    var val = $(this).val();
                    var readableElement = $(this).data('readable-customization');
                    if (val.length != 0) {
                        $(this).addClass('active');
                    } else {
                        $(this).removeClass('active');
                        core.customizationsPage.customizations.setError('Please type in a value for ' + $(this).data('readable-customization') + '.', $(this).data('customization'));
                    }
                    core.customizationsPage.renderer.setOption(element, val);
                    core.customizationsPage.customizations.setReadableOption(readableElement, val);
                    core.customizationsPage.renderer.render();
                });
            },
            setError: function(message, element) {
                var error = $('#customization-error');
                error.data('error', message);
                error.data('element', element);
                error.data('state', true);
            },
            clearError: function() {
                var errorElement = $('#customization-error');
                errorElement.data('error', '');
                errorElement.data('element', '');
                errorElement.data('state', 'false');
            },
            checkError: function() {
                var error = $('#customization-error');
                if ($('[data-customization=' + error.data('element') + ']').find('.active:not(.btn)').exists()) {
                    core.customizationsPage.customizations.clearError();
                }
                if (error.data('state') == "true" || error.data('state') == true) {
                    var message = error.data('error');
                    var element = $('[data-customization=' + error.data('element') + ']').data('readable-customization');
                    var modal = $('#customization-error-modal');
                    modal.find('.message').text(message);
                    modal.find('.button').text(element);
                    modal.modal('show');
                    return false;
                } else {
                    return true;
                }
            },
            conditionalOptions: {
                init: function() {
                    core.customizationsPage.customizations.conditionalOptions.sleeveCondition();
                    core.customizationsPage.customizations.conditionalOptions.pocketCondition();
                    core.customizationsPage.customizations.conditionalOptions.cuffCondition();
                },
                sleeveCondition: function() {
                    if (api.elements['sleeve'] == "half" || api.elements['sleeve'] == "roll-up") {
                        core.customizationsPage.customizations.deselectElementVariable('cuff', 'Cuff', false);
                        core.customizationsPage.customizations.deselectElementVariable('cuff-shape', 'Cuff Shape', false);
                        core.customizationsPage.customizations.deselectElementVariable('cuff-stiffness', 'Cuff Stiffness', false);
                        core.customizationsPage.customizations.selectNoneForElement('cuff-contrast', 'Cuff Contrast Type');
                        core.customizationsPage.customizations.selectNoneForElement('cuff-piping', 'Cuff Piping Type');
                        core.customizationsPage.customizations.selectNoneForElement('elbow-patch', 'Elbow Patch');
                        if (api.elements['monogram'] == 'cuff') {
                            core.customizationsPage.customizations.conditionalOptions.selectElement('monogram', 'collar');
                        }
                    } else if (api.elements['sleeve'] == "full") {
                        core.customizationsPage.customizations.reinitializeCustomizationVariable('cuff');
                        core.customizationsPage.customizations.reinitializeCustomizationVariable('cuff-shape');
                        core.customizationsPage.customizations.reinitializeCustomizationVariable('cuff-stiffness');
                    }
                },
                pocketCondition: function() {
                    if (api.elements['pocket'] == "none") {
                        core.customizationsPage.customizations.selectNoneForElement('pocket-contrast', 'Pocket Contrast Type');
                    }
                },
                cuffCondition: function() {
                    if (api.elements['cuff'] == "neopolitan" && api.elements['cuff-shape'] != 'rounded') {
                        core.customizationsPage.customizations.conditionalOptions.selectElement('cuff-shape', 'rounded');
                    }
                },
                selectElement: function(section, element) {
                    $('[data-customization=' + section + '] [data-value= ' + element + ']').trigger('click');
                }
            },
            fabrics: {
                element: '.customization-options[data-customization-group=fabric]',
                init: function() {
                    core.customizationsPage.customizations.fabrics.fabricClick();
                },
                fabricClick: function() {
                    $('body').on('click', core.customizationsPage.customizations.fabrics.element + ' .fabrics .option', function() {
                        core.customizationsPage.customizations.setFabricData($(this));
                    });
                }
            },
            selectNoneForElement: function(apiKey, customizationKey) {
                api.elements[apiKey] = "";
                core.customizationsPage.customizations.elements[customizationKey] = "";
                var ele = 'div[data-customization=' + apiKey + ']';
                var noneElement = $(ele).find('[data-value=none]');
                if (!noneElement.hasClass('active')) {
                    noneElement.trigger('click');
                }
            },
            deselectElementVariable: function(apiKey, customizationKey, deselect) {
                api.elements[apiKey] = "";
                core.customizationsPage.customizations.elements[customizationKey] = "";
                var ele = 'div[data-customization=' + apiKey + ']';
                if (deselect) {
                    $(ele).find('.active').removeClass('active');
                }
            },
            reinitializeCustomizationVariable: function(customization) {
                var ele = 'div[data-customization=' + customization + ']';
                var val = $(ele).find('.active:not(.btn)').data('value');
                if (val != null) {
                    var readable = $(ele).find('.active:not(.btn)').data('readable-value');
                    var readableElement = $(ele).data('readable-customization');
                    var render = $(ele).data('render');
                    core.customizationsPage.customizations.setReadableOption(readableElement, readable);
                    if (render == false) return;
                    core.customizationsPage.renderer.setOption(customization, val);
                }
            },
            shareableUrl: {
                create: function() {
                    var string = "";
                    window.location.hash = "";
                    $.each(api.elements, function(key, val) {
                        if (val != "" && val != "none") {
                            var pair = '&' + key + '=' + val;
                            string += pair;
                        }
                    });
                    window.location.hash = string;
                },
                parse: function() {
                    var string = window.location.hash;
                    string = string.substring(2);
                    var elements = JSON.parse('{"' + decodeURI(string).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}');
                    $.each(elements, function(key, val) {
                        var customization = key;
                        var value = val;
                        var customizationElement = $('div[data-customization=' + customization + ']');
                        customizationElement.find('[data-value=' + value + ']').trigger('click');
                    });
                }
            },
            elements: {
                'Fabric': 'BC001',
                'Bottom Cut': 'Rounded',
                'Sleeve': 'Full Sleeves',
                'Collar': 'Spread Eagle',
                'Collar Top Button': 'Single',
                'Collar Stiffness': 'Stiff',
                'Collar Contrast Type': 'None',
                'Collar Contrast Fabric': '',
                'Collar Piping Type': 'None',
                'Collar Piping Fabric': '',
                'Cuff': 'Single Convertible',
                'Cuff Shape': 'Rounded',
                'Cuff Stiffness': 'Stiff',
                'Cuff Contrast Type': 'None',
                'Cuff Contrast Fabric': '',
                'Cuff Piping Type': 'None',
                'Cuff Piping Fabric': '',
                'Button': 'White',
                'Button Thread Color': 'White',
                'Pocket Style': 'None',
                'Pocket Contrast Type': 'None',
                'Pocket Contrast Fabric': '',
                'Placket': 'Regular',
                'Placket Button': 'Single',
                'Placket Contrast Type': 'None',
                'Placket Contrast Fabric': '',
                'Placket Piping Type': 'None',
                'Placket Piping Fabric': '',
                'Epaulette': 'None',
                'Epaulette Fabric': '',
                'Elbow Patch': 'None',
                'Elbow Patch Fabric': '',
                'Monogram': 'None',
                'Monogram Text': '',
                'Monogram Color': '',
                'Back': 'None'
            },
            resetActiveOption: function(parent) {
                parent.find('.active:not(.btn,li)').removeClass('active');
            },
            setSelectedValue: function(element, val) {
                $('.set[data-customization=' + element + '], [data-customization=' + element + '] .set').find('span').html(val);
            },
            triggerNonDefaultCustomizationFlag: function(element) {
                $('.customization-menu [data-customization=' + element + '] span em').removeClass('hidden').fadeOut(0).fadeIn('slow');
            },
            setReadableOption: function(element, val) {
                core.customizationsPage.customizations.elements[element] = val;
            },
            createCustomizationsString: function() {
                var customizations = "";
                var customized = {};
                for (var option in core.customizationsPage.customizations.elements) {
                    if (core.customizationsPage.customizations.elements[option] != '' && core.customizationsPage.customizations.elements[option] != null) {
                        var variable = option;
                        var val = core.customizationsPage.customizations.elements[option];
                        customizations += option + ': ' + core.customizationsPage.customizations.elements[option] + ', ';
                        customized[variable] = val;
                    }
                }
                customizations = customizations.substring(0, customizations.length - 2);
                $('label:contains("Customizations")').parent().find('textarea').val(customizations);
            },
            setFabricData: function(fabric) {
                var symbol = fabric.find('.price .price-num').first();
                var price = fabric.find('.price .price-num').last();
                var title = fabric.find('.product-title').first().text();
                var sku = fabric.find('.product-sku').text();
                var points = fabric.find('.loyalty-points strong').text();
                var description = fabric.find('.fabric-description').html();
                var composition = fabric.find('.composition').text();
                var count = fabric.find('.count').text();
                var weight = fabric.find('.weight').html();
                var ply = fabric.find('.ply').html();
                var weave = fabric.find('.weave').html();
                $('.generation-container .product-price').text(symbol.text() + price.text());
                $('.generation-container .title').text(title);
                var fabricInformation = $('.generation-container .fabric-overlay');
                fabricInformation.find('.tab-heading').css({
                    'background-color': 'rgba(0, 0, 0, 1)',
                    'border-color': '#000',
                    'color': '#fff'
                }).animate({
                    'background-color': 'rgba(230, 230, 230, 0.99)',
                    'border-color': '#fff',
                    'color': '#000'
                });
                fabricInformation.find('.product-title').text(title);
                fabricInformation.find('.product-sku').text(sku);
                fabricInformation.find('.loyalty-points strong').text(points);
                fabricInformation.find('.fabric-description').html(description);
                fabricInformation.find('.composition').text(composition);
                fabricInformation.find('.count').text(count);
                fabricInformation.find('.weight').html(weight);
                fabricInformation.find('.ply').html(ply);
                fabricInformation.find('.weave').html(weave);
                fabricInformation.find('.price .price-num').first().text(symbol.text());
                fabricInformation.find('.price .price-num').last().text(price.text());
            },
            validateConnectedCustomization: function(option) {
                var options = option.data('connected-customization').split(',');
                var ret = {
                    value: true,
                    element: ''
                };
                options.forEach(function(item, index) {
                    var connectedCustomization = '[data-customization=' + item + ']';
                    var selectedCustomization = $(connectedCustomization).find('.active:not(.btn)').exists();
                    if (selectedCustomization == false) {
                        ret.value = false;
                        ret.element = item;
                    }
                });
                return ret;
            },
            triggerConnectedCustomization: function(option) {
                var options = option.data('connected-customization').split(',');
                options.forEach(function(item, index) {
                    var connectedCustomization = 'div[data-customization=' + item + '], .div[data-customization=' + item + ']';
                    var connectedCustomizationState = option.data('connected-customization-state');
                    if (connectedCustomizationState) {
                        $(connectedCustomization).show().removeClass('removed');
                        core.customizationsPage.customizations.reinitializeCustomizationVariable(item, $(connectedCustomization).data('readable-customization'));
                    } else {
                        $(connectedCustomization).hide().addClass('removed');
                        var deselect = $(connectedCustomization).data('deselect') == false ? false : true;
                        core.customizationsPage.customizations.deselectElementVariable(item, $(connectedCustomization).data('readable-customization'), deselect);
                        core.customizationsPage.customizations.clearError();
                    }
                });
            },
            triggerConnectedSubCustomization: function(option) {
                var options = option.data('connected-sub-customization').split(',');
                var subsection = option.data('connected-sub');
                subsection = '[data-customization=' + subsection + ']';
                options.forEach(function(item, index) {
                    var connectedCustomization = '[data-value=' + item + ']';
                    var connectedCustomizationState = option.data('connected-sub-customization-state');
                    if (connectedCustomizationState) {
                        $(subsection).find(connectedCustomization).show();
                    } else {
                        $(subsection).find(connectedCustomization).hide();
                    }
                });
            }
        },
        renderer: {
            init: function() {
                api.initializeXpo();
                core.customizationsPage.renderer.render();
            },
            setOption: function(element, value) {
                api.setElement(element, value);
            },
            render: function() {
                api.render();
            }
        }
    }
};
Shadowbox.init({
    handleOversize: "drag",
    modal: false,
    initialHeight: 200
});
$.ceEvent('on', 'ce.formpre_profile_form', function(data) {
    core.profilePage.measurements.setModifiedDateTime();
});
$.ceEvent('on', 'ce.formpre_step_three_sizing_form', function(data) {
    if (core.checkoutPage.validateSizing()) {
        if (core.profilePage.measurements.changed) {
            core.profilePage.measurements.setModifiedDateTime();
        }
        return true;
    } else {
        $('#sizing-modal').modal('show');
        return false;
    }
});
$.ceEvent('on', 'ce.formajaxpost_step_one_register_form', function(data) {
    gtag('event', 'conversion', {
        'send_to': 'AW-914419152/O5nQCN3kinoQ0NuDtAM'
    });
    fbq('track', 'CompleteRegistration');
});
var plugins = {
    mixItUp: {
        filters: {
            $filters: [],
            $reset: [],
            $container: [],
            groups: [],
            outputArray: [],
            outputString: [],
            init: function(index, filterWrapper, reset, container, filterSet) {
                var self = this;
                self.$filters[index] = filterWrapper;
                self.$reset[index] = $('#reset');
                self.$container[index] = container;
                self.groups[index] = [];
                self.outputArray[index] = [];
                filterSet.each(function() {
                    self.groups[index].push({
                        $inputs: $(this).find('input'),
                        active: [],
                        tracker: false
                    });
                });
                self.bindHandlers(index);
            },
            bindHandlers: function(index) {
                var self = this;
                self.$filters[index].on('change', function() {
                    self.parseFilters(index);
                });
            },
            parseFilters: function(index) {
                var self = this;
                for (var i = 0, group = []; group = self.groups[index][i]; i++) {
                    group.active = [];
                    group.$inputs.each(function() {
                        $(this).is(':checked') && group.active.push(this.value);
                    });
                    group.active.length && (group.tracker = 0);
                }
                self.concatenate(index);
            },
            concatenate: function(index) {
                var self = this,
                    cache = '',
                    crawled = false,
                    checkTrackers = function(index) {
                        var done = 0;
                        for (var i = 0, group; group = self.groups[index][i]; i++) {
                            (group.tracker === false) && done++;
                        }
                        return (done < self.groups[index].length);
                    },
                    crawl = function(index) {
                        for (var i = 0, group; group = self.groups[index][i]; i++) {
                            group.active[group.tracker] && (cache += group.active[group.tracker]);
                            if (i === self.groups[index].length - 1) {
                                self.outputArray[index].push(cache);
                                cache = '';
                                updateTrackers(index);
                            }
                        }
                    },
                    updateTrackers = function(index) {
                        for (var i = self.groups[index].length - 1; i > -1; i--) {
                            var group = self.groups[index][i];
                            if (group.active[group.tracker + 1]) {
                                group.tracker++;
                                break;
                            } else if (i > 0) {
                                group.tracker && (group.tracker = 0);
                            } else {
                                crawled = true;
                            }
                        }
                    };
                self.outputArray[index] = [];
                do {
                    crawl(index);
                }
                while (!crawled && checkTrackers(index));
                self.outputString[index] = self.outputArray[index].join();
                !self.outputString[index].length && (self.outputString[index] = 'all');
                if (self.$container[index].mixItUp('isLoaded')) {
                    self.$container[index].mixItUp('filter', self.outputString[index]);
                }
            }
        }
    }
};
window.lazySizesConfig = window.lazySizesConfig || {};
$(document).ready(function() {
    Raven.context(function() {
        core.init();
    });
    tracking.init();
});
$(window).load(function() {
    core.initTravelingStylist();
    core.customizationsPage.initPrimaryFabrics();
});
$(document).ajaxStop(function() {
    if ($('.user-sizing').is(':visible')) {
        Shadowbox.clearCache();
        Shadowbox.setup();
        core.profilePage.refresh();
        core.profilePage.init();
    }
    if ($('.custom_shirt_template').exists()) {
        if (core.customizationsPage.initializing) {
            core.customizationsPage.removeFabricsLoadingText();
            core.customizationsPage.initDefaultShirt();
            core.customizationsPage.initializing = false;
        }
        core.perfectScrollbars.reset();
    }
});
$(document).ajaxError(function(event, jqXHR, ajaxSettings, thrownError) {
    Raven.captureMessage(thrownError || jqXHR.statusText, {
        extra: {
            type: ajaxSettings.type,
            url: ajaxSettings.url,
            data: ajaxSettings.data,
            status: jqXHR.status,
            error: thrownError || jqXHR.statusText,
            response: jqXHR.responseText.substring(0, 100)
        }
    });
});
var tracking = {
    init: function() {
        tracking.intercom.init();
        tracking.ga.init();
        tracking.facebook.init();
        tracking.adwords.init();
    },
    adwords: {
        object: undefined,
        init: function() {
            tracking.adwords.object = window[window['gtag']] || gtag;
            tracking.adwords.initAddToCartClicks();
        },
        track: function(event, details) {
            tracking.adwords.object('event', event, details);
        },
        initAddToCartClicks: function() {
            $('body').on('click', '.custom-product .product-form .add-to-cart-container button', function() {
                var form = $(this).closest('form');
                var title = form.find('.product-title').first().text();
                var currency = $('meta[itemprop="priceCurrency"]').attr('content');
                var price = form.find('.price-num').last().text();
                var sku = form.find('.product-sku-code').val();
                var callback = function() {
                    if (typeof(url) != 'undefined') {
                        window.location = url;
                    }
                };
                tracking.adwords.track('conversion', {
                    'send_to': 'AW-914419152/W44CCImQoXoQ0NuDtAM',
                    'value': price,
                    'currency': currency,
                    'event_callback': callback
                });
            });
            $('body').on('click', '.pagination-container .grid-list .add-to-cart-container button', function() {
                var form = $(this).closest('form');
                var title = form.find('.product-title').first().text();
                var currency = $('meta[itemprop="priceCurrency"]').attr('content');
                var price = form.find('.price-num').last().text();
                var sku = form.find('.product-sku-code').val();
                var callback = function() {
                    if (typeof(url) != 'undefined') {
                        window.location = url;
                    }
                };
                tracking.adwords.track('conversion', {
                    'send_to': 'AW-914419152/W44CCImQoXoQ0NuDtAM',
                    'value': price,
                    'currency': currency,
                    'event_callback': callback
                });
            });
            $('body').on('click', '.collections-product .product-block-button .add-to-cart', function() {
                var form = $(this).closest('form');
                var title = form.find('.product-title').first().text();
                var currency = $('meta[itemprop="priceCurrency"]').attr('content');
                var price = form.find('.price-num').last().text();
                var sku = form.find('.product-sku-code').val();
                var callback = function() {
                    if (typeof(url) != 'undefined') {
                        window.location = url;
                    }
                };
                tracking.adwords.track('conversion', {
                    'send_to': 'AW-914419152/W44CCImQoXoQ0NuDtAM',
                    'value': price,
                    'currency': currency,
                    'event_callback': callback
                });
            });
        }
    },
    facebook: {
        object: undefined,
        init: function() {
            tracking.facebook.object = window[window['fbq']] || fbq;
            tracking.facebook.initAddToCartClicks();
        },
        track: function(event, details) {
            tracking.facebook.object('track', event, details);
        },
        initAddToCartClicks: function() {
            $('body').on('click', '.custom-product .product-form .add-to-cart-container button', function() {
                var form = $(this).closest('form');
                var title = form.find('.product-title').first().text();
                var currency = $('meta[itemprop="priceCurrency"]').attr('content');
                var price = form.find('.price-num').last().text();
                var sku = form.find('.product-sku-code').val();
                tracking.facebook.track('AddToCart', {
                    content_name: title,
                    content_ids: [sku],
                    content_type: 'product',
                    value: price,
                    currency: currency
                });
            });
            $('body').on('click', '.pagination-container .grid-list .add-to-cart-container button', function() {
                var form = $(this).closest('form');
                var title = form.find('.product-title').first().text();
                var currency = $('meta[itemprop="priceCurrency"]').attr('content');
                var price = form.find('.price-num').last().text();
                var sku = form.find('.product-sku-code').val();
                tracking.facebook.track('AddToCart', {
                    content_name: title,
                    content_ids: [sku],
                    content_type: 'product',
                    value: price,
                    currency: currency
                });
            });
            $('body').on('click', '.collections-product .product-block-button .add-to-cart', function() {
                var form = $(this).closest('form');
                var title = form.find('.product-title').first().text();
                var currency = $('meta[itemprop="priceCurrency"]').attr('content');
                var price = form.find('.price-num').last().text();
                var sku = form.find('.product-sku-code').val();
                tracking.facebook.track('AddToCart', {
                    content_name: title,
                    content_ids: [sku],
                    content_type: 'product',
                    value: price,
                    currency: currency
                });
            });
        }
    },
    ga: {
        object: undefined,
        init: function() {
            tracking.ga.object = window[window['GoogleAnalyticsObject'] || 'ga'];
            tracking.ga.initAddToCartClicks();
            tracking.ga.initRegisterationClicks();
        },
        track: function(details) {
            if (typeof tracking.ga.object == 'function') {
                tracking.ga.object('send', details);
            }
        },
        initRegisterationClicks: function() {
            $('body').on('click', '[rel=nofollow]:contains("Register")', function() {
                tracking.ga.track({
                    hitType: 'event',
                    eventCategory: 'Registration',
                    eventAction: 'Intent to register',
                    eventLabel: 'Top navigation'
                });
            });
            $('body').on('click', '[name="dispatch[profiles.update]"]:contains("Register")', function() {
                tracking.ga.track({
                    hitType: 'event',
                    eventCategory: 'Registration',
                    eventAction: 'Successfully registered',
                    eventLabel: 'Registration page'
                });
            });
        },
        initAddToCartClicks: function() {
            $('body').on('click', '.custom-product .product-form .add-to-cart-container button', function() {
                var form = $(this).closest('form');
                var title = form.find('.product-title').first().text();
                var price = form.find('.price-num').first().text() + form.find('.price-num').last().text();
                var quantity = form.find('input.qty').val();
                var sku = form.find('.product-sku-code').val();
                tracking.ga.track({
                    hitType: 'event',
                    eventCategory: 'Add to cart',
                    eventAction: 'Clicked on customization page',
                    eventLabel: sku
                });
            });
            $('body').on('click', '.pagination-container .grid-list .add-to-cart-container button', function() {
                var form = $(this).closest('form');
                var title = form.find('.product-title').first().text();
                var price = form.find('.price-num').first().text() + form.find('.price-num').last().text();
                var sku = form.find('.product-sku-code').val();
                tracking.ga.track({
                    hitType: 'event',
                    eventCategory: 'Add to cart',
                    eventAction: 'Clicked on category page',
                    eventLabel: sku
                });
            });
            $('body').on('click', '.collections-product .product-block-button .add-to-cart', function() {
                var form = $(this).closest('form');
                var title = form.find('.product-title').first().text();
                var price = form.find('.price-num').first().text() + form.find('.price-num').last().text();
                var sku = form.find('.product-sku-code').val();
                tracking.ga.track({
                    hitType: 'event',
                    eventCategory: 'Add to cart',
                    eventAction: 'Clicked on product page',
                    eventLabel: sku
                });
            });
        }
    },
    intercom: {
        enabled: true,
        init: function() {
            tracking.intercom.initializeProfile();
            tracking.intercom.initAddToCartClicks();
            tracking.intercom.initCustomizationPage();
            tracking.intercom.initPaymentGatewayClicked();
            tracking.intercom.initCheckoutClicked();
            tracking.intercom.initCheckoutComplete();
        },
        initializeProfile: function() {
            var userInformation = $('.user-information');
            if (userInformation.exists()) {
                var id = userInformation.find('.profile-id').val();
                var email = userInformation.find('.profile-email').val();
                var gender = "";
                var fit = "";
                var smartWeight = "";
                var smartHeight = "";
                var smartShape = "";
                var smartFit = "";
                var userProfile = $('[data-profile=gender]');
                if (userProfile.exists()) {
                    gender = $('[data-profile=gender] option:selected').first().text();
                    fit = $('[data-profile=fit] option:selected').first().text();
                    size = gender == "Male" ? $('[data-profile=standard-size-male] option:selected').first().text() : $('[data-profile=standard-size-female] option:selected').first().text();
                    smartWeight = $('[data-profile=sizing-weight]').val();
                    smartHeight = $('[data-profile=sizing-height]').val();
                    smartShape = $('[data-profile=sizing-shape] option:selected').first().text();
                    smartFit = $('[data-profile=sizing-fit] option:selected').first().text();
                    window.Intercom('update', {
                        user_id: id,
                        'gender': gender,
                        'fit': fit,
                        'size': size,
                        'smart_weight': smartWeight,
                        'smart_height': smartHeight,
                        'smart_shape': smartShape,
                        'smart_fit': smartFit
                    });
                }
            }
        },
        initAddToCartClicks: function() {
            if (tracking.intercom.enabled && typeof window.Intercom == 'function') {
                $('body').on('click', '.custom-product .product-form .add-to-cart-container button', function() {
                    var form = $(this).closest('form');
                    var title = form.find('.product-title').first().text();
                    var price = form.find('.price-num').first().text() + form.find('.price-num').last().text();
                    var quantity = form.find('input.qty').val();
                    var sku = form.find('.product-sku-code').val();
                    window.Intercom('trackEvent', 'added-product-to-cart', {
                        'SKU': sku,
                        'Product': title,
                        'Quantity': quantity,
                        'Price': price,
                        'From': 'Custom'
                    });
                });
                $('body').on('click', '.pagination-container .grid-list .add-to-cart-container button', function() {
                    var form = $(this).closest('form');
                    var title = form.find('.product-title').first().text();
                    var price = form.find('.price-num').first().text() + form.find('.price-num').last().text();
                    var sku = form.find('.product-sku-code').val();
                    window.Intercom('trackEvent', 'added-product-to-cart', {
                        'SKU': sku,
                        'Product': title,
                        'Quantity': 1,
                        'Price': price,
                        'From': 'Category'
                    });
                });
                $('body').on('click', '.collections-product .product-block-button .add-to-cart', function() {
                    var form = $(this).closest('form');
                    var title = form.find('.product-title').first().text();
                    var price = form.find('.price-num').first().text() + form.find('.price-num').last().text();
                    var sku = form.find('.product-sku-code').val();
                    window.Intercom('trackEvent', 'added-product-to-cart', {
                        'SKU': sku,
                        'Product': title,
                        'Quantity': 1,
                        'Price': price,
                        'From': 'Product'
                    });
                });
            }
        },
        initCustomizationPage: function() {
            if ($('.custom_shirt_template').exists()) {
                if (tracking.intercom.enabled && typeof window.Intercom == 'function') {
                    $('.customization-help').click(function() {
                        window.Intercom('trackEvent', 'triggered-customization-help');
                    });
                    $('.trial-shirt-tip a').click(function() {
                        window.Intercom('trackEvent', 'triggered-trial-shirt-modal');
                    });
                }
            }
        },
        initPaymentGatewayClicked: function() {
            if (tracking.intercom.enabled && typeof window.Intercom == 'function') {
                $('body').on('click', '.payments-list .list-group-item label', function() {
                    var gateway = $(this).find('strong').text();
                    window.Intercom('trackEvent', 'payment-gateway-clicked', {
                        'Gateway': gateway
                    });
                });
            }
        },
        initCheckoutClicked: function() {
            if (tracking.intercom.enabled && typeof window.Intercom == 'function') {
                $('body').on('click', 'button.cm-checkout-place-order', function() {
                    var quantity = $('.cart-and-account-grid .dropdown-toggle .quantity').text();
                    var amount = $('<div>' + $('.checkout-summary .products-total').html() + '</div>').remove('span').text();
                    window.Intercom('trackEvent', 'checkout-clicked', {
                        'Quantity': quantity,
                        'Price': amount
                    });
                });
            }
        },
        initCheckoutComplete: function() {}
    }
};
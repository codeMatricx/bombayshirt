<?php get_header(); ?>
<main>
    <?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>
<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
    <div class="navigation index">
       <div class="alignleft"><?php next_posts_link( 'Older Entries' ); ?></div>
       <div class="alignright"><?php previous_posts_link( 'Newer Entries' ); ?></div>
    </div><!--end navigation-->
<?php else : ?>
<?php endif; ?>
</main>
                <section class="content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12">
							   <div id="jssor_1" class="main-banners-block homepage-banners" style="position:relative;margin:0 auto;top:0px;left:0px;width:1263px;height:500px;overflow:hidden;visibility:hidden;">
									<!-- Loading Screen -->
									<div data-u="loading" class="jssorl-009-spin " style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
										<img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo get_theme_file_uri('/img/spin.svg'); ?>" />
									</div>
									<div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;">
										<div data-p="225.00">
											<img data-u="image" src="<?php echo get_theme_file_uri('/img/1.jpg'); ?>" />
										</div>
										<div data-p="225.00">
											<img data-u="image" src="<?php echo get_theme_file_uri('/img/2.jpg'); ?>"  />
										</div>
										<div data-p="225.00">
											<img data-u="image" src="<?php echo get_theme_file_uri('/img/3.jpg'); ?>"  />
										</div>
									</div>
									<!-- Bullet Navigator -->
									<div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
										<div data-u="prototype" class="i" style="width:16px;height:16px;">
											<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
												<circle class="b" cx="8000" cy="8000" r="5800"></circle>
											</svg>
										</div>
									</div>
									<!-- Arrow Navigator -->
									<div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
										<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
											<polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
										</svg>
									</div>
									<div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
										<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
											<polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
										</svg>
									</div>
								</div>
								<!-- #endregion Slider End -->
								
								
                                <div class="shop-custom-made-heading-block">
                                    <div class="shop-custom-made-heading">
                                        <h2 class="hidden-sm hidden-xs"><strong>Custom-made in Noida</strong>,<br>with pride</h2>
                                        <h2 class="hidden-md hidden-lg"><strong>Custom-made in Noida</strong>,<br>with pride</h2>
                                        <p> <span class="hidden-md hidden-lg">
												Lorem ipsum dolor sit amet, nam ea saepe oporteat lobortis. Cum exerci malorum ne, an eam agam cibo convenire. Nec ad velit imperdiet, his vero fastidii maluisset cu. <br /><br />
												Mea graeci maiestatis ne, ei partem scribentur pro, ullum invidunt nam ei. An vis adhuc qualisque, id facilis gubergren sed. Est ea eirmod fastidii, ne pri amet detraxit. Qui velit reformidans cu.<br /> </span> <span class="hidden-xs hidden-sm">
												Modus suscipiantur vix ut. Eam ut omnes nominavi urbanitas, te esse quidam expetendis vis, nec idque tation pertinacia te. Ut sed postea commune insolens, nam no ferri causae luptatum, qui eu elit sale. <br /><br />
												Duis insolens contentiones sed ne. Legere menandri salutandi mel et, ius at vide erant omnesque.<br /> </span> <a href="http://staging.isiwal.com/SunnysBespoke1/?page_id=96" class="btn btn-primary btn-bordered">About Us</a> </p>
                                    </div>
                                </div>
                                <div class="why-buy-from-us-block">
                                    <div class="why-buy-from-us row">
                                        <div class="col-md-12 two-split">
                                            <div class="row">
                                                <div class="col-md-12 overlay design">
                                                    <div class="row hidden-xs hidden-sm">
                                                        <div class="col-md-6 col-md-offset-6">
                                                            <h2><strong>Impeccable Quality</strong><br><span>Rem mollis antiguo diam poenam dui ipsam.<br>Indirecte gennere non-consilium.<br>Immaculate finish.</span></h2>
                                                            <p> <a href="http://staging.isiwal.com/SunnysBespoke1/?page_id=109" class="btn btn-primary btn-bordered">Learn More</a> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 hidden-md hidden-lg">
                                                    <div class="row">
                                                        <div class="col-md-6 col-md-offset-6">
                                                            <h2><strong>Impeccable Quality</strong><br><span>The finest fabrics from around the world.<br>Carefully sourced raw-materials.<br>Immaculate finish.</span></h2>
                                                            <p> <a href="http://localhost/bombay/how-to-select-a-�tom-shirt-fabric/" class="btn btn-primary btn-bordered">Learn More</a> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 two-split">
                                            <div class="row">
                                                <div class="col-md-12 overlay alt fitsmart">
                                                    <div class="row hidden-xs hidden-sm">
                                                        <div class="col-md-6 col-md-offset-1">
                                                            <h2><strong>Custom Size in 1 Minute</strong><br><span>No measuring tapes.<br>The perfect fit or we remake for free.</span></h2>
                                                            <p> <a href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=105" class="btn btn-primary btn-bordered">Learn More</a> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 hidden-md hidden-lg">
                                                    <div class="row">
                                                        <div class="col-md-6 col-md-offset-6">
                                                            <h2><strong>Custom Size in 1 Minute</strong><br><span>No measuring tapes.<br>The perfect fit or we remake for free.</span></h2>
                                                            <p> <a href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=105" class="btn btn-primary btn-bordered">Learn More</a> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 two-split">
                                            <div class="row">
                                                <div class="col-md-12 overlay pricing">
                                                    <div class="row hidden-xs hidden-sm">
                                                        <div class="col-md-6 col-md-offset-6">
                                                            <h2><strong>Don't Pay Over the Top</strong><br><span>Custom-made at Readymade prices.<br>Ships worldwide in 14 days.<br>Cash back on every order.</span></h2>
                                                            <p class="united-states-hidden"> <a href="http://staging.isiwal.com/SunnysBespoke1/?page_id=115" class="btn btn-primary btn-bordered">Learn More</a> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 hidden-md hidden-lg">
                                                    <div class="row">
                                                        <div class="col-md-6 col-md-offset-6">
                                                            <h2><strong>Don't Pay Over the Top</strong><br><span>Custom-made at Readymade prices.<br>Ships worldwide in 14 days.<br>Cash back on every order.</span></h2>
                                                            <p class="united-states-hidden"> <a href="http://staging.isiwal.com/SunnysBespoke1/?page_id=115" class="btn btn-primary btn-bordered">Learn More</a> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="the-store-experience-block">
                                    <div class="the-store-experience">
                                        <div class="text-center"> <a class="cta" href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=223" class="btn">Start Your Design</a> </div> <img class="hidden-xs hidden-sm" src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/homepage/store-experience.jpg" alt="Bombay Shirt Company'); ?>" /> <img class="hidden-lg hidden-md" src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/homepage/store-experience-mobile.jpg'); ?>" alt="Bombay Shirt Company" />
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                            <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                            <br>
                                            <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <a href="http://staging.isiwal.com/SunnysBespoke1/?page_id=113">profile</a>.
                                            <br />
                                            <br> <a href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=77" class="btn btn-primary btn-bordered">Store Locations</a> </p>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col-lg-12">
                                <div class="press-block">
                                    <div class="press">
                                        <div class="row hidden-xs">
                                            <a href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=87">
                                                <div class="col-md-2">
                                                    <div class="mention"> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/homepage/popshot-magazine-logo.png'); ?>" />
                                                        <p>"6 best online stores for men"
                                                            <br />"Bespoke for the people"</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="mention"> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/homepage/footer-logo@2x.png'); ?>" />
                                                        <p>"Top 100 online stores"</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="mention"> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/homepage/cmc-logo-retina.png'); ?>" />
                                                        <p>"The key is their attention to detail"</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="mention"> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/homepage/Entrepreneur-Magazine-Logo.png'); ?>" />
                                                        <p>"The Noida Shirt Company flagship store is a tailored shirt fan's delight."</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="mention"> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/homepage/SmartBusiness_logo.jpg'); ?>" />
                                                        <p>"A hip addition to the Kalaghoda neighbourhood"</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="mention"> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/homepage/footer-logo@2x.png'); ?>" />
                                                        <p>India Business Report
                                                            <br />January 2016</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 hidden-lg hidden-md hidden-sm">
                                                <div class="swiper-container">
                                                    <div class="swiper-wrapper">
                                                        <div class="swiper-slide">
                                                            <div class="mention"> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/homepage/press-gq.png'); ?>" />
                                                                <p>"6 best online stores for men"
                                                                    <br />"Bespoke for the people"</p>
                                                            </div>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <div class="mention"> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/homepage/elite-business.jpg'); ?>" />
                                                                <p>"Top 100 online stores"</p>
                                                            </div>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <div class="mention"> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/homepage/footer-logo@2x.png'); ?>" />
                                                                <p>"The key is their attention to detail"</p>
                                                            </div>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <div class="mention"> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/homepage/Entrepreneur-Magazine-Logo.png'); ?>" />
                                                                <p>"The Noida Shirt Company flagship store is a tailored shirt fan's delight."</p>
                                                            </div>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <div class="mention"> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/homepage/SmartBusiness_logo.jpg'); ?>" />
                                                                <p>"A hip addition to the Kalaghoda neighbourhood"</p>
                                                            </div>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <div class="mention"> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/homepage/cmc-logo-retina.png'); ?>" />
                                                                <p>India Business Report
                                                                    <br />January 2016</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-pagination"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
   <!--footer-->
   <?php get_footer(); ?>            
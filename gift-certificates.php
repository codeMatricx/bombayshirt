﻿<?php /* Template Name: gift-certificates */ ?>
<?php get_header(); ?>

                <section class="dispatch-gift_certificates-add content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12">
                                <div class="gift-certificate-header-block">
                                    <div class="wysiwyg-content">
                                        <div>
                                            <div class="row">
                                                <div class="col-md-12 border">
                                                    <h1 class="heading">Gift Voucher</h1>
                                                    <hr class="heading-separator">
                                                    <h2 class="sub-heading">Gift for a special someone?<br /> Or even your boss?<br />
													A SSC gift card is the answer - we'll create an account for the recipient and credit the amount as Loyalty Points.</h2> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col-lg-6 main-content-grid">
                                <div class="gift-photo-block pull-right">
                                    <div class="wysiwyg-content"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/gift-voucher/voucher.jpg'); ?>" /></div>
                                </div>
                            </section>
                            <section class="col-lg-6 main-content-grid">
                                <div class="main-content-block">
                                    <div class="ty-gift-certificate">
                                        <form class="cm-ajax cm-ajax-full-render" action="#" method="post" target="_self" name="gift_certificates_form">
                                            <input type="hidden" name="redirect_url" value="index.html?dispatch=gift_certificates.add" />
                                            <div class="ty-control-group">
                                                <label for="gift_cert_sender" class="ty-control-group__title cm-required">Your Name</label>
                                                <input type="text" id="gift_cert_sender" name="gift_cert_data[sender]" class="ty-input-text-full" value="" /> </div>
                                            <div class="ty-control-group">
                                                <label for="gift_cert_recipient" class="ty-control-group__title cm-required">Receiver's Name</label>
                                                <input type="text" id="gift_cert_recipient" name="gift_cert_data[recipient]" class="ty-input-text-full cm-focus" value="" /> </div>
                                            <div class="ty-gift-certificate__switch clearfix">
                                                <div class="ty-gift-certificate__switch-label gift-send-right">How to send?</div>
                                                <div class="ty-gift-certificate__switch-mail">
                                                    <div class="ty-gift-certificate__send">
                                                        <input type="radio" name="gift_cert_data[send_via]" value="E" checked="checked" class="radio" id="sw_gc_switcher_suffix_e" />
                                                        <label for="sw_gc_switcher_suffix_e" class="ty-valign">Send via email</label>
                                                    </div>
                                                    <div class="ty-gift-certificate__send">
                                                        <input type="radio" name="gift_cert_data[send_via]" value="P" class="radio" id="sw_gc_switcher_suffix_p" />
                                                        <label for="sw_gc_switcher_suffix_p" class="ty-valign">Send via postal mail</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="gc_switcher">
                                                <div class="ty-gift-certificate__block " id="email_block">
                                                    <div class="ty-control-group">
                                                        <label for="gift_cert_email" class="cm-required cm-email ty-control-group__title">Receiver's Email</label>
                                                        <input type="text" id="gift_cert_email" name="gift_cert_data[email]" class="ty-input-text-full" value="" /> </div>
                                                    <div class="ty-control-group">
                                                        <input id="gift_cert_template" type="hidden" name="gift_cert_data[template]" value="default.tpl" /> </div>
                                                </div>
                                                <div class="ty-gift-certificate__block hidden" id="post_block">
                                                    <div class="ty-control-group">
                                                        <label for="gift_cert_phone" class="ty-control-group__title">phone</label>
                                                        <input type="text" id="gift_cert_phone" name="gift_cert_data[phone]" class="ty-input-text-full" size="50" value="" /> </div>
                                                    <div class="ty-control-group">
                                                        <label for="gift_cert_address" class="ty-control-group__title cm-required">Address</label>
                                                        <input type="text" id="gift_cert_address" name="gift_cert_data[address]" class="ty-input-text-full" size="50" value="" /> </div>
                                                    <div class="ty-control-group">
                                                        <input type="text" id="gift_cert_address_2" name="gift_cert_data[address_2]" class="ty-input-text-full" size="50" value="" /> </div>
                                                    <div class="ty-control-group">
                                                        <label for="gift_cert_city" class="ty-control-group__title cm-required">city</label>
                                                        <input type="text" id="gift_cert_city" name="gift_cert_data[city]" class="ty-input-text-full" size="50" value="" /> </div>
                                                    <div class="ty-control-group ty-float-left ty-gift-certificate__country country">
                                                        <label for="gift_cert_country" class="ty-control-group__title cm-required">country</label>
                                                        <select id="gift_cert_country" name="gift_cert_data[country]" class="ty-gift-certificate__select cm-country cm-location-billing">
                                                            <option value="">- Select country -</option>
                                                            <option value="AF">Afghanistan</option>
                                                            <option value="AX">Aland Islands</option>
                                                            <option value="AL">Albania</option>
                                                            <option value="DZ">Algeria</option>
                                                            <option value="AS">American Samoa</option>
                                                            <option value="AD">Andorra</option>
                                                            <option value="AO">Angola</option>
                                                            <option value="AI">Anguilla</option>
                                                            <option value="AQ">Antarctica</option>
                                                            <option value="AG">Antigua and Barbuda</option>
                                                            <option value="AR">Argentina</option>
                                                            <option value="AM">Armenia</option>
                                                            <option value="AW">Aruba</option>
                                                            <option value="AP">Asia-Pacific</option>
                                                            <option value="AU">Australia</option>
                                                            <option value="AT">Austria</option>
                                                            <option value="AZ">Azerbaijan</option>
                                                            <option value="BS">Bahamas</option>
                                                            <option value="BH">Bahrain</option>
                                                            <option value="BD">Bangladesh</option>
                                                            <option value="BB">Barbados</option>
                                                            <option value="BY">Belarus</option>
                                                            <option value="BE">Belgium</option>
                                                            <option value="BZ">Belize</option>
                                                            <option value="BJ">Benin</option>
                                                            <option value="BM">Bermuda</option>
                                                            <option value="BT">Bhutan</option>
                                                            <option value="BO">Bolivia</option>
                                                            <option value="BA">Bosnia and Herzegowina</option>
                                                            <option value="BW">Botswana</option>
                                                            <option value="BV">Bouvet Island</option>
                                                            <option value="BR">Brazil</option>
                                                            <option value="IO">British Indian Ocean Territory</option>
                                                            <option value="VG">British Virgin Islands</option>
                                                            <option value="BN">Brunei Darussalam</option>
                                                            <option value="BG">Bulgaria</option>
                                                            <option value="BF">Burkina Faso</option>
                                                            <option value="BI">Burundi</option>
                                                            <option value="KH">Cambodia</option>
                                                            <option value="CM">Cameroon</option>
                                                            <option value="CA">Canada</option>
                                                            <option value="CV">Cape Verde</option>
                                                            <option value="KY">Cayman Islands</option>
                                                            <option value="CF">Central African Republic</option>
                                                            <option value="TD">Chad</option>
                                                            <option value="CL">Chile</option>
                                                            <option value="CN">China</option>
                                                            <option value="CX">Christmas Island</option>
                                                            <option value="CC">Cocos (Keeling) Islands</option>
                                                            <option value="CO">Colombia</option>
                                                            <option value="KM">Comoros</option>
                                                            <option value="CG">Congo</option>
                                                            <option value="CK">Cook Islands</option>
                                                            <option value="CR">Costa Rica</option>
                                                            <option value="CI">Cote D&#039;ivoire</option>
                                                            <option value="HR">Croatia</option>
                                                            <option value="CU">Cuba</option>
                                                            <option value="CW">Curaçao</option>
                                                            <option value="CY">Cyprus</option>
                                                            <option value="CZ">Czech Republic</option>
                                                            <option value="DK">Denmark</option>
                                                            <option value="DJ">Djibouti</option>
                                                            <option value="DM">Dominica</option>
                                                            <option value="DO">Dominican Republic</option>
                                                            <option value="TL">East Timor</option>
                                                            <option value="EC">Ecuador</option>
                                                            <option value="EG">Egypt</option>
                                                            <option value="SV">El Salvador</option>
                                                            <option value="GQ">Equatorial Guinea</option>
                                                            <option value="ER">Eritrea</option>
                                                            <option value="EE">Estonia</option>
                                                            <option value="ET">Ethiopia</option>
                                                            <option value="EU">Europe</option>
                                                            <option value="FK">Falkland Islands (Malvinas)</option>
                                                            <option value="FO">Faroe Islands</option>
                                                            <option value="FJ">Fiji</option>
                                                            <option value="FI">Finland</option>
                                                            <option value="FR">France</option>
                                                            <option value="FX">France, Metropolitan</option>
                                                            <option value="GF">French Guiana</option>
                                                            <option value="PF">French Polynesia</option>
                                                            <option value="TF">French Southern Territories</option>
                                                            <option value="GA">Gabon</option>
                                                            <option value="GM">Gambia</option>
                                                            <option value="GE">Georgia</option>
                                                            <option value="DE">Germany</option>
                                                            <option value="GH">Ghana</option>
                                                            <option value="GI">Gibraltar</option>
                                                            <option value="GR">Greece</option>
                                                            <option value="GL">Greenland</option>
                                                            <option value="GD">Grenada</option>
                                                            <option value="GP">Guadeloupe</option>
                                                            <option value="GU">Guam</option>
                                                            <option value="GT">Guatemala</option>
                                                            <option value="GG">Guernsey</option>
                                                            <option value="GN">Guinea</option>
                                                            <option value="GW">Guinea-Bissau</option>
                                                            <option value="GY">Guyana</option>
                                                            <option value="HT">Haiti</option>
                                                            <option value="HM">Heard and McDonald Islands</option>
                                                            <option value="HN">Honduras</option>
                                                            <option value="HK">Hong Kong</option>
                                                            <option value="HU">Hungary</option>
                                                            <option value="IS">Iceland</option>
                                                            <option selected="selected" value="IN">India</option>
                                                            <option value="ID">Indonesia</option>
                                                            <option value="IQ">Iraq</option>
                                                            <option value="IE">Ireland</option>
                                                            <option value="IR">Islamic Republic of Iran</option>
                                                            <option value="IM">Isle of Man</option>
                                                            <option value="IL">Israel</option>
                                                            <option value="IT">Italy</option>
                                                            <option value="JM">Jamaica</option>
                                                            <option value="JP">Japan</option>
                                                            <option value="JE">Jersey</option>
                                                            <option value="JO">Jordan</option>
                                                            <option value="KZ">Kazakhstan</option>
                                                            <option value="KE">Kenya</option>
                                                            <option value="KI">Kiribati</option>
                                                            <option value="KP">Korea</option>
                                                            <option value="KR">Korea, Republic of</option>
                                                            <option value="KW">Kuwait</option>
                                                            <option value="KG">Kyrgyzstan</option>
                                                            <option value="LA">Laos</option>
                                                            <option value="LV">Latvia</option>
                                                            <option value="LB">Lebanon</option>
                                                            <option value="LS">Lesotho</option>
                                                            <option value="LR">Liberia</option>
                                                            <option value="LY">Libyan Arab Jamahiriya</option>
                                                            <option value="LI">Liechtenstein</option>
                                                            <option value="LT">Lithuania</option>
                                                            <option value="LU">Luxembourg</option>
                                                            <option value="MO">Macau</option>
                                                            <option value="MK">Macedonia</option>
                                                            <option value="MG">Madagascar</option>
                                                            <option value="MW">Malawi</option>
                                                            <option value="MY">Malaysia</option>
                                                            <option value="MV">Maldives</option>
                                                            <option value="ML">Mali</option>
                                                            <option value="MT">Malta</option>
                                                            <option value="MH">Marshall Islands</option>
                                                            <option value="MQ">Martinique</option>
                                                            <option value="MR">Mauritania</option>
                                                            <option value="MU">Mauritius</option>
                                                            <option value="YT">Mayotte</option>
                                                            <option value="MX">Mexico</option>
                                                            <option value="FM">Micronesia</option>
                                                            <option value="MD">Moldova, Republic of</option>
                                                            <option value="MC">Monaco</option>
                                                            <option value="MN">Mongolia</option>
                                                            <option value="ME">Montenegro</option>
                                                            <option value="MS">Montserrat</option>
                                                            <option value="MA">Morocco</option>
                                                            <option value="MZ">Mozambique</option>
                                                            <option value="MM">Myanmar</option>
                                                            <option value="NA">Namibia</option>
                                                            <option value="NR">Nauru</option>
                                                            <option value="NP">Nepal</option>
                                                            <option value="NL">Netherlands</option>
                                                            <option value="NC">New Caledonia</option>
                                                            <option value="NZ">New Zealand</option>
                                                            <option value="NI">Nicaragua</option>
                                                            <option value="NE">Niger</option>
                                                            <option value="NG">Nigeria</option>
                                                            <option value="NU">Niue</option>
                                                            <option value="NF">Norfolk Island</option>
                                                            <option value="MP">Northern Mariana Islands</option>
                                                            <option value="NO">Norway</option>
                                                            <option value="OM">Oman</option>
                                                            <option value="PK">Pakistan</option>
                                                            <option value="PW">Palau</option>
                                                            <option value="PS">Palestine Authority</option>
                                                            <option value="PA">Panama</option>
                                                            <option value="PG">Papua New Guinea</option>
                                                            <option value="PY">Paraguay</option>
                                                            <option value="PE">Peru</option>
                                                            <option value="PH">Philippines</option>
                                                            <option value="PN">Pitcairn</option>
                                                            <option value="PL">Poland</option>
                                                            <option value="PT">Portugal</option>
                                                            <option value="PR">Puerto Rico</option>
                                                            <option value="QA">Qatar</option>
                                                            <option value="RS">Republic of Serbia</option>
                                                            <option value="RE">Reunion</option>
                                                            <option value="RO">Romania</option>
                                                            <option value="RU">Russian Federation</option>
                                                            <option value="RW">Rwanda</option>
                                                            <option value="LC">Saint Lucia</option>
                                                            <option value="WS">Samoa</option>
                                                            <option value="SM">San Marino</option>
                                                            <option value="ST">Sao Tome and Principe</option>
                                                            <option value="SA">Saudi Arabia</option>
                                                            <option value="SN">Senegal</option>
                                                            <option value="CS">Serbia</option>
                                                            <option value="SC">Seychelles</option>
                                                            <option value="SL">Sierra Leone</option>
                                                            <option value="SG">Singapore</option>
                                                            <option value="SX">Sint Maarten</option>
                                                            <option value="SK">Slovakia</option>
                                                            <option value="SI">Slovenia</option>
                                                            <option value="SB">Solomon Islands</option>
                                                            <option value="SO">Somalia</option>
                                                            <option value="ZA">South Africa</option>
                                                            <option value="ES">Spain</option>
                                                            <option value="LK">Sri Lanka</option>
                                                            <option value="SH">St. Helena</option>
                                                            <option value="KN">St. Kitts and Nevis</option>
                                                            <option value="PM">St. Pierre and Miquelon</option>
                                                            <option value="VC">St. Vincent and the Grenadines</option>
                                                            <option value="SD">Sudan</option>
                                                            <option value="SR">Suriname</option>
                                                            <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                                            <option value="SZ">Swaziland</option>
                                                            <option value="SE">Sweden</option>
                                                            <option value="CH">Switzerland</option>
                                                            <option value="SY">Syrian Arab Republic</option>
                                                            <option value="TW">Taiwan</option>
                                                            <option value="TJ">Tajikistan</option>
                                                            <option value="TZ">Tanzania, United Republic of</option>
                                                            <option value="TH">Thailand</option>
                                                            <option value="TG">Togo</option>
                                                            <option value="TK">Tokelau</option>
                                                            <option value="TO">Tonga</option>
                                                            <option value="TT">Trinidad and Tobago</option>
                                                            <option value="TN">Tunisia</option>
                                                            <option value="TR">Turkey</option>
                                                            <option value="TM">Turkmenistan</option>
                                                            <option value="TC">Turks and Caicos Islands</option>
                                                            <option value="TV">Tuvalu</option>
                                                            <option value="UG">Uganda</option>
                                                            <option value="UA">Ukraine</option>
                                                            <option value="AE">United Arab Emirates</option>
                                                            <option value="GB">United Kingdom (Great Britain)</option>
                                                            <option value="US">United States</option>
                                                            <option value="VI">United States Virgin Islands</option>
                                                            <option value="UY">Uruguay</option>
                                                            <option value="UZ">Uzbekistan</option>
                                                            <option value="VU">Vanuatu</option>
                                                            <option value="VA">Vatican City State</option>
                                                            <option value="VE">Venezuela</option>
                                                            <option value="VN">Viet Nam</option>
                                                            <option value="WF">Wallis And Futuna Islands</option>
                                                            <option value="EH">Western Sahara</option>
                                                            <option value="YE">Yemen</option>
                                                            <option value="ZR">Zaire</option>
                                                            <option value="ZM">Zambia</option>
                                                            <option value="ZW">Zimbabwe</option>
                                                        </select>
                                                    </div>
                                                    <div class="ty-control-group ty-float-right ty-gift-certificate__state state">
                                                        <label for="gift_cert_state" class="ty-control-group__title cm-required">state</label>
                                                        <select class="ty-gift-certificate__select cm-state cm-location-billing" id="gift_cert_state" name="gift_cert_data[state]">
                                                            <option value="">- Select state -</option>
                                                            <option value="AN">Andaman and Nicobar Islands</option>
                                                            <option value=" AP">Andhra Pradesh</option>
                                                            <option value="AR">Arunachal Pradesh</option>
                                                            <option value="AS">Assam</option>
                                                            <option value="BR">Bihar</option>
                                                            <option value="CH">Chandigarh</option>
                                                            <option value="CT">Chhattisgarh</option>
                                                            <option value="DN">Dadra and Nagar Haveli</option>
                                                            <option value="DD">Daman and Diu</option>
                                                            <option value="DL">Delhi</option>
                                                            <option value="GA">Goa</option>
                                                            <option value="GJ">Gujarat</option>
                                                            <option value="HR">Haryana</option>
                                                            <option value="HP">Himachal Pradesh</option>
                                                            <option value="JK">Jammu and Kashmir</option>
                                                            <option value="JH">Jharkhand</option>
                                                            <option value="KA">Karnataka</option>
                                                            <option value="KL">Kerala</option>
                                                            <option value="LD">Lakshadweep</option>
                                                            <option value="MP">Madhya Pradesh</option>
                                                            <option value="MH" selected="selected">Maharashtra</option>
                                                            <option value="MN">Manipur</option>
                                                            <option value="ML">Meghalaya</option>
                                                            <option value="MZ">Mizoram</option>
                                                            <option value="NL">Nagaland</option>
                                                            <option value="OD">Odisha</option>
                                                            <option value="PY">Puducherry</option>
                                                            <option value="PB">Punjab</option>
                                                            <option value="RJ">Rajasthan</option>
                                                            <option value="SK">Sikkim</option>
                                                            <option value="TN">Tamil Nadu</option>
                                                            <option value="TG">Telangana</option>
                                                            <option value="TR">Tripura</option>
                                                            <option value="UP">Uttar Pradesh</option>
                                                            <option value="UT">Uttarakhand</option>
                                                            <option value="WB">West Bengal</option>
                                                        </select>
                                                        <input type="text" id="gift_cert_state_d" name="gift_cert_data[state]" class="cm-state cm-location-billing ty-input-text hidden" size="50" maxlength="64" value="MH" disabled="disabled" /> </div>
                                                    <div class="ty-control-group zipcode">
                                                        <label for="gift_cert_zipcode" class="ty-control-group__title cm-required cm-zipcode cm-location-billing">Zip/postal code</label>
                                                        <input type="text" id="gift_cert_zipcode" name="gift_cert_data[zipcode]" class="ty-input-text-short" size="50" value="" /> </div>
                                                </div>
                                            </div>
                                            <div class="ty-control-group">
                                                <label for="gift_cert_message" class="ty-control-group__title">Message</label>
                                                <textarea id="gift_cert_message" name="gift_cert_data[message]" cols="49" rows="4" class="ty-input-text-full"></textarea>
                                            </div>
                                            <div class="ty-control-group ty-gift-certificate__amount">
                                                <label for="gift_cert_amount" class="ty-control-group__title cm-required cm-gc-validate-amount">amount</label> <span class="ty-gift-certificate__currency">₹</span>
                                                <input type="text" id="gift_cert_amount" name="gift_cert_data[amount]" class="ty-gift-certificate__amount-input cm-numeric" data-p-sign="s" data-a-sep="" data-a-dec="." size="5" value="100" /> </div>
                                            <div class="ty-gift-certificate__buttons buttons-container">
                                                <div class="ty-float-right ty-gift-certificate__preview-btn">
                                                    <button class="btn ty-btn__tertiary cm-new-window ty-btn" type="submit" name="dispatch[gift_certificates.preview]">Preview</button>
                                                </div>
                                                <input type="hidden" name="result_ids" value="cart_status*,wish_list*,account_info*" />
                                                <input type="hidden" name="redirect_url" value="index.html?dispatch=gift_certificates.add" />
                                                <button class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="">Add to cart</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
<?php get_footer(); ?>               
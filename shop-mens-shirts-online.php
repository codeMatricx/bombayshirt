﻿<?php /* Template Name: Shop men shirts online */ ?>
<?php get_header(); ?>   
		<section class="dispatch-categories-view drawer content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 main-content-grid col-md-12 col-sm-12 col-xs-12">
                                <aside class="sidebox filters-block clearfix">
                                    <h5 class="text-uppercase "> <strong>
										Product filters
										</strong> </h5>
                                    <div class="sidebox-body">
                                        <div class="panel panel-default horizontal-product-filters cm-product-filters cm-horizontal-filters" data-ca-target-id="product_filters_*,products_search_*,category_products_*,product_features_*,breadcrumbs_*,currencies_*,languages_*,selected_filters_*"  id="product_filters_39">
                                            <div class="panel-body">
                                                <ul class="list-inline">
                                                    <li>
                                                        <div class="dropdown">
                                                            <div class="btn-group">
                                                                <div class="btn-group">
                                                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdown_filter_39" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                        Occasion <span class="caret"></span> </button>
                                                                    <div class="dropdown-menu" aria-labelledby="dropdown_filter_39">
                                                                        <div class="form">
                                                                            <ul class="product-filter list-unstyled " id="content_39_1">
                                                                                <li class="product-filters-item-more">
                                                                                    <ul id="ranges_39_1" style="max-height: 20em;" class="product-filter-variants list-unstyled cm-filter-table" data-ca-input-id="elm_search_39_1" data-ca-clear-id="elm_search_clear_39_1" data-ca-empty-id="elm_search_empty_39_1">
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[1]" data-ca-filter-id="1" value="4" id="elm_checkbox_39_1_4">Casual
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[1]" data-ca-filter-id="1" value="5" id="elm_checkbox_39_1_5">Formal
                                                                                            </label>
                                                                                        </li>
                                                                                    </ul>
                                                                                    <p id="elm_search_empty_39_1" class="product-filters-no-items-found hidden">No items found matching the search criteria</p>
                                                                                </li>
                                                                            </ul>
                                                                            <div class="product-filters-tools clearfix"> <a class="btn cm-external-click btn-default  " data-ca-external-click-id="sw_elm_filter_1"> Close</a> </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="dropdown">
                                                            <div class="btn-group">
                                                                <div class="btn-group">
                                                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdown_filter_39" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                        Pattern <span class="caret"></span> </button>
                                                                    <div class="dropdown-menu" aria-labelledby="dropdown_filter_39">
                                                                        <div class="form">
                                                                            <ul class="product-filter list-unstyled " id="content_39_3">
                                                                                <li class="product-filters-item-more">
                                                                                    <ul id="ranges_39_3" style="max-height: 20em;" class="product-filter-variants list-unstyled cm-filter-table" data-ca-input-id="elm_search_39_3" data-ca-clear-id="elm_search_clear_39_3" data-ca-empty-id="elm_search_empty_39_3">
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[3]" data-ca-filter-id="3" value="30" id="elm_checkbox_39_3_30">Checks
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[3]" data-ca-filter-id="3" value="31" id="elm_checkbox_39_3_31">Stripes
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[3]" data-ca-filter-id="3" value="32" id="elm_checkbox_39_3_32">Solids
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[3]" data-ca-filter-id="3" value="33" id="elm_checkbox_39_3_33">Prints
                                                                                            </label>
                                                                                        </li>
                                                                                    </ul>
                                                                                    <p id="elm_search_empty_39_3" class="product-filters-no-items-found hidden">No items found matching the search criteria</p>
                                                                                </li>
                                                                            </ul>
                                                                            <div class="product-filters-tools clearfix"> <a class="btn cm-external-click btn-default  " data-ca-external-click-id="sw_elm_filter_3"> Close</a> </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="dropdown">
                                                            <div class="btn-group">
                                                                <div class="btn-group">
                                                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdown_filter_39" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                        Color <span class="caret"></span> </button>
                                                                    <div class="dropdown-menu" aria-labelledby="dropdown_filter_39">
                                                                        <div class="form">
                                                                            <ul class="product-filter list-unstyled " id="content_39_2">
                                                                                <li>
                                                                                    <div class="form-group"> <i class="product-filter-search-icon bsc-cross-none hidden" id="elm_search_clear_39_2" title="Clear"></i>
                                                                                        <input type="text" placeholder="Search" class="cm-autocomplete-off form-control" name="q" id="elm_search_39_2" value="" /> </div>
                                                                                </li>
                                                                                <li class="product-filters-item-more">
                                                                                    <ul id="ranges_39_2" style="max-height: 20em;" class="product-filter-variants list-unstyled cm-filter-table" data-ca-input-id="elm_search_39_2" data-ca-clear-id="elm_search_clear_39_2" data-ca-empty-id="elm_search_empty_39_2">
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[2]" data-ca-filter-id="2" value="6" id="elm_checkbox_39_2_6">Blacks
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[2]" data-ca-filter-id="2" value="2" id="elm_checkbox_39_2_2">Blues
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[2]" data-ca-filter-id="2" value="3" id="elm_checkbox_39_2_3">Browns
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[2]" data-ca-filter-id="2" value="7" id="elm_checkbox_39_2_7">Greens
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[2]" data-ca-filter-id="2" value="42" id="elm_checkbox_39_2_42">Greys
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[2]" data-ca-filter-id="2" value="44" id="elm_checkbox_39_2_44">Oranges
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[2]" data-ca-filter-id="2" value="45" id="elm_checkbox_39_2_45">Peach
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[2]" data-ca-filter-id="2" value="46" id="elm_checkbox_39_2_46">Pinks
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[2]" data-ca-filter-id="2" value="47" id="elm_checkbox_39_2_47">Purples
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[2]" data-ca-filter-id="2" value="48" id="elm_checkbox_39_2_48">Reds
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[2]" data-ca-filter-id="2" value="49" id="elm_checkbox_39_2_49">Whites
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[2]" data-ca-filter-id="2" value="50" id="elm_checkbox_39_2_50">Yellows
                                                                                            </label>
                                                                                        </li>
                                                                                    </ul>
                                                                                    <p id="elm_search_empty_39_2" class="product-filters-no-items-found hidden">No items found matching the search criteria</p>
                                                                                </li>
                                                                            </ul>
                                                                            <div class="product-filters-tools clearfix"> <a class="btn cm-external-click btn-default  " data-ca-external-click-id="sw_elm_filter_2"> Close</a> </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="price-range">
                                                        <div class="dropdown">
                                                            <div class="btn-group">
                                                                <div class="btn-group">
                                                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdown_filter_39" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                        Price Range <span class="caret"></span> </button>
                                                                    <div class="dropdown-menu" aria-labelledby="dropdown_filter_39">
                                                                        <div class="form">
                                                                            <ul class="product-filter list-unstyled " id="content_39_4">
                                                                                <li class="product-filters-item-more">
                                                                                    <ul id="ranges_39_4" style="max-height: 20em;" class="product-filter-variants list-unstyled cm-filter-table" data-ca-input-id="elm_search_39_4" data-ca-clear-id="elm_search_clear_39_4" data-ca-empty-id="elm_search_empty_39_4">
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[4]" data-ca-filter-id="4" value="37" id="elm_checkbox_39_4_37">1000-1900
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[4]" data-ca-filter-id="4" value="34" id="elm_checkbox_39_4_34">1900-2400
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[4]" data-ca-filter-id="4" value="35" id="elm_checkbox_39_4_35">2400-3700
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[4]" data-ca-filter-id="4" value="36" id="elm_checkbox_39_4_36">3700-5100
                                                                                            </label>
                                                                                        </li>
                                                                                        <li class="checkbox">
                                                                                            <label>
                                                                                                <input class="cm-product-filters-checkbox" type="checkbox" name="product_filters[4]" data-ca-filter-id="4" value="62" id="elm_checkbox_39_4_62">5100+
                                                                                            </label>
                                                                                        </li>
                                                                                    </ul>
                                                                                    <p id="elm_search_empty_39_4" class="product-filters-no-items-found hidden">No items found matching the search criteria</p>
                                                                                </li>
                                                                            </ul>
                                                                            <div class="product-filters-tools clearfix"> <a class="btn cm-external-click btn-default  " data-ca-external-click-id="sw_elm_filter_4"> Close</a> </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </aside>
                                <div class="main-content-block">
                                    <div id="category_products_11">
                                        <div class="pagination-container cm-pagination-container" id="pagination_contents">
                                            <div>
                                                <a data-ca-scroll=".cm-pagination-container" href="#" data-ca-page="" data-ca-target-id="pagination_contents" class="hidden"></a>
                                            </div>
                                            <div class="panel panel-default sorting">
                                                <div class="panel-body">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Newest Items First <span class="caret"></span> </button>
                                                        <ul class="dropdown-menu">
                                                            <li class="sort-by-price-asc"> <a class="cm-ajax" data-ca-target-id="pagination_contents" href="#" rel="nofollow">Sort by Price: Low to High</a> </li>
                                                            <li class="sort-by-price-desc"> <a class="cm-ajax" data-ca-target-id="pagination_contents" href="#" rel="nofollow">Sort by Price: High to Low</a> </li>
                                                            <li class="sort-by-popularity-desc"> <a class="cm-ajax" data-ca-target-id="pagination_contents" href="#" rel="nofollow">Sort by Popularity</a> </li>
                                                            <li class="sort-by-bestsellers-desc"> <a class="cm-ajax" data-ca-target-id="pagination_contents" href="#" rel="nofollow">Sort by Bestselling</a> </li>
                                                        </ul>
                                                    </div>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            36 Per Page <span class="caret"></span> </button>
                                                        <ul class="dropdown-menu">
                                                            <li> <a class="cm-ajax" href="#" data-ca-target-id="pagination_contents" rel="nofollow">12 Per Page</a> </li>
                                                            <li> <a class="cm-ajax" href="#" data-ca-target-id="pagination_contents" rel="nofollow">24 Per Page</a> </li>
                                                            <li> <a class="cm-ajax" href="#" data-ca-target-id="pagination_contents" rel="nofollow">48 Per Page</a> </li>
                                                            <li> <a class="cm-ajax" href="#" data-ca-target-id="pagination_contents" rel="nofollow">96 Per Page</a> </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid-list">
                                                <div class="row">
                                                    <div class="col-sm-4 col-lg-4 col-md-4">
                                                        <div class="thumbnail grid-thumbnail">
                                                            <div class="new-overlay discount-overlay"><span>NEW</span></div>
                                                            <form action="#" method="post" name="product_form_7856" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle ">
                                                                <input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*" />
                                                                <input type="hidden" name="redirect_url" value="#" />
                                                                <input type="hidden" name="product_data[7856][product_id]" value="7856" />
                                                                <input type="hidden" class="product-sku-code" value="RM-PT060" />
                                                                <div class="grid-list-rating"> </div>
                                                                <div class="grid-list-image">
                                                                    <a href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=230"> <img class="   " id="det_img_7856" src="<?php echo get_theme_file_uri('/images/thumbnails/450/563/detailed/38/H0zK5rO.png'); ?>"  alt="" title="" /> </a>
                                                                    <div class="grid-hover hidden-xs hidden-sm"><a href="http://staging.isiwal.com/SunnysBespoke1/?page_id=230" class="vertical-align"><span class=""><span class="product-title">Green White Stripes</span><div class="grid-list-price"> <span class="cm-reload-7856" id="old_price_update_7856"> </span> <span class="cm-reload-7856 price-update" id="price_update_7856"> <input type="hidden" name="appearance[show_price_values]" value="1" /> <input type="hidden" name="appearance[show_price]" value="1" /> <span class="price" id="line_discounted_price_7856"><span class="price-num">₹</span><span id="sec_discounted_price_7856" class="price-num">2,490</span></span> </span> </div></span></a></div>
                                                                </div>
                                                                <div class="caption">
                                                                    <div class="hidden-lg hidden-md title-and-price clearfix">
                                                                        <div class="grid-list-price"> <span class="cm-reload-7856" id="old_price_update_7856"> </span> <span class="cm-reload-7856 price-update" id="price_update_7856"> <input type="hidden" name="appearance[show_price_values]" value="1" /> <input type="hidden" name="appearance[show_price]" value="1" /> <span class="price" id="line_discounted_price_7856"><span class="price-num">₹</span><span id="sec_discounted_price_7856" class="price-num">2,490</span></span>
                                                                            </span>
                                                                        </div> <a href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=230" class="product-title">Green White Stripes</a> </div>
                                                                    <div class="actions grid-list-actions">
                                                                        <a href="#" class="fa fa-angle-left gallery-nav prev"></a>
                                                                        <div class="cm-reload-7856  add-to-cart-container" id="add_to_cart_update_7856">
                                                                            <input type="hidden" name="appearance[show_add_to_cart]" value="1" />
                                                                            <input type="hidden" name="appearance[separate_buttons]" value="" />
                                                                            <input type="hidden" name="appearance[show_list_buttons]" value="" />
                                                                            <input type="hidden" name="appearance[but_role]" value="action" />
                                                                            <input type="hidden" name="appearance[quick_view]" value="" /> <span id="cart_add_block_7856"> <button id="button_cart_7856" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7856]">Add to cart</button> </span> </div>
                                                                        <a href="#" class="fa fa-angle-right gallery-nav next"></a>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 col-lg-4 col-md-4">
                                                        <div class="thumbnail grid-thumbnail">
                                                            <div class="new-overlay discount-overlay"><span>NEW</span></div>
                                                            <form action="#" method="post" name="product_form_7847" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle ">
                                                                <input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*" />
                                                                <input type="hidden" name="redirect_url" value="" />
                                                                <input type="hidden" name="product_data[7847][product_id]" value="7847" />
                                                                <input type="hidden" class="product-sku-code" value="RM-PC086" />
                                                                <div class="grid-list-rating"> </div>
                                                                <div class="grid-list-image">
                                                                    <a href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=230"> <img class="   " id="det_img_7847" src="<?php echo get_theme_file_uri('/images/thumbnails/450/563/detailed/38/H0zJv7m_i8gx-61.png'); ?>"  alt="" title="" /> </a>
                                                                    <div class="grid-hover hidden-xs hidden-sm"><a href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=230" class="vertical-align"><span class=""><span class="product-title">Light Grey Olive checks</span><div class="grid-list-price"> <span class="cm-reload-7847" id="old_price_update_7847"> </span> <span class="cm-reload-7847 price-update" id="price_update_7847"> <input type="hidden" name="appearance[show_price_values]" value="1" /> <input type="hidden" name="appearance[show_price]" value="1" /> <span class="price" id="line_discounted_price_7847"><span class="price-num">₹</span><span id="sec_discounted_price_7847" class="price-num">2,490</span></span> </span> </div></span></a></div>
                                                                </div>
                                                                <div class="caption">
                                                                    <div class="hidden-lg hidden-md title-and-price clearfix">
                                                                        <div class="grid-list-price"> <span class="cm-reload-7847" id="old_price_update_7847"> </span> <span class="cm-reload-7847 price-update" id="price_update_7847"> <input type="hidden" name="appearance[show_price_values]" value="1" /> <input type="hidden" name="appearance[show_price]" value="1" /> <span class="price" id="line_discounted_price_7847"><span class="price-num">₹</span><span id="sec_discounted_price_7847" class="price-num">2,490</span></span>
                                                                            </span>
                                                                        </div> <a href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=230" class="product-title">Light Grey Olive checks</a> </div>
                                                                    <div class="actions grid-list-actions">
                                                                        <a href="#" class="fa fa-angle-left gallery-nav prev"></a>
                                                                        <div class="cm-reload-7847  add-to-cart-container" id="add_to_cart_update_7847">
                                                                            <input type="hidden" name="appearance[show_add_to_cart]" value="1" />
                                                                            <input type="hidden" name="appearance[separate_buttons]" value="" />
                                                                            <input type="hidden" name="appearance[show_list_buttons]" value="" />
                                                                            <input type="hidden" name="appearance[but_role]" value="action" />
                                                                            <input type="hidden" name="appearance[quick_view]" value="" /> <span id="cart_add_block_7847"> <button id="button_cart_7847" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7847]">Add to cart</button> </span> </div>
                                                                        <a href="#" class="fa fa-angle-right gallery-nav next"></a>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 col-lg-4 col-md-4">
                                                        <div class="thumbnail grid-thumbnail">
                                                            <div class="new-overlay discount-overlay"><span>NEW</span></div>
                                                            <form action="#" method="post" name="product_form_7843" enctype="multipart/form-data" class="product-form cm-disable-empty-files  cm-ajax cm-ajax-full-render cm-ajax-status-middle ">
                                                                <input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*" />
                                                                <input type="hidden" name="redirect_url" value="" />
                                                                <input type="hidden" name="product_data[7843][product_id]" value="7843" />
                                                                <input type="hidden" class="product-sku-code" value="RM-PC082" />
                                                                <div class="grid-list-rating"> </div>
                                                                <div class="grid-list-image">
                                                                    <a href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=230"> <img class="   " id="det_img_7843" src="<?php echo get_theme_file_uri('/images/thumbnails/450/563/detailed/38/H0zHUDY_7l4q-1n.png'); ?>"  alt="" title="" /> </a>
                                                                    <div class="grid-hover hidden-xs hidden-sm"><a href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=230" class="vertical-align"><span class=""><span class="product-title">Light Blue Checks</span><div class="grid-list-price"> <span class="cm-reload-7843" id="old_price_update_7843"> </span> <span class="cm-reload-7843 price-update" id="price_update_7843"> <input type="hidden" name="appearance[show_price_values]" value="1" /> <input type="hidden" name="appearance[show_price]" value="1" /> <span class="price" id="line_discounted_price_7843"><span class="price-num">₹</span><span id="sec_discounted_price_7843" class="price-num">2,490</span></span> </span> </div></span></a></div>
                                                                </div>
                                                                <div class="caption">
                                                                    <div class="hidden-lg hidden-md title-and-price clearfix">
                                                                        <div class="grid-list-price"> <span class="cm-reload-7843" id="old_price_update_7843"> </span> <span class="cm-reload-7843 price-update" id="price_update_7843"> <input type="hidden" name="appearance[show_price_values]" value="1" /> <input type="hidden" name="appearance[show_price]" value="1" /> <span class="price" id="line_discounted_price_7843"><span class="price-num">₹</span><span id="sec_discounted_price_7843" class="price-num">2,490</span></span>
                                                                            </span>
                                                                        </div> <a href=" http://staging.isiwal.com/SunnysBespoke1/?page_id=230" class="product-title">Light Blue Checks</a> </div>
                                                                    <div class="actions grid-list-actions">
                                                                        <a href="#" class="fa fa-angle-left gallery-nav prev"></a>
                                                                        <div class="cm-reload-7843  add-to-cart-container" id="add_to_cart_update_7843">
                                                                            <input type="hidden" name="appearance[show_add_to_cart]" value="1" />
                                                                            <input type="hidden" name="appearance[separate_buttons]" value="" />
                                                                            <input type="hidden" name="appearance[show_list_buttons]" value="" />
                                                                            <input type="hidden" name="appearance[but_role]" value="action" />
                                                                            <input type="hidden" name="appearance[quick_view]" value="" /> <span id="cart_add_block_7843"> <button id="button_cart_7843" class="btn ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..7843]">Add to cart</button> </span> </div>
                                                                        <a href="#" class="fa fa-angle-right gallery-nav next"></a>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pagination-bottom text-center">
                                                <nav>
                                                    <ul class="pagination">
                                                        <li class="disabled"> <a data-ca-scroll=".cm-pagination-container" class=""><span>Prev</span> </a> </li>
                                                        <li class="active"><a>1</a></li>
                                                        <li><a data-ca-scroll=".cm-pagination-container" href="#" data-ca-page="2" class="cm-history cm-ajax" data-ca-target-id="pagination_contents">2</a></li>
                                                        <li><a data-ca-scroll=".cm-pagination-container" href="#" data-ca-page="3" class="cm-history cm-ajax" data-ca-target-id="pagination_contents">3</a></li>
                                                        <li> <a data-ca-scroll=".cm-pagination-container" class=" cm-history cm-ajax" href="#" data-ca-page="2" data-ca-target-id="pagination_contents"><span>Next</span></a> </li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>

		
		
<!--footer-->
   <?php get_footer(); ?>		
<?php /* Template Name: Tracking */ ?>
<?php

get_header(); ?>

        
		<section class="dispatch-orders-tracking_history content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-3">
                                <div class="sub-menu-block">
                                    <div class="wysiwyg-content">
                                        <div class="profile-sidebar">
                                            <ul>
                                                <li><a href="login">Profile Details</a></li>
                                                <li><a href="index" class="active">Order Tracking</a></li>
                                                <li><a href="orders">Order History</a></li>
                                                <li><a href="loyalty-points">Loyalty Points History</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="col-lg-9">
                                <div class="main-content-block">
                                    <div class="text-center well">
                                        <p>We couldn't find any orders for your account. </p>
                                        <a class="btn btn-default" href="index.php"> <span class="fa fa-arrow-circle-left"></span> Go to Homepage
                                        </a>
                                        <a class="btn btn-default btn-primary" href="../custom/shirt.php"> <span class="fa"></span> Design a Custom Shirt
                                        </a>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
		
		
<!--footer-->
   <?php get_footer(); ?>		
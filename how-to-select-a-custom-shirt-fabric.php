﻿<?php /* Template Name: how-to-select-a-custom-shirt-fabric */ ?>
  <?php get_header(); ?>
  
                <section class="dispatch-pages-view how-to-select-a-custom-shirt-fabric-page content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="main-content-block">
                                    <div class="wysiwyg-content">
                                        <div>
                                            <div class="our-fabrics">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h1 class="heading">Fabrics</h1>
                                                        <hr class="heading-separator">
                                                        <h2 class="sub-heading">Selecting a fabric is where the process of crafting the perfect custom-made shirt starts. Read on to know more about our fabrics & selections.</h2> </div>
                                                </div>
                                                <div class="row two-split">
                                                    <div class="col-md-6 overlay ply">
                                                        <p>&nbsp;</p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h2>Single vs Double / Two Ply</h2>
                                                        <p>Typically, dress fabrics can either be Single ply or Double / Two ply. Ply refers to the number of yarns that are twisted together to make a single thread.
                                                            <br>
                                                            <br> Two-ply fabrics are generally superior to single-ply fabrics. They have a smooth, crisp hand feel and are luxurious to wear. This is not to say that Single-ply fabrics are not luxurious – higher count single ply fabrics with compact construction are equally sought after.
                                                            <br>
                                                            <br> The 2 in a 100/2's or the 1 in the 70/1 refer to the ply of the fabric.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row two-split">
                                                    <div class="col-md-6 overlay count hidden-md hidden-lg">
                                                        <p>&nbsp;</p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h2>Fabric Count</h2>
                                                        <p>The count refers to the yarn size, and as a rule of thumb, the higher the thread count; the more luxurious (and expensive) you can expect the fabric to be.
                                                            <br>
                                                            <br> However, thread count is not everything – the compactness and density of the woven fabric play a big part in determining the overall fabric quality.
                                                            <br>
                                                            <br> Higher count fabrics have a tendency to wrinkle more easily, given that they are relatively finer. Hence, the weave plays an important role in fabric selection.
                                                            <br>
                                                            <br> The 100 in a 100/2 or the 70 in the 70/1 refer to the count of the fabric.
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 overlay count hidden-xs hidden-sm">
                                                        <p>&nbsp;</p>
                                                    </div>
                                                </div>
                                                <div class="row two-split">
                                                    <div class="col-md-6 overlay composition">
                                                        <p>&nbsp;</p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h2>Fabric Composition</h2>
                                                        <p>Various materials can be used to weave a fabric. The most common are Cotton and Linen.
                                                            <br>
                                                            <br> Known for its long staples and superior quality 100% Egyptian Cotton is the premier choice of shirting fabric.
                                                            <br>
                                                            <br> 100% Linen is a strong, durable material, best used in warm weather.
                                                            <br>
                                                            <br> For added stretch, Lycra is commonly blended with cotton fabric. The Cotton-Lycra blend typically works for shirts that are meant to slim fit or body hugging.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row two-split">
                                                    <div class="col-md-6 overlay sourcing hidden-md hidden-lg">
                                                        <p>&nbsp;</p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h2>Fabric Sourcing</h2>
                                                        <p>At BSC, much attention is paid to the provenance of our raw materials, especially our fabrics. We work with only a handful of suppliers across the world, to bring you only the best quality Egyptian cotton and Pure Linens.
                                                            <br>
                                                            <br> Our fabrics are pre-washed to avoid shrinkage, and in certain cases, treated with Enzymes to give a softer, more comfortable feel. Many of our fabrics are treated with easy-care washes, to make ironing easier.
                                                            <br>
                                                            <br> Ultimately, the selection of your dress fabric will be a careful consideration of the Ply, Count, Composition and Weave.
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 overlay sourcing hidden-xs hidden-sm">
                                                        <p>&nbsp;</p>
                                                    </div>
                                                </div>
                                                <div class="row fabrics">
                                                    <div class="col-md-12">
                                                        <h2 class="text-center">Fabric Weaves</h2> </div>
                                                    <div class="col-md-5 col-sm-5 fabric">
                                                        <div class="row">
                                                            <div class="col-md-6 image"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/fabrics/1-our-fabrics-poplin.jpg'); ?>"> </div>
                                                            <div class="col-md-6 content">
                                                                <h3>Poplin</h3>
                                                                <p>The quintessential shirting fabric, Poplin or Broadcloth bears little texture, with its tightly woven, over-under weave pattern creating a thin, soft, smooth and lightweight fabric. Poplin are breathable, making them the ideal choice to wear in the summers, or under jackets and blazers for formal occasions. The higher the thread count, the more luxurious the fabric.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 fabric">
                                                        <div class="row">
                                                            <div class="col-md-6 image"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/fabrics/2-our-fabrics-oxford.jpg'); ?>"> </div>
                                                            <div class="col-md-6 content">
                                                                <h3>Oxford</h3>
                                                                <p>Even though Oxford fabric has a slightly coarse and heavy texture, it is a soft, breathable and durable fabric to wear on a daily basis. Known for its characteristic 'basket' weave, the fabric is usually reserved for casual shirts paired with a button down collar. A versatile 'must have' for every wardrobe.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 fabric">
                                                        <div class="row">
                                                            <div class="col-md-6 image"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/fabrics/3-our-fabrics-satin.jpg'); ?>"> </div>
                                                            <div class="col-md-6 content">
                                                                <h3>Satin</h3>
                                                                <p>A type of textile weave, Satin is somewhat more shiny than the average Poplin weave. The fabric thus created is more lustrous and gives an exclusive look to the shirt. The weave is most classically used with silk thread, though it can also be used with other materials. Satin shirts work well under a jacket or in informal night-time settings.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 fabric">
                                                        <div class="row">
                                                            <div class="col-md-6 image"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/fabrics/4-our-fabrics-dobby.jpg'); ?>"> </div>
                                                            <div class="col-md-6 content">
                                                                <h3>Dobby</h3>
                                                                <p>The Dobby weave is created using small geometric patterns that repeat throughout the fabric. Solid coloured Dobby shirts tend to have a faint stripe or dotted patterns woven in the same colour as the base cloth. This slightly weighted fabric became popular in the 1950's, its uniquely patterned nature and hard-to-wrinkle structure proving to be an excellent choice for workwear shirts.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 fabric">
                                                        <div class="row">
                                                            <div class="col-md-6 image"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/fabrics/5-our-fabrics-twill.jpg'); ?>"> </div>
                                                            <div class="col-md-6 content">
                                                                <h3>Twill</h3>
                                                                <p>A popular weave, Twill is easily recognizable for its distinctive diagonal ribbed texture. Twill fabrics are soft, heavier and more durable that their counterparts, and drape easier as well. Twill won’t give you the same “crisp” look that freshly pressed poplin can, but it’s relatively easy to iron and resistant to wrinkles. Twill shirts work well for situations that call for smart informals.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 fabric">
                                                        <div class="row">
                                                            <div class="col-md-6 image"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/fabrics/6-our-fabrics-herringbone.jpg'); ?>"> </div>
                                                            <div class="col-md-6 content">
                                                                <h3>Herringbone</h3>
                                                                <p>An ancient variation of the twill weave, Herringbone is characterized by a distinctive V shape weave - It’s zigzagging wale gives the fabric the appearance of a fish skeleton. In addition to adding style to a shirt, herringbone’s zigzag construction also adds strength and durability. Pair your Herringbone shirt with a sharp suit for an elegant event or dark denims for a dressed-down affair.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 fabric">
                                                        <div class="row">
                                                            <div class="col-md-6 image"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/fabrics/7-our-fabrics-houndstooth.jpg'); ?>"> </div>
                                                            <div class="col-md-6 content">
                                                                <h3>Houndstooth</h3>
                                                                <p>Yet another variation of Twill, the Houndstooth pattern is made from weaving two threads over and two threads under the warp. When applied to shirts, the pattern makes for an interesting style for semi-formal and casual styles.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 fabric">
                                                        <div class="row">
                                                            <div class="col-md-6 image"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/fabrics/8-our-fabrics-royal-oxford.jpg'); ?>"> </div>
                                                            <div class="col-md-6 content">
                                                                <h3>Royal Oxford</h3>
                                                                <p>Royal Oxford is a dressy fabric with a distinctive shine and texture. The diamond weave is much more noticeable, and the fabric is a lot thinner than regular Oxfords. Soft, warm and easy to iron, Royal Oxfords are ideal for the office.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 fabric">
                                                        <div class="row">
                                                            <div class="col-md-6 image"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/fabrics/9-our-fabrics-linen.jpg'); ?>"> </div>
                                                            <div class="col-md-6 content">
                                                                <h3>Linen</h3>
                                                                <p>Our ultra-luxurious Linen fabric is cool and perfect for summer. Linen fabric feels cool to the touch. It is light and smooth, making the finished fabric lint-free,which gets softer with every wash. Linen also tends to wrinkle more easily than cotton and generally feels much more relaxed because of this.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 fabric">
                                                        <div class="row">
                                                            <div class="col-md-6 image"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/fabrics/10-our-fabrics-chambray.jpg'); ?>"> </div>
                                                            <div class="col-md-6 content">
                                                                <h3>Chambray</h3>
                                                                <p>Chambray is soft, shiny, thin, light weight cotton that looks similar to denim. Since Chambray is lighter in weight, it's a good option for when you want to look sharp but feel cool. Pair it with an outfit that can go from the board-room to the bar.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 fabric">
                                                        <div class="row">
                                                            <div class="col-md-6 image"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/fabrics/11-our-fabrics-fil-a-fil.jpg'); ?>"> </div>
                                                            <div class="col-md-6 content">
                                                                <h3>Fil-a-fil</h3>
                                                                <p>The Fil-à-Fil or End-on-End Giza fabric is woven with threads in two different colours for a subtly irregular, yet richer texture. It is a light, thin and comfortable fabric suitable for solid shirts. Fil-a-Fil shirts match well with a proper suit but also go with a pair of khakis, making them a good choice to pick all year long.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 fabric">
                                                        <div class="row">
                                                            <div class="col-md-6 image"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/fabrics/12-our-fabrics-denim.jpg'); ?>"> </div>
                                                            <div class="col-md-6 content">
                                                                <h3>Denim</h3>
                                                                <p>Essentially a twill fabric, denim is a sturdy and coarse version, often dyed with indigo. Denim shirts, however, are softer and lighter than jeans, and work very well for a number of occasions. Lighter shades work well in the day, while darker denims shirts can be worn for dressier events in the evening.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 fabric">
                                                        <div class="row">
                                                            <div class="col-md-6 image"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/fabrics/15-our-fabrics-jacquard.jpg'); ?>"> </div>
                                                            <div class="col-md-6 content">
                                                                <h3>Jacquard</h3>
                                                                <p>Jacquard fabrics are made using complex weaving techniques that create patterns in the weave itself often by way of color. The fabric itself is fairly sturdy and heavy, resulting in a crease-free look throughout the day.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 fabric">
                                                        <div class="row">
                                                            <div class="col-md-6 image"><img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/fabrics/16-our-fabrics-stretch-dobby.jpg'); ?>"> </div>
                                                            <div class="col-md-6 content">
                                                                <h3>Stretch Dobby</h3>
                                                                <p>A combination of cotton and lycra lends added stretch to this dobby weave. The stretchiness of the fabric lends perfectly to slim-fit shirts.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
  <!--footer-->
 <?php get_footer(); ?>
<?php /* Template Name: Parts of shirt anatomy of shirt */ ?>
<?php
get_header(); ?>

        
		<section class="dispatch-pages-view parts-of-shirt-anatomy-of-shirt-page content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="main-content-block">
                                    <div class="wysiwyg-content">
                                        <div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h1 class="heading">Shirt Construction</h1>
                                                    <hr class="heading-separator">
                                                    <h2 class="sub-heading">From collar to cuff, every little detail matters.</h2> </div>
                                            </div>
                                            <div class="row" style="padding-bottom:45px">
                                                <div class="col-md-12"> <img src="<?php echo get_theme_file_uri('/design/themes/bsc/media/images/static/shirt-construction/shirt-construction.gif'); ?>"> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>

		
		
<!--footer-->
   <?php get_footer(); ?>		
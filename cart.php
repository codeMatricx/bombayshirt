<?php /* Template Name: cart */ ?>
<?php get_header(); ?>

                <section class="dispatch-checkout-cart content">
                    <div class="container-fluid  content-grid">
                        <div class="row">
                            <section class="col-lg-12 main-content-grid">
                                <div class="mainbox-container clearfix">
                                    <div class="mainbox-body">
                                        <p class="well well-lg no-items">Your cart is empty. Kindly ensure that you have SIGNED IN. </p>
                                        <div class="buttons-container wrap"> <a class="btn btn-default  " href="#"> Continue shopping</a> </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
  <!--footer-->
 <?php get_footer(); ?> 